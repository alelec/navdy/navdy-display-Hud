.class public Lcom/navdy/hud/app/framework/glance/GlanceNotification;
.super Ljava/lang/Object;
.source "GlanceNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/framework/notifications/IScrollEvent;
.implements Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
    }
.end annotation


# static fields
.field private static final IMAGE_SCALE:F = 0.5f

.field private static handler:Landroid/os/Handler;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private alive:Z

.field private appIcon:I

.field private audioFeedback:Landroid/widget/ImageView;

.field private bottomScrub:Landroid/view/View;

.field private bus:Lcom/squareup/otto/Bus;

.field private cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

.field private cannedReplyMessages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private final color:I

.field private colorImageView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field private glanceContainer:Landroid/view/ViewGroup;

.field private final glanceEvent:Lcom/navdy/service/library/events/glances/GlanceEvent;

.field private glanceExtendedContainer:Landroid/view/ViewGroup;

.field private hasFuelLevelInfo:Z

.field private final id:Ljava/lang/String;

.field private initialReplyMode:Z

.field private largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field private mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field private mainTitle:Landroid/widget/TextView;

.field private messageStr:Ljava/lang/String;

.field private number:Ljava/lang/String;

.field private onBottom:Z

.field private onTop:Z

.field private operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

.field private photoCheckRequired:Z

.field private final postTime:Ljava/util/Date;

.field private progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

.field private replyExitView:Landroid/view/ViewGroup;

.field private replyMsgView:Landroid/view/ViewGroup;

.field private roundTransformation:Lcom/squareup/picasso/Transformation;

.field private scrollListener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

.field private scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

.field private sideImage:Landroid/widget/ImageView;

.field private smallType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field private source:Ljava/lang/String;

.field private stringBuilder1:Ljava/lang/StringBuilder;

.field private stringBuilder2:Ljava/lang/StringBuilder;

.field private subTitle:Landroid/widget/TextView;

.field private final supportsScroll:Z

.field private text1:Landroid/widget/TextView;

.field private text2:Landroid/widget/TextView;

.field private text3:Landroid/widget/TextView;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private topScrub:Landroid/view/View;

.field private ttsMessage:Ljava/lang/String;

.field private ttsSent:Z

.field private updateTimeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Ljava/util/Map;)V
    .locals 7
    .param p1, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .param p2, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .param p3, "largeType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "eventData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x1

    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    .line 116
    new-instance v2, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v2}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    invoke-virtual {v2, v3}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 124
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->alive:Z

    .line 156
    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;

    .line 228
    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 299
    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollListener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    .line 357
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceEvent:Lcom/navdy/service/library/events/glances/GlanceEvent;

    .line 358
    iput-object p2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 359
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceEvent:Lcom/navdy/service/library/events/glances/GlanceEvent;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getNotificationId(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    .line 360
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->getColor()I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->color:I

    .line 361
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->getSideIcon()I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->appIcon:I

    .line 363
    if-nez p4, :cond_1

    .line 364
    invoke-static {p1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->buildDataMap(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    .line 369
    :goto_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 385
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPhotoCheckRequired(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->photoCheckRequired:Z

    .line 386
    new-instance v2, Ljava/util/Date;

    iget-object v3, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->postTime:Ljava/util/Date;

    .line 387
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 388
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getSmallViewType(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->smallType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 389
    if-nez p3, :cond_2

    .line 390
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getLargeViewType(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 394
    :goto_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getTtsMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsMessage:Ljava/lang/String;

    .line 395
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getGlanceMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->messageStr:Ljava/lang/String;

    .line 396
    new-instance v2, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    .line 397
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->supportScroll(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    .line 399
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bus:Lcom/squareup/otto/Bus;

    .line 400
    return-void

    .line 366
    :cond_1
    iput-object p4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    goto :goto_0

    .line 371
    :sswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v3, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 372
    .local v0, "iconStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 373
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getIcon(Ljava/lang/String;)I

    move-result v1

    .line 374
    .local v1, "nIcon":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 375
    iput v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->appIcon:I

    goto/16 :goto_1

    .line 381
    .end local v0    # "iconStr":Ljava/lang/String;
    .end local v1    # "nIcon":I
    :sswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v3, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->hasFuelLevelInfo:Z

    goto/16 :goto_1

    .line 392
    :cond_2
    iput-object p3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    goto :goto_2

    .line 369
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x13 -> :sswitch_0
    .end sparse-switch
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .param p2, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 350
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Ljava/util/Map;)V

    .line 351
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToReplyScreen()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->call()V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->revertChoice()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->topScrub:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setMainImage()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->hasFuelLevelInfo:Z

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setTitle()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setExpandedContent(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->postTime:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/common/TimeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    return-object v0
.end method

.method private call()V
    .locals 4

    .prologue
    .line 1028
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V

    .line 1029
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    return-void
.end method

.method private cancelTts()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1284
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isTtsOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1285
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsSent:Z

    if-eqz v0, :cond_0

    .line 1286
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tts-cancelled ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1287
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1288
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsSent:Z

    .line 1289
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stopAudioAnimation(Z)V

    .line 1292
    :cond_0
    return-void
.end method

.method private cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V
    .locals 9
    .param p1, "viewType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const v8, 0x7f0e0051

    const v7, 0x7f0e0050

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1201
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1202
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 1203
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1206
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1264
    :cond_1
    :goto_0
    invoke-static {p1, p2}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 1265
    return-void

    .line 1208
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1209
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1211
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1212
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1214
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 1215
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 1216
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 1217
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v7, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1218
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v8, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1220
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v5, v6, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 1221
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 1223
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1224
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto :goto_0

    .line 1228
    :pswitch_1
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 1229
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    if-eqz v2, :cond_1

    .line 1230
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/ObservableScrollView;->fullScroll(I)Z

    .line 1231
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z

    .line 1232
    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z

    goto :goto_0

    .line 1238
    :pswitch_2
    const v2, 0x7f0e00c3

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1239
    .local v1, "textView":Landroid/widget/TextView;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0018

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1243
    .end local v1    # "textView":Landroid/widget/TextView;
    :pswitch_3
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto :goto_0

    .line 1247
    :pswitch_4
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1248
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1250
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1251
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1253
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 1254
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 1255
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 1256
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v7, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1257
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v8, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1259
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1260
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto/16 :goto_0

    .line 1206
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 636
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 637
    return-void
.end method

.method private getCannedReplyMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cannedReplyMessages:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1409
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->getCannedMessages()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cannedReplyMessages:Ljava/util/List;

    .line 1411
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cannedReplyMessages:Ljava/util/List;

    return-object v0
.end method

.method private getReplyView(ILandroid/content/Context;)Landroid/view/ViewGroup;
    .locals 6
    .param p1, "item"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1295
    if-nez p1, :cond_1

    .line 1297
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    .line 1298
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030026

    invoke-virtual {v2, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    .line 1302
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    .line 1318
    :goto_1
    return-object v2

    .line 1300
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    goto :goto_0

    .line 1305
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    if-nez v2, :cond_3

    .line 1306
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030023

    invoke-virtual {v2, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    .line 1310
    :goto_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getCannedReplyMessages()Ljava/util/List;

    move-result-object v0

    .line 1311
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le p1, v2, :cond_2

    .line 1312
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    .line 1314
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    const v3, 0x7f0e00c3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1315
    .local v1, "textView":Landroid/widget/TextView;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1316
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    const v3, 0x7f0e010a

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "textView":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 1317
    .restart local v1    # "textView":Landroid/widget/TextView;
    add-int/lit8 v2, p1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1318
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    goto :goto_1

    .line 1308
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "textView":Landroid/widget/TextView;
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    goto :goto_2
.end method

.method private hasReplyAction()Z
    .locals 2

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceEvent:Lcom/navdy/service/library/events/glances/GlanceEvent;

    iget-object v0, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceEvent:Lcom/navdy/service/library/events/glances/GlanceEvent;

    iget-object v0, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeParent(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1323
    if-eqz p1, :cond_0

    .line 1324
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1325
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 1326
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1327
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "reply view removed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1330
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method

.method private reply(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "caller"    # Ljava/lang/String;

    .prologue
    .line 1058
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->alive:Z

    .line 1059
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v0

    .line 1060
    .local v0, "glanceHandler":Lcom/navdy/hud/app/framework/glance/GlanceHandler;
    invoke-virtual {v0, p2, p1, p3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1062
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, p2, p1, p3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsSuccessNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    :goto_0
    return-void

    .line 1065
    :cond_0
    invoke-virtual {v0, p2, p1, p3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private revertChoice()V
    .locals 7

    .prologue
    const v6, 0x7f0e0051

    const v5, 0x7f0e0050

    const/4 v4, 0x0

    .line 807
    const/4 v0, 0x0

    .line 808
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    const/4 v1, 0x0

    .line 809
    .local v1, "defaultSelection":I
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    .line 810
    .local v2, "tag":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 812
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    .line 813
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v5, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 814
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v6, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 816
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    if-ne v2, v3, :cond_1

    .line 817
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    .line 818
    const/4 v1, 0x1

    .line 833
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 834
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 835
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 836
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 837
    :goto_1
    return-void

    .line 819
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    if-ne v2, v3, :cond_2

    .line 820
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    .line 821
    const/4 v1, 0x1

    goto :goto_0

    .line 822
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noMessage:Ljava/util/List;

    if-ne v2, v3, :cond_3

    .line 823
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noMessage:Ljava/util/List;

    goto :goto_0

    .line 824
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    if-ne v2, v3, :cond_4

    .line 825
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    goto :goto_0

    .line 827
    :cond_4
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 828
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setChoices()V

    goto :goto_1
.end method

.method private sendTts()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1268
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isTtsOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1269
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsSent:Z

    if-nez v1, :cond_0

    .line 1270
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tts-send ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1271
    new-instance v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsMessage:Ljava/lang/String;

    .line 1272
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_MESSAGE_READ_OUT:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 1273
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    .line 1274
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v1

    .line 1275
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->sendStatus(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v1

    .line 1276
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v0

    .line 1277
    .local v0, "request":Lcom/navdy/service/library/events/audio/SpeechRequest;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/LocalSpeechRequest;

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/event/LocalSpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1278
    iput-boolean v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsSent:Z

    .line 1281
    .end local v0    # "request":Lcom/navdy/service/library/events/audio/SpeechRequest;
    :cond_0
    return-void
.end method

.method private setChoices()V
    .locals 7

    .prologue
    .line 971
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 972
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V

    .line 1025
    :goto_0
    return-void

    .line 976
    :cond_0
    const/4 v0, 0x0

    .line 977
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    const/4 v1, 0x0

    .line 978
    .local v1, "defaultSelection":I
    const/4 v2, 0x0

    .line 980
    .local v2, "replyAction":Z
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 991
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 992
    .local v3, "tag":Ljava/lang/Object;
    if-nez v3, :cond_3

    .line 993
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->hasReplyAction()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 994
    const/4 v2, 0x1

    .line 997
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    if-eqz v4, :cond_6

    .line 999
    :cond_2
    if-eqz v2, :cond_5

    .line 1001
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    .line 1002
    const/4 v1, 0x1

    .line 1021
    .end local v3    # "tag":Ljava/lang/Object;
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 1022
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 1023
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v4, v0, v1, v5, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 1024
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 982
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v5, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_4

    .line 983
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoices:Ljava/util/List;

    .line 988
    :goto_2
    const/4 v1, 0x0

    .line 989
    goto :goto_1

    .line 985
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoicesNoRoute:Ljava/util/List;

    goto :goto_2

    .line 1005
    .restart local v3    # "tag":Ljava/lang/Object;
    :cond_5
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    .line 1006
    const/4 v1, 0x1

    goto :goto_1

    .line 1011
    :cond_6
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v4}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isCalendarApp(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1012
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarOptions:Ljava/util/List;

    goto :goto_1

    .line 1014
    :cond_7
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    goto :goto_1

    .line 980
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method private setExpandedContent(Landroid/view/View;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1099
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    if-nez v8, :cond_0

    .line 1198
    :goto_0
    return-void

    .line 1106
    :cond_0
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    iget-object v9, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    goto :goto_0

    .line 1110
    :pswitch_0
    const v8, 0x7f0e0105

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1111
    .local v5, "textview":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1112
    .local v1, "s":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1113
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1119
    :goto_1
    const v8, 0x7f0e0106

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "textview":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 1120
    .restart local v5    # "textview":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "s":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1121
    .restart local v1    # "s":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1122
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1123
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1128
    :goto_2
    const v8, 0x7f0e0107

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 1129
    .local v7, "viewGroup":Landroid/view/ViewGroup;
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "s":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1130
    .restart local v1    # "s":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1131
    const v8, 0x7f0e0108

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "textview":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 1132
    .restart local v5    # "textview":Landroid/widget/TextView;
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1133
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 1116
    .end local v7    # "viewGroup":Landroid/view/ViewGroup;
    :cond_1
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1125
    :cond_2
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 1135
    .restart local v7    # "viewGroup":Landroid/view/ViewGroup;
    :cond_3
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 1141
    .end local v1    # "s":Ljava/lang/String;
    .end local v5    # "textview":Landroid/widget/TextView;
    .end local v7    # "viewGroup":Landroid/view/ViewGroup;
    :pswitch_1
    const v8, 0x7f0e0088

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1142
    .local v3, "textView1":Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0c0019

    invoke-virtual {v3, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1144
    const v8, 0x7f0e0089

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1145
    .local v4, "textView2":Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0c001a

    invoke-virtual {v4, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1147
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1149
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_4

    .line 1150
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09010a

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v13, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_ADDRESS:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v13}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v13, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v13}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1155
    :goto_3
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1156
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1152
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090185

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 1174
    .end local v3    # "textView1":Landroid/widget/TextView;
    .end local v4    # "textView2":Landroid/widget/TextView;
    :pswitch_2
    const v8, 0x7f0e00c3

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1175
    .local v6, "title":Landroid/widget/TextView;
    const v8, 0x7f0e010a

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1177
    .local v0, "message":Landroid/widget/TextView;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    iget-object v9, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 1192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v10, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->postTime:Ljava/util/Date;

    iget-object v11, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-static {v8, v9, v10, v11}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getTimeStr(JLjava/util/Date;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1195
    :goto_4
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->messageStr:Ljava/lang/String;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1182
    :pswitch_3
    sget v8, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1183
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1184
    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v9, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1185
    .local v2, "subject":Ljava/lang/String;
    if-nez v2, :cond_5

    .line 1186
    const-string v2, ""

    .line 1188
    :cond_5
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 1106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 1177
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private setMainImage()V
    .locals 24

    .prologue
    .line 870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isCalendarApp(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->colorImageView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->getColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 872
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text1:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->starts:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 874
    const-wide/16 v18, 0x0

    .line 875
    .local v18, "millis":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v3, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 876
    .local v22, "str":Ljava/lang/String;
    if-eqz v22, :cond_0

    .line 878
    :try_start_0
    invoke-static/range {v22 .. v22}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v18

    .line 883
    :cond_0
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v18, v2

    if-lez v2, :cond_4

    .line 885
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getCalendarTime(JLjava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v9

    .line 886
    .local v9, "data":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 890
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 892
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 893
    .local v20, "pmMarker":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 895
    sget v16, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarTimeMargin:I

    .line 896
    .local v16, "margin":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text3:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0027

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 898
    new-instance v21, Landroid/text/SpannableStringBuilder;

    invoke-direct/range {v21 .. v21}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 899
    .local v21, "spannable":Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 900
    invoke-virtual/range {v21 .. v21}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v15

    .line 901
    .local v15, "len":I
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 902
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarpmMarker:I

    invoke-direct {v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual/range {v21 .. v21}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v15, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 917
    .end local v15    # "len":I
    .end local v20    # "pmMarker":Ljava/lang/String;
    .end local v21    # "spannable":Landroid/text/SpannableStringBuilder;
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 918
    .local v14, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move/from16 v0, v16

    iput v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 929
    .end local v9    # "data":Ljava/lang/String;
    .end local v16    # "margin":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getTtsMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->ttsMessage:Ljava/lang/String;

    .line 931
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder1:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 932
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stringBuilder2:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 968
    .end local v14    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "millis":J
    .end local v22    # "str":Ljava/lang/String;
    :cond_1
    :goto_3
    return-void

    .line 879
    .restart local v18    # "millis":J
    .restart local v22    # "str":Ljava/lang/String;
    :catch_0
    move-exception v23

    .line 880
    .local v23, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 905
    .end local v23    # "t":Ljava/lang/Throwable;
    .restart local v9    # "data":Ljava/lang/String;
    .restart local v20    # "pmMarker":Ljava/lang/String;
    :cond_2
    sget v16, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNowMargin:I

    .line 906
    .restart local v16    # "margin":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0026

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text3:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 911
    .end local v16    # "margin":I
    .end local v20    # "pmMarker":Ljava/lang/String;
    :cond_3
    sget v16, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNormalMargin:I

    .line 912
    .restart local v16    # "margin":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0025

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 913
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 914
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text3:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 920
    .end local v9    # "data":Ljava/lang/String;
    .end local v16    # "margin":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text1:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 921
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0026

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 922
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->questionMark:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text3:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 925
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 926
    .restart local v14    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNowMargin:I

    iput v2, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/16 :goto_2

    .line 934
    .end local v14    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "millis":J
    .end local v22    # "str":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->getMainIcon()I

    move-result v12

    .line 936
    .local v12, "image":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    if-ne v2, v3, :cond_6

    .line 937
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    sget-object v3, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 938
    .local v11, "iconStr":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 939
    invoke-static {v11}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getIcon(Ljava/lang/String;)I

    move-result v17

    .line 940
    .local v17, "nIcon":I
    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_6

    .line 941
    move/from16 v12, v17

    .line 946
    .end local v11    # "iconStr":Ljava/lang/String;
    .end local v17    # "nIcon":I
    :cond_6
    const/4 v2, -0x1

    if-eq v12, v2, :cond_7

    .line 947
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v3, 0x0

    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v12, v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto/16 :goto_3

    .line 951
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->photoCheckRequired:Z

    if-eqz v2, :cond_9

    .line 952
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    .line 953
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v8

    .line 954
    .local v8, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v8, v2}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v10

    .line 955
    .local v10, "defaultImageIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v8, v10}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 959
    .end local v8    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v10    # "defaultImageIndex":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->requestLayout()V

    goto/16 :goto_3

    .line 957
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->photoCheckRequired:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object/from16 v7, p0

    invoke-static/range {v2 .. v7}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto :goto_4

    .line 961
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->isDefaultIconBasedOnId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 962
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v8

    .line 963
    .restart local v8    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v8, v2}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v13

    .line 964
    .local v13, "index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v8, v13}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto/16 :goto_3
.end method

.method private setSideImage()V
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sideImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->appIcon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 867
    return-void
.end method

.method private setSubTitle()V
    .locals 3

    .prologue
    .line 858
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getSubTitle(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 859
    .local v0, "text":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 860
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 862
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 863
    return-void
.end method

.method private setTitle()V
    .locals 3

    .prologue
    .line 840
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getTitle(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    .line 842
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 843
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;

    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getNumber(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    .line 846
    :cond_0
    const/4 v0, 0x0

    .line 847
    .local v0, "isNumber":Z
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 848
    const/4 v0, 0x1

    .line 850
    :cond_1
    if-eqz v0, :cond_2

    .line 851
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 855
    :goto_0
    return-void

    .line 853
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private startAudioAnimation()V
    .locals 3

    .prologue
    .line 1373
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 1374
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1375
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1376
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1377
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    const v2, 0x7f02000e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1378
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 1379
    .local v0, "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 1381
    .end local v0    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_0
    return-void
.end method

.method private stopAudioAnimation(Z)V
    .locals 4
    .param p1, "showFadeOutAnimation"    # Z

    .prologue
    .line 1384
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1385
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 1386
    .local v0, "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1387
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 1388
    if-eqz p1, :cond_1

    .line 1389
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$4;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 1401
    .end local v0    # "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    :cond_0
    :goto_0
    return-void

    .line 1397
    .restart local v0    # "animationDrawable":Landroid/graphics/drawable/AnimationDrawable;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V
    .locals 7
    .param p1, "mode"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1033
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_1

    .line 1055
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v3}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    .line 1037
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v1, 0x7f0e0050

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1038
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v1, 0x7f0e0051

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 1039
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-ne p1, v0, :cond_2

    .line 1040
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 1041
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v4, v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 1042
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 1043
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v3}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->expandNotification(Z)V

    goto :goto_0

    .line 1047
    :cond_2
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 1048
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v6, v4, v6, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 1049
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 1050
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->initialReplyMode:Z

    .line 1052
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->expandNotification(Z)V

    goto :goto_0
.end method

.method private switchToReplyScreen()V
    .locals 12

    .prologue
    .line 1415
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1416
    .local v10, "args":Landroid/os/Bundle;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1417
    .local v0, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    if-nez v2, :cond_0

    .line 1418
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v5

    .line 1419
    .local v5, "defaultImageIndex":I
    new-instance v1, Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;IJ)V

    .line 1420
    .local v1, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1424
    .end local v1    # "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v5    # "defaultImageIndex":I
    :goto_0
    const-string v2, "CONTACTS"

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1425
    const-string v2, "NOTIF_ID"

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v2, "MENU_MODE"

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->REPLY_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v3

    invoke-virtual {v10, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1427
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    const/4 v8, 0x0

    sget-object v9, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 1428
    return-void

    .line 1422
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private updateState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 616
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 617
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-nez v0, :cond_1

    .line 618
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setTitle()V

    .line 623
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setSubTitle()V

    .line 624
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setSideImage()V

    .line 625
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setMainImage()V

    .line 626
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setChoices()V

    .line 628
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 630
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 631
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 546
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v1, 0x7f0e0051

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1086
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V

    .line 1087
    const/4 v0, 0x1

    .line 1089
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 551
    iget v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->color:I

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 438
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-nez v3, :cond_2

    .line 439
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v3, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    .line 441
    iget-boolean v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    if-eqz v3, :cond_1

    .line 442
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e010b

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->topScrub:Landroid/view/View;

    .line 443
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->topScrub:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 444
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e010c

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bottomScrub:Landroid/view/View;

    .line 445
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bottomScrub:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 446
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e0109

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/ObservableScrollView;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    .line 447
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollListener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->setScrollListener(Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;)V

    .line 450
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setExpandedContent(Landroid/view/View;)V

    .line 451
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    .line 467
    .end local p2    # "data":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 453
    .restart local p2    # "data":Ljava/lang/Object;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    .line 455
    .local v1, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    iget-boolean v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->initialReplyMode:Z

    if-eqz v3, :cond_4

    .line 456
    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->initialReplyMode:Z

    .line 457
    const/4 v0, 0x1

    .line 462
    .end local p2    # "data":Ljava/lang/Object;
    .local v0, "current":I
    :goto_1
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v2

    .line 463
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 464
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    .line 466
    :cond_3
    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getReplyView(ILandroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v2

    .line 467
    goto :goto_0

    .line 459
    .end local v0    # "current":I
    .end local v2    # "view":Landroid/view/View;
    .restart local p2    # "data":Ljava/lang/Object;
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "current":I
    goto :goto_1
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 473
    sget v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    return v0
.end method

.method public getGlanceApp()Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 531
    const/16 v0, 0x61a8

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->notificationType:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f0e0112

    .line 416
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->smallType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e00cc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    .line 419
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    .line 420
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sideImage:Landroid/widget/ImageView;

    .line 421
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0113

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    .line 424
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isCalendarApp(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->colorImageView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .line 426
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text1:Landroid/widget/TextView;

    .line 427
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0106

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text2:Landroid/widget/TextView;

    .line 428
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0108

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->text3:Landroid/widget/TextView;

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    return-object v0

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    goto :goto_0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 11
    .param p1, "viewIn"    # Z

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 600
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 601
    .local v3, "set":Landroid/animation/AnimatorSet;
    if-nez p1, :cond_0

    .line 602
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 603
    .local v0, "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 604
    .local v1, "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_2

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 605
    .local v2, "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 612
    :goto_0
    return-object v3

    .line 607
    .end local v0    # "o1":Landroid/animation/ObjectAnimator;
    .end local v1    # "o2":Landroid/animation/ObjectAnimator;
    .end local v2    # "o3":Landroid/animation/ObjectAnimator;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_3

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 608
    .restart local v0    # "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_4

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 609
    .restart local v1    # "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_5

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 610
    .restart local v2    # "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    .line 602
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 603
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 604
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 607
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 608
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 609
    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 536
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->alive:Z

    return v0
.end method

.method public isContextValid()Z
    .locals 1

    .prologue
    .line 803
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x0

    return v0
.end method

.method public isShowingReadOption()Z
    .locals 2

    .prologue
    .line 1070
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v1, :cond_1

    .line 1071
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 1073
    .local v0, "tag":Ljava/lang/Object;
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    if-ne v0, v1, :cond_1

    .line 1076
    :cond_0
    const/4 v1, 0x1

    .line 1080
    .end local v0    # "tag":Ljava/lang/Object;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 798
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1361
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 1362
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stopAudioAnimation(Z)V

    .line 1364
    :cond_0
    return-void
.end method

.method public onContactFound(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;)V
    .locals 11
    .param p1, "event"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 660
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_1

    .line 697
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag()Ljava/lang/Object;

    move-result-object v10

    .line 664
    .local v10, "tag":Ljava/lang/Object;
    if-eqz v10, :cond_0

    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    if-ne v10, v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->contact:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->identifier:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->contact:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/service/library/events/contacts/Contact;

    .line 670
    .local v6, "c":Lcom/navdy/service/library/events/contacts/Contact;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    if-nez v1, :cond_3

    .line 671
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    .line 673
    :cond_3
    new-instance v9, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v9, v6}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/Contact;)V

    .line 674
    .local v9, "obj":Lcom/navdy/hud/app/framework/contacts/Contact;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v9, v1}, Lcom/navdy/hud/app/framework/contacts/Contact;->setName(Ljava/lang/String;)V

    .line 675
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    iget-object v1, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->contact:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 677
    iget-object v1, v6, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    goto :goto_1

    .line 680
    .end local v6    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v9    # "obj":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contact info found:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->identifier:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " number:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->contact:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 681
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setSubTitle()V

    .line 682
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->hasReplyAction()Z

    move-result v0

    if-nez v0, :cond_5

    .line 683
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 684
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 689
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_6

    .line 690
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v7

    .line 691
    .local v7, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v8

    .line 692
    .local v8, "defaultImageIndex":I
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto/16 :goto_0

    .line 686
    .end local v7    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v8    # "defaultImageIndex":I
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 687
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_2

    .line 694
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->identifier:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto/16 :goto_0
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 6
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 559
    sget-object v2, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p1, v2, :cond_1

    .line 560
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cancelTts()V

    .line 561
    iput-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 562
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v3, 0x7f0e0051

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    .line 563
    .local v1, "tag":Ljava/lang/Object;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v2, :cond_0

    .line 564
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->revertChoice()V

    .line 565
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getTimeout()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V

    .line 567
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    .line 568
    iput-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    .line 569
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    .line 570
    iput-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    .line 571
    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->initialReplyMode:Z

    .line 573
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 589
    .end local v1    # "tag":Ljava/lang/Object;
    :goto_0
    return-void

    .line 578
    .restart local v1    # "tag":Ljava/lang/Object;
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 581
    .end local v1    # "tag":Ljava/lang/Object;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-nez v2, :cond_3

    .line 582
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sendTts()V

    goto :goto_0

    .line 585
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 586
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getCannedReplyMessages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    sget v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator(III)V

    goto :goto_0

    .line 573
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onExpandedNotificationSwitched()V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sendTts()V

    .line 596
    :cond_0
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 707
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 712
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v6, :cond_1

    move v2, v5

    .line 794
    :cond_0
    :goto_0
    return v2

    .line 716
    :cond_1
    const/4 v2, 0x0

    .line 718
    .local v2, "handled":Z
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpanded()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 719
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 739
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    sget-object v7, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->REPLY:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    if-ne v6, v7, :cond_4

    .line 740
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v3

    .line 741
    .local v3, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedIndicatorCurrentItem()I

    move-result v1

    .line 743
    .local v1, "current":I
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    move v2, v5

    .line 759
    goto :goto_0

    .line 721
    .end local v1    # "current":I
    .end local v3    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :pswitch_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v6, 0x21

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/view/ObservableScrollView;->arrowScroll(I)Z

    move-result v2

    .line 722
    iget-boolean v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z

    if-eqz v5, :cond_0

    move v2, v4

    .line 724
    goto :goto_0

    .line 729
    :pswitch_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v6, 0x82

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/view/ObservableScrollView;->arrowScroll(I)Z

    move-result v2

    .line 730
    iget-boolean v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z

    if-eqz v5, :cond_0

    move v2, v4

    .line 732
    goto :goto_0

    .restart local v1    # "current":I
    .restart local v3    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :pswitch_2
    move v2, v4

    .line 745
    goto :goto_0

    :pswitch_3
    move v2, v4

    .line 748
    goto :goto_0

    .line 751
    :pswitch_4
    if-nez v1, :cond_3

    .line 752
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v6, v4, v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    :goto_1
    move v2, v5

    .line 756
    goto :goto_0

    .line 754
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getCannedReplyMessages()Ljava/util/List;

    move-result-object v4

    add-int/lit8 v6, v1, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-direct {p0, v4, v6, v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->reply(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 763
    .end local v1    # "current":I
    .end local v3    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_4
    if-nez v2, :cond_5

    .line 764
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    :cond_5
    move v2, v4

    .line 794
    goto/16 :goto_0

    .line 766
    :pswitch_5
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    move v2, v5

    .line 767
    goto/16 :goto_0

    .line 770
    :pswitch_6
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    move v2, v5

    .line 771
    goto/16 :goto_0

    .line 774
    :pswitch_7
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getCurrentSelectedChoice()Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    move-result-object v0

    .line 775
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    if-eqz v0, :cond_6

    .line 776
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_3

    .line 790
    :cond_6
    :goto_2
    :pswitch_8
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v2, v5

    .line 791
    goto/16 :goto_0

    .line 778
    :pswitch_9
    const-string v4, "Glance_Open_Full"

    const-string v6, "dial"

    invoke-static {v4, p0, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_2

    .line 781
    :pswitch_a
    const-string v4, "Glance_Open_Mini"

    const-string v6, "dial"

    invoke-static {v4, p0, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_2

    .line 784
    :pswitch_b
    const-string v4, "Glance_Dismiss"

    const-string v6, "dial"

    invoke-static {v4, p0, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_2

    .line 719
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 743
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 764
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 776
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 555
    return-void
.end method

.method public onPhotoDownload(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->photoCheckRequired:Z

    if-nez v0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne v0, v1, :cond_0

    .line 646
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->sourceIdentifier:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->sourceIdentifier:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    .line 647
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649
    :cond_2
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 652
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "photo available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->source:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto :goto_0
.end method

.method public onShowToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1334
    iget-object v0, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "message-sent-toast"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1335
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reply toast:animateOutExpandedView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1336
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 1338
    :cond_0
    return-void
.end method

.method public onSpeechRequestNotification(Lcom/navdy/service/library/events/audio/SpeechRequestStatus;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->audioFeedback:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    .line 1343
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    .line 1344
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    if-eqz v0, :cond_0

    .line 1346
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1356
    :cond_0
    :goto_0
    return-void

    .line 1348
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->startAudioAnimation()V

    goto :goto_0

    .line 1352
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->stopAudioAnimation(Z)V

    goto :goto_0

    .line 1346
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 6
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    const/4 v4, 0x0

    .line 478
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    .line 479
    .local v0, "expandedMode":Z
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start called:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " expanded:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 481
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 482
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 484
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v1, :cond_0

    .line 485
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 486
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v2, 0x7f0e0050

    invoke-virtual {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 487
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const v2, 0x7f0e0051

    invoke-virtual {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(ILjava/lang/Object;)V

    .line 489
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 490
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateState()V

    .line 491
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 498
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop called:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 499
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 501
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cancelTts()V

    .line 503
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clear()V

    .line 507
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/view/ObservableScrollView;->setScrollListener(Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;)V

    .line 511
    :cond_1
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 512
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 513
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->smallType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V

    .line 514
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceContainer:Landroid/view/ViewGroup;

    .line 517
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 518
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->largeType:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V

    .line 519
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;

    .line 522
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    .line 523
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyMsgView:Landroid/view/ViewGroup;

    .line 524
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->removeParent(Landroid/view/View;)V

    .line 525
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->replyExitView:Landroid/view/ViewGroup;

    .line 526
    iput-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->operationMode:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 527
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 703
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 494
    return-void
.end method

.method public setContactList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1431
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->contacts:Ljava/util/List;

    .line 1432
    return-void
.end method

.method public setListener(Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;)V
    .locals 0
    .param p1, "callback"    # Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    .prologue
    .line 1369
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    .line 1370
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 1095
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z

    return v0
.end method
