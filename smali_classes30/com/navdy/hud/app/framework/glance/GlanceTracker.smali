.class Lcom/navdy/hud/app/framework/glance/GlanceTracker;
.super Ljava/lang/Object;
.source "GlanceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    }
.end annotation


# static fields
.field private static final NOTIFICATION_SEEN_THRESHOLD:J

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private notificationSavedState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private notificationSeen:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 23
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->NOTIFICATION_SEEN_THRESHOLD:J

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    .line 41
    return-void
.end method

.method private isNotificationSame(Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Z
    .locals 6
    .param p1, "glanceInfo"    # Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    .param p2, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 116
    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v3

    iget-object v5, p1, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->data:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-eq v3, v5, :cond_0

    move v3, v4

    .line 134
    :goto_0
    return v3

    .line 120
    :cond_0
    iget-object v3, p1, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->data:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 121
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 122
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 123
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 124
    .local v2, "val":Ljava/lang/String;
    if-nez v2, :cond_2

    move v3, v4

    .line 126
    goto :goto_0

    .line 129
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    .line 130
    goto :goto_0

    .line 134
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "val":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized clearState()V
    .locals 2

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearState"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSavedState:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isNotificationSeen(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)J
    .locals 12
    .param p1, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .param p2, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-wide/16 v6, 0x0

    .line 44
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceTracker$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v8

    aget v5, v5, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v5, :pswitch_data_0

    .line 91
    :goto_0
    monitor-exit p0

    return-wide v6

    .line 52
    :pswitch_0
    :try_start_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    invoke-virtual {v5, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 53
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    if-nez v4, :cond_0

    .line 55
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-direct {v2, v8, v9, p1, p3}, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;-><init>(JLcom/navdy/service/library/events/glances/GlanceEvent;Ljava/util/Map;)V

    .line 57
    .local v2, "glanceInfo":Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v5, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    invoke-virtual {v5, p2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "first notification["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 44
    .end local v2    # "glanceInfo":Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 63
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    :cond_0
    :try_start_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 64
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;>;"
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 65
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;

    .line 66
    .restart local v2    # "glanceInfo":Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->time:J

    sub-long v0, v8, v10

    .line 67
    .local v0, "diff":J
    sget-wide v8, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->NOTIFICATION_SEEN_THRESHOLD:J

    cmp-long v5, v0, v8

    if-lez v5, :cond_2

    .line 69
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 70
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "expired notification removed ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 75
    :cond_2
    invoke-direct {p0, v2, p2, p3}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->isNotificationSame(Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    iget-wide v6, v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->time:J

    .line 77
    .local v6, "ret":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->time:J

    .line 78
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "notification has been seen ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] time ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 84
    .end local v0    # "diff":J
    .end local v2    # "glanceInfo":Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    .end local v6    # "ret":J
    :cond_3
    new-instance v2, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-direct {v2, v8, v9, p1, p3}, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;-><init>(JLcom/navdy/service/library/events/glances/GlanceEvent;Ljava/util/Map;)V

    .line 85
    .restart local v2    # "glanceInfo":Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "new notification ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method declared-synchronized restoreState()V
    .locals 2

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "restoreState"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSavedState:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSavedState:Ljava/util/HashMap;

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :cond_0
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized saveState()V
    .locals 2

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "saveState"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSavedState:Ljava/util/HashMap;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->notificationSeen:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
