.class public final enum Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
.super Ljava/lang/Enum;
.source "GlanceNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

.field public static final enum READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

.field public static final enum REPLY:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 83
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    const-string v1, "REPLY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->REPLY:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 84
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    const-string v1, "READ"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    .line 82
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->REPLY:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    const-class v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    return-object v0
.end method
