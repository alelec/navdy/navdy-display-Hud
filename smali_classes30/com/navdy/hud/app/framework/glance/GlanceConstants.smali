.class public Lcom/navdy/hud/app/framework/glance/GlanceConstants;
.super Ljava/lang/Object;
.source "GlanceConstants.java"


# static fields
.field public static final AT_SEPARATOR:Ljava/lang/String; = "@"

.field public static final AUDIO_FEEDBACK_FADE_OUT:I = 0x12c

.field public static final CALENDAR_PREFIX:Ljava/lang/String; = "glance_cal_"

.field public static final COLON_SEPARATOR:Ljava/lang/String; = ":"

.field public static final DEFAULT_TIMEOUT:I = 0x61a8

.field public static final DELETE_ALL_GLANCE_TIMEOUT:I = 0x3e8

.field public static final DELETE_ALL_VIEW_TAG:Ljava/lang/Object;

.field public static final EMAIL_PREFIX:Ljava/lang/String; = "glance_email_"

.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final FUEL_PREFIX:Ljava/lang/String; = "glance_fuel_"

.field public static final GENERIC_PREFIX:Ljava/lang/String; = "glance_gen_"

.field public static final GMAIL_SEPARATOR:Ljava/lang/String; = "\u2022"

.field public static final INVALID_APPLE_MAIL_GLANCE_BODY:Ljava/lang/String; = "this message has no content"

.field public static final MESSAGE_PREFIX:Ljava/lang/String; = "glance_msg_"

.field public static final NEWLINE:Ljava/lang/String; = "\n"

.field public static final PERIOD:Ljava/lang/String; = "."

.field public static final SCROLL_DISCARD_EVENTS_THRESHOLD:I = 0x2ee

.field public static final SCROLL_PROGRESS_INDICATOR_ITEM_COUNT:I = 0x64

.field public static final SECONDS_IN_MINUTE:I = 0xea60

.field public static final SOCIAL_PREFIX:Ljava/lang/String; = "glance_soc_"

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final TAG_ADD_FUEL:I = 0x7

.field public static final TAG_BACK:I = 0x5

.field public static final TAG_CALL:I = 0x3

.field public static final TAG_DISMISS:I = 0x6

.field public static final TAG_DISMISS_ADD_FUEL:I = 0x8

.field public static final TAG_QUICK:I = 0x4

.field public static final TAG_READ:I = 0x2

.field public static final TAG_REPLY:I = 0x1

.field public static final TIME_UPDATE_INTERVAL:I = 0x7530

.field public static final am:Ljava/lang/String;

.field public static final amMarker:Ljava/lang/String;

.field public static final back:Ljava/lang/String;

.field public static backChoice:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static backChoice2:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final calendarNormalMargin:I

.field public static final calendarNowMargin:I

.field public static calendarOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final calendarTimeMargin:I

.field public static final calendarpmMarker:I

.field public static final call:Ljava/lang/String;

.field public static final choiceItemDefaultPadding:I

.field public static final choiceItemLargePadding:I

.field public static final choiceItemSmallPadding:I

.field public static final choiceTextLarge:I

.field public static final choiceTextSmall:I

.field public static final colorAppleCalendar:I

.field public static final colorAppleMail:I

.field public static final colorDeleteAll:I

.field public static final colorFacebook:I

.field public static final colorFacebookMessenger:I

.field public static final colorFasterRoute:I

.field public static final colorFuelLevel:I

.field public static final colorGeneric:I

.field public static final colorGoogleCalendar:I

.field public static final colorGoogleHangout:I

.field public static final colorGoogleInbox:I

.field public static final colorGoogleMail:I

.field public static final colorIMessage:I

.field public static final colorPhoneCall:I

.field public static final colorSlack:I

.field public static final colorSms:I

.field public static final colorTrafficDelay:I

.field public static final colorTwitter:I

.field public static final colorWhatsapp:I

.field public static final colorWhite:I

.field public static final dismiss:Ljava/lang/String;

.field public static final done:Ljava/lang/String;

.field public static final facebook:Ljava/lang/String;

.field public static fuelChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static fuelChoicesNoRoute:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final fuelNotificationSubtitle:Ljava/lang/String;

.field public static final fuelNotificationTitle:Ljava/lang/String;

.field public static final glancesCannotDelete:Ljava/lang/String;

.field public static final glancesDismissAll:Ljava/lang/String;

.field public static final hour:Ljava/lang/String;

.field public static final message:Ljava/lang/String;

.field public static final messageHeightBound:I

.field private static messageTitleHeight:I

.field public static final messageTitleMarginBottom:I

.field public static final messageTitleMarginTop:I

.field public static final messageWidthBound:I

.field public static final minute:Ljava/lang/String;

.field public static noMessage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static noNumberandNoReplyBack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final notification:Ljava/lang/String;

.field public static final nowStr:Ljava/lang/String;

.field public static numberAndNoReplyBack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static numberAndReplyBack_1:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final pm:Ljava/lang/String;

.field public static final pmMarker:Ljava/lang/String;

.field public static final questionMark:Ljava/lang/String;

.field public static final quick:Ljava/lang/String;

.field public static final read:Ljava/lang/String;

.field public static final reply:Ljava/lang/String;

.field private static final replyBackMessages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final replySent:Ljava/lang/String;

.field public static final scrollingIndicatorCircleFocusSize:I

.field public static final scrollingIndicatorCircleSize:I

.field public static final scrollingIndicatorHeight:I

.field public static final scrollingIndicatorLeftPadding:I

.field public static final scrollingIndicatorPadding:I

.field public static final scrollingIndicatorParentHeight:I

.field public static final scrollingIndicatorParentWidth:I

.field public static final scrollingIndicatorProgressSize:I

.field public static final scrollingIndicatorSize:I

.field public static final scrollingIndicatorWidth:I

.field public static final scrollingRoundSize:I

.field public static final senderUnknown:Ljava/lang/String;

.field public static final starts:Ljava/lang/String;

.field public static final view:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 25

    .prologue
    .line 34
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->DELETE_ALL_VIEW_TAG:Ljava/lang/Object;

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarOptions:Ljava/util/List;

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noMessage:Ljava/util/List;

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice:Ljava/util/List;

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    .line 137
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoices:Ljava/util/List;

    .line 138
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoicesNoRoute:Ljava/util/List;

    .line 162
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 164
    .local v24, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09021e

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->reply:Ljava/lang/String;

    .line 165
    const v1, 0x7f09021b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    .line 166
    const v1, 0x7f09003a

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->call:Ljava/lang/String;

    .line 167
    const v1, 0x7f090216

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->quick:Ljava/lang/String;

    .line 168
    const v1, 0x7f09002d

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->back:Ljava/lang/String;

    .line 169
    const v1, 0x7f0900dc

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    .line 170
    const v1, 0x7f0900df

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->done:Ljava/lang/String;

    .line 171
    const v1, 0x7f0902ea

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->view:Ljava/lang/String;

    .line 172
    const v1, 0x7f09004b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->senderUnknown:Ljava/lang/String;

    .line 173
    const v1, 0x7f090220

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replySent:Ljava/lang/String;

    .line 174
    const v1, 0x7f0901dc

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->notification:Ljava/lang/String;

    .line 175
    const v1, 0x7f0900fe

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->facebook:Ljava/lang/String;

    .line 176
    const v1, 0x7f0901e3

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->nowStr:Ljava/lang/String;

    .line 177
    const v1, 0x7f090114

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->glancesCannotDelete:Ljava/lang/String;

    .line 178
    const v1, 0x7f090118

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->glancesDismissAll:Ljava/lang/String;

    .line 179
    const v1, 0x7f09027c

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->starts:Ljava/lang/String;

    .line 180
    const v1, 0x7f09017e

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->hour:Ljava/lang/String;

    .line 181
    const v1, 0x7f0901ab

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->minute:Ljava/lang/String;

    .line 182
    const v1, 0x7f0901a8

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->message:Ljava/lang/String;

    .line 183
    const v1, 0x7f090107

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelNotificationTitle:Ljava/lang/String;

    .line 184
    const v1, 0x7f090011

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelNotificationSubtitle:Ljava/lang/String;

    .line 185
    const v1, 0x7f090215

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->questionMark:Ljava/lang/String;

    .line 186
    const v1, 0x7f090016

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->am:Ljava/lang/String;

    .line 187
    const v1, 0x7f09020a

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->pm:Ljava/lang/String;

    .line 188
    const v1, 0x7f090017

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->amMarker:Ljava/lang/String;

    .line 189
    const v1, 0x7f09020b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->pmMarker:Ljava/lang/String;

    .line 191
    const v1, 0x7f0d0029

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleCalendar:I

    .line 192
    const v1, 0x7f0d002c

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleMail:I

    .line 193
    const v1, 0x7f0d002a

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleHangout:I

    .line 194
    const v1, 0x7f0d00a3

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorSlack:I

    .line 195
    const v1, 0x7f0d00c0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhatsapp:I

    .line 196
    const v1, 0x7f0d0017

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFacebookMessenger:I

    .line 197
    const v1, 0x7f0d0016

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFacebook:I

    .line 198
    const v1, 0x7f0d00bc

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorTwitter:I

    .line 199
    const v1, 0x7f0d0048

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorIMessage:I

    .line 200
    const v1, 0x7f0d0002

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleMail:I

    .line 201
    const v1, 0x7f0d0001

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleCalendar:I

    .line 202
    const v1, 0x7f0d00a5

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorSms:I

    .line 203
    const v1, 0x7f0d001d

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGeneric:I

    .line 204
    const v1, 0x7f0d0010

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorDeleteAll:I

    .line 205
    const v1, 0x7f0d0096

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorPhoneCall:I

    .line 206
    const v1, 0x7f0d0018

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFasterRoute:I

    .line 207
    const v1, 0x7f0d001c

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFuelLevel:I

    .line 208
    const v1, 0x7f0d00b7

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorTrafficDelay:I

    .line 209
    const v1, 0x106000b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    .line 210
    const v1, 0x7f0d002b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleInbox:I

    .line 212
    const v1, 0x7f0d0020

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    .line 213
    .local v20, "backColor":I
    const/high16 v6, -0x1000000

    .line 214
    .local v6, "unselectedColor":I
    const v1, 0x7f0d0021

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 215
    .local v22, "dismissColor":I
    const v1, 0x7f0d0024

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 216
    .local v10, "readColor":I
    const v1, 0x7f0d0024

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 217
    .local v4, "replyColor":I
    const v1, 0x7f0d0006

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    .line 218
    .local v21, "callColor":I
    const v1, 0x7f0d0023

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    .line 219
    .local v23, "quickColor":I
    const v1, 0x7f0d0024

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    .line 221
    .local v19, "addColor":I
    sget-object v9, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v2, 0x1

    const v3, 0x7f02012b

    const v5, 0x7f02012b

    sget-object v7, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->reply:Ljava/lang/String;

    move v8, v4

    invoke-direct/range {v1 .. v8}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndReplyBack_1:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x6

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x3

    const v13, 0x7f0200ef

    const v15, 0x7f0200ef

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->call:Ljava/lang/String;

    move/from16 v14, v21

    move/from16 v16, v6

    move/from16 v18, v21

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->numberAndNoReplyBack:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x6

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noNumberandNoReplyBack:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x6

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarOptions:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarOptions:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x6

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->noMessage:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x6

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice:Ljava/util/List;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v3, 0x7f09002d

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x5

    invoke-direct {v2, v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x5

    const v13, 0x7f020123

    const v15, 0x7f020123

    const v2, 0x7f09002d

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move/from16 v14, v20

    move/from16 v16, v20

    move/from16 v18, v20

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoices:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v12, 0x7

    const v13, 0x7f020122

    const v15, 0x7f020122

    const v2, 0x7f090010

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move/from16 v14, v19

    move/from16 v16, v6

    move/from16 v18, v19

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoices:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoices:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v12, 0x8

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoicesNoRoute:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v8, 0x2

    const v9, 0x7f02012a

    const v11, 0x7f02012a

    sget-object v13, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelChoicesNoRoute:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v12, 0x8

    const v13, 0x7f020125

    const v15, 0x7f020125

    sget-object v17, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->dismiss:Ljava/lang/String;

    move/from16 v14, v22

    move/from16 v16, v6

    move/from16 v18, v22

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    const v1, 0x7f0b00d3

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->choiceTextLarge:I

    .line 249
    const v1, 0x7f0b00d4

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->choiceTextSmall:I

    .line 250
    const v1, 0x7f0b00d1

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->choiceItemLargePadding:I

    .line 251
    const v1, 0x7f0b00d0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->choiceItemDefaultPadding:I

    .line 252
    const v1, 0x7f0b00d2

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->choiceItemSmallPadding:I

    .line 254
    const v1, 0x7f0b009b

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNormalMargin:I

    .line 255
    const v1, 0x7f0b009c

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNowMargin:I

    .line 256
    const v1, 0x7f0b009e

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarTimeMargin:I

    .line 257
    const v1, 0x7f0b009d

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarpmMarker:I

    .line 258
    const v1, 0x7f0b00a2

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageWidthBound:I

    .line 259
    const v1, 0x7f0b009f

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageHeightBound:I

    .line 260
    const v1, 0x7f0b00a1

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleMarginTop:I

    .line 261
    const v1, 0x7f0b00a0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleMarginBottom:I

    .line 262
    const v1, 0x7f0b00ac

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorSize:I

    .line 263
    const v1, 0x7f0b00ae

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingRoundSize:I

    .line 264
    const v1, 0x7f0b00ad

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorWidth:I

    .line 265
    const v1, 0x7f0b00a7

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorHeight:I

    .line 266
    const v1, 0x7f0b00ab

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorParentWidth:I

    .line 267
    const v1, 0x7f0b00aa

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorParentHeight:I

    .line 268
    const v1, 0x7f0b00ac

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorProgressSize:I

    .line 269
    const v1, 0x7f0b00a4

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorCircleSize:I

    .line 270
    const v1, 0x7f0b00a6

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorCircleFocusSize:I

    .line 271
    const v1, 0x7f0b00a9

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorPadding:I

    .line 272
    const v1, 0x7f0b00a8

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorLeftPadding:I

    .line 274
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    .line 275
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090221

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090222

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090223

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090224

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090225

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    const v2, 0x7f090226

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static areMessageCanned()Z
    .locals 2

    .prologue
    .line 305
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getCannedMessages()Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    move-result-object v0

    .line 306
    .local v0, "cannedMessagesUpdate":Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->cannedMessage:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->cannedMessage:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 307
    const/4 v1, 0x0

    .line 309
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static getCannedMessages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getCannedMessages()Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    move-result-object v0

    .line 293
    .local v0, "cannedMessagesUpdate":Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->cannedMessage:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->cannedMessage:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 294
    iget-object v1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->cannedMessage:Ljava/util/List;

    .line 296
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->replyBackMessages:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public static getMessageTitleHeight()I
    .locals 1

    .prologue
    .line 284
    sget v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleHeight:I

    return v0
.end method

.method public static setMessageTitleHeight(I)V
    .locals 0
    .param p0, "n"    # I

    .prologue
    .line 288
    sput p0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleHeight:I

    .line 289
    return-void
.end method
