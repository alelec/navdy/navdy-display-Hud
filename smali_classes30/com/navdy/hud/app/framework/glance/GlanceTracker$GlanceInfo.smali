.class Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;
.super Ljava/lang/Object;
.source "GlanceTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GlanceInfo"
.end annotation


# instance fields
.field data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field event:Lcom/navdy/service/library/events/glances/GlanceEvent;

.field time:J


# direct methods
.method constructor <init>(JLcom/navdy/service/library/events/glances/GlanceEvent;Ljava/util/Map;)V
    .locals 1
    .param p1, "time"    # J
    .param p3, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p4, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->time:J

    .line 28
    iput-object p3, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->event:Lcom/navdy/service/library/events/glances/GlanceEvent;

    .line 29
    iput-object p4, p0, Lcom/navdy/hud/app/framework/glance/GlanceTracker$GlanceInfo;->data:Ljava/util/Map;

    .line 30
    return-void
.end method
