.class public Lcom/navdy/hud/app/framework/glance/GlanceHandler;
.super Ljava/lang/Object;
.source "GlanceHandler.java"


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final dateFormat1:Ljava/text/SimpleDateFormat;

.field private static final dateFormat2:Ljava/text/SimpleDateFormat;

.field private static final dateFormat3:Ljava/text/SimpleDateFormat;

.field private static final dateFormat4:Ljava/text/SimpleDateFormat;

.field private static final dateFormat5:Ljava/text/SimpleDateFormat;

.field private static final lockObj:Ljava/lang/Object;

.field private static final sInstance:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

.field private messagesSent:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sInstance:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->lockObj:Ljava/lang/Object;

    .line 64
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat1:Ljava/text/SimpleDateFormat;

    .line 65
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat2:Ljava/text/SimpleDateFormat;

    .line 66
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d, hh:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat3:Ljava/text/SimpleDateFormat;

    .line 67
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat4:Ljava/text/SimpleDateFormat;

    .line 68
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat5:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->bus:Lcom/squareup/otto/Bus;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/glance/GlanceHandler;Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glance/GlanceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->handleGlancEventInternal(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    return-void
.end method

.method private clearAllMessage()V
    .locals 2

    .prologue
    .line 527
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    monitor-enter v1

    .line 528
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 529
    monitor-exit v1

    .line 530
    return-void

    .line 529
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private extractTime(Ljava/lang/String;)J
    .locals 12
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 371
    const/4 v5, 0x0

    .line 372
    .local v5, "firstPart":Ljava/lang/String;
    const/4 v7, 0x0

    .line 373
    .local v7, "secondPart":Ljava/lang/String;
    const-string v8, "\u2013"

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 374
    .local v6, "index":I
    const/4 v8, -0x1

    if-eq v6, v8, :cond_0

    .line 375
    add-int/lit8 v8, v6, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 376
    invoke-virtual {p1, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 377
    invoke-static {v7}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->sanitizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 378
    invoke-static {v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->sanitizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 384
    :goto_0
    sget-object v10, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->lockObj:Ljava/lang/Object;

    monitor-enter v10

    .line 385
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 387
    .local v2, "date":Ljava/util/Calendar;
    :try_start_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 388
    .local v1, "d":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat1:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 389
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 390
    const/16 v8, 0xc

    const/16 v9, 0xc

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 391
    const/16 v8, 0xd

    const/16 v9, 0xd

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 392
    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 393
    const/16 v8, 0x9

    const/16 v9, 0x9

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 395
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 396
    .local v4, "eventDate":Ljava/util/Date;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "event-time-date = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 397
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v8

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 482
    .end local v1    # "d":Ljava/util/Calendar;
    .end local v4    # "eventDate":Ljava/util/Date;
    :goto_1
    return-wide v8

    .line 380
    .end local v2    # "date":Ljava/util/Calendar;
    :cond_0
    invoke-static {p1}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->sanitizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 398
    .restart local v2    # "date":Ljava/util/Calendar;
    :catch_0
    move-exception v8

    .line 402
    :try_start_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 403
    .restart local v1    # "d":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat2:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 404
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 405
    const/16 v8, 0xc

    const/16 v9, 0xc

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 406
    const/16 v8, 0xd

    const/16 v9, 0xd

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 407
    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 410
    if-eqz v7, :cond_1

    .line 412
    :try_start_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 413
    .local v0, "cal2":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat1:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 414
    .local v3, "date2":Ljava/util/Date;
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 415
    const/16 v8, 0x9

    const/16 v9, 0x9

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 420
    .end local v0    # "cal2":Ljava/util/Calendar;
    .end local v3    # "date2":Ljava/util/Date;
    :cond_1
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 421
    .restart local v4    # "eventDate":Ljava/util/Date;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "event-time-date = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 422
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-wide v8

    :try_start_6
    monitor-exit v10

    goto :goto_1

    .line 480
    .end local v1    # "d":Ljava/util/Calendar;
    .end local v2    # "date":Ljava/util/Calendar;
    .end local v4    # "eventDate":Ljava/util/Date;
    :catchall_0
    move-exception v8

    monitor-exit v10
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v8

    .line 423
    .restart local v2    # "date":Ljava/util/Calendar;
    :catch_1
    move-exception v8

    .line 427
    :try_start_7
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 428
    .restart local v1    # "d":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat3:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 429
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 430
    const/16 v8, 0xc

    const/16 v9, 0xc

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 431
    const/16 v8, 0xd

    const/16 v9, 0xd

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 432
    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 433
    const/16 v8, 0x9

    const/16 v9, 0x9

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 435
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 436
    .restart local v4    # "eventDate":Ljava/util/Date;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "event-time-date = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-wide v8

    :try_start_8
    monitor-exit v10
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 438
    .end local v1    # "d":Ljava/util/Calendar;
    .end local v4    # "eventDate":Ljava/util/Date;
    :catch_2
    move-exception v8

    .line 442
    :try_start_9
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 443
    .restart local v1    # "d":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat4:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 444
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 445
    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 446
    const/16 v8, 0xd

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 447
    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 450
    if-eqz v7, :cond_2

    .line 452
    :try_start_a
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 453
    .restart local v0    # "cal2":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat5:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 454
    .restart local v3    # "date2":Ljava/util/Date;
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 455
    const/16 v8, 0x9

    const/16 v9, 0x9

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 460
    .end local v0    # "cal2":Ljava/util/Calendar;
    .end local v3    # "date2":Ljava/util/Date;
    :cond_2
    :goto_3
    :try_start_b
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 461
    .restart local v4    # "eventDate":Ljava/util/Date;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "event-time-date = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 462
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-wide v8

    :try_start_c
    monitor-exit v10
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_1

    .line 463
    .end local v1    # "d":Ljava/util/Calendar;
    .end local v4    # "eventDate":Ljava/util/Date;
    :catch_3
    move-exception v8

    .line 467
    :try_start_d
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 468
    .restart local v1    # "d":Ljava/util/Calendar;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->dateFormat5:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 469
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 470
    const/16 v8, 0xc

    const/16 v9, 0xc

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 471
    const/16 v8, 0xd

    const/16 v9, 0xd

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 472
    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 473
    const/16 v8, 0x9

    const/16 v9, 0x9

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 475
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 476
    .restart local v4    # "eventDate":Ljava/util/Date;
    sget-object v8, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "event-time-date = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 477
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-wide v8

    :try_start_e
    monitor-exit v10

    goto/16 :goto_1

    .line 478
    .end local v1    # "d":Ljava/util/Calendar;
    .end local v4    # "eventDate":Ljava/util/Date;
    :catch_4
    move-exception v8

    .line 480
    monitor-exit v10
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 482
    const-wide/16 v8, 0x0

    goto/16 :goto_1

    .line 456
    .restart local v1    # "d":Ljava/util/Calendar;
    :catch_5
    move-exception v8

    goto/16 :goto_3

    .line 416
    :catch_6
    move-exception v8

    goto/16 :goto_2
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sInstance:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    return-object v0
.end method

.method private handleGlancEventInternal(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 38
    .param p1, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 96
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->printGlance(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 98
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 99
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    if-eqz v6, :cond_0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-eqz v6, :cond_0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 101
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 102
    :cond_0
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "invalid glance event"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 289
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getGlancesApp(Lcom/navdy/service/library/events/glances/GlanceEvent;)Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-result-object v19

    .line 107
    .local v19, "glanceApp":Lcom/navdy/hud/app/framework/glance/GlanceApp;
    if-nez v19, :cond_2

    .line 108
    sget-object v19, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 111
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->buildDataMap(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/util/Map;

    move-result-object v17

    .line 113
    .local v17, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v16, 0x0

    .line 115
    .local v16, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler$3;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 229
    :cond_3
    :goto_1
    invoke-static/range {v19 .. v19}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->usesMessageLayout(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v37

    .line 233
    .local v37, "usesMessageLayout":Z
    if-eqz v37, :cond_c

    .line 235
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getGlanceMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v36

    .line 236
    .local v36, "text":Ljava/lang/String;
    invoke-static/range {v36 .. v36}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->needsScrollLayout(Ljava/lang/String;)Z

    move-result v28

    .line 238
    .local v28, "needsScroll":Z
    if-eqz v28, :cond_b

    .line 239
    new-instance v20, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Ljava/util/Map;)V

    .line 243
    .local v20, "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "posted glance ["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "] scroll["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 274
    .end local v28    # "needsScroll":Z
    .end local v36    # "text":Ljava/lang/String;
    .local v24, "logMsg":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v6, v0, v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->isNotificationSeen(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)J

    move-result-wide v32

    .line 275
    .local v32, "seenAt":J
    const-wide/16 v8, 0x0

    cmp-long v6, v32, v8

    if-lez v6, :cond_11

    .line 276
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance already seen ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] at ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, v32

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] now ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    .end local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .end local v24    # "logMsg":Ljava/lang/String;
    .end local v32    # "seenAt":J
    .end local v37    # "usesMessageLayout":Z
    :pswitch_0
    const/16 v21, 0x0

    .line 121
    .local v21, "hasNumber":Z
    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 122
    .local v25, "message":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 123
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance message does not exist:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_4
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getNumber(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v7

    .line 127
    .local v7, "id":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getFrom(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    .line 128
    .local v5, "source":Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 129
    const/16 v21, 0x1

    .line 130
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "hasNumber:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    if-nez v5, :cond_5

    .line 132
    move-object v5, v7

    .line 138
    :cond_5
    :goto_4
    if-eqz v7, :cond_3

    .line 139
    new-instance v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    sget-object v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    sget-object v8, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    sget-object v10, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const/4 v11, -0x1

    const-wide/16 v12, 0x0

    invoke-direct/range {v4 .. v13}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V

    .line 148
    .local v4, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v30

    .line 149
    .local v30, "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->handleNewCall(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)Z

    move-result v31

    .line 150
    .local v31, "ret":Z
    if-nez v31, :cond_6

    .line 151
    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getContactsFromId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    .line 152
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "recent call contact list found ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    :cond_6
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "recent call id["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 156
    if-nez v21, :cond_3

    .line 157
    if-eqz v31, :cond_a

    .line 158
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "got Number:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 159
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v29, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 162
    .local v22, "k":Lcom/navdy/service/library/events/glances/KeyValue;
    move-object/from16 v0, v22

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    sget-object v9, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 165
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 135
    .end local v4    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v22    # "k":Lcom/navdy/service/library/events/glances/KeyValue;
    .end local v29    # "newData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v30    # "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    .end local v31    # "ret":Z
    :cond_8
    move-object v7, v5

    .line 136
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 167
    .restart local v4    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .restart local v29    # "newData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .restart local v30    # "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    .restart local v31    # "ret":Z
    :cond_9
    new-instance v6, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-direct {v6, v8, v9}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    new-instance v6, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    .line 170
    invoke-virtual {v6, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 171
    invoke-virtual {v6, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    .line 172
    invoke-virtual {v6, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 173
    invoke-virtual {v6, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 174
    invoke-virtual {v6, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 175
    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object p1

    .line 177
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->buildDataMap(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/util/Map;

    move-result-object v17

    .line 178
    goto/16 :goto_1

    .line 179
    .end local v29    # "newData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    :cond_a
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "no Number yet:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 189
    .end local v4    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v5    # "source":Ljava/lang/String;
    .end local v7    # "id":Ljava/lang/String;
    .end local v21    # "hasNumber":Z
    .end local v25    # "message":Ljava/lang/String;
    .end local v30    # "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    .end local v31    # "ret":Z
    :pswitch_1
    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 190
    .restart local v25    # "message":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 191
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance message does not exist:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    .end local v25    # "message":Ljava/lang/String;
    :pswitch_2
    sget-object v6, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 201
    .restart local v25    # "message":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 202
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance message does not exist:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    .end local v25    # "message":Ljava/lang/String;
    :pswitch_3
    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 210
    .restart local v25    # "message":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 211
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance message does not exist:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 220
    .end local v25    # "message":Ljava/lang/String;
    :pswitch_4
    sget-object v6, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 221
    .local v14, "body":Ljava/lang/String;
    sget-object v6, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 222
    .local v34, "subject":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 223
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "glance message does not exist:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 241
    .end local v14    # "body":Ljava/lang/String;
    .end local v34    # "subject":Ljava/lang/String;
    .restart local v28    # "needsScroll":Z
    .restart local v36    # "text":Ljava/lang/String;
    .restart local v37    # "usesMessageLayout":Z
    :cond_b
    new-instance v20, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)V

    .restart local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    goto/16 :goto_2

    .line 244
    .end local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .end local v28    # "needsScroll":Z
    .end local v36    # "text":Ljava/lang/String;
    :cond_c
    invoke-static/range {v19 .. v19}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isCalendarApp(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 245
    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 246
    .local v18, "eventTime":Ljava/lang/String;
    const-wide/16 v26, 0x0

    .line 247
    .local v26, "millis":J
    if-eqz v18, :cond_d

    .line 249
    :try_start_0
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v26

    .line 254
    :cond_d
    :goto_6
    const-wide/16 v8, 0x0

    cmp-long v6, v26, v8

    if-nez v6, :cond_f

    .line 256
    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "eventTime":Ljava/lang/String;
    check-cast v18, Ljava/lang/String;

    .line 257
    .restart local v18    # "eventTime":Ljava/lang/String;
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "event-time-str ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 258
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 259
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->extractTime(Ljava/lang/String;)J

    move-result-wide v26

    .line 262
    :cond_e
    const-wide/16 v8, 0x0

    cmp-long v6, v26, v8

    if-eqz v6, :cond_f

    .line 263
    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v17

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    :cond_f
    new-instance v20, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)V

    .line 267
    .restart local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "posted glance ["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "] time["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 268
    .restart local v24    # "logMsg":Ljava/lang/String;
    goto/16 :goto_3

    .line 250
    .end local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    .end local v24    # "logMsg":Ljava/lang/String;
    :catch_0
    move-exception v35

    .line 251
    .local v35, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v35

    invoke-virtual {v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 269
    .end local v18    # "eventTime":Ljava/lang/String;
    .end local v26    # "millis":J
    .end local v35    # "t":Ljava/lang/Throwable;
    :cond_10
    new-instance v20, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent;Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)V

    .line 270
    .restart local v20    # "glanceNotification":Lcom/navdy/hud/app/framework/glance/GlanceNotification;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "posted glance ["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .restart local v24    # "logMsg":Ljava/lang/String;
    goto/16 :goto_3

    .line 280
    .restart local v32    # "seenAt":J
    :cond_11
    if-eqz v16, :cond_13

    .line 281
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 282
    .local v23, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/navdy/service/library/events/contacts/Contact;

    .line 283
    .local v15, "c":Lcom/navdy/service/library/events/contacts/Contact;
    new-instance v8, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v8, v15}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/Contact;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 285
    .end local v15    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    :cond_12
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setContactList(Ljava/util/List;)V

    .line 287
    .end local v23    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    :cond_13
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 288
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private static printGlance(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 6
    .param p0, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    const/4 v3, 0x0

    .line 292
    if-nez p0, :cond_1

    .line 293
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "null glance event"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 314
    :cond_0
    return-void

    .line 297
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[glance-event] id["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "] type["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "] provider["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "] data-size ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-nez v2, :cond_2

    move v2, v3

    .line 300
    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "] action-size ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-nez v5, :cond_3

    .line 301
    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 297
    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 303
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 304
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 305
    .local v1, "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[glance-event] data key["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] val["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 297
    .end local v1    # "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    :cond_2
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 300
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 301
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_1

    .line 309
    :cond_4
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 310
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    .line 311
    .local v0, "action":Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[glance-event] action["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public addMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 509
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    monitor-enter v1

    .line 513
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearState()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->clearState()V

    .line 487
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->clearAllMessage()V

    .line 488
    return-void
.end method

.method public onGlanceEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 83
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/glance/GlanceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceHandler$1;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceHandler;Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 93
    return-void
.end method

.method public onMessage(Lcom/navdy/service/library/events/messaging/SmsMessageResponse;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/service/library/events/messaging/SmsMessageResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 320
    :try_start_0
    iget-object v2, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v2, v3, :cond_0

    .line 321
    const/4 v2, 0x1

    const-string v3, "Android"

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->areMessageCanned()Z

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSmsSent(ZLjava/lang/String;Z)V

    .line 335
    :goto_0
    return-void

    .line 325
    :cond_0
    const/4 v2, 0x0

    const-string v3, "Android"

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->areMessageCanned()Z

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSmsSent(ZLjava/lang/String;Z)V

    .line 326
    iget-object v2, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->id:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->removeMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "msg":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 328
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->number:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v4}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 332
    .end local v0    # "msg":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 333
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 330
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sms message with id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public removeMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 518
    if-nez p1, :cond_0

    .line 519
    const/4 v0, 0x0

    .line 522
    :goto_0
    return-object v0

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    monitor-enter v1

    .line 522
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->messagesSent:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public restoreState()V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->restoreState()V

    .line 496
    return-void
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->glanceTracker:Lcom/navdy/hud/app/framework/glance/GlanceTracker;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/glance/GlanceTracker;->saveState()V

    .line 492
    return-void
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 339
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isClientiOS()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 340
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->getInstance()Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;

    invoke-direct {v7, p0, p1, p2, p3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;-><init>(Lcom/navdy/hud/app/framework/glance/GlanceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, p1, p2, v7}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendSms(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    move-result-object v0

    .line 348
    .local v0, "errorCode":Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    sget-object v6, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    if-ne v0, v6, :cond_0

    .line 349
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "sms-twilio queued"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 365
    .end local v0    # "errorCode":Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    :goto_0
    return v4

    .line 352
    .restart local v0    # "errorCode":Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    :cond_0
    const/4 v4, 0x0

    const-string v6, "iOS"

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->areMessageCanned()Z

    move-result v7

    invoke-static {v4, v6, v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSmsSent(ZLjava/lang/String;Z)V

    move v4, v5

    .line 353
    goto :goto_0

    .line 356
    .end local v0    # "errorCode":Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    :cond_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "msgId":Ljava/lang/String;
    invoke-virtual {p0, v1, p2}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->addMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    new-instance v2, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    invoke-direct {v2, p1, p2, p3, v1}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    .local v2, "sms":Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v7, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 360
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "sms sent"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    .end local v1    # "msgId":Ljava/lang/String;
    .end local v2    # "sms":Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
    :catch_0
    move-exception v3

    .line 364
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    move v4, v5

    .line 365
    goto :goto_0
.end method

.method public sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 499
    new-instance v0, Lcom/navdy/hud/app/framework/message/SmsNotification;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;->Failed:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/message/SmsNotification;-><init>(Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    .local v0, "smsNotification":Lcom/navdy/hud/app/framework/message/SmsNotification;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 501
    return-void
.end method

.method public sendSmsSuccessNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 504
    new-instance v0, Lcom/navdy/hud/app/framework/message/SmsNotification;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;->Success:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/message/SmsNotification;-><init>(Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    .local v0, "smsNotification":Lcom/navdy/hud/app/framework/message/SmsNotification;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 506
    return-void
.end method
