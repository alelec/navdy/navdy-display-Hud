.class public final enum Lcom/navdy/hud/app/framework/glance/GlanceApp;
.super Ljava/lang/Enum;
.source "GlanceApp.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum APPLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum APPLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum FUEL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GENERIC_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GENERIC_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GENERIC_SOCIAL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GOOGLE_INBOX:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum IMESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum SMS:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

.field public static final enum WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;


# instance fields
.field color:I

.field defaultPhotoBasedOnId:Z

.field mainIcon:I

.field notificationType:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field sideIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 11
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "FUEL"

    const/4 v2, 0x0

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFuelLevel:I

    const v4, 0x7f0200f4

    const v5, 0x7f020118

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->LOW_FUEL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FUEL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GOOGLE_CALENDAR"

    const/4 v2, 0x1

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleCalendar:I

    const v4, 0x7f02011b

    const/4 v5, -0x1

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GOOGLE_MAIL"

    const/4 v2, 0x2

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleMail:I

    const v4, 0x7f02011a

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GOOGLE_HANGOUT"

    const/4 v2, 0x3

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleHangout:I

    const v4, 0x7f02011d

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 38
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "SLACK"

    const/4 v2, 0x4

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorSlack:I

    const v4, 0x7f02011f

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 45
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "WHATS_APP"

    const/4 v2, 0x5

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhatsapp:I

    const v4, 0x7f020121

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "FACEBOOK_MESSENGER"

    const/4 v2, 0x6

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFacebookMessenger:I

    const v4, 0x7f020117

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 59
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "FACEBOOK"

    const/4 v2, 0x7

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFacebook:I

    const v4, 0x7f020116

    const v5, 0x7f02011e

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 66
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "TWITTER"

    const/16 v2, 0x8

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorTwitter:I

    const v4, 0x7f020120

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 73
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "IMESSAGE"

    const/16 v2, 0x9

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorIMessage:I

    const v4, 0x7f02016d

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->IMESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 80
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "APPLE_CALENDAR"

    const/16 v2, 0xa

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleCalendar:I

    const v4, 0x7f020113

    const/4 v5, -0x1

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 87
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "APPLE_MAIL"

    const/16 v2, 0xb

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleMail:I

    const v4, 0x7f020115

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 94
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "SMS"

    const/16 v2, 0xc

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorSms:I

    const v4, 0x7f02016d

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SMS:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 101
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GENERIC"

    const/16 v2, 0xd

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGeneric:I

    const v4, 0x7f020119

    const v5, 0x7f02011e

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 108
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GOOGLE_INBOX"

    const/16 v2, 0xe

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorGoogleInbox:I

    const v4, 0x7f02011c

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_INBOX:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 115
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GENERIC_MAIL"

    const/16 v2, 0xf

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleMail:I

    const v4, 0x7f020115

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 122
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GENERIC_CALENDAR"

    const/16 v2, 0x10

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorAppleCalendar:I

    const v4, 0x7f020113

    const/4 v5, -0x1

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 129
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GENERIC_MESSAGE"

    const/16 v2, 0x11

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorSms:I

    const v4, 0x7f02016d

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 136
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const-string v1, "GENERIC_SOCIAL"

    const/16 v2, 0x12

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorTwitter:I

    const v4, 0x7f0201eb

    const/4 v5, -0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;-><init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_SOCIAL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 9
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/navdy/hud/app/framework/glance/GlanceApp;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FUEL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->IMESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SMS:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_INBOX:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_SOCIAL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZLcom/navdy/hud/app/framework/notifications/NotificationType;)V
    .locals 0
    .param p3, "color"    # I
    .param p4, "sideIcon"    # I
    .param p5, "mainIcon"    # I
    .param p6, "defaultPhotoBasedOnId"    # Z
    .param p7, "notificationType"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ",
            "Lcom/navdy/hud/app/framework/notifications/NotificationType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 144
    iput p3, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->color:I

    .line 145
    iput p4, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->sideIcon:I

    .line 146
    iput p5, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->mainIcon:I

    .line 147
    iput-boolean p6, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->defaultPhotoBasedOnId:Z

    .line 148
    iput-object p7, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->notificationType:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 149
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/glance/GlanceApp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-object v0
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->color:I

    return v0
.end method

.method public getMainIcon()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->mainIcon:I

    return v0
.end method

.method public getSideIcon()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->sideIcon:I

    return v0
.end method

.method public isDefaultIconBasedOnId()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->defaultPhotoBasedOnId:Z

    return v0
.end method
