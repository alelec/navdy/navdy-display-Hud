.class Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;
.super Ljava/lang/Object;
.source "GlanceNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    const/4 v4, 0x0

    .line 231
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v2

    if-nez v2, :cond_0

    .line 286
    :goto_0
    return-void

    .line 237
    :cond_0
    iget v2, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 239
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToReplyScreen()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1300(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    goto :goto_0

    .line 243
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->READ:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    const/4 v4, 0x0

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1400(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    goto :goto_0

    .line 251
    :pswitch_3
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->call()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1600(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    goto :goto_0

    .line 255
    :pswitch_4
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;->REPLY:Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->number:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1700(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->switchToMode(Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1400(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Lcom/navdy/hud/app/framework/glance/GlanceNotification$Mode;Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :pswitch_5
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1800(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-result-object v2

    const v3, 0x7f0e0051

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    .line 260
    .local v1, "tag":Ljava/lang/Object;
    if-eqz v1, :cond_1

    .line 261
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v2

    invoke-interface {v2, v4, v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    goto :goto_0

    .line 263
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->revertChoice()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1900(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    .line 264
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getTimeout()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V

    goto :goto_0

    .line 269
    .end local v1    # "tag":Ljava/lang/Object;
    :pswitch_6
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v0

    .line 271
    .local v0, "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->routeToGasStation()V

    .line 274
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    goto :goto_0

    .line 278
    .end local v0    # "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    :pswitch_7
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v0

    .line 280
    .restart local v0    # "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    if-eqz v0, :cond_3

    .line 281
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->dismissGasRoute()V

    .line 283
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 293
    :cond_0
    return-void
.end method
