.class public final enum Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
.super Ljava/lang/Enum;
.source "GlanceViewCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceViewCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

.field public static final enum SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;


# instance fields
.field final cacheSize:I

.field final layoutId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 24
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "SMALL_GLANCE_MESSAGE"

    const v2, 0x7f030028

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "BIG_GLANCE_MESSAGE"

    const v2, 0x7f030022

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "BIG_GLANCE_MESSAGE_SINGLE"

    const v2, 0x7f030023

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "SMALL_IMAGE"

    const v2, 0x7f030027

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "BIG_TEXT"

    const v2, 0x7f030025

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 29
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "BIG_MULTI_TEXT"

    const/4 v2, 0x5

    const v3, 0x7f030024

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "SMALL_SIGN"

    const/4 v2, 0x6

    const v3, 0x7f030029

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    const-string v1, "BIG_CALENDAR"

    const/4 v2, 0x7

    const v3, 0x7f030021

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 23
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "layoutId"    # I
    .param p4, "cacheSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->layoutId:I

    .line 35
    iput p4, p0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->$VALUES:[Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    return-object v0
.end method
