.class public Lcom/navdy/hud/app/framework/glance/GlanceViewCache;
.super Ljava/lang/Object;
.source "GlanceViewCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static bigCalendarViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static bigGlanceMessageSingleViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static bigGlanceMessageViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static bigMultiTextViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static bigTextViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static cachedViewMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static smallGlanceMessageViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static smallImageViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static smallSignViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static viewSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallGlanceMessageViewCache:Ljava/util/ArrayList;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageViewCache:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageSingleViewCache:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallImageViewCache:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigTextViewCache:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigMultiTextViewCache:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallSignViewCache:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget v1, v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigCalendarViewCache:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->viewSet:Ljava/util/HashSet;

    .line 56
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallGlanceMessageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageSingleViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallImageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigTextViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigMultiTextViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallSignViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigCalendarViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallGlanceMessageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigGlanceMessageSingleViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 125
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallImageViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigTextViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 127
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigMultiTextViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 128
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->smallSignViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 129
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->bigCalendarViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 130
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->viewSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 131
    return-void
.end method

.method public static getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p0, "viewType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "cachedView":Landroid/view/View;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 70
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 71
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cachedView":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 72
    .restart local v0    # "cachedView":Landroid/view/View;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->viewSet:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 73
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 74
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ":-/ view already has parent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 78
    :cond_0
    if-nez v0, :cond_2

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->layoutId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 80
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " creating view for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 89
    :cond_1
    :goto_0
    return-object v0

    .line 84
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 85
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " reusing cache for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V
    .locals 5
    .param p0, "viewType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 93
    if-nez p1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->viewSet:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 98
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ":-/ view already in cache:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 103
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ":-/ view already has parent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 106
    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 107
    invoke-virtual {p1, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 108
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 110
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->cachedViewMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 111
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->cacheSize:I

    if-ge v1, v2, :cond_4

    .line 112
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->viewSet:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " putView cache for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static supportScroll(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;)Z
    .locals 2
    .param p0, "viewType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .prologue
    .line 134
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 139
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 136
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
