.class Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;
.super Ljava/lang/Object;
.source "GlanceNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBottom()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2302(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 320
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2402(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 322
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;->onPosChange(I)V

    goto :goto_0
.end method

.method public onScroll(IIII)V
    .locals 8
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    const/4 v4, 0x0

    .line 329
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;

    move-result-object v3

    if-nez v3, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->topScrub:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2200(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 333
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2302(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 334
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2402(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 336
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 337
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->getHeight()I

    move-result v4

    sub-int v2, v3, v4

    .line 338
    .local v2, "range":I
    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    int-to-double v6, v2

    div-double v0, v4, v6

    .line 339
    .local v0, "pos":D
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v3

    double-to-int v4, v0

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;->onPosChange(I)V

    goto :goto_0
.end method

.method public onTop()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->supportsScroll:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->scrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/view/ObservableScrollView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->topScrub:Landroid/view/View;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2200(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onTop:Z
    invoke-static {v0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2302(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 307
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->onBottom:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2402(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Z)Z

    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$3;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->progressUpdate:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$2500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;->onPosChange(I)V

    goto :goto_0
.end method
