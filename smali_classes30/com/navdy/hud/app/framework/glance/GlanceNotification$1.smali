.class Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;
.super Ljava/lang/Object;
.source "GlanceNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glance/GlanceNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    const-wide/16 v10, 0x7530

    .line 159
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$000(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v6

    if-nez v6, :cond_0

    .line 225
    :goto_0
    return-void

    .line 163
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/ViewGroup;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-nez v6, :cond_1

    .line 223
    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1200()Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 168
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/ViewGroup;

    move-result-object v6

    const v7, 0x7f0e00c3

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 170
    .local v5, "title":Landroid/widget/TextView;
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceNotification$5;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceApp:Lcom/navdy/hud/app/framework/glance/GlanceApp;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$200(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v7

    aget v6, v6, v7
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    packed-switch v6, :pswitch_data_0

    .line 223
    :goto_1
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1200()Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 175
    :pswitch_1
    :try_start_2
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setMainImage()V
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$300(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 220
    .end local v5    # "title":Landroid/widget/TextView;
    :catch_0
    move-exception v4

    .line 221
    .local v4, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 223
    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1200()Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 185
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v5    # "title":Landroid/widget/TextView;
    :pswitch_2
    :try_start_4
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->hasFuelLevelInfo:Z
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$400(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 186
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v0

    .line 187
    .local v0, "fuelLevel":I
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Map;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v6

    if-le v0, v6, :cond_2

    .line 223
    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1200()Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 190
    :cond_2
    :try_start_5
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Map;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    .end local v0    # "fuelLevel":I
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v1

    .line 195
    .local v1, "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getCurrentGasStation()Lcom/here/android/mpa/search/Place;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 196
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getCurrentGasStation()Lcom/here/android/mpa/search/Place;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v6

    .line 197
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v7

    invoke-virtual {v7}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v6

    mul-double/2addr v6, v12

    const-wide v8, 0x4099255c28f5c28fL    # 1609.34

    div-double/2addr v6, v8

    .line 196
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-double v6, v6

    div-double v2, v6, v12

    .line 199
    .local v2, "milesToGasStation":D
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->data:Ljava/util/Map;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$500(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Map;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    .end local v2    # "milesToGasStation":D
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setTitle()V
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$600(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)V

    .line 203
    iget-object v6, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->glanceExtendedContainer:Landroid/view/ViewGroup;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Landroid/view/ViewGroup;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->setExpandedContent(Landroid/view/View;)V
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$700(Lcom/navdy/hud/app/framework/glance/GlanceNotification;Landroid/view/View;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 223
    .end local v1    # "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .end local v5    # "title":Landroid/widget/TextView;
    :catchall_0
    move-exception v6

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1200()Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$1100(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/lang/Runnable;

    move-result-object v8

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v6

    .line 217
    .restart local v5    # "title":Landroid/widget/TextView;
    :pswitch_3
    :try_start_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->postTime:Ljava/util/Date;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$800(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Ljava/util/Date;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/glance/GlanceNotification$1;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    # getter for: Lcom/navdy/hud/app/framework/glance/GlanceNotification;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;
    invoke-static {v9}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->access$900(Lcom/navdy/hud/app/framework/glance/GlanceNotification;)Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getTimeStr(JLjava/util/Date;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
