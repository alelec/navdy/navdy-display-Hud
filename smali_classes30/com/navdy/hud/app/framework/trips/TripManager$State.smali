.class public final enum Lcom/navdy/hud/app/framework/trips/TripManager$State;
.super Ljava/lang/Enum;
.source "TripManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/trips/TripManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/trips/TripManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/trips/TripManager$State;

.field public static final enum STARTED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

.field public static final enum STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 327
    new-instance v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/trips/TripManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STARTED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    new-instance v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/trips/TripManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/framework/trips/TripManager$State;

    sget-object v1, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STARTED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;->$VALUES:[Lcom/navdy/hud/app/framework/trips/TripManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/trips/TripManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 327
    const-class v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/trips/TripManager$State;
    .locals 1

    .prologue
    .line 327
    sget-object v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;->$VALUES:[Lcom/navdy/hud/app/framework/trips/TripManager$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/trips/TripManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/trips/TripManager$State;

    return-object v0
.end method
