.class Lcom/navdy/hud/app/framework/trips/TripManager$1;
.super Ljava/lang/Object;
.source "TripManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/trips/TripManager;->onTrack(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

.field final synthetic val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 32

    .prologue
    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/framework/trips/TripManager$State;

    move-result-object v27

    sget-object v28, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_0

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # invokes: Lcom/navdy/hud/app/framework/trips/TripManager;->createNewTrip()V
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$100(Lcom/navdy/hud/app/framework/trips/TripManager;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    sget-object v28, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STARTED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$002(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/hud/app/framework/trips/TripManager$State;)Lcom/navdy/hud/app/framework/trips/TripManager$State;

    .line 108
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    .line 110
    .local v6, "coords":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v10

    .line 111
    .local v10, "latitude":D
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v12

    .line 112
    .local v12, "longitude":D
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoCoordinate;->getAltitude()D

    move-result-wide v4

    .line 114
    .local v4, "altitude":D
    invoke-static {v6}, Lcom/navdy/hud/app/maps/util/MapUtils;->sanitizeCoords(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v16

    .line 116
    .local v16, "sanitizedCoords":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v16, :cond_2

    .line 117
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "filtering out bad coords: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 217
    :cond_1
    :goto_0
    return-void

    .line 121
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v27

    if-eqz v27, :cond_4

    .line 122
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 124
    .local v8, "currentTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTrackingTime:J
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$500(Lcom/navdy/hud/app/framework/trips/TripManager;)J

    move-result-wide v28

    sub-long v20, v8, v28

    .line 125
    .local v20, "timeElapsed":J
    const-wide/16 v28, 0x3e8

    cmp-long v27, v20, v28

    if-ltz v27, :cond_1

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTrackingTime:J
    invoke-static {v0, v8, v9}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$502(Lcom/navdy/hud/app/framework/trips/TripManager;J)J

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v7, v0

    .line 131
    .local v7, "dist":I
    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide v30, 0x408f400000000000L    # 1000.0

    div-double v22, v28, v30

    .line 132
    .local v22, "timeElapsedInSeconds":D
    int-to-double v0, v7

    move-wide/from16 v28, v0

    div-double v18, v28, v22

    .line 133
    .local v18, "speedMetersPerSecond":D
    const-wide/high16 v28, 0x4069000000000000L    # 200.0

    cmpg-double v27, v18, v28

    if-gtz v27, :cond_8

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$600(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    add-int v28, v28, v7

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$602(Lcom/navdy/hud/app/framework/trips/TripManager;I)I

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$700(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    add-int v28, v28, v7

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$702(Lcom/navdy/hud/app/framework/trips/TripManager;I)I

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$800(Lcom/navdy/hud/app/framework/trips/TripManager;)J

    move-result-wide v28

    const-wide/16 v30, 0x0

    cmp-long v27, v28, v30

    if-eqz v27, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$800(Lcom/navdy/hud/app/framework/trips/TripManager;)J

    move-result-wide v28

    sub-long v28, v8, v28

    const-wide/16 v30, 0x7530

    cmp-long v27, v28, v30

    if-lez v27, :cond_4

    .line 137
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J
    invoke-static {v0, v8, v9}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$802(Lcom/navdy/hud/app/framework/trips/TripManager;J)J

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelledWithNavdy()J

    move-result-wide v14

    .line 139
    .local v14, "newValue":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    const-string v28, "trip_manager_total_distance_travelled_with_navdy"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v0, v1, v14, v15}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$700(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistedValue:J
    invoke-static/range {v27 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$902(Lcom/navdy/hud/app/framework/trips/TripManager;J)J

    .line 151
    .end local v7    # "dist":I
    .end local v8    # "currentTime":J
    .end local v14    # "newValue":J
    .end local v18    # "speedMetersPerSecond":D
    .end local v20    # "timeElapsed":J
    .end local v22    # "timeElapsedInSeconds":D
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v28

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$402(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->isClientConnected:Z
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1000(Lcom/navdy/hud/app/framework/trips/TripManager;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1100(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1102(Lcom/navdy/hud/app/framework/trips/TripManager;I)I

    .line 159
    sget-object v17, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    .line 161
    .local v17, "telemetrySession":Lcom/navdy/hud/app/analytics/TelemetrySession;
    new-instance v27, Lcom/navdy/service/library/events/TripUpdate$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/TripUpdate$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    .line 162
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripNumber:J
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1500(Lcom/navdy/hud/app/framework/trips/TripManager;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->trip_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    .line 163
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1100(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->sequence_number(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    .line 165
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$600(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_traveled(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    new-instance v28, Lcom/navdy/service/library/events/location/LatLong;

    .line 166
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v29

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->current_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 167
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoCoordinate;->getAltitude()D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 168
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 169
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->gps_speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    .line 170
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->obd_speed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 171
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_acceleration_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 172
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_breaking_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 173
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHighGCount()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->high_g_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 174
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionSpeedingPercentage()F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->speeding_ratio(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 175
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionExcessiveSpeedingPercentage()F

    move-result v28

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->excessive_speeding_ratio(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    .line 176
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDistance()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->meters_traveled_since_boot(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 177
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getLatitudeAccuracy()F

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v29, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoPosition;->getLongitudeAccuracy()F

    move-result v29

    add-float v28, v28, v29

    const/high16 v29, 0x40000000    # 2.0f

    div-float v28, v28, v29

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->horizontal_accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 178
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getAltitudeAccuracy()F

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation_accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v29, v0

    .line 179
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastLocation:Landroid/location/Location;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1200(Lcom/navdy/hud/app/framework/trips/TripManager;)Landroid/location/Location;

    move-result-object v29

    # invokes: Lcom/navdy/hud/app/framework/trips/TripManager;->toCoordinate(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static/range {v28 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1300(Lcom/navdy/hud/app/framework/trips/TripManager;Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->last_raw_coordinate(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v26

    .line 182
    .local v26, "updateBuilder":Lcom/navdy/service/library/events/TripUpdate$Builder;
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1600(Lcom/navdy/hud/app/framework/trips/TripManager;)Ljava/lang/StringBuilder;

    move-result-object v27

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 185
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "TripUpdate: tripNumber="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripNumber:J
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1500(Lcom/navdy/hud/app/framework/trips/TripManager;)J

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; sequenceNumber="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1100(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; timestamp="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; distanceTraveled="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$600(Lcom/navdy/hud/app/framework/trips/TripManager;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; currentPosition="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; elevation="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 187
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoCoordinate;->getAltitude()D

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; bearing="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 188
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; gpsSpeed="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; obdSpeed="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    .line 189
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 191
    .local v24, "tripUpdateString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1600(Lcom/navdy/hud/app/framework/trips/TripManager;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .end local v24    # "tripUpdateString":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->isRouteTracking()Z

    move-result v27

    if-eqz v27, :cond_6

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v27, v0

    .line 196
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1900(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id(Ljava/lang/String;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 197
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1800(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->estimated_time_remaining(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 198
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1700(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_to_destination(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    .line 200
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 201
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "; chosenDestinationId="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1900(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; estimatedTimeRemaining="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 202
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1800(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "; distanceToDestination="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v28, v0

    .line 203
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$1700(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 205
    .local v25, "tripUpdateStringWithDestination":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1600(Lcom/navdy/hud/app/framework/trips/TripManager;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .end local v25    # "tripUpdateStringWithDestination":Ljava/lang/String;
    :cond_6
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 210
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$1600(Lcom/navdy/hud/app/framework/trips/TripManager;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 212
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    # setter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2002(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/service/library/events/TripUpdate$Builder;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/framework/trips/TripManager$State;

    move-result-object v27

    sget-object v28, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2100(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/squareup/otto/Bus;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/event/RemoteEvent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v29, v0

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/navdy/service/library/events/TripUpdate$Builder;->build()Lcom/navdy/service/library/events/TripUpdate;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual/range {v27 .. v28}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 143
    .end local v17    # "telemetrySession":Lcom/navdy/hud/app/analytics/TelemetrySession;
    .end local v26    # "updateBuilder":Lcom/navdy/service/library/events/TripUpdate$Builder;
    .restart local v7    # "dist":I
    .restart local v8    # "currentTime":J
    .restart local v18    # "speedMetersPerSecond":D
    .restart local v20    # "timeElapsed":J
    .restart local v22    # "timeElapsedInSeconds":D
    :cond_8
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Unusual jump between co ordinates, Last lat :"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v29, v0

    .line 144
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v30

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "Lon :"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v29, v0

    .line 145
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v30

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "Current Lat:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v29, v0

    .line 146
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v30

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "Lon :"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/trips/TripManager$1;->val$event:Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    move-object/from16 v29, v0

    .line 147
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v30

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 143
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
