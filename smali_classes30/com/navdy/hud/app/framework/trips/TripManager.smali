.class public Lcom/navdy/hud/app/framework/trips/TripManager;
.super Ljava/lang/Object;
.source "TripManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/trips/TripManager$State;,
        Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;,
        Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;,
        Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final MAX_SPEED_METERS_PER_SECOND_THRESHOLD:I = 0xc8

.field private static final MIN_TIME_THRESHOLD:J = 0x3e8L

.field public static final PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY:Ljava/lang/String; = "trip_manager_total_distance_travelled_with_navdy"

.field public static final TOTAL_DISTANCE_PERSIST_INTERVAL:J = 0x7530L

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private currentDistanceTraveledInMeters:I

.field private currentSequenceNumber:I

.field private volatile currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;

.field private currentTripNumber:J

.field private currentTripStartTime:J

.field private isClientConnected:Z

.field private lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;

.field private lastLocation:Landroid/location/Location;

.field private lastTotalDistancePersistTime:J

.field private lastTotalDistancePersistedValue:J

.field private lastTrackingTime:J

.field private lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;

.field private logBuilder:Ljava/lang/StringBuilder;

.field preferences:Landroid/content/SharedPreferences;

.field private final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private totalDistanceTravelledInMeters:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-wide v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J

    .line 63
    iput-wide v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistedValue:J

    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->bus:Lcom/squareup/otto/Bus;

    .line 71
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 72
    iput-object p2, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->preferences:Landroid/content/SharedPreferences;

    .line 74
    sget-object v0, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;

    .line 76
    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->isClientConnected:Z

    .line 77
    iput v1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/framework/trips/TripManager$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/hud/app/framework/trips/TripManager$State;)Lcom/navdy/hud/app/framework/trips/TripManager$State;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/trips/TripManager$State;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/trips/TripManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/trips/TripManager;->createNewTrip()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/trips/TripManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->isClientConnected:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/trips/TripManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I

    return v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/framework/trips/TripManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I

    return p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/framework/trips/TripManager;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/framework/trips/TripManager;Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/trips/TripManager;->toCoordinate(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/manager/SpeedManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/trips/TripManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripNumber:J

    return-wide v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/framework/trips/TripManager;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->logBuilder:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/service/library/events/TripUpdate$Builder;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/TripUpdate$Builder;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/trips/TripManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTrackingTime:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/framework/trips/TripManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTrackingTime:J

    return-wide p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/trips/TripManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/framework/trips/TripManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/trips/TripManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I

    return v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/framework/trips/TripManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/trips/TripManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/framework/trips/TripManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistTime:J

    return-wide p1
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/framework/trips/TripManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .param p1, "x1"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistedValue:J

    return-wide p1
.end method

.method private createNewTrip()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 276
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 278
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripNumber:J

    .line 279
    iput v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentSequenceNumber:I

    .line 280
    iput v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripStartTime:J

    .line 282
    return-void
.end method

.method private toCoordinate(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 4
    .param p1, "lastLocation"    # Landroid/location/Location;

    .prologue
    .line 222
    if-nez p1, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 234
    :goto_0
    return-object v0

    .line 225
    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 226
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 227
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 229
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 230
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 231
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 232
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 233
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getCurrentDistanceTraveled()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentDistanceTraveledInMeters:I

    return v0
.end method

.method public getCurrentTripStartTime()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->currentTripStartTime:J

    return-wide v0
.end method

.method public getTotalDistanceTravelled()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I

    return v0
.end method

.method public getTotalDistanceTravelledWithNavdy()J
    .locals 8

    .prologue
    .line 318
    iget-object v4, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "trip_manager_total_distance_travelled_with_navdy"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 319
    .local v2, "persistedValue":J
    iget v4, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->totalDistanceTravelledInMeters:I

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastTotalDistancePersistedValue:J

    sub-long/2addr v4, v6

    add-long v0, v2, v4

    .line 320
    .local v0, "newValue":J
    return-wide v0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_1

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->isClientConnected:Z

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->isClientConnected:Z

    goto :goto_0
.end method

.method public onFinishedTripRouteEvent(Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 251
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/trips/TripManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/trips/TripManager$3;-><init>(Lcom/navdy/hud/app/framework/trips/TripManager;)V

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 263
    return-void
.end method

.method public onLocation(Landroid/location/Location;)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager;->lastLocation:Landroid/location/Location;

    .line 95
    return-void
.end method

.method public onNewTrip(Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 240
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/trips/TripManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/trips/TripManager$2;-><init>(Lcom/navdy/hud/app/framework/trips/TripManager;)V

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 246
    return-void
.end method

.method public onTrack(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 100
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/trips/TripManager$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/trips/TripManager$1;-><init>(Lcom/navdy/hud/app/framework/trips/TripManager;Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)V

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 219
    return-void
.end method
