.class Lcom/navdy/hud/app/framework/trips/TripManager$3;
.super Ljava/lang/Object;
.source "TripManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/trips/TripManager;->onFinishedTripRouteEvent(Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/trips/TripManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/trips/TripManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/TripUpdate$Builder;->arrived_at_destination_id(Ljava/lang/String;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    .line 257
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->currentState:Lcom/navdy/hud/app/framework/trips/TripManager$State;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/framework/trips/TripManager$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/trips/TripManager$State;->STOPPED:Lcom/navdy/hud/app/framework/trips/TripManager$State;

    if-eq v0, v1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2100(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$3;->this$0:Lcom/navdy/hud/app/framework/trips/TripManager;

    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->lastTripUpdateBuilder:Lcom/navdy/service/library/events/TripUpdate$Builder;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$2000(Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/service/library/events/TripUpdate$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/TripUpdate$Builder;->build()Lcom/navdy/service/library/events/TripUpdate;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 261
    :cond_0
    return-void
.end method
