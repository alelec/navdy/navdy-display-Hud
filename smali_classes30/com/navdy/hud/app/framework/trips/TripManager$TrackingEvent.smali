.class public Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;
.super Ljava/lang/Object;
.source "TripManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/trips/TripManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackingEvent"
.end annotation


# instance fields
.field private final chosenDestinationId:Ljava/lang/String;

.field private final distanceToDestination:I

.field private final estimatedTimeRemaining:I

.field private final geoPosition:Lcom/here/android/mpa/common/GeoPosition;


# direct methods
.method public constructor <init>(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 2
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    const/4 v1, -0x1

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;

    .line 294
    iput v1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I

    .line 295
    iput v1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I

    .line 296
    return-void
.end method

.method public constructor <init>(Lcom/here/android/mpa/common/GeoPosition;Ljava/lang/String;II)V
    .locals 0
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p2, "chosenDestinationId"    # Ljava/lang/String;
    .param p3, "estimatedTimeRemaining"    # I
    .param p4, "distanceToDestination"    # I

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    iput-object p1, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 301
    iput-object p2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;

    .line 302
    iput p3, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I

    .line 303
    iput p4, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I

    .line 304
    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    .prologue
    .line 284
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I

    return v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    .prologue
    .line 284
    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I

    return v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method


# virtual methods
.method public isRouteTracking()Z
    .locals 3

    .prologue
    .line 307
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    # getter for: Lcom/navdy/hud/app/framework/trips/TripManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/trips/TripManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[routeTracking] chosenDestinationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; estimatedTimeRemaining="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; distanceToDestination="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->chosenDestinationId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->estimatedTimeRemaining:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;->distanceToDestination:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
