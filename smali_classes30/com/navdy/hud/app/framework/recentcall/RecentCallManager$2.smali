.class Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;
.super Ljava/lang/Object;
.source "RecentCallManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->onContactResponse(Lcom/navdy/service/library/events/contacts/ContactResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

.field final synthetic val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Lcom/navdy/service/library/events/contacts/ContactResponse;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 208
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v12, v12, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 209
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "invalid contact repsonse"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const/4 v3, 0x0

    .line 213
    .local v3, "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    const/4 v5, 0x0

    .line 214
    .local v5, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$200(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;

    move-result-object v13

    monitor-enter v13
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$200(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 216
    .local v2, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-nez v2, :cond_2

    .line 217
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "identifer not found["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 218
    monitor-exit v13

    goto :goto_0

    .line 252
    .end local v2    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :catchall_0
    move-exception v12

    :goto_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v12
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 268
    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :catch_0
    move-exception v11

    .line 269
    .local v11, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 220
    .end local v11    # "t":Ljava/lang/Throwable;
    .restart local v2    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v12, v12, Lcom/navdy/service/library/events/contacts/ContactResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v14, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v12, v14, :cond_9

    .line 222
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v12, v12, Lcom/navdy/service/library/events/contacts/ContactResponse;->contacts:Ljava/util/List;

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v12, v12, Lcom/navdy/service/library/events/contacts/ContactResponse;->contacts:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_4

    .line 223
    :cond_3
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "no contact returned for ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] status["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 224
    monitor-exit v13

    goto/16 :goto_0

    .line 226
    :cond_4
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "contacts returned ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->contacts:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v12, v12, Lcom/navdy/service/library/events/contacts/ContactResponse;->contacts:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    move-object v6, v5

    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .local v6, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v4, v3

    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .local v4, "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :goto_2
    :try_start_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/service/library/events/contacts/Contact;

    .line 228
    .local v8, "obj":Lcom/navdy/service/library/events/contacts/Contact;
    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_5

    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v14}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 229
    :cond_5
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "no number for  ["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] status["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/service/library/events/contacts/ContactResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v3, v4

    .end local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :goto_3
    move-object v6, v5

    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v4, v3

    .line 248
    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    goto :goto_2

    .line 232
    :cond_6
    new-instance v7, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    invoke-direct {v7, v2}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V

    .line 233
    .local v7, "copy":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    iget-object v15, v8, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iget-object v0, v8, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getPhoneNumber(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isDisplayNameValid(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 234
    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    iput-object v14, v7, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    .line 238
    :goto_4
    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iput-object v14, v7, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    .line 239
    iget-object v14, v8, Lcom/navdy/service/library/events/contacts/Contact;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-static {v14}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getNumberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v14

    iput-object v14, v7, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 240
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->validateArguments()V

    .line 241
    if-nez v4, :cond_b

    .line 242
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 243
    .end local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :try_start_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 245
    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :goto_5
    :try_start_6
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 236
    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_7
    const/4 v14, 0x0

    :try_start_7
    iput-object v14, v7, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_4

    .line 252
    .end local v7    # "copy":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v8    # "obj":Lcom/navdy/service/library/events/contacts/Contact;
    :catchall_1
    move-exception v12

    move-object v5, v6

    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v3, v4

    .end local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    goto/16 :goto_1

    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_8
    move-object v5, v6

    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v3, v4

    .end local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :goto_6
    :try_start_8
    monitor-exit v13
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 253
    if-eqz v3, :cond_0

    .line 254
    :try_start_9
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "contact found ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] downloading photo"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$300(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;

    move-result-object v13

    monitor-enter v13
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0

    .line 256
    :try_start_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$300(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v12, v14, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    monitor-exit v13
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 258
    :try_start_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$400(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Lcom/squareup/otto/Bus;

    move-result-object v12

    new-instance v13, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-direct {v13, v14, v5}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v12, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 259
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v9

    .line 260
    .local v9, "phoneImageDownloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 261
    .local v10, "rc":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    iget-object v13, v10, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    sget-object v14, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v9, v13, v14}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->clearPhotoCheckEntry(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 262
    iget-object v13, v10, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    sget-object v14, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v15, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v0, v10, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_7

    .line 250
    .end local v9    # "phoneImageDownloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .end local v10    # "rc":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_9
    :try_start_c
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "request failed for ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] status["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/contacts/ContactResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_6

    .line 257
    :catchall_2
    move-exception v12

    :try_start_d
    monitor-exit v13
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :try_start_e
    throw v12

    .line 264
    .restart local v9    # "phoneImageDownloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    :cond_a
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    .line 265
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    # invokes: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Ljava/util/List;)V
    invoke-static {v12, v3}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$500(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Ljava/util/List;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_0

    .line 252
    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v9    # "phoneImageDownloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v7    # "copy":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .restart local v8    # "obj":Lcom/navdy/service/library/events/contacts/Contact;
    :catchall_3
    move-exception v12

    move-object v5, v6

    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    goto/16 :goto_1

    .end local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_b
    move-object v5, v6

    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v5    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    move-object v3, v4

    .end local v4    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v3    # "callList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    goto/16 :goto_5
.end method
