.class Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;
.super Ljava/lang/Object;
.source "RecentCallManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

.field final synthetic val$calls:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;->this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;->val$calls:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 297
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "driverId":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;->val$calls:Ljava/util/List;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->storeRecentCalls(Ljava/lang/String;Ljava/util/List;Z)V

    .line 299
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;->val$calls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 300
    .local v0, "c":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    # getter for: Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "added contact ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 302
    .end local v0    # "c":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_0
    return-void
.end method
