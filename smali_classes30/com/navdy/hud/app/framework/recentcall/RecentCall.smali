.class public Lcom/navdy/hud/app/framework/recentcall/RecentCall;
.super Ljava/lang/Object;
.source "RecentCall.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;,
        Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public callTime:Ljava/util/Date;

.field public callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

.field public category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

.field public defaultImageIndex:I

.field public firstName:Ljava/lang/String;

.field public formattedNumber:Ljava/lang/String;

.field public initials:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public numberTypeStr:Ljava/lang/String;

.field public numericNumber:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V
    .locals 1
    .param p1, "call"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    .line 120
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 121
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 122
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    .line 123
    iget-object v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 124
    iget v0, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    iput v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "category"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "numberType"    # Lcom/navdy/hud/app/framework/contacts/NumberType;
    .param p5, "callTime"    # Ljava/util/Date;
    .param p6, "callType"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    .param p7, "defaultImageIndex"    # I
    .param p8, "numericNumber"    # J

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    .line 108
    iput-object p3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 110
    iput-object p4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 111
    iput-object p5, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    .line 112
    iput-object p6, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 113
    iput p7, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    .line 114
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->validateArguments()V

    .line 115
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)I
    .locals 2
    .param p1, "another"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 130
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    iget-object v1, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->compareTo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecentCall{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", number=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numberType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", defaultImageIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateArguments()V
    .locals 8

    .prologue
    .line 136
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->firstName:Ljava/lang/String;

    .line 138
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->initials:Ljava/lang/String;

    .line 140
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getPhoneType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberTypeStr:Ljava/lang/String;

    .line 141
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->formattedNumber:Ljava/lang/String;

    .line 142
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 143
    iget-wide v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 144
    iget-wide v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    iput-wide v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    .line 159
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 148
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 149
    .local v1, "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 150
    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v1    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :catch_0
    move-exception v2

    .line 153
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 154
    sget-object v3, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
