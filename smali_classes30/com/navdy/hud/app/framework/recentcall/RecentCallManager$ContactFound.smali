.class public Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;
.super Ljava/lang/Object;
.source "RecentCallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContactFound"
.end annotation


# instance fields
.field public contact:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public identifier:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, "contact":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->identifier:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;->contact:Ljava/util/List;

    .line 51
    return-void
.end method
