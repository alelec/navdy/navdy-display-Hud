.class public final enum Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
.super Ljava/lang/Enum;
.source "RecentCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/recentcall/RecentCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CallType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

.field public static final enum INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

.field public static final enum MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

.field public static final enum OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

.field public static final enum UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;


# instance fields
.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const-string v1, "UNNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const-string v1, "OUTGOING"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const-string v1, "MISSED"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->$VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->value:I

    .line 34
    return-void
.end method

.method public static buildFromValue(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    .locals 1
    .param p0, "n"    # I

    .prologue
    .line 41
    packed-switch p0, :pswitch_data_0

    .line 52
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    :goto_0
    return-object v0

    .line 43
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 49
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->$VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->value:I

    return v0
.end method
