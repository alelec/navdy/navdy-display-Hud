.class public Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
.super Ljava/lang/Object;
.source "RecentCallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;,
        Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;
    }
.end annotation


# static fields
.field private static final RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

.field private static final sInstance:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private contactRequestMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation
.end field

.field private numberMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation
.end field

.field private volatile recentCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation
.end field

.field private tempContactLookupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sInstance:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->numberMap:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->load()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Ljava/util/List;)V

    return-void
.end method

.method private buildMap()V
    .locals 6

    .prologue
    .line 316
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->numberMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 317
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 323
    :cond_0
    return-void

    .line 320
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 321
    .local v0, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->numberMap:Ljava/util/Map;

    iget-wide v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sInstance:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    return-object v0
.end method

.method private load()V
    .locals 5

    .prologue
    .line 119
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "id":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->getRecentsCalls(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    .line 121
    sget-object v2, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load recentCall id["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] calls["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->buildMap()V

    .line 127
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 129
    .end local v0    # "id":Ljava/lang/String;
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v1

    .line 123
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    .line 124
    sget-object v2, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->buildMap()V

    .line 127
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->buildMap()V

    .line 127
    iget-object v3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    throw v2
.end method

.method private requestContactInfo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V
    .locals 5
    .param p1, "call"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .prologue
    .line 189
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "number not available for ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contact ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] request already pending"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 194
    monitor-exit v1

    .line 200
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contact ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] request sent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/contacts/ContactRequest;

    iget-object v4, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/contacts/ContactRequest;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 199
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private storeContactInfo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V
    .locals 1
    .param p1, "call"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .prologue
    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Ljava/util/List;)V

    .line 291
    return-void
.end method

.method private storeContactInfo(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "calls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$3;-><init>(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Ljava/util/List;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 304
    return-void
.end method


# virtual methods
.method public buildRecentCalls()V
    .locals 3

    .prologue
    .line 102
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->contactRequestMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 104
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$1;-><init>(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 115
    :goto_0
    return-void

    .line 104
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 113
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->load()V

    goto :goto_0
.end method

.method public clearContactLookupMap()V
    .locals 2

    .prologue
    .line 276
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 278
    monitor-exit v1

    .line 279
    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearRecentCalls()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->setRecentCalls(Ljava/util/List;)V

    .line 99
    return-void
.end method

.method public getContactsFromId(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 283
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDefaultContactImage(J)I
    .locals 3
    .param p1, "number"    # J

    .prologue
    .line 307
    iget-object v1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->numberMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 308
    .local v0, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-nez v0, :cond_0

    .line 309
    const/4 v1, -0x1

    .line 311
    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    goto :goto_0
.end method

.method public getRecentCalls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    return-object v0
.end method

.method public getRecentCallsSize()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 88
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleNewCall(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)Z
    .locals 7
    .param p1, "call"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 159
    sget-object v2, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handle new call:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 160
    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 163
    const/4 v1, 0x0

    .line 164
    .local v1, "number":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 165
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->tempContactLookupMap:Ljava/util/HashMap;

    iget-object v6, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 166
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    if-eqz v0, :cond_1

    .line 167
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 168
    monitor-exit v5

    move v2, v3

    .line 184
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v1    # "number":Ljava/lang/String;
    :goto_0
    return v2

    .line 170
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .restart local v1    # "number":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/contacts/Contact;

    iget-object v1, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    .line 172
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 175
    iput-object v1, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    .line 176
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V

    move v2, v4

    .line 177
    goto :goto_0

    .line 172
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 179
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_2
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->requestContactInfo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V

    move v2, v3

    .line 180
    goto :goto_0

    .line 183
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v1    # "number":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->storeContactInfo(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V

    move v2, v4

    .line 184
    goto :goto_0
.end method

.method public onCallEvent(Lcom/navdy/service/library/events/callcontrol/CallEvent;)V
    .locals 10
    .param p1, "event"    # Lcom/navdy/service/library/events/callcontrol/CallEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 133
    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call event does not have a number, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 151
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 140
    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    sget-object v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .line 148
    .local v6, "callType":Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    :goto_1
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    sget-object v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    const/4 v7, -0x1

    const-wide/16 v8, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V

    .line 150
    .local v0, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->handleNewCall(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)Z

    goto :goto_0

    .line 143
    .end local v0    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v6    # "callType":Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .restart local v6    # "callType":Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    goto :goto_1

    .line 146
    .end local v6    # "callType":Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    .restart local v6    # "callType":Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    goto :goto_1
.end method

.method public onContactResponse(Lcom/navdy/service/library/events/contacts/ContactResponse;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/contacts/ContactResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 204
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$2;-><init>(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Lcom/navdy/service/library/events/contacts/ContactResponse;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 273
    return-void
.end method

.method public setRecentCalls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "recentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->recentCalls:Ljava/util/List;

    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->buildMap()V

    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->RECENT_CALL_CHANGED:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$RecentCallChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 95
    return-void
.end method
