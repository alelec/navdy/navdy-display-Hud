.class public final enum Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
.super Ljava/lang/Enum;
.source "RecentCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/recentcall/RecentCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

.field public static final enum MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

.field public static final enum PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

.field public static final enum UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;


# instance fields
.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    const-string v1, "UNNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 61
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    const-string v1, "PHONE_CALL"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 62
    new-instance v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 59
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->$VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->value:I

    .line 68
    return-void
.end method

.method public static buildFromValue(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
    .locals 1
    .param p0, "n"    # I

    .prologue
    .line 75
    packed-switch p0, :pswitch_data_0

    .line 83
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    goto :goto_0

    .line 80
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    const-class v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->$VALUES:[Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->value:I

    return v0
.end method
