.class public interface abstract Lcom/navdy/hud/app/framework/notifications/INotificationController;
.super Ljava/lang/Object;
.source "INotificationController.java"


# virtual methods
.method public abstract collapseNotification(ZZ)V
.end method

.method public abstract expandNotification(Z)V
.end method

.method public abstract getUIContext()Landroid/content/Context;
.end method

.method public abstract isExpanded()Z
.end method

.method public abstract isExpandedWithStack()Z
.end method

.method public abstract isShowOn()Z
.end method

.method public abstract isTtsOn()Z
.end method

.method public abstract moveNext(Z)V
.end method

.method public abstract movePrevious(Z)V
.end method

.method public abstract resetTimeout()V
.end method

.method public abstract startTimeout(I)V
.end method

.method public abstract stopTimeout(Z)V
.end method
