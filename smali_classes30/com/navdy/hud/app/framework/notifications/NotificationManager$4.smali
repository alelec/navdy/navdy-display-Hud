.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 412
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notif-anim-start ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] type["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mode ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 413
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v2, v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 414
    sget-object v2, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v2, :cond_0

    .line 416
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCollapsed:Z
    invoke-static {v2, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1802(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 417
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 418
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    iget-object v2, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    const/4 v3, 0x0

    invoke-virtual {v2, v6, v3}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->stopTimeout(ZLjava/lang/Runnable;)V

    .line 419
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notif-anim-start showing notif ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 421
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iput-boolean v5, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    .line 422
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iput-boolean v6, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 423
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    .line 424
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current notification ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] called start["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 425
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-virtual {v0, v2, v1}, Lcom/navdy/hud/app/view/NotificationView;->addCustomView(Lcom/navdy/hud/app/framework/notifications/INotification;Landroid/view/View;)V

    .line 426
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 427
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v3, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 432
    .end local v0    # "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 429
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z
    invoke-static {v2, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 430
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->gestureEnabledSettings:Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->removeSettings(Lcom/navdy/hud/app/device/light/LED$Settings;)V

    goto :goto_0
.end method

.method public onStop(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 12
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 436
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notif-anim-stop ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] type["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] mode ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 439
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v4, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingNotification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 440
    .local v4, "pending":Lcom/navdy/hud/app/framework/notifications/INotification;
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingNotification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 441
    sget-object v6, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne p3, v6, :cond_13

    .line 443
    const/4 v3, 0x0

    .line 445
    .local v3, "notificationAdded":Z
    :try_start_1
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x1

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCollapsed:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1802(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 446
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v5

    .line 448
    .local v5, "view":Lcom/navdy/hud/app/view/NotificationView;
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotificationCoverView()V

    .line 450
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 451
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 452
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "*** user selected delete all glances"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 453
    invoke-virtual {v5}, Lcom/navdy/hud/app/view/NotificationView;->removeCustomView()V

    .line 454
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 455
    :try_start_2
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeAllNotification()V
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    .line 456
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 553
    if-nez v3, :cond_0

    .line 555
    :try_start_3
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .local v0, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 579
    .end local v3    # "notificationAdded":Z
    .end local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :goto_0
    return-void

    .line 456
    .restart local v3    # "notificationAdded":Z
    .restart local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :catchall_0
    move-exception v6

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 553
    .end local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :catchall_1
    move-exception v6

    if-nez v3, :cond_1

    .line 555
    :try_start_6
    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v7, v7, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 557
    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "mode-view: notif onStop show"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V

    .line 560
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_1
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 577
    .end local v3    # "notificationAdded":Z
    .end local v4    # "pending":Lcom/navdy/hud/app/framework/notifications/INotification;
    :catchall_2
    move-exception v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    throw v6

    .line 460
    .restart local v3    # "notificationAdded":Z
    .restart local v4    # "pending":Lcom/navdy/hud/app/framework/notifications/INotification;
    .restart local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :cond_2
    :try_start_7
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->cleanupViews(Lcom/navdy/hud/app/view/NotificationView;)V
    invoke-static {v6, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/view/NotificationView;)V

    .line 462
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v6

    if-nez v6, :cond_4

    .line 553
    if-nez v3, :cond_3

    .line 555
    :try_start_8
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto :goto_0

    .line 466
    :cond_4
    :try_start_9
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v7, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-interface {v6, v7}, Lcom/navdy/hud/app/framework/notifications/INotification;->onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 467
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 468
    .local v2, "notifId":Ljava/lang/String;
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notif-anim-start ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 469
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    .line 470
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    if-nez v6, :cond_5

    .line 471
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->isAlive()Z

    move-result v6

    if-nez v6, :cond_5

    .line 472
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notif-anim-stop not alive, marked removed"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 473
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 476
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v6, :cond_8

    .line 477
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notif-anim-stop removed ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 478
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 479
    :try_start_a
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v8

    const/4 v9, 0x0

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotificationfromStack(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V
    invoke-static {v6, v8, v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 480
    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 490
    :cond_6
    :goto_1
    :try_start_b
    invoke-virtual {v5}, Lcom/navdy/hud/app/view/NotificationView;->removeCustomView()V

    .line 493
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    if-eqz v6, :cond_9

    .line 494
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notif-anim-stop notif resurrected"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 495
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    const/4 v7, 0x0

    iput-boolean v7, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    .line 496
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 497
    const/4 v3, 0x1

    .line 498
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x1

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification(Z)V
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 553
    if-nez v3, :cond_7

    .line 555
    :try_start_c
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_7
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_0

    .line 480
    :catchall_3
    move-exception v6

    :try_start_d
    monitor-exit v7
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v6

    .line 483
    :cond_8
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notif-anim-stop pushed back ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 484
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v6, :cond_6

    .line 485
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    const/4 v7, 0x0

    iput-boolean v7, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 486
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    goto/16 :goto_1

    .line 502
    :cond_9
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 504
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v6

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isScreenHighPriority(Lcom/navdy/service/library/events/ui/Screen;)Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 505
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;
    invoke-static {v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;
    invoke-static {v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 506
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "launched pending screen hp:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 507
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/Screen;

    .line 508
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 509
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 553
    if-nez v3, :cond_a

    .line 555
    :try_start_f
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_a
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_0

    .line 514
    :cond_b
    :try_start_10
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 515
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notif-anim-stop staged-notif ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v8

    iget-object v8, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v8}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 516
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 517
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2802(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 518
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 519
    const/4 v3, 0x1

    .line 520
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 553
    if-nez v3, :cond_c

    .line 555
    :try_start_11
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_c
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_0

    .line 523
    :cond_d
    :try_start_12
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notif-anim-stop no staged-notif, check if there is pending one"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 524
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 525
    :try_start_13
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/util/TreeMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TreeMap;->size()I

    move-result v6

    if-lez v6, :cond_e

    .line 526
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/util/TreeMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 527
    .local v1, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    iget-boolean v6, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    if-eqz v6, :cond_e

    .line 528
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "notif-anim-stop found pending ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v9}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 529
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 530
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 531
    const/4 v3, 0x1

    .line 532
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    .line 533
    const/4 v4, 0x0

    .line 536
    .end local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_e
    monitor-exit v7
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    .line 538
    :try_start_14
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_11

    .line 539
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v8, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;
    invoke-static {v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 540
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 549
    :cond_f
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/Screen;

    .line 550
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 551
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 553
    if-nez v3, :cond_10

    .line 555
    :try_start_15
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 556
    .restart local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 557
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "mode-view: notif onStop show"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 577
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .end local v2    # "notifId":Ljava/lang/String;
    .end local v3    # "notificationAdded":Z
    .end local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :cond_10
    :goto_3
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_0

    .line 536
    .restart local v2    # "notifId":Ljava/lang/String;
    .restart local v3    # "notificationAdded":Z
    .restart local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :catchall_4
    move-exception v6

    :try_start_16
    monitor-exit v7
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    :try_start_17
    throw v6

    .line 541
    :cond_11
    if-eqz v4, :cond_12

    .line 542
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "adding pending notification:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " current = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 543
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 544
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_2

    .line 545
    :cond_12
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v6

    if-eqz v6, :cond_f

    .line 546
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;
    invoke-static {v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;
    invoke-static {v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 547
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "launched pending screen:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    goto/16 :goto_2

    .line 564
    .end local v2    # "notifId":Ljava/lang/String;
    .end local v3    # "notificationAdded":Z
    .end local v5    # "view":Lcom/navdy/hud/app/view/NotificationView;
    :cond_13
    :try_start_18
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x1

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 565
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-nez v6, :cond_14

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v6

    if-eqz v6, :cond_15

    .line 566
    :cond_14
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notif-anim-start got removed/changed while it was animating, hide it"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 567
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 568
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    .line 574
    :goto_4
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v8

    const-string v9, "Notification"

    invoke-static {v7, v8, v9}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureDetectionEnabled(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->gestureEnabledSettings:Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/device/light/LED$Settings;)Lcom/navdy/hud/app/device/light/LED$Settings;

    goto/16 :goto_3

    .line 570
    :cond_15
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 571
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/view/NotificationView;->showNextNotificationColor()V

    .line 572
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v7

    iget-object v7, v7, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v7}, Lcom/navdy/hud/app/framework/notifications/INotification;->getTimeout()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    goto :goto_4
.end method
