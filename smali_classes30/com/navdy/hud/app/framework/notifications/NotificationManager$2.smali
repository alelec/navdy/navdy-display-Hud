.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotificationController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public collapseNotification(ZZ)V
    .locals 2
    .param p1, "completely"    # Z
    .param p2, "quickly"    # Z

    .prologue
    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v1, 0x0

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotificationInternal(ZZZ)V
    invoke-static {v0, p1, p2, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;ZZZ)V

    .line 336
    return-void
.end method

.method public expandNotification(Z)V
    .locals 14
    .param p1, "withStack"    # Z

    .prologue
    .line 195
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "expandNotification  running="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z
    invoke-static {v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " qsize:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;
    invoke-static {v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/util/Queue;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Queue;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v8

    .line 197
    .local v8, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-eqz v8, :cond_e

    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v11

    if-eqz v11, :cond_e

    .line 201
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 202
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "animation in progress, ignore expand"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 331
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->isExpanded()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 208
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "already expanded"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_1
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/widget/FrameLayout;

    move-result-object v3

    .line 212
    .local v3, "layout":Landroid/widget/FrameLayout;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationCoverView()Landroid/view/View;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/view/View;

    move-result-object v4

    .line 213
    .local v4, "layoutCover":Landroid/view/View;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v2

    .line 214
    .local v2, "indicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    sget-object v12, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 216
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v11

    if-lez v11, :cond_2

    .line 218
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 222
    :cond_2
    if-eqz p1, :cond_3

    .line 223
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v9

    .line 224
    .local v9, "preferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v12, v9, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 225
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v12, v9, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 226
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "expanded with["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v13

    iget-object v13, v13, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v13}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] tts="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z
    invoke-static {v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " show="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 229
    .end local v9    # "preferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    :cond_3
    const/4 v10, 0x0

    .line 231
    .local v10, "tempExpandedView":Landroid/view/View;
    if-eqz p1, :cond_4

    if-eqz p1, :cond_5

    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 232
    :cond_4
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v11

    iget-object v11, v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x0

    invoke-interface {v11, v12, v13}, Lcom/navdy/hud/app/framework/notifications/INotification;->getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v10

    .line 234
    if-nez v10, :cond_5

    .line 235
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "cannot expand, view is null"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 236
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v12, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 237
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v12, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_0

    .line 242
    :cond_5
    move-object v1, v10

    .line 243
    .local v1, "expandedView":Landroid/view/View;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z
    invoke-static {v11, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 246
    if-eqz p1, :cond_b

    .line 247
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v12, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v12

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 248
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v12, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1302(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 249
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 250
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->printPriorityMap()V

    .line 252
    :cond_6
    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 260
    :goto_1
    iget-object v11, v8, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->stopTimeout(ZLjava/lang/Runnable;)V

    .line 263
    invoke-virtual {v8}, Lcom/navdy/hud/app/view/NotificationView;->hideNextNotificationColor()V

    .line 265
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v11

    iget-object v7, v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 267
    .local v7, "notificationObj":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-eqz p1, :cond_9

    .line 269
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v5

    .line 270
    .local v5, "notificationCount":I
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isPhoneNotifPresentAndNotAlive()Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 271
    add-int/lit8 v5, v5, -0x1

    .line 273
    :cond_7
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v12, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v12

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndex(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I

    move-result v6

    .line 274
    .local v6, "notificationIndex":I
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "expandNotification index:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    const/4 v11, -0x1

    if-ne v6, v11, :cond_c

    .line 276
    const/4 v6, 0x0

    .line 280
    :goto_2
    add-int/lit8 v5, v5, 0x2

    .line 281
    if-lt v6, v5, :cond_8

    .line 282
    add-int/lit8 v6, v5, -0x1

    .line 284
    :cond_8
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v12, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v12

    iget-object v12, v12, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v12}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v12

    invoke-virtual {v11, v5, v6, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator(III)V

    .line 289
    .end local v5    # "notificationCount":I
    .end local v6    # "notificationIndex":I
    :cond_9
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;

    invoke-direct {v0, p0, v7, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;Lcom/navdy/hud/app/framework/notifications/INotification;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;)V

    .line 301
    .local v0, "endAction":Ljava/lang/Runnable;
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_d

    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 304
    :cond_a
    invoke-virtual {v8}, Lcom/navdy/hud/app/view/NotificationView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v11

    new-instance v12, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$3;

    invoke-direct {v12, p0, v1, v3, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$3;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;Landroid/view/View;Landroid/widget/FrameLayout;Ljava/lang/Runnable;)V

    invoke-virtual {v11, v12}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v11

    new-instance v12, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$2;

    invoke-direct {v12, p0, v4, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$2;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;Landroid/view/View;Landroid/widget/FrameLayout;)V

    .line 317
    invoke-virtual {v11, v12}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v11

    .line 323
    invoke-virtual {v11}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_0

    .line 254
    .end local v0    # "endAction":Ljava/lang/Runnable;
    .end local v7    # "notificationObj":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_b
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 255
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v12, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 256
    iget-object v11, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const/4 v12, 0x0

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v11, v12}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1302(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    goto/16 :goto_1

    .line 278
    .restart local v5    # "notificationCount":I
    .restart local v6    # "notificationIndex":I
    .restart local v7    # "notificationObj":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 326
    .end local v5    # "notificationCount":I
    .end local v6    # "notificationIndex":I
    .restart local v0    # "endAction":Ljava/lang/Runnable;
    :cond_d
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    .line 329
    .end local v0    # "endAction":Ljava/lang/Runnable;
    .end local v1    # "expandedView":Landroid/view/View;
    .end local v2    # "indicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .end local v3    # "layout":Landroid/widget/FrameLayout;
    .end local v4    # "layoutCover":Landroid/view/View;
    .end local v7    # "notificationObj":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v10    # "tempExpandedView":Landroid/view/View;
    :cond_e
    sget-object v11, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "cannot display expand notif:no current notif"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getUIContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getUIContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    return v0
.end method

.method public isExpandedWithStack()Z
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    return v0
.end method

.method public isShowOn()Z
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v0

    return v0
.end method

.method public isTtsOn()Z
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v0

    return v0
.end method

.method public moveNext(Z)V
    .locals 1
    .param p1, "gesture"    # Z

    .prologue
    .line 340
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->moveNext(Z)V

    .line 341
    return-void
.end method

.method public movePrevious(Z)V
    .locals 1
    .param p1, "gesture"    # Z

    .prologue
    .line 345
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->movePrevious(Z)V

    .line 346
    return-void
.end method

.method public resetTimeout()V
    .locals 2

    .prologue
    .line 187
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 188
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-eqz v0, :cond_0

    .line 189
    iget-object v1, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->resetTimeout()V

    .line 191
    :cond_0
    return-void
.end method

.method public startTimeout(I)V
    .locals 2
    .param p1, "timeout"    # I

    .prologue
    .line 170
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 171
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-eqz v0, :cond_0

    .line 172
    iget-object v1, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->startTimeout(I)V

    .line 174
    :cond_0
    return-void
.end method

.method public stopTimeout(Z)V
    .locals 3
    .param p1, "force"    # Z

    .prologue
    .line 178
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 179
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-eqz v0, :cond_0

    .line 180
    iget-object v1, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->stopTimeout(ZLjava/lang/Runnable;)V

    .line 183
    :cond_0
    return-void
.end method
