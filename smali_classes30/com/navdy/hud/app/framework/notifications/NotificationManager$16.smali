.class synthetic Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;
.super Ljava/lang/Object;
.source "NotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$ui$Screen:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2892
    invoke-static {}, Lcom/navdy/service/library/events/ui/Screen;->values()[Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    .line 2207
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->values()[Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_NEXT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_PREV:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->SELECT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->COLLAPSE_VIEW:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->UPDATE_INDICATOR:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->REMOVE_NOTIFICATION:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    .line 2120
    :goto_6
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->values()[Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 1580
    :goto_8
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    .line 1558
    :goto_c
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->values()[Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->LIMIT_REACHED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NOT_HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_10
    return-void

    :catch_0
    move-exception v0

    goto :goto_10

    :catch_1
    move-exception v0

    goto :goto_f

    :catch_2
    move-exception v0

    goto :goto_e

    :catch_3
    move-exception v0

    goto :goto_d

    .line 1580
    :catch_4
    move-exception v0

    goto :goto_c

    :catch_5
    move-exception v0

    goto :goto_b

    :catch_6
    move-exception v0

    goto :goto_a

    :catch_7
    move-exception v0

    goto :goto_9

    .line 2120
    :catch_8
    move-exception v0

    goto :goto_8

    :catch_9
    move-exception v0

    goto/16 :goto_7

    .line 2207
    :catch_a
    move-exception v0

    goto/16 :goto_6

    :catch_b
    move-exception v0

    goto/16 :goto_5

    :catch_c
    move-exception v0

    goto/16 :goto_4

    :catch_d
    move-exception v0

    goto/16 :goto_3

    :catch_e
    move-exception v0

    goto/16 :goto_2

    :catch_f
    move-exception v0

    goto/16 :goto_1

    .line 2892
    :catch_10
    move-exception v0

    goto/16 :goto_0
.end method
