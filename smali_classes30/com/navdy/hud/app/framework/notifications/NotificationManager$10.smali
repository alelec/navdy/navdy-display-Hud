.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$endAction:Ljava/lang/Runnable;

.field final synthetic val$large:Landroid/view/View;

.field final synthetic val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field final synthetic val$notifViewOut:Landroid/view/View;

.field final synthetic val$notifViewOutParent:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 1681
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOutParent:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$large:Landroid/view/View;

    iput-object p6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1684
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v1, :cond_0

    .line 1685
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 1686
    .local v0, "set":Landroid/animation/AnimatorSet;
    if-eqz v0, :cond_0

    .line 1687
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1688
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1693
    .end local v0    # "set":Landroid/animation/AnimatorSet;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1694
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    invoke-static {v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1695
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOutParent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1696
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "delete all small view removed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1697
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1698
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$notifViewOut:Landroid/view/View;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->cleanupView(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;)V

    .line 1702
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$large:Landroid/view/View;

    if-nez v1, :cond_2

    .line 1703
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1705
    :cond_2
    return-void
.end method
