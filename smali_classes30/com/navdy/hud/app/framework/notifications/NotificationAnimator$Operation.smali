.class public final enum Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
.super Ljava/lang/Enum;
.source "NotificationAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Operation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum COLLAPSE_VIEW:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum MOVE_NEXT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum MOVE_PREV:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum REMOVE_NOTIFICATION:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum SELECT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field public static final enum UPDATE_INDICATOR:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "MOVE_PREV"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_PREV:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 20
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "MOVE_NEXT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_NEXT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->SELECT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "COLLAPSE_VIEW"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->COLLAPSE_VIEW:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "UPDATE_INDICATOR"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->UPDATE_INDICATOR:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 24
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const-string v1, "REMOVE_NOTIFICATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->REMOVE_NOTIFICATION:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_PREV:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_NEXT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->SELECT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->COLLAPSE_VIEW:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->UPDATE_INDICATOR:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->REMOVE_NOTIFICATION:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    return-object v0
.end method
