.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->expandNotification(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;

.field final synthetic val$indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

.field final synthetic val$notificationObj:Lcom/navdy/hud/app/framework/notifications/INotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;Lcom/navdy/hud/app/framework/notifications/INotification;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->this$1:Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/INotification;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->val$indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 292
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "calling onExpandedNotificationEvent-expand"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 294
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->val$indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->this$1:Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2$1;->val$indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v1

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->displayScrollingIndicator(I)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;I)V

    .line 297
    return-void
.end method
