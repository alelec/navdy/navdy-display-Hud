.class public final Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "NotificationManager$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationManager;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationManager;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 28
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.framework.notifications.NotificationManager"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 29
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 38
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-class v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    .line 39
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 40
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iput-object v0, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    .line 60
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    return-void
.end method
