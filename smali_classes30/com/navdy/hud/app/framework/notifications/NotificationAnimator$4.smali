.class final Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;
.super Ljava/lang/Object;
.source "NotificationAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateExpandedViewOut(Lcom/navdy/hud/app/view/NotificationView;ILandroid/view/View;Landroid/view/View;Ljava/lang/Runnable;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$endAction:Ljava/lang/Runnable;

.field final synthetic val$expandedNotifView:Landroid/view/View;

.field final synthetic val$notificationObj:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field final synthetic val$notificationView:Lcom/navdy/hud/app/view/NotificationView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Lcom/navdy/hud/app/view/NotificationView;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$expandedNotifView:Landroid/view/View;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationView:Lcom/navdy/hud/app/view/NotificationView;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$expandedNotifView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setExpandedStack(Z)V

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v0, :cond_0

    .line 198
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "calling onExpandedNotificationEvent-collapse"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationObj:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 202
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$notificationView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/NotificationView;->showNextNotificationColor()V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$endAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 209
    :cond_1
    return-void
.end method
