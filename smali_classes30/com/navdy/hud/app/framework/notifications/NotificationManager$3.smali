.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$3;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$3;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I
    .locals 6
    .param p1, "lhs"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p2, "rhs"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 381
    iget v1, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    iget v2, p2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    sub-int v0, v1, v2

    .line 382
    .local v0, "diff":I
    if-eqz v0, :cond_0

    .line 386
    .end local v0    # "diff":I
    :goto_0
    return v0

    .restart local v0    # "diff":I
    :cond_0
    iget-wide v2, p2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->uniqueCounter:J

    iget-wide v4, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->uniqueCounter:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 378
    check-cast p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    check-cast p2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$3;->compare(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I

    move-result v0

    return v0
.end method
