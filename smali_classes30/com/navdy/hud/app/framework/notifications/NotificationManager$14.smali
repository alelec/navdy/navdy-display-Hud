.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "NotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleExpandedMove(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$color:I

.field final synthetic val$large:Landroid/view/View;

.field final synthetic val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field final synthetic val$previous:Z

.field final synthetic val$small:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 2755
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$small:Landroid/view/View;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$large:Landroid/view/View;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-boolean p5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$previous:Z

    iput p6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$color:I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2758
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$small:Landroid/view/View;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$large:Landroid/view/View;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$previous:Z

    iget v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;->val$color:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V

    .line 2759
    return-void
.end method
