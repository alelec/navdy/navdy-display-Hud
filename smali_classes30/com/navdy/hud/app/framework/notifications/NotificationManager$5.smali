.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPosChange(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    const/16 v1, 0x64

    .line 585
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->supportScroll()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    if-gtz p1, :cond_2

    .line 587
    const/4 p1, 0x1

    .line 591
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getCurrentItem()I

    move-result v0

    if-ne v0, p1, :cond_3

    .line 599
    :cond_1
    :goto_1
    return-void

    .line 588
    :cond_2
    if-le p1, v1, :cond_0

    .line 589
    const/16 p1, 0x64

    goto :goto_0

    .line 594
    :cond_3
    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    if-ne p1, v1, :cond_5

    .line 595
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->startScrollThresholdTimer()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    .line 597
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setCurrentItem(I)V

    goto :goto_1
.end method
