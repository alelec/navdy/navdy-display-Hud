.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedViewInternal(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$child:Landroid/view/View;

.field final synthetic val$parent:Landroid/view/ViewGroup;

.field final synthetic val$runnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 2487
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$parent:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$runnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideScrollingIndicator()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    .line 2492
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2493
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$parent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2496
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2497
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2498
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 2499
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$child:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 2502
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getMainPanelWidth()I

    move-result v0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getSidePanelWidth()I

    move-result v2

    sub-int v1, v0, v2

    .line 2504
    .local v1, "newX":I
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 2506
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/widget/FrameLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 2507
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->val$runnable:Ljava/lang/Runnable;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 2509
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 2510
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z
    invoke-static {v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 2511
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v7

    .line 2504
    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateExpandedViewOut(Lcom/navdy/hud/app/view/NotificationView;ILandroid/view/View;Landroid/view/View;Ljava/lang/Runnable;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZZ)V

    .line 2512
    return-void
.end method
