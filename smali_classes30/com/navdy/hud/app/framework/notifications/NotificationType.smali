.class public final enum Lcom/navdy/hud/app/framework/notifications/NotificationType;
.super Ljava/lang/Enum;
.source "NotificationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum BRIGHTNESS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum ETA_DELAY:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum FASTER_ROUTE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum GLYMPSE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum LOW_FUEL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum MUSIC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum PHONE_CALL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum PLACE_TYPE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum ROUTE_CALC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum SMS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum TRAFFIC_EVENT:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum TRAFFIC_JAM:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum VOICE_ASSIST:Lcom/navdy/hud/app/framework/notifications/NotificationType;

.field public static final enum VOICE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;


# instance fields
.field priority:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v3, 0x62

    const/4 v4, 0x1

    .line 11
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "GLYMPSE"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLYMPSE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 12
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "SMS"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->SMS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 15
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "PHONE_CALL"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PHONE_CALL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "ROUTE_CALC"

    invoke-direct {v0, v1, v7, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->ROUTE_CALC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 20
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "PLACE_TYPE_SEARCH"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PLACE_TYPE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "VOICE_SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "FASTER_ROUTE"

    const/4 v2, 0x6

    const/16 v3, 0x61

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->FASTER_ROUTE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "ETA_DELAY"

    const/4 v2, 0x7

    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->ETA_DELAY:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "TRAFFIC_EVENT"

    const/16 v2, 0x8

    const/16 v3, 0x5f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_EVENT:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "TRAFFIC_JAM"

    const/16 v2, 0x9

    const/16 v3, 0x5e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_JAM:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "LOW_FUEL"

    const/16 v2, 0xa

    const/16 v3, 0x61

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->LOW_FUEL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 34
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "GLANCE"

    const/16 v2, 0xb

    const/16 v3, 0x32

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 37
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "MUSIC"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->MUSIC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "VOICE_ASSIST"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_ASSIST:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 43
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    const-string v1, "BRIGHTNESS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->BRIGHTNESS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 9
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/navdy/hud/app/framework/notifications/NotificationType;

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLYMPSE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationType;->SMS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PHONE_CALL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationType;->ROUTE_CALC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PLACE_TYPE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->FASTER_ROUTE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->ETA_DELAY:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_EVENT:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_JAM:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->LOW_FUEL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->MUSIC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_ASSIST:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationType;->BRIGHTNESS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->priority:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/notifications/NotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method


# virtual methods
.method public getPriority()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->priority:I

    return v0
.end method
