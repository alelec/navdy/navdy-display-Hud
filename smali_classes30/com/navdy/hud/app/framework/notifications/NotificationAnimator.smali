.class public Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;
.super Ljava/lang/Object;
.source "NotificationAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;,
        Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    }
.end annotation


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static animateChildViewOut(Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 4
    .param p0, "child"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 166
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    .line 167
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 168
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 169
    invoke-virtual {v0, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 171
    return-void
.end method

.method static animateExpandedViewOut(Lcom/navdy/hud/app/view/NotificationView;ILandroid/view/View;Landroid/view/View;Ljava/lang/Runnable;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZZ)V
    .locals 4
    .param p0, "notificationView"    # Lcom/navdy/hud/app/view/NotificationView;
    .param p1, "newX"    # I
    .param p2, "expandedNotifView"    # Landroid/view/View;
    .param p3, "indicator"    # Landroid/view/View;
    .param p4, "endAction"    # Ljava/lang/Runnable;
    .param p5, "notificationObj"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p6, "showOn"    # Z
    .param p7, "isExpandNotificationViewVisible"    # Z

    .prologue
    .line 181
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$3;

    invoke-direct {v0, p3}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$3;-><init>(Landroid/view/View;)V

    .line 188
    .local v0, "runnable1":Ljava/lang/Runnable;
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;

    invoke-direct {v1, p2, p5, p0, p4}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$4;-><init>(Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Lcom/navdy/hud/app/view/NotificationView;Ljava/lang/Runnable;)V

    .line 212
    .local v1, "runnable2":Ljava/lang/Runnable;
    if-nez p6, :cond_0

    if-eqz p7, :cond_1

    .line 213
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/NotificationView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v3, p1

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 214
    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 215
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 220
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 218
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method static animateExpandedViews(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)V
    .locals 8
    .param p0, "viewIn"    # Landroid/view/View;
    .param p1, "viewOut"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "translationBy"    # I
    .param p4, "duration"    # I
    .param p5, "startDelay"    # I
    .param p6, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 85
    const/4 v7, 0x0

    .line 86
    .local v7, "out":Landroid/view/ViewPropertyAnimator;
    const/4 v6, 0x0

    .line 88
    .local v6, "in":Landroid/view/ViewPropertyAnimator;
    if-eqz p1, :cond_0

    .line 89
    const/4 v4, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateOut(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    .line 90
    const/4 p6, 0x0

    .line 93
    :cond_0
    if-eqz p0, :cond_1

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 94
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateIn(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 97
    :cond_1
    if-eqz v7, :cond_2

    .line 98
    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 101
    :cond_2
    if-eqz v6, :cond_3

    .line 102
    invoke-virtual {v6}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 104
    :cond_3
    return-void
.end method

.method private static animateIn(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "translationBy"    # I
    .param p3, "duration"    # I
    .param p4, "startDelay"    # I
    .param p5, "endAction"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 112
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 113
    .local v1, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    int-to-float v2, p2

    invoke-virtual {p0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 115
    invoke-virtual {p0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 117
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 119
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 120
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 121
    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-long v4, p3

    .line 122
    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-long v4, p4

    .line 123
    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$1;-><init>(Landroid/view/View;)V

    .line 124
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 131
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    if-eqz p5, :cond_0

    .line 132
    invoke-virtual {v0, p5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 135
    :cond_0
    return-object v0
.end method

.method static animateNotifViews(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)V
    .locals 8
    .param p0, "viewIn"    # Landroid/view/View;
    .param p1, "viewOut"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "translationBy"    # I
    .param p4, "duration"    # I
    .param p5, "startDelay"    # I
    .param p6, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    const/4 v7, 0x0

    .line 58
    .local v7, "out":Landroid/view/ViewPropertyAnimator;
    const/4 v6, 0x0

    .line 60
    .local v6, "in":Landroid/view/ViewPropertyAnimator;
    if-eqz p1, :cond_0

    .line 61
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateOut(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    .line 64
    :cond_0
    if-eqz p0, :cond_1

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 65
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateIn(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 68
    :cond_1
    if-eqz v7, :cond_2

    .line 69
    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 72
    :cond_2
    if-eqz v6, :cond_3

    .line 73
    invoke-virtual {v6}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 75
    :cond_3
    return-void
.end method

.method private static animateOut(Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "translationBy"    # I
    .param p3, "duration"    # I
    .param p4, "startDelay"    # I
    .param p5, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 144
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 145
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-int v2, p2

    int-to-float v2, v2

    .line 146
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p3

    .line 147
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p4

    .line 148
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$2;

    invoke-direct {v2, p1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$2;-><init>(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 149
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 156
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    if-eqz p5, :cond_0

    .line 157
    invoke-virtual {v0, p5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 160
    :cond_0
    return-object v0
.end method
