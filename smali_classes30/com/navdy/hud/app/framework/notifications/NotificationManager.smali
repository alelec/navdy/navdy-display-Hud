.class public final Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.super Ljava/lang/Object;
.source "NotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;,
        Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;,
        Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    }
.end annotation


# static fields
.field private static final EXPAND_VIEW_ANIMATION_DURATION:I = 0xfa

.field public static final EXPAND_VIEW_ANIMATION_START_DELAY_DURATION:I = 0xfa

.field private static final EXPAND_VIEW_ELEMENT_ANIMATION_DURATION:I = 0x64

.field private static final EXPAND_VIEW_QUICK_DURATION:I = 0x0

.field public static final EXTRA_VOICE_SEARCH_NOTIFICATION:Ljava/lang/String; = "extra_voice_search_notification"

.field private static final NOTIFICATION_CHANGE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;

.field private static final sInstance:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private NOTIFS_NOT_ALLOWED_ON_DISCONNECT:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private NOTIFS_REMOVED_ON_DISCONNECT:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field animationTranslation:I

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;",
            ">;"
        }
    .end annotation
.end field

.field private currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field private deleteAllGlances:Z

.field private disconnected:Z

.field private enable:Z

.field private enablePhoneCalls:Z

.field private expandedNotifCoverView:Landroid/view/View;

.field private expandedNotifView:Landroid/widget/FrameLayout;

.field private expandedWithStack:Z

.field private gestureEnabledSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

.field private handler:Landroid/os/Handler;

.field private ignoreScrollRunnable:Ljava/lang/Runnable;

.field private volatile isAnimating:Z

.field private volatile isCollapsed:Z

.field private volatile isExpanded:Z

.field private lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field private lockObj:Ljava/lang/Object;

.field private mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

.field private notifAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

.field private notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

.field private notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

.field private notifView:Lcom/navdy/hud/app/view/NotificationView;

.field private notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private notificationCounter:J

.field private notificationIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;",
            ">;"
        }
    .end annotation
.end field

.field private notificationPriorityMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private notificationSavedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;",
            ">;"
        }
    .end annotation
.end field

.field private notificationSavedListDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field private operationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private operationRunning:Z

.field pendingNotification:Lcom/navdy/hud/app/framework/notifications/INotification;

.field private pendingScreen:Lcom/navdy/service/library/events/ui/Screen;

.field private pendingScreenArgs:Landroid/os/Bundle;

.field private pendingScreenArgs2:Ljava/lang/Object;

.field private pendingShutdownArgs:Landroid/os/Bundle;

.field private resources:Landroid/content/res/Resources;

.field private scrollProgress:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

.field private scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

.field private showOn:Z

.field private stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field private stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

.field private ttsOn:Z

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 107
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFICATION_CHANGE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;

    .line 109
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sInstance:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    .line 139
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    .line 143
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enable:Z

    .line 145
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneCalls:Z

    .line 154
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 156
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$1;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ignoreScrollRunnable:Ljava/lang/Runnable;

    .line 165
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->resources:Landroid/content/res/Resources;

    .line 167
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$2;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 375
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_REMOVED_ON_DISCONNECT:Ljava/util/HashSet;

    .line 376
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_NOT_ALLOWED_ON_DISCONNECT:Ljava/util/HashSet;

    .line 378
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$3;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->comparator:Ljava/util/Comparator;

    .line 391
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    .line 392
    new-instance v1, Ljava/util/TreeMap;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->comparator:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    .line 393
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    .line 398
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    .line 399
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->disconnected:Z

    .line 404
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;

    .line 409
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$4;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .line 582
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$5;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollProgress:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    .line 603
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 605
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V

    .line 607
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_REMOVED_ON_DISCONNECT:Ljava/util/HashSet;

    const-string v2, "navdy#phone#call#notif"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 608
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_REMOVED_ON_DISCONNECT:Ljava/util/HashSet;

    const-string v2, "navdy#music#notif"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 610
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_NOT_ALLOWED_ON_DISCONNECT:Ljava/util/HashSet;

    const-string v2, "navdy#phone#call#notif"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 611
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_NOT_ALLOWED_ON_DISCONNECT:Ljava/util/HashSet;

    const-string v2, "navdy#music#notif"

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 613
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 614
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b009a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animationTranslation:I

    .line 616
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 617
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isPhoneNotifPresentAndNotAlive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndex(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->displayScrollingIndicator(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotificationInternal(ZZZ)V

    return-void
.end method

.method static synthetic access$1802(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCollapsed:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/device/light/LED$Settings;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->gestureEnabledSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/device/light/LED$Settings;)Lcom/navdy/hud/app/device/light/LED$Settings;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/light/LED$Settings;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->gestureEnabledSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeAllNotification()V

    return-void
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/view/NotificationView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->cleanupViews(Lcom/navdy/hud/app/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p2, "x2"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotificationfromStack(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification(Z)V

    return-void
.end method

.method static synthetic access$2800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/service/library/events/ui/Screen;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/Screen;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isScreenHighPriority(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->startScrollThresholdTimer()V

    return-void
.end method

.method static synthetic access$3800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/INotification;
    .param p2, "x2"    # Ljava/util/EnumSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotificationInternal(Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    return-void
.end method

.method static synthetic access$4200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->cleanupView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z
    .param p6, "x6"    # Ljava/lang/String;
    .param p7, "x7"    # Z

    .prologue
    .line 74
    invoke-direct/range {p0 .. p7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$4400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotificationInternal()V

    return-void
.end method

.method static synthetic access$4500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideScrollingIndicator()V

    return-void
.end method

.method static synthetic access$4600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/graphics/RectF;Lcom/navdy/hud/app/framework/notifications/IScrollEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Landroid/graphics/RectF;
    .param p2, "x2"    # Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotifScrollIndicatorXY(Landroid/graphics/RectF;Lcom/navdy/hud/app/framework/notifications/IScrollEvent;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationCoverView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z

    return v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ttsOn:Z

    return p1
.end method

.method private addNotificationInternal(Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V
    .locals 12
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/notifications/INotification;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 654
    .local p2, "allowedScreen":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/navdy/service/library/events/ui/Screen;>;"
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v6, :cond_0

    .line 655
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 656
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v6, :cond_0

    .line 848
    :goto_0
    return-void

    .line 661
    :cond_0
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 662
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 663
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notification id is null"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 666
    :cond_1
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v5

    .line 667
    .local v5, "type":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    if-nez v5, :cond_2

    .line 668
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "notification type is null"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 672
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addNotif type["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] id["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 673
    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v7

    .line 674
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationViewShowing()Z

    move-result v6

    if-nez v6, :cond_3

    .line 675
    sget-object v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif current-notification marked null:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v6

    :goto_1
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 676
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 680
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v6, :cond_7

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->canAddToStackIfCurrentExists()Z

    move-result v6

    if-nez v6, :cond_7

    .line 681
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 682
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v6, :cond_5

    .line 683
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif updated"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 684
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->onUpdate()V

    .line 691
    :goto_2
    monitor-exit v7

    goto/16 :goto_0

    .line 847
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 675
    :cond_4
    :try_start_1
    const-string v6, "unk"

    goto :goto_1

    .line 686
    :cond_5
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif cannot be added current rule-1"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 689
    :cond_6
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif cannot be added current rule-2"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 695
    :cond_7
    if-eqz p2, :cond_c

    .line 696
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v6, :cond_8

    .line 697
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif screen constraint failed, current notification active"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 698
    monitor-exit v7

    goto/16 :goto_0

    .line 700
    :cond_8
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 701
    .local v0, "currentScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 702
    :cond_9
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif screen constraint failed, current screen:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 703
    monitor-exit v7

    goto/16 :goto_0

    .line 705
    :cond_a
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 706
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationCollapsing()Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 707
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 708
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/activity/Main;->isScreenAnimating()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 709
    :cond_b
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif screen constraint failed, mainscreen animating"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 710
    monitor-exit v7

    goto/16 :goto_0

    .line 715
    .end local v0    # "currentScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    :cond_c
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v6, :cond_10

    .line 716
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 718
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v6, :cond_e

    .line 719
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif notif was marked to remove,resurrecting it back"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 720
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x0

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 721
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    .line 731
    :cond_d
    :goto_3
    monitor-exit v7

    goto/16 :goto_0

    .line 722
    :cond_e
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v6, :cond_f

    .line 723
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif already on screen,update"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 724
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onUpdate()V

    goto :goto_3

    .line 726
    :cond_f
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif not on screen yet"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 727
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v6

    if-nez v6, :cond_d

    .line 728
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    goto :goto_3

    .line 736
    :cond_10
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 737
    .local v3, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-eqz v3, :cond_12

    .line 739
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v6, :cond_11

    .line 740
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif already exist on stack, making it current"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 741
    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 742
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    .line 746
    :goto_4
    monitor-exit v7

    goto/16 :goto_0

    .line 744
    :cond_11
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif already exist on stack, no-op"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_4

    .line 750
    :cond_12
    new-instance v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .end local v3    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-direct {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;-><init>()V

    .line 751
    .restart local v3    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    iput-object p1, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 752
    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationType;->getPriority()I

    move-result v6

    iput v6, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    .line 753
    iget-wide v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationCounter:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationCounter:J

    iput-wide v8, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->uniqueCounter:J

    .line 754
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v6, v3, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 757
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif added to stack"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 760
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v6, :cond_15

    .line 761
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif make current ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 762
    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 763
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 765
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    .line 766
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    .line 845
    :cond_13
    :goto_5
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->postNotificationChange()V

    .line 846
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator()V

    .line 847
    monitor-exit v7

    goto/16 :goto_0

    .line 768
    :cond_14
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    move-result v6

    if-nez v6, :cond_13

    .line 769
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif not making current, disabled"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    .line 771
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    goto :goto_5

    .line 777
    :cond_15
    iget v6, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget v8, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    if-le v6, v8, :cond_17

    const/4 v1, 0x1

    .line 778
    .local v1, "highPriority":Z
    :goto_6
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->isPurgeable()Z

    move-result v4

    .line 779
    .local v4, "purgeable":Z
    if-nez v1, :cond_16

    if-eqz v4, :cond_21

    .line 780
    :cond_16
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif high priority["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] purgeable["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 782
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v6, :cond_1c

    .line 783
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v8}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 784
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif stage notif already added"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 785
    monitor-exit v7

    goto/16 :goto_0

    .line 777
    .end local v1    # "highPriority":Z
    .end local v4    # "purgeable":Z
    :cond_17
    const/4 v1, 0x0

    goto :goto_6

    .line 787
    .restart local v1    # "highPriority":Z
    .restart local v4    # "purgeable":Z
    :cond_18
    iget v6, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget v8, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    if-le v6, v8, :cond_1b

    .line 788
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif replacing staged notif since it high priority than staged ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v9, v9, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v9}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 789
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    .line 790
    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 791
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v6, :cond_1a

    .line 792
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-nez v6, :cond_19

    .line 793
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif-1 showNotif"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 794
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    goto/16 :goto_5

    .line 796
    :cond_19
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif-1 hideNotif"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 797
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    .line 798
    monitor-exit v7

    goto/16 :goto_0

    .line 801
    :cond_1a
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif anim in progress"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 804
    :cond_1b
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif not replacing staged notif staged ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v9, v9, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v9}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 805
    monitor-exit v7

    goto/16 :goto_0

    .line 808
    :cond_1c
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->isAlive()Z

    move-result v6

    if-nez v6, :cond_1d

    .line 809
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 810
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif current notification not alive anymore ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v9, v9, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v9}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 814
    :goto_7
    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 815
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v6, :cond_20

    .line 816
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-nez v6, :cond_1f

    .line 817
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-nez v6, :cond_1e

    .line 818
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif swapping notif"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 819
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 820
    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 824
    :goto_8
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    goto/16 :goto_5

    .line 812
    :cond_1d
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    goto :goto_7

    .line 822
    :cond_1e
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif showNotif"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_8

    .line 826
    :cond_1f
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif hideNotif"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 827
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    goto/16 :goto_5

    .line 830
    :cond_20
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addNotif anim in progress"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 835
    :cond_21
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addNotif lower priority than current["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v9, v9, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v9}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] update color"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 836
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    .line 837
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 839
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v6, :cond_13

    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-nez v6, :cond_13

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationViewShowing()Z

    move-result v6

    if-nez v6, :cond_13

    .line 840
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "add notif lower priority, but view hidden"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 841
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_5
.end method

.method private animateOutExpandedView(ZLcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V
    .locals 8
    .param p1, "hideNotification"    # Z
    .param p2, "notificationObj"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p3, "quick"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1339
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1340
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->COLLAPSE_VIEW:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v6, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p3

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 1342
    :cond_1
    return-void
.end method

.method private animateOutExpandedView(ZZ)V
    .locals 1
    .param p1, "hideNotification"    # Z
    .param p2, "quick"    # Z

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-direct {p0, p1, v0, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animateOutExpandedView(ZLcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 1336
    return-void
.end method

.method private cleanupView(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f0e000f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 2114
    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    .line 2116
    .local v6, "viewType":Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    invoke-virtual {p1, v7, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2117
    const v7, 0x7f0e000e

    invoke-virtual {p1, v7, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2119
    if-eqz v6, :cond_0

    .line 2120
    sget-object v7, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2157
    :cond_0
    :goto_0
    return-void

    .line 2122
    :pswitch_0
    const v7, 0x7f0e010f

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2123
    .local v1, "image":Landroid/widget/ImageView;
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2124
    sget-object v7, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v7, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    goto :goto_0

    .line 2128
    .end local v1    # "image":Landroid/widget/ImageView;
    :pswitch_1
    const v7, 0x7f0e00cc

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2129
    .local v3, "mainTitle":Landroid/widget/TextView;
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2130
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2132
    const v7, 0x7f0e0111

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 2133
    .local v5, "subTitle":Landroid/widget/TextView;
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2134
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2136
    const v7, 0x7f0e00d2

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2137
    .local v0, "choiceLayout":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 2138
    invoke-virtual {v0, v11}, Landroid/view/View;->setAlpha(F)V

    .line 2139
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2140
    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2141
    const v7, 0x7f0e0050

    invoke-virtual {v0, v7, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2142
    const v7, 0x7f0e0051

    invoke-virtual {v0, v7, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2145
    :cond_1
    const v7, 0x7f0e0112

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 2146
    .local v2, "mainImage":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    sget-object v7, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v10, v9, v7}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 2147
    invoke-virtual {v2, v9}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 2149
    const v7, 0x7f0e00d1

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 2150
    .local v4, "sideImage":Landroid/widget/ImageView;
    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2152
    invoke-virtual {p1, v11}, Landroid/view/View;->setAlpha(F)V

    .line 2153
    sget-object v7, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v7, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    goto :goto_0

    .line 2120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private cleanupViews(Lcom/navdy/hud/app/view/NotificationView;)V
    .locals 10
    .param p1, "view"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1885
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "*** cleaning up expanded view"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1886
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    .line 1887
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v3

    .line 1888
    .local v3, "parent":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v0

    .line 1890
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1891
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1893
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1894
    invoke-virtual {v0, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1895
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1896
    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v5, v0}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 1899
    :cond_0
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "*** removed expanded view"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1902
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/view/NotificationView;->getNotificationContainer()Landroid/view/ViewGroup;

    move-result-object v4

    .line 1903
    .local v4, "parentNotif":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-lez v5, :cond_3

    .line 1904
    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1905
    .local v1, "childNotif":Landroid/view/View;
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1907
    invoke-static {v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1908
    invoke-virtual {v1, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1909
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->cleanupView(Landroid/view/View;)V

    .line 1912
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "*** removed notif view"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1915
    .end local v1    # "childNotif":Landroid/view/View;
    :cond_3
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v5, :cond_4

    .line 1916
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-boolean v8, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 1917
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 1921
    :cond_4
    iput-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1922
    iput-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1923
    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1925
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v2

    .line 1926
    .local v2, "indicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    if-eqz v2, :cond_5

    .line 1927
    invoke-virtual {v2, v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setVisibility(I)V

    .line 1928
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideScrollingIndicator()V

    .line 1930
    :cond_5
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 1931
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/NotificationView;->showNextNotificationColor()V

    .line 1932
    return-void
.end method

.method private clearAllGlances()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2865
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances turned off"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2866
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2867
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances turned off: expanded mode"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2868
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    .line 2869
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2870
    invoke-virtual {p0, v2, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 2881
    :goto_0
    return-void

    .line 2871
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationViewShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2872
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances turned off: notif showing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2873
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    .line 2874
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotification()Z

    goto :goto_0

    .line 2876
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances turned off: notif not showing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2877
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 2878
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeAllNotification()V

    .line 2879
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private collapseExpandedViewInternal(ZZ)V
    .locals 6
    .param p1, "hideNotification"    # Z
    .param p2, "quick"    # Z

    .prologue
    .line 2411
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "collapseExpandedViewInternal"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2412
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    .line 2413
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2415
    new-instance v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)V

    .line 2484
    .local v3, "runnable":Ljava/lang/Runnable;
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v2

    .line 2485
    .local v2, "parent":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v0

    .line 2487
    .local v0, "child":Landroid/view/View;
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$13;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Runnable;)V

    .line 2515
    .local v1, "expandOutRunnable":Ljava/lang/Runnable;
    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    if-eqz v0, :cond_2

    .line 2516
    if-eqz p2, :cond_1

    const/4 v4, 0x0

    :goto_0
    invoke-static {v0, v4, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateChildViewOut(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 2523
    :goto_1
    return-void

    .line 2516
    :cond_1
    const/16 v4, 0xfa

    goto :goto_0

    .line 2521
    :cond_2
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_1
.end method

.method private collapseNotificationInternal(ZZZ)V
    .locals 3
    .param p1, "completely"    # Z
    .param p2, "quickly"    # Z
    .param p3, "internal"    # Z

    .prologue
    .line 2086
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "collapseNotificationInternal"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2087
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 2088
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2089
    :cond_0
    if-nez p3, :cond_2

    .line 2090
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-direct {p0, p1, v1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animateOutExpandedView(ZLcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 2095
    :cond_1
    :goto_0
    return-void

    .line 2092
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedViewInternal(ZZ)V

    goto :goto_0
.end method

.method private deleteAllGlances()V
    .locals 3

    .prologue
    .line 1781
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1782
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-eqz v0, :cond_0

    .line 1783
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animateOutExpandedView(ZZ)V

    .line 1785
    :cond_0
    monitor-exit v1

    .line 1786
    return-void

    .line 1785
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private displayScrollingIndicator(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 2784
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v1, :cond_0

    .line 2785
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->supportScroll()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2786
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    .line 2787
    .local v0, "scroll":Lcom/navdy/hud/app/framework/notifications/IScrollEvent;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setBackgroundColor(I)V

    .line 2788
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;

    invoke-direct {v2, p0, p1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;ILcom/navdy/hud/app/framework/notifications/IScrollEvent;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2805
    .end local v0    # "scroll":Lcom/navdy/hud/app/framework/notifications/IScrollEvent;
    :cond_0
    :goto_0
    return-void

    .line 2802
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideScrollingIndicator()V

    goto :goto_0
.end method

.method private executeItemInternal()V
    .locals 5

    .prologue
    .line 2353
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    .line 2354
    .local v0, "verbose":Z
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2355
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_2

    .line 2356
    if-eqz v0, :cond_0

    .line 2357
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "executeItemInternal: expanded :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2359
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2360
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v3, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    .line 2408
    :cond_1
    :goto_0
    return-void

    .line 2365
    :cond_2
    if-eqz v0, :cond_3

    .line 2366
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "executeItemInternal: expanded check for delete:"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2369
    :cond_3
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v2, :cond_4

    .line 2370
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v1

    .line 2375
    .local v1, "view":Landroid/view/View;
    :goto_1
    invoke-static {v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2376
    const v2, 0x7f0e000e

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2378
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "executeItemInternal: no glance to delete"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2379
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto :goto_0

    .line 2372
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->getCurrentNotificationViewChild()Landroid/view/View;

    move-result-object v1

    .restart local v1    # "view":Landroid/view/View;
    goto :goto_1

    .line 2383
    :cond_5
    if-eqz v0, :cond_6

    .line 2384
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "executeItemInternal: delete all"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2386
    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    .line 2387
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2388
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->showGlancesDeletedToast()V

    .line 2389
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "executeItemInternal: show delete all toast"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 2393
    .end local v1    # "view":Landroid/view/View;
    :cond_7
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2394
    if-eqz v0, :cond_8

    .line 2395
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "executeItemInternal: expandedNoStack :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2399
    :cond_8
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_9

    .line 2400
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2401
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v3, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    goto :goto_0

    .line 2403
    :cond_9
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_0
.end method

.method private getExpandedNotificationCoverView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifCoverView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifCoverView:Landroid/view/View;

    .line 1105
    :goto_0
    return-object v0

    .line 1099
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v0, :cond_1

    .line 1100
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getMainScreen()Lcom/navdy/hud/app/ui/activity/Main;

    .line 1102
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_2

    .line 1103
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->getExpandedNotificationCoverView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifCoverView:Landroid/view/View;

    .line 1105
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifCoverView:Landroid/view/View;

    goto :goto_0
.end method

.method private getExpandedNotificationView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    .line 1092
    :goto_0
    return-object v0

    .line 1086
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v0, :cond_1

    .line 1087
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getMainScreen()Lcom/navdy/hud/app/ui/activity/Main;

    .line 1089
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_2

    .line 1090
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    .line 1092
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method private getHigherNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 2
    .param p1, "info"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 1936
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1937
    .local v0, "higher":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-nez v0, :cond_1

    .line 1938
    const/4 v0, 0x0

    .line 1946
    .end local v0    # "higher":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_0
    return-object v0

    .line 1940
    .restart local v0    # "higher":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_1
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    instance-of v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    if-eqz v1, :cond_0

    .line 1941
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1942
    move-object p1, v0

    .line 1943
    goto :goto_0
.end method

.method private getHighestNotification()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 2

    .prologue
    .line 1951
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1952
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    monitor-exit v1

    return-object v0

    .line 1953
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sInstance:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    return-object v0
.end method

.method private getLowerNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 2
    .param p1, "info"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 1964
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1965
    .local v0, "lower":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-nez v0, :cond_1

    .line 1966
    const/4 v0, 0x0

    .line 1974
    .end local v0    # "lower":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_0
    return-object v0

    .line 1968
    .restart local v0    # "lower":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_1
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    instance-of v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    if-eqz v1, :cond_0

    .line 1969
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1970
    move-object p1, v0

    .line 1971
    goto :goto_0
.end method

.method private getLowestNotification()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 2

    .prologue
    .line 1957
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1958
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    monitor-exit v1

    return-object v0

    .line 1959
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getMainScreen()Lcom/navdy/hud/app/ui/activity/Main;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 1066
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    return-object v0
.end method

.method private getNonRemovableNotifCount()I
    .locals 2

    .prologue
    .line 1994
    const/4 v0, 0x0

    .line 1995
    .local v0, "count":I
    const-string v1, "navdy#phone#call#notif"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1996
    add-int/lit8 v0, v0, 0x1

    .line 1999
    :cond_0
    const-string v1, "navdy#traffic#reroute#notif"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2000
    add-int/lit8 v0, v0, 0x1

    .line 2003
    :cond_1
    return v0
.end method

.method private getNotificationByIndex(I)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 1849
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 1850
    if-ltz p1, :cond_0

    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v5

    if-lt p1, v5, :cond_1

    .line 1851
    :cond_0
    monitor-exit v4

    move-object v1, v3

    .line 1863
    :goto_0
    return-object v1

    .line 1854
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1855
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    const/4 v0, 0x0

    .line 1856
    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1857
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1858
    .local v1, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-ne v0, p1, :cond_2

    .line 1859
    monitor-exit v4

    goto :goto_0

    .line 1864
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1861
    .restart local v0    # "i":I
    .restart local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 1862
    goto :goto_1

    .line 1863
    .end local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_3
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v3

    goto :goto_0
.end method

.method private getNotificationFromId(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1979
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1980
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    monitor-exit v1

    return-object v0

    .line 1981
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getNotificationIndex(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I
    .locals 4
    .param p1, "info"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .prologue
    .line 1835
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 1836
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1837
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    const/4 v0, -0x1

    .line 1838
    .local v0, "index":I
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1839
    add-int/lit8 v0, v0, 0x1

    .line 1840
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1841
    monitor-exit v3

    move v2, v0

    .line 1844
    :goto_0
    return v2

    :cond_1
    const/4 v2, -0x1

    monitor-exit v3

    goto :goto_0

    .line 1845
    .end local v0    # "index":I
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 1119
    :goto_0
    return-object v0

    .line 1112
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v0, :cond_1

    .line 1113
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getMainScreen()Lcom/navdy/hud/app/ui/activity/Main;

    .line 1115
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_2

    .line 1116
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 1117
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .line 1119
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    goto :goto_0
.end method

.method private getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    if-eqz v0, :cond_0

    .line 1071
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    .line 1079
    :goto_0
    return-object v0

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v0, :cond_1

    .line 1074
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getMainScreen()Lcom/navdy/hud/app/ui/activity/Main;

    .line 1076
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_2

    .line 1077
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    .line 1079
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    goto :goto_0
.end method

.method private handleExpandedMove(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V
    .locals 34
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    .param p2, "gesture"    # Z

    .prologue
    .line 2526
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "handleExpandedMove key:"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v30, " gesture:"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2527
    const/16 v18, 0x0

    .line 2528
    .local v18, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    const/16 v21, 0x0

    .line 2529
    .local v21, "prev":Z
    const/4 v15, 0x0

    .line 2530
    .local v15, "deleteAll":Z
    const/16 v23, 0x0

    .line 2532
    .local v23, "runQueuedOperation":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v14

    .line 2533
    .local v14, "currentItem":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v17

    .line 2536
    .local v17, "indicatorCount":I
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2664
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v28

    .line 2665
    .local v28, "uiContext":Landroid/content/Context;
    move/from16 v9, v21

    .line 2667
    .local v9, "previous":Z
    if-nez v18, :cond_0

    if-eqz v15, :cond_1f

    .line 2669
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    move-object/from16 v30, v0

    monitor-enter v30
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2670
    move-object/from16 v8, v18

    .line 2677
    .local v8, "notifInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-eqz v18, :cond_17

    .line 2679
    :try_start_1
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2680
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "handleExpandedMove: going to "

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2682
    :cond_1
    iget-object v4, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v24

    .line 2684
    .local v24, "s":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_16

    .line 2685
    iget-object v4, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    const/4 v5, 0x0

    move-object/from16 v0, v28

    invoke-interface {v4, v0, v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v19

    .line 2690
    .local v19, "l":Landroid/view/View;
    :goto_1
    const/4 v4, 0x1

    iput-boolean v4, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 2691
    iget-object v4, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v4, v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    .line 2692
    iget-object v4, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v11

    .line 2747
    .local v11, "c":I
    :goto_2
    move-object/from16 v6, v24

    .line 2748
    .local v6, "small":Landroid/view/View;
    move-object/from16 v7, v19

    .line 2749
    .local v7, "large":Landroid/view/View;
    move v10, v11

    .line 2752
    .local v10, "color":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v4, :cond_1e

    .line 2753
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;

    move-result-object v25

    .line 2754
    .local v25, "set":Landroid/animation/AnimatorSet;
    if-eqz v25, :cond_1d

    .line 2755
    new-instance v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2761
    const-wide/16 v4, 0x64

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2762
    invoke-virtual/range {v25 .. v25}, Landroid/animation/AnimatorSet;->start()V

    .line 2769
    .end local v25    # "set":Landroid/animation/AnimatorSet;
    :goto_3
    monitor-exit v30
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 2777
    .end local v6    # "small":Landroid/view/View;
    .end local v7    # "large":Landroid/view/View;
    .end local v8    # "notifInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v10    # "color":I
    .end local v11    # "c":I
    .end local v19    # "l":Landroid/view/View;
    .end local v24    # "s":Landroid/view/View;
    :goto_4
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    .line 2781
    .end local v9    # "previous":Z
    .end local v28    # "uiContext":Landroid/content/Context;
    :cond_2
    :goto_5
    return-void

    .line 2540
    :pswitch_0
    if-nez v14, :cond_4

    .line 2541
    const/16 v23, 0x1

    .line 2542
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2543
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleExpandedMove: cannot go up"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2777
    :cond_3
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto :goto_5

    .line 2548
    :cond_4
    const/4 v4, 0x1

    if-ne v14, v4, :cond_5

    .line 2550
    const/4 v15, 0x1

    .line 2577
    :goto_6
    const/16 v21, 0x1

    .line 2578
    goto/16 :goto_0

    .line 2552
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2553
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v4, :cond_8

    .line 2554
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getHigherNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v18

    .line 2555
    if-nez v18, :cond_6

    .line 2557
    const/4 v15, 0x1

    .line 2558
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/16 v30, 0x2

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2559
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "handleExpandedMove: cannot go up, no notification left"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2574
    :cond_6
    :goto_7
    monitor-exit v5

    goto :goto_6

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2777
    :catchall_1
    move-exception v4

    if-eqz v23, :cond_7

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    :cond_7
    throw v4

    .line 2563
    :cond_8
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-object/from16 v18, v0

    .line 2564
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2566
    if-nez v18, :cond_6

    .line 2567
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "handleExpandedMove: no prev notification"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2568
    add-int/lit8 v4, v17, -0x1

    if-ne v14, v4, :cond_6

    .line 2569
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getLowestNotification()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v18

    .line 2570
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "handleExpandedMove: got lowest:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_7

    .line 2582
    :pswitch_1
    add-int/lit8 v4, v17, -0x1

    if-ne v14, v4, :cond_e

    .line 2584
    if-eqz p2, :cond_c

    .line 2586
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_a

    .line 2587
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v29

    .line 2591
    .local v29, "view":Landroid/view/View;
    :goto_8
    const v4, 0x7f0e000e

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 2593
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no glance to delete:right"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2594
    const/16 v23, 0x1

    .line 2777
    .end local v29    # "view":Landroid/view/View;
    :cond_9
    :goto_9
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_5

    .line 2589
    :cond_a
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/NotificationView;->getCurrentNotificationViewChild()Landroid/view/View;

    move-result-object v29

    .restart local v29    # "view":Landroid/view/View;
    goto :goto_8

    .line 2596
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    .line 2597
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2598
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->showGlancesDeletedToast()V

    .line 2599
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2600
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleExpandedMove: delete all glances"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_9

    .line 2604
    .end local v29    # "view":Landroid/view/View;
    :cond_c
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2605
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleExpandedMove: cannot go down"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2607
    :cond_d
    const/16 v23, 0x1

    goto :goto_9

    .line 2612
    :cond_e
    add-int/lit8 v4, v17, -0x2

    if-ne v14, v4, :cond_f

    .line 2614
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 2616
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2617
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v4, :cond_11

    .line 2618
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getLowerNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v18

    .line 2631
    :cond_10
    :goto_a
    monitor-exit v5

    goto/16 :goto_0

    :catchall_2
    move-exception v4

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 2620
    :cond_11
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-object/from16 v18, v0

    .line 2621
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2623
    if-nez v18, :cond_10

    .line 2624
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "handleExpandedMove: no next notification"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2625
    if-nez v14, :cond_10

    .line 2626
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getHighestNotification()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v18

    .line 2627
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "handleExpandedMove: got highest:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_a

    .line 2636
    :pswitch_2
    :try_start_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v4, :cond_12

    .line 2637
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 2777
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_5

    .line 2643
    :cond_12
    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_13

    .line 2644
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v29

    .line 2648
    .restart local v29    # "view":Landroid/view/View;
    :goto_b
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2649
    const v4, 0x7f0e000e

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_14

    .line 2651
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no glance to delete:select"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 2652
    const/16 v23, 0x1

    .line 2777
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_5

    .line 2646
    .end local v29    # "view":Landroid/view/View;
    :cond_13
    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/NotificationView;->getCurrentNotificationViewChild()Landroid/view/View;

    move-result-object v29

    .restart local v29    # "view":Landroid/view/View;
    goto :goto_b

    .line 2656
    :cond_14
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    .line 2657
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->showGlancesDeletedToast()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 2777
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_5

    .line 2777
    :cond_15
    if-eqz v23, :cond_2

    .line 2778
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    goto/16 :goto_5

    .line 2687
    .end local v29    # "view":Landroid/view/View;
    .restart local v8    # "notifInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .restart local v9    # "previous":Z
    .restart local v24    # "s":Landroid/view/View;
    .restart local v28    # "uiContext":Landroid/content/Context;
    :cond_16
    const/16 v19, 0x0

    .restart local v19    # "l":Landroid/view/View;
    goto/16 :goto_1

    .line 2695
    .end local v19    # "l":Landroid/view/View;
    .end local v24    # "s":Landroid/view/View;
    :cond_17
    :try_start_f
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 2696
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleExpandedMove: going to delete all"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2699
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_1a

    .line 2700
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v24

    .line 2701
    .restart local v24    # "s":Landroid/view/View;
    const v4, 0x7f0e000f

    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2702
    const v4, 0x7f0e010f

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 2732
    .local v16, "imageView":Landroid/widget/ImageView;
    :cond_19
    :goto_c
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->DELETE_ALL_VIEW_TAG:Ljava/lang/Object;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2733
    const v4, 0x7f020107

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2735
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_1c

    .line 2736
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v19

    .line 2737
    .restart local v19    # "l":Landroid/view/View;
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->DELETE_ALL_VIEW_TAG:Ljava/lang/Object;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2738
    const v4, 0x7f0e010d

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 2739
    .local v27, "textView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateDeleteAllGlanceCount(Landroid/widget/TextView;Landroid/view/View;)V

    .line 2744
    .end local v27    # "textView":Landroid/widget/TextView;
    :goto_d
    sget v11, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorDeleteAll:I

    .restart local v11    # "c":I
    goto/16 :goto_2

    .line 2704
    .end local v11    # "c":I
    .end local v16    # "imageView":Landroid/widget/ImageView;
    .end local v19    # "l":Landroid/view/View;
    .end local v24    # "s":Landroid/view/View;
    :cond_1a
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v24

    .line 2705
    .restart local v24    # "s":Landroid/view/View;
    const v4, 0x7f0e000f

    sget-object v5, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2706
    const v4, 0x7f0e0112

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 2708
    .restart local v16    # "imageView":Landroid/widget/ImageView;
    const v4, 0x7f0e00cc

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 2709
    .local v20, "mainTitle":Landroid/widget/TextView;
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->glancesDismissAll:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2710
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2711
    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2713
    const v4, 0x7f0e0111

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 2714
    .local v26, "subTitle":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v13

    .line 2715
    .local v13, "count":I
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNonRemovableNotifCount()I

    move-result v22

    .line 2716
    .local v22, "removeCount":I
    sub-int v13, v13, v22

    .line 2717
    if-nez v13, :cond_1b

    .line 2718
    const v4, 0x7f0e000e

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2722
    :goto_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0900af

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    move-object/from16 v0, v31

    invoke-virtual {v4, v5, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2723
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2724
    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2726
    const v4, 0x7f0e00d2

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 2727
    .local v12, "choiceLayout":Landroid/view/View;
    if-eqz v12, :cond_19

    .line 2728
    const/4 v4, 0x4

    invoke-virtual {v12, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_c

    .line 2769
    .end local v12    # "choiceLayout":Landroid/view/View;
    .end local v13    # "count":I
    .end local v16    # "imageView":Landroid/widget/ImageView;
    .end local v20    # "mainTitle":Landroid/widget/TextView;
    .end local v22    # "removeCount":I
    .end local v24    # "s":Landroid/view/View;
    .end local v26    # "subTitle":Landroid/widget/TextView;
    :catchall_3
    move-exception v4

    monitor-exit v30
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    throw v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 2720
    .restart local v13    # "count":I
    .restart local v16    # "imageView":Landroid/widget/ImageView;
    .restart local v20    # "mainTitle":Landroid/widget/TextView;
    .restart local v22    # "removeCount":I
    .restart local v24    # "s":Landroid/view/View;
    .restart local v26    # "subTitle":Landroid/widget/TextView;
    :cond_1b
    const v4, 0x7f0e000e

    const/4 v5, 0x0

    :try_start_11
    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_e

    .line 2741
    .end local v13    # "count":I
    .end local v20    # "mainTitle":Landroid/widget/TextView;
    .end local v22    # "removeCount":I
    .end local v26    # "subTitle":Landroid/widget/TextView;
    :cond_1c
    const/16 v19, 0x0

    .restart local v19    # "l":Landroid/view/View;
    goto/16 :goto_d

    .end local v16    # "imageView":Landroid/widget/ImageView;
    .restart local v6    # "small":Landroid/view/View;
    .restart local v7    # "large":Landroid/view/View;
    .restart local v10    # "color":I
    .restart local v11    # "c":I
    .restart local v25    # "set":Landroid/animation/AnimatorSet;
    :cond_1d
    move-object/from16 v5, p0

    .line 2764
    invoke-virtual/range {v5 .. v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V

    goto/16 :goto_3

    .end local v25    # "set":Landroid/animation/AnimatorSet;
    :cond_1e
    move-object/from16 v5, p0

    .line 2767
    invoke-virtual/range {v5 .. v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto/16 :goto_3

    .line 2771
    .end local v6    # "small":Landroid/view/View;
    .end local v7    # "large":Landroid/view/View;
    .end local v8    # "notifInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v10    # "color":I
    .end local v11    # "c":I
    .end local v19    # "l":Landroid/view/View;
    .end local v24    # "s":Landroid/view/View;
    :cond_1f
    :try_start_12
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 2772
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleExpandedMove: no-op"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 2774
    :cond_20
    const/16 v23, 0x1

    goto/16 :goto_4

    .line 2536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleRemoveNotificationInExpandedMode(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "collapse"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2041
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-nez v0, :cond_0

    .line 2042
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->REMOVE_NOTIFICATION:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 2044
    :cond_0
    return-void
.end method

.method private handleRemoveNotificationInExpandedModeInternal(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "collapse"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2047
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotifCurrent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2049
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notif-end: is current:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2050
    if-eqz p2, :cond_0

    .line 2051
    invoke-direct {p0, v4, v4, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotificationInternal(ZZZ)V

    .line 2083
    :cond_0
    :goto_0
    return-void

    .line 2055
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 2057
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "remove-end: no more glance"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2058
    invoke-direct {p0, v3, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedViewInternal(ZZ)V

    goto :goto_0

    .line 2061
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remove-end: not current, remove:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2062
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationFromId(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotificationfromStack(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 2063
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 2064
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2065
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v0, :cond_6

    .line 2066
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getLowerNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2067
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after remove notif is:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v0, :cond_5

    const-string v0, "null"

    .line 2068
    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2067
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2074
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2075
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2076
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after remove current notif set:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v0, :cond_7

    const-string v0, "null"

    .line 2077
    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2076
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2080
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->postNotificationChange()V

    .line 2081
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicatorInternal()V

    goto/16 :goto_0

    .line 2067
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 2068
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2070
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    goto :goto_2

    .line 2076
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 2077
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private hideNotificationInternal()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1034
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .line 1035
    .local v0, "view":Lcom/navdy/hud/app/view/NotificationView;
    iget-object v1, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1037
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "hideNotification:border visible"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1038
    iget-object v1, v0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    new-instance v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$8;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$8;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    invoke-virtual {v1, v4, v2}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->stopTimeout(ZLjava/lang/Runnable;)V

    .line 1049
    :goto_0
    return-void

    .line 1046
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "hideNotification:screen_back"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1047
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v2, v3, v5, v5, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private hideScrollingIndicator()V
    .locals 2

    .prologue
    .line 2808
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    if-eqz v0, :cond_0

    .line 2809
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2811
    :cond_0
    return-void
.end method

.method private isDeleteAllIndex(II)Z
    .locals 1
    .param p1, "index"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 1773
    if-eqz p1, :cond_0

    add-int/lit8 v0, p2, -0x1

    if-ne p1, v0, :cond_1

    .line 1774
    :cond_0
    const/4 v0, 0x1

    .line 1776
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNotifCurrent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2007
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    if-eqz v2, :cond_2

    .line 2008
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 2009
    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v2, :cond_1

    .line 2021
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2013
    goto :goto_0

    .line 2016
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 2017
    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    .line 2021
    goto :goto_0
.end method

.method private isPhoneNotifPresentAndNotAlive()Z
    .locals 2

    .prologue
    .line 1985
    const-string v1, "navdy#phone#call#notif"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .line 1986
    .local v0, "callNotification":Lcom/navdy/hud/app/framework/phonecall/CallNotification;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1987
    const/4 v1, 0x1

    .line 1989
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isScreenHighPriority(Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 2
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 2892
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2897
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2894
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2892
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private moveNextInternal(Z)V
    .locals 9
    .param p1, "gesture"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2287
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v8

    .line 2288
    .local v8, "verbose":Z
    if-eqz v8, :cond_0

    .line 2289
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moveNextInternal:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2292
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2293
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleExpandedMove(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V

    .line 2319
    :cond_1
    :goto_0
    return-void

    .line 2295
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2297
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v7

    .line 2298
    .local v7, "current":I
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v6

    .line 2299
    .local v6, "count":I
    add-int/lit8 v0, v6, -0x1

    if-ne v7, v0, :cond_3

    .line 2301
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    .line 2302
    if-eqz v8, :cond_1

    .line 2303
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "moveNextInternal:cannot go next"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 2308
    :cond_3
    add-int/lit8 v7, v7, 0x1

    .line 2310
    if-eqz v8, :cond_4

    .line 2311
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moveNextInternal:going to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2314
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    .line 2315
    .local v2, "newView":Landroid/view/View;
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getExpandedViewIndicatorColor()I

    move-result v5

    .line 2316
    .local v5, "itemColor":I
    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, v1

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V

    goto :goto_0
.end method

.method private movePrevInternal(Z)V
    .locals 9
    .param p1, "gesture"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2322
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v8

    .line 2323
    .local v8, "verbose":Z
    if-eqz v8, :cond_0

    .line 2324
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "movePrevInternal:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2327
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2328
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleExpandedMove(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V

    .line 2350
    :cond_1
    :goto_0
    return-void

    .line 2330
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2332
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v7

    .line 2333
    .local v7, "current":I
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v6

    .line 2334
    .local v6, "count":I
    if-nez v7, :cond_3

    .line 2335
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    .line 2336
    if-eqz v8, :cond_1

    .line 2337
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "movePrevInternal: cannot go prev"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 2342
    :cond_3
    add-int/lit8 v7, v7, -0x1

    .line 2343
    if-eqz v8, :cond_4

    .line 2344
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "movePrevInternal: going to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2346
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    .line 2347
    .local v2, "newView":Landroid/view/View;
    const/4 v4, 0x1

    sget v5, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    move-object v0, p0

    move-object v3, v1

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V

    goto :goto_0
.end method

.method private removeAllNotification()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1789
    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    .line 1791
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v6

    .line 1792
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 1794
    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationHelper;->isNotificationRemovable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1795
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeAllNotification calling stop: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v8, v8, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v8}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1796
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 1797
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 1800
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v4

    .line 1801
    .local v4, "n":I
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeAllNotification:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1802
    const/4 v0, 0x0

    .line 1803
    .local v0, "current":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-lez v4, :cond_2

    .line 1804
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1805
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1806
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1807
    .local v1, "id":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1808
    .local v2, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationHelper;->isNotificationRemovable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1809
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5, v2}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1810
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1811
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeAllNotification removed:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1823
    .end local v0    # "current":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "n":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1813
    .restart local v0    # "current":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .restart local v1    # "id":Ljava/lang/String;
    .restart local v2    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v4    # "n":I
    :cond_1
    move-object v0, v2

    .line 1814
    :try_start_1
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeAllNotification left:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1819
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1820
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1821
    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1822
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->postNotificationChange()V

    .line 1823
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824
    return-void
.end method

.method private removeNotificationfromStack(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V
    .locals 5
    .param p1, "info"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p2, "internal"    # Z

    .prologue
    .line 924
    if-nez p1, :cond_1

    .line 953
    :cond_0
    :goto_0
    return-void

    .line 928
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 929
    .local v0, "obj":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 930
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "removeNotification, not found in id map"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 932
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 933
    if-nez v0, :cond_3

    .line 934
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "removeNotification, not found in priority map"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 936
    :cond_3
    iget-boolean v2, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v2, :cond_4

    .line 937
    const/4 v2, 0x0

    iput-boolean v2, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 938
    iget-object v2, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 940
    :cond_4
    const/4 v1, 0x0

    .line 941
    .local v1, "stagedId":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_5

    .line 942
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v1

    .line 943
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 944
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 945
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "cancelling staged notification, getting removed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 948
    :cond_5
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->postNotificationChange()V

    .line 949
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removed notification from stack ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] staged["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 950
    if-nez p2, :cond_0

    .line 951
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator()V

    goto/16 :goto_0
.end method

.method private restoreStack()V
    .locals 9

    .prologue
    .line 1290
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v5

    .line 1291
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1292
    .local v3, "n":I
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "restoreStack:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1293
    if-lez v3, :cond_3

    .line 1296
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 1297
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-nez v0, :cond_0

    .line 1298
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "restoreStack no device id"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1299
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1300
    monitor-exit v5

    .line 1319
    .end local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    return-void

    .line 1302
    .restart local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedListDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1303
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "restoreStack device id does not match current["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] saved["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedListDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1304
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1305
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->clearState()V

    .line 1306
    monitor-exit v5

    goto :goto_0

    .line 1316
    .end local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 1308
    .restart local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v3    # "n":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1309
    .local v2, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    iget-object v6, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v1

    .line 1310
    .local v1, "id":Ljava/lang/String;
    sget-object v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "restoreStack restored:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1311
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1312
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v6, v2, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1314
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->restoreState()V

    .line 1316
    .end local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :cond_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    goto/16 :goto_0
.end method

.method private runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V
    .locals 8
    .param p1, "operation"    # Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    .param p2, "ignoreCurrent"    # Z
    .param p3, "hideNotification"    # Z
    .param p4, "quick"    # Z
    .param p5, "gesture"    # Z
    .param p6, "id"    # Ljava/lang/String;
    .param p7, "collapse"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2200
    if-nez p2, :cond_2

    .line 2201
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    if-eqz v0, :cond_2

    .line 2202
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 2207
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2216
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZLjava/lang/String;Z)V

    invoke-interface {v7, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2257
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 2221
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "run operation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    .line 2223
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2225
    :pswitch_1
    invoke-direct {p0, p5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->moveNextInternal(Z)V

    .line 2226
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-eqz v0, :cond_4

    .line 2227
    const-string v1, "Glance_Delete_All"

    if-eqz p5, :cond_3

    const-string v0, "swipe"

    :goto_1
    invoke-static {v1, v3, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "dial"

    goto :goto_1

    .line 2229
    :cond_4
    if-eqz p5, :cond_5

    const-string v0, "swipe"

    :goto_2
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setGlanceNavigationSource(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v0, "dial"

    goto :goto_2

    .line 2234
    :pswitch_2
    invoke-direct {p0, p5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->movePrevInternal(Z)V

    .line 2235
    if-eqz p5, :cond_6

    const-string v0, "swipe"

    :goto_3
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setGlanceNavigationSource(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v0, "dial"

    goto :goto_3

    .line 2239
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->executeItemInternal()V

    .line 2240
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-eqz v0, :cond_1

    .line 2241
    const-string v1, "Glance_Delete_All"

    if-eqz p5, :cond_7

    const-string v0, "swipe"

    :goto_4
    invoke-static {v1, v3, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v0, "dial"

    goto :goto_4

    .line 2246
    :pswitch_4
    invoke-direct {p0, p3, p4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedViewInternal(ZZ)V

    goto :goto_0

    .line 2250
    :pswitch_5
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicatorInternal()V

    goto :goto_0

    .line 2254
    :pswitch_6
    invoke-direct {p0, p6, p7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleRemoveNotificationInExpandedModeInternal(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 2223
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private runQueuedOperation()V
    .locals 3

    .prologue
    .line 2260
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2261
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    .line 2262
    .local v0, "operationInfo":Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2278
    .end local v0    # "operationInfo":Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;
    :goto_0
    return-void

    .line 2275
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "operationRunning=false"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2276
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    goto :goto_0
.end method

.method private saveStack()V
    .locals 9

    .prologue
    .line 1250
    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v6

    .line 1252
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v4

    .line 1253
    .local v4, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v5, "incomingcall#toast"

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 1254
    const-string v5, "incomingcall#toast"

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 1256
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->clearCallStack()V

    .line 1258
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1259
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v3

    .line 1260
    .local v3, "n":I
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveStack:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1261
    if-lez v3, :cond_2

    .line 1262
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1263
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1264
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1265
    .local v0, "id":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1266
    .local v1, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_REMOVED_ON_DISCONNECT:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1267
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1268
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1269
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveStack removed:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1284
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v3    # "n":I
    .end local v4    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1270
    .restart local v0    # "id":Ljava/lang/String;
    .restart local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v3    # "n":I
    .restart local v4    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_0
    :try_start_1
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLANCE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    iget-object v7, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v7}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v7

    if-ne v5, v7, :cond_1

    .line 1271
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveStack saved:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1272
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1273
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1274
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1276
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveStack left:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1281
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1282
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stagedNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1283
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->saveState()V

    .line 1284
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1286
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 1287
    return-void
.end method

.method private setNotifScrollIndicatorXY(Landroid/graphics/RectF;Lcom/navdy/hud/app/framework/notifications/IScrollEvent;)V
    .locals 3
    .param p1, "rectF"    # Landroid/graphics/RectF;
    .param p2, "scroll"    # Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    .prologue
    .line 2814
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollProgress:Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;

    invoke-interface {p2, v1}, Lcom/navdy/hud/app/framework/notifications/IScrollEvent;->setListener(Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;)V

    .line 2815
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setCurrentItem(I)V

    .line 2816
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2817
    .local v0, "parent":Landroid/view/View;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getX()F

    move-result v1

    sget v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorLeftPadding:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 2818
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getY()F

    move-result v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    sget v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorCircleSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v1, v2

    sget v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 2819
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2820
    return-void
.end method

.method private showNotification(Z)V
    .locals 4
    .param p1, "ignoreAnimation"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1015
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "show notification isExpanded:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " anim:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ignore:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1016
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v3, p1}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1019
    :cond_0
    return-void
.end method

.method private startScrollThresholdTimer()V
    .locals 4

    .prologue
    .line 2823
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NOT_HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 2824
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ignoreScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2825
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ignoreScrollRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2826
    return-void
.end method

.method private updateDeleteAllGlanceCount(Landroid/widget/TextView;Landroid/view/View;)V
    .locals 8
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "container"    # Landroid/view/View;

    .prologue
    const v7, 0x7f0e000e

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 1868
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v0

    .line 1869
    .local v0, "count":I
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNonRemovableNotifCount()I

    move-result v1

    .line 1870
    .local v1, "removeCount":I
    sub-int/2addr v0, v1

    .line 1871
    if-nez v0, :cond_0

    .line 1872
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->glancesCannotDelete:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1873
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v7, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1882
    :goto_0
    return-void

    .line 1875
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v2, :cond_1

    .line 1876
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090119

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1880
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p2, v7, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 1878
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900af

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateExpandedIndicator()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1428
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-nez v0, :cond_0

    .line 1429
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1430
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "updateExpandedIndicator animating no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1435
    :cond_0
    :goto_0
    return-void

    .line 1433
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->UPDATE_INDICATOR:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v6, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    goto :goto_0
.end method

.method private updateExpandedIndicatorInternal()V
    .locals 11

    .prologue
    .line 1438
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v9

    .line 1439
    .local v9, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 1441
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v10}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v6

    .line 1442
    .local v6, "oldCount":I
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v10}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v3

    .line 1443
    .local v3, "currentItem":I
    invoke-direct {p0, v3, v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isDeleteAllIndex(II)Z

    move-result v7

    .line 1445
    .local v7, "onDeleteIndex":Z
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v2

    .line 1446
    .local v2, "currentCount":I
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isPhoneNotifPresentAndNotAlive()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1447
    add-int/lit8 v2, v2, -0x1

    .line 1449
    :cond_0
    add-int/lit8 v4, v2, 0x2

    .line 1451
    .local v4, "n":I
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorDeleteAll:I

    .line 1453
    .local v1, "color":I
    if-lt v3, v4, :cond_1

    .line 1454
    add-int/lit8 v3, v4, -0x1

    .line 1457
    :cond_1
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v10, :cond_2

    .line 1458
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-direct {p0, v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationIndex(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)I

    move-result v3

    .line 1459
    const/4 v10, -0x1

    if-eq v3, v10, :cond_2

    .line 1460
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v10, v10, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v10}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v1

    .line 1461
    add-int/lit8 v3, v3, 0x1

    .line 1465
    :cond_2
    invoke-direct {p0, v3, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isDeleteAllIndex(II)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1466
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorDeleteAll:I

    .line 1486
    :cond_3
    :goto_0
    invoke-virtual {p0, v4, v3, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator(III)V

    .line 1490
    iget-boolean v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v10, :cond_8

    .line 1491
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v0

    .line 1498
    .local v0, "child":Landroid/view/View;
    :goto_1
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1500
    iget-boolean v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v10, :cond_a

    .line 1501
    const v10, 0x7f0e010d

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1505
    .local v8, "textView":Landroid/widget/TextView;
    :goto_2
    invoke-direct {p0, v8, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateDeleteAllGlanceCount(Landroid/widget/TextView;Landroid/view/View;)V

    .line 1508
    .end local v8    # "textView":Landroid/widget/TextView;
    :cond_4
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V

    .line 1510
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "color":I
    .end local v2    # "currentCount":I
    .end local v3    # "currentItem":I
    .end local v4    # "n":I
    .end local v6    # "oldCount":I
    .end local v7    # "onDeleteIndex":Z
    :cond_5
    :goto_3
    return-void

    .line 1468
    .restart local v1    # "color":I
    .restart local v2    # "currentCount":I
    .restart local v3    # "currentItem":I
    .restart local v4    # "n":I
    .restart local v6    # "oldCount":I
    .restart local v7    # "onDeleteIndex":Z
    :cond_6
    if-eqz v7, :cond_3

    .line 1470
    add-int/lit8 v10, v2, -0x1

    invoke-direct {p0, v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationByIndex(I)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v5

    .line 1471
    .local v5, "newInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-nez v5, :cond_7

    .line 1473
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorDeleteAll:I

    .line 1474
    add-int/lit8 v3, v4, -0x1

    goto :goto_0

    .line 1476
    :cond_7
    iput-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1477
    iget-object v10, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v10}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v1

    .line 1479
    add-int/lit8 v10, v4, -0x1

    invoke-virtual {p0, v4, v10, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator(III)V

    .line 1480
    const/4 v10, 0x0

    invoke-direct {p0, v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->movePrevInternal(Z)V

    goto :goto_3

    .line 1493
    .end local v5    # "newInfo":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_8
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    if-nez v10, :cond_9

    .line 1494
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 1496
    :cond_9
    iget-object v10, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v10}, Lcom/navdy/hud/app/view/NotificationView;->getCurrentNotificationViewChild()Landroid/view/View;

    move-result-object v0

    .restart local v0    # "child":Landroid/view/View;
    goto :goto_1

    .line 1503
    :cond_a
    const v10, 0x7f0e0111

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .restart local v8    # "textView":Landroid/widget/TextView;
    goto :goto_2
.end method


# virtual methods
.method public addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V
    .locals 1
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;

    .prologue
    .line 620
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V

    .line 621
    return-void
.end method

.method public addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V
    .locals 4
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/notifications/INotification;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 626
    .local p2, "allowedScreen":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/navdy/service/library/events/ui/Screen;>;"
    if-nez p1, :cond_0

    .line 651
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 631
    .local v0, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-nez v0, :cond_1

    .line 632
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notification not accepted, Main screen not on ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 636
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->disconnected:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFS_NOT_ALLOWED_ON_DISCONNECT:Ljava/util/HashSet;

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notification not allowed during disconnect["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 641
    :cond_2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_3

    .line 642
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$6;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 649
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotificationInternal(Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/util/EnumSet;)V

    goto :goto_0
.end method

.method public addPendingNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V
    .locals 3
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;

    .prologue
    .line 851
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingNotification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 852
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addPendingNotification:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingNotification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 853
    return-void
.end method

.method public clearOperationQueue()V
    .locals 3

    .prologue
    .line 2281
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearOperationQueue:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2282
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    .line 2284
    return-void
.end method

.method public collapseExpandedNotification(ZZ)V
    .locals 1
    .param p1, "completely"    # Z
    .param p2, "quickly"    # Z

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    .line 1414
    return-void
.end method

.method public collapseNotification()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1346
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v1, :cond_1

    .line 1347
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 1348
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v1, :cond_1

    .line 1359
    :cond_0
    :goto_0
    return v0

    .line 1353
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 1354
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationCollapsing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 1355
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1356
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    .line 1357
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public currentNotificationTimeout()V
    .locals 3

    .prologue
    .line 956
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v0, :cond_0

    .line 957
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "currentNotificationTimeout"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 958
    const-string v0, "Glance_Dismiss"

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    const-string v2, "timeout"

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 959
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;Z)V

    .line 961
    :cond_0
    return-void
.end method

.method public enableNotifications(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2829
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification enabled["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2830
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enable:Z

    .line 2831
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneNotifications(Z)V

    .line 2832
    return-void
.end method

.method public enablePhoneNotifications(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2839
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification enabled phone["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2840
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneCalls:Z

    .line 2841
    return-void
.end method

.method public executeItem()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2184
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2185
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-nez v0, :cond_2

    .line 2186
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->SELECT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v6, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 2191
    :cond_1
    :goto_0
    return-void

    .line 2188
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "executeItem: no-op deleteGlances set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public expandNotification()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1365
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v2, :cond_1

    .line 1366
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 1367
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v2, :cond_1

    .line 1379
    :cond_0
    :goto_0
    return v0

    .line 1372
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 1373
    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1374
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->makeNotificationCurrent(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1375
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    move v0, v1

    .line 1376
    goto :goto_0
.end method

.method public getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v0, :cond_0

    .line 965
    const/4 v0, 0x0

    .line 967
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    goto :goto_0
.end method

.method public getExpandedIndicatorCount()I
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v0

    return v0
.end method

.method public getExpandedIndicatorCurrentItem()I
    .locals 1

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getExpandedViewChild()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1417
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 1418
    .local v0, "lyt":Landroid/widget/FrameLayout;
    if-nez v0, :cond_1

    .line 1424
    :cond_0
    :goto_0
    return-object v1

    .line 1421
    :cond_1
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1424
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 971
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 980
    :goto_0
    return-object v1

    .line 974
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 975
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 976
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-eqz v0, :cond_1

    .line 977
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    monitor-exit v2

    goto :goto_0

    .line 979
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getNotificationColor()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1187
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 1188
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1189
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1190
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    iget-boolean v3, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v3, :cond_0

    .line 1191
    monitor-exit v2

    .line 1195
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :goto_0
    return v1

    .line 1193
    .restart local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_0
    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v1

    monitor-exit v2

    goto :goto_0

    .line 1197
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1195
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getNotificationCount()I
    .locals 2

    .prologue
    .line 984
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 985
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 986
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTopNotificationId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1400
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1401
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v0, :cond_0

    .line 1402
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    .line 1407
    :goto_0
    return-object v0

    .line 1404
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1405
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 1409
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1407
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getUIContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1528
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public handleConnect()V
    .locals 2

    .prologue
    .line 1216
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "client connected: restore stack"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->disconnected:Z

    .line 1218
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->restoreStack()V

    .line 1219
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationSavedListDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 1220
    return-void
.end method

.method public handleDisconnect(Z)V
    .locals 4
    .param p1, "linkLost"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1201
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->disconnected:Z

    if-nez v0, :cond_1

    .line 1202
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "client disconnected: save stack :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1203
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->disconnected:Z

    .line 1204
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->saveStack()V

    .line 1206
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1207
    :cond_0
    invoke-virtual {p0, v3, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 1211
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->disconnect()V

    .line 1213
    :cond_1
    return-void

    .line 1209
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotification()Z

    goto :goto_0
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 7
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1532
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 1533
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleKey: running="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " qsize:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " animating:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1535
    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-eqz v4, :cond_0

    .line 1536
    sget-object v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "handleKey: animating no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v0, v2

    .line 1604
    :goto_0
    return v0

    .line 1540
    :cond_0
    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->operationRunning:Z

    if-nez v4, :cond_4

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1544
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v4, :cond_4

    .line 1545
    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showOn:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->supportScroll()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v4, :cond_4

    .line 1547
    sget-object v4, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-eq p1, v4, :cond_1

    sget-object v4, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-ne p1, v4, :cond_2

    :cond_1
    move v1, v3

    .line 1549
    .local v1, "scrollKey":Z
    :goto_1
    if-eqz v1, :cond_4

    .line 1550
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4, p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 1551
    .local v0, "handled":Z
    if-eqz v0, :cond_3

    .line 1553
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 1554
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->ignoreScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move v0, v3

    .line 1555
    goto :goto_0

    .end local v0    # "handled":Z
    .end local v1    # "scrollKey":Z
    :cond_2
    move v1, v2

    .line 1547
    goto :goto_1

    .line 1558
    .restart local v0    # "handled":Z
    .restart local v1    # "scrollKey":Z
    :cond_3
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState:[I

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1576
    .end local v0    # "handled":Z
    .end local v1    # "scrollKey":Z
    :cond_4
    :pswitch_0
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    iput-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->scrollState:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 1578
    const/4 v0, 0x0

    .line 1580
    .restart local v0    # "handled":Z
    sget-object v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$16;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 1582
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->movePrevious(Z)V

    .line 1583
    const/4 v0, 0x1

    .line 1584
    goto :goto_0

    .restart local v1    # "scrollKey":Z
    :pswitch_2
    move v0, v3

    .line 1564
    goto :goto_0

    .line 1567
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->startScrollThresholdTimer()V

    move v0, v3

    .line 1568
    goto :goto_0

    .line 1587
    .end local v1    # "scrollKey":Z
    :pswitch_4
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->moveNext(Z)V

    .line 1588
    const/4 v0, 0x1

    .line 1589
    goto :goto_0

    .line 1592
    :pswitch_5
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->executeItem()V

    .line 1593
    const/4 v0, 0x1

    .line 1594
    goto :goto_0

    .line 1597
    :pswitch_6
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "power button click: show shutdown"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1598
    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v2}, Lcom/navdy/hud/app/event/Shutdown$Reason;->asBundle()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingShutdownArgs:Landroid/os/Bundle;

    .line 1599
    invoke-virtual {p0, v3, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 1600
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1558
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1580
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public hideConnectionToast(Z)V
    .locals 2
    .param p1, "connected"    # Z

    .prologue
    .line 1241
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 1242
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    if-eqz p1, :cond_0

    .line 1243
    const-string v1, "disconnection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 1247
    :goto_0
    return-void

    .line 1245
    :cond_0
    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hideNotification()V
    .locals 3

    .prologue
    .line 1022
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v0, :cond_0

    .line 1023
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1024
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hideNotification:expand hide"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1026
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animateOutExpandedView(ZLcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 1031
    :cond_0
    :goto_0
    return-void

    .line 1028
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotificationInternal()V

    goto :goto_0
.end method

.method public hideNotificationCoverView()V
    .locals 3

    .prologue
    .line 2884
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationCoverView()Landroid/view/View;

    move-result-object v0

    .line 2885
    .local v0, "layoutCover":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2886
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "hideNotificationCoverView"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2887
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2889
    :cond_0
    return-void
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 2848
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    return v0
.end method

.method public isCurrentItemDeleteAll()Z
    .locals 3

    .prologue
    .line 1763
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-nez v2, :cond_0

    .line 1764
    const/4 v2, 0x0

    .line 1769
    :goto_0
    return v2

    .line 1767
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemCount()I

    move-result v1

    .line 1768
    .local v1, "oldCount":I
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v0

    .line 1769
    .local v0, "currentItem":I
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isDeleteAllIndex(II)Z

    move-result v2

    goto :goto_0
.end method

.method public isCurrentNotificationId(Ljava/lang/String;)Z
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1052
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v1, :cond_1

    .line 1058
    :cond_0
    :goto_0
    return v0

    .line 1055
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1056
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 1331
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    return v0
.end method

.method public isExpandedNotificationVisible()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1322
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 1323
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1324
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1326
    :cond_0
    return v1
.end method

.method public isNotificationMarkedForRemoval(Ljava/lang/String;)Z
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 2104
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 2105
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationFromId(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    .line 2106
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-eqz v0, :cond_0

    .line 2107
    iget-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    monitor-exit v2

    .line 2109
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 2110
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isNotificationPresent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 990
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 991
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 992
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isNotificationViewShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1384
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v1, :cond_1

    .line 1385
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    .line 1386
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-nez v1, :cond_1

    .line 1395
    :cond_0
    :goto_0
    return v0

    .line 1391
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 1392
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1393
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNotificationsEnabled()Z
    .locals 1

    .prologue
    .line 2835
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enable:Z

    return v0
.end method

.method public isPhoneNotificationsEnabled()Z
    .locals 1

    .prologue
    .line 2844
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneCalls:Z

    return v0
.end method

.method public makeNotificationCurrent(Z)Z
    .locals 7
    .param p1, "makeAllVisible"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1123
    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 1124
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v5, :cond_4

    .line 1125
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 1126
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v3, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v3, :cond_0

    .line 1127
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 1129
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1130
    monitor-exit v4

    .line 1162
    :goto_0
    return v2

    .line 1135
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1136
    .local v0, "highest":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eq v0, v2, :cond_3

    .line 1137
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mknc:current["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] is not highest["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 1138
    invoke-interface {v6}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1137
    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1139
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v2, :cond_2

    .line 1140
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v5, 0x0

    iput-boolean v5, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 1141
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 1142
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "mknc: stop called"

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1144
    :cond_2
    iput-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1146
    :cond_3
    monitor-exit v4

    move v2, v3

    goto :goto_0

    .line 1148
    .end local v0    # "highest":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 1149
    if-eqz p1, :cond_5

    .line 1152
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "make all visible"

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1153
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1154
    .local v1, "infoIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1155
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->pushBackDuetoHighPriority:Z

    goto :goto_1

    .line 1161
    .end local v1    # "infoIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1158
    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->lastKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1159
    monitor-exit v4

    move v2, v3

    goto/16 :goto_0

    .line 1161
    :cond_6
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public moveNext(Z)V
    .locals 8
    .param p1, "gesture"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2174
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2175
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-nez v0, :cond_2

    .line 2176
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_NEXT:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v6, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, p1

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 2181
    :cond_1
    :goto_0
    return-void

    .line 2178
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "moveNext: no-op deleteGlances set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public movePrevious(Z)V
    .locals 8
    .param p1, "gesture"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2164
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2165
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-nez v0, :cond_2

    .line 2166
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;->MOVE_PREV:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v6, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, p1

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 2171
    :cond_1
    :goto_0
    return-void

    .line 2168
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "movePrevious: no-op deleteGlances set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCallAccepted(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2099
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "phone-start"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2100
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->updateExpandedIndicator()V

    .line 2101
    return-void
.end method

.method public onCallEnded(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2032
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2034
    const-string v0, "navdy#phone#call#notif"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleRemoveNotificationInExpandedMode(Ljava/lang/String;Z)V

    .line 2038
    :goto_0
    return-void

    .line 2036
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "phone-end"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClearGlances(Lcom/navdy/service/library/events/glances/ClearGlances;)V
    .locals 0
    .param p1, "clearGlances"    # Lcom/navdy/service/library/events/glances/ClearGlances;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2861
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearAllGlances()V

    .line 2862
    return-void
.end method

.method public onNotificationPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 1
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2853
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2855
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearAllGlances()V

    .line 2857
    :cond_0
    return-void
.end method

.method public onShowToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1828
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;->name:Ljava/lang/String;

    const-string v1, "glance-deleted"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showToast: called deleteAllGlances"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1830
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->deleteAllGlances()V

    .line 1832
    :cond_0
    return-void
.end method

.method public postNotificationChange()V
    .locals 2

    .prologue
    .line 2027
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->NOTIFICATION_CHANGE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 2028
    return-void
.end method

.method public printPriorityMap()V
    .locals 8

    .prologue
    .line 1738
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 1739
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v2, :cond_0

    .line 1740
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "notif-debug-map stack current = null"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1745
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v2, :cond_1

    .line 1746
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "notif-debug-map current = null"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1751
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationPriorityMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1752
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "notif-debug-map-start"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1753
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1754
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1755
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notif-debug ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] priority["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->priority:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] counter ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->uniqueCounter:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 1759
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1742
    :cond_0
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notif-debug-map stack current = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1748
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notif-debug-map current = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1758
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;>;"
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "notif-debug-map-stop"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1759
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1760
    return-void
.end method

.method public removeNotification(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 856
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 857
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$7;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 866
    :goto_0
    return-void

    .line 864
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public removeNotification(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "remove"    # Z

    .prologue
    const/4 v3, 0x0

    .line 869
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 870
    return-void
.end method

.method public removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "remove"    # Z
    .param p3, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p4, "screenArgs"    # Landroid/os/Bundle;
    .param p5, "screenArgs2"    # Ljava/lang/Object;

    .prologue
    .line 875
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 921
    :goto_0
    return-void

    .line 879
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 880
    :try_start_0
    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreen:Lcom/navdy/service/library/events/ui/Screen;

    .line 881
    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs:Landroid/os/Bundle;

    .line 882
    iput-object p5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->pendingScreenArgs2:Ljava/lang/Object;

    .line 883
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationIdMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 884
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-nez v0, :cond_1

    .line 885
    monitor-exit v2

    goto :goto_0

    .line 920
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 888
    .restart local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 889
    invoke-static {p1}, Lcom/navdy/hud/app/framework/notifications/NotificationHelper;->isNotificationRemovable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 891
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "removeNotification, handleRemoveNotificationInExpandedMode"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 892
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 893
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleRemoveNotificationInExpandedMode(Ljava/lang/String;Z)V

    .line 894
    monitor-exit v2

    goto :goto_0

    .line 898
    :cond_2
    iput-boolean p2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 899
    iget-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    if-eqz v1, :cond_3

    .line 900
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeNotification ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] resurrected:false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 901
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->resurrected:Z

    .line 903
    :cond_3
    iget-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->isAlive()Z

    move-result v1

    if-nez v1, :cond_4

    .line 904
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeNotification ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] is not alive"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 905
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    .line 907
    :cond_4
    iget-boolean v1, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-nez v1, :cond_5

    .line 908
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeNotification ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] is still alive"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 910
    :cond_5
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 912
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z

    if-nez v1, :cond_6

    .line 913
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotification()V

    .line 920
    :cond_6
    :goto_1
    monitor-exit v2

    goto/16 :goto_0

    .line 916
    :cond_7
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "bkgnd notification being removed"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 917
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotificationfromStack(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Z)V

    .line 918
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public setExpandedStack(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 2160
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z

    .line 2161
    return-void
.end method

.method public setNotificationColor()V
    .locals 4

    .prologue
    .line 1166
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v1

    .line 1167
    .local v1, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    if-nez v1, :cond_0

    .line 1183
    :goto_0
    return-void

    .line 1170
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_2

    .line 1171
    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 1172
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getLowerNotification(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    .line 1173
    .local v0, "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    if-nez v0, :cond_1

    .line 1174
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/NotificationView;->resetNextNotificationColor()V

    .line 1178
    :goto_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1176
    .restart local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_1
    :try_start_1
    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/NotificationView;->setNextNotificationColor(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1181
    .end local v0    # "info":Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    :cond_2
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/NotificationView;->resetNextNotificationColor()V

    goto :goto_0
.end method

.method public showHideToast(Z)V
    .locals 3
    .param p1, "connect"    # Z

    .prologue
    .line 1223
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 1224
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isWelcomeScreenOn()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1225
    if-nez p1, :cond_1

    .line 1226
    const-string v1, "disconnection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isCurrentToast(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1227
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showDisconnectedToast(Z)V

    .line 1238
    :cond_0
    :goto_0
    return-void

    .line 1230
    :cond_1
    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isCurrentToast(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1231
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showConnectedToast()V

    goto :goto_0

    .line 1235
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "welcome screen on, no disconnect notification"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1236
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideConnectionToast(Z)V

    goto :goto_0
.end method

.method public showNotification()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 996
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enable:Z

    if-nez v1, :cond_0

    .line 997
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneCalls:Z

    if-eqz v1, :cond_1

    const-string v1, "navdy#phone#call#notif"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCurrentNotificationId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 998
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "notification only phone-notif allowed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1005
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isWelcomeScreenOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1007
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "welcome screen on, don\'t show notification"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1011
    :goto_0
    return v0

    .line 1000
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "notification not enabled don\'t show notification"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1010
    :cond_2
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification(Z)V

    .line 1011
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateExpandedIndicator(III)V
    .locals 3
    .param p1, "count"    # I
    .param p2, "currentItem"    # I
    .param p3, "color"    # I

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setItemCount(I)V

    .line 1514
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(II)V

    .line 1515
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->displayScrollingIndicator(I)V

    .line 1516
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateExpandedIndicator count ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currentitem="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1517
    return-void
.end method

.method public viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
    .locals 21
    .param p1, "small"    # Landroid/view/View;
    .param p2, "large"    # Landroid/view/View;
    .param p3, "notifInfo"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    .param p4, "previous"    # Z
    .param p5, "color"    # I

    .prologue
    .line 1612
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedViewChild()Landroid/view/View;

    move-result-object v6

    .line 1614
    .local v6, "expandViewOut":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v20

    .line 1615
    .local v20, "pos":I
    if-eqz p4, :cond_4

    .line 1616
    add-int/lit8 v20, v20, -0x1

    .line 1621
    :goto_0
    move/from16 v3, v20

    .line 1623
    .local v3, "item":I
    new-instance v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;

    move-object/from16 v2, p0

    move/from16 v4, p5

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;IILcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Landroid/view/View;)V

    .line 1670
    .local v1, "endAction":Ljava/lang/Runnable;
    if-eqz p1, :cond_0

    .line 1672
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->getCurrentNotificationViewChild()Landroid/view/View;

    move-result-object v10

    .line 1673
    .local v10, "notifViewOut":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->getNotificationContainer()Landroid/view/ViewGroup;

    move-result-object v11

    .line 1675
    .local v11, "notifViewOutParent":Landroid/view/ViewGroup;
    if-eqz p4, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animationTranslation:I

    neg-int v2, v2

    :goto_1
    const/16 v4, 0xfa

    const/16 v14, 0xfa

    new-instance v7, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;

    move-object/from16 v8, p0

    move-object/from16 v9, p3

    move-object/from16 v12, p2

    move-object v13, v1

    invoke-direct/range {v7 .. v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$10;-><init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Runnable;)V

    move-object/from16 v9, p1

    move v12, v2

    move v13, v4

    move-object v15, v7

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateNotifViews(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)V

    .line 1709
    .end local v10    # "notifViewOut":Landroid/view/View;
    .end local v11    # "notifViewOutParent":Landroid/view/ViewGroup;
    :cond_0
    if-eqz p2, :cond_1

    .line 1711
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;

    if-eqz p4, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animationTranslation:I

    neg-int v15, v2

    :goto_2
    const/16 v16, 0xfa

    const/16 v17, 0xfa

    move-object/from16 v12, p2

    move-object v13, v6

    move-object/from16 v18, v1

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;->animateExpandedViews(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IIILjava/lang/Runnable;)V

    .line 1721
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1722
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideScrollingIndicator()V

    .line 1724
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->supportScroll()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1725
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    check-cast v2, Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lcom/navdy/hud/app/framework/notifications/IScrollEvent;->setListener(Lcom/navdy/hud/app/framework/notifications/IProgressUpdate;)V

    .line 1730
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move/from16 v0, p5

    invoke-virtual {v2, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v19

    .line 1731
    .local v19, "indicatorAnimator":Landroid/animation/AnimatorSet;
    if-eqz v19, :cond_3

    .line 1732
    invoke-virtual/range {v19 .. v19}, Landroid/animation/AnimatorSet;->start()V

    .line 1734
    :cond_3
    return-void

    .line 1618
    .end local v1    # "endAction":Ljava/lang/Runnable;
    .end local v3    # "item":I
    .end local v19    # "indicatorAnimator":Landroid/animation/AnimatorSet;
    :cond_4
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 1675
    .restart local v1    # "endAction":Ljava/lang/Runnable;
    .restart local v3    # "item":I
    .restart local v10    # "notifViewOut":Landroid/view/View;
    .restart local v11    # "notifViewOutParent":Landroid/view/ViewGroup;
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animationTranslation:I

    goto :goto_1

    .line 1711
    .end local v10    # "notifViewOut":Landroid/view/View;
    .end local v11    # "notifViewOutParent":Landroid/view/ViewGroup;
    :cond_6
    move-object/from16 v0, p0

    iget v15, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->animationTranslation:I

    goto :goto_2
.end method
