.class public interface abstract Lcom/navdy/hud/app/framework/notifications/INotification;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;


# virtual methods
.method public abstract canAddToStackIfCurrentExists()Z
.end method

.method public abstract expandNotification()Z
.end method

.method public abstract getColor()I
.end method

.method public abstract getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
.end method

.method public abstract getExpandedViewIndicatorColor()I
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getTimeout()I
.end method

.method public abstract getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
.end method

.method public abstract getView(Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
.end method

.method public abstract isAlive()Z
.end method

.method public abstract isPurgeable()Z
.end method

.method public abstract onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
.end method

.method public abstract onExpandedNotificationSwitched()V
.end method

.method public abstract onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
.end method

.method public abstract onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
.end method

.method public abstract onStop()V
.end method

.method public abstract onUpdate()V
.end method

.method public abstract supportScroll()Z
.end method
