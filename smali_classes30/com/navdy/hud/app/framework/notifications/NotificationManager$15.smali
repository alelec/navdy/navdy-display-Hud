.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->displayScrollingIndicator(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$index:I

.field final synthetic val$scroll:Lcom/navdy/hud/app/framework/notifications/IScrollEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;ILcom/navdy/hud/app/framework/notifications/IScrollEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 2788
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->val$index:I

    iput-object p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->val$scroll:Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2791
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->val$index:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemPos(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 2792
    .local v0, "rectF":Landroid/graphics/RectF;
    if-eqz v0, :cond_1

    .line 2793
    iget v1, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    .line 2799
    :goto_0
    return-void

    .line 2796
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->val$scroll:Lcom/navdy/hud/app/framework/notifications/IScrollEvent;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotifScrollIndicatorXY(Landroid/graphics/RectF;Lcom/navdy/hud/app/framework/notifications/IScrollEvent;)V
    invoke-static {v1, v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/graphics/RectF;Lcom/navdy/hud/app/framework/notifications/IScrollEvent;)V

    .line 2798
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$15;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
