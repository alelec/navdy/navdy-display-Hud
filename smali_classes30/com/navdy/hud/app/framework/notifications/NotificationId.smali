.class public final Lcom/navdy/hud/app/framework/notifications/NotificationId;
.super Ljava/lang/Object;
.source "NotificationId.java"


# static fields
.field public static final BRIGHTNESS_NOTIFICATION_ID:Ljava/lang/String; = "navdy#brightness#notif"

.field public static final GLYMPSE_NOTIFICATION_ID:Ljava/lang/String; = "navdy#glympse#notif"

.field public static final MUSIC_NOTIFICATION_ID:Ljava/lang/String; = "navdy#music#notif"

.field public static final PHONE_CALL_NOTIFICATION_ID:Ljava/lang/String; = "navdy#phone#call#notif"

.field public static final PLACE_TYPE_SEARCH_NOTIFICATION_ID:Ljava/lang/String; = "navdy#place#type#search#notif"

.field public static final ROUTE_CALC_NOTIFICATION_ID:Ljava/lang/String; = "navdy#route#calc#notif"

.field public static final SMS_NOTIFICATION_ID:Ljava/lang/String; = "navdy#sms#notif#"

.field public static final TRAFFIC_DELAY_NOTIFICATION_ID:Ljava/lang/String; = "navdy#traffic#delay#notif"

.field public static final TRAFFIC_EVENT_NOTIFICATION_ID:Ljava/lang/String; = "navdy#traffic#event#notif"

.field public static final TRAFFIC_JAM_NOTIFICATION_ID:Ljava/lang/String; = "navdy#traffic#jam#notif"

.field public static final TRAFFIC_REROUTE_NOTIFICATION_ID:Ljava/lang/String; = "navdy#traffic#reroute#notif"

.field public static final VOICE_ASSIST_NOTIFICATION_ID:Ljava/lang/String; = "navdy#voiceassist#notif"

.field public static final VOICE_SEARCH_NOTIFICATION_ID:Ljava/lang/String; = "navdy#voicesearch#notif"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
