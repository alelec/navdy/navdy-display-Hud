.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->viewSwitchAnimation(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$color:I

.field final synthetic val$expandViewOut:Landroid/view/View;

.field final synthetic val$item:I

.field final synthetic val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;IILcom/navdy/hud/app/framework/notifications/NotificationManager$Info;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 1623
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$item:I

    iput p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$color:I

    iput-object p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    iput-object p5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1626
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lockObj:Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$2300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1627
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v0

    iget v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$item:I

    iget v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$color:I

    invoke-virtual {v0, v3, v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(II)V

    .line 1629
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1630
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v0, :cond_0

    .line 1631
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 1632
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 1634
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    if-nez v0, :cond_1

    .line 1635
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v3

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1302(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1637
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$notifInfo:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 1640
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1641
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isDeleteAllView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1642
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedNotifView:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4000(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1643
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "delete all big view removed"

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1644
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1645
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$expandViewOut:Landroid/view/View;

    invoke-static {v0, v3}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 1649
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1650
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->printPriorityMap()V

    .line 1653
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1654
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->onExpandedNotificationSwitched()V

    .line 1657
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->val$item:I

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->displayScrollingIndicator(I)V
    invoke-static {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1600(Lcom/navdy/hud/app/framework/notifications/NotificationManager;I)V

    .line 1662
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4100(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    .line 1663
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1664
    const-string v2, "Glance_Advance"

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 1665
    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$9;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 1664
    :goto_0
    invoke-static {v2, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 1667
    return-void

    .line 1663
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-object v0, v1

    .line 1665
    goto :goto_0
.end method
