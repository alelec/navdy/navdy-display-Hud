.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedViewInternal(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$hideNotification:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 2415
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput-boolean p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->val$hideNotification:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2419
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getExpandedNotificationCoverView()Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$700(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Landroid/view/View;

    move-result-object v1

    .line 2420
    .local v1, "layoutCover":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2422
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandedWithStack:Z
    invoke-static {v2, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1102(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    .line 2424
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->val$hideNotification:Z

    .line 2426
    .local v0, "hide":Z
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 2428
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2429
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v2, :cond_2

    .line 2430
    const/4 v0, 0x1

    .line 2431
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stackchange:hide-remove"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2469
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1202(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2470
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2, v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1302(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2474
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->clearOperationQueue()V

    .line 2476
    if-eqz v0, :cond_8

    .line 2477
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotificationInternal()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V

    .line 2481
    :goto_1
    return-void

    .line 2432
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-nez v2, :cond_1

    .line 2433
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getView(Landroid/content/Context;)Landroid/view/View;

    .line 2434
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iput-boolean v6, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 2435
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    goto :goto_0

    .line 2439
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stackchange:current and stack are diff"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2440
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2441
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-eqz v2, :cond_4

    .line 2442
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stackchange:stopping current:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2443
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iput-boolean v5, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 2444
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStop()V

    .line 2447
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1200(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v3

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$402(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    .line 2448
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 2449
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v3

    iget-object v3, v3, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/NotificationView;->switchNotfication(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 2450
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->removed:Z

    if-eqz v2, :cond_7

    .line 2451
    const/4 v0, 0x1

    .line 2452
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stackchange:hide-diff-remove"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2459
    :cond_5
    :goto_2
    if-nez v0, :cond_6

    .line 2460
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "calling onExpandedNotificationEvent-collapse-"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2461
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    sget-object v3, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 2465
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->setNotificationColor()V

    .line 2466
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->showNextNotificationColor()V

    goto/16 :goto_0

    .line 2453
    :cond_7
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    if-nez v2, :cond_5

    .line 2454
    sget-object v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stackchange:starting stacked:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v4}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2455
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notifView:Lcom/navdy/hud/app/view/NotificationView;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$3500(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->getView(Landroid/content/Context;)Landroid/view/View;

    .line 2456
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iput-boolean v6, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->startCalled:Z

    .line 2457
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->currentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$400(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # getter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$1900(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/notifications/INotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    goto/16 :goto_2

    .line 2479
    :cond_8
    iget-object v2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$12;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    # setter for: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating:Z
    invoke-static {v2, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$502(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Z)Z

    goto/16 :goto_1
.end method
