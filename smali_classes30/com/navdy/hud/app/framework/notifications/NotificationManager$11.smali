.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runQueuedOperation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field final synthetic val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 2262
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2265
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->operation:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-boolean v3, v3, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->hideNotification:Z

    iget-object v4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-boolean v4, v4, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->quick:Z

    iget-object v5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-boolean v5, v5, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->gesture:Z

    iget-object v6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->id:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$11;->val$operationInfo:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;

    iget-boolean v7, v7, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->collapse:Z

    # invokes: Lcom/navdy/hud/app/framework/notifications/NotificationManager;->runOperation(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V
    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->access$4300(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZZLjava/lang/String;Z)V

    .line 2272
    return-void
.end method
