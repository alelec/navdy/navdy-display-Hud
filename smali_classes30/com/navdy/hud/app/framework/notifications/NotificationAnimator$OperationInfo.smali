.class public Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;
.super Ljava/lang/Object;
.source "NotificationAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OperationInfo"
.end annotation


# instance fields
.field collapse:Z

.field gesture:Z

.field hideNotification:Z

.field id:Ljava/lang/String;

.field operation:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

.field quick:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;ZZZLjava/lang/String;Z)V
    .locals 0
    .param p1, "operation"    # Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;
    .param p2, "hideNotification"    # Z
    .param p3, "quick"    # Z
    .param p4, "gesture"    # Z
    .param p5, "id"    # Ljava/lang/String;
    .param p6, "collapse"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->operation:Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$Operation;

    .line 42
    iput-boolean p2, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->hideNotification:Z

    .line 43
    iput-boolean p3, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->quick:Z

    .line 44
    iput-boolean p4, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->gesture:Z

    .line 45
    iput-object p5, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->id:Ljava/lang/String;

    .line 46
    iput-boolean p6, p0, Lcom/navdy/hud/app/framework/notifications/NotificationAnimator$OperationInfo;->collapse:Z

    .line 47
    return-void
.end method
