.class Lcom/navdy/hud/app/framework/notifications/NotificationManager$8;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideNotificationInternal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/notifications/NotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .prologue
    .line 1038
    iput-object p1, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$8;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1041
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hideNotification-anim:screen_back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1042
    iget-object v0, p0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$8;->this$0:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v4, v4, v3}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1043
    return-void
.end method
