.class final enum Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;
.super Ljava/lang/Enum;
.source "NotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/notifications/NotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ScrollState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

.field public static final enum HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

.field public static final enum LIMIT_REACHED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

.field public static final enum NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

.field public static final enum NOT_HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 148
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 149
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    const-string v1, "HANDLED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 150
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    const-string v1, "NOT_HANDLED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NOT_HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 151
    new-instance v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    const-string v1, "LIMIT_REACHED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->LIMIT_REACHED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    .line 147
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NONE:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->NOT_HANDLED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->LIMIT_REACHED:Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 147
    const-class v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->$VALUES:[Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/notifications/NotificationManager$ScrollState;

    return-object v0
.end method
