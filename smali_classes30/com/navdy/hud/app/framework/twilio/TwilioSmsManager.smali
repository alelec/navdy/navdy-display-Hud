.class public Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;
.super Ljava/lang/Object;
.source "TwilioSmsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;,
        Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    }
.end annotation


# static fields
.field private static final ACCOUNT_SID:Ljava/lang/String;

.field private static final AUTH_TOKEN:Ljava/lang/String;

.field private static final CONNECTION_TIMEOUT:I

.field private static final CREDENTIALS:Ljava/lang/String;

.field private static final MESSAGE_SERVICE_ID:Ljava/lang/String;

.field private static final READ_TIMEOUT:I

.field private static final SMS_ENDPOINT:Ljava/lang/String;

.field private static final WRITE_TIMEOUT:I

.field private static instance:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field private final okHttpClient:Lokhttp3/OkHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1e

    .line 35
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 41
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->CONNECTION_TIMEOUT:I

    .line 42
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->READ_TIMEOUT:I

    .line 43
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->WRITE_TIMEOUT:I

    .line 61
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 62
    .local v0, "context":Landroid/content/Context;
    const-string v1, "TWILIO_ACCOUNT_SID"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->ACCOUNT_SID:Ljava/lang/String;

    .line 63
    const-string v1, "TWILIO_AUTH_TOKEN"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->AUTH_TOKEN:Ljava/lang/String;

    .line 64
    const-string v1, "TWILIO_MSG_SERVICE_ID"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->MESSAGE_SERVICE_ID:Ljava/lang/String;

    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.twilio.com/2010-04-01/Accounts/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->ACCOUNT_SID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Messages.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->SMS_ENDPOINT:Ljava/lang/String;

    .line 66
    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->ACCOUNT_SID:Ljava/lang/String;

    sget-object v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->AUTH_TOKEN:Ljava/lang/String;

    invoke-static {v1, v2}, Lokhttp3/Credentials;->basic(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->CREDENTIALS:Ljava/lang/String;

    .line 69
    new-instance v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->instance:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getHttpManager()Lcom/navdy/service/library/network/http/IHttpManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/navdy/service/library/network/http/IHttpManager;->getClient()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 82
    .local v0, "client":Lokhttp3/OkHttpClient;
    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->READ_TIMEOUT:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 83
    invoke-virtual {v1, v2, v3, v4}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->WRITE_TIMEOUT:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 84
    invoke-virtual {v1, v2, v3, v4}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->CONNECTION_TIMEOUT:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 85
    invoke-virtual {v1, v2, v3, v4}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->okHttpClient:Lokhttp3/OkHttpClient;

    .line 88
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 89
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->MESSAGE_SERVICE_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->CREDENTIALS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->SMS_ENDPOINT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->okHttpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->instance:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    return-object v0
.end method

.method private sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    .locals 5
    .param p1, "errorCode"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    .param p2, "sendProfileId"    # Ljava/lang/String;
    .param p3, "cb"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    .prologue
    .line 164
    if-nez p3, :cond_0

    .line 184
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    .line 168
    .local v1, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "currentProfileName":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$2;->$SwitchMap$com$navdy$hud$app$framework$twilio$TwilioSmsManager$ErrorCode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 176
    const/4 v2, 0x0

    const-string v3, "iOS"

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->areMessageCanned()Z

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSmsSent(ZLjava/lang/String;Z)V

    .line 180
    :goto_1
    invoke-interface {p3, p1}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;->result(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;)V

    goto :goto_0

    .line 172
    :pswitch_0
    const/4 v2, 0x1

    const-string v3, "iOS"

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->areMessageCanned()Z

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSmsSent(ZLjava/lang/String;Z)V

    goto :goto_1

    .line 182
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profile changed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public sendSms(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    .locals 8
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "cb"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    .prologue
    .line 104
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 105
    :cond_0
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INVALID_PARAMETER:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 159
    :goto_0
    return-object v0

    .line 107
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->NETWORK_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v4

    .line 111
    .local v4, "profileId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v7

    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;-><init>(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V

    const/16 v1, 0x17

    invoke-virtual {v7, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 156
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 157
    .end local v4    # "profileId":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 158
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "sendSms"

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 159
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INTERNAL_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    goto :goto_0
.end method
