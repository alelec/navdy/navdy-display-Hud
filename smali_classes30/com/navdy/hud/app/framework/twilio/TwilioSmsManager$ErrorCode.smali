.class public final enum Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
.super Ljava/lang/Enum;
.source "TwilioSmsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

.field public static final enum INVALID_PARAMETER:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

.field public static final enum NETWORK_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

.field public static final enum SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

.field public static final enum TWILIO_SERVER_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 50
    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    const-string v1, "INTERNAL_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INTERNAL_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->NETWORK_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    const-string v1, "TWILIO_SERVER_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->TWILIO_SERVER_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    const-string v1, "INVALID_PARAMETER"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INVALID_PARAMETER:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INTERNAL_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->NETWORK_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->TWILIO_SERVER_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INVALID_PARAMETER:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->$VALUES:[Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->$VALUES:[Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    return-object v0
.end method
