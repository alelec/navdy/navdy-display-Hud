.class Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;
.super Ljava/lang/Object;
.source "TwilioSmsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendSms(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

.field final synthetic val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$number:Ljava/lang/String;

.field final synthetic val$profileId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$message:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$number:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$profileId:Ljava/lang/String;

    iput-object p5, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 115
    :try_start_0
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "sendSms-start"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getFirstName()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "driverName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 121
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f090190

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$message:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "formattedMessage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$number:Ljava/lang/String;

    invoke-static {v7}, Lcom/navdy/hud/app/util/PhoneUtil;->convertToE164Format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "e164Number":Ljava/lang/String;
    new-instance v7, Lokhttp3/FormBody$Builder;

    invoke-direct {v7}, Lokhttp3/FormBody$Builder;-><init>()V

    const-string v8, "To"

    .line 129
    invoke-virtual {v7, v8, v1}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v7

    const-string v8, "MessagingServiceSid"

    .line 130
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->MESSAGE_SERVICE_ID:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$100()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v7

    const-string v8, "Body"

    .line 131
    invoke-virtual {v7, v8, v3}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    move-result-object v7

    .line 132
    invoke-virtual {v7}, Lokhttp3/FormBody$Builder;->build()Lokhttp3/FormBody;

    move-result-object v2

    .line 133
    .local v2, "formBody":Lokhttp3/RequestBody;
    new-instance v7, Lokhttp3/Request$Builder;

    invoke-direct {v7}, Lokhttp3/Request$Builder;-><init>()V

    .line 134
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->SMS_ENDPOINT:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$300()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    const-string v8, "Authorization"

    .line 135
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->CREDENTIALS:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$200()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    .line 136
    invoke-virtual {v7, v2}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v7

    .line 137
    invoke-virtual {v7}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v4

    .line 138
    .local v4, "request":Lokhttp3/Request;
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->okHttpClient:Lokhttp3/OkHttpClient;
    invoke-static {v7}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$400(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;)Lokhttp3/OkHttpClient;

    move-result-object v7

    invoke-virtual {v7, v4}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v7

    invoke-interface {v7}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v5

    .line 139
    .local v5, "response":Lokhttp3/Response;
    invoke-virtual {v5}, Lokhttp3/Response;->isSuccessful()Z

    move-result v7

    if-nez v7, :cond_1

    .line 140
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendSms-end-err:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lokhttp3/Response;->code()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lokhttp3/Response;->message()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 141
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    sget-object v8, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->TWILIO_SERVER_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    iget-object v9, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$profileId:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    # invokes: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    invoke-static {v7, v8, v9, v10}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$500(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V

    .line 154
    .end local v0    # "driverName":Ljava/lang/String;
    .end local v1    # "e164Number":Ljava/lang/String;
    .end local v2    # "formBody":Lokhttp3/RequestBody;
    .end local v3    # "formattedMessage":Ljava/lang/String;
    .end local v4    # "request":Lokhttp3/Request;
    .end local v5    # "response":Lokhttp3/Response;
    :goto_1
    return-void

    .line 123
    .restart local v0    # "driverName":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$message:Ljava/lang/String;

    .restart local v3    # "formattedMessage":Ljava/lang/String;
    goto/16 :goto_0

    .line 143
    .restart local v1    # "e164Number":Ljava/lang/String;
    .restart local v2    # "formBody":Lokhttp3/RequestBody;
    .restart local v4    # "request":Lokhttp3/Request;
    .restart local v5    # "response":Lokhttp3/Response;
    :cond_1
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendSms-end-suc:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lokhttp3/Response;->code()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    sget-object v8, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    iget-object v9, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$profileId:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    # invokes: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    invoke-static {v7, v8, v9, v10}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$500(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 146
    .end local v0    # "driverName":Ljava/lang/String;
    .end local v1    # "e164Number":Ljava/lang/String;
    .end local v2    # "formBody":Lokhttp3/RequestBody;
    .end local v3    # "formattedMessage":Ljava/lang/String;
    .end local v4    # "request":Lokhttp3/Request;
    .end local v5    # "response":Lokhttp3/Response;
    :catch_0
    move-exception v6

    .line 147
    .local v6, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "sendSms-end-err"

    invoke-virtual {v7, v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    instance-of v7, v6, Ljava/io/IOException;

    if-eqz v7, :cond_2

    .line 149
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    sget-object v8, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->NETWORK_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    iget-object v9, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$profileId:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    # invokes: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    invoke-static {v7, v8, v9, v10}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$500(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V

    goto :goto_1

    .line 151
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->this$0:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;

    sget-object v8, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->INTERNAL_ERROR:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    iget-object v9, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$profileId:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$1;->val$cb:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;

    # invokes: Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->sendResult(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V
    invoke-static {v7, v8, v9, v10}, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;->access$500(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;Ljava/lang/String;Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;)V

    goto :goto_1
.end method
