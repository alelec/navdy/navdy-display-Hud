.class Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;
.super Ljava/lang/Object;
.source "GlympseManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;

.field final synthetic val$etaDate:Ljava/util/Date;

.field final synthetic val$track:Lcom/glympse/android/api/GTrack;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;Ljava/util/Date;Lcom/glympse/android/api/GTrack;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->this$2:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->val$etaDate:Ljava/util/Date;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->val$track:Lcom/glympse/android/api/GTrack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 178
    .local v8, "now":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->val$etaDate:Ljava/util/Date;

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v4, v12, v8

    .line 179
    .local v4, "durationInMillis":J
    const-wide/16 v12, 0x0

    cmp-long v12, v4, v12

    if-gez v12, :cond_0

    .line 180
    const-wide/16 v4, 0x0

    .line 182
    :cond_0
    const-wide/32 v12, 0xea60

    div-long v6, v4, v12

    .line 183
    .local v6, "minutes":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->this$2:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;

    iget-object v12, v12, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;->this$1:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;

    iget-object v12, v12, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$300(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Lcom/glympse/android/api/GGlympse;

    move-result-object v12

    invoke-interface {v12}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v12

    invoke-interface {v12}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 184
    .local v2, "activeTickets":Lcom/glympse/android/core/GArray;, "Lcom/glympse/android/core/GArray<Lcom/glympse/android/api/GTicket;>;"
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v11

    .line 185
    .local v11, "ticketListLength":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v11, :cond_3

    .line 186
    invoke-interface {v2, v3}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/glympse/android/api/GTicket;

    .line 187
    .local v10, "ticket":Lcom/glympse/android/api/GTicket;
    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->isActive()Z

    move-result v12

    if-nez v12, :cond_1

    .line 188
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "update ETA ticket not active:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 185
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 191
    :cond_1
    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v12

    if-nez v12, :cond_2

    .line 192
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "update ETA ticket not for trip:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 195
    :cond_2
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "update ETA for ticket with id["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] durationMillis["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] ETA["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->val$etaDate:Ljava/util/Date;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] currentTime="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " minutes["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] expireTime["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/util/Date;

    .line 196
    invoke-interface {v10}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v14, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 195
    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 197
    invoke-interface {v10, v4, v5}, Lcom/glympse/android/api/GTicket;->updateEta(J)V

    .line 198
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;->val$track:Lcom/glympse/android/api/GTrack;

    invoke-interface {v10, v12}, Lcom/glympse/android/api/GTicket;->updateRoute(Lcom/glympse/android/api/GTrack;)V

    goto/16 :goto_1

    .line 200
    .end local v10    # "ticket":Lcom/glympse/android/api/GTicket;
    :cond_3
    return-void
.end method
