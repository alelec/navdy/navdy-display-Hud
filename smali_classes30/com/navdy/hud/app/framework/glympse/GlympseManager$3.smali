.class Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;
.super Ljava/lang/Object;
.source "GlympseManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addTicketListener(Lcom/glympse/android/api/GTicket;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

.field final synthetic val$ticket:Lcom/glympse/android/api/GTicket;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/glympse/android/api/GTicket;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->val$ticket:Lcom/glympse/android/api/GTicket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 10
    .param p1, "gGlympse"    # Lcom/glympse/android/api/GGlympse;
    .param p2, "listener"    # I
    .param p3, "events"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v8, 0x1

    .line 413
    move-object v3, p4

    check-cast v3, Lcom/glympse/android/api/GTicket;

    .line 415
    .local v3, "updatedTicket":Lcom/glympse/android/api/GTicket;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    const/16 v5, 0x2000

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->hasGlympseEvent(II)Z
    invoke-static {v4, p3, v5}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$600(Lcom/navdy/hud/app/framework/glympse/GlympseManager;II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 416
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v2

    .line 417
    .local v2, "ticketId":Ljava/lang/String;
    const-string v4, "navdy_contact_uuid"

    invoke-interface {v3, v8, v9, v4}, Lcom/glympse/android/api/GTicket;->getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    .line 419
    .local v1, "contactUuid":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ticket added, ticket id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; contact uuid is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 421
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$900(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 422
    iget-object v5, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$900(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/framework/contacts/Contact;

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addGlympseSentGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    invoke-static {v5, v4}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$1000(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/navdy/hud/app/framework/contacts/Contact;)V

    .line 426
    .end local v1    # "contactUuid":Ljava/lang/String;
    .end local v2    # "ticketId":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    const/16 v5, 0x4000

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->hasGlympseEvent(II)Z
    invoke-static {v4, p3, v5}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$600(Lcom/navdy/hud/app/framework/glympse/GlympseManager;II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 427
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v2

    .line 428
    .restart local v2    # "ticketId":Ljava/lang/String;
    const-string v4, "navdy_contact_uuid"

    invoke-interface {v3, v8, v9, v4}, Lcom/glympse/android/api/GTicket;->getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    .line 430
    .restart local v1    # "contactUuid":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ticket updated, ticket id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; contact uuid is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 432
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$900(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 433
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$900(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 434
    .local v0, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addGlympseReadGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    invoke-static {v4, v0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$1100(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/navdy/hud/app/framework/contacts/Contact;)V

    .line 435
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;->val$ticket:Lcom/glympse/android/api/GTicket;

    invoke-interface {v4, p0}, Lcom/glympse/android/api/GTicket;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 436
    if-nez v1, :cond_1

    const-string v1, ""

    .end local v1    # "contactUuid":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlympseViewed(Ljava/lang/String;)V

    .line 437
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Glympse viewed ticket id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " contact["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] number["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] e164["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/hud/app/util/PhoneUtil;->convertToE164Format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 440
    .end local v0    # "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v2    # "ticketId":Ljava/lang/String;
    :cond_2
    return-void
.end method
