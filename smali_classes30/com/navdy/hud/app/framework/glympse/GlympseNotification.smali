.class public Lcom/navdy/hud/app/framework/glympse/GlympseNotification;
.super Ljava/lang/Object;
.source "GlympseNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;
    }
.end annotation


# static fields
.field private static final NOTIFICATION_ALERT_BADGE_SCALE:F = 0.95f

.field private static final NOTIFICATION_BADGE_SCALE:F = 0.5f

.field private static final NOTIFICATION_DEFAULT_ICON_SCALE:F = 1.0f

.field private static final NOTIFICATION_ICON_SCALE:F = 1.38f

.field private static final NOTIFICATION_TIMEOUT:I = 0x1388

.field private static final alertColor:I

.field private static final choicesOnlyDismiss:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final choicesRetryAndDismiss:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final handler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final resources:Landroid/content/res/Resources;

.field private static final shareLocationTripColor:I


# instance fields
.field badge:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0151
    .end annotation
.end field

.field badgeIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0152
    .end annotation
.end field

.field choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0153
    .end annotation
.end field

.field private final contact:Lcom/navdy/hud/app/framework/contacts/Contact;

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private final destinationLabel:Ljava/lang/String;

.field private final latitude:D

.field private final longitude:D

.field private final message:Ljava/lang/String;

.field private final notifId:Ljava/lang/String;

.field notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014e
    .end annotation
.end field

.field notificationUserImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014f
    .end annotation
.end field

.field private retrySendingMessage:Z

.field subtitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014c
    .end annotation
.end field

.field title:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00c3
    .end annotation
.end field

.field private final type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

.field private final uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const v9, 0x7f02012c

    const v2, 0x7f020125

    .line 54
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v4, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    invoke-direct {v1, v4}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->logger:Lcom/navdy/service/library/log/Logger;

    .line 62
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    .line 69
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->handler:Landroid/os/Handler;

    .line 72
    const/high16 v5, -0x1000000

    .line 73
    .local v5, "unselectedColor":I
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0d0021

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 74
    .local v3, "dismissColor":I
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0900dc

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 75
    .local v6, "dismiss":Ljava/lang/String;
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v1, 0x7f0e000b

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    .line 77
    .local v0, "dismissChoice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesOnlyDismiss:Ljava/util/List;

    .line 78
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesOnlyDismiss:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0d0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 81
    .local v10, "retryColor":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesRetryAndDismiss:Ljava/util/List;

    .line 82
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesRetryAndDismiss:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v8, 0x7f0e006f

    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f09022d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move v11, v9

    move v12, v5

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesRetryAndDismiss:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0d00a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->shareLocationTripColor:I

    .line 86
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0d00a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->alertColor:I

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;)V
    .locals 10
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    .prologue
    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    .line 128
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;Ljava/lang/String;Ljava/lang/String;DD)V

    .line 129
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;Ljava/lang/String;Ljava/lang/String;DD)V
    .locals 3
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "destinationLabel"    # Ljava/lang/String;
    .param p5, "latitude"    # D
    .param p7, "longitude"    # D

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 133
    iput-object p2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    .line 135
    iput-object p3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->message:Ljava/lang/String;

    .line 136
    iput-object p4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->destinationLabel:Ljava/lang/String;

    .line 137
    iput-wide p5, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->latitude:D

    .line 138
    iput-wide p7, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->longitude:D

    .line 140
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->uuid:Ljava/lang/String;

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "navdy#glympse#notif#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notifId:Ljava/lang/String;

    .line 142
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Lcom/navdy/hud/app/framework/contacts/Contact;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->destinationLabel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->latitude:D

    return-wide v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->longitude:D

    return-wide v0
.end method

.method static synthetic access$600(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # D
    .param p5, "x4"    # D

    .prologue
    .line 52
    invoke-static/range {p0 .. p6}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->addGlympseOfflineGlance(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V

    return-void
.end method

.method private static addGlympseOfflineGlance(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V
    .locals 11
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "destinationLabel"    # Ljava/lang/String;
    .param p3, "latitude"    # D
    .param p5, "longitude"    # D

    .prologue
    .line 405
    new-instance v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    sget-object v3, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->OFFLINE:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    move-object v2, p1

    move-object v4, p0

    move-object v5, p2

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;Ljava/lang/String;Ljava/lang/String;DD)V

    .line 406
    .local v1, "glympseOfflineNotification":Lcom/navdy/hud/app/framework/glympse/GlympseNotification;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 407
    return-void
.end method

.method private getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 421
    :goto_0
    return-object v0

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    goto :goto_0

    .line 421
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private setOfflineUI()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f090258

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->subtitle:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0901e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const v1, 0x7f02016c

    sget v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->shareLocationTripColor:I

    const v3, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badgeIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const v1, 0x7f02018f

    sget v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->alertColor:I

    const v3, 0x3f733333    # 0.95f

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 256
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badgeIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesRetryAndDismiss:Ljava/util/List;

    invoke-virtual {v0, v1, v4, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 259
    return-void
.end method

.method private setReadUI()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 202
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->title:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->getContactName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->subtitle:Landroid/widget/TextView;

    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0902ec

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    sget-object v5, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v2

    .line 211
    .local v2, "imagePath":Ljava/io/File;
    invoke-static {v2}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 213
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 214
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationUserImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 215
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationUserImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 227
    :goto_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badgeIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const v4, 0x7f02016c

    sget v5, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->shareLocationTripColor:I

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v3, v4, v5, v8, v6}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 233
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badgeIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 235
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesOnlyDismiss:Ljava/util/List;

    invoke-virtual {v3, v4, v7, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 236
    return-void

    .line 207
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "imagePath":Ljava/io/File;
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->subtitle:Landroid/widget/TextView;

    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0902eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 217
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "imagePath":Ljava/io/File;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v1

    .line 218
    .local v1, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget v4, v4, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 219
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget v5, v5, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 220
    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceColor(I)I

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    .line 218
    invoke-virtual {v3, v4, v5, v8, v6}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 224
    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private setSentUI()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 179
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0902aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->subtitle:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f090292

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->getContactName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const v1, 0x7f02016c

    sget v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->shareLocationTripColor:I

    const/4 v3, 0x0

    const v4, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notificationIcon:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badge:Landroid/widget/ImageView;

    const v1, 0x7f020191

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->badge:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choicesOnlyDismiss:Ljava/util/List;

    invoke-virtual {v0, v1, v5, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 199
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f09019e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 2
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 356
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    const v1, 0x7f0e006f

    if-ne v0, v1, :cond_1

    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->retrySendingMessage:Z

    .line 358
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notifId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    const v1, 0x7f0e000b

    if-ne v0, v1, :cond_0

    .line 360
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notifId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 263
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->notifId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeout()I
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->OFFLINE:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    if-ne v0, v1, :cond_0

    .line 304
    const/4 v0, 0x0

    .line 306
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x1388

    goto :goto_0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->GLYMPSE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030039

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 163
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 165
    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$2;->$SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type:[I

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 175
    :goto_0
    return-object v0

    .line 167
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->setSentUI()V

    goto :goto_0

    .line 170
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->setReadUI()V

    goto :goto_0

    .line 173
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->setOfflineUI()V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 341
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 365
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 334
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 337
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 374
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 396
    :goto_0
    return v0

    .line 377
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 379
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    .line 380
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->OFFLINE:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    if-eq v0, v2, :cond_1

    .line 381
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    :cond_1
    move v0, v1

    .line 383
    goto :goto_0

    .line 386
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    .line 387
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->type:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->OFFLINE:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    if-eq v0, v2, :cond_2

    .line 388
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    :cond_2
    move v0, v1

    .line 390
    goto :goto_0

    .line 393
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v0, v1

    .line 394
    goto :goto_0

    .line 377
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 331
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 0
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 274
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 282
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->retrySendingMessage:Z

    if-eqz v0, :cond_0

    .line 283
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 299
    :cond_0
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 148
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 277
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    return v0
.end method
