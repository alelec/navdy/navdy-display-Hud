.class public final Lcom/navdy/hud/app/framework/glympse/GlympseManager;
.super Ljava/lang/Object;
.source "GlympseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;,
        Lcom/navdy/hud/app/framework/glympse/GlympseManager$InternalSingleton;
    }
.end annotation


# static fields
.field private static final GLYMPSE_API_KEY:Ljava/lang/String;

.field private static final GLYMPSE_BASE_URL:Ljava/lang/String; = "api.glympse.com"

.field public static final GLYMPSE_DURATION:I

.field public static final GLYMPSE_TYPE_SHARE_LOCATION:Ljava/lang/String; = "Location"

.field public static final GLYMPSE_TYPE_SHARE_TRIP:Ljava/lang/String; = "Trip"

.field private static final NAVDY_CONTACT_UUID:Ljava/lang/String; = "navdy_contact_uuid"

.field private static final NAVDY_GLYMPSE_PARTNER_ID:J = 0x1L

.field private static final UPDATE_ETA_INTERVAL:I

.field private static final handler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final messages:[Ljava/lang/String;


# instance fields
.field private currentGlympseUserId:Ljava/lang/String;

.field private glympse:Lcom/glympse/android/api/GGlympse;

.field private glympseInitFailed:Z

.field private isTrackingETA:Z

.field private final notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field private final ticketsAwaitingReadReceipt:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private updateETA:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 70
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 83
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 84
    .local v0, "resources":Landroid/content/res/Resources;
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x7f090257

    .line 85
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0901ec

    .line 86
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f090182

    .line 87
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f09024f

    .line 88
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f090183

    .line 89
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->messages:[Ljava/lang/String;

    .line 97
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "GLYMPSE_API_KEY"

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->GLYMPSE_API_KEY:Ljava/lang/String;

    .line 99
    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->GLYMPSE_DURATION:I

    .line 100
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->UPDATE_ETA_INTERVAL:I

    .line 105
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->updateETA:Ljava/lang/Runnable;

    .line 211
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 212
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;

    .line 215
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "creating glympse"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "api.glympse.com"

    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->GLYMPSE_API_KEY:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/glympse/android/api/GlympseFactory;->createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    .line 217
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->setUpEventListener()V

    .line 220
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/glympse/android/api/GGlympse;->setEtaMode(I)V

    .line 221
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->start()V

    .line 222
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calling glympse start duration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->getGlympseDuration()I

    move-result v4

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    div-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minutes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    .line 229
    .local v0, "bus":Lcom/squareup/otto/Bus;
    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 230
    return-void

    .line 223
    .end local v0    # "bus":Lcom/squareup/otto/Bus;
    :catch_0
    move-exception v1

    .line 224
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 225
    iput-boolean v10, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympseInitFailed:Z

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addGlympseSentGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addGlympseReadGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    return-void
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Lcom/glympse/android/api/GGlympse;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    return-object v0
.end method

.method static synthetic access$400()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 68
    sget v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->UPDATE_ETA_INTERVAL:I

    return v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/glympse/GlympseManager;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->hasGlympseEvent(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->setUserProfile()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/glympse/android/api/GTicket;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    .param p1, "x1"    # Lcom/glympse/android/api/GTicket;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addTicketListener(Lcom/glympse/android/api/GTicket;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;

    return-object v0
.end method

.method private addGlympseReadGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 2
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 450
    new-instance v0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->READ:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;)V

    .line 451
    .local v0, "glympseReadNotification":Lcom/navdy/hud/app/framework/glympse/GlympseNotification;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 452
    return-void
.end method

.method private addGlympseSentGlance(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 2
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 445
    new-instance v0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    sget-object v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->SENT:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;)V

    .line 446
    .local v0, "glympseNotification":Lcom/navdy/hud/app/framework/glympse/GlympseNotification;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 447
    return-void
.end method

.method private addTicketListener(Lcom/glympse/android/api/GTicket;)V
    .locals 1
    .param p1, "ticket"    # Lcom/glympse/android/api/GTicket;

    .prologue
    .line 410
    new-instance v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$3;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/glympse/android/api/GTicket;)V

    invoke-interface {p1, v0}, Lcom/glympse/android/api/GTicket;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 442
    return-void
.end method

.method private deleteActiveTickets()V
    .locals 7

    .prologue
    .line 352
    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "deleteActiveTickets"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 353
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->stopTrackingETA()V

    .line 355
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v4}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v0

    .line 356
    .local v0, "activeTickets":Lcom/glympse/android/core/GArray;, "Lcom/glympse/android/core/GArray<Lcom/glympse/android/api/GTicket;>;"
    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 358
    .local v3, "ticketListLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 359
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/api/GTicket;

    .line 360
    .local v2, "ticket":Lcom/glympse/android/api/GTicket;
    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleteActiveTickets deleting ticket with id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 361
    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->deleteTicket()Z

    .line 358
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 363
    .end local v2    # "ticket":Lcom/glympse/android/api/GTicket;
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    .locals 1

    .prologue
    .line 77
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager$InternalSingleton;->singleton:Lcom/navdy/hud/app/framework/glympse/GlympseManager;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$InternalSingleton;->access$100()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v0

    return-object v0
.end method

.method private hasGlympseEvent(II)Z
    .locals 1
    .param p1, "receivedEvents"    # I
    .param p2, "glympseEvent"    # I

    .prologue
    .line 388
    and-int v0, p1, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setUpEventListener()V
    .locals 2

    .prologue
    .line 392
    new-instance v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V

    .line 406
    .local v0, "glympseListener":Lcom/glympse/android/api/GEventListener;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GGlympse;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 407
    return-void
.end method

.method private setUserProfile()V
    .locals 5

    .prologue
    .line 304
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 306
    .local v0, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->currentGlympseUserId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "called setUserProfile with same profile as current, no-op"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 335
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GUserManager;->getSelf()Lcom/glympse/android/api/GUser;

    move-result-object v1

    .line 313
    .local v1, "user":Lcom/glympse/android/api/GUser;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v2

    if-nez v2, :cond_3

    .line 314
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->deleteActiveTickets()V

    .line 315
    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 317
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting new user profile for Glympse: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 319
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 320
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GUser;->setNickname(Ljava/lang/String;)Z

    .line 323
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverImage()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 324
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setting photo"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 325
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverImage()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/core/CoreFactory;->createDrawable(Landroid/graphics/Bitmap;)Lcom/glympse/android/core/GDrawable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GUser;->setAvatar(Lcom/glympse/android/core/GDrawable;)Z

    .line 331
    :goto_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->currentGlympseUserId:Ljava/lang/String;

    goto :goto_0

    .line 327
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "not setting photo"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 328
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GUser;->setAvatar(Lcom/glympse/android/core/GDrawable;)Z

    goto :goto_1

    .line 333
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "driverProfile is default, not setting it for Glympse"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startTrackingETA()V
    .locals 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isTrackingETA:Z

    if-eqz v0, :cond_0

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isTrackingETA:Z

    .line 343
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->updateETA:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private stopTrackingETA()V
    .locals 2

    .prologue
    .line 347
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->updateETA:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 348
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isTrackingETA:Z

    .line 349
    return-void
.end method


# virtual methods
.method public addMessage(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/StringBuilder;)Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;
    .locals 16
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "destinationLabel"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "latitude"    # D
    .param p6, "longitude"    # D
    .param p8, "id"    # Ljava/lang/StringBuilder;

    .prologue
    .line 248
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympseInitFailed:Z

    if-eqz v11, :cond_0

    .line 249
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "glympse init failed, cannot add message, no-op"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 250
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;->INTERNAL_ERROR:Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    .line 289
    :goto_0
    return-object v11

    .line 253
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v6

    .line 255
    .local v6, "internetConnected":Z
    if-nez v6, :cond_1

    .line 256
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "addMessage, no connection, no-op"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;->NO_INTERNET:Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    goto :goto_0

    .line 260
    :cond_1
    const/4 v8, 0x0

    .line 262
    .local v8, "place":Lcom/glympse/android/api/GPlace;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    const-wide/16 v12, 0x0

    cmpl-double v11, p4, v12

    if-nez v11, :cond_2

    const-wide/16 v12, 0x0

    cmpl-double v11, p6, v12

    if-eqz v11, :cond_3

    :cond_2
    const/4 v5, 0x1

    .line 264
    .local v5, "hasRouteData":Z
    :goto_1
    if-eqz v5, :cond_4

    .line 268
    invoke-static/range {p3 .. p3}, Lcom/glympse/android/core/CoreFactory;->createString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 265
    move-wide/from16 v0, p4

    move-wide/from16 v2, p6

    invoke-static {v0, v1, v2, v3, v11}, Lcom/glympse/android/api/GlympseFactory;->createPlace(DDLjava/lang/String;)Lcom/glympse/android/api/GPlace;

    move-result-object v8

    .line 270
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->startTrackingETA()V

    .line 275
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->getGlympseDuration()I

    move-result v11

    move-object/from16 v0, p2

    invoke-static {v11, v0, v8}, Lcom/glympse/android/api/GlympseFactory;->createTicket(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Lcom/glympse/android/api/GTicket;

    move-result-object v9

    .line 276
    .local v9, "ticket":Lcom/glympse/android/api/GTicket;
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v11}, Lcom/navdy/hud/app/util/PhoneUtil;->convertToE164Format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 277
    .local v4, "e164Number":Ljava/lang/String;
    const/4 v11, 0x3

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v11, v12, v4}, Lcom/glympse/android/api/GlympseFactory;->createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v7

    .line 278
    .local v7, "invite":Lcom/glympse/android/api/GInvite;
    invoke-interface {v9, v7}, Lcom/glympse/android/api/GTicket;->addInvite(Lcom/glympse/android/api/GInvite;)Z

    .line 280
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 281
    .local v10, "uuid":Ljava/lang/String;
    const/4 v11, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 282
    move-object/from16 v0, p8

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->ticketsAwaitingReadReceipt:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v11, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    const-wide/16 v12, 0x1

    const-string v11, "navdy_contact_uuid"

    invoke-static {v10}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v14

    invoke-interface {v9, v12, v13, v11, v14}, Lcom/glympse/android/api/GTicket;->appendData(JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Z

    .line 286
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v11, v9}, Lcom/glympse/android/api/GGlympse;->sendTicket(Lcom/glympse/android/api/GTicket;)Z

    .line 287
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "addMessage, added ticket name["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] e164Number["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] number["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] uuid["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 289
    sget-object v11, Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;->NONE:Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    goto/16 :goto_0

    .line 262
    .end local v4    # "e164Number":Ljava/lang/String;
    .end local v5    # "hasRouteData":Z
    .end local v7    # "invite":Lcom/glympse/android/api/GInvite;
    .end local v9    # "ticket":Lcom/glympse/android/api/GTicket;
    .end local v10    # "uuid":Ljava/lang/String;
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 272
    .restart local v5    # "hasRouteData":Z
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->stopTrackingETA()V

    goto/16 :goto_2
.end method

.method public expireActiveTickets()V
    .locals 10

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isSynced()Z

    move-result v4

    if-nez v4, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "expireActiveTickets"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 370
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->stopTrackingETA()V

    .line 372
    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v4}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v0

    .line 373
    .local v0, "activeTickets":Lcom/glympse/android/core/GArray;, "Lcom/glympse/android/core/GArray<Lcom/glympse/android/api/GTicket;>;"
    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 375
    .local v3, "ticketListLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_0

    .line 376
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/api/GTicket;

    .line 377
    .local v2, "ticket":Lcom/glympse/android/api/GTicket;
    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->isActive()Z

    move-result v4

    if-nez v4, :cond_2

    .line 378
    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "expireActiveTickets ticket["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] not active"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "expireActiveTickets ticket[ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] expiryTime["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 383
    invoke-interface {v2}, Lcom/glympse/android/api/GTicket;->expire()Z

    .line 375
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getMessages()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->messages:[Ljava/lang/String;

    return-object v0
.end method

.method public isSynced()Z
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympseInitFailed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->glympse:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v0}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GHistoryManager;->isSynced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isSynced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->setUserProfile()V

    .line 301
    :cond_0
    return-void
.end method
