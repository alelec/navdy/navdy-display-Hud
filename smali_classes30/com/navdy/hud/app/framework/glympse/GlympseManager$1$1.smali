.class Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;
.super Ljava/lang/Object;
.source "GlympseManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;->this$1:Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 132
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v24

    if-nez v24, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v9

    .line 136
    .local v9, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v19

    .line 137
    .local v19, "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v21

    .line 139
    .local v21, "state":Lcom/navdy/hud/app/maps/here/HereNavController$State;
    sget-object v24, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 141
    const/16 v24, 0x1

    sget-object v25, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    move-object/from16 v0, v19

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v8

    .line 142
    .local v8, "etaDate":Ljava/util/Date;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 147
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 148
    .local v10, "l1":J
    invoke-static {}, Lcom/glympse/android/api/GlympseFactory;->createTrackBuilder()Lcom/glympse/android/api/GTrackBuilder;

    move-result-object v23

    .line 149
    .local v23, "trackBuilder":Lcom/glympse/android/api/GTrackBuilder;
    const/16 v24, 0x0

    invoke-interface/range {v23 .. v24}, Lcom/glympse/android/api/GTrackBuilder;->setSource(I)V

    .line 150
    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v6

    .line 151
    .local v6, "distance":J
    long-to-int v0, v6

    move/from16 v24, v0

    invoke-interface/range {v23 .. v24}, Lcom/glympse/android/api/GTrackBuilder;->setDistance(I)V

    .line 152
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v20

    .line 153
    .local v20, "route":Lcom/here/android/mpa/routing/Route;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    .local v4, "builder":Ljava/lang/StringBuilder;
    if-eqz v20, :cond_3

    .line 155
    invoke-virtual/range {v20 .. v20}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v18

    .line 156
    .local v18, "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz v18, :cond_3

    .line 157
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_2
    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_3

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/here/android/mpa/routing/Maneuver;

    .line 158
    .local v17, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/routing/Maneuver;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    .line 159
    .local v5, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v5, :cond_2

    .line 160
    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v12

    .line 161
    .local v12, "lat":D
    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v14

    .line 162
    .local v14, "lng":D
    invoke-static {v12, v13, v14, v15}, Lcom/glympse/android/core/CoreFactory;->createLocation(DD)Lcom/glympse/android/core/GLocation;

    move-result-object v16

    .line 163
    .local v16, "location":Lcom/glympse/android/core/GLocation;
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GTrackBuilder;->addLocation(Lcom/glympse/android/core/GLocation;)V

    .line 164
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "{"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ","

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "} "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 170
    .end local v5    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v12    # "lat":D
    .end local v14    # "lng":D
    .end local v16    # "location":Lcom/glympse/android/core/GLocation;
    .end local v17    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v18    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    :cond_3
    invoke-interface/range {v23 .. v23}, Lcom/glympse/android/api/GTrackBuilder;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v22

    .line 171
    .local v22, "track":Lcom/glympse/android/api/GTrack;
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v24

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "update ETA track = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 172
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v24

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "update ETA time to generate track:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v26

    sub-long v26, v26, v10

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$400()Landroid/os/Handler;

    move-result-object v24

    new-instance v25, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1$1;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;Ljava/util/Date;Lcom/glympse/android/api/GTrack;)V

    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
