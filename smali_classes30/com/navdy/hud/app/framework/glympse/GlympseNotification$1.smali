.class Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;
.super Ljava/lang/Object;
.source "GlympseNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->onStop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 286
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Glympse:retry sending message"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 287
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 288
    .local v8, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$100(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Lcom/navdy/hud/app/framework/contacts/Contact;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->message:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$200(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->destinationLabel:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$300(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->latitude:D
    invoke-static {v4}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$400(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D

    move-result-wide v4

    iget-object v6, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->longitude:D
    invoke-static {v6}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$500(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D

    move-result-wide v6

    invoke-virtual/range {v0 .. v8}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addMessage(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/StringBuilder;)Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    move-result-object v9

    .line 289
    .local v9, "error":Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;
    sget-object v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;->NONE:Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    if-eq v9, v0, :cond_1

    move v10, v12

    .line 290
    .local v10, "hasFailed":Z
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->destinationLabel:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$300(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v11, "Location"

    .line 291
    .local v11, "shareType":Ljava/lang/String;
    :goto_1
    if-eqz v10, :cond_0

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->message:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$200(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$100(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Lcom/navdy/hud/app/framework/contacts/Contact;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->destinationLabel:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$300(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->latitude:D
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$400(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D

    move-result-wide v4

    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->longitude:D
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$500(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)D

    move-result-wide v6

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->addGlympseOfflineGlance(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V
    invoke-static/range {v1 .. v7}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$600(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V

    .line 294
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Glympse:retry sucess:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v10, :cond_3

    move v0, v12

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 295
    if-nez v10, :cond_4

    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->message:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;->access$200(Lcom/navdy/hud/app/framework/glympse/GlympseNotification;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v11, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlympseSent(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-void

    .end local v10    # "hasFailed":Z
    .end local v11    # "shareType":Ljava/lang/String;
    :cond_1
    move v10, v13

    .line 289
    goto :goto_0

    .line 290
    .restart local v10    # "hasFailed":Z
    :cond_2
    const-string v11, "Trip"

    goto :goto_1

    .restart local v11    # "shareType":Ljava/lang/String;
    :cond_3
    move v0, v13

    .line 294
    goto :goto_2

    :cond_4
    move v12, v13

    .line 295
    goto :goto_3
.end method
