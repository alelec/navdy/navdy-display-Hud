.class Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;
.super Ljava/lang/Object;
.source "GlympseManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseManager;->setUpEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 392
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 3
    .param p1, "gGlympse"    # Lcom/glympse/android/api/GGlympse;
    .param p2, "listener"    # I
    .param p3, "events"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 395
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    const/16 v2, 0x80

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->hasGlympseEvent(II)Z
    invoke-static {v1, p3, v2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$600(Lcom/navdy/hud/app/framework/glympse/GlympseManager;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "synced with server, setting user profile"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 397
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->setUserProfile()V
    invoke-static {v1}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$700(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    const/high16 v2, 0x20000

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->hasGlympseEvent(II)Z
    invoke-static {v1, p3, v2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$600(Lcom/navdy/hud/app/framework/glympse/GlympseManager;II)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v0, p4

    .line 401
    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 402
    .local v0, "ticket":Lcom/glympse/android/api/GTicket;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$2;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    # invokes: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addTicketListener(Lcom/glympse/android/api/GTicket;)V
    invoke-static {v1, v0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$800(Lcom/navdy/hud/app/framework/glympse/GlympseManager;Lcom/glympse/android/api/GTicket;)V

    .line 404
    .end local v0    # "ticket":Lcom/glympse/android/api/GTicket;
    :cond_1
    return-void
.end method
