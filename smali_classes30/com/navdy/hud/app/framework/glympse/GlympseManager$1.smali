.class Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;
.super Ljava/lang/Object;
.source "GlympseManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/glympse/GlympseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;->this$0:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 124
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isSynced()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "update ETA: glympse not synched"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 207
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/glympse/GlympseManager$1$1;-><init>(Lcom/navdy/hud/app/framework/glympse/GlympseManager$1;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 206
    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$400()Landroid/os/Handler;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/framework/glympse/GlympseManager;->UPDATE_ETA_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->access$500()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
