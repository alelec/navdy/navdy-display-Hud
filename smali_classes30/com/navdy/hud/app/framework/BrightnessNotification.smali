.class public Lcom/navdy/hud/app/framework/BrightnessNotification;
.super Landroid/database/ContentObserver;
.source "BrightnessNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field private static final DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT:I

.field private static final DEFAULT_BRIGHTNESS_INT:I

.field private static final DETENT_TIMEOUT:J = 0xfaL

.field private static final HANDLER:Landroid/os/Handler;

.field private static final INITIAL_CHANGING_STEP:I = 0x1

.field private static final MAX_BRIGHTNESS:I = 0xff

.field private static final MAX_STEP:I = 0x10

.field private static final NOTIFICATION_TIMEOUT:I = 0x2710

.field private static final SCREEN_BRIGHTNESS_SETTING_URI:Landroid/net/Uri;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final AUTO_BRIGHTNESS_ADJ_ENABLED:Z

.field private autoSuffix:Ljava/lang/String;

.field private changingStep:I

.field private choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field private container:Landroid/view/ViewGroup;

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private currentAdjustmentValue:I

.field private currentValue:I

.field private isAutoBrightnessOn:Z

.field private isLastChangeIncreasing:Z

.field private lastChangeActionTime:J

.field private previousAdjustmentValue:I

.field private previousBrightnessValue:I

.field private progressBar:Lcom/navdy/hud/app/view/Gauge;

.field private resources:Landroid/content/res/Resources;

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

.field private showAutoBrightnessAdjustmentUI:Z

.field private textIndicator:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/BrightnessNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 48
    const-string v0, "128"

    .line 49
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    .line 50
    const-string v0, "0"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT:I

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->HANDLER:Landroid/os/Handler;

    .line 69
    const-string v0, "screen_brightness"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->SCREEN_BRIGHTNESS_SETTING_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 84
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 59
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->autoSuffix:Ljava/lang/String;

    .line 65
    iput v5, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    .line 66
    const-wide/16 v2, -0xfb

    iput-wide v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->lastChangeActionTime:J

    .line 67
    iput-boolean v4, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isLastChangeIncreasing:Z

    .line 80
    iput-boolean v4, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->showAutoBrightnessAdjustmentUI:Z

    .line 81
    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->AUTO_BRIGHTNESS_ADJ_ENABLED:Z

    .line 85
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 86
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 87
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    .line 88
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentIsAutoBrightnessOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    .line 89
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->showAutoBrightnessAdjustmentUI:Z

    .line 90
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->resources:Landroid/content/res/Resources;

    .line 91
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->resources:Landroid/content/res/Resources;

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->autoSuffix:Ljava/lang/String;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/BrightnessNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/BrightnessNotification;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->close()V

    return-void
.end method

.method private close()V
    .locals 3

    .prologue
    .line 274
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 275
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    if-eqz v0, :cond_0

    .line 276
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->previousBrightnessValue:I

    iget v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->previousAdjustmentValue:I

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentAutoBrigthnessAdjustment()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordAutobrightnessAdjustmentChanged(III)V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->previousBrightnessValue:I

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentBrightness()I

    move-result v1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordBrightnessChanged(II)V

    goto :goto_0
.end method

.method private detentChangingStep(Z)V
    .locals 7
    .param p1, "isChangeIncreasing"    # Z

    .prologue
    const/16 v6, 0x10

    .line 297
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 298
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isLastChangeIncreasing:Z

    if-ne p1, v2, :cond_1

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->lastChangeActionTime:J

    const-wide/16 v4, 0xfa

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-ltz v2, :cond_1

    .line 300
    iget v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    .line 301
    iget v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    if-le v2, v6, :cond_0

    .line 302
    iput v6, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    .line 307
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isLastChangeIncreasing:Z

    .line 308
    iput-wide v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->lastChangeActionTime:J

    .line 309
    return-void

    .line 305
    :cond_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    goto :goto_0
.end method

.method private getCurrentAutoBrigthnessAdjustment()I
    .locals 4

    .prologue
    .line 374
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "screen.auto_brightness_adj"

    const-string v3, "0"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 377
    :goto_0
    return v1

    .line 375
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot parse auto brightness adjustment from the shared preferences"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 377
    sget v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT:I

    goto :goto_0
.end method

.method private getCurrentBrightness()I
    .locals 4

    .prologue
    .line 353
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    if-eqz v1, :cond_0

    .line 355
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 366
    :goto_0
    return v1

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Settings not found exception "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 358
    sget v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    goto :goto_0

    .line 362
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "screen.brightness"

    const-string v3, "128"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    goto :goto_0

    .line 364
    :catch_1
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot parse brightness from the shared preferences"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 366
    sget v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    goto :goto_0
.end method

.method private getCurrentIsAutoBrightnessOn()Z
    .locals 4

    .prologue
    .line 349
    const-string v0, "true"

    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "screen.auto_brightness"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private showFixedChoices(Landroid/view/ViewGroup;)V
    .locals 10
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    const v2, 0x7f020128

    .line 140
    const v0, 0x7f0e00d2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 142
    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    .local v8, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 145
    .local v9, "resources":Landroid/content/res/Resources;
    const v0, 0x7f09003e

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 146
    .local v6, "dismiss":Ljava/lang/String;
    const v0, 0x7f0d0024

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 147
    .local v3, "okColor":I
    const/high16 v5, -0x1000000

    .line 151
    .local v5, "unselectedColor":I
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v1, 0x2

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v1, 0x1

    new-instance v2, Lcom/navdy/hud/app/framework/BrightnessNotification$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/BrightnessNotification$1;-><init>(Lcom/navdy/hud/app/framework/BrightnessNotification;)V

    invoke-virtual {v0, v8, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 165
    return-void
.end method

.method public static showNotification()V
    .locals 2

    .prologue
    .line 270
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/BrightnessNotification;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/BrightnessNotification;-><init>()V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 271
    return-void
.end method

.method private updateBrightness(I)V
    .locals 4
    .param p1, "delta"    # I

    .prologue
    .line 395
    if-nez p1, :cond_0

    .line 429
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->showAutoBrightnessAdjustmentUI:Z

    if-eqz v1, :cond_3

    .line 409
    iget v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentAdjustmentValue:I

    add-int v0, v1, p1

    .line 410
    .local v0, "newValue":I
    const/16 v1, -0x40

    if-ge v0, v1, :cond_1

    .line 411
    const/16 v0, -0x40

    .line 413
    :cond_1
    const/16 v1, 0x40

    if-le v0, v1, :cond_2

    .line 414
    const/16 v0, 0x40

    .line 416
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "screen.auto_brightness_adj"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 417
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 419
    .end local v0    # "newValue":I
    :cond_3
    iget v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    add-int v0, v1, p1

    .line 420
    .restart local v0    # "newValue":I
    if-gez v0, :cond_4

    .line 421
    const/4 v0, 0x0

    .line 423
    :cond_4
    const/16 v1, 0xff

    if-le v0, v1, :cond_5

    .line 424
    const/16 v0, 0xff

    .line 426
    :cond_5
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "screen.brightness"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "navdy#brightness#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 219
    const/16 v0, 0x2710

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->BRIGHTNESS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 114
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->showAutoBrightnessAdjustmentUI:Z

    if-eqz v0, :cond_1

    .line 115
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030037

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    .line 121
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->textIndicator:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0147

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/Gauge;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setAnimated(Z)V

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->showFixedChoices(Landroid/view/ViewGroup;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    return-object v0

    .line 118
    :cond_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030038

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->container:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 256
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x0

    return-object v0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 96
    if-nez p1, :cond_0

    sget-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->SCREEN_BRIGHTNESS_SETTING_URI:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateState()V

    .line 99
    :cond_0
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->close()V

    .line 285
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 248
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 293
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 313
    iget-object v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 340
    :goto_0
    return v0

    .line 317
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-eq p1, v2, :cond_1

    .line 318
    iget-object v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 320
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/BrightnessNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 322
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    :goto_1
    move v0, v1

    .line 327
    goto :goto_0

    .line 325
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->close()V

    goto :goto_1

    .line 329
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Decreasing brightness"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 331
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->detentChangingStep(Z)V

    .line 332
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateBrightness(I)V

    move v0, v1

    .line 333
    goto :goto_0

    .line 335
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Increasing brightness"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 336
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/BrightnessNotification;->detentChangingStep(Z)V

    .line 337
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->changingStep:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateBrightness(I)V

    move v0, v1

    .line 338
    goto :goto_0

    .line 320
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 244
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 383
    const-string v1, "screen.brightness"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "screen.auto_brightness_adj"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 384
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateState()V

    .line 392
    :cond_1
    :goto_0
    return-void

    .line 385
    :cond_2
    const-string v1, "screen.auto_brightness"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    const-string v1, "true"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 387
    .local v0, "newValue":Z
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    if-eq v1, v0, :cond_1

    .line 388
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    .line 389
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateState()V

    goto :goto_0
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 3
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 169
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/BrightnessNotification;->SCREEN_BRIGHTNESS_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 170
    iput-object p1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 172
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentBrightness()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->previousBrightnessValue:I

    .line 173
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentAutoBrigthnessAdjustment()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->previousAdjustmentValue:I

    .line 174
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateState()V

    .line 175
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 213
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 215
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 289
    return-void
.end method

.method public onUpdate()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->updateState()V

    .line 208
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public updateState()V
    .locals 7

    .prologue
    const/16 v6, 0xff

    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_0

    .line 179
    sget-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "brightness notif offscreen"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentBrightness()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    .line 183
    sget-object v0, Lcom/navdy/hud/app/framework/BrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current brightness :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 184
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->showAutoBrightnessAdjustmentUI:Z

    if-nez v0, :cond_2

    .line 185
    iget-object v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->textIndicator:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090036

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v5, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    .line 187
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x2

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->isAutoBrightnessOn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->autoSuffix:Ljava/lang/String;

    :goto_1
    aput-object v0, v4, v5

    .line 185
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    iget v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setValue(I)V

    goto :goto_0

    .line 187
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 190
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/BrightnessNotification;->getCurrentAutoBrigthnessAdjustment()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentAdjustmentValue:I

    .line 191
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    iget v1, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentAdjustmentValue:I

    add-int/lit8 v1, v1, 0x40

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setValue(I)V

    .line 192
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    if-lt v0, v6, :cond_3

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->textIndicator:Landroid/widget/TextView;

    const v1, 0x7f0901a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 194
    :cond_3
    iget v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->currentValue:I

    if-gtz v0, :cond_4

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->textIndicator:Landroid/widget/TextView;

    const v1, 0x7f0901ac

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/BrightnessNotification;->textIndicator:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
