.class public Lcom/navdy/hud/app/framework/toast/ToastManager;
.super Ljava/lang/Object;
.source "ToastManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;,
        Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;,
        Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;,
        Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;
    }
.end annotation


# static fields
.field private static final sInstance:Lcom/navdy/hud/app/framework/toast/ToastManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

.field private handler:Landroid/os/Handler;

.field private isToastDisplayDisabled:Z

.field private isToastScreenDisplayed:Z

.field private nextToast:Ljava/lang/Runnable;

.field private queue:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/toast/ToastManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sInstance:Lcom/navdy/hud/app/framework/toast/ToastManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v1, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 92
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z

    .line 94
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    .line 96
    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/toast/ToastManager$1;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->nextToast:Ljava/lang/Runnable;

    .line 140
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 141
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->bus:Lcom/squareup/otto/Bus;

    .line 142
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 143
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/toast/ToastManager;)Ljava/util/concurrent/LinkedBlockingDeque;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/toast/ToastManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/toast/ToastManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->showToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToastInternal(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/toast/ToastManager;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToastInternal(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/toast/ToastManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToastInternal(Ljava/lang/String;)V

    return-void
.end method

.method private addToastInternal(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V
    .locals 6
    .param p1, "toastParams"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    .prologue
    .line 167
    iget-object v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->data:Landroid/os/Bundle;

    if-nez v2, :cond_1

    .line 168
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "invalid toast passed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v1

    .line 173
    .local v1, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-nez v1, :cond_2

    .line 174
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "toast ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] not accepted, Main screen not on"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isLocked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "locked, cannot add toast["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :cond_3
    iget-object v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->data:Landroid/os/Bundle;

    const-string v3, "id"

    iget-object v4, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 187
    .local v0, "info":Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v3

    .line 188
    :try_start_0
    iget-boolean v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->makeCurrent:Z

    if-eqz v2, :cond_6

    .line 189
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->dismissed:Z

    if-nez v2, :cond_5

    .line 190
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    .line 191
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "replace current, item to front:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    .line 193
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 202
    :cond_4
    :goto_1
    monitor-exit v3

    goto/16 :goto_0

    .line 221
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 195
    :cond_5
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "replace current not reqd"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    .line 197
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    if-nez v2, :cond_4

    .line 198
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "replace called next:run"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 199
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->nextToast:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 205
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v2

    if-gtz v2, :cond_7

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v2, :cond_8

    .line 207
    :cond_7
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] wait for display to be cleared size["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v5}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] current["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] displayed["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 209
    monitor-exit v3

    goto/16 :goto_0

    .line 212
    :cond_8
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z

    if-eqz v2, :cond_9

    .line 213
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] toasts disabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 214
    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 215
    monitor-exit v3

    goto/16 :goto_0

    .line 218
    :cond_9
    iput-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 219
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "push toast to display"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 220
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->showToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    .line 221
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private clearPendingToastInternal(Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 345
    iget-object v4, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v4

    .line 346
    if-nez p1, :cond_1

    .line 347
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removed-all:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v6}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 348
    iget-object v3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v3}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 349
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 350
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    .line 363
    :cond_0
    monitor-exit v4

    .line 364
    return-void

    .line 352
    :cond_1
    const/4 v0, 0x0

    .line 353
    .local v0, "found":Z
    iget-object v3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v3}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 354
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;>;"
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 355
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v2, v3, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    .line 356
    .local v2, "toastId":Ljava/lang/String;
    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 357
    sget-object v3, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removed:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 358
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 359
    const/4 v0, 0x1

    goto :goto_0

    .line 363
    .end local v0    # "found":Z
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;>;"
    .end local v2    # "toastId":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private dismissCurrentToast(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "disable"    # Z

    .prologue
    .line 280
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToastInternal(Ljava/lang/String;Z)V

    .line 290
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/framework/toast/ToastManager$3;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private dismissCurrentToastInternal(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "disable"    # Z

    .prologue
    .line 293
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-nez v0, :cond_0

    .line 309
    :goto_0
    return-void

    .line 296
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 297
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current toast["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] does not match["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->removeOnDisable:Z

    if-eqz v0, :cond_2

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 302
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismiss current toast["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] removed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 308
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->dismiss()V

    goto :goto_0

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->dismissed:Z

    .line 305
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismiss current toast["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] saved"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sInstance:Lcom/navdy/hud/app/framework/toast/ToastManager;

    return-object v0
.end method

.method private isLocked()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->lock:Z

    .line 396
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .prologue
    .line 226
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[show-toast] id["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getToastView()Lcom/navdy/hud/app/view/ToastView;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->present(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    .line 228
    return-void
.end method


# virtual methods
.method public addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V
    .locals 2
    .param p1, "toastParams"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    .prologue
    .line 149
    if-nez p1, :cond_0

    .line 150
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "invalid toast params"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 163
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToastInternal(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager$2;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public clearAllPendingToast()V
    .locals 2

    .prologue
    .line 319
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToastInternal(Ljava/lang/String;)V

    .line 329
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/toast/ToastManager$4;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public clearPendingToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 332
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToastInternal(Ljava/lang/String;)V

    .line 342
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager$5;-><init>(Lcom/navdy/hud/app/framework/toast/ToastManager;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public varargs clearPendingToast([Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 313
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 314
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 313
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 316
    .end local v0    # "s":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public disableToasts(Z)V
    .locals 3
    .param p1, "disable"    # Z

    .prologue
    .line 369
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disableToast ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 370
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z

    if-ne v0, p1, :cond_0

    .line 371
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "disableToast same state as before"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 390
    :goto_0
    return-void

    .line 374
    :cond_0
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z

    .line 375
    if-eqz p1, :cond_2

    .line 376
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "disableToast locked, cannot disable toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Z)V

    goto :goto_0

    .line 382
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v1

    .line 383
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v0, :cond_3

    .line 384
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->showToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    .line 388
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 386
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->nextToast:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public dismissCurrentToast()V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;Z)V

    .line 269
    return-void
.end method

.method public dismissCurrentToast(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 276
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;Z)V

    .line 277
    return-void
.end method

.method dismissCurrentToast(Z)V
    .locals 1
    .param p1, "disable"    # Z

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;Z)V

    .line 273
    return-void
.end method

.method public varargs dismissCurrentToast([Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 256
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 257
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 259
    .end local v0    # "s":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public dismissToast(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 263
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 264
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public getCallback(Ljava/lang/String;)Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 232
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 233
    :cond_0
    const/4 v0, 0x0

    .line 236
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->cb:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    goto :goto_0
.end method

.method public getCurrentToastId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-nez v0, :cond_0

    .line 249
    const/4 v0, 0x0

    .line 251
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    goto :goto_0
.end method

.method public isCurrentToast(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 240
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x1

    .line 243
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isToastDisplayed()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    return v0
.end method

.method public setToastDisplayFlag(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    .line 128
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->nextToast:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager;->nextToast:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 133
    :cond_0
    return-void
.end method
