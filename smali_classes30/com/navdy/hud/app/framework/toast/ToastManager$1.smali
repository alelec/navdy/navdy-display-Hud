.class Lcom/navdy/hud/app/framework/toast/ToastManager$1;
.super Ljava/lang/Object;
.source "ToastManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/toast/ToastManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/toast/ToastManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/toast/ToastManager;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 99
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$000(Lcom/navdy/hud/app/framework/toast/ToastManager;)Ljava/util/concurrent/LinkedBlockingDeque;

    move-result-object v2

    monitor-enter v2

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$000(Lcom/navdy/hud/app/framework/toast/ToastManager;)Ljava/util/concurrent/LinkedBlockingDeque;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 101
    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v3, "no pending toast"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 102
    monitor-exit v2

    .line 119
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastScreenDisplayed:Z
    invoke-static {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$200(Lcom/navdy/hud/app/framework/toast/ToastManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v3, "cannot display toast screen still on"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 107
    monitor-exit v2

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 110
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayDisabled:Z
    invoke-static {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$300(Lcom/navdy/hud/app/framework/toast/ToastManager;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v3, "toasts disabled"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 112
    monitor-exit v2

    goto :goto_0

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # getter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->queue:Ljava/util/concurrent/LinkedBlockingDeque;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$000(Lcom/navdy/hud/app/framework/toast/ToastManager;)Ljava/util/concurrent/LinkedBlockingDeque;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 116
    .local v0, "info":Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # setter for: Lcom/navdy/hud/app/framework/toast/ToastManager;->currentToast:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;
    invoke-static {v1, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$402(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 117
    iget-object v1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$1;->this$0:Lcom/navdy/hud/app/framework/toast/ToastManager;

    # invokes: Lcom/navdy/hud/app/framework/toast/ToastManager;->showToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V
    invoke-static {v1, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->access$500(Lcom/navdy/hud/app/framework/toast/ToastManager;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    .line 118
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
