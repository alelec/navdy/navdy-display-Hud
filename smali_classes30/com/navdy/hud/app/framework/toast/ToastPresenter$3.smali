.class final Lcom/navdy/hud/app/framework/toast/ToastPresenter$3;
.super Ljava/lang/Object;
.source "ToastPresenter.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/toast/ToastPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 162
    # getter for: Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->access$200()Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    # getter for: Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->access$200()Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->executeChoiceItem(II)V

    .line 165
    :cond_0
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 169
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->resetTimeout()V

    .line 170
    return-void
.end method
