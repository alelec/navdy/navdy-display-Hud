.class public final Lcom/navdy/hud/app/framework/toast/ToastPresenter;
.super Ljava/lang/Object;
.source "ToastPresenter.java"


# static fields
.field public static final ANIMATION_TRANSLATION:I

.field private static final DISMISS_CHOICE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_CHOICE_LAYOUT_PADDING:Ljava/lang/String; = "15"

.field public static final EXTRA_CHOICE_LIST:Ljava/lang/String; = "20"

.field public static final EXTRA_DEFAULT_CHOICE:Ljava/lang/String; = "12"

.field public static final EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH:Ljava/lang/String; = "16_1"

.field public static final EXTRA_INFO_CONTAINER_LEFT_PADDING:Ljava/lang/String; = "16_2"

.field public static final EXTRA_INFO_CONTAINER_MAX_WIDTH:Ljava/lang/String; = "16"

.field public static final EXTRA_MAIN_IMAGE:Ljava/lang/String; = "8"

.field public static final EXTRA_MAIN_IMAGE_CACHE_KEY:Ljava/lang/String; = "9"

.field public static final EXTRA_MAIN_IMAGE_INITIALS:Ljava/lang/String; = "10"

.field public static final EXTRA_MAIN_TITLE:Ljava/lang/String; = "1"

.field public static final EXTRA_MAIN_TITLE_1:Ljava/lang/String; = "2"

.field public static final EXTRA_MAIN_TITLE_1_STYLE:Ljava/lang/String; = "3"

.field public static final EXTRA_MAIN_TITLE_2:Ljava/lang/String; = "4"

.field public static final EXTRA_MAIN_TITLE_2_STYLE:Ljava/lang/String; = "5"

.field public static final EXTRA_MAIN_TITLE_3:Ljava/lang/String; = "6"

.field public static final EXTRA_MAIN_TITLE_3_STYLE:Ljava/lang/String; = "7"

.field public static final EXTRA_MAIN_TITLE_STYLE:Ljava/lang/String; = "1_1"

.field public static final EXTRA_NO_START_DELAY:Ljava/lang/String; = "21"

.field public static final EXTRA_SHOW_SCREEN_ID:Ljava/lang/String; = "18"

.field public static final EXTRA_SIDE_IMAGE:Ljava/lang/String; = "11"

.field public static final EXTRA_SUPPORTS_GESTURE:Ljava/lang/String; = "19"

.field public static final EXTRA_TIMEOUT:Ljava/lang/String; = "13"

.field public static final EXTRA_TOAST_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_TTS:Ljava/lang/String; = "17"

.field public static final TOAST_ANIM_DURATION_IN:I = 0x64

.field public static final TOAST_ANIM_DURATION_OUT:I = 0x64

.field public static final TOAST_ANIM_DURATION_OUT_QUICK:I = 0x32

.field public static final TOAST_START_DELAY:I = 0xfa

.field private static final bus:Lcom/squareup/otto/Bus;

.field private static choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private static currentInfo:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

.field private static defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field public static final defaultMaxInfoWidth:I

.field private static gestureLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

.field private static final gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

.field private static handler:Landroid/os/Handler;

.field private static id:Ljava/lang/String;

.field private static mainSectionBottomPadding:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile screenName:Ljava/lang/String;

.field private static supportsGesture:Z

.field private static timeout:I

.field private static timeoutRunnable:Ljava/lang/Runnable;

.field private static toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

.field private static toastView:Lcom/navdy/hud/app/view/ToastView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/hud/app/framework/toast/ToastPresenter;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 107
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->DISMISS_CHOICE:Ljava/util/List;

    .line 113
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 114
    .local v1, "resources":Landroid/content/res/Resources;
    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->DISMISS_CHOICE:Ljava/util/List;

    const v3, 0x7f0900dc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const v2, 0x7f0b018c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->ANIMATION_TRANSLATION:I

    .line 116
    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->defaultMaxInfoWidth:I

    .line 117
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 118
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->bus:Lcom/squareup/otto/Bus;

    .line 119
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getGestureServiceConnector()Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 123
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    .line 125
    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter$1;

    invoke-direct {v2}, Lcom/navdy/hud/app/framework/toast/ToastPresenter$1;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    .line 143
    const/4 v2, -0x1

    sput v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->mainSectionBottomPadding:I

    .line 149
    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter$2;

    invoke-direct {v2}, Lcom/navdy/hud/app/framework/toast/ToastPresenter$2;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 159
    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter$3;

    invoke-direct {v2}, Lcom/navdy/hud/app/framework/toast/ToastPresenter$3;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/hud/app/view/ToastView;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    return-object v0
.end method

.method static synthetic access$300(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 40
    invoke-static {p0}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->dismissInternal(Z)V

    return-void
.end method

.method static synthetic access$400()V
    .locals 0

    .prologue
    .line 40
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->clearInternal()V

    return-void
.end method

.method private static applyTextAndStyle(Landroid/widget/TextView;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "textAttribute"    # Ljava/lang/String;
    .param p3, "styleAttribute"    # Ljava/lang/String;
    .param p4, "maxLines"    # I

    .prologue
    .line 432
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {p0, v0, p4, v1}, Lcom/navdy/hud/app/util/ViewUtil;->applyTextAndStyle(Landroid/widget/TextView;Ljava/lang/CharSequence;II)I

    move-result v0

    return v0
.end method

.method public static clear()V
    .locals 2

    .prologue
    .line 213
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->clearInternal()V

    .line 224
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter$5;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/toast/ToastPresenter$5;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private static clearInternal()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 227
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearInternal"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 228
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 229
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    if-eqz v0, :cond_0

    .line 230
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "called onStop():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 231
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->onStop()V

    .line 232
    sget-boolean v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    if-eqz v0, :cond_0

    .line 233
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->gestureLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-static {v0}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->removeSettings(Lcom/navdy/hud/app/device/light/LED$Settings;)V

    .line 237
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;

    sget-object v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 239
    sput-object v3, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->currentInfo:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 240
    sput-boolean v4, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    .line 241
    sput-object v3, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    .line 242
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "toast-cb null"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 243
    sput-object v3, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 244
    sput-object v3, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    .line 245
    sput v4, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    .line 246
    return-void
.end method

.method public static clearScreenName()V
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->screenName:Ljava/lang/String;

    .line 437
    return-void
.end method

.method public static dismiss()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->dismiss(Z)V

    .line 175
    return-void
.end method

.method public static dismiss(Z)V
    .locals 2
    .param p0, "quickly"    # Z

    .prologue
    .line 178
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-static {p0}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->dismissInternal(Z)V

    .line 188
    :goto_0
    return-void

    .line 181
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/toast/ToastPresenter$4;-><init>(Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private static dismissInternal(Z)V
    .locals 2
    .param p0, "quickly"    # Z

    .prologue
    .line 191
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dismissInternal"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 193
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    if-eqz v0, :cond_0

    .line 194
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/view/ToastView;->animateOut(Z)V

    .line 196
    :cond_0
    return-void
.end method

.method public static getCurrentCallback()Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    return-object v0
.end method

.method public static getCurrentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    return-object v0
.end method

.method public static getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method public static hasTimeout()Z
    .locals 1

    .prologue
    .line 265
    sget v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static present(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V
    .locals 2
    .param p0, "view"    # Lcom/navdy/hud/app/view/ToastView;
    .param p1, "info"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .prologue
    .line 199
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 200
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "invalid toast info"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 203
    :cond_1
    sput-object p1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->currentInfo:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .line 204
    sput-object p0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 205
    sget v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->mainSectionBottomPadding:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 207
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ToastView;->getMainLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->mainSectionBottomPadding:I

    .line 209
    :cond_2
    invoke-static {p0, p1}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->updateView(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V

    goto :goto_0
.end method

.method public static resetTimeout()V
    .locals 4

    .prologue
    .line 249
    sget v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    if-lez v0, :cond_0

    .line 250
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 251
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 252
    sget-object v0, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset timeout = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 254
    :cond_0
    return-void
.end method

.method private static updateView(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;)V
    .locals 32
    .param p0, "view"    # Lcom/navdy/hud/app/view/ToastView;
    .param p1, "info"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;

    .prologue
    .line 270
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 271
    .local v8, "context":Landroid/content/Context;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->toastParams:Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v9, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->data:Landroid/os/Bundle;

    .line 273
    .local v9, "data":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/ToastView;->getConfirmation()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v14

    .line 275
    .local v14, "layout":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const v28, 0x7f0c0021

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 276
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const v28, 0x7f0c002b

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 277
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const v28, 0x7f0c002c

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 278
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const v28, 0x7f0c002d

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 280
    const-string v27, "15"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v22

    .line 281
    .local v22, "padding":I
    if-lez v22, :cond_0

    .line 282
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/widget/RelativeLayout$LayoutParams;

    .line 283
    .local v23, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 284
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 285
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 286
    move/from16 v0, v22

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 289
    .end local v23    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    const-string v27, "id"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    sput-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    .line 291
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v27

    sget-object v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCallback(Ljava/lang/String;)Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-result-object v27

    sput-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    .line 292
    sget-object v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "toast-cb-update :"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v29, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    if-nez v27, :cond_8

    const-string v27, "null"

    :goto_0
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 294
    const-string v27, "8"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v18

    .line 295
    .local v18, "mainImage":I
    const-string v27, "9"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 296
    .local v19, "mainImagePath":Ljava/lang/String;
    const-string v27, "10"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 297
    .local v13, "initials":Ljava/lang/String;
    const-string v27, "13"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v27

    sput v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    .line 298
    const-string v27, "12"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 299
    .local v10, "defaultChoice":Z
    const-string v27, "11"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v24

    .line 300
    .local v24, "sideImage":I
    const/16 v25, 0x0

    .line 303
    .local v25, "tts":Ljava/lang/String;
    const-string v27, "18"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    sput-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->screenName:Ljava/lang/String;

    .line 304
    const-string v27, "19"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v27

    sput-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    .line 306
    sget-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    if-eqz v27, :cond_1

    .line 307
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->isRunning()Z

    move-result v27

    if-nez v27, :cond_1

    .line 308
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "gesture not available, turn off"

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 309
    const/16 v27, 0x0

    sput-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    .line 313
    :cond_1
    const-string v27, "20"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 315
    .local v7, "choices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    const v27, 0x7f0e00bf

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 316
    .local v11, "infoContainer":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    const/16 v20, 0x0

    .line 317
    .local v20, "maxWidth":I
    const-string v27, "16"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 318
    const-string v27, "16"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 319
    .local v12, "infoContainerWidth":I
    if-lez v12, :cond_9

    move/from16 v20, v12

    .line 324
    .end local v12    # "infoContainerWidth":I
    :cond_2
    :goto_1
    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 325
    invoke-virtual {v11}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 326
    .local v15, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v27, -0x2

    move/from16 v0, v27

    iput v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 328
    const-string v27, "16_2"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 329
    const-string v27, "16_2"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 330
    .local v16, "leftPadding":I
    invoke-virtual {v11}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getPaddingTop()I

    move-result v27

    .line 331
    invoke-virtual {v11}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getPaddingRight()I

    move-result v28

    invoke-virtual {v11}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getPaddingBottom()I

    move-result v29

    .line 330
    move/from16 v0, v16

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setPadding(IIII)V

    .line 334
    .end local v16    # "leftPadding":I
    :cond_3
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->fluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 336
    const/16 v17, 0x0

    .line 337
    .local v17, "lines":I
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const-string v28, "1"

    const-string v29, "1_1"

    const/16 v30, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v30

    invoke-static {v0, v9, v1, v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->applyTextAndStyle(Landroid/widget/TextView;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    add-int v17, v17, v27

    .line 338
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const-string v28, "2"

    const-string v29, "3"

    const/16 v30, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v30

    invoke-static {v0, v9, v1, v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->applyTextAndStyle(Landroid/widget/TextView;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    add-int v17, v17, v27

    .line 339
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const-string v28, "4"

    const-string v29, "5"

    const/16 v30, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v30

    invoke-static {v0, v9, v1, v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->applyTextAndStyle(Landroid/widget/TextView;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    add-int v17, v17, v27

    .line 340
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const-string v28, "6"

    const-string v29, "7"

    const/16 v30, 0x3

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v30

    invoke-static {v0, v9, v1, v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->applyTextAndStyle(Landroid/widget/TextView;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v27

    add-int v17, v17, v27

    .line 342
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    .line 343
    .local v26, "v":Landroid/view/View;
    const/16 v27, 0x4

    move/from16 v0, v17

    move/from16 v1, v27

    if-le v0, v1, :cond_b

    .line 346
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    move-object/from16 v27, v0

    sget v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->mainSectionBottomPadding:I

    .line 348
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    const v30, 0x7f0b00be

    invoke-virtual/range {v29 .. v30}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v29

    add-int v28, v28, v29

    .line 346
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/util/ViewUtil;->setBottomPadding(Landroid/view/View;I)V

    .line 353
    :goto_2
    if-eqz v18, :cond_d

    .line 354
    if-eqz v13, :cond_c

    .line 355
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    sget-object v28, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    move-object/from16 v0, v27

    move/from16 v1, v18

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v13, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 356
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 373
    :cond_4
    :goto_3
    if-eqz v24, :cond_f

    .line 374
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 375
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 380
    :goto_4
    if-eqz v10, :cond_10

    .line 381
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->DISMISS_CHOICE:Ljava/util/List;

    const/16 v28, 0x0

    sget-object v29, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v14, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 382
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setVisibility(I)V

    .line 383
    const/16 v27, 0x0

    sput-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    .line 395
    :goto_5
    sget-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    if-eqz v27, :cond_13

    .line 396
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->leftSwipe:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 397
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->rightSwipe:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 398
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lcom/navdy/hud/app/view/ToastView;->gestureOn:Z

    .line 405
    :goto_6
    sget v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    if-lez v27, :cond_5

    .line 406
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->handler:Landroid/os/Handler;

    sget-object v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeoutRunnable:Ljava/lang/Runnable;

    sget v29, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    add-int/lit8 v29, v29, 0x64

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 407
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "timeout = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget v29, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->timeout:I

    add-int/lit8 v29, v29, 0x64

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 410
    :cond_5
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    if-eqz v27, :cond_6

    .line 411
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "called onStart():"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget-object v29, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 412
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastCallback:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->onStart(Lcom/navdy/hud/app/view/ToastView;)V

    .line 414
    sget-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    if-eqz v27, :cond_6

    .line 415
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v27

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Toast"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "1"

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureDetectionEnabled(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v27

    sput-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->gestureLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 419
    :cond_6
    if-eqz v25, :cond_7

    .line 420
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->ttsDone:Z

    move/from16 v27, v0

    if-eqz v27, :cond_14

    .line 421
    const/16 v25, 0x0

    .line 427
    :cond_7
    :goto_7
    const-string v27, "21"

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    .line 428
    .local v21, "noStartDelay":Z
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->screenName:Ljava/lang/String;

    sget-object v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v27

    move-object/from16 v3, v28

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/view/ToastView;->animateIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 429
    return-void

    .line 292
    .end local v7    # "choices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    .end local v10    # "defaultChoice":Z
    .end local v11    # "infoContainer":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    .end local v13    # "initials":Ljava/lang/String;
    .end local v15    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v17    # "lines":I
    .end local v18    # "mainImage":I
    .end local v19    # "mainImagePath":Ljava/lang/String;
    .end local v20    # "maxWidth":I
    .end local v21    # "noStartDelay":Z
    .end local v24    # "sideImage":I
    .end local v25    # "tts":Ljava/lang/String;
    .end local v26    # "v":Landroid/view/View;
    :cond_8
    const-string v27, "not null"

    goto/16 :goto_0

    .line 319
    .restart local v7    # "choices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    .restart local v10    # "defaultChoice":Z
    .restart local v11    # "infoContainer":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    .restart local v12    # "infoContainerWidth":I
    .restart local v13    # "initials":Ljava/lang/String;
    .restart local v18    # "mainImage":I
    .restart local v19    # "mainImagePath":Ljava/lang/String;
    .restart local v20    # "maxWidth":I
    .restart local v24    # "sideImage":I
    .restart local v25    # "tts":Ljava/lang/String;
    :cond_9
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 320
    .end local v12    # "infoContainerWidth":I
    :cond_a
    const-string v27, "16_1"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 321
    sget v20, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->defaultMaxInfoWidth:I

    goto/16 :goto_1

    .line 350
    .restart local v15    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v17    # "lines":I
    .restart local v26    # "v":Landroid/view/View;
    :cond_b
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    move-object/from16 v27, v0

    sget v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->mainSectionBottomPadding:I

    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/util/ViewUtil;->setBottomPadding(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 358
    :cond_c
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    sget-object v29, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    move-object/from16 v0, v27

    move/from16 v1, v18

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 359
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 361
    :cond_d
    if-eqz v19, :cond_e

    .line 362
    new-instance v27, Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 363
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_4

    .line 364
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    sget-object v29, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual/range {v27 .. v29}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 365
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 366
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 369
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_e
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageResource(I)V

    .line 370
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 377
    :cond_f
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 384
    :cond_10
    if-eqz v7, :cond_12

    .line 385
    sget-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    if-eqz v27, :cond_11

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v27

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_11

    .line 386
    new-instance v27, Ljava/lang/RuntimeException;

    const-string v28, "max 2 choice allowed when gesture are on"

    invoke-direct/range {v27 .. v28}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v27

    .line 388
    :cond_11
    const/16 v27, 0x0

    sget-object v28, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    move/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v14, v7, v0, v1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setChoicesList(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 389
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setVisibility(I)V

    goto/16 :goto_5

    .line 391
    :cond_12
    const/16 v27, 0x0

    sput-boolean v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->supportsGesture:Z

    .line 392
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setVisibility(I)V

    goto/16 :goto_5

    .line 400
    :cond_13
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->leftSwipe:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 401
    iget-object v0, v14, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->rightSwipe:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    invoke-virtual/range {v27 .. v28}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 402
    sget-object v27, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->toastView:Lcom/navdy/hud/app/view/ToastView;

    move-object/from16 v0, v27

    iput-boolean v10, v0, Lcom/navdy/hud/app/view/ToastView;->gestureOn:Z

    goto/16 :goto_6

    .line 423
    :cond_14
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastInfo;->ttsDone:Z

    goto/16 :goto_7
.end method
