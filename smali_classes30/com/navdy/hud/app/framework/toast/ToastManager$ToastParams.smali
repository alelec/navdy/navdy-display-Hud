.class public Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;
.super Ljava/lang/Object;
.source "ToastManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/toast/ToastManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ToastParams"
.end annotation


# instance fields
.field cb:Lcom/navdy/hud/app/framework/toast/IToastCallback;

.field data:Landroid/os/Bundle;

.field id:Ljava/lang/String;

.field lock:Z

.field makeCurrent:Z

.field removeOnDisable:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .param p4, "removeOnDisable"    # Z
    .param p5, "makeCurrent"    # Z

    .prologue
    .line 53
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZZ)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZZ)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .param p4, "removeOnDisable"    # Z
    .param p5, "makeCurrent"    # Z
    .param p6, "lock"    # Z

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->id:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->data:Landroid/os/Bundle;

    .line 64
    iput-object p3, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->cb:Lcom/navdy/hud/app/framework/toast/IToastCallback;

    .line 65
    iput-boolean p4, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->removeOnDisable:Z

    .line 66
    iput-boolean p5, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->makeCurrent:Z

    .line 67
    iput-boolean p6, p0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;->lock:Z

    .line 68
    return-void
.end method
