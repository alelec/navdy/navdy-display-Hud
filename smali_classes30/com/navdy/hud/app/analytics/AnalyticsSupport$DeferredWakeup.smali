.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeferredWakeup"
.end annotation


# instance fields
.field batteryDrain:Ljava/lang/String;

.field batteryLevel:Ljava/lang/String;

.field maxBattery:Ljava/lang/String;

.field public reason:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

.field sleepTime:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "r"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;
    .param p2, "bLevel"    # Ljava/lang/String;
    .param p3, "bDrain"    # Ljava/lang/String;
    .param p4, "sTime"    # Ljava/lang/String;
    .param p5, "maxBat"    # Ljava/lang/String;

    .prologue
    .line 2166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->reason:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .line 2168
    iput-object p2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->batteryLevel:Ljava/lang/String;

    .line 2169
    iput-object p3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->batteryDrain:Ljava/lang/String;

    .line 2170
    iput-object p4, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->sleepTime:Ljava/lang/String;

    .line 2171
    iput-object p5, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->maxBattery:Ljava/lang/String;

    .line 2172
    return-void
.end method
