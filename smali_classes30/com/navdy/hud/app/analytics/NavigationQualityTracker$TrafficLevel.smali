.class final enum Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;
.super Ljava/lang/Enum;
.source "NavigationQualityTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TrafficLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

.field public static final enum HEAVY:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

.field public static final enum LIGHT:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

.field public static final enum MODERATE:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

.field public static final enum NO_DATA:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 182
    new-instance v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->NO_DATA:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    new-instance v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->LIGHT:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    new-instance v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    const-string v1, "MODERATE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->MODERATE:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    new-instance v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    const-string v1, "HEAVY"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->HEAVY:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    .line 181
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    sget-object v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->NO_DATA:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->LIGHT:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->MODERATE:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->HEAVY:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->$VALUES:[Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 181
    const-class v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->$VALUES:[Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    return-object v0
.end method
