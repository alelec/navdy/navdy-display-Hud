.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Threshold"
.end annotation


# instance fields
.field private final max:I

.field private final minimumTime:J

.field private triggerTime:J


# direct methods
.method constructor <init>(IJ)V
    .locals 2
    .param p1, "max"    # I
    .param p2, "minimumTime"    # J

    .prologue
    .line 1008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    .line 1009
    iput p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->max:I

    .line 1010
    iput-wide p2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->minimumTime:J

    .line 1011
    return-void
.end method


# virtual methods
.method isHit()Z
    .locals 4

    .prologue
    .line 1024
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1025
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->minimumTime:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(I)V
    .locals 4
    .param p1, "val"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 1014
    iget v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->max:I

    if-lt p1, v0, :cond_1

    .line 1015
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1016
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    .line 1021
    :cond_0
    :goto_0
    return-void

    .line 1019
    :cond_1
    iput-wide v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->triggerTime:J

    goto :goto_0
.end method
