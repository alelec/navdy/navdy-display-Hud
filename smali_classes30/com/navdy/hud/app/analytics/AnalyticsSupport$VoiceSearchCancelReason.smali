.class final enum Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
.super Ljava/lang/Enum;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VoiceSearchCancelReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum cancel_list:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum cancel_route_calculation:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum cancel_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum end_trip:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum new_non_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

.field public static final enum new_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1640
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "end_trip"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->end_trip:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1641
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "new_voice_search"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1642
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "new_non_voice_search"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_non_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1643
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "cancel_search"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1644
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "cancel_route_calculation"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_route_calculation:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1645
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    const-string v1, "cancel_list"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_list:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 1639
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->end_trip:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_non_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_route_calculation:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_list:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1639
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1639
    const-class v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    .locals 1

    .prologue
    .line 1639
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    return-object v0
.end method
