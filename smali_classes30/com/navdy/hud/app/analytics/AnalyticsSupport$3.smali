.class synthetic Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1782
    invoke-static {}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->values()[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    .line 919
    :goto_4
    invoke-static {}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->values()[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_NEVER:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    .line 826
    :goto_7
    invoke-static {}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->values()[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_EXPERIMENTAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    .line 919
    :catch_3
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_5

    .line 1782
    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_2

    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
