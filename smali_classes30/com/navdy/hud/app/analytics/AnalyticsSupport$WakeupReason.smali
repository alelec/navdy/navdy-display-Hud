.class public final enum Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;
.super Ljava/lang/Enum;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WakeupReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

.field public static final enum DIAL:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

.field public static final enum PHONE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

.field public static final enum POWERBUTTON:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

.field public static final enum VOLTAGE_SPIKE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;


# instance fields
.field public final attr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2134
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    const-string v1, "POWERBUTTON"

    const-string v2, "Power_Button"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->POWERBUTTON:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .line 2135
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    const-string v1, "VOLTAGE_SPIKE"

    const-string v2, "Voltage_Spike"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->VOLTAGE_SPIKE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .line 2136
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    const-string v1, "DIAL"

    const-string v2, "Dial"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->DIAL:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .line 2137
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    const-string v1, "PHONE"

    const-string v2, "Phone"

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->PHONE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .line 2133
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->POWERBUTTON:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->VOLTAGE_SPIKE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->DIAL:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->PHONE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "attr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2141
    iput-object p3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->attr:Ljava/lang/String;

    .line 2142
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2133
    const-class v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;
    .locals 1

    .prologue
    .line 2133
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    return-object v0
.end method
