.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventSender"
.end annotation


# instance fields
.field private event:Lcom/navdy/hud/app/analytics/Event;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/analytics/Event;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/analytics/Event;

    .prologue
    .line 2264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2265
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;->event:Lcom/navdy/hud/app/analytics/Event;

    .line 2266
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2269
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;->event:Lcom/navdy/hud/app/analytics/Event;

    iget-object v0, v0, Lcom/navdy/hud/app/analytics/Event;->tag:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;->event:Lcom/navdy/hud/app/analytics/Event;

    iget-object v1, v1, Lcom/navdy/hud/app/analytics/Event;->argMap:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/localytics/android/Localytics;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 2270
    return-void
.end method
