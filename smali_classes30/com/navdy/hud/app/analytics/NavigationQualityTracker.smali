.class public Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
.super Ljava/lang/Object;
.source "NavigationQualityTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;,
        Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;
    }
.end annotation


# static fields
.field private static final INVALID:I = -0x1

.field private static final instance:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private isRoutingWithTraffic:Z

.field private report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

.field private tripDurationIntervalInit:J

.field private tripStartUtc:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    invoke-direct {v0}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->instance:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    .line 31
    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->instance:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized cancelTrip()V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    const-wide/16 v0, -0x1

    :try_start_0
    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->isRoutingWithTraffic:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getActualDurationSoFar()I
    .locals 4

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getExpectedDistance()I
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, -0x1

    .line 118
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    iget-wide v0, v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    long-to-int v0, v0

    goto :goto_0
.end method

.method public getExpectedDuration()I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    if-nez v0, :cond_0

    .line 106
    const/4 v0, -0x1

    .line 108
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    iget-wide v0, v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    long-to-int v0, v0

    goto :goto_0
.end method

.method public getTripStartUtc()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    return-wide v0
.end method

.method public declared-synchronized trackCalculationWithTraffic(Z)V
    .locals 1
    .param p1, "factoringInTraffic"    # Z

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->isRoutingWithTraffic:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized trackTripEnded(J)V
    .locals 9
    .param p1, "distanceCovered"    # J

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    if-nez v2, :cond_0

    .line 62
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "report is null, no-op"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 68
    .local v0, "tripEndUtc":J
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tripEndUtc: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 69
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tripEndUtc - tripStartUtc, should be same as actualDuration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 71
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iput-wide v4, v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDuration:J

    .line 72
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    iput-wide p1, v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDistance:J

    .line 74
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "route has ended, submitting navigation quality report: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 76
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->isValid()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->submitNavigationQualityReport(Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;)V

    .line 80
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 61
    .end local v0    # "tripEndUtc":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized trackTripRecalculated()V
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    if-nez v0, :cond_0

    .line 46
    sget-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "report is null, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :goto_0
    monitor-exit p0

    return-void

    .line 49
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    iget v1, v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->nRecalculations:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->nRecalculations:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized trackTripStarted(JJJ)V
    .locals 11
    .param p1, "durationWithTraffic"    # J
    .param p3, "duration"    # J
    .param p5, "distance"    # J

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "trip started, init new navigation quality report"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    .line 38
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripDurationIntervalInit:J

    .line 40
    sget-object v0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tripStartUtc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->tripStartUtc:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 41
    new-instance v1, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    iget-boolean v8, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->isRoutingWithTraffic:Z

    const/4 v9, 0x0

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;-><init>(JJJZLcom/navdy/hud/app/analytics/NavigationQualityTracker$1;)V

    iput-object v1, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->report:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    monitor-exit p0

    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
