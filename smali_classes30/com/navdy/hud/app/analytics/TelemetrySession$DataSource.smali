.class public interface abstract Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;
.super Ljava/lang/Object;
.source "TelemetrySession.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/TelemetrySession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DataSource"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\u000bH&J\u0008\u0010\r\u001a\u00020\u0003H&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;",
        "",
        "absoluteCurrentTime",
        "",
        "currentSpeed",
        "Lcom/navdy/hud/app/analytics/RawSpeed;",
        "interestingEventDetected",
        "",
        "event",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "isHighAccuracySpeedAvailable",
        "",
        "isVerboseLoggingNeeded",
        "totalDistanceTravelledWithMeters",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# virtual methods
.method public abstract absoluteCurrentTime()J
.end method

.method public abstract currentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V
    .param p1    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract isHighAccuracySpeedAvailable()Z
.end method

.method public abstract isVerboseLoggingNeeded()Z
.end method

.method public abstract totalDistanceTravelledWithMeters()J
.end method
