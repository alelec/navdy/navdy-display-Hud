.class public final Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;
.super Ljava/lang/Object;
.source "TelemetryDataManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/TelemetryDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00120\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u001c2\u0006\u0010\u001d\u001a\u00020\u0012R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;",
        "",
        "()V",
        "ANALYTICS_REPORTING_INTERVAL",
        "",
        "getANALYTICS_REPORTING_INTERVAL",
        "()J",
        "DRIVE_SCORE_PUBLISH_INTERVAL",
        "getDRIVE_SCORE_PUBLISH_INTERVAL",
        "LOG_TELEMETRY_DATA",
        "",
        "getLOG_TELEMETRY_DATA",
        "()Z",
        "MAX_ACCEL",
        "",
        "getMAX_ACCEL",
        "()F",
        "PREFERENCE_TROUBLE_CODES",
        "",
        "getPREFERENCE_TROUBLE_CODES",
        "()Ljava/lang/String;",
        "sLogger",
        "Lcom/navdy/service/library/log/Logger;",
        "getSLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "serializeTroubleCodes",
        "Lkotlin/Pair;",
        "troubleCodes",
        "",
        "savedTroubleCodesStringFromPreviousSession",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getANALYTICS_REPORTING_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J
    .locals 2
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getANALYTICS_REPORTING_INTERVAL()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J
    .locals 2
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getDRIVE_SCORE_PUBLISH_INTERVAL()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic access$getLOG_TELEMETRY_DATA$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Z
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getLOG_TELEMETRY_DATA()Z

    move-result v0

    return v0
.end method

.method public static final synthetic access$getPREFERENCE_TROUBLE_CODES$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getPREFERENCE_TROUBLE_CODES()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getANALYTICS_REPORTING_INTERVAL()J
    .locals 2

    .prologue
    .line 78
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->ANALYTICS_REPORTING_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getANALYTICS_REPORTING_INTERVAL$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getDRIVE_SCORE_PUBLISH_INTERVAL()J
    .locals 2

    .prologue
    .line 79
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->DRIVE_SCORE_PUBLISH_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getLOG_TELEMETRY_DATA()Z
    .locals 1

    .prologue
    .line 81
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->LOG_TELEMETRY_DATA:Z
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getLOG_TELEMETRY_DATA$cp()Z

    move-result v0

    return v0
.end method

.method private final getMAX_ACCEL()F
    .locals 1

    .prologue
    .line 80
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->MAX_ACCEL:F
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getMAX_ACCEL$cp()F

    move-result v0

    return v0
.end method

.method private final getPREFERENCE_TROUBLE_CODES()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->PREFERENCE_TROUBLE_CODES:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getPREFERENCE_TROUBLE_CODES$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getSLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 76
    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getSLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final serializeTroubleCodes(Ljava/util/List;Ljava/lang/String;)Lkotlin/Pair;
    .locals 12
    .param p1, "troubleCodes"    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "savedTroubleCodesStringFromPreviousSession"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "troubleCodes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "savedTroubleCodesStringFromPreviousSession"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .local v6, "hashSet":Ljava/util/HashSet;
    move-object v0, p2

    .line 84
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p2

    .line 85
    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, " "

    aput-object v3, v1, v2

    const/4 v4, 0x6

    const/4 v5, 0x0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 86
    .local v10, "troubleCodesSaved":Ljava/util/List;
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 87
    .local v8, "troubleCode":Ljava/lang/String;
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 90
    .end local v8    # "troubleCode":Ljava/lang/String;
    .end local v10    # "troubleCodesSaved":Ljava/util/List;
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .local v11, "updatedTroubleCodesString":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v9, "troubleCodesDiffString":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 93
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setSessionTroubleCodeCount(I)V

    .line 94
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 95
    .local v7, "newTroubleCode":Ljava/lang/String;
    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 101
    .end local v7    # "newTroubleCode":Ljava/lang/String;
    :cond_2
    new-instance v1, Lkotlin/Pair;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
