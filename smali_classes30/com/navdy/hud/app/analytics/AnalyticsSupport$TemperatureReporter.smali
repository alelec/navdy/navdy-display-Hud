.class public Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;
.super Ljava/lang/Thread;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TemperatureReporter"
.end annotation


# static fields
.field private static final SOCKET_NAME:Ljava/lang/String; = "tempstatus"


# instance fields
.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1049
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 34

    .prologue
    .line 1061
    :try_start_0
    new-instance v25, Landroid/net/LocalSocket;

    invoke-direct/range {v25 .. v25}, Landroid/net/LocalSocket;-><init>()V

    .line 1062
    .local v25, "socket":Landroid/net/LocalSocket;
    new-instance v4, Landroid/net/LocalSocketAddress;

    const-string v28, "tempstatus"

    sget-object v29, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-direct {v4, v0, v1}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    .line 1064
    .local v4, "addr":Landroid/net/LocalSocketAddress;
    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 1065
    invoke-virtual/range {v25 .. v25}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v26

    .line 1066
    .local v26, "socketInputStream":Ljava/io/InputStream;
    new-instance v23, Ljava/io/BufferedReader;

    new-instance v28, Ljava/io/InputStreamReader;

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071
    .local v23, "reader":Ljava/io/BufferedReader;
    const/16 v24, 0x0

    .line 1073
    .local v24, "retries":I
    :try_start_1
    new-instance v15, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;

    const/16 v28, 0x48

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_INLET_HIGH_TIME_MS:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2300()J

    move-result-wide v30

    move/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-direct {v15, v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;-><init>(IJ)V

    .line 1074
    .local v15, "inletThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    new-instance v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;

    const/16 v28, 0x41

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_DMD_HIGH_TIME_MS:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2400()J

    move-result-wide v30

    move/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-direct {v9, v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;-><init>(IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1077
    .local v9, "dmdThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v22

    .line 1078
    .local v22, "origData":Ljava/lang/String;
    if-eqz v22, :cond_0

    .line 1081
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    .line 1082
    move-object/from16 v7, v22

    .line 1083
    .local v7, "data":Ljava/lang/String;
    const/16 v28, 0xd

    move/from16 v0, v28

    new-array v13, v0, [Ljava/lang/String;

    .line 1084
    .local v13, "fields":[Ljava/lang/String;
    const/16 v18, 0x0

    .local v18, "nFields":I
    move/from16 v19, v18

    .line 1085
    .end local v18    # "nFields":I
    .local v19, "nFields":I
    :goto_1
    array-length v0, v13

    move/from16 v28, v0

    add-int/lit8 v28, v28, -0x1

    move/from16 v0, v19

    move/from16 v1, v28

    if-ge v0, v1, :cond_1

    const-string v28, " "

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 1086
    const-string v28, " +"

    const/16 v29, 0x2

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v27

    .line 1087
    .local v27, "split":[Ljava/lang/String;
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "nFields":I
    .restart local v18    # "nFields":I
    const/16 v28, 0x0

    aget-object v28, v27, v28

    aput-object v28, v13, v19

    .line 1088
    const/16 v28, 0x1

    aget-object v7, v27, v28
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move/from16 v19, v18

    .line 1089
    .end local v18    # "nFields":I
    .restart local v19    # "nFields":I
    goto :goto_1

    .line 1067
    .end local v4    # "addr":Landroid/net/LocalSocketAddress;
    .end local v7    # "data":Ljava/lang/String;
    .end local v9    # "dmdThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .end local v13    # "fields":[Ljava/lang/String;
    .end local v15    # "inletThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .end local v19    # "nFields":I
    .end local v22    # "origData":Ljava/lang/String;
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v24    # "retries":I
    .end local v25    # "socket":Landroid/net/LocalSocket;
    .end local v26    # "socketInputStream":Ljava/io/InputStream;
    .end local v27    # "split":[Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 1068
    .local v12, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    const-string v29, "error connecting to temperature reporting socket"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1152
    :goto_2
    return-void

    .line 1090
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v4    # "addr":Landroid/net/LocalSocketAddress;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v9    # "dmdThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .restart local v13    # "fields":[Ljava/lang/String;
    .restart local v15    # "inletThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .restart local v19    # "nFields":I
    .restart local v22    # "origData":Ljava/lang/String;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    .restart local v24    # "retries":I
    .restart local v25    # "socket":Landroid/net/LocalSocket;
    .restart local v26    # "socketInputStream":Ljava/io/InputStream;
    :cond_1
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "nFields":I
    .restart local v18    # "nFields":I
    :try_start_3
    aput-object v7, v13, v19

    .line 1091
    array-length v0, v13

    move/from16 v28, v0

    move/from16 v0, v18

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    .line 1092
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    sget-object v29, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v30, "missing fields in temperature reporting data, found = %d, expected = %d data: %s"

    const/16 v31, 0x3

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    array-length v0, v13

    move/from16 v33, v0

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x2

    aput-object v22, v31, v32

    invoke-static/range {v29 .. v31}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1148
    .end local v7    # "data":Ljava/lang/String;
    .end local v13    # "fields":[Ljava/lang/String;
    .end local v18    # "nFields":I
    .end local v22    # "origData":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 1149
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    const-string v29, "error reading from temperature reporting socket"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1150
    add-int/lit8 v24, v24, 0x1

    const/16 v28, 0x3

    move/from16 v0, v24

    move/from16 v1, v28

    if-le v0, v1, :cond_0

    .line 1151
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    const-string v29, "too many errors reading temperature reporting socket, giving up"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1157
    invoke-static/range {v26 .. v26}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1158
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_2

    .line 1094
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v13    # "fields":[Ljava/lang/String;
    .restart local v18    # "nFields":I
    .restart local v22    # "origData":Ljava/lang/String;
    :cond_2
    add-int/lit8 v28, v18, -0x1

    :try_start_5
    aget-object v28, v13, v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 1095
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "too many fields in temperature reporting data: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1096
    add-int/lit8 v28, v18, -0x1

    add-int/lit8 v29, v18, -0x1

    aget-object v29, v13, v29

    const-string v30, " "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    aget-object v29, v29, v30

    aput-object v29, v13, v28

    .line 1101
    :cond_3
    const/16 v28, 0x5

    aget-object v28, v13, v28

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->parseTemp(Ljava/lang/String;)I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2500(Ljava/lang/String;)I

    move-result v5

    .line 1102
    .local v5, "avgCpuTemp":I
    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->currentCpuAvgTemperature:I
    invoke-static {v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2602(I)I

    .line 1103
    const/16 v28, 0x2

    aget-object v28, v13, v28

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->parseTemp(Ljava/lang/String;)I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2500(Ljava/lang/String;)I

    move-result v14

    .line 1104
    .local v14, "inletTemp":I
    const/16 v28, 0x3

    aget-object v28, v13, v28

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->parseTemp(Ljava/lang/String;)I
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2500(Ljava/lang/String;)I

    move-result v8

    .line 1105
    .local v8, "dmdTemp":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 1106
    .local v20, "now":J
    invoke-virtual {v9, v8}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->update(I)V

    .line 1107
    invoke-virtual {v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->isHit()Z

    move-result v10

    .line 1108
    .local v10, "doShutdown":Z
    const/4 v11, 0x0

    .line 1110
    .local v11, "doWarning":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v28

    if-nez v28, :cond_7

    .line 1111
    invoke-virtual {v15, v14}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->update(I)V

    .line 1112
    const/16 v28, 0x5d

    move/from16 v0, v28

    if-ge v5, v0, :cond_4

    invoke-virtual {v15}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;->isHit()Z

    move-result v28

    if-eqz v28, :cond_a

    :cond_4
    const/4 v11, 0x1

    .line 1114
    :goto_3
    invoke-static {}, Lcom/navdy/hud/app/view/TemperatureWarningView;->getLastDismissedTime()J

    move-result-wide v16

    .line 1116
    .local v16, "lastWarningTime":J
    const-wide/16 v28, 0x0

    cmp-long v28, v16, v28

    if-eqz v28, :cond_5

    const-wide/16 v28, -0x1

    cmp-long v28, v16, v28

    if-eqz v28, :cond_6

    sub-long v28, v20, v16

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_INTERVAL_MS:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2700()J

    move-result-wide v30

    cmp-long v28, v28, v30

    if-gtz v28, :cond_6

    .line 1117
    :cond_5
    const/4 v11, 0x0

    .line 1120
    :cond_6
    if-eqz v11, :cond_7

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2800()J

    move-result-wide v28

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_BOOT_DELAY_S:J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2900()J

    move-result-wide v30

    cmp-long v28, v28, v30

    if-gez v28, :cond_7

    .line 1121
    const/4 v11, 0x0

    .line 1124
    .end local v16    # "lastWarningTime":J
    :cond_7
    if-eqz v11, :cond_8

    .line 1125
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v6

    .line 1126
    .local v6, "bus":Lcom/squareup/otto/Bus;
    new-instance v28, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct/range {v28 .. v28}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v29, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1128
    .end local v6    # "bus":Lcom/squareup/otto/Bus;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v28

    if-eqz v28, :cond_9

    if-eqz v10, :cond_9

    .line 1129
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    const-string v29, "Shutting down due to high DMD temperature"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1130
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v6

    .line 1131
    .restart local v6    # "bus":Lcom/squareup/otto/Bus;
    new-instance v28, Lcom/navdy/hud/app/event/Shutdown;

    sget-object v29, Lcom/navdy/hud/app/event/Shutdown$Reason;->HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct/range {v28 .. v29}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1133
    .end local v6    # "bus":Lcom/squareup/otto/Bus;
    :cond_9
    const-string v29, "Temperature_Change"

    const/16 v28, 0x1c

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v28, 0x0

    const-string v31, "Fan_Speed"

    aput-object v31, v30, v28

    const/16 v28, 0x1

    const/16 v31, 0x0

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x2

    const-string v31, "CPU_Temperature"

    aput-object v31, v30, v28

    const/16 v28, 0x3

    const/16 v31, 0x1

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x4

    const-string v31, "Inlet_Temperature"

    aput-object v31, v30, v28

    const/16 v28, 0x5

    const/16 v31, 0x2

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x6

    const-string v31, "DMD_Temperature"

    aput-object v31, v30, v28

    const/16 v28, 0x7

    const/16 v31, 0x3

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x8

    const-string v31, "LED_Temperature"

    aput-object v31, v30, v28

    const/16 v28, 0x9

    const/16 v31, 0x4

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0xa

    const-string v31, "CPU_Average"

    aput-object v31, v30, v28

    const/16 v28, 0xb

    const/16 v31, 0x5

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0xc

    const-string v31, "Inlet_Average"

    aput-object v31, v30, v28

    const/16 v28, 0xd

    const/16 v31, 0x6

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0xe

    const-string v31, "DMD_Average"

    aput-object v31, v30, v28

    const/16 v28, 0xf

    const/16 v31, 0x7

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x10

    const-string v31, "LED_Average"

    aput-object v31, v30, v28

    const/16 v28, 0x11

    const/16 v31, 0x8

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x12

    const-string v31, "CPU_Max"

    aput-object v31, v30, v28

    const/16 v28, 0x13

    const/16 v31, 0x9

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x14

    const-string v31, "Inlet_Max"

    aput-object v31, v30, v28

    const/16 v28, 0x15

    const/16 v31, 0xa

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x16

    const-string v31, "DMD_Max"

    aput-object v31, v30, v28

    const/16 v28, 0x17

    const/16 v31, 0xb

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x18

    const-string v31, "LED_Max"

    aput-object v31, v30, v28

    const/16 v28, 0x19

    const/16 v31, 0xc

    aget-object v31, v13, v31

    aput-object v31, v30, v28

    const/16 v28, 0x1a

    const-string v31, "Warning"

    aput-object v31, v30, v28

    const/16 v31, 0x1b

    if-eqz v11, :cond_b

    const-string v28, "true"

    :goto_4
    aput-object v28, v30, v31

    invoke-static/range {v29 .. v30}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1157
    .end local v5    # "avgCpuTemp":I
    .end local v7    # "data":Ljava/lang/String;
    .end local v8    # "dmdTemp":I
    .end local v9    # "dmdThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .end local v10    # "doShutdown":Z
    .end local v11    # "doWarning":Z
    .end local v13    # "fields":[Ljava/lang/String;
    .end local v14    # "inletTemp":I
    .end local v15    # "inletThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .end local v18    # "nFields":I
    .end local v20    # "now":J
    .end local v22    # "origData":Ljava/lang/String;
    :catchall_0
    move-exception v28

    invoke-static/range {v26 .. v26}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1158
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v28

    .line 1112
    .restart local v5    # "avgCpuTemp":I
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v8    # "dmdTemp":I
    .restart local v9    # "dmdThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .restart local v10    # "doShutdown":Z
    .restart local v11    # "doWarning":Z
    .restart local v13    # "fields":[Ljava/lang/String;
    .restart local v14    # "inletTemp":I
    .restart local v15    # "inletThreshold":Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
    .restart local v18    # "nFields":I
    .restart local v20    # "now":J
    .restart local v22    # "origData":Ljava/lang/String;
    :cond_a
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 1133
    :cond_b
    :try_start_6
    const-string v28, "false"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4
.end method
