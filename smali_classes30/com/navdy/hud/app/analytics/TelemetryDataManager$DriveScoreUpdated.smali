.class public final Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
.super Ljava/lang/Object;
.source "TelemetryDataManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/TelemetryDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DriveScoreUpdated"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0018\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J;\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00052\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\tH\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0013\"\u0004\u0008\u0016\u0010\u0015R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0004\u0010\u0013\"\u0004\u0008\u0017\u0010\u0015\u00a8\u0006#"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "",
        "interestingEvent",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "isSpeeding",
        "",
        "isExcessiveSpeeding",
        "isDoingHighGManeuver",
        "driveScore",
        "",
        "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V",
        "getDriveScore",
        "()I",
        "setDriveScore",
        "(I)V",
        "getInterestingEvent",
        "()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "setInterestingEvent",
        "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V",
        "()Z",
        "setDoingHighGManeuver",
        "(Z)V",
        "setExcessiveSpeeding",
        "setSpeeding",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private driveScore:I

.field private interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private isDoingHighGManeuver:Z

.field private isExcessiveSpeeding:Z

.field private isSpeeding:Z


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V
    .locals 1
    .param p1, "interestingEvent"    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "isSpeeding"    # Z
    .param p3, "isExcessiveSpeeding"    # Z
    .param p4, "isDoingHighGManeuver"    # Z
    .param p5, "driveScore"    # I

    .prologue
    const-string v0, "interestingEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    iput-boolean p2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    iput-boolean p3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    iput-boolean p4, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    iput p5, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    return-void
.end method

.method public static bridge synthetic copy$default(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZIILjava/lang/Object;)Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 v0, p6, 0x1

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    :goto_0
    and-int/lit8 v0, p6, 0x2

    if-eqz v0, :cond_3

    iget-boolean v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    :goto_1
    and-int/lit8 v0, p6, 0x4

    if-eqz v0, :cond_2

    iget-boolean v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    :goto_2
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_1

    iget-boolean v4, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    :goto_3
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    iget v5, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    :goto_4
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->copy(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    move-result-object v0

    return-object v0

    :cond_0
    move v5, p5

    goto :goto_4

    :cond_1
    move v4, p4

    goto :goto_3

    :cond_2
    move v3, p3

    goto :goto_2

    :cond_3
    move v2, p2

    goto :goto_1

    :cond_4
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public final component1()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    .prologue
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    .prologue
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    .prologue
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    return v0
.end method

.method public final component5()I
    .locals 1

    .prologue
    iget v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    return v0
.end method

.method public final copy(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .locals 6
    .param p1, "interestingEvent"    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "isSpeeding"    # Z
    .param p3, "isExcessiveSpeeding"    # Z
    .param p4, "isDoingHighGManeuver"    # Z
    .param p5, "driveScore"    # I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const-string v0, "interestingEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;-><init>(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    iget-object v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    iget-object v3, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    iget-boolean v3, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    iget-boolean v3, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    iget-boolean v3, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    iget v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    iget v3, p1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_3
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method

.method public final getDriveScore()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    return v0
.end method

.method public final getInterestingEvent()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    if-eqz v0, :cond_0

    move v0, v1

    :cond_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    if-eqz v0, :cond_1

    move v0, v1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    if-eqz v0, :cond_3

    :goto_1
    add-int v0, v2, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    add-int/2addr v0, v1

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public final isDoingHighGManeuver()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    return v0
.end method

.method public final isExcessiveSpeeding()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    return v0
.end method

.method public final isSpeeding()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    return v0
.end method

.method public final setDoingHighGManeuver(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    return-void
.end method

.method public final setDriveScore(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    return-void
.end method

.method public final setExcessiveSpeeding(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    return-void
.end method

.method public final setInterestingEvent(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-void
.end method

.method public final setSpeeding(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DriveScoreUpdated(interestingEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->interestingEvent:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSpeeding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isExcessiveSpeeding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isDoingHighGManeuver="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", driveScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->driveScore:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
