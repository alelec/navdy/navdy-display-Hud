.class public Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$AnalyticsIntentsReceiver;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;
    }
.end annotation


# static fields
.field private static final ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE:Ljava/lang/String; = "Adaptive_Brightness_Adjustment_Change"

.field private static final ANALYTICS_EVENT_ATTEMPT_TO_CONNECT:Ljava/lang/String; = "Attempt_To_Connect"

.field private static final ANALYTICS_EVENT_BAD_ROUTE_POSITION:Ljava/lang/String; = "Route_Bad_Pos"

.field private static final ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE:Ljava/lang/String; = "Brightness_Adjustment_Change"

.field private static final ANALYTICS_EVENT_BRIGHTNESS_CHANGE:Ljava/lang/String; = "Brightness_Change"

.field private static final ANALYTICS_EVENT_CPU_OVERHEAT:Ljava/lang/String; = "Cpu_Overheat"

.field private static final ANALYTICS_EVENT_DASH_GAUGE_SETTING:Ljava/lang/String; = "Dash_Gauge_Setting"

.field private static final ANALYTICS_EVENT_DESTINATION_SUGGESTION:Ljava/lang/String; = "Destination_Suggestion_Show"

.field private static final ANALYTICS_EVENT_DIAL_UPDATE_PROMPT:Ljava/lang/String; = "Dial_Update_Prompt"

.field private static final ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT:Ljava/lang/String; = "Display_Update_Prompt"

.field private static final ANALYTICS_EVENT_DISTANCE_TRAVELLED:Ljava/lang/String; = "Distance_Travelled"

.field private static final ANALYTICS_EVENT_DRIVER_PREFERENCES:Ljava/lang/String; = "Driver_Preferences"

.field private static final ANALYTICS_EVENT_FUEL_LEVEL:Ljava/lang/String; = "Fuel_Level"

.field public static final ANALYTICS_EVENT_GLANCE_ADVANCE:Ljava/lang/String; = "Glance_Advance"

.field public static final ANALYTICS_EVENT_GLANCE_DELETE_ALL:Ljava/lang/String; = "Glance_Delete_All"

.field public static final ANALYTICS_EVENT_GLANCE_DISMISS:Ljava/lang/String; = "Glance_Dismiss"

.field public static final ANALYTICS_EVENT_GLANCE_OPEN_FULL:Ljava/lang/String; = "Glance_Open_Full"

.field public static final ANALYTICS_EVENT_GLANCE_OPEN_MINI:Ljava/lang/String; = "Glance_Open_Mini"

.field private static final ANALYTICS_EVENT_GLYMPSE_SENT:Ljava/lang/String; = "Glympse_Sent"

.field private static final ANALYTICS_EVENT_GLYMPSE_VIEWED:Ljava/lang/String; = "Glympse_Viewed"

.field public static final ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS:Ljava/lang/String; = "GPS_Accuracy"

.field public static final ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION:Ljava/lang/String; = "GPS_Acquired_Location"

.field public static final ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION:Ljava/lang/String; = "GPS_Attempt_Acquire_Location"

.field public static final ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE:Ljava/lang/String; = "GPS_Calibration_IMU_Done"

.field public static final ANALYTICS_EVENT_GPS_CALIBRATION_LOST:Ljava/lang/String; = "GPS_Calibration_Lost"

.field public static final ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE:Ljava/lang/String; = "GPS_Calibration_Sensor_Done"

.field public static final ANALYTICS_EVENT_GPS_CALIBRATION_START:Ljava/lang/String; = "GPS_Calibration_Start"

.field public static final ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH:Ljava/lang/String; = "GPS_Calibration_VinSwitch"

.field public static final ANALYTICS_EVENT_GPS_LOST_LOCATION:Ljava/lang/String; = "GPS_Lost_Signal"

.field public static final ANALYTICS_EVENT_IAP_FAILURE:Ljava/lang/String; = "Mobile_iAP_Failure"

.field public static final ANALYTICS_EVENT_IAP_RETRY_SUCCESS:Ljava/lang/String; = "Mobile_iAP_Retry_Success"

.field private static final ANALYTICS_EVENT_KEY_FAILED:Ljava/lang/String; = "Key_Failed"

.field private static final ANALYTICS_EVENT_MENU_SELECTION:Ljava/lang/String; = "Menu_Selection"

.field private static final ANALYTICS_EVENT_MILEAGE:Ljava/lang/String; = "Navdy_Mileage"

.field private static final ANALYTICS_EVENT_MOBILE_APP_CONNECT:Ljava/lang/String; = "Mobile_App_Connect"

.field private static final ANALYTICS_EVENT_MOBILE_APP_DISCONNECT:Ljava/lang/String; = "Mobile_App_Disconnect"

.field private static final ANALYTICS_EVENT_MUSIC_ACTION:Ljava/lang/String; = "Music_Action"

.field private static final ANALYTICS_EVENT_NAVIGATION_ARRIVED:Ljava/lang/String; = "Navigation_Arrived"

.field private static final ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET:Ljava/lang/String; = "Navigation_Destination_Set"

.field private static final ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED:Ljava/lang/String; = "Navigation_Maneuver_Missed"

.field private static final ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED:Ljava/lang/String; = "Navigation_Maneuver_Required"

.field private static final ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING:Ljava/lang/String; = "Navigation_Maneuver_Warning"

.field private static final ANALYTICS_EVENT_NAVIGATION_PREFERENCES:Ljava/lang/String; = "Navigation_Preferences"

.field private static final ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE:Ljava/lang/String; = "Map_Region_Change"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED:Ljava/lang/String; = "Nearby_Search_Arrived"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED:Ljava/lang/String; = "Nearby_Search_Closed"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS:Ljava/lang/String; = "Nearby_Search_Dismiss"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_ENDED:Ljava/lang/String; = "Nearby_Search_Ended"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS:Ljava/lang/String; = "Nearby_Search_No_Results"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE:Ljava/lang/String; = "Nearby_Search_Results_Close"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS:Ljava/lang/String; = "Nearby_Search_Results_Dismiss"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW:Ljava/lang/String; = "Nearby_Search_Results_View"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM:Ljava/lang/String; = "Nearby_Search_Return_Main_Menu"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION:Ljava/lang/String; = "Nearby_Search_Selection"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED:Ljava/lang/String; = "Nearby_Search_Trip_Cancelled"

.field private static final ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED:Ljava/lang/String; = "Nearby_Search_Trip_Initiated"

.field public static final ANALYTICS_EVENT_NEW_DEVICE:Ljava/lang/String; = "Analytics_New_Device"

.field private static final ANALYTICS_EVENT_OBD_CONNECTED:Ljava/lang/String; = "OBD_Connected"

.field private static final ANALYTICS_EVENT_OBD_LISTENING_POSTED:Ljava/lang/String; = "OBD_Listening_Posted"

.field private static final ANALYTICS_EVENT_OBD_LISTENING_STATUS:Ljava/lang/String; = "OBD_Listening_Status"

.field private static final ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED:Ljava/lang/String; = "Offline_Maps_Updated"

.field private static final ANALYTICS_EVENT_OPTION_SELECTED:Ljava/lang/String; = "Option_Selected"

.field private static final ANALYTICS_EVENT_OTADOWNLOAD:Ljava/lang/String; = "OTA_Download"

.field private static final ANALYTICS_EVENT_OTAINSTALL:Ljava/lang/String; = "OTA_Install"

.field private static final ANALYTICS_EVENT_OTA_INSTALL_RESULT:Ljava/lang/String; = "OTA_Install_Result"

.field private static final ANALYTICS_EVENT_PHONE_DISCONNECTED:Ljava/lang/String; = "Phone_Disconnected"

.field private static final ANALYTICS_EVENT_ROUTE_COMPLETED:Ljava/lang/String; = "Route_Completed"

.field private static final ANALYTICS_EVENT_SHUTDOWN:Ljava/lang/String; = "Shutdown"

.field private static final ANALYTICS_EVENT_SMS_SENT:Ljava/lang/String; = "sms_sent"

.field private static final ANALYTICS_EVENT_SWIPED_PREFIX:Ljava/lang/String; = "Swiped_"

.field private static final ANALYTICS_EVENT_TEMPERATURE_CHANGE:Ljava/lang/String; = "Temperature_Change"

.field private static final ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA:Ljava/lang/String; = "Vehicle_Telemetry_Data"

.field private static final ANALYTICS_EVENT_VOICE_SEARCH_ANDROID:Ljava/lang/String; = "Voice_Search_Android"

.field private static final ANALYTICS_EVENT_VOICE_SEARCH_IOS:Ljava/lang/String; = "Voice_Search_iOS"

.field private static final ANALYTICS_EVENT_WAKEUP:Ljava/lang/String; = "Wakeup"

.field private static final ANALYTICS_EVENT_ZERO_FUEL_DATA:Ljava/lang/String; = "OBD_Zero_Fuel_Level"

.field public static final ANALYTICS_INTENT:Ljava/lang/String; = "com.navdy.hud.app.analytics.AnalyticsEvent"

.field public static final ANALYTICS_INTENT_EXTRA_ARGUMENTS:Ljava/lang/String; = "arguments"

.field public static final ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO:Ljava/lang/String; = "include_device_info"

.field public static final ANALYTICS_INTENT_EXTRA_TAG_NAME:Ljava/lang/String; = "tag"

.field private static final ATTR_BRIGHTNESS_ADJ_NEW:Ljava/lang/String; = "New"

.field private static final ATTR_BRIGHTNESS_ADJ_PREVIOUS:Ljava/lang/String; = "Previous"

.field private static final ATTR_BRIGHTNESS_BRIGHTNESS:Ljava/lang/String; = "Brightness"

.field private static final ATTR_BRIGHTNESS_LIGHT_SENSOR:Ljava/lang/String; = "Light_Sensor"

.field private static final ATTR_BRIGHTNESS_NEW:Ljava/lang/String; = "New"

.field private static final ATTR_BRIGHTNESS_PREVIOUS:Ljava/lang/String; = "Previous"

.field private static final ATTR_BRIGHTNESS_RAW1_SENSOR:Ljava/lang/String; = "Raw1_Sensor"

.field private static final ATTR_BRIGHTNESS_RAW_SENSOR:Ljava/lang/String; = "Raw_Sensor"

.field private static final ATTR_CONN_ATTEMPT_NEW_DEVICE:Ljava/lang/String; = "Was_New_Device"

.field private static final ATTR_CONN_ATTEMPT_TIME:Ljava/lang/String; = "Time_To_Connect"

.field private static final ATTR_CONN_CAR_MAKE:Ljava/lang/String; = "Car_Make"

.field private static final ATTR_CONN_CAR_MODEL:Ljava/lang/String; = "Car_Model"

.field private static final ATTR_CONN_CAR_YEAR:Ljava/lang/String; = "Car_Year"

.field private static final ATTR_CONN_DEVICE_ID:Ljava/lang/String; = "Device_Id"

.field private static final ATTR_CONN_REMOTE_CLIENT_VERSION:Ljava/lang/String; = "Remote_Client_Version"

.field private static final ATTR_CONN_REMOTE_DEVICE_ID:Ljava/lang/String; = "Remote_Device_Id"

.field private static final ATTR_CONN_REMOTE_DEVICE_NAME:Ljava/lang/String; = "Remote_Device_Name"

.field private static final ATTR_CONN_REMOTE_PLATFORM:Ljava/lang/String; = "Remote_Platform"

.field private static final ATTR_CONN_VIN:Ljava/lang/String; = "VIN"

.field private static final ATTR_CPU_OVERHEAT_STATE:Ljava/lang/String; = "State"

.field private static final ATTR_CPU_OVERHEAT_UPTIME:Ljava/lang/String; = "Uptime"

.field private static final ATTR_DESTINATION_SUGGESTION_ACCEPTED:Ljava/lang/String; = "Accepted"

.field private static final ATTR_DESTINATION_SUGGESTION_TYPE:Ljava/lang/String; = "Type"

.field private static final ATTR_DISC_CAR_MAKE:Ljava/lang/String; = "Car_Make"

.field private static final ATTR_DISC_CAR_MODEL:Ljava/lang/String; = "Car_Model"

.field private static final ATTR_DISC_CAR_YEAR:Ljava/lang/String; = "Car_Year"

.field private static final ATTR_DISC_ELAPSED_TIME:Ljava/lang/String; = "Elapsed_Time"

.field private static final ATTR_DISC_FORCE_RECONNECT:Ljava/lang/String; = "Force_Reconnect"

.field private static final ATTR_DISC_FORCE_RECONNECT_REASON:Ljava/lang/String; = "Force_Reconnect_Reason"

.field private static final ATTR_DISC_MOVING:Ljava/lang/String; = "While_Moving"

.field private static final ATTR_DISC_PHONE_MAKE:Ljava/lang/String; = "Phone_Make"

.field private static final ATTR_DISC_PHONE_MODEL:Ljava/lang/String; = "Phone_Model"

.field private static final ATTR_DISC_PHONE_OS:Ljava/lang/String; = "Phone_OS"

.field private static final ATTR_DISC_PHONE_TYPE:Ljava/lang/String; = "Phone_Type"

.field private static final ATTR_DISC_REASON:Ljava/lang/String; = "Reason"

.field private static final ATTR_DISC_STATUS:Ljava/lang/String; = "Status"

.field private static final ATTR_DISC_VIN:Ljava/lang/String; = "VIN"

.field private static final ATTR_DISTANCE:Ljava/lang/String; = "Distance"

.field private static final ATTR_DRIVER_PREFERENCES_AUTO_ON:Ljava/lang/String; = "Auto_On"

.field private static final ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT:Ljava/lang/String; = "Display_Format"

.field private static final ATTR_DRIVER_PREFERENCES_FEATURE_MODE:Ljava/lang/String; = "Feature_Mode"

.field private static final ATTR_DRIVER_PREFERENCES_GESTURES:Ljava/lang/String; = "Gestures"

.field private static final ATTR_DRIVER_PREFERENCES_GLANCES:Ljava/lang/String; = "Glances"

.field private static final ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH:Ljava/lang/String; = "Limit_Bandwidth"

.field private static final ATTR_DRIVER_PREFERENCES_LOCALE:Ljava/lang/String; = "Locale"

.field private static final ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM:Ljava/lang/String; = "Manual_Zoom"

.field private static final ATTR_DRIVER_PREFERENCES_OBD:Ljava/lang/String; = "OBD"

.field private static final ATTR_DRIVER_PREFERENCES_UNITS:Ljava/lang/String; = "Units"

.field private static final ATTR_DURATION:Ljava/lang/String; = "Duration"

.field private static final ATTR_EVENT_AVERAGE_MPG:Ljava/lang/String; = "Event_Average_Mpg"

.field private static final ATTR_EVENT_AVERAGE_RPM:Ljava/lang/String; = "Event_Average_Rpm"

.field private static final ATTR_EVENT_ENGINE_TEMPERATURE:Ljava/lang/String; = "Event_Engine_Temperature"

.field private static final ATTR_EVENT_FUEL_LEVEL:Ljava/lang/String; = "Event_Fuel_Level"

.field private static final ATTR_EVENT_MAX_G:Ljava/lang/String; = "Event_Max_G"

.field private static final ATTR_EVENT_MAX_G_ANGLE:Ljava/lang/String; = "Event_Max_G_Angle"

.field private static final ATTR_FUEL_CONSUMPTION:Ljava/lang/String; = "Fuel_Consumption"

.field private static final ATTR_FUEL_DISTANCE:Ljava/lang/String; = "Distance"

.field private static final ATTR_FUEL_LEVEL:Ljava/lang/String; = "Fuel_Level"

.field private static final ATTR_FUEL_VIN:Ljava/lang/String; = "VIN"

.field private static final ATTR_GAUGE_ID:Ljava/lang/String; = "Gauge_name"

.field private static final ATTR_GAUGE_SIDE:Ljava/lang/String; = "Side"

.field private static final ATTR_GAUGE_SIDE_LEFT:Ljava/lang/String; = "Left"

.field private static final ATTR_GAUGE_SIDE_RIGHT:Ljava/lang/String; = "Right"

.field private static final ATTR_GLANCE_NAME:Ljava/lang/String; = "Glance_Name"

.field private static final ATTR_GLANCE_NAV_METHOD:Ljava/lang/String; = "Nav_Method"

.field private static final ATTR_GLYMPSE_ID:Ljava/lang/String; = "Id"

.field private static final ATTR_GLYMPSE_MESSAGE:Ljava/lang/String; = "Message"

.field private static final ATTR_GLYMPSE_SHARE:Ljava/lang/String; = "Share"

.field private static final ATTR_GLYMPSE_SUCCESS:Ljava/lang/String; = "Success"

.field private static final ATTR_KEY_FAIL_ERROR:Ljava/lang/String; = "Error"

.field private static final ATTR_KEY_FAIL_KEY_TYPE:Ljava/lang/String; = "Key_Type"

.field private static final ATTR_MENU_NAME:Ljava/lang/String; = "Name"

.field private static final ATTR_MUSIC_ACTION_TYPE:Ljava/lang/String; = "Type"

.field private static final ATTR_NAVDY_KILOMETERS:Ljava/lang/String; = "Navdy_Kilometers"

.field private static final ATTR_NAVDY_USAGE_RATE:Ljava/lang/String; = "Navdy_Usage_Rate"

.field private static final ATTR_NAVIGATING:Ljava/lang/String; = "Navigating"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS:Ljava/lang/String; = "Allow_Auto_Trains"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES:Ljava/lang/String; = "Allow_Ferries"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS:Ljava/lang/String; = "Allow_Highways"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES:Ljava/lang/String; = "Allow_HOV_Lanes"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS:Ljava/lang/String; = "Allow_Toll_Roads"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS:Ljava/lang/String; = "Allow_Tunnels"

.field private static final ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS:Ljava/lang/String; = "Allow_Unpaved_Roads"

.field private static final ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC:Ljava/lang/String; = "Reroute_For_Traffic"

.field private static final ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE:Ljava/lang/String; = "Routing_Type"

.field private static final ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING:Ljava/lang/String; = "Show_Traffic_Navigating"

.field private static final ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP:Ljava/lang/String; = "Show_Traffic_Open_Map"

.field private static final ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS:Ljava/lang/String; = "Spoken_Speed_Warnings"

.field private static final ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT:Ljava/lang/String; = "Spoken_Turn_By_Turn"

.field private static final ATTR_NEARBY_SEARCH_DEST_SCROLLED:Ljava/lang/String; = "Scrolled"

.field private static final ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION:Ljava/lang/String; = "Selected_Position"

.field private static final ATTR_NEARBY_SEARCH_NO_RESULTS_ACTION:Ljava/lang/String; = "Action"

.field private static final ATTR_NEARBY_SEARCH_PLACE_TYPE:Ljava/lang/String; = "Place_Type"

.field private static final ATTR_OBD_CONN_ECU:Ljava/lang/String; = "ECU"

.field private static final ATTR_OBD_CONN_FIRMWARE_VERSION:Ljava/lang/String; = "Obd_Chip_Firmware"

.field private static final ATTR_OBD_CONN_PROTOCOL:Ljava/lang/String; = "Protocol"

.field private static final ATTR_OBD_CONN_VIN:Ljava/lang/String; = "VIN"

.field private static final ATTR_OBD_DATA_CAR_MAKE:Ljava/lang/String; = "Car_Make"

.field private static final ATTR_OBD_DATA_CAR_MODEL:Ljava/lang/String; = "Car_Model"

.field private static final ATTR_OBD_DATA_CAR_VIN:Ljava/lang/String; = "Vin"

.field private static final ATTR_OBD_DATA_CAR_YEAR:Ljava/lang/String; = "Car_Year"

.field private static final ATTR_OBD_DATA_MONITORING_DATA_SIZE:Ljava/lang/String; = "Data_Size"

.field private static final ATTR_OBD_DATA_MONITORING_SUCCESS:Ljava/lang/String; = "Success"

.field private static final ATTR_OBD_DATA_VERSION:Ljava/lang/String; = "Version"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE:Ljava/lang/String; = "New_Date"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES:Ljava/lang/String; = "New_Packages"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION:Ljava/lang/String; = "New_Version"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE:Ljava/lang/String; = "Old_Date"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES:Ljava/lang/String; = "Old_Packages"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION:Ljava/lang/String; = "Old_Version"

.field private static final ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT:Ljava/lang/String; = "Update_Count"

.field private static final ATTR_OPTION_NAME:Ljava/lang/String; = "Name"

.field private static final ATTR_OTA_CURRENT_VERSION:Ljava/lang/String; = "Current_Version"

.field private static final ATTR_OTA_INSTALL_INCREMENTAL:Ljava/lang/String; = "Incremental"

.field private static final ATTR_OTA_INSTALL_PRIOR_VERSION:Ljava/lang/String; = "Prior_Version"

.field private static final ATTR_OTA_INSTALL_SUCCESS:Ljava/lang/String; = "Success"

.field private static final ATTR_OTA_UPDATE_VERSION:Ljava/lang/String; = "Update_Version"

.field private static final ATTR_PHONE_CONNECTION:Ljava/lang/String; = "Connection"

.field public static final ATTR_PLATFORM_ANDROID:Ljava/lang/String; = "Android"

.field public static final ATTR_PLATFORM_IOS:Ljava/lang/String; = "iOS"

.field private static final ATTR_PROFILE_BUILD_TYPE:Ljava/lang/String; = "Build_Type"

.field private static final ATTR_PROFILE_CAR_MAKE:Ljava/lang/String; = "Car_Make"

.field private static final ATTR_PROFILE_CAR_MODEL:Ljava/lang/String; = "Car_Model"

.field private static final ATTR_PROFILE_CAR_YEAR:Ljava/lang/String; = "Car_Year"

.field private static final ATTR_PROFILE_HUD_VERSION:Ljava/lang/String; = "Hud_Version_Number"

.field private static final ATTR_PROFILE_SERIAL_NUMBER:Ljava/lang/String; = "Hud_Serial_Number"

.field private static final ATTR_REGION_COUNTRY:Ljava/lang/String; = "Country"

.field private static final ATTR_REGION_STATE:Ljava/lang/String; = "State"

.field private static final ATTR_REMOTE_DEVICE_HARDWARE:Ljava/lang/String; = "Remote_Device_Hardware"

.field private static final ATTR_REMOTE_DEVICE_PLATFORM:Ljava/lang/String; = "Remote_Device_Platform"

.field private static final ATTR_REMOTE_DEVICE_SW_VERSION:Ljava/lang/String; = "Remote_Device_SW_Version"

.field private static final ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE:Ljava/lang/String; = "Actual_Distance"

.field private static final ATTR_ROUTE_COMPLETED_ACTUAL_DURATION:Ljava/lang/String; = "Actual_Duration"

.field private static final ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE:Ljava/lang/String; = "Distance_PCT_Diff"

.field private static final ATTR_ROUTE_COMPLETED_DURATION_VARIANCE:Ljava/lang/String; = "Duration_PCT_Diff"

.field private static final ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE:Ljava/lang/String; = "Expected_Distance"

.field private static final ATTR_ROUTE_COMPLETED_EXPECTED_DURATION:Ljava/lang/String; = "Expected_Duration"

.field private static final ATTR_ROUTE_COMPLETED_RECALCULATIONS:Ljava/lang/String; = "Recalculations"

.field private static final ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL:Ljava/lang/String; = "Traffic_Level"

.field private static final ATTR_ROUTE_DISPLAY_POS:Ljava/lang/String; = "Display_Pos"

.field private static final ATTR_ROUTE_NAV_POS:Ljava/lang/String; = "Nav_Pos"

.field private static final ATTR_ROUTE_STREET_ADDRESS:Ljava/lang/String; = "Street_Addr"

.field private static final ATTR_SESSION_ACCELERATION_COUNT:Ljava/lang/String; = "Session_Hard_Acceleration_Count"

.field private static final ATTR_SESSION_AVERAGE_MPG:Ljava/lang/String; = "Session_Average_Mpg"

.field private static final ATTR_SESSION_AVERAGE_ROLLING_SPEED:Ljava/lang/String; = "Session_Average_Rolling_Speed"

.field private static final ATTR_SESSION_AVERAGE_SPEED:Ljava/lang/String; = "Session_Average_Speed"

.field private static final ATTR_SESSION_DISTANCE:Ljava/lang/String; = "Session_Distance"

.field private static final ATTR_SESSION_DURATION:Ljava/lang/String; = "Session_Duration"

.field private static final ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY:Ljava/lang/String; = "Session_Excessive_Speeding_Frequency"

.field private static final ATTR_SESSION_HARD_BRAKING_COUNT:Ljava/lang/String; = "Session_Hard_Braking_Count"

.field private static final ATTR_SESSION_HIGH_G_COUNT:Ljava/lang/String; = "Session_High_G_Count"

.field private static final ATTR_SESSION_MAX_RPM:Ljava/lang/String; = "Session_Max_RPM"

.field private static final ATTR_SESSION_MAX_SPEED:Ljava/lang/String; = "Session_Max_Speed"

.field private static final ATTR_SESSION_ROLLING_DURATION:Ljava/lang/String; = "Session_Rolling_Duration"

.field private static final ATTR_SESSION_SPEEDING_FREQUENCY:Ljava/lang/String; = "Session_Speeding_Frequency"

.field private static final ATTR_SESSION_TROUBLE_CODE_COUNT:Ljava/lang/String; = "Session_Trouble_Code_Count"

.field private static final ATTR_SESSION_TROUBLE_CODE_LAST:Ljava/lang/String; = "Session_Trouble_Code_Last"

.field private static final ATTR_SHUTDOWN_BATTERY_LEVEL:Ljava/lang/String; = "Battery_Level"

.field private static final ATTR_SHUTDOWN_REASON:Ljava/lang/String; = "Reason"

.field private static final ATTR_SHUTDOWN_SLEEP_TIME:Ljava/lang/String; = "Sleep_Time"

.field private static final ATTR_SHUTDOWN_TYPE:Ljava/lang/String; = "Type"

.field private static final ATTR_SHUTDOWN_UPTIME:Ljava/lang/String; = "Uptime"

.field private static final ATTR_SMS_MESSAGE_TYPE:Ljava/lang/String; = "Message_Type"

.field private static final ATTR_SMS_MESSAGE_TYPE_CANNED:Ljava/lang/String; = "Canned"

.field private static final ATTR_SMS_MESSAGE_TYPE_CUSTOM:Ljava/lang/String; = "Custom"

.field private static final ATTR_SMS_PLATFORM:Ljava/lang/String; = "Platform"

.field private static final ATTR_SMS_SUCCESS:Ljava/lang/String; = "Success"

.field private static final ATTR_SPEEDING_AMOUNT:Ljava/lang/String; = "Session_Speeding_Amount"

.field private static final ATTR_TEMP_CPU_AVERAGE:Ljava/lang/String; = "CPU_Average"

.field private static final ATTR_TEMP_CPU_MAX:Ljava/lang/String; = "CPU_Max"

.field private static final ATTR_TEMP_CPU_TEMP:Ljava/lang/String; = "CPU_Temperature"

.field private static final ATTR_TEMP_DMD_AVERAGE:Ljava/lang/String; = "DMD_Average"

.field private static final ATTR_TEMP_DMD_MAX:Ljava/lang/String; = "DMD_Max"

.field private static final ATTR_TEMP_DMD_TEMP:Ljava/lang/String; = "DMD_Temperature"

.field private static final ATTR_TEMP_FAN_SPEED:Ljava/lang/String; = "Fan_Speed"

.field private static final ATTR_TEMP_INLET_AVERAGE:Ljava/lang/String; = "Inlet_Average"

.field private static final ATTR_TEMP_INLET_MAX:Ljava/lang/String; = "Inlet_Max"

.field private static final ATTR_TEMP_INLET_TEMP:Ljava/lang/String; = "Inlet_Temperature"

.field private static final ATTR_TEMP_LED_AVERAGE:Ljava/lang/String; = "LED_Average"

.field private static final ATTR_TEMP_LED_MAX:Ljava/lang/String; = "LED_Max"

.field private static final ATTR_TEMP_LED_TEMP:Ljava/lang/String; = "LED_Temperature"

.field private static final ATTR_TEMP_WARNING:Ljava/lang/String; = "Warning"

.field private static final ATTR_UPDATE_ACCEPTED:Ljava/lang/String; = "Accepted"

.field private static final ATTR_UPDATE_TO_VERSION:Ljava/lang/String; = "Update_To_Version"

.field private static final ATTR_VEHICLE_KILO_METERS:Ljava/lang/String; = "Vehicle_Kilo_Meters"

.field private static final ATTR_VIN:Ljava/lang/String; = "Vin"

.field private static final ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE:Ljava/lang/String; = "Additional_Results_Glance"

.field private static final ATTR_VOICE_SEARCH_CANCEL_TYPE:Ljava/lang/String; = "Cancel_Type"

.field private static final ATTR_VOICE_SEARCH_CLIENT_VERSION:Ljava/lang/String; = "Client_Version"

.field private static final ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL:Ljava/lang/String; = "Confidence_Level"

.field private static final ATTR_VOICE_SEARCH_DESTINATION_TYPE:Ljava/lang/String; = "Destination_Type"

.field private static final ATTR_VOICE_SEARCH_EXPLICIT_RETRY:Ljava/lang/String; = "Explicit_Retry"

.field private static final ATTR_VOICE_SEARCH_LIST_SELECTION:Ljava/lang/String; = "Additional_Results_List_Item_Selected"

.field private static final ATTR_VOICE_SEARCH_MICROPHONE_USED:Ljava/lang/String; = "Microphone_Used"

.field private static final ATTR_VOICE_SEARCH_NAVIGATION_INITIATED:Ljava/lang/String; = "Navigation_Initiated"

.field private static final ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY:Ljava/lang/String; = "Navigation_Cancelled_Quickly"

.field private static final ATTR_VOICE_SEARCH_NUMBER_OF_WORDS:Ljava/lang/String; = "Number_Of_Words"

.field private static final ATTR_VOICE_SEARCH_NUM_RESULTS:Ljava/lang/String; = "Num_Total_Results"

.field private static final ATTR_VOICE_SEARCH_PREFIX:Ljava/lang/String; = "Prefix"

.field private static final ATTR_VOICE_SEARCH_RESPONSE:Ljava/lang/String; = "Response"

.field private static final ATTR_VOICE_SEARCH_RETRY_COUNT:Ljava/lang/String; = "Retry_Count"

.field private static final ATTR_VOICE_SEARCH_SUGGESTION:Ljava/lang/String; = "Suggestion"

.field private static final ATTR_WAKEUP_BATTERY_DRAIN:Ljava/lang/String; = "Battery_Drain"

.field private static final ATTR_WAKEUP_BATTERY_LEVEL:Ljava/lang/String; = "Battery_Level"

.field private static final ATTR_WAKEUP_MAX_BATTERY:Ljava/lang/String; = "Max_Battery"

.field private static final ATTR_WAKEUP_REASON:Ljava/lang/String; = "Reason"

.field private static final ATTR_WAKEUP_SLEEP_TIME:Ljava/lang/String; = "Sleep_Time"

.field private static final ATTR_WAKEUP_VIN:Ljava/lang/String; = "VIN"

.field private static final CUSTOM_DIMENSION_HW_VERSION:I = 0x0

.field private static final DESTINATION_SUGGESTION_TYPE_CALENDAR:Ljava/lang/String; = "Calendar"

.field private static final DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION:Ljava/lang/String; = "SmartSuggestion"

.field private static final FALSE:Ljava/lang/String; = "false"

.field private static final FUEL_REPORTING_INTERVAL:J = 0x2bf20L

.field private static final LIGHT_SENSOR_DEVICE:Ljava/lang/String; = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"

.field private static final LIGHT_SENSOR_RAW1_DEVICE:Ljava/lang/String; = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"

.field private static final LIGHT_SENSOR_RAW_DEVICE:Ljava/lang/String; = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"

.field private static final MAX_CPU_TEMP:I = 0x5d

.field private static final MAX_DMD_HIGH_TIME_MS:J

.field private static final MAX_DMD_TEMP:I = 0x41

.field private static final MAX_INLET_HIGH_TIME_MS:J

.field private static final MAX_INLET_TEMP:I = 0x48

.field private static final NAVIGATION_CANCEL_TIMEOUT:J

.field private static final NOTEMP:I = -0x3e8

.field private static final OFFLINE_MAPS_UPDATE_COUNT:Ljava/lang/String; = "offlineMapsUpdateCount"

.field private static final PREF_CHANGE_DELAY_MS:I = 0x3a98

.field private static final PREVIOUS_OFFLINE_MAPS_DATE:Ljava/lang/String; = "previousOfflineMapsDate"

.field private static final PREVIOUS_OFFLINE_MAPS_PACKAGES:Ljava/lang/String; = "previousOfflineMapsPackages"

.field private static final PREVIOUS_OFFLINE_MAPS_VERSION:Ljava/lang/String; = "previousOfflineMapsVersion"

.field private static final TEMP_WARNING_BOOT_DELAY_S:J

.field private static final TEMP_WARNING_INTERVAL_MS:J

.field private static final TRUE:Ljava/lang/String; = "true"

.field private static final UPLOAD_INTERVAL:J

.field private static final UPLOAD_INTERVAL_BOOT:J

.field private static final UPLOAD_INTERVAL_LIMITED_BANDWIDTH:J

.field private static final VERBOSE:Z

.field private static currentCpuAvgTemperature:I

.field private static deferredEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

.field private static destinationSet:Z

.field private static disconnectDueToForceReconnect:Z

.field private static glanceNavigationSource:Ljava/lang/String;

.field private static handler:Landroid/os/Handler;

.field private static lastDiscReason:I

.field private static lastForceReconnectReason:Ljava/lang/String;

.field private static volatile localyticsInitialized:Z

.field private static nearbySearchDestScrollPosition:I

.field private static nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

.field private static newDevicePaired:Z

.field private static phonePaired:Z

.field private static quietMode:Z

.field private static remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

.field private static sFuelMonitor:Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;

.field private static sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

.field private static sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

.field private static sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

.field private static sLastVIN:Ljava/lang/String;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static sNotificationReceiver:Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;

.field private static sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

.field private static sRemoteDeviceId:Ljava/lang/String;

.field private static sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

.field private static sUploader:Ljava/lang/Runnable;

.field private static startTime:J

.field private static startingBatteryVoltage:D

.field private static startingDistance:I

.field private static voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

.field private static voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

.field private static voiceSearchConfidence:Ljava/lang/Integer;

.field private static voiceSearchCount:I

.field private static voiceSearchExplicitRetry:Z

.field private static voiceSearchListSelection:Ljava/lang/Integer;

.field private static voiceSearchListeningOverBluetooth:Z

.field private static voiceSearchPrefix:Ljava/lang/String;

.field private static voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

.field private static voiceSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private static voiceSearchWords:Ljava/lang/String;

.field private static welcomeScreenTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v8, 0xa

    const-wide/16 v2, 0x5

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 116
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 477
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    .line 478
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    .line 479
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 480
    sput v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I

    .line 481
    const-string v0, ""

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;

    .line 482
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->disconnectDueToForceReconnect:Z

    .line 483
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .line 484
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .line 486
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    .line 487
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    .line 490
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;

    .line 492
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    .line 493
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    .line 494
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingBatteryVoltage:D

    .line 501
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL_BOOT:J

    .line 502
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL:J

    .line 503
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL_LIMITED_BANDWIDTH:J

    .line 505
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sUploader:Ljava/lang/Runnable;

    .line 516
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    .line 979
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_BOOT_DELAY_S:J

    .line 980
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_INTERVAL_MS:J

    .line 981
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_INLET_HIGH_TIME_MS:J

    .line 982
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_DMD_HIGH_TIME_MS:J

    .line 994
    sput v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->currentCpuAvgTemperature:I

    .line 1504
    sput v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchDestScrollPosition:I

    .line 1505
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1571
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->welcomeScreenTime:J

    .line 1572
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->newDevicePaired:Z

    .line 1573
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->phonePaired:Z

    .line 1648
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->NAVIGATION_CANCEL_TIMEOUT:J

    .line 1659
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    .line 1660
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchWords:Ljava/lang/String;

    .line 1661
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;

    .line 1662
    sput v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    .line 1663
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    .line 1664
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;

    .line 1665
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1666
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListSelection:Ljava/lang/Integer;

    .line 1667
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    .line 1668
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;

    .line 1738
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    .line 2095
    sput-boolean v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->destinationSet:Z

    .line 2175
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    .line 2233
    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100()J
    .locals 2

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getUpdateInterval()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPreference(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavigationPreference(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordWakeup(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V

    return-void
.end method

.method static synthetic access$1702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 105
    sput-boolean p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    return p0
.end method

.method static synthetic access$1800()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sUploader:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1900()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->reportObdState()V

    return-void
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->processNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->processNavigationArrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/analytics/Event;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/Event;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    invoke-static {p0, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V

    return-void
.end method

.method static synthetic access$2300()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_INLET_HIGH_TIME_MS:J

    return-wide v0
.end method

.method static synthetic access$2400()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->MAX_DMD_HIGH_TIME_MS:J

    return-wide v0
.end method

.method static synthetic access$2500(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->parseTemp(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 105
    sput p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->currentCpuAvgTemperature:I

    return p0
.end method

.method static synthetic access$2700()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_INTERVAL_MS:J

    return-wide v0
.end method

.method static synthetic access$2800()J
    .locals 2

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2900()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->TEMP_WARNING_BOOT_DELAY_S:J

    return-wide v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3100()I
    .locals 1

    .prologue
    .line 105
    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I

    return v0
.end method

.method static synthetic access$3102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 105
    sput p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I

    return p0
.end method

.method static synthetic access$3202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 105
    sput-boolean p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->disconnectDueToForceReconnect:Z

    return p0
.end method

.method static synthetic access$3300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3400()Lcom/navdy/hud/app/obd/ObdManager;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    return-object v0
.end method

.method static synthetic access$3500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchWords:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$3700()I
    .locals 1

    .prologue
    .line 105
    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    return v0
.end method

.method static synthetic access$3800()Z
    .locals 1

    .prologue
    .line 105
    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    return v0
.end method

.method static synthetic access$3900()Ljava/util/List;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4000()Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/service/library/events/DeviceInfo;)Lcom/navdy/service/library/events/DeviceInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    return-object p0
.end method

.method static synthetic access$4100()Z
    .locals 1

    .prologue
    .line 105
    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    return v0
.end method

.method static synthetic access$4200()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListSelection:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$4300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Integer;
    .param p4, "x4"    # I
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/util/List;
    .param p7, "x7"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    .param p8, "x8"    # Z
    .param p9, "x9"    # Ljava/lang/Integer;
    .param p10, "x10"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static/range {p0 .. p10}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendVoiceSearchEvent(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4502(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    return-object p0
.end method

.method static synthetic access$4600()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNewDevicePaired()V

    return-void
.end method

.method static synthetic access$4700(Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Z
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setupProfile()V

    return-void
.end method

.method static synthetic access$600(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/SharedPreferences;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordOfflineMapsChange(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic access$700()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->maybeOpenSession()V

    return-void
.end method

.method static synthetic access$800()V
    .locals 0

    .prologue
    .line 105
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->closeSession()V

    return-void
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 105
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    return-object p0
.end method

.method public static analyticsApplicationInit(Landroid/app/Application;)V
    .locals 6
    .param p0, "app"    # Landroid/app/Application;

    .prologue
    .line 1188
    new-instance v2, Lcom/localytics/android/LocalyticsActivityLifecycleCallbacks;

    invoke-direct {v2, p0}, Lcom/localytics/android/LocalyticsActivityLifecycleCallbacks;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1189
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/localytics/android/Localytics;->setPushDisabled(Z)V

    .line 1190
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setupCustomDimensions()V

    .line 1191
    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;

    invoke-direct {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sNotificationReceiver:Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;

    .line 1192
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    sput-wide v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startTime:J

    .line 1193
    new-instance v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;

    invoke-direct {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;-><init>()V

    .line 1194
    .local v1, "tempReporter":Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;
    const-string v2, "TemperatureReporter"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;->setName(Ljava/lang/String;)V

    .line 1195
    invoke-virtual {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;->start()V

    .line 1196
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 1197
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 1198
    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;)V

    sput-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sFuelMonitor:Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;

    .line 1199
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sNotificationReceiver:Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;

    iget-object v2, v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v2

    sput-boolean v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    .line 1200
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->maybeOpenSession()V

    .line 1201
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1202
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.navdy.hud.app.force_reconnect"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1203
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$2;

    invoke-direct {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$2;-><init>()V

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1226
    return-void
.end method

.method private static clearVoiceSearch()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1741
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    .line 1742
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchWords:Ljava/lang/String;

    .line 1743
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;

    .line 1744
    sput-boolean v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    .line 1745
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;

    .line 1746
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1747
    sput-boolean v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    .line 1748
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListSelection:Ljava/lang/Integer;

    .line 1749
    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;

    .line 1750
    return-void
.end method

.method private static declared-synchronized closeSession()V
    .locals 3

    .prologue
    .line 1301
    const-class v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1308
    :goto_0
    monitor-exit v1

    return-void

    .line 1304
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendDistance()V

    .line 1305
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "closeSession()"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1306
    invoke-static {}, Lcom/localytics/android/Localytics;->closeSession()V

    .line 1307
    const-string v0, "unknown"

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1301
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static countWords(Ljava/lang/String;)I
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x20

    .line 1830
    const/4 v0, 0x0

    .line 1831
    .local v0, "count":I
    if-eqz p0, :cond_2

    .line 1832
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 1833
    const/4 v1, 0x0

    .line 1834
    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 1835
    .local v2, "slen":I
    :cond_0
    if-ge v1, v2, :cond_2

    .line 1836
    add-int/lit8 v0, v0, 0x1

    .line 1837
    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1838
    :cond_1
    :goto_1
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1841
    .end local v1    # "i":I
    .end local v2    # "slen":I
    :cond_2
    return v0
.end method

.method private static declared-synchronized deferLocalyticsEvent(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 2236
    const-class v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    if-nez v0, :cond_1

    .line 2237
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2238
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    .line 2240
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2241
    const/4 v0, 0x1

    .line 2243
    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2236
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getClientVersion()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1855
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v1

    .line 1856
    .local v1, "devInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 1857
    .local v0, "clientVersion":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_0

    .line 1858
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "failed to get client version"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1859
    const-string v0, ""

    .line 1861
    :cond_0
    return-object v0

    .line 1856
    .end local v0    # "clientVersion":Ljava/lang/String;
    :cond_1
    iget-object v0, v1, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getCurrentTemperature()I
    .locals 1

    .prologue
    .line 997
    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->currentCpuAvgTemperature:I

    return v0
.end method

.method private static getHwVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/navdy/hud/app/util/SerialNumber;->instance:Lcom/navdy/hud/app/util/SerialNumber;

    iget-object v1, v1, Lcom/navdy/hud/app/util/SerialNumber;->configurationCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/util/SerialNumber;->instance:Lcom/navdy/hud/app/util/SerialNumber;

    iget-object v1, v1, Lcom/navdy/hud/app/util/SerialNumber;->revisionCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUpdateInterval()J
    .locals 2

    .prologue
    .line 2333
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL_LIMITED_BANDWIDTH:J

    .line 2336
    :goto_0
    return-wide v0

    :cond_0
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL:J

    goto :goto_0
.end method

.method private static getVoiceSearchEvent()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1845
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1846
    .local v0, "phone_type":Ljava/lang/String;
    :goto_0
    const/4 v1, 0x0

    .line 1847
    .local v1, "res":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1848
    const-string v2, "PLATFORM_Android"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "Voice_Search_Android"

    .line 1851
    :cond_0
    :goto_1
    return-object v1

    .line 1845
    .end local v0    # "phone_type":Ljava/lang/String;
    .end local v1    # "res":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1848
    .restart local v0    # "phone_type":Ljava/lang/String;
    .restart local v1    # "res":Ljava/lang/String;
    :cond_2
    const-string v1, "Voice_Search_iOS"

    goto :goto_1
.end method

.method private static isEnabled(Z)Ljava/lang/String;
    .locals 1
    .param p0, "val"    # Z

    .prologue
    .line 811
    if-eqz p0, :cond_0

    const-string v0, "Enabled"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Disabled"

    goto :goto_0
.end method

.method private static isTimeValid()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1183
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1184
    .local v0, "currentTime":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0x7e0

    if-lt v2, v3, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V
    .locals 3
    .param p0, "event"    # Lcom/navdy/hud/app/analytics/Event;
    .param p1, "immediate"    # Z

    .prologue
    .line 2307
    if-eqz p1, :cond_0

    .line 2308
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/Event;->tag:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/Event;->argMap:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/localytics/android/Localytics;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 2312
    :goto_0
    return-void

    .line 2310
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$EventSender;-><init>(Lcom/navdy/hud/app/analytics/Event;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static varargs localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V
    .locals 6
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "includeDeviceInfo"    # Z
    .param p2, "immediate"    # Z
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 2282
    if-nez p2, :cond_0

    invoke-static {p0, p3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferLocalyticsEvent(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2299
    :goto_0
    return-void

    .line 2286
    :cond_0
    new-instance v2, Lcom/navdy/hud/app/analytics/Event;

    invoke-direct {v2, p0, p3}, Lcom/navdy/hud/app/analytics/Event;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2287
    .local v2, "event":Lcom/navdy/hud/app/analytics/Event;
    iget-object v0, v2, Lcom/navdy/hud/app/analytics/Event;->argMap:Ljava/util/Map;

    .line 2288
    .local v0, "argMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    .line 2289
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v1

    .line 2290
    .local v1, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v1, :cond_2

    .line 2291
    iget-object v3, v1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-eqz v3, :cond_1

    .line 2292
    const-string v3, "Remote_Device_Platform"

    iget-object v4, v1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2294
    :cond_1
    const-string v3, "Remote_Device_Hardware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295
    const-string v3, "Remote_Device_SW_Version"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2298
    .end local v1    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_2
    invoke-static {v2, p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V

    goto :goto_0
.end method

.method private static varargs localyticsSendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "includeDeviceInfo"    # Z
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 2278
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 2279
    return-void
.end method

.method public static varargs localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2274
    invoke-static {p0, v0, v0, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 2275
    return-void
.end method

.method private static maybeOpenSession()V
    .locals 2

    .prologue
    .line 1230
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isTimeValid()Z

    move-result v1

    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 1231
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->openSession()V

    .line 1233
    :cond_0
    return-void

    .line 1230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static normalizeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2040
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 2041
    .local v1, "ca":[C
    const/4 v3, 0x1

    .line 2042
    .local v3, "wordStart":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_2

    .line 2043
    aget-char v0, v1, v2

    .line 2044
    .local v0, "c":C
    if-eqz v3, :cond_0

    .line 2045
    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    .line 2047
    :cond_0
    const/16 v4, 0x5f

    if-ne v0, v4, :cond_1

    const/4 v3, 0x1

    .line 2048
    :goto_1
    aput-char v0, v1, v2

    .line 2042
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2047
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 2050
    .end local v0    # "c":C
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method private static declared-synchronized openSession()V
    .locals 6

    .prologue
    .line 1238
    const-class v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    if-nez v0, :cond_0

    .line 1239
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "openSession()"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1240
    invoke-static {}, Lcom/localytics/android/Localytics;->openSession()V

    .line 1241
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    .line 1242
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendDeferredLocalyticsEvents()V

    .line 1243
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sFuelMonitor:Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;

    const-wide/32 v4, 0x2bf20

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1244
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sUploader:Ljava/lang/Runnable;

    sget-wide v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->UPLOAD_INTERVAL_BOOT:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1246
    :cond_0
    monitor-exit v1

    return-void

    .line 1238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static parseTemp(Ljava/lang/String;)I
    .locals 5
    .param p0, "tempStr"    # Ljava/lang/String;

    .prologue
    .line 985
    const/16 v1, -0x3e8

    .line 987
    .local v1, "result":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 991
    :goto_0
    return v1

    .line 988
    :catch_0
    move-exception v0

    .line 989
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid temperature: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static pidListToHexString(Ljava/util/List;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1436
    .local p0, "pList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v6, 0x5

    new-array v0, v6, [J

    .line 1437
    .local v0, "bits":[J
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/Pid;

    .line 1438
    .local v2, "p":Lcom/navdy/obd/Pid;
    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v5

    .line 1439
    .local v5, "v":I
    div-int/lit8 v7, v5, 0x40

    aget-wide v8, v0, v7

    const-wide/16 v10, 0x1

    rem-int/lit8 v12, v5, 0x40

    shl-long/2addr v10, v12

    or-long/2addr v8, v10

    aput-wide v8, v0, v7

    goto :goto_0

    .line 1441
    .end local v2    # "p":Lcom/navdy/obd/Pid;
    .end local v5    # "v":I
    :cond_0
    const-string v4, ""

    .line 1442
    .local v4, "sep":Ljava/lang/String;
    const-string v3, ""

    .line 1443
    .local v3, "res":Ljava/lang/String;
    array-length v6, v0

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_1

    .line 1444
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%08x"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aget-wide v12, v0, v1

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1445
    const-string v4, " "

    .line 1443
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1447
    :cond_1
    return-object v3
.end method

.method private static processNavigationArrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 4
    .param p0, "unused"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    .prologue
    .line 1974
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v0, :cond_0

    .line 1975
    const-string v0, "Nearby_Search_Arrived"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Place_Type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1976
    invoke-virtual {v3}, Lcom/navdy/service/library/events/places/PlaceType;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1975
    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1977
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1979
    :cond_0
    return-void
.end method

.method private static processNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 6
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    const/4 v1, 0x0

    .line 1948
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v2, :cond_0

    .line 1949
    const-string v2, "Nearby_Search_Trip_Initiated"

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1971
    :goto_0
    return-void

    .line 1953
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_ROUTE_SELECTED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-eq v2, v3, :cond_1

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_DESTINATION_FOUND:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-ne v2, v3, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 1957
    .local v0, "isVoiceSearchRoute":Z
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    if-eqz v2, :cond_2

    .line 1958
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    if-eqz v0, :cond_5

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    :goto_2
    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->cancel(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;)V

    .line 1962
    :cond_2
    if-eqz v0, :cond_6

    .line 1963
    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    sput-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    .line 1964
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    sget-wide v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->NAVIGATION_CANCEL_TIMEOUT:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1965
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    .line 1970
    :cond_3
    :goto_3
    sput v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    goto :goto_0

    .end local v0    # "isVoiceSearchRoute":Z
    :cond_4
    move v0, v1

    .line 1953
    goto :goto_1

    .line 1958
    .restart local v0    # "isVoiceSearchRoute":Z
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_non_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    goto :goto_2

    .line 1966
    :cond_6
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-eqz v2, :cond_3

    .line 1967
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received NavigationRouteRequest with unexpected voice progress state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static readLightSensor(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "devicePath"    # Ljava/lang/String;

    .prologue
    .line 1348
    const-string v1, ""

    .line 1350
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1354
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1351
    :catch_0
    move-exception v0

    .line 1352
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception reading light sensor device: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static recordAdaptiveAutobrightnessAdjustmentChanged(I)V
    .locals 7
    .param p0, "brightness"    # I

    .prologue
    .line 1388
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1389
    .local v2, "lightSensorValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1390
    .local v1, "lightSensorRawValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1391
    .local v0, "lightSensorRaw1Value":Ljava/lang/String;
    const-string v3, "Adaptive_Brightness_Adjustment_Change"

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Brightness"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 1392
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "Light_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v2, v4, v5

    const/4 v5, 0x4

    const-string v6, "Raw_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v1, v4, v5

    const/4 v5, 0x6

    const-string v6, "Raw1_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object v0, v4, v5

    .line 1391
    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1396
    return-void
.end method

.method public static recordAutobrightnessAdjustmentChanged(III)V
    .locals 7
    .param p0, "brightness"    # I
    .param p1, "prevAdjustment"    # I
    .param p2, "newAdjustment"    # I

    .prologue
    .line 1373
    if-eq p1, p2, :cond_0

    .line 1374
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1375
    .local v2, "lightSensorValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1376
    .local v1, "lightSensorRawValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1377
    .local v0, "lightSensorRaw1Value":Ljava/lang/String;
    const-string v3, "Brightness_Adjustment_Change"

    const/16 v4, 0xc

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Brightness"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 1378
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "Previous"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 1379
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "New"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    .line 1380
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "Light_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object v2, v4, v5

    const/16 v5, 0x8

    const-string v6, "Raw_Sensor"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    aput-object v1, v4, v5

    const/16 v5, 0xa

    const-string v6, "Raw1_Sensor"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    aput-object v0, v4, v5

    .line 1377
    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1385
    .end local v0    # "lightSensorRaw1Value":Ljava/lang/String;
    .end local v1    # "lightSensorRawValue":Ljava/lang/String;
    .end local v2    # "lightSensorValue":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static recordBadRoutePosition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "displayPos"    # Ljava/lang/String;
    .param p1, "navPos"    # Ljava/lang/String;
    .param p2, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 1497
    const-string v0, "Route_Bad_Pos"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Display_Pos"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "Nav_Pos"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string v3, "Street_Addr"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1498
    return-void
.end method

.method public static recordBrightnessChanged(II)V
    .locals 7
    .param p0, "prevBrightness"    # I
    .param p1, "newBrightness"    # I

    .prologue
    .line 1358
    if-eq p0, p1, :cond_0

    .line 1359
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1360
    .local v2, "lightSensorValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1361
    .local v1, "lightSensorRawValue":Ljava/lang/String;
    const-string v3, "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->readLightSensor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1363
    .local v0, "lightSensorRaw1Value":Ljava/lang/String;
    const-string v3, "Brightness_Change"

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Previous"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 1364
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "New"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 1365
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "Light_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v2, v4, v5

    const/4 v5, 0x6

    const-string v6, "Raw_Sensor"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object v1, v4, v5

    const/16 v5, 0x8

    const-string v6, "Raw1_Sensor"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    aput-object v0, v4, v5

    .line 1363
    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1370
    .end local v0    # "lightSensorRaw1Value":Ljava/lang/String;
    .end local v1    # "lightSensorRawValue":Ljava/lang/String;
    .end local v2    # "lightSensorValue":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static recordCpuOverheat(Ljava/lang/String;)V
    .locals 5
    .param p0, "state"    # Ljava/lang/String;

    .prologue
    .line 760
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 761
    .local v0, "uptime":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Recording CPU overheat event uptime:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 762
    const-string v1, "Cpu_Overheat"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Uptime"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    const-string v4, "State"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 765
    return-void
.end method

.method public static recordDashPreference(Ljava/lang/String;Z)V
    .locals 4
    .param p0, "gauge"    # Ljava/lang/String;
    .param p1, "leftSide"    # Z

    .prologue
    .line 2213
    const-string v1, "Dash_Gauge_Setting"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Side"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    const-string v0, "Left"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "Gauge_name"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object p0, v2, v0

    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2214
    return-void

    .line 2213
    :cond_0
    const-string v0, "Right"

    goto :goto_0
.end method

.method public static recordDestinationSuggestion(ZZ)V
    .locals 4
    .param p0, "accepted"    # Z
    .param p1, "calendar"    # Z

    .prologue
    .line 2066
    const-string v1, "Destination_Suggestion_Show"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Accepted"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    if-eqz p0, :cond_0

    const-string v0, "true"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "Type"

    aput-object v3, v2, v0

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    const-string v0, "Calendar"

    :goto_1
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2069
    return-void

    .line 2066
    :cond_0
    const-string v0, "false"

    goto :goto_0

    :cond_1
    const-string v0, "SmartSuggestion"

    goto :goto_1
.end method

.method public static recordDownloadOTAUpdate(Landroid/content/SharedPreferences;)V
    .locals 6
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 1312
    sget-object v0, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 1313
    .local v0, "currentVersion":Ljava/lang/String;
    invoke-static {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->getIncrementalUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    .line 1314
    .local v1, "updateVersion":Ljava/lang/String;
    const-string v2, "OTA_Download"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Current_Version"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    const-string v5, "Update_Version"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1319
    return-void
.end method

.method public static recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V
    .locals 7
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;
    .param p2, "navMethod"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1611
    if-nez p2, :cond_0

    .line 1612
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    if-eqz v2, :cond_1

    sget-object p2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    .line 1614
    :cond_0
    :goto_0
    const/4 v2, 0x0

    sput-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    .line 1615
    if-eqz p1, :cond_3

    .line 1617
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v1

    .line 1618
    .local v1, "nt":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    instance-of v2, p1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    if-eqz v2, :cond_2

    .line 1619
    check-cast p1, Lcom/navdy/hud/app/framework/glance/GlanceNotification;

    .end local p1    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/glance/GlanceNotification;->getGlanceApp()Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1623
    .local v0, "glanceName":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Glance_Name"

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    const-string v3, "Nav_Method"

    aput-object v3, v2, v6

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {p0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1627
    .end local v0    # "glanceName":Ljava/lang/String;
    .end local v1    # "nt":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    :goto_2
    return-void

    .line 1612
    .restart local p1    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_1
    const-string p2, "unknown"

    goto :goto_0

    .line 1621
    .restart local v1    # "nt":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    :cond_2
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationType;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "glanceName":Ljava/lang/String;
    goto :goto_1

    .line 1625
    .end local v0    # "glanceName":Ljava/lang/String;
    .end local v1    # "nt":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    :cond_3
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "Nav_Method"

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    invoke-static {p0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static recordGlympseSent(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "success"    # Z
    .param p1, "share"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    .line 2353
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glympse sent id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sucess="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " share="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2354
    const-string v0, "Glympse_Sent"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Success"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 2355
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Share"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string v3, "Message"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p2, v1, v2

    const/4 v2, 0x6

    const-string v3, "Id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    aput-object p3, v1, v2

    .line 2354
    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2359
    return-void
.end method

.method public static recordGlympseViewed(Ljava/lang/String;)V
    .locals 4
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 2362
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glympse viewed id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2363
    const-string v0, "Glympse_Viewed"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2365
    return-void
.end method

.method public static recordIncorrectFuelData()V
    .locals 8

    .prologue
    .line 949
    const/4 v1, 0x0

    .line 950
    .local v1, "make":Ljava/lang/String;
    const/4 v2, 0x0

    .line 951
    .local v2, "model":Ljava/lang/String;
    const/4 v3, 0x0

    .line 953
    .local v3, "year":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 954
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v0, :cond_0

    .line 955
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v1

    .line 956
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v2

    .line 957
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v3

    .line 960
    :cond_0
    const-string v4, "OBD_Zero_Fuel_Level"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Car_Make"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v6, 0x2

    const-string v7, "Car_Model"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object v2, v5, v6

    const/4 v6, 0x4

    const-string v7, "Car_Year"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 965
    return-void
.end method

.method public static recordInstallOTAUpdate(Landroid/content/SharedPreferences;)V
    .locals 6
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 1322
    sget-object v0, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 1323
    .local v0, "currentVersion":Ljava/lang/String;
    invoke-static {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->getIncrementalUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    .line 1324
    .local v1, "updateVersion":Ljava/lang/String;
    const-string v2, "OTA_Install"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Current_Version"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    const-string v5, "Update_Version"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1329
    return-void
.end method

.method public static recordKeyFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "keyType"    # Ljava/lang/String;
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 1429
    const-string v0, "Key_Failed"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Key_Type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "Error"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1430
    return-void
.end method

.method public static recordMenuSelection(Ljava/lang/String;)V
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1492
    const-string v0, "Menu_Selection"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1493
    return-void
.end method

.method public static recordMusicAction(Ljava/lang/String;)V
    .locals 4
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 2084
    const-string v0, "Music_Action"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2086
    return-void
.end method

.method public static recordMusicBrowsePlayAction(Ljava/lang/String;)V
    .locals 3
    .param p0, "collectionType"    # Ljava/lang/String;

    .prologue
    .line 2089
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_played"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 2090
    return-void
.end method

.method public static recordNavdyMileageEvent(DDD)V
    .locals 4
    .param p0, "vehicleKiloMeters"    # D
    .param p2, "navdyKiloMeters"    # D
    .param p4, "navdyUsageRate"    # D

    .prologue
    .line 2217
    const-string v0, "Navdy_Mileage"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Vehicle_Kilo_Meters"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0, p1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Navdy_Kilometers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "Navdy_Usage_Rate"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p4, p5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2218
    return-void
.end method

.method public static recordNavigation(Z)V
    .locals 2
    .param p0, "started"    # Z

    .prologue
    .line 712
    if-eqz p0, :cond_0

    .line 713
    const-string v0, "Navigation_Destination_Set"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 714
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->destinationSet:Z

    .line 716
    :cond_0
    return-void
.end method

.method public static recordNavigationCancelled()V
    .locals 2

    .prologue
    .line 2025
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v0, :cond_1

    .line 2026
    const-string v0, "Nearby_Search_Ended"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2027
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "navigation ended for nearby search trip"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2028
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 2036
    :cond_0
    :goto_0
    return-void

    .line 2032
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    if-eqz v0, :cond_0

    .line 2033
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "navigation ended, cancelling voice search callback"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2034
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->end_trip:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->cancel(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;)V

    goto :goto_0
.end method

.method private static recordNavigationPreference(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;
    .locals 8
    .param p0, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 915
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 916
    .local v0, "prefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    sget-object v4, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    if-ne v3, v4, :cond_0

    const-string v2, "Fastest"

    .line 918
    .local v2, "routingType":Ljava/lang/String;
    :goto_0
    const-string v1, "Unknown"

    .line 919
    .local v1, "reroute":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic:[I

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 931
    :goto_1
    new-instance v3, Lcom/navdy/hud/app/analytics/Event;

    const-string v4, "Navigation_Preferences"

    const/16 v5, 0x1a

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Reroute_For_Traffic"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v6, 0x2

    const-string v7, "Routing_Type"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object v2, v5, v6

    const/4 v6, 0x4

    const-string v7, "Spoken_Turn_By_Turn"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 934
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "Spoken_Speed_Warnings"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 935
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "Show_Traffic_Open_Map"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    .line 936
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "Show_Traffic_Navigating"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    .line 937
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "Allow_Highways"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    .line 938
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "Allow_Toll_Roads"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    .line 939
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, "Allow_Ferries"

    aput-object v7, v5, v6

    const/16 v6, 0x11

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    .line 940
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x12

    const-string v7, "Allow_Tunnels"

    aput-object v7, v5, v6

    const/16 v6, 0x13

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    .line 941
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x14

    const-string v7, "Allow_Unpaved_Roads"

    aput-object v7, v5, v6

    const/16 v6, 0x15

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 942
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x16

    const-string v7, "Allow_Auto_Trains"

    aput-object v7, v5, v6

    const/16 v6, 0x17

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    .line 943
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x18

    const-string v7, "Allow_HOV_Lanes"

    aput-object v7, v5, v6

    const/16 v6, 0x19

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    .line 944
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v3, v4, v5}, Lcom/navdy/hud/app/analytics/Event;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v3

    .line 916
    .end local v1    # "reroute":Ljava/lang/String;
    .end local v2    # "routingType":Ljava/lang/String;
    :cond_0
    const-string v2, "Shortest"

    goto/16 :goto_0

    .line 921
    .restart local v1    # "reroute":Ljava/lang/String;
    .restart local v2    # "routingType":Ljava/lang/String;
    :pswitch_0
    const-string v1, "Automatic"

    .line 922
    goto/16 :goto_1

    .line 924
    :pswitch_1
    const-string v1, "Confirm"

    .line 925
    goto/16 :goto_1

    .line 927
    :pswitch_2
    const-string v1, "Never"

    goto/16 :goto_1

    .line 919
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static recordNavigationPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 1
    .param p0, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 909
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    if-eqz v0, :cond_0

    .line 910
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->enqueue(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 912
    :cond_0
    return-void
.end method

.method public static recordNearbySearchClosed()V
    .locals 2

    .prologue
    .line 1513
    const-string v0, "Nearby_Search_Closed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1514
    return-void
.end method

.method public static recordNearbySearchDestinationScroll(I)V
    .locals 1
    .param p0, "pos"    # I

    .prologue
    .line 1549
    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchDestScrollPosition:I

    if-le p0, v0, :cond_0

    .line 1550
    sput p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchDestScrollPosition:I

    .line 1552
    :cond_0
    return-void
.end method

.method public static recordNearbySearchDismiss()V
    .locals 2

    .prologue
    .line 1517
    const-string v0, "Nearby_Search_Dismiss"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1518
    return-void
.end method

.method public static recordNearbySearchNoResult(Z)V
    .locals 4
    .param p0, "retry"    # Z

    .prologue
    .line 1521
    const-string v1, "Nearby_Search_No_Results"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Action"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    if-eqz p0, :cond_0

    const-string v0, "Retry"

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1523
    return-void

    .line 1521
    :cond_0
    const-string v0, "Dismiss"

    goto :goto_0
.end method

.method public static recordNearbySearchResultsClose()V
    .locals 2

    .prologue
    .line 1545
    const-string v0, "Nearby_Search_Results_Close"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1546
    return-void
.end method

.method public static recordNearbySearchResultsDismiss()V
    .locals 2

    .prologue
    .line 1526
    const-string v0, "Nearby_Search_Results_Dismiss"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1527
    return-void
.end method

.method public static recordNearbySearchResultsView()V
    .locals 2

    .prologue
    .line 1530
    const-string v0, "Nearby_Search_Results_View"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1531
    const/4 v0, -0x1

    sput v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchDestScrollPosition:I

    .line 1532
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1533
    return-void
.end method

.method public static recordNearbySearchReturnMainMenu()V
    .locals 2

    .prologue
    .line 1508
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 1509
    const-string v0, "Nearby_Search_Return_Main_Menu"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1510
    return-void
.end method

.method public static recordNearbySearchSelection(Lcom/navdy/service/library/events/places/PlaceType;I)V
    .locals 6
    .param p0, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;
    .param p1, "pos"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1536
    const-string v0, "Menu_Selection"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "Name"

    aput-object v2, v1, v3

    const-string v2, "nearby_places_results"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1537
    const-string v1, "Nearby_Search_Selection"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Place_Type"

    aput-object v0, v2, v3

    .line 1538
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlaceType;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    const-string v0, "Selected_Position"

    aput-object v0, v2, v5

    const/4 v0, 0x3

    .line 1539
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "Scrolled"

    aput-object v3, v2, v0

    const/4 v3, 0x5

    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchDestScrollPosition:I

    if-lez v0, :cond_0

    const-string v0, "true"

    :goto_0
    aput-object v0, v2, v3

    .line 1537
    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1541
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1542
    return-void

    .line 1539
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method private static recordNewDevicePaired()V
    .locals 1

    .prologue
    .line 1576
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->newDevicePaired:Z

    .line 1577
    return-void
.end method

.method public static recordOTAInstallResult(ZLjava/lang/String;Z)V
    .locals 4
    .param p0, "incremental"    # Z
    .param p1, "priorVersion"    # Ljava/lang/String;
    .param p2, "success"    # Z

    .prologue
    .line 1334
    const-string v1, "OTA_Install_Result"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Incremental"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    if-eqz p0, :cond_0

    const-string v0, "true"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "Prior_Version"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object p1, v2, v0

    const/4 v0, 0x4

    const-string v3, "Success"

    aput-object v3, v2, v0

    const/4 v3, 0x5

    if-eqz p2, :cond_1

    const-string v0, "true"

    :goto_1
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1338
    return-void

    .line 1334
    :cond_0
    const-string v0, "false"

    goto :goto_0

    :cond_1
    const-string v0, "false"

    goto :goto_1
.end method

.method public static recordObdCanBusDataSent(Ljava/lang/String;)V
    .locals 9
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2371
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v3, :cond_0

    .line 2372
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v0

    .line 2373
    .local v0, "make":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v1

    .line 2374
    .local v1, "model":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v2

    .line 2375
    .local v2, "year":Ljava/lang/String;
    const-string v3, "OBD_Listening_Posted"

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "Car_Make"

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    const-string v5, "Car_Model"

    aput-object v5, v4, v8

    const/4 v5, 0x3

    aput-object v1, v4, v5

    const/4 v5, 0x4

    const-string v6, "Car_Year"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v2, v4, v5

    const/4 v5, 0x6

    const-string v6, "Version"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2379
    .end local v0    # "make":Ljava/lang/String;
    .end local v1    # "model":Ljava/lang/String;
    .end local v2    # "year":Ljava/lang/String;
    :goto_0
    return-void

    .line 2377
    :cond_0
    const-string v3, "OBD_Listening_Posted"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Version"

    aput-object v5, v4, v6

    aput-object p0, v4, v7

    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static recordObdCanBusMonitoringState(Ljava/lang/String;ZII)V
    .locals 11
    .param p0, "vin"    # Ljava/lang/String;
    .param p1, "success"    # Z
    .param p2, "approxDataSize"    # I
    .param p3, "version"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2385
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v3, :cond_1

    .line 2386
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v0

    .line 2387
    .local v0, "make":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v1

    .line 2388
    .local v1, "model":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v2

    .line 2389
    .local v2, "year":Ljava/lang/String;
    const-string v4, "OBD_Listening_Status"

    const/16 v3, 0xe

    new-array v5, v3, [Ljava/lang/String;

    const-string v3, "Vin"

    aput-object v3, v5, v6

    aput-object p0, v5, v7

    const-string v3, "Car_Make"

    aput-object v3, v5, v8

    aput-object v0, v5, v9

    const-string v3, "Car_Model"

    aput-object v3, v5, v10

    const/4 v3, 0x5

    aput-object v1, v5, v3

    const/4 v3, 0x6

    const-string v6, "Car_Year"

    aput-object v6, v5, v3

    const/4 v3, 0x7

    aput-object v2, v5, v3

    const/16 v3, 0x8

    const-string v6, "Success"

    aput-object v6, v5, v3

    const/16 v6, 0x9

    if-eqz p1, :cond_0

    const-string v3, "true"

    :goto_0
    aput-object v3, v5, v6

    const/16 v3, 0xa

    const-string v6, "Version"

    aput-object v6, v5, v3

    const/16 v3, 0xb

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xc

    const-string v6, "Data_Size"

    aput-object v6, v5, v3

    const/16 v3, 0xd

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2393
    .end local v0    # "make":Ljava/lang/String;
    .end local v1    # "model":Ljava/lang/String;
    .end local v2    # "year":Ljava/lang/String;
    :goto_1
    return-void

    .line 2389
    .restart local v0    # "make":Ljava/lang/String;
    .restart local v1    # "model":Ljava/lang/String;
    .restart local v2    # "year":Ljava/lang/String;
    :cond_0
    const-string v3, "false"

    goto :goto_0

    .line 2391
    .end local v0    # "make":Ljava/lang/String;
    .end local v1    # "model":Ljava/lang/String;
    .end local v2    # "year":Ljava/lang/String;
    :cond_1
    const-string v4, "OBD_Listening_Status"

    const/16 v3, 0x8

    new-array v5, v3, [Ljava/lang/String;

    const-string v3, "Vin"

    aput-object v3, v5, v6

    aput-object p0, v5, v7

    const-string v3, "Success"

    aput-object v3, v5, v8

    if-eqz p1, :cond_2

    const-string v3, "true"

    :goto_2
    aput-object v3, v5, v9

    const-string v3, "Version"

    aput-object v3, v5, v10

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x6

    const-string v6, "Data_Size"

    aput-object v6, v5, v3

    const/4 v3, 0x7

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v3, "false"

    goto :goto_2
.end method

.method private static recordOfflineMapsChange(Landroid/content/SharedPreferences;)V
    .locals 15
    .param p0, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 719
    const-string v11, "previousOfflineMapsVersion"

    const/4 v12, 0x0

    invoke-interface {p0, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 720
    .local v9, "previousVersion":Ljava/lang/String;
    const-string v11, "previousOfflineMapsPackages"

    const/4 v12, 0x0

    invoke-interface {p0, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 721
    .local v8, "previousPackages":Ljava/lang/String;
    const-string v11, "previousOfflineMapsDate"

    const/4 v12, 0x0

    invoke-interface {p0, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 722
    .local v7, "previousBuildDate":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    .line 723
    .local v3, "mapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 724
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getOfflineMapsVersion()Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    move-result-object v4

    .line 725
    .local v4, "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    if-eqz v4, :cond_1

    .line 726
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getPackages()Ljava/util/List;

    move-result-object v5

    .line 727
    .local v5, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_2

    const-string v11, ","

    invoke-static {v11, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    .line 728
    .local v6, "packages":Ljava/lang/String;
    :goto_0
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getVersion()Ljava/lang/String;

    move-result-object v10

    .line 729
    .local v10, "version":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getRawDate()Ljava/lang/String;

    move-result-object v0

    .line 730
    .local v0, "buildDate":Ljava/lang/String;
    if-eqz v10, :cond_3

    .line 731
    :goto_1
    if-eqz v0, :cond_4

    .line 732
    :goto_2
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 733
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 734
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 736
    :cond_0
    const-string v11, "offlineMapsUpdateCount"

    const/4 v12, 0x0

    invoke-interface {p0, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 737
    .local v1, "count":I
    add-int/lit8 v1, v1, 0x1

    .line 738
    new-instance v2, Lcom/navdy/hud/app/analytics/Event;

    const-string v11, "Offline_Maps_Updated"

    const/16 v12, 0xe

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "Old_Date"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v7, v12, v13

    const/4 v13, 0x2

    const-string v14, "Old_Version"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aput-object v9, v12, v13

    const/4 v13, 0x4

    const-string v14, "Old_Packages"

    aput-object v14, v12, v13

    const/4 v13, 0x5

    aput-object v8, v12, v13

    const/4 v13, 0x6

    const-string v14, "New_Version"

    aput-object v14, v12, v13

    const/4 v13, 0x7

    aput-object v10, v12, v13

    const/16 v13, 0x8

    const-string v14, "New_Packages"

    aput-object v14, v12, v13

    const/16 v13, 0x9

    aput-object v6, v12, v13

    const/16 v13, 0xa

    const-string v14, "New_Date"

    aput-object v14, v12, v13

    const/16 v13, 0xb

    aput-object v0, v12, v13

    const/16 v13, 0xc

    const-string v14, "Update_Count"

    aput-object v14, v12, v13

    const/16 v13, 0xd

    .line 745
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-direct {v2, v11, v12}, Lcom/navdy/hud/app/analytics/Event;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 746
    .local v2, "event":Lcom/navdy/hud/app/analytics/Event;
    sget-object v11, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Recording offline maps change:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 747
    const/4 v11, 0x0

    invoke-static {v2, v11}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V

    .line 748
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "previousOfflineMapsDate"

    .line 749
    invoke-interface {v11, v12, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "previousOfflineMapsPackages"

    .line 750
    invoke-interface {v11, v12, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "previousOfflineMapsVersion"

    .line 751
    invoke-interface {v11, v12, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "offlineMapsUpdateCount"

    .line 752
    invoke-interface {v11, v12, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 753
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 757
    .end local v0    # "buildDate":Ljava/lang/String;
    .end local v1    # "count":I
    .end local v2    # "event":Lcom/navdy/hud/app/analytics/Event;
    .end local v4    # "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .end local v5    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "packages":Ljava/lang/String;
    .end local v10    # "version":Ljava/lang/String;
    :cond_1
    return-void

    .line 727
    .restart local v4    # "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .restart local v5    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const-string v6, ""

    goto/16 :goto_0

    .line 730
    .restart local v0    # "buildDate":Ljava/lang/String;
    .restart local v6    # "packages":Ljava/lang/String;
    .restart local v10    # "version":Ljava/lang/String;
    :cond_3
    const-string v10, ""

    goto/16 :goto_1

    .line 731
    :cond_4
    const-string v0, ""

    goto/16 :goto_2
.end method

.method public static recordOptionSelection(Ljava/lang/String;)V
    .locals 4
    .param p0, "option"    # Ljava/lang/String;

    .prologue
    .line 768
    const-string v0, "Option_Selected"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 769
    return-void
.end method

.method public static recordPhoneDisconnect(ZLjava/lang/String;)V
    .locals 11
    .param p0, "reconnected"    # Z
    .param p1, "elapsedTime"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 772
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v3

    .line 773
    .local v3, "speedMgr":Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v7

    if-lez v7, :cond_2

    move v2, v5

    .line 774
    .local v2, "moving":Z
    :goto_0
    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    if-eqz v7, :cond_1

    .line 775
    const/4 v0, 0x0

    .line 776
    .local v0, "make":Ljava/lang/String;
    const/4 v1, 0x0

    .line 777
    .local v1, "model":Ljava/lang/String;
    const/4 v4, 0x0

    .line 779
    .local v4, "year":Ljava/lang/String;
    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v7, :cond_0

    .line 780
    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v0

    .line 781
    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v1

    .line 782
    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v4

    .line 785
    :cond_0
    const-string v8, "Phone_Disconnected"

    const/16 v7, 0x1c

    new-array v9, v7, [Ljava/lang/String;

    const-string v7, "Status"

    aput-object v7, v9, v6

    if-eqz p0, :cond_3

    const-string v7, "reconnected"

    :goto_1
    aput-object v7, v9, v5

    const/4 v5, 0x2

    const-string v7, "Reason"

    aput-object v7, v9, v5

    const/4 v5, 0x3

    sget v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I

    .line 787
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v5

    const/4 v5, 0x4

    const-string v7, "Force_Reconnect"

    aput-object v7, v9, v5

    const/4 v5, 0x5

    sget-boolean v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->disconnectDueToForceReconnect:Z

    .line 788
    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v5

    const/4 v5, 0x6

    const-string v7, "Force_Reconnect_Reason"

    aput-object v7, v9, v5

    const/4 v5, 0x7

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;

    aput-object v7, v9, v5

    const/16 v5, 0x8

    const-string v7, "While_Moving"

    aput-object v7, v9, v5

    const/16 v5, 0x9

    .line 790
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v5

    const/16 v5, 0xa

    const-string v7, "Phone_Type"

    aput-object v7, v9, v5

    const/16 v5, 0xb

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v7, v7, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 791
    invoke-virtual {v7}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v5

    const/16 v5, 0xc

    const-string v7, "Phone_OS"

    aput-object v7, v9, v5

    const/16 v5, 0xd

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v7, v7, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    aput-object v7, v9, v5

    const/16 v5, 0xe

    const-string v7, "Phone_Make"

    aput-object v7, v9, v5

    const/16 v5, 0xf

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v7, v7, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    aput-object v7, v9, v5

    const/16 v5, 0x10

    const-string v7, "Phone_Model"

    aput-object v7, v9, v5

    const/16 v5, 0x11

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v7, v7, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    aput-object v7, v9, v5

    const/16 v5, 0x12

    const-string v7, "Elapsed_Time"

    aput-object v7, v9, v5

    const/16 v5, 0x13

    aput-object p1, v9, v5

    const/16 v5, 0x14

    const-string v7, "Car_Make"

    aput-object v7, v9, v5

    const/16 v5, 0x15

    aput-object v0, v9, v5

    const/16 v5, 0x16

    const-string v7, "Car_Model"

    aput-object v7, v9, v5

    const/16 v5, 0x17

    aput-object v1, v9, v5

    const/16 v5, 0x18

    const-string v7, "Car_Year"

    aput-object v7, v9, v5

    const/16 v5, 0x19

    aput-object v4, v9, v5

    const/16 v5, 0x1a

    const-string v7, "VIN"

    aput-object v7, v9, v5

    const/16 v5, 0x1b

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    aput-object v7, v9, v5

    .line 785
    invoke-static {v8, v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 801
    sput-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    .line 802
    sput-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 803
    sput-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;

    .line 804
    sput v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I

    .line 805
    const-string v5, ""

    sput-object v5, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;

    .line 806
    sput-boolean v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->disconnectDueToForceReconnect:Z

    .line 808
    .end local v0    # "make":Ljava/lang/String;
    .end local v1    # "model":Ljava/lang/String;
    .end local v4    # "year":Ljava/lang/String;
    :cond_1
    return-void

    .end local v2    # "moving":Z
    :cond_2
    move v2, v6

    .line 773
    goto/16 :goto_0

    .line 785
    .restart local v0    # "make":Ljava/lang/String;
    .restart local v1    # "model":Ljava/lang/String;
    .restart local v2    # "moving":Z
    .restart local v4    # "year":Ljava/lang/String;
    :cond_3
    const-string v7, "failed"

    goto/16 :goto_1
.end method

.method private static recordPreference(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;
    .locals 13
    .param p0, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v3

    .line 816
    .local v3, "inputPreferences":Lcom/navdy/service/library/events/preferences/InputPreferences;
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v5

    .line 817
    .local v5, "notificationPreferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v4

    .line 819
    .local v4, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v8

    sget-object v9, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-ne v8, v9, :cond_0

    const-string v7, "Imperial"

    .line 822
    .local v7, "units":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDisplayFormat()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    move-result-object v8

    sget-object v9, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    if-ne v8, v9, :cond_1

    const-string v0, "Compact"

    .line 825
    .local v0, "displayFormat":Ljava/lang/String;
    :goto_1
    const-string v1, "Unknown"

    .line 826
    .local v1, "featureMode":Ljava/lang/String;
    sget-object v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getFeatureMode()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 839
    :goto_2
    iget-object v8, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_2

    .line 840
    const-string v2, "Disabled"

    .line 849
    .local v2, "glances":Ljava/lang/String;
    :goto_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getObdScanSetting()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v6

    .line 851
    .local v6, "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    new-instance v9, Lcom/navdy/hud/app/analytics/Event;

    const-string v10, "Driver_Preferences"

    const/16 v8, 0x14

    new-array v11, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v12, "Locale"

    aput-object v12, v11, v8

    const/4 v8, 0x1

    .line 852
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocale()Ljava/util/Locale;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v8, 0x2

    const-string v12, "Units"

    aput-object v12, v11, v8

    const/4 v8, 0x3

    aput-object v7, v11, v8

    const/4 v8, 0x4

    const-string v12, "Auto_On"

    aput-object v12, v11, v8

    const/4 v8, 0x5

    .line 854
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->isAutoOnEnabled()Z

    move-result v12

    invoke-static {v12}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v8, 0x6

    const-string v12, "Display_Format"

    aput-object v12, v11, v8

    const/4 v8, 0x7

    aput-object v0, v11, v8

    const/16 v8, 0x8

    const-string v12, "Feature_Mode"

    aput-object v12, v11, v8

    const/16 v8, 0x9

    aput-object v1, v11, v8

    const/16 v8, 0xa

    const-string v12, "Gestures"

    aput-object v12, v11, v8

    const/16 v8, 0xb

    iget-object v12, v3, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    .line 857
    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-static {v12}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    const/16 v8, 0xc

    const-string v12, "Glances"

    aput-object v12, v11, v8

    const/16 v8, 0xd

    aput-object v2, v11, v8

    const/16 v8, 0xe

    const-string v12, "OBD"

    aput-object v12, v11, v8

    const/16 v12, 0xf

    if-eqz v6, :cond_5

    .line 859
    invoke-virtual {v6}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->name()Ljava/lang/String;

    move-result-object v8

    :goto_4
    aput-object v8, v11, v12

    const/16 v8, 0x10

    const-string v12, "Limit_Bandwidth"

    aput-object v12, v11, v8

    const/16 v8, 0x11

    .line 860
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->isLimitBandwidthModeOn()Z

    move-result v12

    invoke-static {v12}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    const/16 v8, 0x12

    const-string v12, "Manual_Zoom"

    aput-object v12, v11, v8

    const/16 v8, 0x13

    iget-object v12, v4, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    .line 861
    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-static {v12}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->isEnabled(Z)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-direct {v9, v10, v11}, Lcom/navdy/hud/app/analytics/Event;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v9

    .line 819
    .end local v0    # "displayFormat":Ljava/lang/String;
    .end local v1    # "featureMode":Ljava/lang/String;
    .end local v2    # "glances":Ljava/lang/String;
    .end local v6    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .end local v7    # "units":Ljava/lang/String;
    :cond_0
    const-string v7, "Metric"

    goto/16 :goto_0

    .line 822
    .restart local v7    # "units":Ljava/lang/String;
    :cond_1
    const-string v0, "Normal"

    goto/16 :goto_1

    .line 828
    .restart local v0    # "displayFormat":Ljava/lang/String;
    .restart local v1    # "featureMode":Ljava/lang/String;
    :pswitch_0
    const-string v1, "Beta"

    .line 829
    goto/16 :goto_2

    .line 831
    :pswitch_1
    const-string v1, "Release"

    .line 832
    goto/16 :goto_2

    .line 834
    :pswitch_2
    const-string v1, "Experimental"

    goto/16 :goto_2

    .line 841
    :cond_2
    iget-object v8, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_3

    .line 842
    const-string v2, "Read_Aloud"

    .restart local v2    # "glances":Ljava/lang/String;
    goto/16 :goto_3

    .line 843
    .end local v2    # "glances":Ljava/lang/String;
    :cond_3
    iget-object v8, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 844
    const-string v2, "Show_Content"

    .restart local v2    # "glances":Ljava/lang/String;
    goto/16 :goto_3

    .line 846
    .end local v2    # "glances":Ljava/lang/String;
    :cond_4
    const-string v2, "Read_and_Show"

    .restart local v2    # "glances":Ljava/lang/String;
    goto/16 :goto_3

    .line 859
    .restart local v6    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :cond_5
    const-string v8, "null"

    goto :goto_4

    .line 826
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static recordPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 1
    .param p0, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 903
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    if-eqz v0, :cond_0

    .line 904
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->enqueue(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 906
    :cond_0
    return-void
.end method

.method public static recordRouteSelectionCancelled()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1983
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v2, :cond_0

    .line 1984
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "nearby search route calculation cancelled"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1985
    const-string v2, "Nearby_Search_Trip_Cancelled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1986
    sput-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->nearbySearchPlaceType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 1989
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-nez v2, :cond_2

    .line 1990
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    if-eqz v1, :cond_1

    .line 1992
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "route calculation cancelled"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1993
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_route_calculation:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->cancel(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;)V

    .line 2021
    .local v0, "reason":Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    :cond_1
    :goto_0
    return-void

    .line 1997
    .end local v0    # "reason":Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    :cond_2
    const/4 v0, 0x0

    .line 1998
    .restart local v0    # "reason":Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_SEARCHING:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-ne v2, v3, :cond_4

    .line 1999
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .line 2004
    :cond_3
    :goto_1
    if-nez v0, :cond_5

    .line 2005
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected progress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in recordRouteSelectionCancelled()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 2006
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    goto :goto_0

    .line 2000
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_LIST_DISPLAYED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-ne v2, v3, :cond_3

    .line 2001
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->cancel_list:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    goto :goto_1

    .line 2009
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "route selection cancelled, reason = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2011
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;

    sget v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    add-int/lit8 v4, v2, -0x1

    sget-boolean v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    if-eqz v2, :cond_6

    const-string v5, "bluetooth"

    :goto_2
    sget-object v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    sget-boolean v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;

    move-object v2, v1

    move-object v9, v1

    invoke-static/range {v0 .. v10}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendVoiceSearchEvent(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v5, "phone"

    goto :goto_2
.end method

.method public static recordScreen(Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 12
    .param p0, "screen"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1580
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v3, v4, :cond_1

    .line 1581
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sput-wide v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->welcomeScreenTime:J

    .line 1596
    :cond_0
    :goto_0
    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    if-nez v3, :cond_3

    .line 1604
    :goto_1
    return-void

    .line 1582
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v3, v4, :cond_0

    .line 1583
    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->phonePaired:Z

    if-nez v3, :cond_0

    sget-wide v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->welcomeScreenTime:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_0

    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1585
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1586
    .local v0, "curTime":J
    const-string v4, "Attempt_To_Connect"

    const/4 v3, 0x4

    new-array v5, v3, [Ljava/lang/String;

    const-string v3, "Was_New_Device"

    aput-object v3, v5, v8

    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->newDevicePaired:Z

    if-eqz v3, :cond_2

    const-string v3, "true"

    :goto_2
    aput-object v3, v5, v9

    const/4 v3, 0x2

    const-string v6, "Time_To_Connect"

    aput-object v6, v5, v3

    const/4 v3, 0x3

    sget-wide v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->welcomeScreenTime:J

    sub-long v6, v0, v6

    .line 1588
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 1586
    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1590
    sput-wide v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->welcomeScreenTime:J

    .line 1591
    sput-boolean v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->newDevicePaired:Z

    .line 1592
    sput-boolean v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->phonePaired:Z

    goto :goto_0

    .line 1586
    :cond_2
    const-string v3, "false"

    goto :goto_2

    .line 1599
    .end local v0    # "curTime":J
    :cond_3
    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->screenName(Lcom/navdy/hud/app/screen/BaseScreen;)Ljava/lang/String;

    move-result-object v2

    .line 1600
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/localytics/android/Localytics;->tagScreen(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static recordShutdown(Lcom/navdy/hud/app/event/Shutdown$Reason;Z)V
    .locals 13
    .param p0, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;
    .param p1, "isQuietMode"    # Z

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2146
    if-eqz p1, :cond_0

    const-string v3, "quiet"

    .line 2147
    .local v3, "type":Ljava/lang/String;
    :goto_0
    sget-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v0

    .line 2148
    .local v0, "batteryLevel":D
    sget-boolean v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    if-eqz v4, :cond_1

    const-string v2, "Sleep_Time"

    .line 2150
    .local v2, "timeAttr":Ljava/lang/String;
    :goto_1
    const-string v4, "Shutdown"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "Type"

    aput-object v6, v5, v11

    aput-object v3, v5, v12

    const/4 v6, 0x2

    const-string v7, "Reason"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/navdy/hud/app/event/Shutdown$Reason;->attr:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "Battery_Level"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%6.1f"

    new-array v9, v12, [Ljava/lang/Object;

    .line 2153
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    aput-object v2, v5, v6

    const/4 v6, 0x7

    .line 2154
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 2150
    invoke-static {v4, v11, v12, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 2157
    return-void

    .line 2146
    .end local v0    # "batteryLevel":D
    .end local v2    # "timeAttr":Ljava/lang/String;
    .end local v3    # "type":Ljava/lang/String;
    :cond_0
    const-string v3, "full"

    goto :goto_0

    .line 2148
    .restart local v0    # "batteryLevel":D
    .restart local v3    # "type":Ljava/lang/String;
    :cond_1
    const-string v2, "Uptime"

    goto :goto_1
.end method

.method public static recordSmsSent(ZLjava/lang/String;Z)V
    .locals 4
    .param p0, "success"    # Z
    .param p1, "platform"    # Ljava/lang/String;
    .param p2, "cannedMessage"    # Z

    .prologue
    .line 2396
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sms-sent success="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " plaform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " canned="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2397
    const-string v1, "sms_sent"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Success"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    .line 2398
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "Platform"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object p1, v2, v0

    const/4 v0, 0x4

    const-string v3, "Message_Type"

    aput-object v3, v2, v0

    const/4 v3, 0x5

    if-eqz p2, :cond_0

    const-string v0, "Canned"

    :goto_0
    aput-object v0, v2, v3

    .line 2397
    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2401
    return-void

    .line 2398
    :cond_0
    const-string v0, "Custom"

    goto :goto_0
.end method

.method public static recordStartingBatteryVoltage(D)V
    .locals 6
    .param p0, "voltage"    # D

    .prologue
    .line 2127
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingBatteryVoltage:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 2128
    sput-wide p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingBatteryVoltage:D

    .line 2129
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "starting battery voltage = %6.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 2131
    :cond_0
    return-void
.end method

.method public static recordSwipedCalibration(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p0, "eventName"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 2057
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Swiped_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2059
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 2060
    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 2059
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 2062
    :cond_0
    invoke-static {p0, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2063
    return-void
.end method

.method public static recordUpdatePrompt(ZZLjava/lang/String;)V
    .locals 4
    .param p0, "accepted"    # Z
    .param p1, "isDial"    # Z
    .param p2, "toVersion"    # Ljava/lang/String;

    .prologue
    .line 2075
    if-eqz p1, :cond_0

    const-string v0, "Dial_Update_Prompt"

    .line 2076
    .local v0, "event":Ljava/lang/String;
    :goto_0
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "Accepted"

    aput-object v3, v2, v1

    const/4 v3, 0x1

    if-eqz p0, :cond_1

    const-string v1, "true"

    :goto_1
    aput-object v1, v2, v3

    const/4 v1, 0x2

    const-string v3, "Update_To_Version"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    aput-object p2, v2, v1

    invoke-static {v0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2079
    return-void

    .line 2075
    .end local v0    # "event":Ljava/lang/String;
    :cond_0
    const-string v0, "Display_Update_Prompt"

    goto :goto_0

    .line 2076
    .restart local v0    # "event":Ljava/lang/String;
    :cond_1
    const-string v1, "false"

    goto :goto_1
.end method

.method public static recordVehicleTelemetryData(JJJFFFIIFFILjava/lang/String;IIIIIFFFI)V
    .locals 8
    .param p0, "sessionDistance"    # J
    .param p2, "sessionDuration"    # J
    .param p4, "sessionRollingDuration"    # J
    .param p6, "sessionAverageSpeed"    # F
    .param p7, "sessionAverageRollingSpeed"    # F
    .param p8, "sessionMaxSpeed"    # F
    .param p9, "sessionHardBrakingCount"    # I
    .param p10, "sessionHardAccelerationCount"    # I
    .param p11, "sessionSpeedingFrequency"    # F
    .param p12, "sessionExcessiveSpeedingFrequency"    # F
    .param p13, "sessionTroubleCodeCount"    # I
    .param p14, "sessionTroubleCodes"    # Ljava/lang/String;
    .param p15, "averageMpg"    # I
    .param p16, "maxRpm"    # I
    .param p17, "eventAverageMpg"    # I
    .param p18, "eventAverageRpm"    # I
    .param p19, "eventFuelLevel"    # I
    .param p20, "eventEngineTemperature"    # F
    .param p21, "maxG"    # F
    .param p22, "maxGAngle"    # F
    .param p23, "sessionHighGCount"    # I

    .prologue
    .line 2422
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 2423
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    const-string v1, ""

    .line 2424
    .local v1, "make":Ljava/lang/String;
    const-string v2, ""

    .line 2425
    .local v2, "model":Ljava/lang/String;
    const-string v3, ""

    .line 2427
    .local v3, "year":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2428
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v1

    .line 2429
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v2

    .line 2430
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v3

    .line 2432
    :cond_0
    const-string v4, "Vehicle_Telemetry_Data"

    const/16 v5, 0x30

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Session_Distance"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 2433
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "Session_Duration"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    .line 2434
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "Session_Rolling_Duration"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    .line 2435
    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "Session_Average_Speed"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    .line 2436
    invoke-static {p6}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "Session_Average_Rolling_Speed"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    .line 2437
    invoke-static {p7}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "Session_Max_Speed"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    .line 2438
    invoke-static/range {p8 .. p8}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "Session_Hard_Braking_Count"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    .line 2439
    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "Session_Hard_Acceleration_Count"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    .line 2440
    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, "Session_Speeding_Frequency"

    aput-object v7, v5, v6

    const/16 v6, 0x11

    .line 2441
    invoke-static/range {p11 .. p11}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x12

    const-string v7, "Session_Excessive_Speeding_Frequency"

    aput-object v7, v5, v6

    const/16 v6, 0x13

    .line 2442
    invoke-static/range {p12 .. p12}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x14

    const-string v7, "Session_Trouble_Code_Count"

    aput-object v7, v5, v6

    const/16 v6, 0x15

    .line 2443
    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x16

    const-string v7, "Session_Trouble_Code_Last"

    aput-object v7, v5, v6

    const/16 v6, 0x17

    aput-object p14, v5, v6

    const/16 v6, 0x18

    const-string v7, "Session_Average_Mpg"

    aput-object v7, v5, v6

    const/16 v6, 0x19

    .line 2445
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x1a

    const-string v7, "Session_Max_RPM"

    aput-object v7, v5, v6

    const/16 v6, 0x1b

    .line 2446
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x1c

    const-string v7, "Event_Average_Mpg"

    aput-object v7, v5, v6

    const/16 v6, 0x1d

    .line 2447
    invoke-static/range {p17 .. p17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x1e

    const-string v7, "Event_Average_Rpm"

    aput-object v7, v5, v6

    const/16 v6, 0x1f

    .line 2448
    invoke-static/range {p18 .. p18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x20

    const-string v7, "Event_Fuel_Level"

    aput-object v7, v5, v6

    const/16 v6, 0x21

    .line 2449
    invoke-static/range {p19 .. p19}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x22

    const-string v7, "Event_Engine_Temperature"

    aput-object v7, v5, v6

    const/16 v6, 0x23

    .line 2450
    invoke-static/range {p20 .. p20}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x24

    const-string v7, "Event_Max_G"

    aput-object v7, v5, v6

    const/16 v6, 0x25

    .line 2451
    invoke-static/range {p21 .. p21}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x26

    const-string v7, "Event_Max_G_Angle"

    aput-object v7, v5, v6

    const/16 v6, 0x27

    .line 2452
    invoke-static/range {p22 .. p22}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x28

    const-string v7, "Session_High_G_Count"

    aput-object v7, v5, v6

    const/16 v6, 0x29

    .line 2453
    invoke-static/range {p23 .. p23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x2a

    const-string v7, "Car_Make"

    aput-object v7, v5, v6

    const/16 v6, 0x2b

    aput-object v1, v5, v6

    const/16 v6, 0x2c

    const-string v7, "Car_Model"

    aput-object v7, v5, v6

    const/16 v6, 0x2d

    aput-object v2, v5, v6

    const/16 v6, 0x2e

    const-string v7, "Car_Year"

    aput-object v7, v5, v6

    const/16 v6, 0x2f

    aput-object v3, v5, v6

    .line 2432
    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2457
    return-void
.end method

.method public static recordVoiceSearchAdditionalResultsAction(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;)V
    .locals 1
    .param p0, "action"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .prologue
    .line 1764
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1765
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    if-ne p0, v0, :cond_0

    .line 1766
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_LIST_DISPLAYED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    .line 1770
    :goto_0
    return-void

    .line 1768
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_ROUTE_SELECTED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    goto :goto_0
.end method

.method public static recordVoiceSearchEntered()V
    .locals 2

    .prologue
    .line 1753
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_LIST_DISPLAYED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-ne v0, v1, :cond_0

    .line 1754
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    .line 1755
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    .line 1757
    :cond_0
    return-void
.end method

.method public static recordVoiceSearchListItemSelection(I)V
    .locals 1
    .param p0, "selection"    # I

    .prologue
    .line 1773
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const/4 v0, -0x2

    if-ne p0, v0, :cond_1

    .line 1774
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    .line 1779
    :goto_0
    return-void

    .line 1776
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListSelection:Ljava/lang/Integer;

    .line 1777
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_ROUTE_SELECTED:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    goto :goto_0
.end method

.method public static recordVoiceSearchResult(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V
    .locals 3
    .param p0, "resp"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    .prologue
    .line 1782
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$3;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1827
    :cond_0
    :goto_0
    return-void

    .line 1784
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    .line 1785
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    if-eqz v0, :cond_1

    .line 1786
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting voice search with unexpected progress state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1788
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    .line 1790
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting voice search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1791
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_SEARCHING:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    goto :goto_0

    .line 1794
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1795
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    goto :goto_0

    .line 1800
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "voice search successful"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1801
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;->PROGRESS_DESTINATION_FOUND:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchProgress:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchProgress;

    .line 1802
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1803
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z

    .line 1805
    :cond_2
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;

    .line 1809
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1811
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchWords:Ljava/lang/String;

    .line 1813
    :cond_3
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1814
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;

    .line 1816
    :cond_4
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1817
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;

    goto :goto_0

    .line 1822
    :pswitch_4
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "voice search error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1823
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V

    .line 1824
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    sget v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendVoiceSearchErrorEvent(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;I)V

    goto/16 :goto_0

    .line 1782
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static recordVoiceSearchRetry()V
    .locals 1

    .prologue
    .line 1760
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z

    .line 1761
    return-void
.end method

.method private static recordWakeup(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V
    .locals 14
    .param p0, "reason"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 2178
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 2179
    .local v4, "sleepTimeStr":Ljava/lang/String;
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%6.1f"

    new-array v9, v13, [Ljava/lang/Object;

    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v0, v1, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2180
    .local v2, "batteryLevelStr":Ljava/lang/String;
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%6.1f"

    new-array v9, v13, [Ljava/lang/Object;

    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getMaxBatteryVoltage()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v0, v1, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2181
    .local v5, "maxBatteryStr":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v8

    .line 2182
    .local v8, "vin":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 2184
    .local v6, "batteryDrain":D
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingBatteryVoltage:D

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v10

    if-eqz v0, :cond_0

    .line 2185
    sget-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingBatteryVoltage:D

    sget-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/obd/ObdManager;->getMinBatteryVoltage()D

    move-result-wide v10

    sub-double v6, v0, v10

    .line 2187
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%6.1f"

    new-array v9, v13, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v0, v1, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2189
    .local v3, "batteryDrainStr":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 2190
    const-string v0, "Wakeup"

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const-string v9, "Reason"

    aput-object v9, v1, v12

    iget-object v9, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->attr:Ljava/lang/String;

    aput-object v9, v1, v13

    const/4 v9, 0x2

    const-string v10, "Battery_Level"

    aput-object v10, v1, v9

    const/4 v9, 0x3

    aput-object v2, v1, v9

    const/4 v9, 0x4

    const-string v10, "Battery_Drain"

    aput-object v10, v1, v9

    const/4 v9, 0x5

    aput-object v3, v1, v9

    const/4 v9, 0x6

    const-string v10, "Sleep_Time"

    aput-object v10, v1, v9

    const/4 v9, 0x7

    aput-object v4, v1, v9

    const/16 v9, 0x8

    const-string v10, "Max_Battery"

    aput-object v10, v1, v9

    const/16 v9, 0x9

    aput-object v5, v1, v9

    const/16 v9, 0xa

    const-string v10, "VIN"

    aput-object v10, v1, v9

    const/16 v9, 0xb

    aput-object v8, v1, v9

    invoke-static {v0, v12, v13, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 2208
    :goto_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startTime:J

    .line 2209
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTripManager()Lcom/navdy/hud/app/framework/trips/TripManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelled()I

    move-result v0

    sput v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingDistance:I

    .line 2210
    return-void

    .line 2198
    :cond_1
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    goto :goto_0
.end method

.method private static reportObdState()V
    .locals 16

    .prologue
    const/4 v15, 0x2

    const/4 v14, 0x1

    const/4 v9, 0x0

    .line 1452
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getEcus()Ljava/util/List;

    move-result-object v3

    .line 1453
    .local v3, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    if-nez v3, :cond_1

    move v2, v9

    .line 1454
    .local v2, "ecuCount":I
    :goto_0
    if-nez v2, :cond_0

    .line 1455
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Obd connection with no ECUs"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1457
    :cond_0
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    .line 1458
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getObdChipFirmwareVersion()Ljava/lang/String;

    move-result-object v7

    .line 1459
    .local v7, "obdChipFirmwareVersion":Ljava/lang/String;
    mul-int/lit8 v10, v2, 0x2

    add-int/lit8 v10, v10, 0x6

    new-array v0, v10, [Ljava/lang/String;

    .line 1460
    .local v0, "attrs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 1461
    .local v5, "idx":I
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "idx":I
    .local v6, "idx":I
    const-string v10, "Obd_Chip_Firmware"

    aput-object v10, v0, v5

    .line 1462
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "idx":I
    .restart local v5    # "idx":I
    if-eqz v7, :cond_2

    .end local v7    # "obdChipFirmwareVersion":Ljava/lang/String;
    :goto_1
    aput-object v7, v0, v6

    .line 1463
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "idx":I
    .restart local v6    # "idx":I
    const-string v10, "VIN"

    aput-object v10, v0, v5

    .line 1464
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "idx":I
    .restart local v5    # "idx":I
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    aput-object v10, v0, v6

    .line 1465
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "idx":I
    .restart local v6    # "idx":I
    const-string v10, "Protocol"

    aput-object v10, v0, v5

    .line 1466
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "idx":I
    .restart local v5    # "idx":I
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v10}, Lcom/navdy/hud/app/obd/ObdManager;->getProtocol()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v6

    .line 1468
    const/4 v4, 0x0

    .local v4, "i":I
    move v6, v5

    .end local v5    # "idx":I
    .restart local v6    # "idx":I
    :goto_2
    if-ge v4, v2, :cond_3

    .line 1469
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ECU;

    .line 1470
    .local v1, "e":Lcom/navdy/obd/ECU;
    iget-object v10, v1, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v10}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v8

    .line 1471
    .local v8, "pList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "idx":I
    .restart local v5    # "idx":I
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "%s%d"

    new-array v12, v15, [Ljava/lang/Object;

    const-string v13, "ECU"

    aput-object v13, v12, v9

    add-int/lit8 v13, v4, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v14

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v6

    .line 1472
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "idx":I
    .restart local v6    # "idx":I
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Address: 0x%x, Pids: %s"

    new-array v12, v15, [Ljava/lang/Object;

    iget v13, v1, Lcom/navdy/obd/ECU;->address:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v9

    invoke-static {v8}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->pidListToHexString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v14

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v5

    .line 1468
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1453
    .end local v0    # "attrs":[Ljava/lang/String;
    .end local v1    # "e":Lcom/navdy/obd/ECU;
    .end local v2    # "ecuCount":I
    .end local v4    # "i":I
    .end local v6    # "idx":I
    .end local v8    # "pList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    goto/16 :goto_0

    .line 1462
    .restart local v0    # "attrs":[Ljava/lang/String;
    .restart local v2    # "ecuCount":I
    .restart local v5    # "idx":I
    .restart local v7    # "obdChipFirmwareVersion":Ljava/lang/String;
    :cond_2
    const-string v7, ""

    goto :goto_1

    .line 1475
    .end local v5    # "idx":I
    .end local v7    # "obdChipFirmwareVersion":Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v6    # "idx":I
    :cond_3
    const-string v10, "OBD_Connected"

    invoke-static {v10, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1476
    sget-object v10, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    if-eqz v10, :cond_4

    .line 1477
    const-string v10, "Wakeup"

    const/16 v11, 0xc

    new-array v11, v11, [Ljava/lang/String;

    const-string v12, "Reason"

    aput-object v12, v11, v9

    sget-object v12, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    iget-object v12, v12, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->reason:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    iget-object v12, v12, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->attr:Ljava/lang/String;

    aput-object v12, v11, v14

    const-string v12, "Battery_Level"

    aput-object v12, v11, v15

    const/4 v12, 0x3

    sget-object v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    iget-object v13, v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->batteryLevel:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x4

    const-string v13, "Battery_Drain"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    sget-object v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    iget-object v13, v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->batteryDrain:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x6

    const-string v13, "Sleep_Time"

    aput-object v13, v11, v12

    const/4 v12, 0x7

    sget-object v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    iget-object v13, v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->sleepTime:Ljava/lang/String;

    aput-object v13, v11, v12

    const/16 v12, 0x8

    const-string v13, "Max_Battery"

    aput-object v13, v11, v12

    const/16 v12, 0x9

    sget-object v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    iget-object v13, v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;->maxBattery:Ljava/lang/String;

    aput-object v13, v11, v12

    const/16 v12, 0xa

    const-string v13, "VIN"

    aput-object v13, v11, v12

    const/16 v12, 0xb

    sget-object v13, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v9, v14, v11}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 1484
    const/4 v9, 0x0

    sput-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredWakeup:Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredWakeup;

    .line 1486
    :cond_4
    return-void
.end method

.method private static screenName(Lcom/navdy/hud/app/screen/BaseScreen;)Ljava/lang/String;
    .locals 6
    .param p0, "screen"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    .line 1559
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    .line 1560
    .local v3, "scr":Lcom/navdy/service/library/events/ui/Screen;
    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/Screen;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1561
    .local v1, "name":Ljava/lang/String;
    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v3, v4, :cond_0

    .line 1562
    check-cast p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    .end local p0    # "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    .line 1563
    .local v0, "dm":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1564
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v2, "_ROUTE"

    .line 1565
    .local v2, "route":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1568
    .end local v0    # "dm":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    .end local v2    # "route":Ljava/lang/String;
    :cond_0
    return-object v1

    .line 1564
    .restart local v0    # "dm":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method private static declared-synchronized sendDeferredLocalyticsEvents()V
    .locals 5

    .prologue
    .line 2247
    const-class v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsInitialized:Z

    if-nez v1, :cond_1

    .line 2248
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "sendDeferredLocalyticsEvents() called before Localytics was initialized"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2259
    .local v0, "de":Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 2251
    .end local v0    # "de":Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2254
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sending "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deferred localytics events"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2255
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v0    # "de":Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "de":Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;
    check-cast v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;

    .line 2256
    .restart local v0    # "de":Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;
    iget-object v3, v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;->tag:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$DeferredLocalyticsEvent;->args:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2247
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 2258
    :cond_2
    const/4 v1, 0x0

    :try_start_2
    sput-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->deferredEvents:Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static sendDistance()V
    .locals 10

    .prologue
    const/16 v5, 0xa

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2106
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTripManager()Lcom/navdy/hud/app/framework/trips/TripManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelled()I

    move-result v3

    sget v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startingDistance:I

    sub-int v2, v3, v4

    .line 2107
    .local v2, "totalDistance":I
    if-le v2, v5, :cond_0

    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z

    if-nez v3, :cond_0

    .line 2108
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->uptime()J

    move-result-wide v0

    .line 2111
    .local v0, "duration":J
    const-string v4, "Distance_Travelled"

    new-array v5, v5, [Ljava/lang/String;

    const-string v3, "Distance"

    aput-object v3, v5, v8

    int-to-long v6, v2

    .line 2113
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v9

    const/4 v3, 0x2

    const-string v6, "Duration"

    aput-object v6, v5, v3

    const/4 v3, 0x3

    .line 2114
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x4

    const-string v6, "Navigating"

    aput-object v6, v5, v3

    const/4 v6, 0x5

    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->destinationSet:Z

    if-eqz v3, :cond_1

    const-string v3, "true"

    :goto_0
    aput-object v3, v5, v6

    const/4 v3, 0x6

    const-string v6, "Connection"

    aput-object v6, v5, v3

    const/4 v6, 0x7

    sget-boolean v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->phonePaired:Z

    if-eqz v3, :cond_2

    const-string v3, "true"

    :goto_1
    aput-object v3, v5, v6

    const/16 v3, 0x8

    const-string v6, "Vin"

    aput-object v6, v5, v3

    const/16 v3, 0x9

    sget-object v6, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastVIN:Ljava/lang/String;

    aput-object v6, v5, v3

    .line 2111
    invoke-static {v4, v8, v9, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;ZZ[Ljava/lang/String;)V

    .line 2120
    .end local v0    # "duration":J
    :cond_0
    return-void

    .line 2114
    .restart local v0    # "duration":J
    :cond_1
    const-string v3, "false"

    goto :goto_0

    :cond_2
    const-string v3, "false"

    goto :goto_1
.end method

.method private static sendVoiceSearchErrorEvent(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;I)V
    .locals 4
    .param p0, "error"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .param p1, "retryCount"    # I

    .prologue
    .line 1866
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getVoiceSearchEvent()Ljava/lang/String;

    move-result-object v0

    .line 1867
    .local v0, "event":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1868
    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "cannot determine phone_type for voice search"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1875
    :goto_0
    return-void

    .line 1871
    :cond_0
    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "Response"

    aput-object v3, v2, v1

    const/4 v3, 0x1

    if-nez p0, :cond_1

    const-string v1, ""

    .line 1872
    :goto_1
    aput-object v1, v2, v3

    const/4 v1, 0x2

    const-string v3, "Retry_Count"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    .line 1873
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string v3, "Client_Version"

    aput-object v3, v2, v1

    const/4 v1, 0x5

    .line 1874
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getClientVersion()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1871
    invoke-static {v0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 1872
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private static sendVoiceSearchEvent(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 10
    .param p0, "reason"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "words"    # Ljava/lang/String;
    .param p3, "confidence"    # Ljava/lang/Integer;
    .param p4, "retryCount"    # I
    .param p5, "microphoneUsed"    # Ljava/lang/String;
    .param p7, "additionalResultsAction"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    .param p8, "explicitRetry"    # Z
    .param p9, "listItemSelection"    # Ljava/lang/Integer;
    .param p10, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;",
            "Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;",
            "Z",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1888
    .local p6, "searchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getVoiceSearchEvent()Ljava/lang/String;

    move-result-object v5

    .line 1890
    .local v5, "event":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 1891
    sget-object v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "cannot determine phone_type for voice search"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1944
    :goto_0
    return-void

    .line 1894
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1895
    .local v1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v8, "Response"

    const-string v9, "Success"

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896
    const-string v8, "Client_Version"

    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getClientVersion()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1897
    if-eqz p3, :cond_1

    .line 1898
    const-string v8, "Confidence_Level"

    invoke-virtual {p3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1900
    :cond_1
    if-eqz p2, :cond_2

    .line 1901
    const-string v8, "Number_Of_Words"

    invoke-static {p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->countWords(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1903
    :cond_2
    sget-object v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->end_trip:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    if-eq p0, v8, :cond_3

    sget-object v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    if-eq p0, v8, :cond_3

    sget-object v8, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->new_non_voice_search:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    if-ne p0, v8, :cond_b

    :cond_3
    const/4 v2, 0x1

    .line 1906
    .local v2, "cancelledQuickly":Z
    :goto_1
    const-string v9, "Navigation_Initiated"

    if-eqz p0, :cond_4

    if-eqz v2, :cond_c

    :cond_4
    const-string v8, "true"

    :goto_2
    invoke-interface {v1, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908
    const-string v9, "Navigation_Cancelled_Quickly"

    if-eqz v2, :cond_d

    const-string v8, "true"

    :goto_3
    invoke-interface {v1, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909
    const-string v8, "Retry_Count"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910
    const-string v8, "Microphone_Used"

    invoke-interface {v1, v8, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1911
    if-nez p6, :cond_e

    const/4 v7, 0x0

    .line 1912
    .local v7, "numResults":I
    :goto_4
    const-string v8, "Num_Total_Results"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1913
    if-eqz p7, :cond_5

    .line 1914
    const-string v8, "Additional_Results_Glance"

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->tag:Ljava/lang/String;

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916
    :cond_5
    const-string v9, "Explicit_Retry"

    if-eqz p8, :cond_f

    const-string v8, "true"

    :goto_5
    invoke-interface {v1, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917
    if-eqz p0, :cond_6

    .line 1918
    const-string v8, "Cancel_Type"

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920
    :cond_6
    if-eqz p9, :cond_7

    .line 1921
    const-string v8, "Additional_Results_List_Item_Selected"

    invoke-virtual/range {p9 .. p9}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1923
    :cond_7
    const/4 v6, 0x0

    .line 1924
    .local v6, "isSuggestion":Z
    const-string v4, "ADDRESS"

    .line 1925
    .local v4, "destType":Ljava/lang/String;
    if-eqz p1, :cond_9

    .line 1926
    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 1927
    .local v3, "dest":Lcom/navdy/service/library/events/destination/Destination;
    if-eqz v3, :cond_10

    iget-object v8, v3, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eqz v8, :cond_10

    iget-object v8, v3, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sget-object v9, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_NONE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eq v8, v9, :cond_10

    const/4 v6, 0x1

    .line 1930
    :goto_6
    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-eqz v8, :cond_11

    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    sget-object v9, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-eq v8, v9, :cond_11

    .line 1932
    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1936
    :cond_8
    :goto_7
    const-string v8, "Destination_Type"

    invoke-interface {v1, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938
    .end local v3    # "dest":Lcom/navdy/service/library/events/destination/Destination;
    :cond_9
    const-string v9, "Suggestion"

    if-eqz v6, :cond_12

    const-string v8, "true"

    :goto_8
    invoke-interface {v1, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939
    if-eqz p10, :cond_a

    .line 1940
    const-string v8, "Prefix"

    move-object/from16 v0, p10

    invoke-interface {v1, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1942
    :cond_a
    new-instance v8, Lcom/navdy/hud/app/analytics/Event;

    invoke-direct {v8, v5, v1}, Lcom/navdy/hud/app/analytics/Event;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V

    goto/16 :goto_0

    .line 1903
    .end local v2    # "cancelledQuickly":Z
    .end local v4    # "destType":Ljava/lang/String;
    .end local v6    # "isSuggestion":Z
    .end local v7    # "numResults":I
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1906
    .restart local v2    # "cancelledQuickly":Z
    :cond_c
    const-string v8, "false"

    goto/16 :goto_2

    .line 1908
    :cond_d
    const-string v8, "false"

    goto/16 :goto_3

    .line 1911
    :cond_e
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v7

    goto/16 :goto_4

    .line 1916
    .restart local v7    # "numResults":I
    :cond_f
    const-string v8, "false"

    goto :goto_5

    .line 1927
    .restart local v3    # "dest":Lcom/navdy/service/library/events/destination/Destination;
    .restart local v4    # "destType":Ljava/lang/String;
    .restart local v6    # "isSuggestion":Z
    :cond_10
    const/4 v6, 0x0

    goto :goto_6

    .line 1933
    :cond_11
    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    if-eqz v8, :cond_8

    .line 1934
    const-string v4, "PLACE"

    goto :goto_7

    .line 1938
    .end local v3    # "dest":Lcom/navdy/service/library/events/destination/Destination;
    :cond_12
    const-string v8, "false"

    goto :goto_8
.end method

.method public static setGlanceNavigationSource(Ljava/lang/String;)V
    .locals 0
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 519
    sput-object p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;

    .line 520
    return-void
.end method

.method private static setupCustomDimensions()V
    .locals 2

    .prologue
    .line 1175
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getHwVersion()Ljava/lang/String;

    move-result-object v0

    .line 1176
    .local v0, "hwVersion":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/localytics/android/Localytics;->setCustomDimension(ILjava/lang/String;)V

    .line 1177
    return-void
.end method

.method private static setupProfile()V
    .locals 13

    .prologue
    .line 1250
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    .line 1251
    .local v1, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    const/4 v2, 0x0

    .line 1252
    .local v2, "make":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1253
    .local v3, "model":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1255
    .local v8, "year":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v9

    if-nez v9, :cond_0

    .line 1256
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/localytics/android/Localytics;->setCustomerFullName(Ljava/lang/String;)V

    .line 1257
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/localytics/android/Localytics;->setCustomerEmail(Ljava/lang/String;)V

    .line 1258
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v2

    .line 1259
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v3

    .line 1260
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v8

    .line 1261
    const-string v9, "Hud_Serial_Number"

    sget-object v10, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    const-string v9, "Car_Make"

    invoke-static {v9, v2}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    const-string v9, "Car_Model"

    invoke-static {v9, v3}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    const-string v9, "Car_Year"

    invoke-static {v9, v8}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1265
    const-string v9, "Hud_Version_Number"

    const-string v10, "ro.build.description"

    const-string v11, ""

    .line 1266
    invoke-static {v10, v11}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1265
    invoke-static {v9, v10}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    const-string v9, "Build_Type"

    sget-object v10, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/localytics/android/Localytics;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    :cond_0
    sget-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 1273
    .local v0, "devInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_1

    .line 1274
    iget-object v9, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1275
    .local v6, "remPlatform":Ljava/lang/String;
    iget-object v5, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    .line 1276
    .local v5, "remDeviceName":Ljava/lang/String;
    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    .line 1277
    .local v4, "remClientVersion":Ljava/lang/String;
    iget-object v9, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    sput-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    .line 1281
    :goto_0
    sget-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v7

    .line 1282
    .local v7, "vin":Ljava/lang/String;
    const-string v9, "Mobile_App_Connect"

    const/16 v10, 0x12

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "Device_Id"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    sget-object v12, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string v12, "Remote_Device_Id"

    aput-object v12, v10, v11

    const/4 v11, 0x3

    sget-object v12, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x4

    const-string v12, "Remote_Device_Name"

    aput-object v12, v10, v11

    const/4 v11, 0x5

    aput-object v5, v10, v11

    const/4 v11, 0x6

    const-string v12, "Remote_Platform"

    aput-object v12, v10, v11

    const/4 v11, 0x7

    aput-object v6, v10, v11

    const/16 v11, 0x8

    const-string v12, "Remote_Client_Version"

    aput-object v12, v10, v11

    const/16 v11, 0x9

    aput-object v4, v10, v11

    const/16 v11, 0xa

    const-string v12, "Car_Make"

    aput-object v12, v10, v11

    const/16 v11, 0xb

    aput-object v2, v10, v11

    const/16 v11, 0xc

    const-string v12, "Car_Model"

    aput-object v12, v10, v11

    const/16 v11, 0xd

    aput-object v3, v10, v11

    const/16 v11, 0xe

    const-string v12, "Car_Year"

    aput-object v12, v10, v11

    const/16 v11, 0xf

    aput-object v8, v10, v11

    const/16 v11, 0x10

    const-string v12, "VIN"

    aput-object v12, v10, v11

    const/16 v11, 0x11

    aput-object v7, v10, v11

    invoke-static {v9, v10}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1295
    sget-object v9, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setupProfile: DeviceId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", RemoteDeviceId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", remotePlatform = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", RemoteDeviceName = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", RemoteClientVersion = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vin = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1298
    return-void

    .line 1279
    .end local v4    # "remClientVersion":Ljava/lang/String;
    .end local v5    # "remDeviceName":Ljava/lang/String;
    .end local v6    # "remPlatform":Ljava/lang/String;
    .end local v7    # "vin":Ljava/lang/String;
    :cond_1
    const-string v4, "unknown"

    sput-object v4, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;

    .restart local v4    # "remClientVersion":Ljava/lang/String;
    move-object v5, v4

    .restart local v5    # "remDeviceName":Ljava/lang/String;
    move-object v6, v4

    .restart local v6    # "remPlatform":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method static submitNavigationQualityReport(Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;)V
    .locals 6
    .param p0, "report"    # Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;

    .prologue
    .line 2341
    const-string v0, "Route_Completed"

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Expected_Duration"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    .line 2342
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Actual_Duration"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDuration:J

    .line 2343
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "Expected_Distance"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    .line 2344
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "Actual_Distance"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDistance:J

    .line 2345
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Recalculations"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget v3, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->nRecalculations:I

    .line 2346
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "Duration_PCT_Diff"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    .line 2347
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->getDurationVariance()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "Distance_PCT_Diff"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    .line 2348
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->getDistanceVariance()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "Traffic_Level"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    .line 2349
    invoke-virtual {v3}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2341
    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2350
    return-void
.end method

.method private static uptime()J
    .locals 4

    .prologue
    .line 2102
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sget-wide v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->startTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
