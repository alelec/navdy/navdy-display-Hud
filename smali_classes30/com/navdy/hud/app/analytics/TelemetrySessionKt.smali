.class public final Lcom/navdy/hud/app/analytics/TelemetrySessionKt;
.super Ljava/lang/Object;
.source "TelemetrySession.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0002\u0008\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0004\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0006\u001a\u0012\u0010\u0007\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0006\u00a8\u0006\t"
    }
    d2 = {
        "KMPHToMetersPerSecond",
        "",
        "MPHToMetersPerSecond",
        "MetersPerSecondToKMPH",
        "MetersPerSecondToMPH",
        "milliSecondsToSeconds",
        "",
        "timeSince",
        "time",
        "app_hudRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method public static final KMPHToMetersPerSecond(F)F
    .locals 1
    .param p0, "$receiver"    # F

    .prologue
    .line 478
    const v0, 0x3e8e38eb    # 0.277778f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static final MPHToMetersPerSecond(F)F
    .locals 1
    .param p0, "$receiver"    # F

    .prologue
    .line 473
    const v0, 0x3ee4e26d    # 0.44704f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static final MetersPerSecondToKMPH(F)F
    .locals 1
    .param p0, "$receiver"    # F

    .prologue
    .line 487
    const v0, 0x40666666    # 3.6f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static final MetersPerSecondToMPH(F)F
    .locals 1
    .param p0, "$receiver"    # F

    .prologue
    .line 482
    const v0, 0x400f2a06

    mul-float/2addr v0, p0

    return v0
.end method

.method public static final milliSecondsToSeconds(J)J
    .locals 2
    .param p0, "$receiver"    # J

    .prologue
    .line 491
    const/16 v0, 0x3e8

    int-to-long v0, v0

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static final timeSince(JJ)J
    .locals 2
    .param p0, "$receiver"    # J
    .param p2, "time"    # J

    .prologue
    .line 495
    sub-long v0, p0, p2

    return-wide v0
.end method
