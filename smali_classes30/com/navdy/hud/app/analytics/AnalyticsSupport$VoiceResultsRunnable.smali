.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VoiceResultsRunnable"
.end annotation


# instance fields
.field additionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

.field private done:Z

.field explicitRetry:Z

.field listItemSelected:Ljava/lang/Integer;

.field microphoneUsed:Ljava/lang/String;

.field prefix:Ljava/lang/String;

.field private request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field private retryCount:I

.field private searchConfidence:Ljava/lang/Integer;

.field searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private searchWords:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 1
    .param p1, "req"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    const/4 v0, 0x0

    .line 1683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1680
    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->listItemSelected:Ljava/lang/Integer;

    .line 1681
    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->prefix:Ljava/lang/String;

    .line 1684
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z

    .line 1685
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1686
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchWords:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3500()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchWords:Ljava/lang/String;

    .line 1687
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchConfidence:Ljava/lang/Integer;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3600()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchConfidence:Ljava/lang/Integer;

    .line 1688
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchCount:I
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3700()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->retryCount:I

    .line 1689
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListeningOverBluetooth:Z
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3800()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bluetooth"

    :goto_0
    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->microphoneUsed:Ljava/lang/String;

    .line 1690
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchResults:Ljava/util/List;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3900()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchResults:Ljava/util/List;

    .line 1691
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchAdditionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4000()Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->additionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1692
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchExplicitRetry:Z
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4100()Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->explicitRetry:Z

    .line 1693
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchListSelection:Ljava/lang/Integer;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4200()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->listItemSelected:Ljava/lang/Integer;

    .line 1694
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceSearchPrefix:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4300()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->prefix:Ljava/lang/String;

    .line 1695
    return-void

    .line 1689
    :cond_0
    const-string v0, "phone"

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized cancel(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;)V
    .locals 11
    .param p1, "reason"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;

    .prologue
    .line 1710
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z

    if-nez v0, :cond_0

    .line 1711
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1712
    const/4 v0, 0x0

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4502(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    .line 1713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z

    .line 1714
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelling voice search reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1715
    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchWords:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchConfidence:Ljava/lang/Integer;

    iget v4, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->retryCount:I

    iget-object v5, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->microphoneUsed:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchResults:Ljava/util/List;

    iget-object v7, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->additionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    iget-boolean v8, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->explicitRetry:Z

    iget-object v9, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->listItemSelected:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->prefix:Ljava/lang/String;

    move-object v0, p1

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendVoiceSearchEvent(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V
    invoke-static/range {v0 .. v10}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4400(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1719
    :cond_0
    monitor-exit p0

    return-void

    .line 1710
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 11

    .prologue
    .line 1699
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z

    if-nez v0, :cond_0

    .line 1700
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sending voice search event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1701
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchWords:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchConfidence:Ljava/lang/Integer;

    iget v4, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->retryCount:I

    iget-object v5, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->microphoneUsed:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchResults:Ljava/util/List;

    iget-object v7, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->additionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    iget-boolean v8, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->explicitRetry:Z

    iget-object v9, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->listItemSelected:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->prefix:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sendVoiceSearchEvent(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V
    invoke-static/range {v0 .. v10}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4400(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchCancelReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;Ljava/util/List;Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;ZLjava/lang/Integer;Ljava/lang/String;)V

    .line 1704
    const/4 v0, 0x0

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->voiceResultsRunnable:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4502(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;

    .line 1705
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1707
    :cond_0
    monitor-exit p0

    return-void

    .line 1699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 1723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VoiceResultsRunnable{done="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->done:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchWords=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchWords:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchConfidence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchConfidence:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->retryCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", microphoneUsed=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->microphoneUsed:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->searchResults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", additionalResultsAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->additionalResultsAction:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", explicitRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->explicitRetry:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", listItemSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->listItemSelected:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", prefix=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceResultsRunnable;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
