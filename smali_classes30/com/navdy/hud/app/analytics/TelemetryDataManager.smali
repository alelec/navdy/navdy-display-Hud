.class public final Lcom/navdy/hud/app/analytics/TelemetryDataManager;
.super Ljava/lang/Object;
.source "TelemetryDataManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;,
        Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 D2\u00020\u0001:\u0002DEB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010&\u001a\u00020#2\u0006\u0010\'\u001a\u00020(H\u0007J\u0010\u0010)\u001a\u00020#2\u0006\u0010\'\u001a\u00020*H\u0007J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0007J\u0010\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u000200H\u0007J\u0010\u00101\u001a\u00020#2\u0006\u0010\'\u001a\u000202H\u0007J\u0010\u00103\u001a\u00020#2\u0006\u0010\'\u001a\u000204H\u0007J\u0010\u00105\u001a\u00020#2\u0006\u00106\u001a\u000207H\u0007J\u0010\u00108\u001a\u00020#2\u0006\u00109\u001a\u00020:H\u0007J\u0010\u0010;\u001a\u00020#2\u0006\u0010<\u001a\u00020=H\u0007J\u0008\u0010>\u001a\u00020#H\u0002J\u0006\u0010?\u001a\u00020#J\u0010\u0010@\u001a\u00020#2\u0008\u0008\u0002\u0010A\u001a\u00020BJ\u0008\u0010C\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager;",
        "",
        "uiStateManager",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        "powerManager",
        "Lcom/navdy/hud/app/device/PowerManager;",
        "bus",
        "Lcom/squareup/otto/Bus;",
        "sharedPreferences",
        "Landroid/content/SharedPreferences;",
        "tripManager",
        "Lcom/navdy/hud/app/framework/trips/TripManager;",
        "(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V",
        "driveScoreUpdatedEvent",
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "getDriveScoreUpdatedEvent",
        "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "setDriveScoreUpdatedEvent",
        "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V",
        "handler",
        "Landroid/os/Handler;",
        "highAccuracySpeedAvailable",
        "",
        "getHighAccuracySpeedAvailable",
        "()Z",
        "setHighAccuracySpeedAvailable",
        "(Z)V",
        "lastDriveScore",
        "",
        "getLastDriveScore",
        "()I",
        "setLastDriveScore",
        "(I)V",
        "reportRunnable",
        "Lkotlin/Function0;",
        "",
        "updateDriveScoreRunnable",
        "Ljava/lang/Runnable;",
        "GPSSpeedChangeEvent",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;",
        "ObdPidChangeEvent",
        "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;",
        "onCalibratedGForceData",
        "calibratedGForceData",
        "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;",
        "onGpsSwitched",
        "gpsSwitch",
        "Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;",
        "onInit",
        "Lcom/navdy/hud/app/event/InitEvents$InitPhase;",
        "onMapEvent",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "onObdConnectionStateChanged",
        "connectionStateChangeEvent",
        "Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;",
        "onSpeedDataExpired",
        "speedDataExpired",
        "Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;",
        "onWakeup",
        "wakeUpEvent",
        "Lcom/navdy/hud/app/event/Wakeup;",
        "reportTelemetryData",
        "startTelemetrySession",
        "updateDriveScore",
        "interestingEvent",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "updateSpeed",
        "Companion",
        "DriveScoreUpdated",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field private static final ANALYTICS_REPORTING_INTERVAL:J = 0x493e0L

.field public static final Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

# The value of this static final field might be set in the static constructor
.field private static final DRIVE_SCORE_PUBLISH_INTERVAL:J = 0xbb8L

.field private static final LOG_TELEMETRY_DATA:Z

# The value of this static final field might be set in the static constructor
.field private static final MAX_ACCEL:F = 2.0f

# The value of this static final field might be set in the static constructor
.field private static final PREFERENCE_TROUBLE_CODES:Ljava/lang/String; = "PREFRENCE_TROUBLE_CODES"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private highAccuracySpeedAvailable:Z

.field private lastDriveScore:I

.field private final powerManager:Lcom/navdy/hud/app/device/PowerManager;

.field private final reportRunnable:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0",
            "<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private final tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

.field private final uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private final updateDriveScoreRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    .line 76
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TelemetryDataManager"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 77
    const-string v0, "PREFRENCE_TROUBLE_CODES"

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->PREFERENCE_TROUBLE_CODES:Ljava/lang/String;

    .line 78
    const-wide/32 v0, 0x493e0

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->ANALYTICS_REPORTING_INTERVAL:J

    .line 79
    const-wide/16 v0, 0xbb8

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->DRIVE_SCORE_PUBLISH_INTERVAL:J

    .line 80
    const/high16 v0, 0x40000000    # 2.0f

    sput v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->MAX_ACCEL:F

    .line 81
    const-string v0, "persist.sys.prop.log.telemetry"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->LOG_TELEMETRY_DATA:Z

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V
    .locals 6
    .param p1, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "bus"    # Lcom/squareup/otto/Bus;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "sharedPreferences"    # Landroid/content/SharedPreferences;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "tripManager"    # Lcom/navdy/hud/app/framework/trips/TripManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const-string v0, "uiStateManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "powerManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sharedPreferences"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tripManager"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iput-object p2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    iput-object p3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->bus:Lcom/squareup/otto/Bus;

    iput-object p4, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sharedPreferences:Landroid/content/SharedPreferences;

    iput-object p5, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 33
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$reportRunnable$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$reportRunnable$1;-><init>(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->reportRunnable:Lkotlin/jvm/functions/Function0;

    .line 39
    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;-><init>(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V

    check-cast v0, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setDataSource(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V

    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    .line 71
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$updateDriveScoreRunnable$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$updateDriveScoreRunnable$1;-><init>(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScoreRunnable:Ljava/lang/Runnable;

    .line 108
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const/16 v5, 0x64

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;-><init>(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    return-void
.end method

.method public static final synthetic access$getANALYTICS_REPORTING_INTERVAL$cp()J
    .locals 2

    .prologue
    .line 31
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->ANALYTICS_REPORTING_INTERVAL:J

    return-wide v0
.end method

.method public static final synthetic access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp()J
    .locals 2

    .prologue
    .line 31
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->DRIVE_SCORE_PUBLISH_INTERVAL:J

    return-wide v0
.end method

.method public static final synthetic access$getLOG_TELEMETRY_DATA$cp()Z
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->LOG_TELEMETRY_DATA:Z

    return v0
.end method

.method public static final synthetic access$getMAX_ACCEL$cp()F
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->MAX_ACCEL:F

    return v0
.end method

.method public static final synthetic access$getPREFERENCE_TROUBLE_CODES$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->PREFERENCE_TROUBLE_CODES:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getSLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getTripManager$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)Lcom/navdy/hud/app/framework/trips/TripManager;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    return-object v0
.end method

.method public static final synthetic access$getUiStateManager$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method public static final synthetic access$reportTelemetryData(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->reportTelemetryData()V

    return-void
.end method

.method private final reportTelemetryData()V
    .locals 26

    .prologue
    .line 237
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getLOG_TELEMETRY_DATA()Z
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getLOG_TELEMETRY_DATA$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Distance = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDistance()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Duration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDuration()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->milliSecondsToSeconds(J)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RollingDuration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->milliSecondsToSeconds(J)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AverageSpeed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionAverageSpeed()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AverageRollingSpeed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionAverageRollingSpeed()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MaxSpeed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionMaxSpeed()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HardBrakingCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HardAccelerationCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SpeedingPercentage = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionSpeedingPercentage()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TroubleCodeCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionTroubleCodeCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TroubleCodesReport = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionTroubleCodesReport()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MaxRPM = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getMaxRpm()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 250
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EVENT , averageMpg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getEventAverageMpg()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "averageRPM = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v5}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getEventAverageRpm()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fuel level = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Engine Temperature = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 255
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDistance()J

    move-result-wide v2

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v4}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->milliSecondsToSeconds(J)J

    move-result-wide v4

    sget-object v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v6}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->milliSecondsToSeconds(J)J

    move-result-wide v6

    sget-object v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionAverageSpeed()F

    move-result v8

    sget-object v9, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v9}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionAverageRollingSpeed()F

    move-result v9

    sget-object v10, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v10}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionMaxSpeed()F

    move-result v10

    sget-object v11, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v11}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v11

    sget-object v12, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v12}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v12

    sget-object v13, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v13}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionSpeedingPercentage()F

    move-result v13

    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v14}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionExcessiveSpeedingPercentage()F

    move-result v14

    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v15}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionTroubleCodeCount()I

    move-result v15

    sget-object v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionTroubleCodesReport()Ljava/lang/String;

    move-result-object v16

    sget-object v17, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionAverageMpg()D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    sget-object v18, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getMaxRpm()I

    move-result v18

    sget-object v19, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getEventAverageMpg()D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v19, v0

    sget-object v20, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getEventAverageRpm()I

    move-result v20

    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v21

    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v22

    const/16 v23, 0x5

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    sget-object v23, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getMaxG()F

    move-result v23

    sget-object v24, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getMaxGAngle()F

    move-result v24

    sget-object v25, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHighGCount()I

    move-result v25

    invoke-static/range {v2 .. v25}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVehicleTelemetryData(JJJFFFIIFFILjava/lang/String;IIIIIFFFI)V

    .line 276
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventReported()V

    .line 277
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->reportRunnable:Lkotlin/jvm/functions/Function0;

    if-nez v4, :cond_1

    const/4 v2, 0x0

    :goto_0
    check-cast v2, Ljava/lang/Runnable;

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getANALYTICS_REPORTING_INTERVAL()J
    invoke-static {v4}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getANALYTICS_REPORTING_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J

    move-result-wide v4

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    return-void

    .line 277
    :cond_1
    invoke-direct {v2, v4}, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;-><init>(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0
.end method

.method public static bridge synthetic updateDriveScore$default(Lcom/navdy/hud/app/analytics/TelemetryDataManager;Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ILjava/lang/Object;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 112
    sget-object p1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScore(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    return-void
.end method

.method private final updateSpeed()V
    .locals 3

    .prologue
    .line 230
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeedInMetersPerSecond()Lcom/navdy/hud/app/analytics/RawSpeed;

    move-result-object v1

    const-string v2, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setCurrentSpeed(Lcom/navdy/hud/app/analytics/RawSpeed;)V

    .line 234
    :cond_0
    return-void
.end method


# virtual methods
.method public final GPSSpeedChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateSpeed()V

    .line 168
    return-void
.end method

.method public final ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 219
    :goto_0
    return-void

    .line 216
    :sswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateSpeed()V

    goto :goto_0

    .line 217
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    iget-object v1, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/ConversionUtil;->convertLpHundredKmToMPG(D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setCurrentMpg(D)V

    goto :goto_0

    .line 218
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    iget-object v1, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setCurrentRpm(I)V

    goto :goto_0

    .line 215
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_2
        0xd -> :sswitch_0
        0x100 -> :sswitch_1
    .end sparse-switch
.end method

.method public final getDriveScoreUpdatedEvent()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    return-object v0
.end method

.method public final getHighAccuracySpeedAvailable()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->highAccuracySpeedAvailable:Z

    return v0
.end method

.method public final getLastDriveScore()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->lastDriveScore:I

    return v0
.end method

.method public final onCalibratedGForceData(Lcom/navdy/hud/app/device/gps/CalibratedGForceData;)V
    .locals 4
    .param p1, "calibratedGForceData"    # Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "calibratedGForceData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getXAccel()F

    move-result v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getYAccel()F

    move-result v2

    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getZAccel()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setGForce(FFF)V

    .line 227
    :cond_0
    return-void
.end method

.method public final onGpsSwitched(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;)V
    .locals 3
    .param p1, "gpsSwitch"    # Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "gpsSwitch"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGpsSwitched "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 179
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedSourceChanged()V

    .line 180
    return-void
.end method

.method public final onInit(Lcom/navdy/hud/app/event/InitEvents$InitPhase;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/InitEvents$InitPhase;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    iget-object v0, p1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetryDataManager$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 140
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Post Start , Quiet mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->startTelemetrySession()V

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onMapEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    iget v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setCurrentSpeedLimit(F)V

    .line 186
    return-void
.end method

.method public final onObdConnectionStateChanged(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 7
    .param p1, "connectionStateChangeEvent"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v3, "connectionStateChangeEvent"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v3}, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedSourceChanged()V

    .line 191
    iget-boolean v3, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;->connected:Z

    if-eqz v3, :cond_1

    .line 192
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v0

    .line 193
    .local v0, "pidSet":Lcom/navdy/obd/PidSet;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->highAccuracySpeedAvailable:Z

    .line 194
    iget-object v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sharedPreferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getPREFERENCE_TROUBLE_CODES()Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getPREFERENCE_TROUBLE_CODES$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    .local v2, "troubleCodesString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/obd/ObdManager;->getTroubleCodes()Ljava/util/List;

    move-result-object v1

    .line 197
    .local v1, "troubleCodes":Ljava/util/List;
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    iget-object v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getPREFERENCE_TROUBLE_CODES()Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getPREFERENCE_TROUBLE_CODES$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 199
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setSessionTroubleCodesReport(Ljava/lang/String;)V

    .line 209
    .end local v0    # "pidSet":Lcom/navdy/obd/PidSet;
    .end local v1    # "troubleCodes":Ljava/util/List;
    .end local v2    # "troubleCodesString":Ljava/lang/String;
    :goto_0
    return-void

    .line 202
    .restart local v0    # "pidSet":Lcom/navdy/obd/PidSet;
    .restart local v1    # "troubleCodes":Ljava/util/List;
    .restart local v2    # "troubleCodesString":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    const-string v4, "troubleCodesString"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->serializeTroubleCodes(Ljava/util/List;Ljava/lang/String;)Lkotlin/Pair;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 203
    iget-object v5, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getPREFERENCE_TROUBLE_CODES()Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getPREFERENCE_TROUBLE_CODES$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 204
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setSessionTroubleCodesReport(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    .end local v0    # "pidSet":Lcom/navdy/obd/PidSet;
    .end local v1    # "troubleCodes":Ljava/util/List;
    .end local v2    # "troubleCodesString":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->highAccuracySpeedAvailable:Z

    goto :goto_0
.end method

.method public final onSpeedDataExpired(Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;)V
    .locals 1
    .param p1, "speedDataExpired"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "speedDataExpired"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedSourceChanged()V

    .line 173
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateSpeed()V

    .line 174
    return-void
.end method

.method public final onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 1
    .param p1, "wakeUpEvent"    # Lcom/navdy/hud/app/event/Wakeup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "wakeUpEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->startTelemetrySession()V

    .line 151
    return-void
.end method

.method public final setDriveScoreUpdatedEvent(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    return-void
.end method

.method public final setHighAccuracySpeedAvailable(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->highAccuracySpeedAvailable:Z

    return-void
.end method

.method public final setLastDriveScore(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->lastDriveScore:I

    return-void
.end method

.method public final startTelemetrySession()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 154
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionStarted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 155
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "Starting telemetry session"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 156
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->startSession()V

    .line 157
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->reportRunnable:Lkotlin/jvm/functions/Function0;

    if-nez v3, :cond_0

    move-object v0, v1

    :goto_0
    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 158
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->reportRunnable:Lkotlin/jvm/functions/Function0;

    if-nez v3, :cond_1

    move-object v0, v1

    :goto_1
    check-cast v0, Ljava/lang/Runnable;

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getANALYTICS_REPORTING_INTERVAL()J
    invoke-static {v1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getANALYTICS_REPORTING_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScoreRunnable:Ljava/lang/Runnable;

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getDRIVE_SCORE_PUBLISH_INTERVAL()J
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    :goto_2
    return-void

    .line 157
    :cond_0
    invoke-direct {v0, v3}, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;-><init>(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 158
    :cond_1
    invoke-direct {v0, v3}, Lcom/navdy/hud/app/analytics/TelemetryDataManagerKt$sam$Runnable$cc6381d8;-><init>(Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 161
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Telemetry session is already started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final updateDriveScore(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V
    .locals 18
    .param p1, "interestingEvent"    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v2, "interestingEvent"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    .line 115
    .local v8, "$receiver":Lcom/navdy/hud/app/analytics/TelemetrySession;
    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v2

    const/4 v4, 0x0

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v2

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHighGCount()I

    move-result v3

    add-int/2addr v2, v3

    int-to-double v2, v2

    const-wide v4, 0x4080e00000000000L    # 540.0

    mul-double/2addr v2, v4

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v4

    long-to-double v4, v4

    const/high16 v6, 0x447a0000    # 1000.0f

    float-to-double v14, v6

    div-double/2addr v4, v14

    const/16 v6, 0x3c

    int-to-double v14, v6

    add-double/2addr v4, v14

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v10, v2, v4

    .line 116
    .local v10, "hardAccelerationFactor":D
    :goto_0
    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionSpeedingPercentage()F

    move-result v2

    const/16 v3, 0x32

    int-to-float v3, v3

    mul-float v12, v2, v3

    .line 117
    .local v12, "speedingFactor":F
    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionExcessiveSpeedingPercentage()F

    move-result v2

    const/16 v3, 0x32

    int-to-float v3, v3

    mul-float v9, v2, v3

    .line 118
    .local v9, "excessiveSpeedingFactor":F
    const-wide/16 v2, 0x0

    const/16 v4, 0x64

    int-to-double v4, v4

    float-to-double v14, v12

    add-double/2addr v14, v10

    float-to-double v0, v9

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    sub-double/2addr v4, v14

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v7, v2

    .line 119
    .local v7, "drivingScore":I
    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->isSpeeding()Z

    move-result v4

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->isExcessiveSpeeding()Z

    move-result v5

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->isDoingHighGManeuver()Z

    move-result v6

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;-><init>(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    .line 121
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->lastDriveScore:I

    if-eq v7, v2, :cond_1

    .line 122
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HA : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , HB : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , HG : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHighGCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 123
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Speeding percentage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionSpeedingPercentage()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 124
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Excessive Speeding percentage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionExcessiveSpeedingPercentage()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 125
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hard Acceleration Factor : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , Speeding Factor : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , excessive Speeding factor "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 126
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DriveScoreUpdate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->driveScoreUpdatedEvent:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 129
    :cond_1
    move-object/from16 v0, p0

    iput v7, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->lastDriveScore:I

    .line 131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScoreRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScoreRunnable:Ljava/lang/Runnable;

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->Companion:Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;

    # invokes: Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->getDRIVE_SCORE_PUBLISH_INTERVAL()J
    invoke-static {v4}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;->access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 134
    return-void

    .line 115
    .end local v7    # "drivingScore":I
    .end local v9    # "excessiveSpeedingFactor":F
    .end local v10    # "hardAccelerationFactor":D
    .end local v12    # "speedingFactor":F
    :cond_2
    const-wide/16 v10, 0x0

    goto/16 :goto_0
.end method
