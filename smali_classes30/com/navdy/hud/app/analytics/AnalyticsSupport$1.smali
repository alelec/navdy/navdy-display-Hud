.class final Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 507
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 508
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isNetworkAccessAllowed(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "called Localytics.upload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 510
    invoke-static {}, Lcom/localytics/android/Localytics;->upload()V

    .line 512
    :cond_0
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v0

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getUpdateInterval()J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$100()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 513
    return-void
.end method
