.class public final Lcom/navdy/hud/app/analytics/RawSpeed;
.super Ljava/lang/Object;
.source "RawSpeed.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\t\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/RawSpeed;",
        "",
        "speed",
        "",
        "timeStamp",
        "",
        "(FJ)V",
        "getSpeed",
        "()F",
        "getTimeStamp",
        "()J",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final speed:F

.field private final timeStamp:J


# direct methods
.method public constructor <init>(FJ)V
    .locals 0
    .param p1, "speed"    # F
    .param p2, "timeStamp"    # J

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    iput-wide p2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    return-void
.end method

.method public synthetic constructor <init>(FJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    .line 8
    const-wide/16 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V

    return-void
.end method

.method public static bridge synthetic copy$default(Lcom/navdy/hud/app/analytics/RawSpeed;FJILjava/lang/Object;)Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    iget p1, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    iget-wide p2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/navdy/hud/app/analytics/RawSpeed;->copy(FJ)Lcom/navdy/hud/app/analytics/RawSpeed;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    .prologue
    iget v0, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    return v0
.end method

.method public final component2()J
    .locals 2

    .prologue
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    return-wide v0
.end method

.method public final copy(FJ)Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 2
    .param p1, "speed"    # F
    .param p2, "timeStamp"    # J
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lcom/navdy/hud/app/analytics/RawSpeed;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/navdy/hud/app/analytics/RawSpeed;

    iget v2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    iget v3, p1, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    iget-wide v4, p1, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final getSpeed()F
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    return v0
.end method

.method public final getTimeStamp()J
    .locals 2

    .prologue
    .line 8
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 6

    iget v0, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RawSpeed(speed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->speed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeStamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/RawSpeed;->timeStamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
