.class Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;
.super Ljava/lang/Object;
.source "NavigationQualityTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Report"
.end annotation


# static fields
.field private static final HEAVY_TRAFFIC_RATIO:D = 1.25

.field private static final MODERATE_TRAFFIC_RATIO:D = 1.1


# instance fields
.field actualDistance:J

.field actualDuration:J

.field final expectedDistance:J

.field final expectedDuration:J

.field nRecalculations:I

.field final trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;


# direct methods
.method private constructor <init>(JJJZ)V
    .locals 9
    .param p1, "durationWithTraffic"    # J
    .param p3, "duration"    # J
    .param p5, "expectedDistance"    # J
    .param p7, "isTrafficEnabled"    # Z

    .prologue
    const-wide v6, 0x3ff199999999999aL    # 1.1

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-wide p1, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    .line 137
    iput-wide p5, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    .line 139
    const/4 v2, 0x0

    iput v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->nRecalculations:I

    .line 141
    if-eqz p7, :cond_2

    .line 142
    long-to-double v2, p1

    long-to-double v4, p3

    div-double v0, v2, v4

    .line 144
    .local v0, "trafficRatio":D
    cmpg-double v2, v0, v6

    if-gez v2, :cond_0

    .line 145
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->LIGHT:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    iput-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    .line 154
    .end local v0    # "trafficRatio":D
    :goto_0
    return-void

    .line 146
    .restart local v0    # "trafficRatio":D
    :cond_0
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_1

    const-wide/high16 v2, 0x3ff4000000000000L    # 1.25

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    .line 147
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->MODERATE:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    iput-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    goto :goto_0

    .line 149
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->HEAVY:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    iput-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    goto :goto_0

    .line 152
    .end local v0    # "trafficRatio":D
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;->NO_DATA:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    iput-object v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    goto :goto_0
.end method

.method synthetic constructor <init>(JJJZLcom/navdy/hud/app/analytics/NavigationQualityTracker$1;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # J
    .param p5, "x2"    # J
    .param p7, "x3"    # Z
    .param p8, "x4"    # Lcom/navdy/hud/app/analytics/NavigationQualityTracker$1;

    .prologue
    .line 121
    invoke-direct/range {p0 .. p7}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;-><init>(JJJZ)V

    return-void
.end method


# virtual methods
.method getDistanceVariance()D
    .locals 6

    .prologue
    .line 161
    const-wide/16 v0, 0x64

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDistance:J

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    sub-long/2addr v2, v4

    mul-long/2addr v0, v2

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method getDurationVariance()D
    .locals 6

    .prologue
    .line 157
    const-wide/16 v0, 0x64

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDuration:J

    iget-wide v4, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    sub-long/2addr v2, v4

    mul-long/2addr v0, v2

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public isValid()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 177
    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDistance:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDuration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Report{trafficLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->trafficLevel:Lcom/navdy/hud/app/analytics/NavigationQualityTracker$TrafficLevel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", expectedDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDuration:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", expectedDistance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->expectedDistance:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actualDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDuration:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actualDistance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->actualDistance:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nRecalculations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/analytics/NavigationQualityTracker$Report;->nRecalculations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
