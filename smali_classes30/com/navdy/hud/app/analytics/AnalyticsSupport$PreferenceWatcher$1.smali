.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->enqueue(Lcom/navdy/hud/app/profile/DriverProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

.field final synthetic val$profile:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .prologue
    .line 883
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->this$0:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    iput-object p2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->val$profile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 886
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->this$0:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    iget-object v2, v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->adapter:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;

    iget-object v3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->val$profile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;->calculateState(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;

    move-result-object v1

    .line 887
    .local v1, "newState":Lcom/navdy/hud/app/analytics/Event;
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->this$0:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    iget-object v2, v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->oldState:Lcom/navdy/hud/app/analytics/Event;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/analytics/Event;->getChangedFields(Lcom/navdy/hud/app/analytics/Event;)Ljava/lang/String;

    move-result-object v0

    .line 888
    .local v0, "changedFields":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 889
    iget-object v2, v1, Lcom/navdy/hud/app/analytics/Event;->argMap:Ljava/util/Map;

    const-string v3, "Preferences_Updated"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    iget-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;->this$0:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    iput-object v1, v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->oldState:Lcom/navdy/hud/app/analytics/Event;

    .line 891
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Recording preference change: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 892
    const/4 v2, 0x0

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Lcom/navdy/hud/app/analytics/Event;Z)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2200(Lcom/navdy/hud/app/analytics/Event;Z)V

    .line 896
    :goto_0
    return-void

    .line 894
    :cond_0
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No change in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/navdy/hud/app/analytics/Event;->tag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
