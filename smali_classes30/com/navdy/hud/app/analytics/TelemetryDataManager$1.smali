.class public final Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;
.super Ljava/lang/Object;
.source "TelemetryDataManager.kt"

# interfaces
.implements Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/analytics/TelemetryDataManager;-><init>(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000cH\u0016J\u0008\u0010\u000e\u001a\u00020\u0004H\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "com/navdy/hud/app/analytics/TelemetryDataManager$1",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;",
        "(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V",
        "absoluteCurrentTime",
        "",
        "currentSpeed",
        "Lcom/navdy/hud/app/analytics/RawSpeed;",
        "interestingEventDetected",
        "",
        "event",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "isHighAccuracySpeedAvailable",
        "",
        "isVerboseLoggingNeeded",
        "totalDistanceTravelledWithMeters",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public absoluteCurrentTime()J
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public currentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 63
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeedInMetersPerSecond()Lcom/navdy/hud/app/analytics/RawSpeed;

    move-result-object v0

    const-string v1, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->updateDriveScore(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    .line 56
    return-void
.end method

.method public isHighAccuracySpeedAvailable()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->getHighAccuracySpeedAvailable()Z

    move-result v0

    return v0
.end method

.method public isVerboseLoggingNeeded()Z
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getUiStateManager$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getUiStateManager$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->isShowingDriveScoreGauge:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public totalDistanceTravelledWithMeters()J
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$1;->this$0:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    # getter for: Lcom/navdy/hud/app/analytics/TelemetryDataManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->access$getTripManager$p(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)Lcom/navdy/hud/app/framework/trips/TripManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelledWithNavdy()J

    move-result-wide v0

    return-wide v0
.end method
