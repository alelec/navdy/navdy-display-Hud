.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FuelMonitor"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$1;

    .prologue
    .line 1402
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$FuelMonitor;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const-wide/32 v10, 0x2bf20

    .line 1408
    :try_start_0
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3400()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getDistanceTravelled()I

    move-result v0

    .line 1409
    .local v0, "distance":I
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3400()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v1

    .line 1410
    .local v1, "fuelLevel":I
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3400()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getInstantFuelConsumption()D

    move-result-wide v2

    .line 1411
    .local v2, "fuelConsumption":D
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3400()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v4

    .line 1412
    .local v4, "vin":Ljava/lang/String;
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 1413
    const-string v5, "Fuel_Level"

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "VIN"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v7, 0x2

    const-string v8, "Fuel_Level"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    .line 1415
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "Fuel_Consumption"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    .line 1416
    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "Distance"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    .line 1417
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1413
    invoke-static {v5, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1420
    :cond_0
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, p0, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1422
    return-void

    .line 1420
    .end local v0    # "distance":I
    .end local v1    # "fuelLevel":I
    .end local v2    # "fuelConsumption":D
    .end local v4    # "vin":Ljava/lang/String;
    :catchall_0
    move-exception v5

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, p0, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v5
.end method
