.class public final enum Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
.super Ljava/lang/Enum;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VoiceSearchAdditionalResultsAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

.field public static final enum ADDITIONAL_RESULTS_GO:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

.field public static final enum ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

.field public static final enum ADDITIONAL_RESULTS_TIMEOUT:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;


# instance fields
.field public final tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1651
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    const-string v1, "ADDITIONAL_RESULTS_TIMEOUT"

    const-string v2, "timeout"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_TIMEOUT:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1652
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    const-string v1, "ADDITIONAL_RESULTS_LIST"

    const-string v2, "list"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1653
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    const-string v1, "ADDITIONAL_RESULTS_GO"

    const-string v2, "go"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_GO:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    .line 1650
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_TIMEOUT:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_GO:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1656
    iput-object p3, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->tag:Ljava/lang/String;

    .line 1657
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1650
    const-class v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;
    .locals 1

    .prologue
    .line 1650
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->$VALUES:[Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    return-object v0
.end method
