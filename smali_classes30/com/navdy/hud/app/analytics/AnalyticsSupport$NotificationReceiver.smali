.class public Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotificationReceiver"
.end annotation


# instance fields
.field public appPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private lastRequiredManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field private lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field public powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 631
    iput-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 652
    iput-object v2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastRequiredManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 530
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    .line 531
    .local v0, "bus":Lcom/squareup/otto/Bus;
    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 533
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    .line 534
    .local v1, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V

    .line 535
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 536
    return-void
.end method

.method private objEquals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "s1"    # Ljava/lang/Object;
    .param p2, "s2"    # Ljava/lang/Object;

    .prologue
    .line 635
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 696
    iget-boolean v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;->connected:Z

    if-eqz v0, :cond_0

    .line 697
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->reportObdState()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1900()V

    .line 699
    :cond_0
    return-void
.end method

.method public onArrived(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 707
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->processNavigationArrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    invoke-static {p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2100(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V

    .line 708
    return-void
.end method

.method public onArrived(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 669
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;->LAST:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    if-ne v0, v1, :cond_0

    .line 670
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "NAVIGATION_ARRIVED"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 671
    const-string v0, "Navigation_Arrived"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 673
    :cond_0
    return-void
.end method

.method public onBandwidthSettingChanged(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 688
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getUpdateInterval()J
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$100()J

    move-result-wide v0

    .line 689
    .local v0, "interval":J
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uploader changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 690
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sUploader:Ljava/lang/Runnable;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1800()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 691
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sUploader:Ljava/lang/Runnable;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1800()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 692
    return-void
.end method

.method public onConnectionStatusChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 605
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 606
    const-string v0, "Mobile_App_Disconnect"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Device_Id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Remote_Device_Id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 609
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sRemoteDeviceId:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1400()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 606
    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 611
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->clearVoiceSearch()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1500()V

    .line 613
    :cond_0
    return-void
.end method

.method public onDateTimeAvailable(Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 572
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->maybeOpenSession()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$700()V

    .line 573
    return-void
.end method

.method public onDeviceInfoAvailable(Lcom/navdy/hud/app/event/DeviceInfoAvailable;)V
    .locals 1
    .param p1, "deviceInfoAvailable"    # Lcom/navdy/hud/app/event/DeviceInfoAvailable;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 554
    iget-object v0, p1, Lcom/navdy/hud/app/event/DeviceInfoAvailable;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p1, Lcom/navdy/hud/app/event/DeviceInfoAvailable;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->remoteDevice:Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$402(Lcom/navdy/service/library/events/DeviceInfo;)Lcom/navdy/service/library/events/DeviceInfo;

    .line 556
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setupProfile()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$500()V

    .line 558
    :cond_0
    return-void
.end method

.method public onDismissScreen(Lcom/navdy/service/library/events/ui/DismissScreen;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/ui/DismissScreen;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 676
    return-void
.end method

.method public onDriverProfileChange(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 584
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 585
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 586
    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$902(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/profile/DriverProfile;

    .line 587
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Remembering cached preferences"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 588
    new-instance v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$2;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;)V

    invoke-direct {v1, v0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;-><init>(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;)V

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastProfilePreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;
    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1002(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .line 594
    new-instance v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    new-instance v2, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$3;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;)V

    invoke-direct {v1, v0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;-><init>(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;)V

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLastNavigationPreferences:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;
    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1202(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;)Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;

    .line 601
    :cond_0
    return-void
.end method

.method public onManeuverMissed(Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 664
    const-string v0, "Navigation_Maneuver_Missed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 665
    return-void
.end method

.method public onManeuverRequired(Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 656
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;->maneuver:Lcom/here/android/mpa/routing/Maneuver;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastRequiredManeuver:Lcom/here/android/mpa/routing/Maneuver;

    if-eq v0, v1, :cond_0

    .line 657
    const-string v0, "Navigation_Maneuver_Required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 658
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;->maneuver:Lcom/here/android/mpa/routing/Maneuver;

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastRequiredManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 660
    :cond_0
    return-void
.end method

.method public onManeuverWarning(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iget v1, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 643
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->objEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 644
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->objEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 645
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->objEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 646
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->objEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 647
    :cond_0
    const-string v0, "Navigation_Maneuver_Warning"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 648
    iput-object p1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;->lastWarningEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 650
    :cond_1
    return-void
.end method

.method public onMapEngineInitialized(Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 562
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver$1;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 568
    return-void
.end method

.method public onRegionEvent(Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 624
    const-string v0, "Map_Region_Change"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "State"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Country"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 629
    return-void
.end method

.method public onRouteSearchRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 702
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->processNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    invoke-static {p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$2000(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 703
    return-void
.end method

.method public onShowToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;)V
    .locals 2
    .param p1, "toast"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 617
    iget-object v0, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;->name:Ljava/lang/String;

    const-string v1, "disconnection#toast"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPhoneDisconnect(ZLjava/lang/String;)V

    .line 620
    :cond_0
    return-void
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 577
    iget-object v0, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->SHUTTING_DOWN:Lcom/navdy/hud/app/event/Shutdown$State;

    if-ne v0, v1, :cond_0

    .line 578
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->closeSession()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$800()V

    .line 580
    :cond_0
    return-void
.end method

.method public onStart(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 539
    sget-object v2, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v2, :cond_0

    .line 540
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    .line 541
    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    .line 542
    .local v0, "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$300()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$300()Ljava/lang/String;

    move-result-object v1

    .line 543
    .local v1, "source":Ljava/lang/String;
    :goto_0
    const-string v2, "Glance_Open_Mini"

    invoke-static {v2, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 545
    .end local v0    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v1    # "source":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->glanceNavigationSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$302(Ljava/lang/String;)Ljava/lang/String;

    .line 546
    return-void

    .line 542
    .restart local v0    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_1
    const-string v1, "auto"

    goto :goto_0
.end method

.method public onStop(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 550
    return-void
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 680
    iget-object v0, p1, Lcom/navdy/hud/app/event/Wakeup;->reason:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordWakeup(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1600(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V

    .line 681
    const/4 v0, 0x0

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->quietMode:Z
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$1702(Z)Z

    .line 682
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->maybeOpenSession()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$700()V

    .line 683
    return-void
.end method
