.class public Lcom/navdy/hud/app/analytics/AnalyticsSupport$AnalyticsIntentsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnalyticsIntentsReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2314
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 2317
    const-string v3, "tag"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2318
    .local v2, "tagName":Ljava/lang/String;
    const-string v3, "include_device_info"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2319
    .local v1, "includeDeviceInfo":Z
    if-nez v2, :cond_0

    .line 2329
    :goto_0
    return-void

    .line 2320
    :cond_0
    const-string v3, "arguments"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2321
    .local v0, "args":[Ljava/lang/String;
    if-nez v0, :cond_1

    new-array v0, v4, [Ljava/lang/String;

    .line 2323
    :cond_1
    const-string v3, "Analytics_New_Device"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2325
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNewDevicePaired()V
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4600()V

    goto :goto_0

    .line 2327
    :cond_2
    # invokes: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V
    invoke-static {v2, v1, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$4700(Ljava/lang/String;Z[Ljava/lang/String;)V

    goto :goto_0
.end method
