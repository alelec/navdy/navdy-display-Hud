.class public final Lcom/navdy/hud/app/analytics/TelemetrySession;
.super Ljava/lang/Object;
.source "TelemetrySession.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;,
        Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0019\n\u0002\u0018\u0002\n\u0002\u00081\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u00081\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0004\u00c6\u0001\u00c7\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u00bc\u0001\u001a\u00030\u00bd\u0001J#\u0010\u00be\u0001\u001a\u00030\u00bd\u00012\u0007\u0010\u00bf\u0001\u001a\u00020\u00042\u0007\u0010\u00c0\u0001\u001a\u00020\u00042\u0007\u0010\u00c1\u0001\u001a\u00020\u0004J\u0008\u0010\u00c2\u0001\u001a\u00030\u00bd\u0001J\u0008\u0010\u00c3\u0001\u001a\u00030\u00bd\u0001J\u0008\u0010\u00c4\u0001\u001a\u00030\u00bd\u0001J\n\u0010\u00c5\u0001\u001a\u00030\u00bd\u0001H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\tR\u000e\u0010\u000c\u001a\u00020\rX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u0011X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u0011X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013R \u0010\u0018\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8B@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0017\u001a\u00020\u001b8B@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010\u001a\"\u0004\u0008$\u0010%R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+R$\u0010-\u001a\u00020\u00072\u0006\u0010,\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010\t\"\u0004\u0008/\u00100R$\u00101\u001a\u00020\u00112\u0006\u0010,\u001a\u00020\u0011@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00082\u0010\u0013\"\u0004\u00083\u00104R$\u00105\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00086\u0010\u001e\"\u0004\u00087\u00108R$\u00109\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0004@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008:\u0010;\"\u0004\u0008<\u0010=R\u0011\u0010>\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008?\u0010\u001aR\u001c\u0010@\u001a\u0004\u0018\u00010AX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008B\u0010C\"\u0004\u0008D\u0010ER\u001a\u0010F\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008G\u0010\t\"\u0004\u0008H\u00100R\u001a\u0010I\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008J\u0010\u0013\"\u0004\u0008K\u00104R\u001a\u0010L\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008M\u0010\u001a\"\u0004\u0008N\u0010%R\u001a\u0010O\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008P\u0010\u0013\"\u0004\u0008Q\u00104R\u001c\u0010R\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008S\u0010\u001a\"\u0004\u0008T\u0010%R\u001a\u0010U\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008V\u0010\u001a\"\u0004\u0008W\u0010%R\u001a\u0010X\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008Y\u0010\u001a\"\u0004\u0008Z\u0010%R\u001a\u0010[\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008[\u0010)\"\u0004\u0008\\\u0010+R$\u0010]\u001a\u00020\'2\u0006\u0010,\u001a\u00020\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008]\u0010)\"\u0004\u0008^\u0010+R$\u0010_\u001a\u00020\'2\u0006\u0010,\u001a\u00020\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008_\u0010)\"\u0004\u0008`\u0010+R$\u0010a\u001a\u00020\'2\u0006\u0010,\u001a\u00020\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008a\u0010)\"\u0004\u0008b\u0010+R\u001a\u0010c\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008d\u0010;\"\u0004\u0008e\u0010=R\u001a\u0010f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008g\u0010;\"\u0004\u0008h\u0010=R\u001a\u0010i\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008j\u0010\u001a\"\u0004\u0008k\u0010%R\u001a\u0010l\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008m\u0010\u001a\"\u0004\u0008n\u0010%R\u001a\u0010o\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008p\u0010\u001a\"\u0004\u0008q\u0010%R\u000e\u0010r\u001a\u00020sX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008u\u0010;\"\u0004\u0008v\u0010=R\u001a\u0010w\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008x\u0010;\"\u0004\u0008y\u0010=R\u001a\u0010z\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008{\u0010\u0013\"\u0004\u0008|\u00104R\u000e\u0010}\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010~\u001a\u0015\u0012\u0011\u0012\u000f\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0080\u00010\u007f\u00a2\u0006\n\n\u0000\u001a\u0006\u0008\u0081\u0001\u0010\u0082\u0001R\u001d\u0010\u0083\u0001\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u0084\u0001\u0010\t\"\u0005\u0008\u0085\u0001\u00100R)\u0010\u0086\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u0087\u0001\u0010;\"\u0005\u0008\u0088\u0001\u0010=R)\u0010\u0089\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u008a\u0001\u0010;\"\u0005\u0008\u008b\u0001\u0010=R\u001f\u0010\u008c\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u008d\u0001\u0010\u001a\"\u0005\u0008\u008e\u0001\u0010%R)\u0010\u008f\u0001\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u0090\u0001\u0010\u001a\"\u0005\u0008\u0091\u0001\u0010%R\'\u0010\u0092\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\u0008\u0093\u0001\u0010;\"\u0005\u0008\u0094\u0001\u0010=R\u001d\u0010\u0095\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u0096\u0001\u0010\u0013\"\u0005\u0008\u0097\u0001\u00104R\u001d\u0010\u0098\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u0099\u0001\u0010\u0013\"\u0005\u0008\u009a\u0001\u00104R\u001d\u0010\u009b\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u009c\u0001\u0010\u0013\"\u0005\u0008\u009d\u0001\u00104R\u001d\u0010\u009e\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u009f\u0001\u0010;\"\u0005\u0008\u00a0\u0001\u0010=R\u001d\u0010\u00a1\u0001\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u00a2\u0001\u0010\u001a\"\u0005\u0008\u00a3\u0001\u0010%R\u001f\u0010\u00a4\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u00a5\u0001\u0010\u001a\"\u0005\u0008\u00a6\u0001\u0010%R\'\u0010\u00a7\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\u0008\u00a8\u0001\u0010;\"\u0005\u0008\u00a9\u0001\u0010=R\u000f\u0010\u00aa\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u00ab\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u00ac\u0001\u0010)\"\u0005\u0008\u00ad\u0001\u0010+R\u001d\u0010\u00ae\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u00af\u0001\u0010\u0013\"\u0005\u0008\u00b0\u0001\u00104R \u0010\u00b1\u0001\u001a\u00030\u00b2\u0001X\u0086\u000e\u00a2\u0006\u0012\n\u0000\u001a\u0006\u0008\u00b3\u0001\u0010\u00b4\u0001\"\u0006\u0008\u00b5\u0001\u0010\u00b6\u0001R\u001f\u0010\u00b7\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\u0008\u00b8\u0001\u0010\u001a\"\u0005\u0008\u00b9\u0001\u0010%R\u000f\u0010\u00ba\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000f\u0010\u00bb\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00c8\u0001"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetrySession;",
        "",
        "()V",
        "EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS",
        "",
        "G_LOWER_THRESHOLD_THRESHOLD",
        "HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND",
        "",
        "getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND",
        "()D",
        "HARD_BRAKING_THRESHOLD_METERS_PER_SECOND",
        "getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND",
        "HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS",
        "",
        "HIGH_G_THRESHOLD_TIME_MILLIS",
        "MAX_G_THRESHOLD",
        "MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION",
        "",
        "getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION",
        "()I",
        "SPEEDING_SPEED_OVER_SPEED_LIMIT_MS",
        "SPEED_DATA_ROLLING_WINDOW_SIZE",
        "getSPEED_DATA_ROLLING_WINDOW_SIZE",
        "<set-?>",
        "_currentDistance",
        "get_currentDistance",
        "()J",
        "Lcom/navdy/hud/app/analytics/RawSpeed;",
        "_currentSpeed",
        "get_currentSpeed",
        "()Lcom/navdy/hud/app/analytics/RawSpeed;",
        "_excessiveSpeedingDuration",
        "_rollingDuration",
        "_speedingDuration",
        "confirmedHighGManeuverEndTime",
        "getConfirmedHighGManeuverEndTime",
        "setConfirmedHighGManeuverEndTime",
        "(J)V",
        "confirmedHighGManueverNotified",
        "",
        "getConfirmedHighGManueverNotified",
        "()Z",
        "setConfirmedHighGManueverNotified",
        "(Z)V",
        "value",
        "currentMpg",
        "getCurrentMpg",
        "setCurrentMpg",
        "(D)V",
        "currentRpm",
        "getCurrentRpm",
        "setCurrentRpm",
        "(I)V",
        "currentSpeed",
        "getCurrentSpeed",
        "setCurrentSpeed",
        "(Lcom/navdy/hud/app/analytics/RawSpeed;)V",
        "currentSpeedLimit",
        "getCurrentSpeedLimit",
        "()F",
        "setCurrentSpeedLimit",
        "(F)V",
        "currentTime",
        "getCurrentTime",
        "dataSource",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;",
        "getDataSource",
        "()Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;",
        "setDataSource",
        "(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V",
        "eventAverageMpg",
        "getEventAverageMpg",
        "setEventAverageMpg",
        "eventAverageRpm",
        "getEventAverageRpm",
        "setEventAverageRpm",
        "eventMpgSamples",
        "getEventMpgSamples",
        "setEventMpgSamples",
        "eventRpmSamples",
        "getEventRpmSamples",
        "setEventRpmSamples",
        "excessiveSpeedingDuration",
        "getExcessiveSpeedingDuration",
        "setExcessiveSpeedingDuration",
        "excessiveSpeedingStartTime",
        "getExcessiveSpeedingStartTime",
        "setExcessiveSpeedingStartTime",
        "highGManeuverStartTime",
        "getHighGManeuverStartTime",
        "setHighGManeuverStartTime",
        "isAboveWaterMark",
        "setAboveWaterMark",
        "isDoingHighGManeuver",
        "setDoingHighGManeuver",
        "isExcessiveSpeeding",
        "setExcessiveSpeeding",
        "isSpeeding",
        "setSpeeding",
        "lastG",
        "getLastG",
        "setLastG",
        "lastGAngle",
        "getLastGAngle",
        "setLastGAngle",
        "lastGTime",
        "getLastGTime",
        "setLastGTime",
        "lastHardAccelerationTime",
        "getLastHardAccelerationTime",
        "setLastHardAccelerationTime",
        "lastHardBrakingTime",
        "getLastHardBrakingTime",
        "setLastHardBrakingTime",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "maxG",
        "getMaxG",
        "setMaxG",
        "maxGAngle",
        "getMaxGAngle",
        "setMaxGAngle",
        "maxRpm",
        "getMaxRpm",
        "setMaxRpm",
        "movingStartTime",
        "rollingSpeedData",
        "Ljava/util/LinkedList;",
        "Lkotlin/Pair;",
        "getRollingSpeedData",
        "()Ljava/util/LinkedList;",
        "sessionAverageMpg",
        "getSessionAverageMpg",
        "setSessionAverageMpg",
        "sessionAverageRollingSpeed",
        "getSessionAverageRollingSpeed",
        "setSessionAverageRollingSpeed",
        "sessionAverageSpeed",
        "getSessionAverageSpeed",
        "setSessionAverageSpeed",
        "sessionDistance",
        "getSessionDistance",
        "setSessionDistance",
        "sessionDuration",
        "getSessionDuration",
        "setSessionDuration",
        "sessionExcessiveSpeedingPercentage",
        "getSessionExcessiveSpeedingPercentage",
        "setSessionExcessiveSpeedingPercentage",
        "sessionHardAccelerationCount",
        "getSessionHardAccelerationCount",
        "setSessionHardAccelerationCount",
        "sessionHardBrakingCount",
        "getSessionHardBrakingCount",
        "setSessionHardBrakingCount",
        "sessionHighGCount",
        "getSessionHighGCount",
        "setSessionHighGCount",
        "sessionMaxSpeed",
        "getSessionMaxSpeed",
        "setSessionMaxSpeed",
        "sessionMpgSamples",
        "getSessionMpgSamples",
        "setSessionMpgSamples",
        "sessionRollingDuration",
        "getSessionRollingDuration",
        "setSessionRollingDuration",
        "sessionSpeedingPercentage",
        "getSessionSpeedingPercentage",
        "setSessionSpeedingPercentage",
        "sessionStartTime",
        "sessionStarted",
        "getSessionStarted",
        "setSessionStarted",
        "sessionTroubleCodeCount",
        "getSessionTroubleCodeCount",
        "setSessionTroubleCodeCount",
        "sessionTroubleCodesReport",
        "",
        "getSessionTroubleCodesReport",
        "()Ljava/lang/String;",
        "setSessionTroubleCodesReport",
        "(Ljava/lang/String;)V",
        "speedingDuration",
        "getSpeedingDuration",
        "setSpeedingDuration",
        "speedingStartTime",
        "startDistance",
        "eventReported",
        "",
        "setGForce",
        "x",
        "y",
        "z",
        "speedSourceChanged",
        "startSession",
        "stopSession",
        "updateValuesBasedOnSpeed",
        "DataSource",
        "InterestingEvent",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field private static final EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F = 0.0f

.field private static final G_LOWER_THRESHOLD_THRESHOLD:F = 0.3f

.field private static final HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND:D = 3.53

.field private static final HARD_BRAKING_THRESHOLD_METERS_PER_SECOND:D = 3.919

.field private static final HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS:J = 0xbb8L

.field private static final HIGH_G_THRESHOLD_TIME_MILLIS:J = 0x1f4L

.field public static final INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession; = null

.field private static final MAX_G_THRESHOLD:F = 0.45f

.field private static final MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION:I = 0x7d0

.field private static final SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F = 0.0f

.field private static final SPEED_DATA_ROLLING_WINDOW_SIZE:I = 0x1f4

.field private static _currentDistance:J

.field private static _currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

.field private static _excessiveSpeedingDuration:J

.field private static _rollingDuration:J

.field private static _speedingDuration:J

.field private static confirmedHighGManeuverEndTime:J

.field private static confirmedHighGManueverNotified:Z

.field private static currentMpg:D

.field private static currentRpm:I

.field private static currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static currentSpeedLimit:F

.field private static dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private static eventAverageMpg:D

.field private static eventAverageRpm:I

.field private static eventMpgSamples:J

.field private static eventRpmSamples:I

.field private static excessiveSpeedingDuration:J

.field private static excessiveSpeedingStartTime:J

.field private static highGManeuverStartTime:J

.field private static isAboveWaterMark:Z

.field private static lastG:F

.field private static lastGAngle:F

.field private static lastGTime:J

.field private static lastHardAccelerationTime:J

.field private static lastHardBrakingTime:J

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static maxG:F

.field private static maxGAngle:F

.field private static maxRpm:I

.field private static movingStartTime:J

.field private static final rollingSpeedData:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lkotlin/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static sessionAverageMpg:D

.field private static sessionAverageRollingSpeed:F

.field private static sessionAverageSpeed:F

.field private static sessionDistance:J

.field private static sessionDuration:J

.field private static sessionHardAccelerationCount:I

.field private static sessionHardBrakingCount:I

.field private static sessionHighGCount:I

.field private static sessionMaxSpeed:F

.field private static sessionMpgSamples:J

.field private static sessionRollingDuration:J

.field private static sessionStartTime:J

.field private static sessionStarted:Z

.field private static sessionTroubleCodeCount:I

.field private static sessionTroubleCodesReport:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static speedingDuration:J

.field private static speedingStartTime:J

.field private static startDistance:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-direct {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, -0x1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lcom/navdy/hud/app/analytics/TelemetrySession;

    .end local p0    # "this":Lcom/navdy/hud/app/analytics/TelemetrySession;
    sput-object p0, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    .line 33
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->MPHToMetersPerSecond(F)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F

    .line 34
    const/high16 v0, 0x41800000    # 16.0f

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->MPHToMetersPerSecond(F)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F

    .line 35
    const v0, 0x3ee66666    # 0.45f

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->MAX_G_THRESHOLD:F

    .line 36
    const v0, 0x3e99999a    # 0.3f

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->G_LOWER_THRESHOLD_THRESHOLD:F

    .line 37
    const-wide/16 v0, 0x1f4

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HIGH_G_THRESHOLD_TIME_MILLIS:J

    .line 38
    const-wide/16 v0, 0xbb8

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS:J

    .line 50
    sput-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    .line 108
    const-string v0, ""

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionTroubleCodesReport:Ljava/lang/String;

    .line 114
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    int-to-float v1, v2

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->_currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

    .line 118
    const/16 v0, 0x1f4

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->SPEED_DATA_ROLLING_WINDOW_SIZE:I

    .line 119
    const-wide v0, 0x400c3d70a3d70a3dL    # 3.53

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND:D

    .line 120
    const-wide v0, 0x400f5a1cac083127L    # 3.919

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_BRAKING_THRESHOLD_METERS_PER_SECOND:D

    .line 121
    const/16 v0, 0x7d0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION:I

    .line 131
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    int-to-float v1, v2

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

    .line 351
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    .line 386
    sput-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    .line 389
    const/4 v0, 0x1

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    return-void
.end method

.method private final get_currentDistance()J
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->totalDistanceTravelledWithMeters()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private final get_currentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 4

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->currentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    const/4 v1, -0x1

    int-to-float v1, v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V

    goto :goto_0
.end method

.method private final setSessionAverageRollingSpeed(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 91
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageRollingSpeed:F

    return-void
.end method

.method private final setSessionAverageSpeed(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 84
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageSpeed:F

    return-void
.end method

.method private final setSessionDuration(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 326
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionDuration:J

    return-void
.end method

.method private final setSessionExcessiveSpeedingPercentage(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 105
    return-void
.end method

.method private final setSessionSpeedingPercentage(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 99
    return-void
.end method

.method private final updateValuesBasedOnSpeed()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 210
    sget-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    if-nez v3, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

    .line 214
    .local v2, "speedValue":Lcom/navdy/hud/app/analytics/RawSpeed;
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    .line 215
    .local v0, "now":J
    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v3

    const/4 v4, 0x0

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 216
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    cmp-long v3, v4, v8

    if-nez v3, :cond_2

    .line 218
    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    .line 226
    :cond_2
    :goto_1
    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v3

    sget v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMaxSpeed:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMaxSpeed:F

    .line 229
    sget v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeedLimit:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-eqz v3, :cond_7

    .line 230
    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v3

    sget v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeedLimit:F

    sget v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_5

    .line 232
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    cmp-long v3, v4, v8

    if-nez v3, :cond_3

    .line 234
    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    .line 235
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_3

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    .line 244
    :cond_3
    :goto_2
    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v3

    sget v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeedLimit:F

    sget v5, Lcom/navdy/hud/app/analytics/TelemetrySession;->EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS:F

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_6

    .line 249
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    cmp-long v3, v4, v8

    if-nez v3, :cond_0

    .line 251
    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    .line 252
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_0

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->EXCESSIVE_SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto :goto_0

    .line 221
    :cond_4
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    .line 223
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_rollingDuration:J

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    sub-long v6, v0, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_rollingDuration:J

    .line 224
    sput-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    goto :goto_1

    .line 238
    :cond_5
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_3

    .line 240
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    sub-long v6, v0, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    .line 241
    sput-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    .line 242
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_3

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto :goto_2

    .line 255
    :cond_6
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_0

    .line 257
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    sub-long v6, v0, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    .line 258
    sput-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    .line 259
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_0

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->EXCESSIVE_SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto/16 :goto_0

    .line 265
    :cond_7
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_8

    .line 266
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    sub-long v6, v0, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    .line 267
    sput-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    .line 268
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_8

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    .line 272
    :cond_8
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_0

    .line 274
    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    sub-long v6, v0, v6

    add-long/2addr v4, v6

    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    .line 275
    sput-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    .line 276
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_0

    sget-object v4, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->EXCESSIVE_SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final eventReported()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    .line 394
    const-wide/16 v0, 0x1

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    .line 396
    const/4 v0, 0x0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    .line 397
    const/4 v0, 0x1

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    .line 398
    sput v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxG:F

    .line 399
    sput v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxGAngle:F

    .line 400
    return-void
.end method

.method public final getConfirmedHighGManeuverEndTime()J
    .locals 2

    .prologue
    .line 413
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManeuverEndTime:J

    return-wide v0
.end method

.method public final getConfirmedHighGManueverNotified()Z
    .locals 1

    .prologue
    .line 414
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    return v0
.end method

.method public final getCurrentMpg()D
    .locals 2

    .prologue
    .line 52
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentMpg:D

    return-wide v0
.end method

.method public final getCurrentRpm()I
    .locals 1

    .prologue
    .line 71
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentRpm:I

    return v0
.end method

.method public final getCurrentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

    return-object v0
.end method

.method public final getCurrentSpeedLimit()F
    .locals 1

    .prologue
    .line 284
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeedLimit:F

    return v0
.end method

.method public final getCurrentTime()J
    .locals 2

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->absoluteCurrentTime()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getDataSource()Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    return-object v0
.end method

.method public final getEventAverageMpg()D
    .locals 2

    .prologue
    .line 385
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    return-wide v0
.end method

.method public final getEventAverageRpm()I
    .locals 1

    .prologue
    .line 388
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    return v0
.end method

.method public final getEventMpgSamples()J
    .locals 2

    .prologue
    .line 386
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    return-wide v0
.end method

.method public final getEventRpmSamples()I
    .locals 1

    .prologue
    .line 389
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    return v0
.end method

.method public final getExcessiveSpeedingDuration()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 314
    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    sub-long/2addr v0, v4

    :cond_0
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final getExcessiveSpeedingStartTime()J
    .locals 2

    .prologue
    .line 293
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    return-wide v0
.end method

.method public final getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND()D
    .locals 2

    .prologue
    .line 119
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND:D

    return-wide v0
.end method

.method public final getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND()D
    .locals 2

    .prologue
    .line 120
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_BRAKING_THRESHOLD_METERS_PER_SECOND:D

    return-wide v0
.end method

.method public final getHighGManeuverStartTime()J
    .locals 2

    .prologue
    .line 411
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    return-wide v0
.end method

.method public final getLastG()F
    .locals 1

    .prologue
    .line 405
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastG:F

    return v0
.end method

.method public final getLastGAngle()F
    .locals 1

    .prologue
    .line 406
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGAngle:F

    return v0
.end method

.method public final getLastGTime()J
    .locals 2

    .prologue
    .line 407
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGTime:J

    return-wide v0
.end method

.method public final getLastHardAccelerationTime()J
    .locals 2

    .prologue
    .line 122
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardAccelerationTime:J

    return-wide v0
.end method

.method public final getLastHardBrakingTime()J
    .locals 2

    .prologue
    .line 123
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardBrakingTime:J

    return-wide v0
.end method

.method public final getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION()I
    .locals 1

    .prologue
    .line 121
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION:I

    return v0
.end method

.method public final getMaxG()F
    .locals 1

    .prologue
    .line 402
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxG:F

    return v0
.end method

.method public final getMaxGAngle()F
    .locals 1

    .prologue
    .line 403
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxGAngle:F

    return v0
.end method

.method public final getMaxRpm()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxRpm:I

    return v0
.end method

.method public final getRollingSpeedData()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lkotlin/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 351
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    return-object v0
.end method

.method public final getSPEED_DATA_ROLLING_WINDOW_SIZE()I
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->SPEED_DATA_ROLLING_WINDOW_SIZE:I

    return v0
.end method

.method public final getSessionAverageMpg()D
    .locals 2

    .prologue
    .line 49
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageMpg:D

    return-wide v0
.end method

.method public final getSessionAverageRollingSpeed()F
    .locals 4

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v0

    const/4 v2, 0x0

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDistance()J

    move-result-wide v0

    long-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v2

    long-to-float v1, v2

    const/16 v2, 0x3e8

    int-to-float v2, v2

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSessionAverageSpeed()F
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDuration()J

    move-result-wide v0

    const/4 v2, 0x0

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDistance()J

    move-result-wide v0

    long-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDuration()J

    move-result-wide v2

    long-to-float v1, v2

    const/16 v2, 0x3e8

    int-to-float v2, v2

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSessionDistance()J
    .locals 4

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->get_currentDistance()J

    move-result-wide v0

    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->startDistance:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final getSessionDuration()J
    .locals 4

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStartTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final getSessionExcessiveSpeedingPercentage()F
    .locals 4

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getExcessiveSpeedingDuration()J

    move-result-wide v0

    long-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public final getSessionHardAccelerationCount()I
    .locals 1

    .prologue
    .line 125
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardAccelerationCount:I

    return v0
.end method

.method public final getSessionHardBrakingCount()I
    .locals 1

    .prologue
    .line 126
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardBrakingCount:I

    return v0
.end method

.method public final getSessionHighGCount()I
    .locals 1

    .prologue
    .line 404
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHighGCount:I

    return v0
.end method

.method public final getSessionMaxSpeed()F
    .locals 1

    .prologue
    .line 321
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMaxSpeed:F

    return v0
.end method

.method public final getSessionMpgSamples()J
    .locals 2

    .prologue
    .line 50
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    return-wide v0
.end method

.method public final getSessionRollingDuration()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 341
    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_rollingDuration:J

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    sub-long/2addr v0, v4

    :cond_0
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final getSessionSpeedingPercentage()F
    .locals 4

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSpeedingDuration()J

    move-result-wide v0

    long-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public final getSessionStarted()Z
    .locals 1

    .prologue
    .line 353
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    return v0
.end method

.method public final getSessionTroubleCodeCount()I
    .locals 1

    .prologue
    .line 107
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionTroubleCodeCount:I

    return v0
.end method

.method public final getSessionTroubleCodesReport()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionTroubleCodesReport:Ljava/lang/String;

    return-object v0
.end method

.method public final getSpeedingDuration()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 305
    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    sub-long/2addr v0, v4

    :cond_0
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final isAboveWaterMark()Z
    .locals 1

    .prologue
    .line 409
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    return v0
.end method

.method public final isDoingHighGManeuver()Z
    .locals 1

    .prologue
    .line 465
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    return v0
.end method

.method public final isExcessiveSpeeding()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 309
    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final isSpeeding()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 298
    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final setAboveWaterMark(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 409
    sput-boolean p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    return-void
.end method

.method public final setConfirmedHighGManeuverEndTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 413
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManeuverEndTime:J

    return-void
.end method

.method public final setConfirmedHighGManueverNotified(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 414
    sput-boolean p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    return-void
.end method

.method public final setCurrentMpg(D)V
    .locals 7
    .param p1, "value"    # D

    .prologue
    const/4 v6, 0x1

    .line 54
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentMpg:D

    .line 55
    const/4 v0, 0x0

    int-to-double v0, v0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    .line 57
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageMpg:D

    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageMpg:D

    sub-double v2, p1, v2

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageMpg:D

    .line 58
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    int-to-long v2, v6

    add-long/2addr v0, v2

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    .line 60
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    sget-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    sub-double v2, p1, v2

    sget-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    .line 61
    sget-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    int-to-long v2, v6

    add-long/2addr v0, v2

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    .line 63
    :cond_0
    return-void
.end method

.method public final setCurrentRpm(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 73
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentRpm:I

    .line 74
    if-lez p1, :cond_0

    .line 75
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxRpm:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxRpm:I

    .line 76
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    sget v1, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    sub-int v1, p1, v1

    sget v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    .line 77
    sget v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    .line 79
    :cond_0
    return-void
.end method

.method public final setCurrentSpeed(Lcom/navdy/hud/app/analytics/RawSpeed;)V
    .locals 18
    .param p1, "value"    # Lcom/navdy/hud/app/analytics/RawSpeed;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v14, "value"

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    sput-object p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeed:Lcom/navdy/hud/app/analytics/RawSpeed;

    .line 134
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->updateValuesBasedOnSpeed()V

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v14

    float-to-int v14, v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_1

    .line 136
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->clear()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v14, :cond_6

    invoke-interface {v14}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->isHighAccuracySpeedAvailable()Z

    move-result v14

    :goto_1
    if-eqz v14, :cond_0

    .line 142
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getTimeStamp()J

    move-result-wide v12

    .line 143
    .local v12, "timeStamp":J
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    new-instance v15, Lkotlin/Pair;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    invoke-direct/range {v15 .. v17}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v14, v15}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_0

    .line 148
    const/4 v9, 0x1

    .line 149
    .local v9, "outDatedDataExists":Z
    const/4 v7, 0x0

    check-cast v7, Lkotlin/Pair;

    .line 150
    .local v7, "lastSamplePastWindow":Lkotlin/Pair;
    :cond_2
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lkotlin/Pair;

    .line 152
    .local v8, "oldestSample":Lkotlin/Pair;
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->longValue()J

    move-result-wide v14

    sub-long v14, v12, v14

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->SPEED_DATA_ROLLING_WINDOW_SIZE:I

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-lez v14, :cond_7

    .line 153
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "lastSamplePastWindow":Lkotlin/Pair;
    check-cast v7, Lkotlin/Pair;

    .line 159
    .restart local v7    # "lastSamplePastWindow":Lkotlin/Pair;
    :cond_3
    :goto_2
    if-nez v9, :cond_2

    .line 163
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lkotlin/Pair;

    .line 164
    .local v6, "firstSampleInWindow":Lkotlin/Pair;
    invoke-virtual {v6}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->longValue()J

    move-result-wide v14

    sub-long v14, v12, v14

    long-to-double v14, v14

    const/16 v16, 0x3e8

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v10, v14, v16

    .line 165
    .local v10, "timeDifference":D
    const/high16 v14, 0x3f000000    # 0.5f

    float-to-double v14, v14

    cmpl-double v14, v10, v14

    if-ltz v14, :cond_0

    .line 166
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v15

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    cmpl-float v14, v15, v14

    if-lez v14, :cond_9

    .line 168
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v15

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    sub-float v14, v15, v14

    float-to-double v14, v14

    div-double v2, v14, v10

    .line 169
    .local v2, "acceleration":D
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v14, :cond_8

    invoke-interface {v14}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->isVerboseLoggingNeeded()Z

    move-result v14

    :goto_3
    if-eqz v14, :cond_4

    .line 170
    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "RawSpeed "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", Change is speed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v17

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    sub-float v14, v17, v14

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " Acceleration : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " Rolling data : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 172
    :cond_4
    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND:D

    cmpl-double v14, v2, v14

    if-lez v14, :cond_0

    .line 173
    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardAccelerationTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_5

    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardAccelerationTime:J

    sub-long v14, v12, v14

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION:I

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-lez v14, :cond_0

    .line 174
    :cond_5
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Hard Acceleration detected, Acceleration "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", In time :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 175
    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Current RawSpeed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", Old RawSpeed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 176
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Rolling Window : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Last G , g : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastG:F

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " , gAngle : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGAngle:F

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " , time : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-wide v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGTime:J

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 178
    sput-wide v12, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardAccelerationTime:J

    .line 179
    sget v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardAccelerationCount:I

    add-int/lit8 v14, v14, 0x1

    sput v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardAccelerationCount:I

    .line 180
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v14, :cond_0

    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_ACCELERATION:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v14, v15}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto/16 :goto_0

    .line 139
    .end local v2    # "acceleration":D
    .end local v6    # "firstSampleInWindow":Lkotlin/Pair;
    .end local v7    # "lastSamplePastWindow":Lkotlin/Pair;
    .end local v8    # "oldestSample":Lkotlin/Pair;
    .end local v9    # "outDatedDataExists":Z
    .end local v10    # "timeDifference":D
    .end local v12    # "timeStamp":J
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 155
    .restart local v7    # "lastSamplePastWindow":Lkotlin/Pair;
    .restart local v8    # "oldestSample":Lkotlin/Pair;
    .restart local v9    # "outDatedDataExists":Z
    .restart local v12    # "timeStamp":J
    :cond_7
    const/4 v9, 0x0

    .line 156
    if-eqz v7, :cond_3

    .line 157
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v14, v7}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 169
    .restart local v2    # "acceleration":D
    .restart local v6    # "firstSampleInWindow":Lkotlin/Pair;
    .restart local v10    # "timeDifference":D
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 185
    .end local v2    # "acceleration":D
    :cond_9
    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v14, v14

    div-double v4, v14, v10

    .line 186
    .local v4, "deceleration":D
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v14, :cond_c

    invoke-interface {v14}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->isVerboseLoggingNeeded()Z

    move-result v14

    :goto_4
    if-eqz v14, :cond_a

    .line 187
    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "RawSpeed "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", Change is speed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/RawSpeed;->getSpeed()F

    move-result v17

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    sub-float v14, v17, v14

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " Braking : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " Rolling data : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 189
    :cond_a
    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->HARD_BRAKING_THRESHOLD_METERS_PER_SECOND:D

    cmpl-double v14, v4, v14

    if-lez v14, :cond_0

    .line 190
    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardBrakingTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_b

    sget-wide v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardBrakingTime:J

    sub-long v14, v12, v14

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION:I

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-lez v14, :cond_0

    .line 191
    :cond_b
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Hard Deceleration detected, Acceleration "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", In time :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 192
    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Current RawSpeed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", Old RawSpeed : "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Number;

    invoke-virtual {v14}, Ljava/lang/Number;->floatValue()F

    move-result v14

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 193
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Rolling Window : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 194
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Last G , g : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastG:F

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " , gAngle : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGAngle:F

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " , time : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-wide v16, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGTime:J

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 195
    sput-wide v12, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardBrakingTime:J

    .line 196
    sget v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardBrakingCount:I

    add-int/lit8 v14, v14, 0x1

    sput v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardBrakingCount:I

    .line 197
    sget-object v14, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v14, :cond_0

    sget-object v15, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_BRAKING:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v14, v15}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    goto/16 :goto_0

    .line 186
    :cond_c
    const/4 v14, 0x0

    goto/16 :goto_4
.end method

.method public final setCurrentSpeedLimit(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 286
    const/4 v0, 0x0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .end local p1    # "value":F
    :goto_0
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->currentSpeedLimit:F

    .line 288
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->updateValuesBasedOnSpeed()V

    .line 289
    return-void

    .line 286
    .restart local p1    # "value":F
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final setDataSource(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V
    .locals 0
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 40
    sput-object p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    return-void
.end method

.method public final setDoingHighGManeuver(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 466
    return-void
.end method

.method public final setEventAverageMpg(D)V
    .locals 1
    .param p1, "<set-?>"    # D

    .prologue
    .line 385
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageMpg:D

    return-void
.end method

.method public final setEventAverageRpm(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 388
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventAverageRpm:I

    return-void
.end method

.method public final setEventMpgSamples(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 386
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventMpgSamples:J

    return-void
.end method

.method public final setEventRpmSamples(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 389
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->eventRpmSamples:I

    return-void
.end method

.method public final setExcessiveSpeeding(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 310
    return-void
.end method

.method public final setExcessiveSpeedingDuration(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 312
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingDuration:J

    return-void
.end method

.method public final setExcessiveSpeedingStartTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 293
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    return-void
.end method

.method public final setGForce(FFF)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 416
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v3, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 417
    .local v0, "g":F
    neg-float v3, p2

    div-float/2addr v3, p1

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const/high16 v3, 0x43340000    # 180.0f

    float-to-double v8, v3

    mul-double/2addr v6, v8

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v6, v8

    double-to-float v1, v6

    .line 418
    .local v1, "gAngle":F
    const/4 v3, 0x0

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    .line 419
    const/high16 v3, 0x43340000    # 180.0f

    add-float/2addr v1, v3

    .line 421
    :cond_0
    const/16 v3, 0x168

    int-to-float v3, v3

    add-float/2addr v3, v1

    const/16 v6, 0x168

    int-to-float v6, v6

    rem-float v1, v3, v6

    .line 423
    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastG:F

    .line 424
    sput v1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGAngle:F

    .line 425
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v4

    .line 426
    .local v4, "now":J
    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGTime:J

    .line 428
    sget v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxG:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    .line 429
    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxG:F

    .line 430
    sput v1, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxGAngle:F

    .line 432
    :cond_1
    sget v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->MAX_G_THRESHOLD:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_b

    .line 433
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "G "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , Angle "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 434
    const/high16 v3, 0x42fa0000    # 125.0f

    cmpg-float v3, v3, v1

    if-gtz v3, :cond_7

    const/high16 v3, 0x436b0000    # 235.0f

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_7

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_2

    const v3, 0x43988000    # 305.0f

    cmpg-float v3, v3, v1

    if-gtz v3, :cond_8

    const/high16 v3, 0x43b40000    # 360.0f

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_8

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_2

    const/4 v3, 0x0

    cmpg-float v3, v3, v1

    if-gtz v3, :cond_9

    const/high16 v3, 0x425c0000    # 55.0f

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_9

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_a

    :cond_2
    const/4 v2, 0x1

    .line 435
    .local v2, "highGAngle":Z
    :goto_3
    if-eqz v2, :cond_6

    .line 436
    sget-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    if-nez v3, :cond_3

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManeuverEndTime:J

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->timeSince(JJ)J

    move-result-wide v6

    sget-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    .line 437
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "High G maneuver started "

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 438
    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    .line 440
    :cond_3
    sget-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    if-nez v3, :cond_5

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_5

    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/hud/app/analytics/TelemetrySessionKt;->timeSince(JJ)J

    move-result-wide v6

    sget-wide v8, Lcom/navdy/hud/app/analytics/TelemetrySession;->HIGH_G_THRESHOLD_TIME_MILLIS:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_5

    .line 441
    sget v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHighGCount:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHighGCount:I

    .line 442
    const/4 v3, 0x1

    sput-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    .line 443
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_4

    sget-object v6, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HIGH_G_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v6}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    .line 444
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "High G maneuver confirmed and notified"

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 446
    :cond_5
    const/4 v3, 0x1

    sput-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    .line 461
    .end local v2    # "highGAngle":Z
    :cond_6
    :goto_4
    return-void

    .line 434
    :cond_7
    const/4 v3, 0x0

    goto :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_1

    :cond_9
    const/4 v3, 0x0

    goto :goto_2

    :cond_a
    const/4 v2, 0x0

    goto :goto_3

    .line 449
    :cond_b
    sget v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->G_LOWER_THRESHOLD_THRESHOLD:F

    cmpg-float v3, v0, v3

    if-gez v3, :cond_6

    .line 450
    sget-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_c

    .line 451
    const-wide/16 v6, 0x0

    sput-wide v6, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    .line 452
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "High G maneuver ended"

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 453
    sget-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    if-eqz v3, :cond_c

    .line 454
    sput-wide v4, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManeuverEndTime:J

    .line 455
    const/4 v3, 0x0

    sput-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->confirmedHighGManueverNotified:Z

    .line 456
    sget-object v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->dataSource:Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;

    if-eqz v3, :cond_c

    sget-object v6, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HIGH_G_ENDED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-interface {v3, v6}, Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;->interestingEventDetected(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V

    .line 459
    :cond_c
    const/4 v3, 0x0

    sput-boolean v3, Lcom/navdy/hud/app/analytics/TelemetrySession;->isAboveWaterMark:Z

    goto :goto_4
.end method

.method public final setHighGManeuverStartTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 411
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->highGManeuverStartTime:J

    return-void
.end method

.method public final setLastG(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 405
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastG:F

    return-void
.end method

.method public final setLastGAngle(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 406
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGAngle:F

    return-void
.end method

.method public final setLastGTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 407
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastGTime:J

    return-void
.end method

.method public final setLastHardAccelerationTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 122
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardAccelerationTime:J

    return-void
.end method

.method public final setLastHardBrakingTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 123
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->lastHardBrakingTime:J

    return-void
.end method

.method public final setMaxG(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 402
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxG:F

    return-void
.end method

.method public final setMaxGAngle(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 403
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxGAngle:F

    return-void
.end method

.method public final setMaxRpm(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 70
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->maxRpm:I

    return-void
.end method

.method public final setSessionAverageMpg(D)V
    .locals 1
    .param p1, "<set-?>"    # D

    .prologue
    .line 49
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionAverageMpg:D

    return-void
.end method

.method public final setSessionDistance(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 347
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionDistance:J

    return-void
.end method

.method public final setSessionHardAccelerationCount(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 125
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardAccelerationCount:I

    return-void
.end method

.method public final setSessionHardBrakingCount(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 126
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHardBrakingCount:I

    return-void
.end method

.method public final setSessionHighGCount(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 404
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHighGCount:I

    return-void
.end method

.method public final setSessionMaxSpeed(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 321
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMaxSpeed:F

    return-void
.end method

.method public final setSessionMpgSamples(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 50
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMpgSamples:J

    return-void
.end method

.method public final setSessionRollingDuration(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 339
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionRollingDuration:J

    return-void
.end method

.method public final setSessionStarted(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 353
    sput-boolean p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    return-void
.end method

.method public final setSessionTroubleCodeCount(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 107
    sput p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionTroubleCodeCount:I

    return-void
.end method

.method public final setSessionTroubleCodesReport(Ljava/lang/String;)V
    .locals 1
    .param p1, "<set-?>"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    sput-object p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionTroubleCodesReport:Ljava/lang/String;

    return-void
.end method

.method public final setSpeeding(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 299
    return-void
.end method

.method public final setSpeedingDuration(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 303
    sput-wide p1, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingDuration:J

    return-void
.end method

.method public final speedSourceChanged()V
    .locals 1

    .prologue
    .line 205
    sget-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    if-eqz v0, :cond_0

    .line 206
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 208
    :cond_0
    return-void
.end method

.method public final startSession()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 359
    invoke-virtual {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getCurrentTime()J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStartTime:J

    .line 360
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionDuration:J

    .line 361
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->get_currentDistance()J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->startDistance:J

    .line 362
    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->rollingSpeedData:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 363
    invoke-direct {p0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->get_currentSpeed()Lcom/navdy/hud/app/analytics/RawSpeed;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/analytics/TelemetrySession;->setCurrentSpeed(Lcom/navdy/hud/app/analytics/RawSpeed;)V

    .line 364
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionRollingDuration:J

    .line 365
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_speedingDuration:J

    .line 366
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_rollingDuration:J

    .line 367
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->_excessiveSpeedingDuration:J

    .line 368
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->excessiveSpeedingStartTime:J

    .line 369
    const/4 v0, 0x0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionMaxSpeed:F

    .line 370
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->movingStartTime:J

    .line 371
    sput-wide v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->speedingStartTime:J

    .line 372
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    .line 373
    const/4 v0, 0x0

    sput v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionHighGCount:I

    .line 375
    return-void
.end method

.method public final stopSession()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/hud/app/analytics/TelemetrySession;->sessionStarted:Z

    .line 379
    return-void
.end method
