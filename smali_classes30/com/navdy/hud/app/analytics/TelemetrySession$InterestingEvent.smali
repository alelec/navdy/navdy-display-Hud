.class public final enum Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
.super Ljava/lang/Enum;
.source "TelemetrySession.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/TelemetrySession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InterestingEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;",
        "",
        "(Ljava/lang/String;I)V",
        "NONE",
        "HARD_BRAKING",
        "HARD_ACCELERATION",
        "HIGH_G_STARTED",
        "HIGH_G_ENDED",
        "SPEEDING_STARTED",
        "SPEEDING_STOPPED",
        "EXCESSIVE_SPEEDING_STARTED",
        "EXCESSIVE_SPEEDING_STOPPED",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum EXCESSIVE_SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum EXCESSIVE_SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum HARD_ACCELERATION:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum HARD_BRAKING:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum HIGH_G_ENDED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum HIGH_G_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

.field public static final enum SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    new-instance v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v2, "NONE"

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v2, "HARD_BRAKING"

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_BRAKING:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v1, v0, v4

    new-instance v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v2, "HARD_ACCELERATION"

    invoke-direct {v1, v2, v5}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_ACCELERATION:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v1, v0, v5

    new-instance v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v2, "HIGH_G_STARTED"

    invoke-direct {v1, v2, v6}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HIGH_G_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v1, v0, v6

    new-instance v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v2, "HIGH_G_ENDED"

    invoke-direct {v1, v2, v7}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HIGH_G_ENDED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v3, "SPEEDING_STARTED"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v3, "SPEEDING_STOPPED"

    const/4 v4, 0x6

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v3, "EXCESSIVE_SPEEDING_STARTED"

    const/4 v4, 0x7

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->EXCESSIVE_SPEEDING_STARTED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const-string v3, "EXCESSIVE_SPEEDING_STOPPED"

    const/16 v4, 0x8

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->EXCESSIVE_SPEEDING_STOPPED:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->$VALUES:[Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "$enum_name_or_ordinal$0"    # Ljava/lang/String;
    .param p2, "$enum_name_or_ordinal$1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
    .locals 1

    const-class v0, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;
    .locals 1

    sget-object v0, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->$VALUES:[Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    return-object v0
.end method
