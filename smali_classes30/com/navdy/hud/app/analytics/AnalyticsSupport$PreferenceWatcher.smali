.class Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;
.super Ljava/lang/Object;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PreferenceWatcher"
.end annotation


# instance fields
.field adapter:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;

.field oldState:Lcom/navdy/hud/app/analytics/Event;

.field queuedUpdate:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;)V
    .locals 1
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p2, "adapter"    # Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;

    .prologue
    .line 874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 875
    invoke-interface {p2, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;->calculateState(Lcom/navdy/hud/app/profile/DriverProfile;)Lcom/navdy/hud/app/analytics/Event;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->oldState:Lcom/navdy/hud/app/analytics/Event;

    .line 876
    iput-object p2, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->adapter:Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceAdapter;

    .line 877
    return-void
.end method


# virtual methods
.method enqueue(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 4
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 880
    iget-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->queuedUpdate:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 881
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->queuedUpdate:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 883
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher$1;-><init>(Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;Lcom/navdy/hud/app/profile/DriverProfile;)V

    iput-object v0, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->queuedUpdate:Ljava/lang/Runnable;

    .line 898
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$200()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$PreferenceWatcher;->queuedUpdate:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 899
    return-void
.end method
