.class final Lcom/navdy/hud/app/analytics/AnalyticsSupport$2;
.super Landroid/content/BroadcastReceiver;
.source "AnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/analytics/AnalyticsSupport;->analyticsApplicationInit(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1203
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1206
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1207
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1208
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1209
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_2

    .line 1210
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 1211
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 1212
    :cond_0
    const-string v3, "android.bluetooth.device.extra.STATUS"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I
    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3102(I)I

    .line 1213
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_ACL_DISCONNECTED, Disconnect reason "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastDiscReason:I
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3100()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1224
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_1
    :goto_0
    return-void

    .line 1215
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    const-string v3, "android.bluetooth.device.extra.STATUS"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1216
    .local v2, "disconnectReason":I
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_ACL_DISCONNECTED, Non classic device , Disconnect reason "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1218
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "disconnectReason":I
    :cond_3
    const-string v3, "com.navdy.hud.app.force_reconnect"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1219
    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->disconnectDueToForceReconnect:Z
    invoke-static {v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3202(Z)Z

    .line 1220
    const-string v3, "force_reconnect_reason"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3302(Ljava/lang/String;)Ljava/lang/String;

    .line 1221
    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_DEVICE_FORCE_RECONNECT, Reason : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/navdy/hud/app/analytics/AnalyticsSupport;->lastForceReconnectReason:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->access$3300()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
