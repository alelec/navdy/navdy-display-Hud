.class public final Lcom/navdy/hud/app/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final accel:I = 0x7f020000

.field public static final activity_label_scrim:I = 0x7f020001

.field public static final app_store_badges:I = 0x7f020002

.field public static final app_store_de:I = 0x7f020003

.field public static final app_store_eng:I = 0x7f020004

.field public static final app_store_es:I = 0x7f020005

.field public static final app_store_fr:I = 0x7f020006

.field public static final app_store_it:I = 0x7f020007

.field public static final asset_active_trip_map_mask:I = 0x7f020008

.field public static final asset_dash20_proto_gauge_clock_analog:I = 0x7f020009

.field public static final asset_dash20_proto_gauge_mpg:I = 0x7f02000a

.field public static final asset_dash20_proto_gauge_weather:I = 0x7f02000b

.field public static final asset_mask_picker_map:I = 0x7f02000c

.field public static final asset_tachometer_background:I = 0x7f02000d

.field public static final audio_loop:I = 0x7f02000e

.field public static final audio_loop_sm:I = 0x7f02000f

.field public static final audio_seq_10:I = 0x7f020010

.field public static final audio_seq_10_sm:I = 0x7f020011

.field public static final audio_seq_11:I = 0x7f020012

.field public static final audio_seq_11_sm:I = 0x7f020013

.field public static final audio_seq_12:I = 0x7f020014

.field public static final audio_seq_12_sm:I = 0x7f020015

.field public static final audio_seq_13:I = 0x7f020016

.field public static final audio_seq_13_sm:I = 0x7f020017

.field public static final audio_seq_14:I = 0x7f020018

.field public static final audio_seq_14_sm:I = 0x7f020019

.field public static final audio_seq_15:I = 0x7f02001a

.field public static final audio_seq_15_sm:I = 0x7f02001b

.field public static final audio_seq_16:I = 0x7f02001c

.field public static final audio_seq_16_sm:I = 0x7f02001d

.field public static final audio_seq_17:I = 0x7f02001e

.field public static final audio_seq_17_sm:I = 0x7f02001f

.field public static final audio_seq_18:I = 0x7f020020

.field public static final audio_seq_18_sm:I = 0x7f020021

.field public static final audio_seq_19:I = 0x7f020022

.field public static final audio_seq_19_sm:I = 0x7f020023

.field public static final audio_seq_20:I = 0x7f020024

.field public static final audio_seq_20_sm:I = 0x7f020025

.field public static final audio_seq_21:I = 0x7f020026

.field public static final audio_seq_21_sm:I = 0x7f020027

.field public static final audio_seq_22:I = 0x7f020028

.field public static final audio_seq_22_sm:I = 0x7f020029

.field public static final audio_seq_23:I = 0x7f02002a

.field public static final audio_seq_23_sm:I = 0x7f02002b

.field public static final audio_seq_24:I = 0x7f02002c

.field public static final audio_seq_24_sm:I = 0x7f02002d

.field public static final audio_seq_25:I = 0x7f02002e

.field public static final audio_seq_25_sm:I = 0x7f02002f

.field public static final audio_seq_26:I = 0x7f020030

.field public static final audio_seq_26_sm:I = 0x7f020031

.field public static final audio_seq_27:I = 0x7f020032

.field public static final audio_seq_27_sm:I = 0x7f020033

.field public static final audio_seq_28:I = 0x7f020034

.field public static final audio_seq_28_sm:I = 0x7f020035

.field public static final audio_seq_29:I = 0x7f020036

.field public static final audio_seq_29_sm:I = 0x7f020037

.field public static final audio_seq_30:I = 0x7f020038

.field public static final audio_seq_30_sm:I = 0x7f020039

.field public static final audio_seq_31:I = 0x7f02003a

.field public static final audio_seq_31_sm:I = 0x7f02003b

.field public static final audio_seq_32:I = 0x7f02003c

.field public static final audio_seq_32_sm:I = 0x7f02003d

.field public static final audio_seq_33:I = 0x7f02003e

.field public static final audio_seq_33_sm:I = 0x7f02003f

.field public static final audio_seq_34:I = 0x7f020040

.field public static final audio_seq_34_sm:I = 0x7f020041

.field public static final audio_seq_35:I = 0x7f020042

.field public static final audio_seq_35_sm:I = 0x7f020043

.field public static final audio_seq_36:I = 0x7f020044

.field public static final audio_seq_36_sm:I = 0x7f020045

.field public static final audio_seq_37:I = 0x7f020046

.field public static final audio_seq_37_sm:I = 0x7f020047

.field public static final audio_seq_38:I = 0x7f020048

.field public static final audio_seq_38_sm:I = 0x7f020049

.field public static final audio_seq_39:I = 0x7f02004a

.field public static final audio_seq_39_sm:I = 0x7f02004b

.field public static final audio_seq_40:I = 0x7f02004c

.field public static final audio_seq_40_sm:I = 0x7f02004d

.field public static final audio_seq_41:I = 0x7f02004e

.field public static final audio_seq_41_sm:I = 0x7f02004f

.field public static final audio_seq_42:I = 0x7f020050

.field public static final audio_seq_42_sm:I = 0x7f020051

.field public static final audio_seq_43:I = 0x7f020052

.field public static final audio_seq_43_sm:I = 0x7f020053

.field public static final audio_seq_44:I = 0x7f020054

.field public static final audio_seq_44_sm:I = 0x7f020055

.field public static final audio_seq_45:I = 0x7f020056

.field public static final audio_seq_45_sm:I = 0x7f020057

.field public static final audio_seq_46:I = 0x7f020058

.field public static final audio_seq_46_sm:I = 0x7f020059

.field public static final audio_seq_47:I = 0x7f02005a

.field public static final audio_seq_47_sm:I = 0x7f02005b

.field public static final audio_seq_48:I = 0x7f02005c

.field public static final audio_seq_48_sm:I = 0x7f02005d

.field public static final audio_seq_49:I = 0x7f02005e

.field public static final audio_seq_49_sm:I = 0x7f02005f

.field public static final audio_seq_50:I = 0x7f020060

.field public static final audio_seq_50_sm:I = 0x7f020061

.field public static final audio_seq_51:I = 0x7f020062

.field public static final audio_seq_51_sm:I = 0x7f020063

.field public static final audio_seq_52:I = 0x7f020064

.field public static final audio_seq_52_sm:I = 0x7f020065

.field public static final audio_seq_53:I = 0x7f020066

.field public static final audio_seq_53_sm:I = 0x7f020067

.field public static final audio_seq_54:I = 0x7f020068

.field public static final audio_seq_54_sm:I = 0x7f020069

.field public static final audio_seq_55:I = 0x7f02006a

.field public static final audio_seq_55_sm:I = 0x7f02006b

.field public static final audio_seq_56:I = 0x7f02006c

.field public static final audio_seq_56_sm:I = 0x7f02006d

.field public static final audio_seq_57:I = 0x7f02006e

.field public static final audio_seq_57_sm:I = 0x7f02006f

.field public static final audio_seq_58:I = 0x7f020070

.field public static final audio_seq_58_sm:I = 0x7f020071

.field public static final audio_seq_59:I = 0x7f020072

.field public static final audio_seq_59_sm:I = 0x7f020073

.field public static final audio_seq_60:I = 0x7f020074

.field public static final audio_seq_60_sm:I = 0x7f020075

.field public static final audio_seq_61:I = 0x7f020076

.field public static final audio_seq_61_sm:I = 0x7f020077

.field public static final audio_seq_62:I = 0x7f020078

.field public static final audio_seq_62_sm:I = 0x7f020079

.field public static final audio_seq_63:I = 0x7f02007a

.field public static final audio_seq_63_sm:I = 0x7f02007b

.field public static final audio_seq_64:I = 0x7f02007c

.field public static final audio_seq_64_sm:I = 0x7f02007d

.field public static final audio_seq_65:I = 0x7f02007e

.field public static final audio_seq_65_sm:I = 0x7f02007f

.field public static final audio_seq_66:I = 0x7f020080

.field public static final audio_seq_66_sm:I = 0x7f020081

.field public static final audio_seq_8:I = 0x7f020082

.field public static final audio_seq_8_sm:I = 0x7f020083

.field public static final audio_seq_9:I = 0x7f020084

.field public static final audio_seq_9_sm:I = 0x7f020085

.field public static final bg_wru:I = 0x7f020086

.field public static final border_main:I = 0x7f020087

.field public static final brake:I = 0x7f020088

.field public static final choice_highlight:I = 0x7f020089

.field public static final circle:I = 0x7f02008a

.field public static final circle_page_indicator:I = 0x7f02008b

.field public static final circle_page_indicator_focus:I = 0x7f02008c

.field public static final content_loading_loop:I = 0x7f02008d

.field public static final content_loading_sel_0:I = 0x7f02008e

.field public static final content_loading_sel_1:I = 0x7f02008f

.field public static final content_loading_sel_10:I = 0x7f020090

.field public static final content_loading_sel_11:I = 0x7f020091

.field public static final content_loading_sel_12:I = 0x7f020092

.field public static final content_loading_sel_13:I = 0x7f020093

.field public static final content_loading_sel_14:I = 0x7f020094

.field public static final content_loading_sel_15:I = 0x7f020095

.field public static final content_loading_sel_16:I = 0x7f020096

.field public static final content_loading_sel_17:I = 0x7f020097

.field public static final content_loading_sel_18:I = 0x7f020098

.field public static final content_loading_sel_19:I = 0x7f020099

.field public static final content_loading_sel_2:I = 0x7f02009a

.field public static final content_loading_sel_20:I = 0x7f02009b

.field public static final content_loading_sel_21:I = 0x7f02009c

.field public static final content_loading_sel_22:I = 0x7f02009d

.field public static final content_loading_sel_23:I = 0x7f02009e

.field public static final content_loading_sel_24:I = 0x7f02009f

.field public static final content_loading_sel_25:I = 0x7f0200a0

.field public static final content_loading_sel_26:I = 0x7f0200a1

.field public static final content_loading_sel_27:I = 0x7f0200a2

.field public static final content_loading_sel_28:I = 0x7f0200a3

.field public static final content_loading_sel_29:I = 0x7f0200a4

.field public static final content_loading_sel_3:I = 0x7f0200a5

.field public static final content_loading_sel_30:I = 0x7f0200a6

.field public static final content_loading_sel_31:I = 0x7f0200a7

.field public static final content_loading_sel_32:I = 0x7f0200a8

.field public static final content_loading_sel_33:I = 0x7f0200a9

.field public static final content_loading_sel_34:I = 0x7f0200aa

.field public static final content_loading_sel_35:I = 0x7f0200ab

.field public static final content_loading_sel_36:I = 0x7f0200ac

.field public static final content_loading_sel_37:I = 0x7f0200ad

.field public static final content_loading_sel_4:I = 0x7f0200ae

.field public static final content_loading_sel_5:I = 0x7f0200af

.field public static final content_loading_sel_6:I = 0x7f0200b0

.field public static final content_loading_sel_7:I = 0x7f0200b1

.field public static final content_loading_sel_8:I = 0x7f0200b2

.field public static final content_loading_sel_9:I = 0x7f0200b3

.field public static final content_loading_unsel:I = 0x7f0200b4

.field public static final dial_update_progress:I = 0x7f0200b5

.field public static final digital_clock_date_background:I = 0x7f0200b6

.field public static final digital_clock_day_background:I = 0x7f0200b7

.field public static final excessive_speed:I = 0x7f0200b8

.field public static final first_launch_logo:I = 0x7f0200b9

.field public static final gauge_scrim:I = 0x7f0200ba

.field public static final gauge_scrim_bottom:I = 0x7f0200bb

.field public static final glogo_v2:I = 0x7f0200bc

.field public static final google_play_de:I = 0x7f0200bd

.field public static final google_play_eng:I = 0x7f0200be

.field public static final google_play_es:I = 0x7f0200bf

.field public static final google_play_fr:I = 0x7f0200c0

.field public static final google_play_it:I = 0x7f0200c1

.field public static final hand:I = 0x7f0200c2

.field public static final hand_selected:I = 0x7f0200c3

.field public static final here_pos_indicator:I = 0x7f0200c4

.field public static final high_g:I = 0x7f0200c5

.field public static final hockeyapp_btn_background:I = 0x7f0200c6

.field public static final ic_launcher:I = 0x7f0200c7

.field public static final icon_active_nav_end_2:I = 0x7f0200c8

.field public static final icon_active_nav_mute_2:I = 0x7f0200c9

.field public static final icon_active_nav_unmute_2:I = 0x7f0200ca

.field public static final icon_add:I = 0x7f0200cb

.field public static final icon_add_driver:I = 0x7f0200cc

.field public static final icon_album:I = 0x7f0200cd

.field public static final icon_alert_dial:I = 0x7f0200ce

.field public static final icon_alert_phone_connected:I = 0x7f0200cf

.field public static final icon_alert_sensor_blocked:I = 0x7f0200d0

.field public static final icon_apple_music:I = 0x7f0200d1

.field public static final icon_arrow:I = 0x7f0200d2

.field public static final icon_arrow_tank_left:I = 0x7f0200d3

.field public static final icon_arrow_tank_right:I = 0x7f0200d4

.field public static final icon_artist:I = 0x7f0200d5

.field public static final icon_bad_map_info:I = 0x7f0200d6

.field public static final icon_bad_route:I = 0x7f0200d7

.field public static final icon_badge_active_trip:I = 0x7f0200d8

.field public static final icon_badge_alert:I = 0x7f0200d9

.field public static final icon_badge_b_t:I = 0x7f0200da

.field public static final icon_badge_background:I = 0x7f0200db

.field public static final icon_badge_bt_tips:I = 0x7f0200dc

.field public static final icon_badge_call:I = 0x7f0200dd

.field public static final icon_badge_callend:I = 0x7f0200de

.field public static final icon_badge_car:I = 0x7f0200df

.field public static final icon_badge_coffee:I = 0x7f0200e0

.field public static final icon_badge_gas:I = 0x7f0200e1

.field public static final icon_badge_phone:I = 0x7f0200e2

.field public static final icon_badge_phone_source:I = 0x7f0200e3

.field public static final icon_badge_route:I = 0x7f0200e4

.field public static final icon_badge_store:I = 0x7f0200e5

.field public static final icon_badge_user_m:I = 0x7f0200e6

.field public static final icon_badge_voice_search:I = 0x7f0200e7

.field public static final icon_bluetooth_connecting:I = 0x7f0200e8

.field public static final icon_bookmark:I = 0x7f0200e9

.field public static final icon_brightness_relative:I = 0x7f0200ea

.field public static final icon_brightness_relative_decr:I = 0x7f0200eb

.field public static final icon_brightness_relative_incr:I = 0x7f0200ec

.field public static final icon_brightness_relative_minus:I = 0x7f0200ed

.field public static final icon_brightness_relative_plus:I = 0x7f0200ee

.field public static final icon_call:I = 0x7f0200ef

.field public static final icon_call_ended:I = 0x7f0200f0

.field public static final icon_call_green:I = 0x7f0200f1

.field public static final icon_call_muted:I = 0x7f0200f2

.field public static final icon_call_red:I = 0x7f0200f3

.field public static final icon_car_alert:I = 0x7f0200f4

.field public static final icon_center_gauge:I = 0x7f0200f5

.field public static final icon_connection_failed:I = 0x7f0200f6

.field public static final icon_destination_pin_unselected:I = 0x7f0200f7

.field public static final icon_device_battery_ok:I = 0x7f0200f8

.field public static final icon_device_batterylow_2_copy:I = 0x7f0200f9

.field public static final icon_device_batterylow_copy:I = 0x7f0200fa

.field public static final icon_device_missing_copy:I = 0x7f0200fb

.field public static final icon_dial_2:I = 0x7f0200fc

.field public static final icon_dial_batterylow_2_copy:I = 0x7f0200fd

.field public static final icon_dial_batterylow_copy:I = 0x7f0200fe

.field public static final icon_dial_forgotten:I = 0x7f0200ff

.field public static final icon_dial_missing_copy:I = 0x7f020100

.field public static final icon_dial_pair_failed:I = 0x7f020101

.field public static final icon_dial_paired:I = 0x7f020102

.field public static final icon_dial_update:I = 0x7f020103

.field public static final icon_dial_update_2:I = 0x7f020104

.field public static final icon_disabled:I = 0x7f020105

.field public static final icon_dismiss:I = 0x7f020106

.field public static final icon_dismiss_all_glances:I = 0x7f020107

.field public static final icon_driver_position:I = 0x7f020108

.field public static final icon_end_call:I = 0x7f020109

.field public static final icon_engine_temperature:I = 0x7f02010a

.field public static final icon_eta:I = 0x7f02010b

.field public static final icon_gauge_compass_heading_indicator:I = 0x7f02010c

.field public static final icon_gauge_eta_pin:I = 0x7f02010d

.field public static final icon_gauge_fuel_gas:I = 0x7f02010e

.field public static final icon_gauge_fuel_low:I = 0x7f02010f

.field public static final icon_gauge_music_blank:I = 0x7f020110

.field public static final icon_gesture_swipe_left:I = 0x7f020111

.field public static final icon_gesture_swipe_right:I = 0x7f020112

.field public static final icon_glance_calendar:I = 0x7f020113

.field public static final icon_glance_calendar_location_pin:I = 0x7f020114

.field public static final icon_glance_email:I = 0x7f020115

.field public static final icon_glance_facebook:I = 0x7f020116

.field public static final icon_glance_facebook_messenger:I = 0x7f020117

.field public static final icon_glance_fuel_low:I = 0x7f020118

.field public static final icon_glance_generic:I = 0x7f020119

.field public static final icon_glance_gmail:I = 0x7f02011a

.field public static final icon_glance_google_calendar:I = 0x7f02011b

.field public static final icon_glance_google_inbox:I = 0x7f02011c

.field public static final icon_glance_hangout:I = 0x7f02011d

.field public static final icon_glance_large_generic:I = 0x7f02011e

.field public static final icon_glance_slack:I = 0x7f02011f

.field public static final icon_glance_twitter:I = 0x7f020120

.field public static final icon_glance_whatsapp:I = 0x7f020121

.field public static final icon_glances_add:I = 0x7f020122

.field public static final icon_glances_back:I = 0x7f020123

.field public static final icon_glances_disabled_modal:I = 0x7f020124

.field public static final icon_glances_dismiss:I = 0x7f020125

.field public static final icon_glances_endcall:I = 0x7f020126

.field public static final icon_glances_mute:I = 0x7f020127

.field public static final icon_glances_ok:I = 0x7f020128

.field public static final icon_glances_ok_strong:I = 0x7f020129

.field public static final icon_glances_read:I = 0x7f02012a

.field public static final icon_glances_reply:I = 0x7f02012b

.field public static final icon_glances_retry:I = 0x7f02012c

.field public static final icon_glances_unmute:I = 0x7f02012d

.field public static final icon_go:I = 0x7f02012e

.field public static final icon_google_pm:I = 0x7f02012f

.field public static final icon_googlenow_off:I = 0x7f020130

.field public static final icon_googlenow_on:I = 0x7f020131

.field public static final icon_gpserror:I = 0x7f020132

.field public static final icon_heart:I = 0x7f020133

.field public static final icon_lg_compboth_heavy_left:I = 0x7f020134

.field public static final icon_lg_compboth_heavy_right:I = 0x7f020135

.field public static final icon_lg_compboth_left:I = 0x7f020136

.field public static final icon_lg_compboth_rec_heavy_left:I = 0x7f020137

.field public static final icon_lg_compboth_rec_heavy_right:I = 0x7f020138

.field public static final icon_lg_compboth_rec_left:I = 0x7f020139

.field public static final icon_lg_compboth_rec_right:I = 0x7f02013a

.field public static final icon_lg_compboth_rec_slight_left:I = 0x7f02013b

.field public static final icon_lg_compboth_rec_slight_right:I = 0x7f02013c

.field public static final icon_lg_compboth_rec_straight:I = 0x7f02013d

.field public static final icon_lg_compboth_right:I = 0x7f02013e

.field public static final icon_lg_compboth_slight_left:I = 0x7f02013f

.field public static final icon_lg_compboth_slight_right:I = 0x7f020140

.field public static final icon_lg_compboth_straight:I = 0x7f020141

.field public static final icon_lg_compleft_heavy_left:I = 0x7f020142

.field public static final icon_lg_compleft_left:I = 0x7f020143

.field public static final icon_lg_compleft_rec_heavy_left:I = 0x7f020144

.field public static final icon_lg_compleft_rec_left:I = 0x7f020145

.field public static final icon_lg_compleft_rec_slight_left:I = 0x7f020146

.field public static final icon_lg_compleft_rec_straight:I = 0x7f020147

.field public static final icon_lg_compleft_rec_uturn_left:I = 0x7f020148

.field public static final icon_lg_compleft_slight_left:I = 0x7f020149

.field public static final icon_lg_compleft_straight:I = 0x7f02014a

.field public static final icon_lg_compleft_uturn_left:I = 0x7f02014b

.field public static final icon_lg_compright_heavy_right:I = 0x7f02014c

.field public static final icon_lg_compright_rec_heavy_right:I = 0x7f02014d

.field public static final icon_lg_compright_rec_right:I = 0x7f02014e

.field public static final icon_lg_compright_rec_slight_right:I = 0x7f02014f

.field public static final icon_lg_compright_rec_straight:I = 0x7f020150

.field public static final icon_lg_compright_rec_uturn_right:I = 0x7f020151

.field public static final icon_lg_compright_right:I = 0x7f020152

.field public static final icon_lg_compright_slight_right:I = 0x7f020153

.field public static final icon_lg_compright_straight:I = 0x7f020154

.field public static final icon_lg_compright_uturn_right:I = 0x7f020155

.field public static final icon_lg_heavy_left_only:I = 0x7f020156

.field public static final icon_lg_heavy_right_only:I = 0x7f020157

.field public static final icon_lg_left_only:I = 0x7f020158

.field public static final icon_lg_rec_heavy_left_only:I = 0x7f020159

.field public static final icon_lg_rec_heavy_right_only:I = 0x7f02015a

.field public static final icon_lg_rec_left_only:I = 0x7f02015b

.field public static final icon_lg_rec_right_only:I = 0x7f02015c

.field public static final icon_lg_rec_slight_left_only:I = 0x7f02015d

.field public static final icon_lg_rec_slight_right_only:I = 0x7f02015e

.field public static final icon_lg_rec_straight_only:I = 0x7f02015f

.field public static final icon_lg_rec_uturn_left_only:I = 0x7f020160

.field public static final icon_lg_rec_uturn_right_only:I = 0x7f020161

.field public static final icon_lg_right_only:I = 0x7f020162

.field public static final icon_lg_slight_left_only:I = 0x7f020163

.field public static final icon_lg_slight_right_only:I = 0x7f020164

.field public static final icon_lg_straight_only:I = 0x7f020165

.field public static final icon_lg_uturn_left_only:I = 0x7f020166

.field public static final icon_lg_uturn_right_only:I = 0x7f020167

.field public static final icon_main_menu_dash_options:I = 0x7f020168

.field public static final icon_main_menu_map_options:I = 0x7f020169

.field public static final icon_main_menu_trip_options_2:I = 0x7f02016a

.field public static final icon_menu_close:I = 0x7f02016b

.field public static final icon_message:I = 0x7f02016c

.field public static final icon_message_blue:I = 0x7f02016d

.field public static final icon_mm_back:I = 0x7f02016e

.field public static final icon_mm_brightness:I = 0x7f02016f

.field public static final icon_mm_brightness_copy:I = 0x7f020170

.field public static final icon_mm_brightness_disabled:I = 0x7f020171

.field public static final icon_mm_contacts_2:I = 0x7f020172

.field public static final icon_mm_dash:I = 0x7f020173

.field public static final icon_mm_dash_2:I = 0x7f020174

.field public static final icon_mm_demo_pause_2:I = 0x7f020175

.field public static final icon_mm_demo_play_2:I = 0x7f020176

.field public static final icon_mm_demo_restart_2:I = 0x7f020177

.field public static final icon_mm_exit:I = 0x7f020178

.field public static final icon_mm_favorite_places:I = 0x7f020179

.field public static final icon_mm_glances_2:I = 0x7f02017a

.field public static final icon_mm_googlenow_2:I = 0x7f02017b

.field public static final icon_mm_incident:I = 0x7f02017c

.field public static final icon_mm_map:I = 0x7f02017d

.field public static final icon_mm_map_2:I = 0x7f02017e

.field public static final icon_mm_map_sm:I = 0x7f02017f

.field public static final icon_mm_music_2:I = 0x7f020180

.field public static final icon_mm_music_control_blank:I = 0x7f020181

.field public static final icon_mm_navdy:I = 0x7f020182

.field public static final icon_mm_places:I = 0x7f020183

.field public static final icon_mm_places_2:I = 0x7f020184

.field public static final icon_mm_recent_contacts:I = 0x7f020185

.field public static final icon_mm_search_2:I = 0x7f020186

.field public static final icon_mm_settings_2:I = 0x7f020187

.field public static final icon_mm_siri_2:I = 0x7f020188

.field public static final icon_mm_slow_traffic:I = 0x7f020189

.field public static final icon_mm_stopped_traffic:I = 0x7f02018a

.field public static final icon_mm_suggested_places:I = 0x7f02018b

.field public static final icon_mm_voice_2:I = 0x7f02018c

.field public static final icon_mm_voice_search_2:I = 0x7f02018d

.field public static final icon_more:I = 0x7f02018e

.field public static final icon_msg_alert:I = 0x7f02018f

.field public static final icon_msg_failed:I = 0x7f020190

.field public static final icon_msg_success:I = 0x7f020191

.field public static final icon_music_queue_sm:I = 0x7f020192

.field public static final icon_music_sm_2:I = 0x7f020193

.field public static final icon_navdyapp:I = 0x7f020194

.field public static final icon_next_music:I = 0x7f020195

.field public static final icon_next_sm_2:I = 0x7f020196

.field public static final icon_not_supported:I = 0x7f020197

.field public static final icon_o_t_a_gears:I = 0x7f020198

.field public static final icon_open_nav_disable_traffic:I = 0x7f020199

.field public static final icon_options_dash_scroll_left_2:I = 0x7f02019a

.field public static final icon_options_dash_scroll_right_2:I = 0x7f02019b

.field public static final icon_options_dash_speedometer_2:I = 0x7f02019c

.field public static final icon_options_dash_tachometer_2:I = 0x7f02019d

.field public static final icon_options_map_zoom_auto_2:I = 0x7f02019e

.field public static final icon_options_map_zoom_manual_2:I = 0x7f02019f

.field public static final icon_options_report_issue_2:I = 0x7f0201a0

.field public static final icon_other_issue:I = 0x7f0201a1

.field public static final icon_pause_sm_2:I = 0x7f0201a2

.field public static final icon_phone_network_error:I = 0x7f0201a3

.field public static final icon_pin_dot_destination:I = 0x7f0201a4

.field public static final icon_pin_dot_destination_blue:I = 0x7f0201a5

.field public static final icon_pin_dot_destination_fuschia:I = 0x7f0201a6

.field public static final icon_pin_dot_destination_green:I = 0x7f0201a7

.field public static final icon_pin_dot_destination_grey:I = 0x7f0201a8

.field public static final icon_pin_dot_destination_indigo:I = 0x7f0201a9

.field public static final icon_pin_dot_destination_left:I = 0x7f0201aa

.field public static final icon_pin_dot_destination_orange:I = 0x7f0201ab

.field public static final icon_pin_dot_destination_red:I = 0x7f0201ac

.field public static final icon_pin_dot_destination_right:I = 0x7f0201ad

.field public static final icon_pin_dot_destination_violet:I = 0x7f0201ae

.field public static final icon_pin_dot_destination_yellow:I = 0x7f0201af

.field public static final icon_pin_route_circle:I = 0x7f0201b0

.field public static final icon_place_a_t_m:I = 0x7f0201b1

.field public static final icon_place_calendar:I = 0x7f0201b2

.field public static final icon_place_calendar_sm:I = 0x7f0201b3

.field public static final icon_place_coffee:I = 0x7f0201b4

.field public static final icon_place_favorite_2:I = 0x7f0201b5

.field public static final icon_place_gas:I = 0x7f0201b6

.field public static final icon_place_home_2:I = 0x7f0201b7

.field public static final icon_place_hospital:I = 0x7f0201b8

.field public static final icon_place_parking:I = 0x7f0201b9

.field public static final icon_place_recent_2:I = 0x7f0201ba

.field public static final icon_place_restaurant:I = 0x7f0201bb

.field public static final icon_place_store:I = 0x7f0201bc

.field public static final icon_place_work_2:I = 0x7f0201bd

.field public static final icon_places_favorite:I = 0x7f0201be

.field public static final icon_places_gas_2:I = 0x7f0201bf

.field public static final icon_places_home:I = 0x7f0201c0

.field public static final icon_places_work:I = 0x7f0201c1

.field public static final icon_play_music:I = 0x7f0201c2

.field public static final icon_play_sm_2:I = 0x7f0201c3

.field public static final icon_playlist:I = 0x7f0201c4

.field public static final icon_podcasts:I = 0x7f0201c5

.field public static final icon_position_center:I = 0x7f0201c6

.field public static final icon_position_raw_gps:I = 0x7f0201c7

.field public static final icon_power:I = 0x7f0201c8

.field public static final icon_prev_music:I = 0x7f0201c9

.field public static final icon_prev_sm_2:I = 0x7f0201ca

.field public static final icon_qm_glances_grey:I = 0x7f0201cb

.field public static final icon_red_light_camera:I = 0x7f0201cc

.field public static final icon_reply:I = 0x7f0201cd

.field public static final icon_report_issue:I = 0x7f0201ce

.field public static final icon_retry:I = 0x7f0201cf

.field public static final icon_road_closed_perm:I = 0x7f0201d0

.field public static final icon_road_closed_temp:I = 0x7f0201d1

.field public static final icon_route:I = 0x7f0201d2

.field public static final icon_route_sm:I = 0x7f0201d3

.field public static final icon_settings_brightness_2:I = 0x7f0201d4

.field public static final icon_settings_connect_phone_2:I = 0x7f0201d5

.field public static final icon_settings_factory_reset:I = 0x7f0201d6

.field public static final icon_settings_factory_reset_2:I = 0x7f0201d7

.field public static final icon_settings_general:I = 0x7f0201d8

.field public static final icon_settings_learn_gestures_2:I = 0x7f0201d9

.field public static final icon_settings_learning_to_gesture:I = 0x7f0201da

.field public static final icon_settings_learning_to_gesture_gray:I = 0x7f0201db

.field public static final icon_settings_navdy_data:I = 0x7f0201dc

.field public static final icon_settings_navdy_data_sm:I = 0x7f0201dd

.field public static final icon_settings_shutdown_2:I = 0x7f0201de

.field public static final icon_share_location:I = 0x7f0201df

.field public static final icon_show:I = 0x7f0201e0

.field public static final icon_shuffle:I = 0x7f0201e1

.field public static final icon_side_gauges:I = 0x7f0201e2

.field public static final icon_siri:I = 0x7f0201e3

.field public static final icon_sm_forgotten:I = 0x7f0201e4

.field public static final icon_sm_spinner:I = 0x7f0201e5

.field public static final icon_sm_success:I = 0x7f0201e6

.field public static final icon_smartdash_options_compass:I = 0x7f0201e7

.field public static final icon_smartdash_options_fuel:I = 0x7f0201e8

.field public static final icon_smartdash_options_krpm:I = 0x7f0201e9

.field public static final icon_smartdash_options_range:I = 0x7f0201ea

.field public static final icon_social_blue:I = 0x7f0201eb

.field public static final icon_software_update:I = 0x7f0201ec

.field public static final icon_software_update_2:I = 0x7f0201ed

.field public static final icon_song:I = 0x7f0201ee

.field public static final icon_sound_off:I = 0x7f0201ef

.field public static final icon_sound_on:I = 0x7f0201f0

.field public static final icon_speed_camera:I = 0x7f0201f1

.field public static final icon_stations:I = 0x7f0201f2

.field public static final icon_stats:I = 0x7f0201f3

.field public static final icon_status_gps_error:I = 0x7f0201f4

.field public static final icon_success:I = 0x7f0201f5

.field public static final icon_tbt_arrive:I = 0x7f0201f6

.field public static final icon_tbt_arrive_grey:I = 0x7f0201f7

.field public static final icon_tbt_enter_highway_from_left_grey:I = 0x7f0201f8

.field public static final icon_tbt_enter_highway_from_right_grey:I = 0x7f0201f9

.field public static final icon_tbt_go_straight_grey:I = 0x7f0201fa

.field public static final icon_tbt_heavy_left_grey:I = 0x7f0201fb

.field public static final icon_tbt_heavy_right_grey:I = 0x7f0201fc

.field public static final icon_tbt_keep_left_grey:I = 0x7f0201fd

.field public static final icon_tbt_keep_right_grey:I = 0x7f0201fe

.field public static final icon_tbt_leave_highway_grey:I = 0x7f0201ff

.field public static final icon_tbt_leave_highway_left_lane_grey:I = 0x7f020200

.field public static final icon_tbt_light_left_grey:I = 0x7f020201

.field public static final icon_tbt_light_right_grey:I = 0x7f020202

.field public static final icon_tbt_merge_grey:I = 0x7f020203

.field public static final icon_tbt_merge_left_grey:I = 0x7f020204

.field public static final icon_tbt_merge_right_grey:I = 0x7f020205

.field public static final icon_tbt_now:I = 0x7f020206

.field public static final icon_tbt_quite_left_grey:I = 0x7f020207

.field public static final icon_tbt_quite_right_grey:I = 0x7f020208

.field public static final icon_tbt_start_grey:I = 0x7f020209

.field public static final icon_tbt_uturn_right_grey:I = 0x7f02020a

.field public static final icon_thumbs_dn:I = 0x7f02020b

.field public static final icon_thumbs_up:I = 0x7f02020c

.field public static final icon_toast_dial_battery_low:I = 0x7f02020d

.field public static final icon_toast_dial_battery_very_low:I = 0x7f02020e

.field public static final icon_toast_phone_battery_low:I = 0x7f02020f

.field public static final icon_toast_phone_battery_very_low:I = 0x7f020210

.field public static final icon_toast_phone_network_error:I = 0x7f020211

.field public static final icon_type_phone_disconnected:I = 0x7f020212

.field public static final icon_user_bg_0:I = 0x7f020213

.field public static final icon_user_bg_1:I = 0x7f020214

.field public static final icon_user_bg_2:I = 0x7f020215

.field public static final icon_user_bg_3:I = 0x7f020216

.field public static final icon_user_bg_4:I = 0x7f020217

.field public static final icon_user_bg_5:I = 0x7f020218

.field public static final icon_user_bg_6:I = 0x7f020219

.field public static final icon_user_bg_7:I = 0x7f02021a

.field public static final icon_user_bg_8:I = 0x7f02021b

.field public static final icon_user_grey:I = 0x7f02021c

.field public static final icon_user_navdy:I = 0x7f02021d

.field public static final icon_user_numberonly:I = 0x7f02021e

.field public static final icon_vehicle:I = 0x7f02021f

.field public static final icon_versions:I = 0x7f020220

.field public static final icon_voice:I = 0x7f020221

.field public static final icon_voice_search_active:I = 0x7f020222

.field public static final icon_voice_search_active_big:I = 0x7f020223

.field public static final icon_voice_search_inactive:I = 0x7f020224

.field public static final icon_voice_search_sm:I = 0x7f020225

.field public static final icon_warning:I = 0x7f020226

.field public static final icon_warning_temperature:I = 0x7f020227

.field public static final icon_wrong_road_name:I = 0x7f020228

.field public static final junction:I = 0x7f020229

.field public static final junction_view_scrim:I = 0x7f02022a

.field public static final lane_bk:I = 0x7f02022b

.field public static final learn_gesture_indicator_background:I = 0x7f02022c

.field public static final left_gesture_arrow:I = 0x7f02022d

.field public static final loader_circle:I = 0x7f02022e

.field public static final locale_icon_background:I = 0x7f02022f

.field public static final map_mask:I = 0x7f020230

.field public static final msg_gradient_bottom:I = 0x7f020231

.field public static final msg_gradient_top:I = 0x7f020232

.field public static final music_progress_drawable:I = 0x7f020233

.field public static final navdy_logo:I = 0x7f020234

.field public static final navigation_map_mask:I = 0x7f020235

.field public static final notification_00:I = 0x7f020236

.field public static final notification_01:I = 0x7f020237

.field public static final notification_02:I = 0x7f020238

.field public static final notification_03:I = 0x7f020239

.field public static final notification_04:I = 0x7f02023a

.field public static final notification_05:I = 0x7f02023b

.field public static final notification_06:I = 0x7f02023c

.field public static final notification_07:I = 0x7f02023d

.field public static final notification_08:I = 0x7f02023e

.field public static final notification_09:I = 0x7f02023f

.field public static final notification_arrived:I = 0x7f020240

.field public static final notification_low_battery:I = 0x7f020241

.field public static final notification_received:I = 0x7f020242

.field public static final notification_warning:I = 0x7f020243

.field public static final right_gesture_arrow:I = 0x7f020244

.field public static final scroll_scrims:I = 0x7f020245

.field public static final small_gauge_background:I = 0x7f020246

.field public static final speed:I = 0x7f020247

.field public static final speed_limit_sign_background:I = 0x7f020248

.field public static final speed_limit_sign_background_eu:I = 0x7f020249

.field public static final switch_button_halo:I = 0x7f02024a

.field public static final switch_button_off:I = 0x7f02024b

.field public static final switch_button_on:I = 0x7f02024c

.field public static final switch_thumb_disabled:I = 0x7f02024d

.field public static final switch_thumb_enabled:I = 0x7f02024e

.field public static final tbt_arrive_left:I = 0x7f02024f

.field public static final tbt_arrive_left_soon:I = 0x7f020250

.field public static final tbt_arrive_right:I = 0x7f020251

.field public static final tbt_arrive_right_soon:I = 0x7f020252

.field public static final tbt_easy_left:I = 0x7f020253

.field public static final tbt_easy_left_soon:I = 0x7f020254

.field public static final tbt_easy_right:I = 0x7f020255

.field public static final tbt_easy_right_soon:I = 0x7f020256

.field public static final tbt_end:I = 0x7f020257

.field public static final tbt_end_soon:I = 0x7f020258

.field public static final tbt_exit_left:I = 0x7f020259

.field public static final tbt_exit_left_soon:I = 0x7f02025a

.field public static final tbt_exit_right:I = 0x7f02025b

.field public static final tbt_exit_right_soon:I = 0x7f02025c

.field public static final tbt_go_straight:I = 0x7f02025d

.field public static final tbt_go_straight_soon:I = 0x7f02025e

.field public static final tbt_icon_return_grey:I = 0x7f02025f

.field public static final tbt_left_roundabout:I = 0x7f020260

.field public static final tbt_left_roundabout_soon:I = 0x7f020261

.field public static final tbt_merge_left:I = 0x7f020262

.field public static final tbt_merge_left_soon:I = 0x7f020263

.field public static final tbt_merge_right:I = 0x7f020264

.field public static final tbt_merge_right_soon:I = 0x7f020265

.field public static final tbt_progress_drawable:I = 0x7f020266

.field public static final tbt_right_roundabout:I = 0x7f020267

.field public static final tbt_right_roundabout_soon:I = 0x7f020268

.field public static final tbt_sharp_left:I = 0x7f020269

.field public static final tbt_sharp_left_soon:I = 0x7f02026a

.field public static final tbt_sharp_right:I = 0x7f02026b

.field public static final tbt_sharp_right_soon:I = 0x7f02026c

.field public static final tbt_stay_left:I = 0x7f02026d

.field public static final tbt_stay_left_soon:I = 0x7f02026e

.field public static final tbt_stay_right:I = 0x7f02026f

.field public static final tbt_stay_right_soon:I = 0x7f020270

.field public static final tbt_straight:I = 0x7f020271

.field public static final tbt_straight_soon:I = 0x7f020272

.field public static final tbt_turn_left:I = 0x7f020273

.field public static final tbt_turn_left_soon:I = 0x7f020274

.field public static final tbt_turn_right:I = 0x7f020275

.field public static final tbt_turn_right_soon:I = 0x7f020276

.field public static final tbt_u_turn_left:I = 0x7f020277

.field public static final tbt_u_turn_left_soon:I = 0x7f020278

.field public static final tbt_u_turn_right:I = 0x7f020279

.field public static final tbt_u_turn_right_soon:I = 0x7f02027a

.field public static final tool_tip_bk:I = 0x7f02027b

.field public static final triangle:I = 0x7f02027c

.field public static final trip_progress_drawable:I = 0x7f02027d

.field public static final trip_progress_point_indicator:I = 0x7f02027e

.field public static final vlist_gradient:I = 0x7f02027f

.field public static final vlist_scrim:I = 0x7f020280

.field public static final voice_search_listening_feedback_background:I = 0x7f020281

.field public static final voice_search_spinner:I = 0x7f020282


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
