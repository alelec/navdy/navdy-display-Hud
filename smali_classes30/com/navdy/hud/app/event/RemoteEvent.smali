.class public Lcom/navdy/hud/app/event/RemoteEvent;
.super Ljava/lang/Object;
.source "RemoteEvent.java"


# instance fields
.field protected message:Lcom/squareup/wire/Message;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/wire/Message;)V
    .locals 0
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    if-ne p0, p1, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v1

    .line 29
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 30
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 33
    check-cast v0, Lcom/navdy/hud/app/event/RemoteEvent;

    .line 35
    .local v0, "that":Lcom/navdy/hud/app/event/RemoteEvent;
    iget-object v3, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    iget-object v4, v0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, v0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    if-nez v3, :cond_4

    goto :goto_0
.end method

.method public getMessage()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/event/RemoteEvent;->message:Lcom/squareup/wire/Message;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
