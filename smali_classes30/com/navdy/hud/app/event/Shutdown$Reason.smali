.class public final enum Lcom/navdy/hud/app/event/Shutdown$Reason;
.super Ljava/lang/Enum;
.source "Shutdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/event/Shutdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/event/Shutdown$Reason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum ACCELERATE_SHUTDOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum CRITICAL_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum DIAL_OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum ENGINE_OFF:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum FACTORY_RESET:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum FORCED_UPDATE:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum LOW_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum MENU:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum POWER_LOSS:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum TIMEOUT:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public static final enum UNKNOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;


# instance fields
.field public final attr:Ljava/lang/String;

.field public final requireConfirmation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 15
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->UNKNOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "CRITICAL_VOLTAGE"

    const-string v2, "Critical_Voltage"

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->CRITICAL_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "LOW_VOLTAGE"

    const-string v2, "Low_Voltage"

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->LOW_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "HIGH_TEMPERATURE"

    const-string v2, "High_Temperature"

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "TIMEOUT"

    const-string v2, "Timeout"

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->TIMEOUT:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "OTA"

    const/4 v2, 0x5

    const-string v3, "OTA"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "DIAL_OTA"

    const/4 v2, 0x6

    const-string v3, "Dial_OTA"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->DIAL_OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "FORCED_UPDATE"

    const/4 v2, 0x7

    const-string v3, "Forced_Update"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->FORCED_UPDATE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 29
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "FACTORY_RESET"

    const/16 v2, 0x8

    const-string v3, "Factory_Reset"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->FACTORY_RESET:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 32
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "POWER_LOSS"

    const/16 v2, 0x9

    const-string v3, "Power_Loss"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_LOSS:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 35
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "MENU"

    const/16 v2, 0xa

    const-string v3, "Menu"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->MENU:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 36
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "POWER_BUTTON"

    const/16 v2, 0xb

    const-string v3, "Power_Button"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "ENGINE_OFF"

    const/16 v2, 0xc

    const-string v3, "Engine_Off"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->ENGINE_OFF:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "ACCELERATE_SHUTDOWN"

    const/16 v2, 0xd

    const-string v3, "Accelerate_Shutdown"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->ACCELERATE_SHUTDOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 44
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    const-string v1, "INACTIVITY"

    const/16 v2, 0xe

    const-string v3, "Inactivity"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 14
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/navdy/hud/app/event/Shutdown$Reason;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->UNKNOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->CRITICAL_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->LOW_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->TIMEOUT:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->DIAL_OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->FORCED_UPDATE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->FACTORY_RESET:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_LOSS:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->MENU:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->ENGINE_OFF:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->ACCELERATE_SHUTDOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->$VALUES:[Lcom/navdy/hud/app/event/Shutdown$Reason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .param p3, "attr"    # Ljava/lang/String;
    .param p4, "requireConfirmation"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lcom/navdy/hud/app/event/Shutdown$Reason;->attr:Ljava/lang/String;

    .line 51
    iput-boolean p4, p0, Lcom/navdy/hud/app/event/Shutdown$Reason;->requireConfirmation:Z

    .line 52
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/event/Shutdown$Reason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/event/Shutdown$Reason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/event/Shutdown$Reason;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->$VALUES:[Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/event/Shutdown$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/event/Shutdown$Reason;

    return-object v0
.end method


# virtual methods
.method public asBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "SHUTDOWN_CAUSE"

    invoke-virtual {p0}, Lcom/navdy/hud/app/event/Shutdown$Reason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-object v0
.end method
