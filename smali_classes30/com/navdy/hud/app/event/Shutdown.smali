.class public Lcom/navdy/hud/app/event/Shutdown;
.super Ljava/lang/Object;
.source "Shutdown.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/event/Shutdown$State;,
        Lcom/navdy/hud/app/event/Shutdown$Reason;
    }
.end annotation


# static fields
.field public static final EXTRA_SHUTDOWN_CAUSE:Ljava/lang/String; = "SHUTDOWN_CAUSE"


# instance fields
.field public final reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field public final state:Lcom/navdy/hud/app/event/Shutdown$State;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
    .locals 1
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/navdy/hud/app/event/Shutdown;->reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 77
    iget-boolean v0, p1, Lcom/navdy/hud/app/event/Shutdown$Reason;->requireConfirmation:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMATION:Lcom/navdy/hud/app/event/Shutdown$State;

    :goto_0
    iput-object v0, p0, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    .line 78
    return-void

    .line 77
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMED:Lcom/navdy/hud/app/event/Shutdown$State;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/navdy/hud/app/event/Shutdown$Reason;Lcom/navdy/hud/app/event/Shutdown$State;)V
    .locals 0
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;
    .param p2, "state"    # Lcom/navdy/hud/app/event/Shutdown$State;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/navdy/hud/app/event/Shutdown;->reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 83
    iput-object p2, p0, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    .line 84
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Shutdown{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/event/Shutdown;->reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 91
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
