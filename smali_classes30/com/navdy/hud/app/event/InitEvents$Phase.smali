.class public final enum Lcom/navdy/hud/app/event/InitEvents$Phase;
.super Ljava/lang/Enum;
.source "InitEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/event/InitEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Phase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/event/InitEvents$Phase;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum CONNECTION_SERVICE_STARTED:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum EARLY:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum LATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum LOCALE_UP_TO_DATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum PRE_USER_INTERACTION:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field public static final enum SWITCHING_LOCALE:Lcom/navdy/hud/app/event/InitEvents$Phase;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "EARLY"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->EARLY:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 11
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "PRE_USER_INTERACTION"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->PRE_USER_INTERACTION:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 13
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "CONNECTION_SERVICE_STARTED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->CONNECTION_SERVICE_STARTED:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 15
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "POST_START"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 17
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "LATE"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->LATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "SWITCHING_LOCALE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->SWITCHING_LOCALE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    const-string v1, "LOCALE_UP_TO_DATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/event/InitEvents$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->LOCALE_UP_TO_DATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 7
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/app/event/InitEvents$Phase;

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->EARLY:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->PRE_USER_INTERACTION:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->CONNECTION_SERVICE_STARTED:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->LATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/event/InitEvents$Phase;->SWITCHING_LOCALE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/event/InitEvents$Phase;->LOCALE_UP_TO_DATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->$VALUES:[Lcom/navdy/hud/app/event/InitEvents$Phase;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/event/InitEvents$Phase;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/event/InitEvents$Phase;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/event/InitEvents$Phase;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/hud/app/event/InitEvents$Phase;->$VALUES:[Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/event/InitEvents$Phase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/event/InitEvents$Phase;

    return-object v0
.end method
