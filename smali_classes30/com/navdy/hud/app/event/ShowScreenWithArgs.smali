.class public Lcom/navdy/hud/app/event/ShowScreenWithArgs;
.super Ljava/lang/Object;
.source "ShowScreenWithArgs.java"


# instance fields
.field public args:Landroid/os/Bundle;

.field public args2:Ljava/lang/Object;

.field public ignoreAnimation:Z

.field public screen:Lcom/navdy/service/library/events/ui/Screen;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "args2"    # Ljava/lang/Object;
    .param p4, "ignoreAnimation"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 22
    iput-object p2, p0, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->args:Landroid/os/Bundle;

    .line 23
    iput-object p3, p0, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->args2:Ljava/lang/Object;

    .line 24
    iput-boolean p4, p0, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->ignoreAnimation:Z

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "ignoreAnimation"    # Z

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    .line 15
    return-void
.end method
