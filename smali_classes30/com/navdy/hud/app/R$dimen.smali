.class public final Lcom/navdy/hud/app/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final active_map_height:I = 0x7f0b0001

.field public static final active_road_early_maneuver_shrink_left_x:I = 0x7f0b0002

.field public static final active_road_early_maneuver_shrink_right_x:I = 0x7f0b0003

.field public static final active_road_early_maneuver_shrink_width:I = 0x7f0b0004

.field public static final active_road_early_maneuver_width:I = 0x7f0b0005

.field public static final active_road_early_maneuver_x:I = 0x7f0b0006

.field public static final active_road_early_maneuver_y:I = 0x7f0b0007

.field public static final active_road_early_maneuver_y_shrunk:I = 0x7f0b0008

.field public static final active_road_info_distance_x:I = 0x7f0b0009

.field public static final active_road_info_icon_shrink_left_x:I = 0x7f0b000a

.field public static final active_road_info_icon_shrink_right_x:I = 0x7f0b000b

.field public static final active_road_info_icon_x:I = 0x7f0b000c

.field public static final active_road_info_instruction_shrink_left_x:I = 0x7f0b000d

.field public static final active_road_info_instruction_shrink_right_x:I = 0x7f0b000e

.field public static final active_road_info_instruction_shrink_width:I = 0x7f0b000f

.field public static final active_road_info_instruction_width:I = 0x7f0b0010

.field public static final active_road_info_instruction_x:I = 0x7f0b0011

.field public static final active_road_info_instruction_y_translation_anim:I = 0x7f0b0012

.field public static final active_road_progress_bar_width:I = 0x7f0b0013

.field public static final active_road_shrunk_distance_left_x:I = 0x7f0b0014

.field public static final active_road_shrunk_distance_right_x:I = 0x7f0b0015

.field public static final active_road_shrunk_distance_x:I = 0x7f0b0016

.field public static final active_road_shrunk_distance_y:I = 0x7f0b0017

.field public static final active_road_shrunk_progress_bar_width:I = 0x7f0b0018

.field public static final active_trip_18:I = 0x7f0b0019

.field public static final active_trip_22:I = 0x7f0b001a

.field public static final active_trip_maneuver_icon_size:I = 0x7f0b001b

.field public static final analog_clock_center_point_width:I = 0x7f0b001c

.field public static final analog_clock_date_text_margin:I = 0x7f0b001d

.field public static final analog_clock_date_text_size:I = 0x7f0b001e

.field public static final analog_clock_hour_hand_width:I = 0x7f0b001f

.field public static final analog_clock_minute_hand_width:I = 0x7f0b0020

.field public static final arc_stroke_width:I = 0x7f0b0021

.field public static final arc_view_width:I = 0x7f0b0022

.field public static final auto_brightness_choice_margin_bottom:I = 0x7f0b0023

.field public static final auto_brightness_choices_marginTop:I = 0x7f0b0024

.field public static final auto_brightness_description_textSize:I = 0x7f0b0025

.field public static final auto_brightness_description_width:I = 0x7f0b0026

.field public static final auto_brightness_icon_marginStart:I = 0x7f0b0027

.field public static final auto_brightness_icon_marginTop:I = 0x7f0b0028

.field public static final auto_brightness_section_width:I = 0x7f0b0029

.field public static final auto_brightness_status_label_marginStart:I = 0x7f0b002a

.field public static final auto_brightness_status_label_marginTop:I = 0x7f0b002b

.field public static final auto_brightness_status_label_textSize:I = 0x7f0b002c

.field public static final calendar_widget_left_padding:I = 0x7f0b002d

.field public static final carousel_big_highlight_size:I = 0x7f0b002e

.field public static final carousel_circle_indicator_focus_size:I = 0x7f0b002f

.field public static final carousel_circle_indicator_margin:I = 0x7f0b0030

.field public static final carousel_circle_indicator_size:I = 0x7f0b0031

.field public static final carousel_fastscroll_hero_translate_x:I = 0x7f0b0032

.field public static final carousel_main_desc_size:I = 0x7f0b0033

.field public static final carousel_main_divider_padding:I = 0x7f0b0034

.field public static final carousel_main_image_size:I = 0x7f0b0035

.field public static final carousel_main_left_padding:I = 0x7f0b0036

.field public static final carousel_main_right_padding:I = 0x7f0b0037

.field public static final carousel_main_right_section_start:I = 0x7f0b0038

.field public static final carousel_main_right_section_width:I = 0x7f0b0039

.field public static final carousel_main_title_size:I = 0x7f0b003a

.field public static final carousel_menu_indicator_bottom_margin:I = 0x7f0b003b

.field public static final carousel_menu_title_margin_top:I = 0x7f0b003c

.field public static final carousel_menu_title_text_size:I = 0x7f0b003d

.field public static final carousel_middle_section_width:I = 0x7f0b003e

.field public static final carousel_progress_currentitem_padding_radius:I = 0x7f0b003f

.field public static final carousel_progress_indicator_h:I = 0x7f0b0040

.field public static final carousel_progress_indicator_vertical_h:I = 0x7f0b0041

.field public static final carousel_progress_indicator_vertical_w:I = 0x7f0b0042

.field public static final carousel_progress_indicator_w:I = 0x7f0b0043

.field public static final carousel_progress_item_padding:I = 0x7f0b0044

.field public static final carousel_progress_item_radius:I = 0x7f0b0045

.field public static final carousel_progress_round_radius:I = 0x7f0b0046

.field public static final carousel_settings_title_text_size:I = 0x7f0b0047

.field public static final carousel_side_image_size:I = 0x7f0b0048

.field public static final carousel_side_margin:I = 0x7f0b0049

.field public static final carousel_small_highlight_size:I = 0x7f0b004a

.field public static final choice_lyt_highlight_view_ht:I = 0x7f0b004b

.field public static final choices2_lyt_halo_end_radius:I = 0x7f0b004c

.field public static final choices2_lyt_halo_middle_radius:I = 0x7f0b004d

.field public static final choices2_lyt_halo_start_radius:I = 0x7f0b004e

.field public static final choices2_lyt_icon_halo_size:I = 0x7f0b004f

.field public static final choices2_lyt_icon_size:I = 0x7f0b0050

.field public static final choices2_lyt_item_padding:I = 0x7f0b0051

.field public static final choices2_lyt_item_top_padding:I = 0x7f0b0052

.field public static final choices2_lyt_text_size:I = 0x7f0b0053

.field public static final choices_lyt_highlight_margin:I = 0x7f0b0054

.field public static final choices_lyt_highlight_view_container_ht:I = 0x7f0b0055

.field public static final choices_lyt_item_padding:I = 0x7f0b0056

.field public static final choices_lyt_item_padding_bottom:I = 0x7f0b0057

.field public static final choices_lyt_text_size:I = 0x7f0b0058

.field public static final compass_indicator_height:I = 0x7f0b0059

.field public static final compass_indicator_top_margin:I = 0x7f0b005a

.field public static final compass_indicator_width:I = 0x7f0b005b

.field public static final compass_major_tick_height:I = 0x7f0b005c

.field public static final compass_minor_tick_height:I = 0x7f0b005d

.field public static final compass_strip_height:I = 0x7f0b005e

.field public static final compass_strip_margin_top:I = 0x7f0b005f

.field public static final compass_strip_width:I = 0x7f0b0060

.field public static final compass_text_height:I = 0x7f0b0061

.field public static final contact_image_large_text:I = 0x7f0b0062

.field public static final contact_image_medium_text:I = 0x7f0b0063

.field public static final contact_image_small_text:I = 0x7f0b0064

.field public static final contact_image_tiny_text:I = 0x7f0b0065

.field public static final context_menu_glance_count_padding:I = 0x7f0b0066

.field public static final context_menu_highlight_move_size:I = 0x7f0b0067

.field public static final context_menu_highlight_size:I = 0x7f0b0068

.field public static final context_menu_icon_size:I = 0x7f0b0069

.field public static final context_menu_image_text_margin:I = 0x7f0b006a

.field public static final context_menu_separator_margin:I = 0x7f0b006b

.field public static final context_menu_text_size:I = 0x7f0b006c

.field public static final context_menu_title_margin_bottom:I = 0x7f0b006d

.field public static final context_menu_title_text_size:I = 0x7f0b006e

.field public static final dashboard_box_height:I = 0x7f0b006f

.field public static final dashboard_height:I = 0x7f0b0070

.field public static final dashboard_width:I = 0x7f0b0071

.field public static final default_highlight_stroke_width:I = 0x7f0b0072

.field public static final dest_picker_left_container_w:I = 0x7f0b0073

.field public static final dest_picker_map_left_container_w:I = 0x7f0b0074

.field public static final dest_picker_map_right_container_w:I = 0x7f0b0075

.field public static final dest_picker_right_container_w:I = 0x7f0b0076

.field public static final destination_title_width:I = 0x7f0b0077

.field public static final dial_connected_status_margin_bottom:I = 0x7f0b0078

.field public static final dial_disconnected_margin:I = 0x7f0b0079

.field public static final dial_firmware_update_toast_width:I = 0x7f0b007a

.field public static final dial_message_max_width:I = 0x7f0b007b

.field public static final dial_pairing_status_margin_bottom:I = 0x7f0b007c

.field public static final drive_score_gauge_width:I = 0x7f0b007d

.field public static final eta_left_x:I = 0x7f0b007e

.field public static final eta_progress_vertical_margin:I = 0x7f0b007f

.field public static final eta_shrink_left_x:I = 0x7f0b0080

.field public static final expand_mode_left_gauge_left_margin:I = 0x7f0b0081

.field public static final expand_mode_right_gauge_left_margin:I = 0x7f0b0082

.field public static final expand_mode_speedo_meter_left_margin:I = 0x7f0b0083

.field public static final expand_mode_time_left_margin:I = 0x7f0b0084

.field public static final expand_notif_cover_width:I = 0x7f0b0085

.field public static final expand_notif_indicator_left_margin:I = 0x7f0b0086

.field public static final expand_notif_width:I = 0x7f0b0087

.field public static final far_margin:I = 0x7f0b0088

.field public static final fuel_gauge_width:I = 0x7f0b0089

.field public static final gauge_frame_height:I = 0x7f0b008a

.field public static final gauge_frame_top_margin_active_mode:I = 0x7f0b008b

.field public static final gauge_frame_top_margin_full_mode:I = 0x7f0b008c

.field public static final gauge_frame_width:I = 0x7f0b008d

.field public static final gauge_height:I = 0x7f0b008e

.field public static final gauge_horizontal_inset:I = 0x7f0b008f

.field public static final gauge_left_margin:I = 0x7f0b0090

.field public static final gauge_transition_margin:I = 0x7f0b0091

.field public static final gauge_vertical_inset:I = 0x7f0b0092

.field public static final gauge_width:I = 0x7f0b0093

.field public static final gesture_icon_animate_distance:I = 0x7f0b0094

.field public static final gesture_icon_margin_from_center_image:I = 0x7f0b0095

.field public static final gesture_icon_width:I = 0x7f0b0096

.field public static final gesture_learning_tips_text_height:I = 0x7f0b0097

.field public static final gesture_learning_tips_text_scroll_height:I = 0x7f0b0098

.field public static final gesture_sensor_blocked_max_width:I = 0x7f0b0099

.field public static final glance_anim_translation:I = 0x7f0b009a

.field public static final glance_calendar_normal_margin:I = 0x7f0b009b

.field public static final glance_calendar_now_margin:I = 0x7f0b009c

.field public static final glance_calendar_pm_marker:I = 0x7f0b009d

.field public static final glance_calendar_time_margin:I = 0x7f0b009e

.field public static final glance_message_height_bound:I = 0x7f0b009f

.field public static final glance_message_title_bottom:I = 0x7f0b00a0

.field public static final glance_message_title_top:I = 0x7f0b00a1

.field public static final glance_message_width_bound:I = 0x7f0b00a2

.field public static final glance_scrolling_indicator_bar_size:I = 0x7f0b00a3

.field public static final glance_scrolling_indicator_circle_size:I = 0x7f0b00a4

.field public static final glance_scrolling_indicator_focus_circle_radius:I = 0x7f0b00a5

.field public static final glance_scrolling_indicator_focus_circle_size:I = 0x7f0b00a6

.field public static final glance_scrolling_indicator_height:I = 0x7f0b00a7

.field public static final glance_scrolling_indicator_left_padding:I = 0x7f0b00a8

.field public static final glance_scrolling_indicator_padding:I = 0x7f0b00a9

.field public static final glance_scrolling_indicator_parent_height:I = 0x7f0b00aa

.field public static final glance_scrolling_indicator_parent_width:I = 0x7f0b00ab

.field public static final glance_scrolling_indicator_size:I = 0x7f0b00ac

.field public static final glance_scrolling_indicator_width:I = 0x7f0b00ad

.field public static final glance_scrolling_round_size:I = 0x7f0b00ae

.field public static final icon_crumb_marginend:I = 0x7f0b00af

.field public static final icon_crumb_margintop:I = 0x7f0b00b0

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0b00b1

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0b00b2

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f0b00b3

.field public static final kmph_speed_limit_top_margin:I = 0x7f0b00b4

.field public static final lane_guidance_icon_h:I = 0x7f0b00b5

.field public static final lane_guidance_icon_w:I = 0x7f0b00b6

.field public static final lane_guidance_separator_h:I = 0x7f0b00b7

.field public static final lane_guidance_separator_margin:I = 0x7f0b00b8

.field public static final lane_guidance_separator_w:I = 0x7f0b00b9

.field public static final lane_guidance_y:I = 0x7f0b00ba

.field public static final locale_change_icon_size:I = 0x7f0b00bb

.field public static final main_screen_right_section_shrink_x:I = 0x7f0b00bc

.field public static final main_screen_right_section_x:I = 0x7f0b00bd

.field public static final main_section_vertical_padding:I = 0x7f0b00be

.field public static final map_icon_indicator_active_y:I = 0x7f0b00bf

.field public static final map_icon_indicator_bottom_margin:I = 0x7f0b00c0

.field public static final map_icon_indicator_bottom_margin_with_lane:I = 0x7f0b00c1

.field public static final map_icon_indicator_open_y:I = 0x7f0b00c2

.field public static final map_icon_indicator_shrink_left_x:I = 0x7f0b00c3

.field public static final map_icon_indicator_shrink_mode_x:I = 0x7f0b00c4

.field public static final map_icon_indicator_shrink_right_x:I = 0x7f0b00c5

.field public static final map_icon_indicator_x:I = 0x7f0b00c6

.field public static final map_mask_shrink_left_x:I = 0x7f0b00c7

.field public static final map_mask_shrink_right_x:I = 0x7f0b00c8

.field public static final map_mask_shrink_width:I = 0x7f0b00c9

.field public static final map_mask_x:I = 0x7f0b00ca

.field public static final map_view_shrink_left_x:I = 0x7f0b00cb

.field public static final map_view_shrink_mode_x:I = 0x7f0b00cc

.field public static final map_view_shrink_right_x:I = 0x7f0b00cd

.field public static final map_view_x:I = 0x7f0b00ce

.field public static final menu_item_margin:I = 0x7f0b00cf

.field public static final message_options_item_padding_default:I = 0x7f0b00d0

.field public static final message_options_item_padding_large:I = 0x7f0b00d1

.field public static final message_options_item_padding_small:I = 0x7f0b00d2

.field public static final message_options_text_large:I = 0x7f0b00d3

.field public static final message_options_text_small:I = 0x7f0b00d4

.field public static final middle_gauge_full_mode_height:I = 0x7f0b00d5

.field public static final middle_gauge_full_mode_width:I = 0x7f0b00d6

.field public static final middle_gauge_shrink_early_tbt_offset:I = 0x7f0b00d7

.field public static final middle_gauge_shrink_margin:I = 0x7f0b00d8

.field public static final middle_gauge_shrink_mode_margin:I = 0x7f0b00d9

.field public static final middle_gauge_tbt_mode_height:I = 0x7f0b00da

.field public static final middle_gauge_tbt_mode_width:I = 0x7f0b00db

.field public static final min_highlight_view_width:I = 0x7f0b00dc

.field public static final mph_speed_limit_top_margin:I = 0x7f0b00dd

.field public static final music_details_rightC_left_margin:I = 0x7f0b00de

.field public static final music_details_rightC_w:I = 0x7f0b00df

.field public static final music_gauge_album_art_size:I = 0x7f0b00e0

.field public static final music_notification_image_size:I = 0x7f0b00e1

.field public static final near_margin:I = 0x7f0b00e2

.field public static final no_glances_choice_padding:I = 0x7f0b00e3

.field public static final no_glances_width:I = 0x7f0b0000

.field public static final no_location_image_shrink_left_x:I = 0x7f0b00e4

.field public static final no_location_image_shrink_right_x:I = 0x7f0b00e5

.field public static final no_location_image_x:I = 0x7f0b00e6

.field public static final no_location_text_shrink_left_x:I = 0x7f0b00e7

.field public static final no_location_text_shrink_right_x:I = 0x7f0b00e8

.field public static final no_location_text_x:I = 0x7f0b00e9

.field public static final notification_choices_big_text_size:I = 0x7f0b00ea

.field public static final notification_choices_margin_top:I = 0x7f0b00eb

.field public static final notification_choices_padding:I = 0x7f0b00ec

.field public static final notification_choices_small_text_size:I = 0x7f0b00ed

.field public static final notification_icon_align_margin:I = 0x7f0b00ee

.field public static final notification_icon_size:I = 0x7f0b00ef

.field public static final notification_image_margin_top:I = 0x7f0b00f0

.field public static final notification_image_size:I = 0x7f0b00f1

.field public static final notification_subtitle_text_size:I = 0x7f0b00f2

.field public static final notification_text_container_size:I = 0x7f0b00f3

.field public static final notification_text_size:I = 0x7f0b00f4

.field public static final notification_title_margin_top:I = 0x7f0b00f5

.field public static final notification_title_text_size:I = 0x7f0b00f6

.field public static final notification_view_ht:I = 0x7f0b00f7

.field public static final notification_view_width:I = 0x7f0b00f8

.field public static final notifications_highlight_view_height:I = 0x7f0b00f9

.field public static final open_map_height:I = 0x7f0b00fa

.field public static final open_map_road_info_margin:I = 0x7f0b00fb

.field public static final open_map_road_info_shrink_left_margin_l:I = 0x7f0b00fc

.field public static final open_map_road_info_shrink_left_margin_r:I = 0x7f0b00fd

.field public static final open_map_road_info_shrink_right_margin_l:I = 0x7f0b00fe

.field public static final open_map_road_info_shrink_right_margin_r:I = 0x7f0b00ff

.field public static final options_main_image_size:I = 0x7f0b0100

.field public static final options_main_left_padding:I = 0x7f0b0101

.field public static final options_right_image_start:I = 0x7f0b0102

.field public static final options_side_image_size:I = 0x7f0b0103

.field public static final options_view_padding:I = 0x7f0b0104

.field public static final options_view_width:I = 0x7f0b0105

.field public static final recalc_lyt_shrink_left_x:I = 0x7f0b0106

.field public static final recalc_lyt_shrink_right_x:I = 0x7f0b0107

.field public static final recalc_lyt_x:I = 0x7f0b0108

.field public static final right_image_height:I = 0x7f0b0109

.field public static final right_image_left_margin:I = 0x7f0b010a

.field public static final right_image_width:I = 0x7f0b010b

.field public static final route_calc_indicator_x:I = 0x7f0b010c

.field public static final route_calc_info_shrink_left_x:I = 0x7f0b010d

.field public static final route_calc_info_shrink_right_x:I = 0x7f0b010e

.field public static final route_calc_info_shrink_width:I = 0x7f0b010f

.field public static final route_calc_info_width:I = 0x7f0b0110

.field public static final route_calc_info_x:I = 0x7f0b0111

.field public static final route_calc_progress_shrink_left_x:I = 0x7f0b0112

.field public static final route_calc_progress_shrink_right_x:I = 0x7f0b0113

.field public static final route_calc_progress_x:I = 0x7f0b0114

.field public static final route_calc_route_name_x:I = 0x7f0b0115

.field public static final route_overview_box_height:I = 0x7f0b0116

.field public static final route_overview_box_width:I = 0x7f0b0117

.field public static final route_overview_box_x:I = 0x7f0b0118

.field public static final route_overview_box_y:I = 0x7f0b0119

.field public static final route_pick_box_height:I = 0x7f0b011a

.field public static final route_pick_box_width:I = 0x7f0b011b

.field public static final route_pick_box_x:I = 0x7f0b011c

.field public static final route_pick_box_y:I = 0x7f0b011d

.field public static final satellite_view_margin_top:I = 0x7f0b011e

.field public static final satellite_view_width:I = 0x7f0b011f

.field public static final satellite_view_x:I = 0x7f0b0120

.field public static final satellite_width:I = 0x7f0b0121

.field public static final satellite_width_separator:I = 0x7f0b0122

.field public static final shrink_mode_right_gauge_left_margin:I = 0x7f0b0123

.field public static final shrink_mode_speedo_meter_left_margin:I = 0x7f0b0124

.field public static final side_panel_width:I = 0x7f0b0125

.field public static final size_30:I = 0x7f0b0126

.field public static final small_analog_clock_frame_stroke_width:I = 0x7f0b0127

.field public static final small_analog_clock_hour_hand_width:I = 0x7f0b0128

.field public static final small_analog_clock_minute_hand_width:I = 0x7f0b0129

.field public static final small_gauge_height:I = 0x7f0b012a

.field public static final small_gauge_left_margin:I = 0x7f0b012b

.field public static final small_gauge_width:I = 0x7f0b012c

.field public static final small_guage_bar_width:I = 0x7f0b012d

.field public static final small_guage_text_shadowdx:I = 0x7f0b012e

.field public static final small_guage_text_shadowdy:I = 0x7f0b012f

.field public static final smart_dash_active_left_gauge_x:I = 0x7f0b0130

.field public static final smart_dash_active_left_gauge_y:I = 0x7f0b0131

.field public static final smart_dash_active_middle_gauge_h:I = 0x7f0b0132

.field public static final smart_dash_active_middle_gauge_w:I = 0x7f0b0133

.field public static final smart_dash_active_middle_gauge_x:I = 0x7f0b0134

.field public static final smart_dash_active_middle_gauge_y:I = 0x7f0b0135

.field public static final smart_dash_active_right_gauge_x:I = 0x7f0b0136

.field public static final smart_dash_active_right_gauge_y:I = 0x7f0b0137

.field public static final smart_dash_left_gauge_shrink_right_x:I = 0x7f0b0138

.field public static final smart_dash_middle_gauge_active_shrink_left_x:I = 0x7f0b0139

.field public static final smart_dash_middle_gauge_active_shrink_right_x:I = 0x7f0b013a

.field public static final smart_dash_middle_gauge_large_text_size:I = 0x7f0b013b

.field public static final smart_dash_middle_gauge_shrink_left_x:I = 0x7f0b013c

.field public static final smart_dash_middle_gauge_shrink_right_x:I = 0x7f0b013d

.field public static final smart_dash_middle_gauge_small_text_size:I = 0x7f0b013e

.field public static final smart_dash_open_left_gauge_x:I = 0x7f0b013f

.field public static final smart_dash_open_left_gauge_y:I = 0x7f0b0140

.field public static final smart_dash_open_middle_gauge_h:I = 0x7f0b0141

.field public static final smart_dash_open_middle_gauge_w:I = 0x7f0b0142

.field public static final smart_dash_open_middle_gauge_x:I = 0x7f0b0143

.field public static final smart_dash_open_middle_gauge_y:I = 0x7f0b0144

.field public static final smart_dash_open_right_gauge_x:I = 0x7f0b0145

.field public static final smart_dash_open_right_gauge_y:I = 0x7f0b0146

.field public static final smart_dash_open_top_animation_dist:I = 0x7f0b0147

.field public static final smart_dash_right_gauge_shrink_left_x:I = 0x7f0b0148

.field public static final speed_left_x:I = 0x7f0b0149

.field public static final speed_shrink_left_x:I = 0x7f0b014a

.field public static final speed_shrink_right_x:I = 0x7f0b014b

.field public static final speedo_meter_guage_inner_bar_width:I = 0x7f0b014c

.field public static final speedo_meter_guage_outer_bar_width:I = 0x7f0b014d

.field public static final speedometer_gauge_margin:I = 0x7f0b014e

.field public static final speedometer_gauge_speed_limit_text_size:I = 0x7f0b014f

.field public static final speedometer_text_letter_spacing:I = 0x7f0b0150

.field public static final start_fluctuator:I = 0x7f0b0151

.field public static final start_trip_image_container_size:I = 0x7f0b0152

.field public static final start_trip_image_size:I = 0x7f0b0153

.field public static final start_trip_left_margin:I = 0x7f0b0154

.field public static final start_trip_text_left_margin:I = 0x7f0b0155

.field public static final subicon_size:I = 0x7f0b0156

.field public static final subitem_margin_left:I = 0x7f0b0157

.field public static final subitem_margin_top:I = 0x7f0b0158

.field public static final subitem_text_icon_margin:I = 0x7f0b0159

.field public static final subtext_size:I = 0x7f0b015a

.field public static final system_alert_notification_ht:I = 0x7f0b015b

.field public static final system_info_image:I = 0x7f0b015c

.field public static final system_info_text:I = 0x7f0b015d

.field public static final tachometer_full_mode_gauge_background_bottom_offset:I = 0x7f0b015e

.field public static final tachometer_full_mode_gauge_margin_bottom:I = 0x7f0b015f

.field public static final tachometer_full_mode_gauge_margin_left:I = 0x7f0b0160

.field public static final tachometer_full_mode_gauge_margin_right:I = 0x7f0b0161

.field public static final tachometer_full_mode_gauge_margin_top:I = 0x7f0b0162

.field public static final tachometer_full_mode_margin_top:I = 0x7f0b0163

.field public static final tachometer_gauge_background_border_width:I = 0x7f0b0164

.field public static final tachometer_gauge_groove_text_offset:I = 0x7f0b0165

.field public static final tachometer_gauge_groove_text_size:I = 0x7f0b0166

.field public static final tachometer_guage_inner_bar_width:I = 0x7f0b0167

.field public static final tachometer_guage_speed_text_size:I = 0x7f0b0168

.field public static final tachometer_guage_speed_text_size_2:I = 0x7f0b0169

.field public static final tachometer_tbt_mode_gauge_background_bottom_offset:I = 0x7f0b016a

.field public static final tachometer_tbt_mode_gauge_margin_bottom:I = 0x7f0b016b

.field public static final tachometer_tbt_mode_gauge_margin_left:I = 0x7f0b016c

.field public static final tachometer_tbt_mode_gauge_margin_right:I = 0x7f0b016d

.field public static final tachometer_tbt_mode_gauge_margin_top:I = 0x7f0b016e

.field public static final tachometer_tbt_mode_margin_top:I = 0x7f0b016f

.field public static final tbt_direction_full_w:I = 0x7f0b0170

.field public static final tbt_direction_medium_w:I = 0x7f0b0171

.field public static final tbt_distance_full_w:I = 0x7f0b0172

.field public static final tbt_distance_medium_w:I = 0x7f0b0173

.field public static final tbt_distance_now_h_full:I = 0x7f0b0174

.field public static final tbt_distance_now_h_medium:I = 0x7f0b0175

.field public static final tbt_distance_now_w_full:I = 0x7f0b0176

.field public static final tbt_distance_now_w_medium:I = 0x7f0b0177

.field public static final tbt_distance_progress_h_full:I = 0x7f0b0178

.field public static final tbt_distance_progress_h_medium:I = 0x7f0b0179

.field public static final tbt_instruction_18:I = 0x7f0b017a

.field public static final tbt_instruction_20:I = 0x7f0b017b

.field public static final tbt_instruction_22:I = 0x7f0b017c

.field public static final tbt_instruction_24:I = 0x7f0b017d

.field public static final tbt_instruction_26:I = 0x7f0b017e

.field public static final tbt_instruction_full_w:I = 0x7f0b017f

.field public static final tbt_instruction_medium_w:I = 0x7f0b0180

.field public static final tbt_instruction_min_w:I = 0x7f0b0181

.field public static final tbt_instruction_small_w:I = 0x7f0b0182

.field public static final tbt_padding_full:I = 0x7f0b0183

.field public static final tbt_padding_medium:I = 0x7f0b0184

.field public static final tbt_shrinkleft_x:I = 0x7f0b0185

.field public static final tbt_view_ht:I = 0x7f0b0186

.field public static final text_fade_anim_translationy:I = 0x7f0b0187

.field public static final thumb_width:I = 0x7f0b0188

.field public static final time_left_x:I = 0x7f0b0189

.field public static final time_shrink_left_x:I = 0x7f0b018a

.field public static final time_shrink_right_x:I = 0x7f0b018b

.field public static final toast_anim_translation:I = 0x7f0b018c

.field public static final toast_app_disconnected_width:I = 0x7f0b018d

.field public static final toast_destination_suggestion_width:I = 0x7f0b018e

.field public static final toast_installing_update_padding:I = 0x7f0b018f

.field public static final toast_installing_update_width:I = 0x7f0b0190

.field public static final toast_phone_disconnected_width:I = 0x7f0b0191

.field public static final tool_tip_margin:I = 0x7f0b0192

.field public static final tool_tip_max_text_width:I = 0x7f0b0193

.field public static final tool_tip_min_text_width:I = 0x7f0b0194

.field public static final tool_tip_padding:I = 0x7f0b0195

.field public static final tool_tip_triangle_width:I = 0x7f0b0196

.field public static final top_view_left_animation_x_in:I = 0x7f0b0197

.field public static final top_view_left_animation_x_out:I = 0x7f0b0198

.field public static final top_view_right_animation_x_in:I = 0x7f0b0199

.field public static final top_view_right_animation_x_out:I = 0x7f0b019a

.field public static final top_view_speed_out:I = 0x7f0b019b

.field public static final traffic_junc_notif_img_h:I = 0x7f0b019c

.field public static final traffic_junc_notif_img_inflate_h:I = 0x7f0b019d

.field public static final traffic_junc_notif_img_inflate_w:I = 0x7f0b019e

.field public static final traffic_junc_notif_img_w:I = 0x7f0b019f

.field public static final traffic_junc_notif_margin_top:I = 0x7f0b01a0

.field public static final traffic_junc_notif_scrim_h:I = 0x7f0b01a1

.field public static final traffic_junc_notif_scrim_top:I = 0x7f0b01a2

.field public static final transform_center_icon_height:I = 0x7f0b01a3

.field public static final transform_center_icon_width:I = 0x7f0b01a4

.field public static final transform_center_map_on_route_overview_y:I = 0x7f0b01a5

.field public static final transform_center_map_on_route_y:I = 0x7f0b01a6

.field public static final transform_center_overview_route_y:I = 0x7f0b01a7

.field public static final transform_center_overview_x:I = 0x7f0b01a8

.field public static final transform_center_overview_y:I = 0x7f0b01a9

.field public static final transform_center_picker_move_x:I = 0x7f0b01aa

.field public static final transform_center_picker_y:I = 0x7f0b01ab

.field public static final transform_center_shrink_left_x:I = 0x7f0b01ac

.field public static final transform_center_shrink_right_x:I = 0x7f0b01ad

.field public static final transform_center_x:I = 0x7f0b01ae

.field public static final trip_gauge_icon_margin:I = 0x7f0b01af

.field public static final trip_gauge_long_margin:I = 0x7f0b01b0

.field public static final trip_gauge_short_margin:I = 0x7f0b01b1

.field public static final vlist_16_16_subtitle_top:I = 0x7f0b01b2

.field public static final vlist_16_16_title_top:I = 0x7f0b01b3

.field public static final vlist_16_subtitle2_top_m_3:I = 0x7f0b01b4

.field public static final vlist_16_subtitle_top_m_2:I = 0x7f0b01b5

.field public static final vlist_16_subtitle_top_m_3:I = 0x7f0b01b6

.field public static final vlist_16_title_top_m_2:I = 0x7f0b01b7

.field public static final vlist_16_title_top_m_3:I = 0x7f0b01b8

.field public static final vlist_18_16_subtitle_top:I = 0x7f0b01b9

.field public static final vlist_18_16_title_top:I = 0x7f0b01ba

.field public static final vlist_18_subtitle2_top_m_3:I = 0x7f0b01bb

.field public static final vlist_18_subtitle_top_m_2:I = 0x7f0b01bc

.field public static final vlist_18_subtitle_top_m_3:I = 0x7f0b01bd

.field public static final vlist_18_title_top_m_2:I = 0x7f0b01be

.field public static final vlist_18_title_top_m_3:I = 0x7f0b01bf

.field public static final vlist_22_16_subtitle_top:I = 0x7f0b01c0

.field public static final vlist_22_16_title_top:I = 0x7f0b01c1

.field public static final vlist_22_18_subtitle_top:I = 0x7f0b01c2

.field public static final vlist_22_18_title_top:I = 0x7f0b01c3

.field public static final vlist_22_subtitle2_top_m_3:I = 0x7f0b01c4

.field public static final vlist_22_subtitle_top_m_2:I = 0x7f0b01c5

.field public static final vlist_22_subtitle_top_m_3:I = 0x7f0b01c6

.field public static final vlist_22_title_top_m_2:I = 0x7f0b01c7

.field public static final vlist_22_title_top_m_3:I = 0x7f0b01c8

.field public static final vlist_26_16_subtitle_top:I = 0x7f0b01c9

.field public static final vlist_26_16_title_top:I = 0x7f0b01ca

.field public static final vlist_26_18_subtitle_top:I = 0x7f0b01cb

.field public static final vlist_26_18_title_top:I = 0x7f0b01cc

.field public static final vlist_26_subtitle2_top_m_3:I = 0x7f0b01cd

.field public static final vlist_26_subtitle_top_m_2:I = 0x7f0b01ce

.field public static final vlist_26_subtitle_top_m_3:I = 0x7f0b01cf

.field public static final vlist_26_title_top_m_2:I = 0x7f0b01d0

.field public static final vlist_26_title_top_m_3:I = 0x7f0b01d1

.field public static final vlist_height:I = 0x7f0b01d2

.field public static final vlist_icon_list_margin:I = 0x7f0b01d3

.field public static final vlist_image:I = 0x7f0b01d4

.field public static final vlist_image_frame:I = 0x7f0b01d5

.field public static final vlist_item_height:I = 0x7f0b01d6

.field public static final vlist_loading_image:I = 0x7f0b01d7

.field public static final vlist_loading_image_unselected:I = 0x7f0b01d8

.field public static final vlist_margin_left:I = 0x7f0b01d9

.field public static final vlist_picker_subtitle:I = 0x7f0b01da

.field public static final vlist_picker_title:I = 0x7f0b01db

.field public static final vlist_scrim_w:I = 0x7f0b01dc

.field public static final vlist_small_image:I = 0x7f0b01dd

.field public static final vlist_subtitle:I = 0x7f0b01de

.field public static final vlist_text_left_margin:I = 0x7f0b01df

.field public static final vlist_title:I = 0x7f0b01e0

.field public static final vlist_title_16:I = 0x7f0b01e1

.field public static final vlist_title_18:I = 0x7f0b01e2

.field public static final vlist_title_22:I = 0x7f0b01e3

.field public static final vlist_title_text_len:I = 0x7f0b01e4

.field public static final vlist_toggle_switch_height:I = 0x7f0b01e5

.field public static final vlist_toggle_switch_thumb_margin:I = 0x7f0b01e6

.field public static final vlist_toggle_switch_width:I = 0x7f0b01e7

.field public static final vmenu_anim_translate_y:I = 0x7f0b01e8

.field public static final vmenu_close_height:I = 0x7f0b01e9

.field public static final vmenu_close_scroll_y:I = 0x7f0b01ea

.field public static final vmenu_close_top_margin:I = 0x7f0b01eb

.field public static final vmenu_indicator_h:I = 0x7f0b01ec

.field public static final vmenu_indicator_margin_right:I = 0x7f0b01ed

.field public static final vmenu_indicator_w:I = 0x7f0b01ee

.field public static final vmenu_indicator_y:I = 0x7f0b01ef

.field public static final vmenu_root_top_offset:I = 0x7f0b01f0

.field public static final vmenu_sel_image_x:I = 0x7f0b01f1

.field public static final vmenu_sel_image_y:I = 0x7f0b01f2

.field public static final vmenu_sel_text_x:I = 0x7f0b01f3

.field public static final vmenu_selected_image:I = 0x7f0b01f4

.field public static final welcome_screen_anim_end_radius:I = 0x7f0b01f5

.field public static final welcome_screen_anim_start_radius:I = 0x7f0b01f6

.field public static final welcome_screen_anim_stroke_width:I = 0x7f0b01f7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
