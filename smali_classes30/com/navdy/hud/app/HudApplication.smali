.class public Lcom/navdy/hud/app/HudApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "HudApplication.java"


# static fields
.field public static final NOT_A_CRASH:Ljava/lang/String; = "NAVDY_NOT_A_CRASH"

.field private static final TAG:Ljava/lang/String; = "HudApplication"

.field private static sAppContext:Landroid/content/Context;

.field private static sApplication:Lcom/navdy/hud/app/HudApplication;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private initialized:Z

.field private lastSeenReason:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field private rootScope:Lmortar/MortarScope;

.field private updateReminderManager:Lcom/navdy/hud/app/manager/UpdateReminderManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/HudApplication;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/HudApplication;->handler:Landroid/os/Handler;

    .line 481
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->UNKNOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    iput-object v0, p0, Lcom/navdy/hud/app/HudApplication;->lastSeenReason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getApplication()Lcom/navdy/hud/app/HudApplication;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/navdy/hud/app/HudApplication;->sApplication:Lcom/navdy/hud/app/HudApplication;

    return-object v0
.end method

.method private initConnectionService(Ljava/lang/String;)V
    .locals 6
    .param p1, "processName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 284
    sget-object v2, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":initializing taskMgr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    .line 286
    .local v1, "taskManager":Lcom/navdy/service/library/task/TaskManager;
    const/4 v2, 0x3

    invoke-virtual {v1, v5, v2}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 287
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v5}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 288
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v5}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 290
    invoke-virtual {v1}, Lcom/navdy/service/library/task/TaskManager;->init()V

    .line 292
    sget-object v2, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":initializing gps manager"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsManager;

    .line 296
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.navdy.service.library.log.action.RELOAD"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 297
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v2, Lcom/navdy/hud/app/receiver/LogLevelReceiver;

    invoke-direct {v2}, Lcom/navdy/hud/app/receiver/LogLevelReceiver;-><init>()V

    invoke-virtual {p0, v2, v0}, Lcom/navdy/hud/app/HudApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 298
    return-void
.end method

.method private static initLogger(Z)V
    .locals 3
    .param p0, "initializingApp"    # Z

    .prologue
    .line 234
    const/4 v1, 0x1

    new-array v0, v1, [Lcom/navdy/service/library/log/LogAppender;

    .line 235
    .local v0, "logAppenders":[Lcom/navdy/service/library/log/LogAppender;
    const/4 v1, 0x0

    new-instance v2, Lcom/navdy/service/library/log/LogcatAppender;

    invoke-direct {v2}, Lcom/navdy/service/library/log/LogcatAppender;-><init>()V

    aput-object v2, v0, v1

    .line 236
    invoke-static {v0}, Lcom/navdy/service/library/log/Logger;->init([Lcom/navdy/service/library/log/LogAppender;)V

    .line 237
    return-void
.end method

.method private initTaskManager(Ljava/lang/String;)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;

    .prologue
    .line 241
    sget-object v2, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":initializing taskMgr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    .line 244
    .local v1, "taskManager":Lcom/navdy/service/library/task/TaskManager;
    const/4 v2, 0x1

    const/4 v3, 0x3

    :try_start_0
    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 245
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 246
    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 247
    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 248
    const/4 v2, 0x7

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 249
    const/16 v2, 0x9

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 250
    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 251
    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 252
    const/16 v2, 0xc

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 253
    const/16 v2, 0xd

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 254
    const/16 v2, 0xe

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 255
    const/16 v2, 0x16

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 258
    const/16 v2, 0xf

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 259
    const/16 v2, 0x10

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 260
    const/16 v2, 0x11

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 261
    const/16 v2, 0x12

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 262
    const/4 v2, 0x2

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 263
    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 264
    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 265
    const/16 v2, 0x13

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 266
    const/16 v2, 0x14

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 267
    const/16 v2, 0x15

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 269
    const/16 v2, 0x17

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->addTaskQueue(II)V

    .line 271
    invoke-virtual {v1}, Lcom/navdy/service/library/task/TaskManager;->init()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :cond_0
    return-void

    .line 272
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "already initialized"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 276
    throw v0
.end method

.method public static isDeveloperBuild()Z
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    sput-object p0, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    .line 103
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-static {p1}, Lcom/navdy/hud/app/profile/HudLocale;->onAttach(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/multidex/MultiDexApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/navdy/hud/app/HudApplication;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public getRootScope()Lmortar/MortarScope;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/HudApplication;->rootScope:Lmortar/MortarScope;

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 217
    invoke-static {p1}, Lmortar/Mortar;->isScopeSystemService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/app/HudApplication;->rootScope:Lmortar/MortarScope;

    .line 220
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public initHudApp()V
    .locals 18

    .prologue
    .line 305
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/HudApplication;->initialized:Z

    if-eqz v14, :cond_0

    .line 469
    :goto_0
    return-void

    .line 308
    :cond_0
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/navdy/hud/app/HudApplication;->initialized:Z

    .line 310
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v15

    invoke-static {v14, v15}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 313
    .local v9, "processName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->loadlibrary()V

    .line 315
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->isEnabled()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 316
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":initializing crash reporter"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v14

    sget-object v15, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    invoke-virtual {v14, v15}, Lcom/navdy/hud/app/util/CrashReporter;->installCrashHandler(Landroid/content/Context;)V

    .line 323
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 324
    invoke-static {}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->run()V

    .line 325
    invoke-static {}, Lcom/navdy/hud/app/device/ProjectorBrightness;->init()V

    .line 328
    :cond_1
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":updating logging setup"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 329
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/navdy/hud/app/HudApplication;->initLogger(Z)V

    .line 331
    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    .line 332
    .local v7, "otaFilter":Landroid/content/IntentFilter;
    const-string v14, "com.navdy.hud.app.service.OTA_DOWNLOAD"

    invoke-virtual {v7, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 334
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    .line 335
    .local v8, "prefs":Landroid/content/SharedPreferences;
    if-nez v8, :cond_6

    .line 336
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "unable to get SharedPreferences"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 344
    :goto_2
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initPicasso(Landroid/content/Context;)V

    .line 347
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 351
    :try_start_0
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "dbase:opening..."

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 352
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 353
    .local v4, "l1":J
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 354
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "dbase: opened["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    sub-long v16, v16, v4

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    .end local v4    # "l1":J
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->analyticsApplicationInit(Landroid/app/Application;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/HudApplication;->rootScope:Lmortar/MortarScope;

    invoke-interface {v14}, Lmortar/MortarScope;->getObjectGraph()Ldagger/ObjectGraph;

    move-result-object v6

    .line 371
    .local v6, "objectGraph":Ldagger/ObjectGraph;
    const-class v14, Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v6, v14}, Ldagger/ObjectGraph;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/debug/DriveRecorder;

    .line 372
    .local v2, "dataRecorder":Lcom/navdy/hud/app/debug/DriveRecorder;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v14

    new-instance v15, Lcom/navdy/hud/app/HudApplication$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/navdy/hud/app/HudApplication$1;-><init>(Lcom/navdy/hud/app/HudApplication;Lcom/navdy/hud/app/debug/DriveRecorder;)V

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 379
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":initializing here maps engine"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 380
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 381
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":called initialized"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 383
    sget-object v11, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedMph:Ljava/lang/String;

    .line 384
    .local v11, "s":Ljava/lang/String;
    sget-object v10, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->routeOverviewRect:Lcom/here/android/mpa/common/ViewRect;

    .line 386
    .local v10, "rect":Lcom/here/android/mpa/common/ViewRect;
    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeShrinkLeftX:I

    .line 387
    .local v3, "n":I
    sget-object v11, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_EXTREMELY_LOW:Ljava/lang/String;

    .line 388
    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFacebook:I

    .line 389
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->initMessageAttributes()V

    .line 392
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->isEnabled()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 394
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/HudApplication;->handler:Landroid/os/Handler;

    new-instance v15, Lcom/navdy/hud/app/HudApplication$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/navdy/hud/app/HudApplication$2;-><init>(Lcom/navdy/hud/app/HudApplication;)V

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 408
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/HudApplication;->handler:Landroid/os/Handler;

    new-instance v15, Lcom/navdy/hud/app/HudApplication$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/navdy/hud/app/HudApplication$3;-><init>(Lcom/navdy/hud/app/HudApplication;)V

    const-wide/32 v16, 0xafc8

    invoke-virtual/range {v14 .. v17}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 425
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    .line 429
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    .line 430
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->getInstance()Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .line 431
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    .line 432
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    .line 433
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    .line 437
    invoke-static {}, Lcom/navdy/hud/app/framework/message/MessageManager;->getInstance()Lcom/navdy/hud/app/framework/message/MessageManager;

    .line 438
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    .line 439
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->initialize()V

    .line 440
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    .line 441
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v14

    if-nez v14, :cond_3

    .line 443
    invoke-static {}, Lcom/navdy/hud/app/audio/SoundUtils;->init()V

    .line 445
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;

    .line 446
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 447
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "start dead reckoning mgr"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 448
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .line 450
    :cond_4
    new-instance v14, Lcom/navdy/hud/app/manager/UpdateReminderManager;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/HudApplication;->updateReminderManager:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    .line 452
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "init network b/w controller"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 453
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .line 456
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->getInstance()Lcom/navdy/hud/app/util/os/CpuProfiler;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/util/os/CpuProfiler;->start()V

    .line 459
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/HudApplication;->handler:Landroid/os/Handler;

    new-instance v15, Lcom/navdy/hud/app/HudApplication$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/navdy/hud/app/HudApplication$4;-><init>(Lcom/navdy/hud/app/HudApplication;)V

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 468
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":background init done"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 320
    .end local v2    # "dataRecorder":Lcom/navdy/hud/app/debug/DriveRecorder;
    .end local v3    # "n":I
    .end local v6    # "objectGraph":Ldagger/ObjectGraph;
    .end local v7    # "otaFilter":Landroid/content/IntentFilter;
    .end local v8    # "prefs":Landroid/content/SharedPreferences;
    .end local v10    # "rect":Lcom/here/android/mpa/common/ViewRect;
    .end local v11    # "s":Ljava/lang/String;
    :cond_5
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":crash reporter not installed"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 338
    .restart local v7    # "otaFilter":Landroid/content/IntentFilter;
    .restart local v8    # "prefs":Landroid/content/SharedPreferences;
    :cond_6
    new-instance v14, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    invoke-direct {v14, v8}, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;-><init>(Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v7}, Lcom/navdy/hud/app/HudApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 355
    :catch_0
    move-exception v12

    .line 356
    .local v12, "t":Ljava/lang/Throwable;
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "dbase: error opening, deleting it"

    invoke-virtual {v14, v15, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 357
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->deleteDatabaseFile()V

    .line 358
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "dbase: deletinged"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 360
    :try_start_1
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "dbase:re-opening"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 361
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 362
    .restart local v4    # "l1":J
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 363
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "dbase: opened["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    sub-long v16, v16, v4

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_3

    .line 364
    .end local v4    # "l1":J
    :catch_1
    move-exception v13

    .line 365
    .local v13, "throwable":Ljava/lang/Throwable;
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "dbase: error opening"

    invoke-virtual {v14, v15, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3
.end method

.method public onCreate()V
    .locals 18

    .prologue
    .line 131
    const-string v14, ""

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "::onCreate locale:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/HudApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    sput-object p0, Lcom/navdy/hud/app/HudApplication;->sApplication:Lcom/navdy/hud/app/HudApplication;

    .line 133
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sApplication:Lcom/navdy/hud/app/HudApplication;

    sput-object v14, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    .line 134
    invoke-super/range {p0 .. p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 136
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sAppContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v15

    invoke-static {v14, v15}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v13

    .line 137
    .local v13, "processName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/HudApplication;->getPackageName()Ljava/lang/String;

    move-result-object v12

    .line 139
    .local v12, "packageName":Ljava/lang/String;
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/navdy/hud/app/HudApplication;->initLogger(Z)V

    .line 141
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " starting  on "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 144
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":connectionService"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 146
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "startup:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " :connection service"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 147
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/navdy/hud/app/HudApplication;->initConnectionService(Ljava/lang/String;)V

    .line 208
    :goto_0
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "startup:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " :initialization done"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    return-void

    .line 148
    :cond_0
    invoke-virtual {v13, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 151
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    .line 152
    .local v5, "locale":Ljava/util/Locale;
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "startup:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " :hud app locale="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":initializing Mortar"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 156
    .local v6, "l1":J
    new-instance v10, Lcom/navdy/hud/app/common/ProdModule;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/navdy/hud/app/common/ProdModule;-><init>(Landroid/content/Context;)V

    .line 157
    .local v10, "module":Lcom/navdy/hud/app/common/ProdModule;
    const/4 v14, 0x0

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v15}, Ldagger/ObjectGraph;->create([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v15

    invoke-static {v14, v15}, Lmortar/Mortar;->createRootScope(ZLdagger/ObjectGraph;)Lmortar/MortarScope;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/HudApplication;->rootScope:Lmortar/MortarScope;

    .line 158
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/HudApplication;->rootScope:Lmortar/MortarScope;

    invoke-interface {v14}, Lmortar/MortarScope;->getObjectGraph()Ldagger/ObjectGraph;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Ldagger/ObjectGraph;->inject(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 161
    .local v8, "l2":J
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":mortar init took:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v8, v6

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 172
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/navdy/hud/app/HudApplication;->initTaskManager(Ljava/lang/String;)V

    .line 175
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "init network state manager"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .line 179
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "*** starting hud sticky-service ***"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 180
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 181
    .local v4, "intent":Landroid/content/Intent;
    const-class v14, Lcom/navdy/hud/app/service/StickyService;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v14}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 182
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/HudApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 183
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "*** started hud sticky-service ***"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 185
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 186
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v14, "GPS_Switch"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 187
    const-string v14, "GPS_WARM_RESET_UBLOX"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string v14, "GPS_SATELLITE_STATUS"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    const-string v14, "GPS_COLLECT_LOGS"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 190
    const-string v14, "driving_started"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    const-string v14, "driving_stopped"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 192
    const-string v14, "GPS_ENABLE_ESF_RAW"

    invoke-virtual {v3, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 193
    new-instance v14, Lcom/navdy/hud/app/maps/GpsEventsReceiver;

    invoke-direct {v14}, Lcom/navdy/hud/app/maps/GpsEventsReceiver;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Lcom/navdy/hud/app/HudApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 194
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "registered GpsEventsReceiver"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 197
    .local v2, "analyticsFilter":Landroid/content/IntentFilter;
    const-string v14, "com.navdy.hud.app.analytics.AnalyticsEvent"

    invoke-virtual {v2, v14}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 198
    new-instance v14, Lcom/navdy/hud/app/analytics/AnalyticsSupport$AnalyticsIntentsReceiver;

    invoke-direct {v14}, Lcom/navdy/hud/app/analytics/AnalyticsSupport$AnalyticsIntentsReceiver;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Lcom/navdy/hud/app/HudApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 199
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "registered AnalyticsIntentsReceiver"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 201
    new-instance v11, Landroid/content/Intent;

    const-class v14, Lcom/navdy/hud/app/ui/activity/MainActivity;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 202
    .local v11, "myIntent":Landroid/content/Intent;
    const/high16 v14, 0x10000000

    invoke-virtual {v11, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 203
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/HudApplication;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 206
    .end local v2    # "analyticsFilter":Landroid/content/IntentFilter;
    .end local v3    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "locale":Ljava/util/Locale;
    .end local v6    # "l1":J
    .end local v8    # "l2":J
    .end local v10    # "module":Lcom/navdy/hud/app/common/ProdModule;
    .end local v11    # "myIntent":Landroid/content/Intent;
    :cond_1
    sget-object v14, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "startup:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " :no-op"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setShutdownReason(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
    .locals 0
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/navdy/hud/app/HudApplication;->lastSeenReason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 485
    return-void
.end method

.method public shutdown()V
    .locals 6

    .prologue
    .line 488
    sget-object v2, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "shutting down HUD"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 491
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/util/CrashReporter;->stopCrashReporting(Z)V

    .line 494
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 495
    .local v1, "intent":Landroid/content/Intent;
    const-class v2, Lcom/navdy/hud/app/service/StickyService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 496
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/HudApplication;->stopService(Landroid/content/Intent;)Z

    .line 500
    iget-object v2, p0, Lcom/navdy/hud/app/HudApplication;->bus:Lcom/squareup/otto/Bus;

    if-eqz v2, :cond_0

    .line 501
    sget-object v2, Lcom/navdy/hud/app/HudApplication;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shutting down HUD - reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/HudApplication;->lastSeenReason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 502
    iget-object v2, p0, Lcom/navdy/hud/app/HudApplication;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/Shutdown;

    iget-object v4, p0, Lcom/navdy/hud/app/HudApplication;->lastSeenReason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    sget-object v5, Lcom/navdy/hud/app/event/Shutdown$State;->SHUTTING_DOWN:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-direct {v3, v4, v5}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;Lcom/navdy/hud/app/event/Shutdown$State;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 505
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    .line 506
    .local v0, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->shutdown()V

    .line 507
    return-void
.end method
