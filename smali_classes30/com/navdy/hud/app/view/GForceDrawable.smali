.class public Lcom/navdy/hud/app/view/GForceDrawable;
.super Lcom/navdy/hud/app/view/drawable/CustomDrawable;
.source "GForceDrawable.java"


# static fields
.field private static final BACKGROUND:I = 0x0

.field private static final DANGER:I = 0x4

.field private static final EXTREME:I = 0x5

.field private static final HIGHLIGHT:I = 0x2

.field public static final HIGH_G:F = 1.05f

.field private static final LOW:I = 0x1

.field public static final LOW_G:F = 0.45f

.field public static final MAX_ACCEL:F = 2.0f

.field public static final MEDIUM_G:F = 0.75f

.field public static final NO_G:F = 0.15f

.field private static final WARNING:I = 0x3

.field private static typeface:Landroid/graphics/Typeface;


# instance fields
.field private colors:[I

.field protected logger:Lcom/navdy/service/library/log/Logger;

.field private mTextPaint:Landroid/graphics/Paint;

.field private textColor:I

.field private xAccel:F

.field private yAccel:F

.field private zAccel:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;-><init>()V

    .line 24
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->logger:Lcom/navdy/service/library/log/Logger;

    .line 47
    iget-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 50
    sget-object v1, Lcom/navdy/hud/app/view/GForceDrawable;->typeface:Landroid/graphics/Typeface;

    if-nez v1, :cond_0

    .line 51
    const-string v1, "sans-serif-medium"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/view/GForceDrawable;->typeface:Landroid/graphics/Typeface;

    .line 54
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    .line 55
    iget-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0a000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    .line 59
    const v1, 0x7f0d001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->textColor:I

    .line 60
    return-void
.end method

.method private clamp(FFF)F
    .locals 1
    .param p1, "val"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    .line 173
    cmpg-float v0, p1, p2

    if-gez v0, :cond_1

    .line 174
    move p1, p2

    .line 178
    :cond_0
    :goto_0
    return p1

    .line 175
    :cond_1
    cmpl-float v0, p1, p3

    if-lez v0, :cond_0

    .line 176
    move p1, p3

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 40
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 80
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/GForceDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v13

    .line 84
    .local v13, "bounds":Landroid/graphics/Rect;
    const/16 v25, 0x5

    .line 86
    .local v25, "margin":I
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/lit8 v33, v2, -0xa

    .line 87
    .local v33, "width":I
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/lit8 v22, v2, -0xa

    .line 89
    .local v22, "height":I
    div-int/lit8 v2, v22, 0x2

    int-to-float v0, v2

    move/from16 v16, v0

    .line 90
    .local v16, "centerY":F
    div-int/lit8 v2, v33, 0x2

    int-to-float v15, v2

    .line 92
    .local v15, "centerX":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->xAccel:F

    const/high16 v4, -0x40000000    # -2.0f

    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/navdy/hud/app/view/GForceDrawable;->clamp(FFF)F

    move-result v34

    .line 93
    .local v34, "x":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->yAccel:F

    const/high16 v4, -0x40000000    # -2.0f

    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/navdy/hud/app/view/GForceDrawable;->clamp(FFF)F

    move-result v35

    .line 94
    .local v35, "y":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->zAccel:F

    const/high16 v4, -0x40000000    # -2.0f

    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/navdy/hud/app/view/GForceDrawable;->clamp(FFF)F

    move-result v36

    .line 96
    .local v36, "z":F
    const/16 v26, 0x0

    .line 97
    .local v26, "oldGauge":Z
    if-eqz v26, :cond_3

    .line 98
    const/16 v29, 0x2

    .line 99
    .local v29, "range":I
    const/4 v2, 0x4

    div-int v27, v33, v2

    .line 100
    .local v27, "pixelsPerGee":I
    const/16 v2, 0xa

    const/4 v4, 0x1

    div-int/lit8 v5, v27, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v30

    .line 101
    .local v30, "subTicks":I
    mul-int v32, v29, v30

    .line 102
    .local v32, "ticks":I
    move/from16 v0, v32

    neg-int v0, v0

    move/from16 v23, v0

    .local v23, "i":I
    :goto_0
    move/from16 v0, v23

    move/from16 v1, v32

    if-gt v0, v1, :cond_2

    .line 103
    move/from16 v0, v25

    int-to-float v2, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v23

    int-to-float v5, v0

    move/from16 v0, v32

    int-to-float v7, v0

    div-float/2addr v5, v7

    add-float/2addr v4, v5

    mul-float/2addr v4, v15

    add-float v3, v2, v4

    .line 104
    .local v3, "tickX":F
    rem-int v2, v23, v30

    if-nez v2, :cond_0

    const/16 v24, 0x1

    .line 105
    .local v24, "majorTick":Z
    :goto_1
    if-eqz v24, :cond_1

    const/16 v31, 0xa

    .line 106
    .local v31, "tickHeight":I
    :goto_2
    move/from16 v0, v25

    int-to-float v2, v0

    add-float v2, v2, v16

    div-int/lit8 v4, v31, 0x2

    int-to-float v4, v4

    sub-float v4, v2, v4

    move/from16 v0, v25

    int-to-float v2, v0

    add-float v2, v2, v16

    div-int/lit8 v5, v31, 0x2

    int-to-float v5, v5

    add-float v6, v2, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 108
    move/from16 v0, v25

    int-to-float v2, v0

    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v23

    int-to-float v5, v0

    move/from16 v0, v32

    int-to-float v7, v0

    div-float/2addr v5, v7

    add-float/2addr v4, v5

    mul-float v4, v4, v16

    add-float v6, v2, v4

    .line 109
    .local v6, "tickY":F
    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    div-int/lit8 v4, v31, 0x2

    int-to-float v4, v4

    sub-float v5, v2, v4

    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    div-int/lit8 v4, v31, 0x2

    int-to-float v4, v4

    add-float v7, v2, v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    move v8, v6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 102
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .line 104
    .end local v6    # "tickY":F
    .end local v24    # "majorTick":Z
    .end local v31    # "tickHeight":I
    :cond_0
    const/16 v24, 0x0

    goto :goto_1

    .line 105
    .restart local v24    # "majorTick":Z
    :cond_1
    const/16 v31, 0x5

    goto :goto_2

    .line 111
    .end local v3    # "tickX":F
    .end local v24    # "majorTick":Z
    :cond_2
    const/high16 v2, 0x40800000    # 4.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/GForceDrawable;->zAccel:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    add-float v28, v2, v4

    .line 112
    .local v28, "r":F
    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    mul-float v4, v34, v15

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    move/from16 v0, v25

    int-to-float v4, v0

    add-float v4, v4, v16

    mul-float v5, v35, v16

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v5, v7

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 170
    .end local v23    # "i":I
    .end local v27    # "pixelsPerGee":I
    .end local v29    # "range":I
    .end local v30    # "subTicks":I
    .end local v32    # "ticks":I
    :goto_3
    return-void

    .line 116
    .end local v28    # "r":F
    :cond_3
    invoke-static/range {v34 .. v34}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static/range {v35 .. v35}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v19

    .line 118
    .local v19, "g":F
    const v20, 0x3f19999a    # 0.6f

    .line 121
    .local v20, "geeRange":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 122
    const/high16 v28, 0x42480000    # 50.0f

    .line 123
    .restart local v28    # "r":F
    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    move/from16 v0, v25

    int-to-float v4, v0

    add-float v4, v4, v16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 124
    move/from16 v0, v35

    neg-float v2, v0

    div-float v2, v2, v34

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v38, 0x4066800000000000L    # 180.0

    mul-double v4, v4, v38

    const-wide v38, 0x400921fb54442d18L    # Math.PI

    div-double v4, v4, v38

    double-to-float v14, v4

    .line 125
    .local v14, "centerAngle":F
    const/4 v2, 0x0

    cmpg-float v2, v34, v2

    if-gez v2, :cond_4

    .line 126
    const/high16 v2, 0x43340000    # 180.0f

    add-float/2addr v14, v2

    .line 129
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v4, 0x1

    aget v17, v2, v4

    .line 131
    .local v17, "color":I
    const v2, 0x3e19999a    # 0.15f

    cmpl-float v2, v19, v2

    if-lez v2, :cond_5

    .line 132
    const/high16 v2, 0x41a00000    # 20.0f

    const/high16 v4, 0x428c0000    # 70.0f

    const v5, 0x3e19999a    # 0.15f

    const/high16 v7, 0x3f400000    # 0.75f

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v5, v7}, Lcom/navdy/hud/app/view/GForceDrawable;->clamp(FFF)F

    move-result v5

    const v7, 0x3e19999a    # 0.15f

    sub-float/2addr v5, v7

    mul-float/2addr v4, v5

    const v5, 0x3f19999a    # 0.6f

    div-float/2addr v4, v5

    add-float v10, v2, v4

    .line 133
    .local v10, "sweepAngle":F
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v10, v2

    sub-float v9, v14, v2

    .line 134
    .local v9, "startAngle":F
    const v2, 0x3ee66666    # 0.45f

    cmpg-float v2, v19, v2

    if-gez v2, :cond_6

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v4, 0x2

    aget v17, v2, v4

    .line 143
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    new-instance v8, Landroid/graphics/RectF;

    const/high16 v2, 0x42480000    # 50.0f

    sub-float v2, v2, v28

    const/high16 v4, 0x42480000    # 50.0f

    sub-float v4, v4, v28

    const/high16 v5, 0x42480000    # 50.0f

    add-float v5, v5, v28

    const/high16 v7, 0x42480000    # 50.0f

    add-float v7, v7, v28

    invoke-direct {v8, v2, v4, v5, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 145
    .local v8, "boundsF":Landroid/graphics/RectF;
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 148
    .end local v8    # "boundsF":Landroid/graphics/RectF;
    .end local v9    # "startAngle":F
    .end local v10    # "sweepAngle":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 151
    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    move/from16 v0, v25

    int-to-float v4, v0

    add-float v4, v4, v16

    const/high16 v5, 0x42100000    # 36.0f

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/GForceDrawable;->textColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 157
    const-string v2, "G"

    move/from16 v0, v25

    int-to-float v4, v0

    add-float/2addr v4, v15

    move/from16 v0, v25

    int-to-float v5, v0

    add-float v5, v5, v16

    const/high16 v7, 0x41900000    # 18.0f

    sub-float/2addr v5, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 158
    const-string v2, "-"

    move/from16 v0, v25

    int-to-float v4, v0

    add-float/2addr v4, v15

    move/from16 v0, v25

    int-to-float v5, v0

    add-float v5, v5, v16

    const/high16 v7, 0x41e00000    # 28.0f

    add-float/2addr v5, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x41e00000    # 28.0f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Lcom/navdy/hud/app/view/GForceDrawable;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 165
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->xAccel:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->yAccel:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->zAccel:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-nez v2, :cond_9

    const/16 v18, 0x1

    .line 166
    .local v18, "dataInvalid":Z
    :goto_5
    if-eqz v18, :cond_a

    const-string v21, "-"

    .line 167
    .local v21, "gees":Ljava/lang/String;
    :goto_6
    move/from16 v0, v25

    int-to-float v2, v0

    add-float/2addr v2, v15

    move/from16 v0, v25

    int-to-float v4, v0

    add-float v4, v4, v16

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/GForceDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 136
    .end local v18    # "dataInvalid":Z
    .end local v21    # "gees":Ljava/lang/String;
    .restart local v9    # "startAngle":F
    .restart local v10    # "sweepAngle":F
    :cond_6
    const/high16 v2, 0x3f400000    # 0.75f

    cmpg-float v2, v19, v2

    if-gez v2, :cond_7

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v4, 0x3

    aget v17, v2, v4

    goto/16 :goto_4

    .line 138
    :cond_7
    const v2, 0x3f866666    # 1.05f

    cmpg-float v2, v19, v2

    if-gez v2, :cond_8

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v4, 0x4

    aget v17, v2, v4

    goto/16 :goto_4

    .line 141
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/GForceDrawable;->colors:[I

    const/4 v4, 0x5

    aget v17, v2, v4

    goto/16 :goto_4

    .line 165
    .end local v9    # "startAngle":F
    .end local v10    # "sweepAngle":F
    :cond_9
    const/16 v18, 0x0

    goto :goto_5

    .line 166
    .restart local v18    # "dataInvalid":Z
    :cond_a
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%01.2f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v5, v7

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    goto :goto_6
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 71
    return-void
.end method

.method public setAcceleration(FFF)V
    .locals 0
    .param p1, "xAccel"    # F
    .param p2, "yAccel"    # F
    .param p3, "zAccel"    # F

    .prologue
    .line 63
    iput p1, p0, Lcom/navdy/hud/app/view/GForceDrawable;->xAccel:F

    .line 64
    iput p2, p0, Lcom/navdy/hud/app/view/GForceDrawable;->yAccel:F

    .line 65
    iput p3, p0, Lcom/navdy/hud/app/view/GForceDrawable;->zAccel:F

    .line 66
    return-void
.end method
