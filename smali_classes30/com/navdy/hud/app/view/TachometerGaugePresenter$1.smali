.class Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;
.super Ljava/lang/Object;
.source "TachometerGaugePresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/TachometerGaugePresenter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    new-instance v1, Landroid/animation/ValueAnimator;

    invoke-direct {v1}, Landroid/animation/ValueAnimator;-><init>()V

    # setter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$002(Lcom/navdy/hud/app/view/TachometerGaugePresenter;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$1;-><init>(Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;-><init>(Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 110
    return-void

    .line 75
    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method
