.class Lcom/navdy/hud/app/view/WelcomeView$4;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/WelcomeView;->initializeCarousel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/WelcomeView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCurrentItemChanged(II)V
    .locals 6
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 369
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v4, v1, Lcom/navdy/hud/app/view/WelcomeView;->leftDot:Landroid/widget/ImageView;

    const/4 v1, 0x1

    if-le p1, v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView;->rightDot:Landroid/widget/ImageView;

    add-int/lit8 v4, p1, 0x2

    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v5, v5, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 372
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getModel(I)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v0

    .line 374
    .local v0, "model":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->updateInfo(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V
    invoke-static {v1, v0}, Lcom/navdy/hud/app/view/WelcomeView;->access$1100(Lcom/navdy/hud/app/view/WelcomeView;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V

    .line 376
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    iget v2, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-virtual {v1, p1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->onCurrentItemChanged(II)V

    .line 379
    :cond_0
    return-void

    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    :cond_1
    move v1, v3

    .line 369
    goto :goto_0

    :cond_2
    move v2, v3

    .line 370
    goto :goto_1
.end method

.method public onCurrentItemChanging(III)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "newPos"    # I
    .param p3, "id"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 359
    if-le p2, p1, :cond_1

    .line 360
    iget-object v2, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/WelcomeView;->rightDot:Landroid/widget/ImageView;

    add-int/lit8 v3, p2, 0x2

    iget-object v4, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v4, v4, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getModel(I)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->updateInfo(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/WelcomeView;->access$1100(Lcom/navdy/hud/app/view/WelcomeView;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V

    .line 365
    return-void

    :cond_0
    move v0, v1

    .line 360
    goto :goto_0

    .line 362
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/WelcomeView;->leftDot:Landroid/widget/ImageView;

    const/4 v3, 0x1

    if-le p2, v3, :cond_2

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public onExecuteItem(II)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 383
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$4;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->executeItem(II)V

    .line 386
    :cond_0
    return-void
.end method

.method public onExit()V
    .locals 0

    .prologue
    .line 389
    return-void
.end method
