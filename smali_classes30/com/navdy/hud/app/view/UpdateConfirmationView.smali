.class public Lcom/navdy/hud/app/view/UpdateConfirmationView;
.super Landroid/widget/RelativeLayout;
.source "UpdateConfirmationView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final CONFIRMATION_TIMEOUT:J = 0x7530L

.field private static final UPDATE_MESSAGE_MAX_WIDTH:I = 0x168

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private handler:Landroid/os/Handler;

.field private isReminder:Z

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mInfoText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field mLefttSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d3
    .end annotation
.end field

.field mMainTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mRightSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d4
    .end annotation
.end field

.field mScreenTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mVersionText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008b
    .end annotation
.end field

.field mainSection:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cd
    .end annotation
.end field

.field private timeout:Ljava/lang/Runnable;

.field private updateVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/UpdateConfirmationView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/view/UpdateConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/UpdateConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/view/UpdateConfirmationView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/UpdateConfirmationView$1;-><init>(Lcom/navdy/hud/app/view/UpdateConfirmationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    .line 91
    const-string v0, "1.3.2887"

    iput-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->updateVersion:Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isReminder:Z

    .line 86
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 89
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public executeItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 158
    packed-switch p1, :pswitch_data_0

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->install()V

    .line 161
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->finish()V

    .line 162
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isReminder:Z

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->updateVersion:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordUpdatePrompt(ZZLjava/lang/String;)V

    goto :goto_0

    .line 167
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->finish()V

    .line 168
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isReminder:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->updateVersion:Ljava/lang/String;

    invoke-static {v2, v2, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordUpdatePrompt(ZZLjava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 176
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 142
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 147
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 148
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 149
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 153
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatMatches"
        }
    .end annotation

    .prologue
    const/16 v13, 0x168

    const/16 v12, 0x8

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 97
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 98
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 99
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 100
    .local v3, "res":Landroid/content/res/Resources;
    const-string v0, "1.2.2884"

    .line 101
    .local v0, "currentVersion":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    if-eqz v5, :cond_0

    .line 102
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v5, p0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 103
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 104
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 105
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v5}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->getUpdateVersion()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->updateVersion:Ljava/lang/String;

    .line 106
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v5}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->getCurrentVersion()Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;

    invoke-virtual {v5}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;->isReminder()Z

    move-result v5

    iput-boolean v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isReminder:Z

    .line 109
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mVersionText:Landroid/widget/TextView;

    const v6, 0x7f0902e0

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->updateVersion:Ljava/lang/String;

    aput-object v8, v7, v9

    aput-object v0, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mVersionText:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    const v5, 0x7f0e0088

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 113
    .local v4, "title1":Landroid/view/View;
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v6, 0x7f0902e1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 116
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mainSection:Landroid/widget/RelativeLayout;

    const/16 v6, 0xa

    invoke-static {v5, v9, v9, v9, v6}, Lcom/navdy/hud/app/util/ViewUtil;->adjustPadding(Landroid/view/View;IIII)V

    .line 117
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v6, 0x7f0a0014

    invoke-static {v5, v11, v13, v6}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;III)V

    .line 118
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mInfoText:Landroid/widget/TextView;

    const v6, 0x7f0901f2

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 120
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mIcon:Landroid/widget/ImageView;

    const v6, 0x7f0201ec

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 122
    const v5, 0x7f0e00bf

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 123
    .local v2, "maxWidthLayout":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    invoke-virtual {v2, v13}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-boolean v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->isReminder:Z

    if-eqz v5, :cond_1

    .line 126
    new-instance v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v6, 0x7f09018b

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v6, 0x7f09018a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :goto_0
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v6, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v5, v6, v1, v10, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 133
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mRightSwipe:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mLefttSwipe:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    iget-object v5, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    const-wide/16 v8, 0x7530

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 137
    return-void

    .line 129
    :cond_1
    new-instance v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v6, 0x7f090188

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    new-instance v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v6, 0x7f09004f

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_0

    .line 182
    sget-object v2, Lcom/navdy/hud/app/view/UpdateConfirmationView$2;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 194
    :goto_0
    return v0

    .line 184
    :pswitch_0
    invoke-virtual {p0, v1, v1}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 188
    :pswitch_1
    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/view/UpdateConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 199
    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 200
    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 201
    sget-object v1, Lcom/navdy/hud/app/view/UpdateConfirmationView$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 214
    :goto_0
    return v0

    .line 203
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 206
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 209
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/UpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
