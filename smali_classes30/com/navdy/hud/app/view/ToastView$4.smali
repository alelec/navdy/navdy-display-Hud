.class Lcom/navdy/hud/app/view/ToastView$4;
.super Ljava/lang/Object;
.source "ToastView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ToastView;->animateOut(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ToastView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/navdy/hud/app/view/ToastView$4;->this$0:Lcom/navdy/hud/app/view/ToastView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 213
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->getScreenName()Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "screenName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 216
    :try_start_0
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "out dismiss screen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 217
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView$4;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/view/ToastView;->access$100(Lcom/navdy/hud/app/view/ToastView;)Lcom/squareup/otto/Bus;

    move-result-object v2

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    invoke-static {v0}, Lcom/navdy/service/library/events/ui/Screen;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 218
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->clearScreenName()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v1

    .line 220
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
