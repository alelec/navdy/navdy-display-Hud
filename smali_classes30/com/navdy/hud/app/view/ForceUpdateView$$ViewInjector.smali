.class public Lcom/navdy/hud/app/view/ForceUpdateView$$ViewInjector;
.super Ljava/lang/Object;
.source "ForceUpdateView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/ForceUpdateView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/ForceUpdateView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00cc

    const-string v2, "field \'mScreenTitleText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mScreenTitleText:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0088

    const-string v2, "field \'mTextView1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView1:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e0089

    const-string v2, "field \'mTextView2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView2:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e00d2

    const-string v2, "field \'mChoiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 18
    const v1, 0x7f0e008a

    const-string v2, "field \'mTextView3\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e00cf

    const-string v2, "field \'mIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ForceUpdateView;->mIcon:Landroid/widget/ImageView;

    .line 22
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/ForceUpdateView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/ForceUpdateView;

    .prologue
    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mScreenTitleText:Landroid/widget/TextView;

    .line 26
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView1:Landroid/widget/TextView;

    .line 27
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView2:Landroid/widget/TextView;

    .line 28
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mIcon:Landroid/widget/ImageView;

    .line 31
    return-void
.end method
