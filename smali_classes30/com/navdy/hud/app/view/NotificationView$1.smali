.class Lcom/navdy/hud/app/view/NotificationView$1;
.super Ljava/lang/Object;
.source "NotificationView.java"

# interfaces
.implements Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/NotificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/NotificationView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/NotificationView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    # getter for: Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;
    invoke-static {v1}, Lcom/navdy/hud/app/view/NotificationView;->access$100(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    # getter for: Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-static {v1}, Lcom/navdy/hud/app/view/NotificationView;->access$200(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    .line 75
    # getter for: Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-static {v1}, Lcom/navdy/hud/app/view/NotificationView;->access$200(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    # getter for: Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-static {v1}, Lcom/navdy/hud/app/view/NotificationView;->access$200(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 82
    :cond_1
    :goto_0
    return v0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    # getter for: Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;
    invoke-static {v1}, Lcom/navdy/hud/app/view/NotificationView;->access$100(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMultipleClick(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView$1;->this$0:Lcom/navdy/hud/app/view/NotificationView;

    const/4 v1, 0x0

    # invokes: Lcom/navdy/hud/app/view/NotificationView;->takeNotificationAction(Z)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/NotificationView;->access$000(Lcom/navdy/hud/app/view/NotificationView;Z)V

    .line 63
    return-void
.end method
