.class public final enum Lcom/navdy/hud/app/view/MainView$AnimationMode;
.super Ljava/lang/Enum;
.source "MainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/MainView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnimationMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/view/MainView$AnimationMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/view/MainView$AnimationMode;

.field public static final enum MOVE_LEFT_SHRINK:Lcom/navdy/hud/app/view/MainView$AnimationMode;

.field public static final enum RIGHT_EXPAND:Lcom/navdy/hud/app/view/MainView$AnimationMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;

    const-string v1, "MOVE_LEFT_SHRINK"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/MainView$AnimationMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;->MOVE_LEFT_SHRINK:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;

    const-string v1, "RIGHT_EXPAND"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/view/MainView$AnimationMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;->RIGHT_EXPAND:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/view/MainView$AnimationMode;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$AnimationMode;->MOVE_LEFT_SHRINK:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/view/MainView$AnimationMode;->RIGHT_EXPAND:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;->$VALUES:[Lcom/navdy/hud/app/view/MainView$AnimationMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/view/MainView$AnimationMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/view/MainView$AnimationMode;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/view/MainView$AnimationMode;->$VALUES:[Lcom/navdy/hud/app/view/MainView$AnimationMode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/view/MainView$AnimationMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/view/MainView$AnimationMode;

    return-object v0
.end method
