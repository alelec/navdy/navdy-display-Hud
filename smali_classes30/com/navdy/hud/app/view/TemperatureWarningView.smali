.class public Lcom/navdy/hud/app/view/TemperatureWarningView;
.super Landroid/widget/RelativeLayout;
.source "TemperatureWarningView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final WARNING_MESSAGE_MAX_WIDTH:I = 0x140

.field private static lastDismissedTime:J

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mMainTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mRightSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d4
    .end annotation
.end field

.field mScreenTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mTitle1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0088
    .end annotation
.end field

.field mWarningMessage:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/TemperatureWarningView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/TemperatureWarningView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 54
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/navdy/hud/app/view/TemperatureWarningView;->lastDismissedTime:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TemperatureWarningView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 60
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TemperatureWarningView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 66
    :cond_0
    return-void
.end method

.method public static getLastDismissedTime()J
    .locals 2

    .prologue
    .line 144
    sget-wide v0, Lcom/navdy/hud/app/view/TemperatureWarningView;->lastDismissedTime:J

    return-wide v0
.end method


# virtual methods
.method public executeItem(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mPresenter:Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;->finish()V

    .line 107
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 110
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 95
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 100
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/view/TemperatureWarningView;->lastDismissedTime:J

    .line 102
    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 70
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 71
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 74
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mScreenTitleText:Landroid/widget/TextView;

    const v3, 0x7f09028b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 75
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mMainTitleText:Landroid/widget/TextView;

    const v3, 0x7f0901d2

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 76
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mIcon:Landroid/widget/ImageView;

    const v3, 0x7f020227

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    const v2, 0x7f0e00bf

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/view/TemperatureWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 78
    .local v1, "maxWidthLayout":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    const/16 v2, 0x140

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 80
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mTitle1:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mWarningMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mWarningMessage:Landroid/widget/TextView;

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 83
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mWarningMessage:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TemperatureWarningView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0900dc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v2, v3, v0, v5, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 87
    iget-object v2, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mRightSwipe:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    const-wide/16 v2, 0x0

    sput-wide v2, Lcom/navdy/hud/app/view/TemperatureWarningView;->lastDismissedTime:J

    .line 89
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 114
    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mRightSwipe:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mRightSwipe:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 116
    .local v0, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mPresenter:Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;->finish()V

    .line 118
    const/4 v1, 0x1

    .line 121
    .end local v0    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 126
    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 127
    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-ne p1, v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
