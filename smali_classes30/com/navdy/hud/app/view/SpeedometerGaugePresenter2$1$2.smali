.class Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1$2;
.super Ljava/lang/Object;
.source "SpeedometerGaugePresenter2.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1$2;->this$1:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 98
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1$2;->this$1:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;->this$0:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    # setter for: Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitAlpha:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->access$102(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;I)I

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1$2;->this$1:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;->this$0:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    # setter for: Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showingSpeedLimitAnimation:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->access$202(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;Z)Z

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1$2;->this$1:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;->this$0:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->reDraw()V

    .line 93
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 103
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 86
    return-void
.end method
