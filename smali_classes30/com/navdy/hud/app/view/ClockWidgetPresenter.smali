.class public Lcom/navdy/hud/app/view/ClockWidgetPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "ClockWidgetPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;
    }
.end annotation


# static fields
.field private static final TIME_UPDATE_INTERVAL:I = 0x7530


# instance fields
.field private amPmMarker:Ljava/lang/StringBuilder;

.field private analogClockDrawable:Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;

.field private analogClockWidgetName:Ljava/lang/String;

.field private bus:Lcom/squareup/otto/Bus;

.field private dateText:Ljava/lang/String;

.field private dayOfTheMonth:I

.field private dayOfTheWeek:I

.field private dayText:Ljava/lang/String;

.field private digitalClock2WidgetName:Ljava/lang/String;

.field private digitalClockWidgetName:Ljava/lang/String;

.field private mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private smallAnalogClockDrawable:Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private timeHour:I

.field private timeMinute:I

.field private timeSeconds:I

.field private timeText:Ljava/lang/String;

.field private updateTimeRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clockType"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->amPmMarker:Ljava/lang/StringBuilder;

    .line 71
    iput-object p2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mContext:Landroid/content/Context;

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 74
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->bus:Lcom/squareup/otto/Bus;

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09030e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->analogClockWidgetName:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->digitalClockWidgetName:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090310

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->digitalClock2WidgetName:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;-><init>(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;

    .line 86
    sget-object v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 96
    :goto_0
    return-void

    .line 88
    :pswitch_0
    new-instance v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->analogClockDrawable:Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;

    goto :goto_0

    .line 91
    :pswitch_1
    new-instance v0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->smallAnalogClockDrawable:Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTime()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->nextUpdateInterval()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private nextUpdateInterval()I
    .locals 3

    .prologue
    .line 60
    const/16 v0, 0x1e

    .line 61
    .local v0, "intervalSecs":I
    iget v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeSeconds:I

    add-int/2addr v2, v0

    rem-int/lit8 v1, v2, 0x3c

    .line 62
    .local v1, "nextSecs":I
    const/16 v2, 0x2d

    if-lt v1, v2, :cond_1

    .line 63
    rsub-int/lit8 v2, v1, 0x3c

    add-int/2addr v0, v2

    .line 67
    :cond_0
    :goto_0
    mul-int/lit16 v2, v0, 0x3e8

    return v2

    .line 64
    :cond_1
    const/16 v2, 0xf

    if-gt v1, v2, :cond_0

    .line 65
    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private updateTime()V
    .locals 4

    .prologue
    .line 104
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 105
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 106
    .local v1, "currentTime":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 107
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeMinute:I

    .line 108
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHour:I

    .line 109
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeSeconds:I

    .line 110
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayOfTheMonth:I

    .line 111
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayOfTheWeek:I

    .line 112
    sget-object v2, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v3, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->reDraw()V

    .line 123
    return-void

    .line 114
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v2}, Lcom/navdy/hud/app/common/TimeHelper;->getDate()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dateText:Ljava/lang/String;

    .line 115
    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v2}, Lcom/navdy/hud/app/common/TimeHelper;->getDay()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayText:Ljava/lang/String;

    .line 116
    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dateText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dateText:Ljava/lang/String;

    .line 117
    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayText:Ljava/lang/String;

    .line 119
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    iget-object v3, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->amPmMarker:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1, v3}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeText:Ljava/lang/String;

    goto :goto_0

    .line 112
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 164
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 158
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->analogClockDrawable:Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;

    goto :goto_0

    .line 160
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->smallAnalogClockDrawable:Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;

    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 208
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 202
    :pswitch_0
    const-string v0, "ANALOG_CLOCK_WIDGET"

    goto :goto_0

    .line 204
    :pswitch_1
    const-string v0, "DIGITAL_CLOCK_WIDGET"

    goto :goto_0

    .line 206
    :pswitch_2
    const-string v0, "DIGITAL_CLOCK_2_WIDGET"

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 221
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 215
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->analogClockWidgetName:Ljava/lang/String;

    goto :goto_0

    .line 217
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->digitalClockWidgetName:Ljava/lang/String;

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->digitalClock2WidgetName:Ljava/lang/String;

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method public onUpdateClock(Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTime()V

    .line 101
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 127
    if-eqz p1, :cond_0

    .line 128
    sget-object v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 139
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 141
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTime()V

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->nextUpdateInterval()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    :goto_1
    return-void

    .line 130
    :pswitch_0
    const v0, 0x7f03006a

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    goto :goto_0

    .line 133
    :pswitch_1
    const v0, 0x7f030012

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    goto :goto_0

    .line 136
    :pswitch_2
    const v0, 0x7f030013

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 145
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    goto :goto_1

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected updateGauge()V
    .locals 10

    .prologue
    const v9, 0x7f0e00be

    const v8, 0x7f0e00bd

    .line 169
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v4, :cond_0

    .line 170
    sget-object v4, Lcom/navdy/hud/app/view/ClockWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType:[I

    iget-object v5, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mClockType:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-virtual {v5}, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 172
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->analogClockDrawable:Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;

    iget v5, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayOfTheMonth:I

    iget v6, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHour:I

    iget v7, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeMinute:I

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->setTime(IIII)V

    goto :goto_0

    .line 175
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v4, v9}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 176
    .local v3, "timeTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v4, v8}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 177
    .local v2, "pmTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->amPmMarker:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    const v5, 0x7f0e00dd

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 181
    .local v0, "dateTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dateText:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    const v5, 0x7f0e00de

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 183
    .local v1, "dayTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->dayText:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 187
    .end local v0    # "dateTextView":Landroid/widget/TextView;
    .end local v1    # "dayTextView":Landroid/widget/TextView;
    .end local v2    # "pmTextView":Landroid/widget/TextView;
    .end local v3    # "timeTextView":Landroid/widget/TextView;
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->smallAnalogClockDrawable:Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;

    iget v5, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeHour:I

    iget v6, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeMinute:I

    iget v7, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeSeconds:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->setTime(III)V

    .line 188
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v4, v9}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 189
    .restart local v3    # "timeTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v4, v8}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 190
    .restart local v2    # "pmTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->amPmMarker:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v4, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->timeText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
