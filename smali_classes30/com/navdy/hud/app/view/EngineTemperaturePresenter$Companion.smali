.class public final Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;
.super Ljava/lang/Object;
.source "EngineTemperaturePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/EngineTemperaturePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u000b\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;",
        "",
        "()V",
        "TEMPERATURE_GAUGE_COLD_THRESHOLD",
        "",
        "getTEMPERATURE_GAUGE_COLD_THRESHOLD",
        "()D",
        "TEMPERATURE_GAUGE_HOT_THRESHOLD",
        "getTEMPERATURE_GAUGE_HOT_THRESHOLD",
        "TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS",
        "getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS",
        "TEMPERATURE_GAUGE_MID_POINT_CELSIUS",
        "getTEMPERATURE_GAUGE_MID_POINT_CELSIUS",
        "TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS",
        "getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getTEMPERATURE_GAUGE_COLD_THRESHOLD()D
    .locals 2

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_COLD_THRESHOLD:D
    invoke-static {}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getTEMPERATURE_GAUGE_HOT_THRESHOLD()D
    .locals 2

    .prologue
    .line 32
    # getter for: Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_HOT_THRESHOLD:D
    invoke-static {}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS()D
    .locals 2

    .prologue
    .line 28
    # getter for: Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS:D
    invoke-static {}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getTEMPERATURE_GAUGE_MID_POINT_CELSIUS()D
    .locals 2

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_MID_POINT_CELSIUS:D
    invoke-static {}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()D
    .locals 2

    .prologue
    .line 29
    # getter for: Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS:D
    invoke-static {}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp()D

    move-result-wide v0

    return-wide v0
.end method
