.class public Lcom/navdy/hud/app/view/FactoryResetView$$ViewInjector;
.super Ljava/lang/Object;
.source "FactoryResetView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/FactoryResetView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/FactoryResetView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e017e

    const-string v2, "field \'factoryResetConfirmation\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FactoryResetView;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .line 12
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/FactoryResetView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/FactoryResetView;

    .prologue
    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .line 16
    return-void
.end method
