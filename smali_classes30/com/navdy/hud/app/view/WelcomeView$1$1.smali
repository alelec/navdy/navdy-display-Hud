.class Lcom/navdy/hud/app/view/WelcomeView$1$1;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/WelcomeView$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

.field final synthetic val$appView:Landroid/widget/ImageView;

.field final synthetic val$playView:Landroid/widget/ImageView;

.field final synthetic val$txtView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView$1;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/view/WelcomeView$1;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iput-object p2, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$txtView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$appView:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$playView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # operator++ for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v0}, Lcom/navdy/hud/app/view/WelcomeView;->access$008(Lcom/navdy/hud/app/view/WelcomeView;)I

    .line 182
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v0}, Lcom/navdy/hud/app/view/WelcomeView;->access$000(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v0

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$300()[I

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/WelcomeView;->access$002(Lcom/navdy/hud/app/view/WelcomeView;I)I

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/view/WelcomeView;->access$500(Lcom/navdy/hud/app/view/WelcomeView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/view/WelcomeView;->access$400(Lcom/navdy/hud/app/view/WelcomeView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/view/WelcomeView;->access$500(Lcom/navdy/hud/app/view/WelcomeView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/view/WelcomeView;->access$400(Lcom/navdy/hud/app/view/WelcomeView;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_STAY_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$600()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$txtView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v2, v2, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/WelcomeView;->access$000(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v2

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->setDownloadText(Landroid/widget/TextView;I)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/view/WelcomeView;->access$700(Lcom/navdy/hud/app/view/WelcomeView;Landroid/widget/TextView;I)V

    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$appView:Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$300()[I

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v3, v3, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/WelcomeView;->access$000(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v3

    aget v2, v2, v3

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/view/WelcomeView;->access$800(Lcom/navdy/hud/app/view/WelcomeView;Landroid/widget/ImageView;I)V

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->val$playView:Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$900()[I

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/WelcomeView$1$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$1;

    iget-object v3, v3, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/WelcomeView;->access$000(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v3

    aget v2, v2, v3

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/view/WelcomeView;->access$800(Lcom/navdy/hud/app/view/WelcomeView;Landroid/widget/ImageView;I)V

    .line 190
    return-void
.end method
