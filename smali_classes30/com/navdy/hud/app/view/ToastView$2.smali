.class Lcom/navdy/hud/app/view/ToastView$2;
.super Ljava/lang/Object;
.source "ToastView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ToastView;->animateIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ToastView;

.field final synthetic val$screenName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ToastView;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/hud/app/view/ToastView$2;->this$0:Lcom/navdy/hud/app/view/ToastView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/ToastView$2;->val$screenName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 163
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toast:animationIn :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView$2;->val$screenName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 164
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "[focus] toastView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$2;->this$0:Lcom/navdy/hud/app/view/ToastView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/ToastView;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$2;->this$0:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$2;->this$0:Lcom/navdy/hud/app/view/ToastView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/ToastView;->setVisibility(I)V

    .line 167
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->setToastDisplayFlag(Z)V

    .line 168
    return-void
.end method
