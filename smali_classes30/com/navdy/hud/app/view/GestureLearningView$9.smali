.class Lcom/navdy/hud/app/view/GestureLearningView$9;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;

.field final synthetic val$directionText:Landroid/widget/TextView;

.field final synthetic val$overLay:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$9;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/GestureLearningView$9;->val$directionText:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/navdy/hud/app/view/GestureLearningView$9;->val$overLay:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 343
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$9;->val$directionText:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 344
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$9;->val$overLay:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 345
    return-void
.end method
