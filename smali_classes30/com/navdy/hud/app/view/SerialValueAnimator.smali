.class public Lcom/navdy/hud/app/view/SerialValueAnimator;
.super Ljava/lang/Object;
.source "SerialValueAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;
    }
.end annotation


# instance fields
.field private animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

.field private mAnimationRunning:Z

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mDuration:I

.field private mValueQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;I)V
    .locals 2
    .param p1, "adapter"    # Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;
    .param p2, "animationDuration"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/SerialValueAnimator$1;-><init>(Lcom/navdy/hud/app/view/SerialValueAnimator;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    .line 42
    iput-object p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    .line 43
    iput p2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mDuration:I

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SerialValueAnimator cannot run without the adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/SerialValueAnimator;)Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/SerialValueAnimator;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/view/SerialValueAnimator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimationRunning:Z

    return p1
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/SerialValueAnimator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/view/SerialValueAnimator;F)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;
    .param p1, "x1"    # F

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/SerialValueAnimator;->animate(F)V

    return-void
.end method

.method private animate(F)V
    .locals 4
    .param p1, "value"    # F

    .prologue
    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v2}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->getValue()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/navdy/hud/app/view/SerialValueAnimator$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/SerialValueAnimator$2;-><init>(Lcom/navdy/hud/app/view/SerialValueAnimator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 76
    return-void
.end method


# virtual methods
.method public release()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 85
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimationRunning:Z

    .line 87
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mDuration:I

    .line 91
    return-void
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "val"    # F

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->getValue()F

    move-result v0

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 51
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimationRunning:Z

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 61
    :goto_0
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimationRunning:Z

    .line 55
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/SerialValueAnimator;->animate(F)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->setValue(F)V

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->animationComplete(F)V

    goto :goto_0
.end method
