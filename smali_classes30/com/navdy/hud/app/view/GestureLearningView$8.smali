.class Lcom/navdy/hud/app/view/GestureLearningView$8;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;

.field final synthetic val$rightSide:Z

.field final synthetic val$view:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/view/ViewGroup;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->val$view:Landroid/view/ViewGroup;

    iput-boolean p3, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->val$rightSide:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 326
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->val$view:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 327
    .local v1, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 328
    .local v0, "animatedValue":I
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->val$rightSide:Z

    if-eqz v2, :cond_0

    .line 329
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 333
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$8;->val$view:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    return-void

    .line 331
    :cond_0
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_0
.end method
