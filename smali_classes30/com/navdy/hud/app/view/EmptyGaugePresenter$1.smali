.class Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;
.super Ljava/lang/Object;
.source "EmptyGaugePresenter.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/EmptyGaugePresenter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/EmptyGaugePresenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/EmptyGaugePresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 43
    .local v0, "alphaValue":F
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    iget-object v1, v1, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mEmptyGaugeText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    iget-object v1, v1, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mEmptyGaugeText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 46
    :cond_0
    return-void
.end method
