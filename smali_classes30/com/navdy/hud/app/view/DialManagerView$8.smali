.class Lcom/navdy/hud/app/view/DialManagerView$8;
.super Ljava/lang/Object;
.source "DialManagerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/DialManagerView;->showPairingSuccessUI(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/DialManagerView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/DialManagerView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 662
    iput-object p1, p0, Lcom/navdy/hud/app/view/DialManagerView$8;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView$8;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView$8;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView$8;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dismiss()V

    .line 670
    :goto_0
    return-void

    .line 668
    :cond_0
    # getter for: Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/DialManagerView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "showPairingSuccessUI:hide not active"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
