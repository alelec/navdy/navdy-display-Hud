.class Lcom/navdy/hud/app/view/WelcomeView$3$1;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/WelcomeView$3;->updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/view/WelcomeView$3;

.field final synthetic val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field final synthetic val$imagePath:Ljava/io/File;

.field final synthetic val$initials:Ljava/lang/String;

.field final synthetic val$initialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field final synthetic val$view:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView$3;Landroid/widget/ImageView;Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/view/WelcomeView$3;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$3;

    iput-object p2, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$view:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    iput-object p4, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$imagePath:Ljava/io/File;

    iput-object p5, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$initialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object p6, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$initials:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 319
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$view:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-eq v1, v2, :cond_0

    .line 340
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$imagePath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 323
    .local v0, "imageExists":Z
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$3;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$3;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/view/WelcomeView;->access$500(Lcom/navdy/hud/app/view/WelcomeView;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;-><init>(Lcom/navdy/hud/app/view/WelcomeView$3$1;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
