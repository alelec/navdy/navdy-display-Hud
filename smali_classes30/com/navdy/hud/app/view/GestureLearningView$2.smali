.class Lcom/navdy/hud/app/view/GestureLearningView$2;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$2;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 154
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$2;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 155
    .local v0, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 156
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$2;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    return-void
.end method
