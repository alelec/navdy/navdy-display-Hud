.class Lcom/navdy/hud/app/view/MainView$3$1;
.super Ljava/lang/Object;
.source "MainView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MainView$3;->onGlobalLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/view/MainView$3;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MainView$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/view/MainView$3;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView$3$1;->this$1:Lcom/navdy/hud/app/view/MainView$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$3$1;->this$1:Lcom/navdy/hud/app/view/MainView$3;

    iget-object v0, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView$3$1;->this$1:Lcom/navdy/hud/app/view/MainView$3;

    iget-object v1, v1, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v1}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/NotificationView;->setX(F)V

    .line 287
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifView x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView$3$1;->this$1:Lcom/navdy/hud/app/view/MainView$3;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 288
    return-void
.end method
