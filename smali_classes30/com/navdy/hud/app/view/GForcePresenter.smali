.class public Lcom/navdy/hud/app/view/GForcePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "GForcePresenter.java"


# instance fields
.field drawable:Lcom/navdy/hud/app/view/GForceDrawable;

.field private gMeterGaugeName:Ljava/lang/String;

.field private registered:Z

.field private xAccel:F

.field private yAccel:F

.field private zAccel:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 25
    new-instance v0, Lcom/navdy/hud/app/view/GForceDrawable;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/view/GForceDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->drawable:Lcom/navdy/hud/app/view/GForceDrawable;

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090316

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->gMeterGaugeName:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->drawable:Lcom/navdy/hud/app/view/GForceDrawable;

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "GFORCE_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->gMeterGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public onCalibratedGForceData(Lcom/navdy/hud/app/device/gps/CalibratedGForceData;)V
    .locals 1
    .param p1, "calibratedGForceData"    # Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getXAccel()F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->xAccel:F

    .line 46
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getYAccel()F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->yAccel:F

    .line 47
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getZAccel()F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->zAccel:F

    .line 48
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GForcePresenter;->reDraw()V

    .line 49
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 1
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 31
    if-eqz p1, :cond_0

    .line 32
    const v0, 0x7f03006a

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 35
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 36
    return-void
.end method

.method protected updateGauge()V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/GForcePresenter;->drawable:Lcom/navdy/hud/app/view/GForceDrawable;

    iget v1, p0, Lcom/navdy/hud/app/view/GForcePresenter;->xAccel:F

    iget v2, p0, Lcom/navdy/hud/app/view/GForcePresenter;->yAccel:F

    iget v3, p0, Lcom/navdy/hud/app/view/GForcePresenter;->zAccel:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/view/GForceDrawable;->setAcceleration(FFF)V

    .line 60
    return-void
.end method
