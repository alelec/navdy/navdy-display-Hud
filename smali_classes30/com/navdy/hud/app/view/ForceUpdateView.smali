.class public Lcom/navdy/hud/app/view/ForceUpdateView;
.super Landroid/widget/RelativeLayout;
.source "ForceUpdateView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final TAG_DISMISS:I = 0x2

.field private static final TAG_INSTALL:I = 0x1

.field private static final TAG_SHUT_DOWN:I = 0x0

.field private static final UPDATE_MESSAGE_MAX_WIDTH:I = 0x15e

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mScreenTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mTextView1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0088
    .end annotation
.end field

.field mTextView2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field mTextView3:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field private waitingForUpdate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/ForceUpdateView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/ForceUpdateView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    .line 106
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 108
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    .line 112
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 114
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    .line 118
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 120
    :cond_0
    return-void
.end method

.method private performAction(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 190
    packed-switch p1, :pswitch_data_0

    .line 201
    sget-object v0, Lcom/navdy/hud/app/view/ForceUpdateView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown action received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 204
    :goto_0
    return-void

    .line 192
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->shutDown()V

    goto :goto_0

    .line 195
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->install()V

    goto :goto_0

    .line 198
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->dismiss()V

    goto :goto_0

    .line 190
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public executeItem(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 65
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/view/ForceUpdateView;->performAction(I)V

    .line 67
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 70
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 185
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 187
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 146
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 147
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->isSoftwareUpdatePending()Z

    move-result v2

    .line 148
    .local v2, "updateAvailable":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 151
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 152
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mPresenter:Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 153
    const v3, 0x7f0e00bf

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/view/ForceUpdateView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 154
    .local v1, "maxWidthLayout":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    const/16 v3, 0x15e

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 155
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f0201ec

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView1:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView2:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    sget-object v3, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sget-object v4, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_REMOTE_NEEDS_UPDATE:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    if-ne v3, v4, :cond_0

    .line 161
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mScreenTitleText:Landroid/widget/TextView;

    const v4, 0x7f090290

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 162
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    const v4, 0x7f0901ff

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 164
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0900dc

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v3, v4, v0, v6, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 181
    return-void

    .line 167
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mScreenTitleText:Landroid/widget/TextView;

    const v4, 0x7f090291

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 169
    if-nez v2, :cond_1

    .line 170
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    const v4, 0x7f09017f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 171
    iput-boolean v7, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    .line 177
    :goto_1
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090265

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 173
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    const v4, 0x7f090180

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 174
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090188

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 78
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 79
    sget-object v1, Lcom/navdy/hud/app/view/ForceUpdateView$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 95
    :goto_0
    return v0

    .line 81
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 85
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 89
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdateReady(Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 126
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    if-eqz v1, :cond_0

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090188

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ForceUpdateView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090265

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v1, v2, v0, v4, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->mTextView3:Landroid/widget/TextView;

    const v2, 0x7f090180

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 134
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/ForceUpdateView;->waitingForUpdate:Z

    .line 136
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_0
    return-void
.end method
