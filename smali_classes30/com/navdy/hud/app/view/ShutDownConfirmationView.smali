.class public Lcom/navdy/hud/app/view/ShutDownConfirmationView;
.super Landroid/widget/RelativeLayout;
.source "ShutDownConfirmationView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field public static final DIAL_OTA:I = 0x2

.field public static final INACTIVITY:I = 0x0

.field private static final INACTIVITY_SHUTDOWN_TIMEOUT:I = 0xea60

.field private static final INSTALL_UPDATE_AND_SHUTDOWN_TIMEOUT:I = 0x7530

.field public static final OTA:I = 0x1

.field private static final SHUTDOWN_MESSAGE_MAX_WIDTH:I = 0x168

.field private static final SHUTDOWN_RESET_TIMEOUT:I = 0x7530

.field private static final SHUTDOWN_TIMEOUT:I = 0x1388

.field private static final TAG_DO_NOT_SHUT_DOWN:I = 0x2

.field private static final TAG_INSTALL_AND_SHUT_DOWN:I = 0x1

.field private static final TAG_INSTALL_DIAL_UPDATE:I = 0x3

.field private static final TAG_SHUT_DOWN:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private gesturesEnabled:Z

.field private handler:Landroid/os/Handler;

.field private initialState:I

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mInfoText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field mIsUpdateAvailable:Z

.field mLefttSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d3
    .end annotation
.end field

.field mMainTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mRightSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d4
    .end annotation
.end field

.field mScreenTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mSecondsAnimator:Landroid/animation/ValueAnimator;

.field mSummaryText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008b
    .end annotation
.end field

.field mTextView1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0088
    .end annotation
.end field

.field private shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field private swipeLeftAction:I

.field private swipeRightAction:I

.field private timeout:Ljava/lang/Runnable;

.field timerMessage:Ljava/lang/String;

.field private updateTargetVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 120
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    .line 97
    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->gesturesEnabled:Z

    .line 98
    iput v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    .line 99
    iput v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timerMessage:Ljava/lang/String;

    .line 102
    const-string v0, "1.3.2884"

    iput-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    iput-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 105
    iput v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    .line 121
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 124
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/ShutDownConfirmationView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ShutDownConfirmationView;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->performAction(I)V

    return-void
.end method

.method private cancelTimeout()V
    .locals 2

    .prologue
    .line 497
    sget-object v0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancelTimeout"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 504
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 505
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 507
    :cond_1
    return-void
.end method

.method private getTimeout(Lcom/navdy/hud/app/event/Shutdown$Reason;)I
    .locals 2
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    .line 488
    sget-object v0, Lcom/navdy/hud/app/view/ShutDownConfirmationView$7;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 492
    const/16 v0, 0x1388

    :goto_0
    return v0

    .line 490
    :pswitch_0
    const v0, 0xea60

    goto :goto_0

    .line 488
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 128
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->ShutdownScreen:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 134
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 138
    return-void

    .line 136
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private performAction(I)V
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 343
    packed-switch p1, :pswitch_data_0

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 345
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->recordUpdateSelection(Z)V

    .line 346
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDown()V

    goto :goto_0

    .line 349
    :pswitch_1
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->recordUpdateSelection(Z)V

    .line 350
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->finish()V

    goto :goto_0

    .line 353
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->isSoftwareUpdatePending()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->recordUpdateSelection(Z)V

    .line 355
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->installAndShutDown()V

    goto :goto_0

    .line 359
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->isDialFirmwareUpdatePending()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->recordUpdateSelection(Z)V

    .line 361
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    const v2, 0x7f0900d7

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 362
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 366
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090265

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v1, v2, v0, v4, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 369
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->installDialUpdateAndShutDown()V

    goto :goto_0

    .line 343
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private recordUpdateSelection(Z)V
    .locals 2
    .param p1, "accepted"    # Z

    .prologue
    .line 376
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->isSoftwareUpdatePending()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 378
    .local v0, "isDial":Z
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordUpdatePrompt(ZZLjava/lang/String;)V

    .line 380
    .end local v0    # "isDial":Z
    :cond_0
    return-void

    .line 377
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startCountDown()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const-wide/16 v4, 0x7530

    const/4 v3, 0x2

    .line 403
    sget-object v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startCountDown"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 408
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_1

    .line 409
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 410
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 411
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 414
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    if-ne v1, v7, :cond_3

    .line 415
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    if-nez v1, :cond_2

    .line 416
    new-instance v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$1;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    .line 423
    :cond_2
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    .line 424
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 425
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 426
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/hud/app/view/ShutDownConfirmationView$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$2;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 434
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 435
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 485
    :goto_0
    return-void

    .line 436
    :cond_3
    iget v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    if-ne v1, v3, :cond_5

    .line 437
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    if-nez v1, :cond_4

    .line 438
    new-instance v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$3;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    .line 445
    :cond_4
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    .line 446
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 447
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 448
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 456
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 457
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 459
    :cond_5
    const/16 v0, 0x7530

    .line 460
    .local v0, "timeVal":I
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_6

    .line 461
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getTimeout(Lcom/navdy/hud/app/event/Shutdown$Reason;)I

    move-result v0

    .line 463
    :cond_6
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    if-nez v1, :cond_7

    .line 464
    new-instance v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$5;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    .line 471
    :cond_7
    new-array v1, v3, [I

    div-int/lit16 v2, v0, 0x3e8

    aput v2, v1, v6

    aput v6, v1, v7

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    .line 472
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 473
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 474
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/hud/app/view/ShutDownConfirmationView$6;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView$6;-><init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 482
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timeout:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 483
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSecondsAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0

    .line 423
    :array_0
    .array-data 4
        0x1e
        0x0
    .end array-data

    .line 445
    :array_1
    .array-data 4
        0x1e
        0x0
    .end array-data
.end method


# virtual methods
.method public executeItem(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 336
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->performAction(I)V

    .line 337
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 340
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 262
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->cancelTimeout()V

    .line 263
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 265
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 266
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 268
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 13

    .prologue
    .line 148
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 149
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 150
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mTextView1:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    const v8, 0x7f0e00bf

    invoke-virtual {p0, v8}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 152
    .local v0, "linearLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    move-object v8, v0

    .line 153
    check-cast v8, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    const/16 v9, 0x168

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 154
    const/4 v8, -0x2

    iput v8, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 155
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    const-string v5, "1.2.2872"

    .line 159
    .local v5, "updateFromVersion":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    if-eqz v8, :cond_0

    .line 160
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8, p0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 161
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 162
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 164
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->isSoftwareUpdatePending()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 165
    const/4 v8, 0x1

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    .line 166
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->getUpdateVersion()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    .line 167
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->getCurrentVersion()Ljava/lang/String;

    move-result-object v5

    .line 178
    :cond_0
    :goto_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->gesturesEnabled:Z

    .line 179
    const/4 v8, -0x1

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    .line 180
    const/4 v8, -0x1

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    .line 182
    iget v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 183
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    .line 184
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v9, 0x7f0902dd

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 186
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0902df

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/16 v12, 0x1e

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timerMessage:Ljava/lang/String;

    .line 187
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timerMessage:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0902e0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 190
    .local v6, "updateSummary":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIcon:Landroid/widget/ImageView;

    const v9, 0x7f0201ec

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 194
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f090188

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f090265

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0900e2

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v1, v10, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 200
    const/4 v8, 0x1

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    .line 201
    const/4 v8, 0x2

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    .line 202
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->gesturesEnabled:Z

    .line 203
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mRightSwipe:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mLefttSwipe:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 205
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V

    .line 256
    .end local v6    # "updateSummary":Ljava/lang/String;
    :goto_1
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const/4 v9, 0x2

    const/16 v10, 0x168

    const v11, 0x7f0a0014

    invoke-static {v8, v9, v10, v11}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;III)V

    .line 257
    return-void

    .line 168
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_1
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->isDialFirmwareUpdatePending()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 169
    const/4 v8, 0x2

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    .line 170
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v7

    .line 171
    .local v7, "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "1.0."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v7, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v9, v9, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    .line 172
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "1.0."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v7, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v9, v9, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 173
    goto/16 :goto_0

    .line 174
    .end local v7    # "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    :cond_2
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v8}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->getShutDownCause()Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 175
    const/4 v8, 0x0

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    goto/16 :goto_0

    .line 206
    :cond_3
    iget v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->initialState:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    .line 207
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    .line 208
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    const-string v9, ""

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v9, 0x7f0900d2

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 210
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0900d4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/16 v12, 0x1e

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timerMessage:Ljava/lang/String;

    .line 212
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->timerMessage:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0900d9

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->updateTargetVersion:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 215
    .restart local v6    # "updateSummary":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIcon:Landroid/widget/ImageView;

    const v9, 0x7f020103

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .restart local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f090188

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f090265

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0900e2

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v1, v10, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 225
    const/4 v8, 0x3

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    .line 226
    const/4 v8, 0x2

    iput v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    .line 227
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->gesturesEnabled:Z

    .line 228
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mRightSwipe:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mLefttSwipe:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 230
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V

    goto/16 :goto_1

    .line 232
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    .end local v6    # "updateSummary":Ljava/lang/String;
    :cond_4
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    .line 233
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v9, 0x7f090266

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 235
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {p0, v8}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->setShutdownCause(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    .line 237
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020f

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 238
    .local v3, "shutsDownInMessage":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mSummaryText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIcon:Landroid/widget/ImageView;

    const v9, 0x7f0201c8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 242
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .restart local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f090265

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0900e2

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-direct {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v1, v10, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 248
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {p0, v8}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getTimeout(Lcom/navdy/hud/app/event/Shutdown$Reason;)I

    move-result v4

    .line 249
    .local v4, "timeVal":I
    sget-object v8, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "shutdown cause is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " time is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mRightSwipe:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    iget-object v8, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mLefttSwipe:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V

    goto/16 :goto_1
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 5
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 272
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->gesturesEnabled:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_0

    .line 273
    sget-object v2, Lcom/navdy/hud/app/view/ShutDownConfirmationView$7;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 289
    :goto_0
    return v0

    .line 275
    :pswitch_0
    iget v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    if-eq v2, v4, :cond_0

    .line 276
    iget v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeLeftAction:I

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 281
    :pswitch_1
    iget v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    if-eq v2, v4, :cond_0

    .line 282
    iget v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->swipeRightAction:I

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 294
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 295
    sget-object v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 325
    :goto_0
    return v0

    .line 297
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V

    .line 298
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 302
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V

    .line 303
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 307
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->cancelTimeout()V

    .line 308
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 312
    :pswitch_3
    sget-object v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "power btn dlb-click"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->cancelTimeout()V

    .line 314
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->finish()V

    goto :goto_0

    .line 318
    :pswitch_4
    sget-object v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "power btn click"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 319
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->cancelTimeout()V

    .line 320
    iget-object v1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->finish()V

    goto :goto_0

    .line 325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setShutdownCause(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
    .locals 4
    .param p1, "cause"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    const/4 v3, 0x0

    .line 383
    sget-object v1, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "available, shut down cause:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 385
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mIsUpdateAvailable:Z

    if-nez v0, :cond_1

    .line 386
    sget-object v0, Lcom/navdy/hud/app/view/ShutDownConfirmationView$7;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 394
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 400
    :goto_1
    return-void

    .line 383
    :cond_0
    const-string v0, "not "

    goto :goto_0

    .line 390
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
