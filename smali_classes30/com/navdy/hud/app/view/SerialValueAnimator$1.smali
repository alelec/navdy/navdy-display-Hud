.class Lcom/navdy/hud/app/view/SerialValueAnimator$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "SerialValueAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/SerialValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/SerialValueAnimator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 26
    instance-of v2, p1, Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 27
    check-cast v1, Landroid/animation/ValueAnimator;

    .line 28
    .local v1, "valueAnimator":Landroid/animation/ValueAnimator;
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    # getter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;
    invoke-static {v2}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$000(Lcom/navdy/hud/app/view/SerialValueAnimator;)Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    move-result-object v3

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-interface {v3, v2}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->animationComplete(F)V

    .line 30
    .end local v1    # "valueAnimator":Landroid/animation/ValueAnimator;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    # getter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;
    invoke-static {v2}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$100(Lcom/navdy/hud/app/view/SerialValueAnimator;)Ljava/util/Queue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 31
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimationRunning:Z
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$202(Lcom/navdy/hud/app/view/SerialValueAnimator;Z)Z

    .line 32
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$302(Lcom/navdy/hud/app/view/SerialValueAnimator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    # getter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mValueQueue:Ljava/util/Queue;
    invoke-static {v2}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$100(Lcom/navdy/hud/app/view/SerialValueAnimator;)Ljava/util/Queue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 35
    .local v0, "val":F
    iget-object v2, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$1;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    # invokes: Lcom/navdy/hud/app/view/SerialValueAnimator;->animate(F)V
    invoke-static {v2, v0}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$400(Lcom/navdy/hud/app/view/SerialValueAnimator;F)V

    goto :goto_0
.end method
