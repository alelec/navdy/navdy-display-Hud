.class synthetic Lcom/navdy/hud/app/view/DialManagerView$9;
.super Ljava/lang/Object;
.source "DialManagerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/DialManagerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 549
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->values()[Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->NO_DIAL_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    .line 455
    :goto_4
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->values()[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    .line 268
    :goto_8
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_e
    return-void

    :catch_0
    move-exception v0

    goto :goto_e

    :catch_1
    move-exception v0

    goto :goto_d

    :catch_2
    move-exception v0

    goto :goto_c

    :catch_3
    move-exception v0

    goto :goto_b

    :catch_4
    move-exception v0

    goto :goto_a

    :catch_5
    move-exception v0

    goto :goto_9

    .line 455
    :catch_6
    move-exception v0

    goto :goto_8

    :catch_7
    move-exception v0

    goto :goto_7

    :catch_8
    move-exception v0

    goto :goto_6

    :catch_9
    move-exception v0

    goto :goto_5

    .line 549
    :catch_a
    move-exception v0

    goto/16 :goto_4

    :catch_b
    move-exception v0

    goto/16 :goto_3

    :catch_c
    move-exception v0

    goto/16 :goto_2

    :catch_d
    move-exception v0

    goto/16 :goto_1

    :catch_e
    move-exception v0

    goto/16 :goto_0
.end method
