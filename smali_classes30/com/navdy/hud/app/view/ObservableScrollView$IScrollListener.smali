.class public interface abstract Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;
.super Ljava/lang/Object;
.source "ObservableScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/ObservableScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IScrollListener"
.end annotation


# virtual methods
.method public abstract onBottom()V
.end method

.method public abstract onScroll(IIII)V
.end method

.method public abstract onTop()V
.end method
