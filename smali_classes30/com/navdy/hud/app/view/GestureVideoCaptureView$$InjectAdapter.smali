.class public final Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "GestureVideoCaptureView$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/view/GestureVideoCaptureView;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/view/GestureVideoCaptureView;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private gestureService:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/gesture/GestureServiceConnector;",
            ">;"
        }
    .end annotation
.end field

.field private mPresenter:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 29
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.view.GestureVideoCaptureView"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 30
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 39
    const-string v0, "com.navdy.hud.app.screen.GestureLearningScreen$Presenter"

    const-class v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->mPresenter:Ldagger/internal/Binding;

    .line 40
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    const-class v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    .line 41
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 42
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->mPresenter:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/view/GestureVideoCaptureView;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->mPresenter:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->bus:Lcom/squareup/otto/Bus;

    .line 64
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/view/GestureVideoCaptureView;)V

    return-void
.end method
