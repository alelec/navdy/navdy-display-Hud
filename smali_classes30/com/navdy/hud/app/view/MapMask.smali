.class public Lcom/navdy/hud/app/view/MapMask;
.super Landroid/view/View;
.source "MapMask.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bottom:I

.field private fadePaint:Landroid/graphics/Paint;

.field private fadePath:Landroid/graphics/Path;

.field private left:I

.field private mHorizontalRadius:I

.field private mHorizontalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

.field private mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

.field private mMaskColor:I

.field private mVerticalRadius:I

.field private mVerticalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

.field private maskPaint:Landroid/graphics/Paint;

.field private maskPath:Landroid/graphics/Path;

.field private right:I

.field private top:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/navdy/hud/app/view/MapMask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/view/MapMask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/MapMask;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/MapMask;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/MapMask;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    new-instance v0, Lcom/navdy/hud/app/view/IndicatorView;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/app/view/IndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    .line 68
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MapMask;->initDrawingTools()V

    .line 69
    return-void
.end method

.method private evaluateDimensions(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/4 v2, 0x0

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    int-to-float v1, p1

    invoke-virtual {v0, p0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    int-to-float v1, p2

    invoke-virtual {v0, p0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadius:I

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/navdy/hud/app/view/IndicatorView;->evaluateDimensions(II)V

    .line 92
    return-void
.end method

.method private initDrawingTools()V
    .locals 14

    .prologue
    .line 109
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    .line 110
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPaint:Landroid/graphics/Paint;

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->mMaskColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 115
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MapMask;->getWidth()I

    move-result v12

    .line 116
    .local v12, "width":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MapMask;->getHeight()I

    move-result v8

    .line 118
    .local v8, "height":I
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    const/4 v1, 0x0

    int-to-float v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    int-to-float v1, v12

    int-to-float v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    int-to-float v1, v12

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 124
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadius:I

    sub-int v0, v8, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->top:I

    .line 125
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->top:I

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadius:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    .line 126
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadius:I

    add-int v10, v0, v1

    .line 127
    .local v10, "ovalBottom":I
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v12, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    .line 128
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/MapMask;->right:I

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 131
    new-instance v9, Landroid/graphics/RectF;

    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->top:I

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->right:I

    int-to-float v2, v2

    int-to-float v6, v10

    invoke-direct {v9, v0, v1, v2, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 132
    .local v9, "oval":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    const/high16 v1, 0x43340000    # 180.0f

    const/high16 v2, 0x43340000    # 180.0f

    const/4 v6, 0x0

    invoke-virtual {v0, v9, v1, v2, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    int-to-float v2, v2

    iget-object v6, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/view/IndicatorView;->getCurveRadius()F

    move-result v3

    iget-object v6, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    .line 134
    invoke-virtual {v6}, Lcom/navdy/hud/app/view/IndicatorView;->getIndicatorWidth()I

    move-result v6

    int-to-float v4, v6

    iget-object v6, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    .line 135
    invoke-virtual {v6}, Lcom/navdy/hud/app/view/IndicatorView;->getIndicatorHeight()I

    move-result v6

    int-to-float v5, v6

    iget v6, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    mul-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    .line 133
    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/view/IndicatorView;->drawIndicatorPath(Landroid/graphics/Path;FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/IndicatorView;->getStrokeWidth()F

    move-result v11

    .line 141
    .local v11, "strokeWidth":F
    const/high16 v7, 0x42700000    # 60.0f

    .line 142
    .local v7, "fadeWidth":F
    iget v0, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    int-to-float v3, v0

    .line 143
    .local v3, "radius":F
    cmpg-float v0, v3, v7

    if-gez v0, :cond_0

    move v3, v7

    .line 145
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 147
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 148
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 149
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 150
    const/4 v0, 0x3

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    .line 151
    .local v4, "colors":[I
    const/4 v0, 0x3

    new-array v5, v0, [F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v5, v0

    const/4 v0, 0x1

    sub-float v1, v3, v7

    div-float/2addr v1, v3

    aput v1, v5, v0

    const/4 v0, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v5, v0

    .line 153
    .local v5, "stops":[F
    iget-object v13, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RadialGradient;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v1, v1

    add-float/2addr v1, v3

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    int-to-float v2, v2

    add-float/2addr v2, v11

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 155
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePath:Landroid/graphics/Path;

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePath:Landroid/graphics/Path;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v7, v0

    sub-float/2addr v0, v11

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float/2addr v1, v11

    invoke-virtual {v9, v0, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 158
    iget v0, v9, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v11

    iput v0, v9, Landroid/graphics/RectF;->top:F

    .line 159
    iget v0, v9, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v11

    iput v0, v9, Landroid/graphics/RectF;->bottom:F

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->fadePath:Landroid/graphics/Path;

    const/high16 v1, 0x43340000    # 180.0f

    const/high16 v2, 0x43340000    # 180.0f

    const/4 v6, 0x0

    invoke-virtual {v0, v9, v1, v2, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 161
    return-void

    .line 150
    :array_0
    .array-data 4
        0x0
        0x0
        -0x1000000
    .end array-data
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->MapMask:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 78
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/hud/app/util/CustomDimension;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    .line 79
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/hud/app/util/CustomDimension;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/MapMask;->mVerticalRadiusAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    .line 80
    const/4 v1, 0x2

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/MapMask;->mMaskColor:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 84
    return-void

    .line 82
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method


# virtual methods
.method public getIndicatorPoint()Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 164
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->mHorizontalRadius:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    iget-object v3, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/IndicatorView;->getIndicatorHeight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public layout(IIII)V
    .locals 5
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/navdy/hud/app/view/MapMask;->right:I

    iget-object v4, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/IndicatorView;->getIndicatorHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/view/IndicatorView;->layout(IIII)V

    .line 106
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 169
    iget-object v1, p0, Lcom/navdy/hud/app/view/MapMask;->maskPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/navdy/hud/app/view/MapMask;->maskPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 171
    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->bottom:I

    iget-object v2, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/IndicatorView;->getIndicatorHeight()I

    move-result v2

    sub-int v0, v1, v2

    .line 172
    .local v0, "yOffset":I
    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 173
    iget-object v1, p0, Lcom/navdy/hud/app/view/MapMask;->mIndicator:Lcom/navdy/hud/app/view/IndicatorView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/view/IndicatorView;->draw(Landroid/graphics/Canvas;)V

    .line 174
    iget v1, p0, Lcom/navdy/hud/app/view/MapMask;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 176
    iget-object v1, p0, Lcom/navdy/hud/app/view/MapMask;->fadePath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/navdy/hud/app/view/MapMask;->fadePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 177
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/MapMask;->evaluateDimensions(II)V

    .line 97
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MapMask;->initDrawingTools()V

    .line 98
    return-void
.end method
