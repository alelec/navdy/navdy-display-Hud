.class Lcom/navdy/hud/app/view/GestureLearningView$10;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;

.field final synthetic val$directionText:Landroid/widget/TextView;

.field final synthetic val$overLay:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$overLay:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$directionText:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 364
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$overLay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 358
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$directionText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 359
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 369
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 351
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$overLay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView$10;->val$directionText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 353
    return-void
.end method
