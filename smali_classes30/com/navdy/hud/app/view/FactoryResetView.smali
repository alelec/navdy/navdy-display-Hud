.class public Lcom/navdy/hud/app/view/FactoryResetView;
.super Landroid/widget/RelativeLayout;
.source "FactoryResetView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# instance fields
.field factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e017e
    .end annotation
.end field

.field presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/FactoryResetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/FactoryResetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/FactoryResetView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public getConfirmation()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    return-object v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 80
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 59
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 66
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 51
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/view/FactoryResetView;->presenter:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method
