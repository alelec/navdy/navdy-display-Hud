.class public Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
.super Landroid/widget/FrameLayout;
.source "LearnGestureScreenLayout.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;
    }
.end annotation


# static fields
.field public static final TAG_BACK:I = 0x0

.field public static final TAG_TIPS:I = 0x1


# instance fields
.field mCameraBlockedMessage:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01cc
    .end annotation
.end field

.field mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

.field private mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

.field mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ca
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01cb
    .end annotation
.end field

.field mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01cd
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 74
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 218
    packed-switch p2, :pswitch_data_0

    .line 226
    :goto_0
    return-void

    .line 220
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->hideCameraSensorBlocked()V

    goto :goto_0

    .line 223
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->showTips()V

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hideCaptureGestureVideosView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 210
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 214
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 215
    return-void
.end method

.method public hideSensorBlocked()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 197
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 198
    return-void
.end method

.method public hideTips()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 181
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 182
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 231
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$1;->$SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 164
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 158
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    goto :goto_0

    .line 160
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    goto :goto_0

    .line 162
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 128
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 14

    .prologue
    const/16 v11, 0x8

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 80
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 81
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 82
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e00d1

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 83
    .local v8, "sideImageView":Landroid/widget/ImageView;
    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    const v9, 0x7f0200d0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e00cf

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 87
    .local v5, "mainImageView":Landroid/widget/ImageView;
    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    const v9, 0x7f0201db

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 90
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e0088

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 91
    .local v6, "mainText":Landroid/widget/TextView;
    const v9, 0x7f090259

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(I)V

    .line 93
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e008a

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 95
    .local v7, "secondaryText":Landroid/widget/TextView;
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e00bf

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 96
    .local v3, "infoContainer":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0099

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 98
    const v9, 0x7f09025a

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(I)V

    .line 99
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e0089

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e008b

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 102
    const/high16 v9, 0x41880000    # 17.0f

    invoke-virtual {v7, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 104
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f09002d

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    new-instance v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f09028f

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v13}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const v10, 0x7f0e00d2

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iput-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 108
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v10, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v9, v10, v4, v12, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 109
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v9, v13}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setHighlightPersistent(Z)V

    .line 110
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "array":[Ljava/lang/String;
    array-length v9, v0

    new-array v1, v9, [Ljava/lang/CharSequence;

    .line 112
    .local v1, "charSequences":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, v0

    if-ge v2, v9, :cond_0

    .line 113
    aget-object v9, v0, v2

    aput-object v9, v1, v2

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    :cond_0
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v9, v1}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setTextContents([Ljava/lang/CharSequence;)V

    .line 116
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    if-eqz v9, :cond_1

    .line 117
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v9, p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 119
    :cond_1
    iget-object v9, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v9, v12}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 137
    iget-object v1, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    sget-object v2, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->SENSOR_BLOCKED:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    if-ne v1, v2, :cond_0

    .line 138
    sget-object v1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 151
    :goto_0
    return v0

    .line 140
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 143
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 146
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraSensorBlockedChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public showCaptureGestureVideosView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 201
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 205
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->CAPTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 206
    return-void
.end method

.method public showSensorBlocked()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 189
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->SENSOR_BLOCKED:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 190
    return-void
.end method

.method public showTips()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 169
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureLearningView;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setVisibility(I)V

    .line 173
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->TIPS:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCurrentMode:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 174
    return-void
.end method
