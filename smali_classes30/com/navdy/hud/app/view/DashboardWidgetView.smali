.class public Lcom/navdy/hud/app/view/DashboardWidgetView;
.super Landroid/widget/RelativeLayout;
.source "DashboardWidgetView.java"


# instance fields
.field protected mCustomView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00db
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field private mLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    sget-object v2, Lcom/navdy/hud/app/R$styleable;->GaugeView:[I

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    .local v0, "customAttributes":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 39
    const/4 v2, 0x1

    const v3, 0x7f030069

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 40
    .local v1, "layout":I
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 41
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    .end local v1    # "layout":I
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mCustomView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mCustomView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 75
    :cond_0
    return-void
.end method

.method public getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mCustomView:Landroid/view/View;

    return-object v0
.end method

.method public setContentView(I)Z
    .locals 1
    .param p1, "layoutResId"    # I

    .prologue
    .line 57
    if-lez p1, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mLayout:I

    if-eq p1, v0, :cond_0

    .line 58
    iput p1, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mLayout:I

    .line 59
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->removeAllViews()V

    .line 60
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 61
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 62
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentView(Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetView;->mLayout:I

    .line 48
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->removeAllViews()V

    .line 49
    if-eqz p1, :cond_0

    .line 50
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->addView(Landroid/view/View;)V

    .line 51
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
