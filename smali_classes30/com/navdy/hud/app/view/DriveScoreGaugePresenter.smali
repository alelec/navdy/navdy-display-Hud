.class public final Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "DriveScoreGaugePresenter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u001d\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\n\u0010P\u001a\u0004\u0018\u00010QH\u0016J\u0008\u0010R\u001a\u00020\u001cH\u0016J\u0008\u0010S\u001a\u00020\u001cH\u0016J\u0008\u0010T\u001a\u00020\u0007H\u0014J\u0010\u0010U\u001a\u00020V2\u0006\u0010\'\u001a\u00020&H\u0007J\u0008\u0010W\u001a\u00020VH\u0016J\u001c\u0010X\u001a\u00020V2\u0008\u0010Y\u001a\u0004\u0018\u00010Z2\u0008\u0010[\u001a\u0004\u0018\u00010\\H\u0016J\u0010\u0010]\u001a\u00020V2\u0006\u0010^\u001a\u00020\u0007H\u0016J\u0006\u0010_\u001a\u00020VJ\u0006\u0010`\u001a\u00020VJ\u0008\u0010a\u001a\u00020VH\u0014R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$R$\u0010\'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+R\u0011\u0010,\u001a\u00020-\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u00101R\u001e\u00102\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0007@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u00083\u00104R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00106R\u001a\u00107\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00088\u0010\"\"\u0004\u00089\u0010$R\u0011\u0010:\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008;\u00101R\u0011\u0010<\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008=\u00101R\u0011\u0010>\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008?\u0010\u001eR\u0011\u0010@\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008A\u0010\u001eR\u0011\u0010B\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008C\u0010\u001eR\u0011\u0010D\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008E\u0010\u001eR\u0011\u0010F\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008G\u0010\u001eR\u0011\u0010H\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008I\u0010\u001eR\u001a\u0010J\u001a\u00020KX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008L\u0010M\"\u0004\u0008N\u0010O\u00a8\u0006b"
    }
    d2 = {
        "Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;",
        "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;",
        "context",
        "Landroid/content/Context;",
        "layoutId",
        "",
        "showScore",
        "",
        "(Landroid/content/Context;IZ)V",
        "CLEAR_WARNING_TIME_MS",
        "",
        "getCLEAR_WARNING_TIME_MS",
        "()J",
        "clearWarning",
        "Ljava/lang/Runnable;",
        "getClearWarning",
        "()Ljava/lang/Runnable;",
        "setClearWarning",
        "(Ljava/lang/Runnable;)V",
        "getContext",
        "()Landroid/content/Context;",
        "driveScoreGaugeDrawable",
        "Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;",
        "getDriveScoreGaugeDrawable",
        "()Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;",
        "setDriveScoreGaugeDrawable",
        "(Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;)V",
        "driveScoreGaugeName",
        "",
        "getDriveScoreGaugeName",
        "()Ljava/lang/String;",
        "driveScoreText",
        "Landroid/widget/TextView;",
        "getDriveScoreText",
        "()Landroid/widget/TextView;",
        "setDriveScoreText",
        "(Landroid/widget/TextView;)V",
        "value",
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "driveScoreUpdated",
        "getDriveScoreUpdated",
        "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "setDriveScoreUpdated",
        "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V",
        "handler",
        "Landroid/os/Handler;",
        "getHandler",
        "()Landroid/os/Handler;",
        "getLayoutId",
        "()I",
        "newEvent",
        "setNewEvent",
        "(Z)V",
        "getShowScore",
        "()Z",
        "summaryText",
        "getSummaryText",
        "setSummaryText",
        "summaryTextColorNormal",
        "getSummaryTextColorNormal",
        "summaryTextColorWarning",
        "getSummaryTextColorWarning",
        "summaryTextDriveScore",
        "getSummaryTextDriveScore",
        "summaryTextExcessiveSpeeding",
        "getSummaryTextExcessiveSpeeding",
        "summaryTextHardAcceleration",
        "getSummaryTextHardAcceleration",
        "summaryTextHardBraking",
        "getSummaryTextHardBraking",
        "summaryTextHighG",
        "getSummaryTextHighG",
        "summaryTextSpeeding",
        "getSummaryTextSpeeding",
        "warningImage",
        "Landroid/widget/ImageView;",
        "getWarningImage",
        "()Landroid/widget/ImageView;",
        "setWarningImage",
        "(Landroid/widget/ImageView;)V",
        "getDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "getWidgetIdentifier",
        "getWidgetName",
        "isRegisteringToBusRequired",
        "onDriveScoreUpdated",
        "",
        "onResume",
        "setView",
        "dashboardWidgetView",
        "Lcom/navdy/hud/app/view/DashboardWidgetView;",
        "arguments",
        "Landroid/os/Bundle;",
        "setWidgetVisibleToUser",
        "b",
        "showHighG",
        "showSpeeding",
        "updateGauge",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final CLEAR_WARNING_TIME_MS:J

.field private clearWarning:Ljava/lang/Runnable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final context:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final driveScoreGaugeName:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public driveScoreText:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final layoutId:I

.field private newEvent:Z

.field private final showScore:Z

.field public summaryText:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextColorNormal:I

.field private final summaryTextColorWarning:I

.field private final summaryTextDriveScore:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextExcessiveSpeeding:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextHardAcceleration:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextHardBraking:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextHighG:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final summaryTextSpeeding:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public warningImage:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "layoutId"    # I
    .param p3, "showScore"    # Z

    .prologue
    const/4 v2, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->context:Landroid/content/Context;

    iput p2, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->layoutId:I

    iput-boolean p3, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    .line 24
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->CLEAR_WARNING_TIME_MS:J

    .line 42
    new-instance v0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->context:Landroid/content/Context;

    const v3, 0x7f0a000b

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setMinValue(F)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    if-eqz v0, :cond_1

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setMaxGaugeValue(F)V

    .line 45
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->handler:Landroid/os/Handler;

    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 47
    .local v6, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0900e7

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.drive_score)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextDriveScore:Ljava/lang/String;

    .line 48
    const v0, 0x7f090131

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.hard_accel)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardAcceleration:Ljava/lang/String;

    .line 49
    const v0, 0x7f090132

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.hard_braking)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardBraking:Ljava/lang/String;

    .line 50
    const v0, 0x7f090276

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.speeding)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextSpeeding:Ljava/lang/String;

    .line 51
    const v0, 0x7f090312

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.widget_drive_score)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeName:Ljava/lang/String;

    .line 52
    const v0, 0x7f0900fa

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.excessive_speeding)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextExcessiveSpeeding:Ljava/lang/String;

    .line 53
    const v0, 0x7f090133

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.high_g)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHighG:Ljava/lang/String;

    .line 54
    const v0, 0x106000b

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorNormal:I

    .line 55
    const v0, 0x7f0d0013

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorWarning:I

    .line 58
    new-instance v0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter$clearWarning$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter$clearWarning$1;-><init>(Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    .line 63
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    const/16 v5, 0x64

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;-><init>(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    return-void
.end method

.method public static final synthetic access$getNewEvent$p(Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;)Z
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->newEvent:Z

    return v0
.end method

.method public static final synthetic access$setNewEvent$p(Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;Z)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;
    .param p1, "<set-?>"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setNewEvent(Z)V

    return-void
.end method

.method private final setNewEvent(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->newEvent:Z

    .line 75
    return-void
.end method


# virtual methods
.method public final getCLEAR_WARNING_TIME_MS()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->CLEAR_WARNING_TIME_MS:J

    return-wide v0
.end method

.method public final getClearWarning()Ljava/lang/Runnable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 106
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDriveScoreGaugeDrawable()Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    return-object v0
.end method

.method public final getDriveScoreGaugeName()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method public final getDriveScoreText()Landroid/widget/TextView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "driveScoreText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getDriveScoreUpdated()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    return-object v0
.end method

.method public final getHandler()Landroid/os/Handler;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public final getLayoutId()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->layoutId:I

    return v0
.end method

.method public final getShowScore()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    return v0
.end method

.method public final getSummaryText()Landroid/widget/TextView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getSummaryTextColorNormal()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorNormal:I

    return v0
.end method

.method public final getSummaryTextColorWarning()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorWarning:I

    return v0
.end method

.method public final getSummaryTextDriveScore()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextDriveScore:Ljava/lang/String;

    return-object v0
.end method

.method public final getSummaryTextExcessiveSpeeding()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextExcessiveSpeeding:Ljava/lang/String;

    return-object v0
.end method

.method public final getSummaryTextHardAcceleration()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardAcceleration:Ljava/lang/String;

    return-object v0
.end method

.method public final getSummaryTextHardBraking()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardBraking:Ljava/lang/String;

    return-object v0
.end method

.method public final getSummaryTextHighG()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHighG:Ljava/lang/String;

    return-object v0
.end method

.method public final getSummaryTextSpeeding()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextSpeeding:Ljava/lang/String;

    return-object v0
.end method

.method public final getWarningImage()Landroid/widget/ImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 195
    const-string v0, "DRIVE_SCORE_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public final onDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V
    .locals 1
    .param p1, "driveScoreUpdated"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "driveScoreUpdated"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V

    .line 80
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setNewEvent(Z)V

    .line 187
    invoke-super {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->onResume()V

    .line 188
    return-void
.end method

.method public final setClearWarning(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "<set-?>"    # Ljava/lang/Runnable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    return-void
.end method

.method public final setDriveScoreGaugeDrawable(Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    return-void
.end method

.method public final setDriveScoreText(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    return-void
.end method

.method public final setDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V
    .locals 2
    .param p1, "value"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    .line 66
    invoke-virtual {p1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getInterestingEvent()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->NONE:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setNewEvent(Z)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->reDraw()V

    .line 70
    return-void
.end method

.method public final setSummaryText(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "arguments"    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 87
    iget v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->layoutId:I

    .line 88
    .local v0, "layoutResourceId":I
    if-eqz p1, :cond_4

    .line 90
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTelemetryDataManager()Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->getDriveScoreUpdatedEvent()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V

    .line 91
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setNewEvent(Z)V

    .line 92
    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 93
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v1, :cond_1

    .line 94
    const v1, 0x7f0e00be

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    .line 96
    :cond_1
    const v1, 0x7f0e00e0

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    .line 97
    const v1, 0x7f0e00df

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    .line 99
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public final setWarningImage(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/ImageView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    return-void
.end method

.method public setWidgetVisibleToUser(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setNewEvent(Z)V

    .line 192
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    .line 193
    return-void
.end method

.method public final showHighG()V
    .locals 2

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setState(I)V

    .line 161
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "driveScoreText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorWarning:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_4

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const v1, 0x7f0200c5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_5

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHighG:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    return-void
.end method

.method public final showSpeeding()V
    .locals 2

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setState(I)V

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "driveScoreText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorWarning:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isExcessiveSpeeding()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_4

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_5

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextExcessiveSpeeding:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_7

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const v1, 0x7f020247

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 181
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_8

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextSpeeding:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected updateGauge()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update Drive score "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v0, :cond_8

    .line 113
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getDriveScore()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setGaugeValue(F)V

    .line 116
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->newEvent:Z

    if-eqz v0, :cond_e

    .line 117
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setState(I)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "driveScoreText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_4

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorWarning:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getInterestingEvent()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_ACCELERATION:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_6

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardAcceleration:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :cond_7
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->CLEAR_WARNING_TIME_MS:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    :cond_8
    :goto_1
    return-void

    .line 126
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getInterestingEvent()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;->HARD_BRAKING:Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_a

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    const v1, 0x7f020088

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 128
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_b

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextHardBraking:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :cond_c
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showHighG()V

    goto :goto_0

    .line 131
    :cond_d
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 132
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showSpeeding()V

    goto :goto_0

    .line 137
    :cond_e
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isDoingHighGManeuver()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 138
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showHighG()V

    goto :goto_1

    .line 139
    :cond_f
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->isSpeeding()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 140
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showSpeeding()V

    goto :goto_1

    .line 143
    :cond_10
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->clearWarning:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 144
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->showScore:Z

    if-eqz v0, :cond_16

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreGaugeDrawable:Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->setState(I)V

    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v0, :cond_11

    const-string v1, "driveScoreText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreText:Landroid/widget/TextView;

    if-nez v1, :cond_12

    const-string v0, "driveScoreText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->driveScoreUpdated:Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getDriveScore()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_13

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    iget v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextColorNormal:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    iget-object v1, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v1, :cond_14

    const-string v0, "summaryText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryTextDriveScore:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->warningImage:Landroid/widget/ImageView;

    if-nez v0, :cond_15

    const-string v1, "warningImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 151
    :cond_16
    iget-object v0, p0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_17

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
