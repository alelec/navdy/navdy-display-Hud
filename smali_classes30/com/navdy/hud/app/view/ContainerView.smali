.class public Lcom/navdy/hud/app/view/ContainerView;
.super Landroid/widget/FrameLayout;
.source "ContainerView.java"

# interfaces
.implements Lcom/navdy/hud/app/util/CanShowScreen;
.implements Lcom/navdy/hud/app/util/Pauseable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/navdy/hud/app/util/CanShowScreen",
        "<",
        "Lmortar/Blueprint;",
        ">;",
        "Lcom/navdy/hud/app/util/Pauseable;"
    }
.end annotation


# instance fields
.field private screenMaestro:Lcom/navdy/hud/app/util/ScreenConductor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/navdy/hud/app/util/ScreenConductor",
            "<",
            "Lmortar/Blueprint;",
            ">;"
        }
    .end annotation
.end field

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ContainerView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public createScreens()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/view/ContainerView;->screenMaestro:Lcom/navdy/hud/app/util/ScreenConductor;

    invoke-virtual {v0}, Lcom/navdy/hud/app/util/ScreenConductor;->createScreens()V

    .line 88
    return-void
.end method

.method public getCurrentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/view/ContainerView;->screenMaestro:Lcom/navdy/hud/app/util/ScreenConductor;

    invoke-virtual {v0}, Lcom/navdy/hud/app/util/ScreenConductor;->getChildView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getCustomContainerAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)Landroid/animation/Animator;
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ContainerView;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 79
    instance-of v1, v0, Lcom/navdy/hud/app/view/ICustomAnimator;

    if-eqz v1, :cond_0

    .line 80
    check-cast v0, Lcom/navdy/hud/app/view/ICustomAnimator;

    .end local v0    # "view":Landroid/view/View;
    invoke-interface {v0, p1}, Lcom/navdy/hud/app/view/ICustomAnimator;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)Landroid/animation/Animator;

    move-result-object v1

    .line 83
    :goto_0
    return-object v1

    .restart local v0    # "view":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 48
    const v1, 0x7f0e0133

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 49
    .local v0, "screenContainer":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/hud/app/util/ScreenConductor;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ContainerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/ContainerView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-direct {v1, v2, v0, v3}, Lcom/navdy/hud/app/util/ScreenConductor;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/framework/UIStateManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ContainerView;->screenMaestro:Lcom/navdy/hud/app/util/ScreenConductor;

    .line 50
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ContainerView;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/navdy/hud/app/util/Pauseable;

    if-eqz v1, :cond_0

    .line 60
    check-cast v0, Lcom/navdy/hud/app/util/Pauseable;

    .end local v0    # "view":Landroid/view/View;
    invoke-interface {v0}, Lcom/navdy/hud/app/util/Pauseable;->onPause()V

    .line 62
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ContainerView;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/navdy/hud/app/util/Pauseable;

    if-eqz v1, :cond_0

    .line 68
    check-cast v0, Lcom/navdy/hud/app/util/Pauseable;

    .end local v0    # "view":Landroid/view/View;
    invoke-interface {v0}, Lcom/navdy/hud/app/util/Pauseable;->onResume()V

    .line 70
    :cond_0
    return-void
.end method

.method public showScreen(Lmortar/Blueprint;Lflow/Flow$Direction;II)V
    .locals 1
    .param p1, "screen"    # Lmortar/Blueprint;
    .param p2, "direction"    # Lflow/Flow$Direction;
    .param p3, "animIn"    # I
    .param p4, "animOut"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/view/ContainerView;->screenMaestro:Lcom/navdy/hud/app/util/ScreenConductor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/navdy/hud/app/util/ScreenConductor;->showScreen(Lmortar/Blueprint;Lflow/Flow$Direction;II)V

    .line 55
    return-void
.end method
