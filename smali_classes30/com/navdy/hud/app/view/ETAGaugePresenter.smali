.class public Lcom/navdy/hud/app/view/ETAGaugePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "ETAGaugePresenter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TRIP_INFO_UPDATE_INTERVAL:J


# instance fields
.field etaAmPmText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00ed
    .end annotation
.end field

.field private etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

.field etaText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00ec
    .end annotation
.end field

.field private etaTripGaugeName:Ljava/lang/String;

.field etaView:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e3
    .end annotation
.end field

.field private farMargin:I

.field private feetLabel:Ljava/lang/String;

.field private fontSize18:I

.field private fontSize30:I

.field private gravity:I

.field private greyColor:I

.field private isNavigationActive:Z

.field private kiloMetersLabel:Ljava/lang/String;

.field private labelShort:[Ljava/lang/String;

.field private labels:[Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mTripInfoUpdateRunnable:Ljava/lang/Runnable;

.field private mTripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

.field private maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field private metersLabel:Ljava/lang/String;

.field private milesLabel:Ljava/lang/String;

.field private nearMargin:I

.field remainingDistanceText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00ea
    .end annotation
.end field

.field remainingDistanceUnitText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00eb
    .end annotation
.end field

.field tripDistanceView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f0
    .end annotation
.end field

.field private tripGaugeIconMargin:I

.field private tripGaugeLongMargin:I

.field private tripGaugeShortMargin:I

.field tripIconView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f1
    .end annotation
.end field

.field tripOpenMapView:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00ee
    .end annotation
.end field

.field tripTimeView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00ef
    .end annotation
.end field

.field ttaText1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e5
    .end annotation
.end field

.field ttaText2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e6
    .end annotation
.end field

.field ttaText3:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e7
    .end annotation
.end field

.field ttaText4:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    const-class v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->$assertionsDisabled:Z

    .line 103
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->TRIP_INFO_UPDATE_INTERVAL:J

    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTripManager()Lcom/navdy/hud/app/framework/trips/TripManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 117
    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labels:[Ljava/lang/String;

    .line 118
    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labelShort:[Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->metersLabel:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->kiloMetersLabel:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->feetLabel:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0902d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->milesLabel:Ljava/lang/String;

    .line 123
    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->farMargin:I

    .line 124
    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->nearMargin:I

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09031b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaTripGaugeName:Ljava/lang/String;

    .line 127
    const v1, 0x7f0b01b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripGaugeShortMargin:I

    .line 128
    const v1, 0x7f0b01b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripGaugeLongMargin:I

    .line 129
    const v1, 0x7f0b01af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripGaugeIconMargin:I

    .line 131
    const v1, 0x7f0d002f

    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->greyColor:I

    .line 133
    const v1, 0x7f0b0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize18:I

    .line 134
    const v1, 0x7f0b0126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize30:I

    .line 136
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;

    .line 137
    new-instance v1, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;-><init>(Lcom/navdy/hud/app/view/ETAGaugePresenter;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;

    .line 144
    new-instance v1, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    .line 145
    iget-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setMinValue(F)V

    .line 146
    iget-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setMaxGaugeValue(F)V

    .line 147
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/ETAGaugePresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ETAGaugePresenter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100()J
    .locals 2

    .prologue
    .line 44
    sget-wide v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->TRIP_INFO_UPDATE_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/ETAGaugePresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ETAGaugePresenter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addPadding(Landroid/text/SpannableStringBuilder;)V
    .locals 1
    .param p1, "stringBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 460
    const-string v0, " "

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 461
    const-string v0, " "

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 462
    const-string v0, " "

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 463
    return-void
.end method

.method private getDistance(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p1, "val"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x22

    .line 435
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 436
    .local v2, "stringBuilder":Landroid/text/SpannableStringBuilder;
    iget v3, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    if-nez v3, :cond_0

    .line 437
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->addPadding(Landroid/text/SpannableStringBuilder;)V

    .line 439
    :cond_0
    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 440
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 441
    .local v0, "len":I
    new-instance v3, Landroid/text/style/AbsoluteSizeSpan;

    iget v4, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize18:I

    invoke-direct {v3, v4}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v2, v3, v6, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 442
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const/4 v4, -0x1

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v3, v6, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 444
    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 446
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 447
    .local v1, "start":I
    invoke-virtual {v2, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 448
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 449
    new-instance v3, Landroid/text/style/AbsoluteSizeSpan;

    iget v4, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize18:I

    invoke-direct {v3, v4}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 450
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v4, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->greyColor:I

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 452
    iget v3, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 453
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->addPadding(Landroid/text/SpannableStringBuilder;)V

    .line 456
    :cond_1
    return-object v2
.end method

.method private varargs getTime([Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 9
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    const/16 v8, 0x22

    .line 400
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 401
    .local v3, "stringBuilder":Landroid/text/SpannableStringBuilder;
    iget v6, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    if-nez v6, :cond_0

    .line 402
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->addPadding(Landroid/text/SpannableStringBuilder;)V

    .line 404
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, p1

    if-ge v0, v6, :cond_2

    .line 405
    if-eqz v0, :cond_1

    .line 406
    const-string v6, " "

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 409
    :cond_1
    aget-object v5, p1, v0

    .line 410
    .local v5, "val":Ljava/lang/String;
    add-int/lit8 v6, v0, 0x1

    aget-object v4, p1, v6

    .line 411
    .local v4, "unit":Ljava/lang/String;
    add-int/lit8 v0, v0, 0x2

    .line 413
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 414
    .local v2, "start":I
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 415
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 416
    .local v1, "len":I
    new-instance v6, Landroid/text/style/AbsoluteSizeSpan;

    iget v7, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize30:I

    invoke-direct {v6, v7}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v3, v6, v2, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 417
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v6, v2, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 419
    const-string v6, " "

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 421
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 422
    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 423
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 424
    new-instance v6, Landroid/text/style/AbsoluteSizeSpan;

    iget v7, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->fontSize18:I

    invoke-direct {v6, v7}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v3, v6, v2, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 425
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    iget v7, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->greyColor:I

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v6, v2, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 428
    .end local v1    # "len":I
    .end local v2    # "start":I
    .end local v4    # "unit":Ljava/lang/String;
    .end local v5    # "val":Ljava/lang/String;
    :cond_2
    iget v6, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 429
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->addPadding(Landroid/text/SpannableStringBuilder;)V

    .line 431
    :cond_3
    return-object v3
.end method

.method private parseSeconds(J[I[B)Z
    .locals 11
    .param p1, "timeInSeconds"    # J
    .param p3, "splitTime"    # [I
    .param p4, "startEnd"    # [B

    .prologue
    const-wide/16 v8, 0x3c

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v4

    long-to-int v3, v4

    aput v3, p3, v6

    .line 154
    const/4 v3, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    aget v6, p3, v6

    mul-int/lit8 v6, v6, 0x18

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, p3, v3

    .line 157
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    mul-long/2addr v6, v8

    sub-long/2addr v4, v6

    long-to-int v3, v4

    aput v3, p3, v1

    .line 160
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    mul-long/2addr v4, v8

    sub-long v4, p1, v4

    long-to-int v3, v4

    aput v3, p3, v2

    .line 162
    aput-byte v2, p4, v2

    .line 163
    aput-byte v2, p4, v1

    .line 164
    const/4 v0, 0x3

    .local v0, "i":B
    :goto_0
    if-lez v0, :cond_2

    .line 165
    aget v3, p3, v0

    if-lez v3, :cond_1

    .line 166
    aput-byte v0, p4, v1

    .line 167
    aget-byte v3, p4, v2

    if-nez v3, :cond_0

    .line 168
    aput-byte v0, p4, v2

    .line 164
    :cond_0
    add-int/lit8 v3, v0, -0x1

    int-to-byte v0, v3

    goto :goto_0

    .line 171
    :cond_1
    aget-byte v3, p4, v2

    if-eqz v3, :cond_0

    .line 176
    :cond_2
    sget-boolean v3, Lcom/navdy/hud/app/view/ETAGaugePresenter;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    aget-byte v3, p4, v2

    aget-byte v4, p4, v1

    if-eq v3, v4, :cond_3

    aget-byte v3, p4, v2

    aget-byte v4, p4, v1

    add-int/lit8 v4, v4, 0x1

    if-eq v3, v4, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 177
    :cond_3
    aget-byte v3, p4, v2

    aget-byte v4, p4, v1

    if-le v3, v4, :cond_4

    .line 178
    .local v1, "multiCharacter":Z
    :goto_1
    return v1

    .end local v1    # "multiCharacter":Z
    :cond_4
    move v1, v2

    .line 177
    goto :goto_1
.end method

.method private setTTAText(J)V
    .locals 13
    .param p1, "timeInMilliSeconds"    # J

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 188
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 189
    .local v6, "timeInSeconds":J
    const-wide/16 v8, 0x3c

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 190
    const/4 v5, 0x4

    new-array v2, v5, [I

    .line 191
    .local v2, "splitTime":[I
    const/4 v5, 0x2

    new-array v4, v5, [B

    .line 193
    .local v4, "startEnd":[B
    invoke-direct {p0, v6, v7, v2, v4}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->parseSeconds(J[I[B)Z

    move-result v1

    .line 194
    .local v1, "singleCharacter":Z
    aget-byte v3, v4, v10

    .line 195
    .local v3, "start":B
    const/4 v5, 0x1

    aget-byte v0, v4, v5

    .line 197
    .local v0, "end":B
    if-eqz v1, :cond_0

    .line 198
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText1:Landroid/widget/TextView;

    aget v8, v2, v3

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText2:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labels:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText3:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText4:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText3:Landroid/widget/TextView;

    aget v8, v2, v0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText4:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labels:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText1:Landroid/widget/TextView;

    aget v8, v2, v3

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText2:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labels:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText3:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v5, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText4:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTripTimeText(J)V
    .locals 13
    .param p1, "timeInMilliSeconds"    # J

    .prologue
    .line 222
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 223
    .local v6, "timeInSeconds":J
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-nez v8, :cond_0

    .line 224
    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripDistanceView:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labelShort:[Ljava/lang/String;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    aput-object v11, v9, v10

    invoke-direct {p0, v9}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->getTime([Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    :goto_0
    return-void

    .line 228
    :cond_0
    const-wide/16 v8, 0x3c

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 229
    const/4 v8, 0x4

    new-array v2, v8, [I

    .line 230
    .local v2, "splitTime":[I
    const/4 v8, 0x2

    new-array v4, v8, [B

    .line 232
    .local v4, "startEnd":[B
    invoke-direct {p0, v6, v7, v2, v4}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->parseSeconds(J[I[B)Z

    move-result v1

    .line 233
    .local v1, "singleCharacter":Z
    const/4 v8, 0x0

    aget-byte v3, v4, v8

    .line 234
    .local v3, "start":B
    const/4 v8, 0x1

    aget-byte v0, v4, v8

    .line 237
    .local v0, "end":B
    if-eqz v1, :cond_1

    .line 238
    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aget v10, v2, v3

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labelShort:[Ljava/lang/String;

    aget-object v10, v10, v3

    const/4 v11, 0x0

    .line 239
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aget v10, v2, v0

    .line 240
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget-object v10, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labelShort:[Ljava/lang/String;

    aget-object v10, v10, v0

    const/4 v11, 0x0

    .line 241
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 238
    invoke-direct {p0, v8}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->getTime([Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 245
    .local v5, "stringBuilder":Landroid/text/SpannableStringBuilder;
    :goto_1
    iget-object v8, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 243
    .end local v5    # "stringBuilder":Landroid/text/SpannableStringBuilder;
    :cond_1
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aget v10, v2, v3

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->labels:[Ljava/lang/String;

    aget-object v10, v10, v3

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {p0, v8}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->getTime([Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .restart local v5    # "stringBuilder":Landroid/text/SpannableStringBuilder;
    goto :goto_1
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    const-string v0, "ETA_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaTripGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public onManeuverDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "maneuverDisplay"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 386
    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 397
    :goto_0
    return-void

    .line 389
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    .line 390
    .local v0, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    iput-object p1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 391
    iget-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    if-eqz v1, :cond_1

    .line 392
    iget-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->isNavigating()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->isNavigationActive:Z

    .line 396
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->reDraw()V

    goto :goto_0

    .line 394
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isNavigationActive()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->isNavigationActive:Z

    goto :goto_1
.end method

.method public onNavigationModeChange(Lcom/navdy/hud/app/maps/NavigationMode;)V
    .locals 2
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 378
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    .line 379
    .local v0, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isNavigationActive()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->isNavigationActive:Z

    .line 380
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 381
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->reDraw()V

    .line 382
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 250
    if-eqz p1, :cond_1

    .line 251
    if-eqz p2, :cond_0

    const-string v2, "EXTRA_GRAVITY"

    const/4 v4, 0x0

    invoke-virtual {p2, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    :goto_0
    iput v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    .line 252
    const v2, 0x7f030019

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 253
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 254
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 255
    .local v10, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v0, Landroid/support/constraint/ConstraintSet;

    invoke-direct {v0}, Landroid/support/constraint/ConstraintSet;-><init>()V

    .line 256
    .local v0, "constraintSet":Landroid/support/constraint/ConstraintSet;
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v2}, Landroid/support/constraint/ConstraintSet;->clone(Landroid/support/constraint/ConstraintLayout;)V

    .line 257
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v2}, Landroid/support/constraint/ConstraintLayout;->getId()I

    move-result v3

    .line 258
    .local v3, "parentId":I
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripIconView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v1

    .line 259
    .local v1, "iconViewId":I
    iget v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->gravity:I

    packed-switch v2, :pswitch_data_0

    .line 274
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v2}, Landroid/support/constraint/ConstraintSet;->applyTo(Landroid/support/constraint/ConstraintLayout;)V

    .line 275
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v10}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v11

    .line 277
    .local v11, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isNavigationActive()Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->isNavigationActive:Z

    .line 278
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->postManeuverDisplay()V

    .line 279
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 280
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->reDraw()V

    .line 285
    .end local v0    # "constraintSet":Landroid/support/constraint/ConstraintSet;
    .end local v1    # "iconViewId":I
    .end local v3    # "parentId":I
    .end local v10    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v11    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :goto_2
    return-void

    .line 251
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 261
    .restart local v0    # "constraintSet":Landroid/support/constraint/ConstraintSet;
    .restart local v1    # "iconViewId":I
    .restart local v3    # "parentId":I
    .restart local v10    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_1
    iget v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->nearMargin:I

    iput v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 262
    iget v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->farMargin:I

    iput v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 263
    const/4 v2, 0x6

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 264
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getId()I

    move-result v5

    const/4 v6, 0x1

    const/4 v8, 0x2

    iget v9, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripGaugeIconMargin:I

    move-object v4, v0

    move v7, v1

    invoke-virtual/range {v4 .. v9}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    goto :goto_1

    .line 267
    :pswitch_2
    iget v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->farMargin:I

    iput v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 268
    iget v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->nearMargin:I

    iput v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 269
    const/4 v2, 0x7

    const/4 v4, 0x7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 270
    iget-object v2, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getId()I

    move-result v5

    const/4 v6, 0x2

    const/4 v8, 0x1

    iget v9, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripGaugeIconMargin:I

    move-object v4, v0

    move v7, v1

    invoke-virtual/range {v4 .. v9}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    goto :goto_1

    .line 282
    .end local v0    # "constraintSet":Landroid/support/constraint/ConstraintSet;
    .end local v1    # "iconViewId":I
    .end local v3    # "parentId":I
    .end local v10    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 283
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->reDraw()V

    goto :goto_2

    .line 259
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected updateGauge()V
    .locals 28

    .prologue
    .line 299
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->isNavigationActive:Z

    move/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v23, v0

    if-nez v23, :cond_2

    .line 304
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/framework/trips/TripManager;->getCurrentTripStartTime()J

    move-result-wide v8

    .line 306
    .local v8, "currentTripStartTime":J
    const-wide/16 v24, 0x0

    cmp-long v23, v8, v24

    if-lez v23, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/framework/trips/TripManager;->getCurrentTripStartTime()J

    move-result-wide v26

    sub-long v18, v24, v26

    .line 307
    .local v18, "tripTime":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/framework/trips/TripManager;->getCurrentDistanceTraveled()I

    move-result v23

    move/from16 v0, v23

    int-to-long v10, v0

    .line 308
    .local v10, "distanceTravelledInMeters":J
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v16

    .line 309
    .local v16, "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    new-instance v5, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct {v5}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    .line 310
    .local v5, "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v23

    long-to-float v0, v10

    move/from16 v24, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 311
    const-string v22, ""

    .line 312
    .local v22, "unitLabel":Ljava/lang/String;
    sget-object v23, Lcom/navdy/hud/app/view/ETAGaugePresenter$2;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    iget-object v0, v5, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v24

    aget v23, v23, v24

    packed-switch v23, :pswitch_data_0

    .line 326
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripDistanceView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    iget v0, v5, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->getDistance(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripDistanceView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 328
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->setTripTimeText(J)V

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    sget-wide v26, Lcom/navdy/hud/app/view/ETAGaugePresenter;->TRIP_INFO_UPDATE_INTERVAL:J

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 364
    .end local v5    # "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v8    # "currentTripStartTime":J
    .end local v10    # "distanceTravelledInMeters":J
    .end local v16    # "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    .end local v18    # "tripTime":J
    .end local v22    # "unitLabel":Ljava/lang/String;
    :goto_2
    return-void

    .line 306
    .restart local v8    # "currentTripStartTime":J
    :cond_1
    const-wide/16 v18, 0x0

    goto/16 :goto_0

    .line 314
    .restart local v5    # "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .restart local v10    # "distanceTravelledInMeters":J
    .restart local v16    # "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    .restart local v18    # "tripTime":J
    .restart local v22    # "unitLabel":Ljava/lang/String;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->metersLabel:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 315
    goto :goto_1

    .line 317
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->kiloMetersLabel:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 318
    goto :goto_1

    .line 320
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->milesLabel:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 321
    goto :goto_1

    .line 323
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->feetLabel:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_1

    .line 333
    .end local v5    # "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v8    # "currentTripStartTime":J
    .end local v10    # "distanceTravelledInMeters":J
    .end local v16    # "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    .end local v18    # "tripTime":J
    .end local v22    # "unitLabel":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 336
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 337
    .local v6, "currentTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    .line 338
    .local v12, "etaTime":J
    cmp-long v23, v12, v6

    if-lez v23, :cond_3

    sub-long v20, v12, v6

    .line 339
    .local v20, "tta":J
    :goto_3
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->setTTAText(J)V

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaAmPmText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistance:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToMeters(FLcom/navdy/service/library/events/navigation/DistanceUnit;)F

    move-result v17

    .line 344
    .local v17, "totalDistanceInMeters":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToMeters(FLcom/navdy/service/library/events/navigation/DistanceUnit;)F

    move-result v15

    .line 345
    .local v15, "remainingDistanceInMeters":F
    sub-float v4, v17, v15

    .line 346
    .local v4, "coveredDistanceInMeters":F
    const/16 v23, 0x0

    cmpl-float v23, v17, v23

    if-lez v23, :cond_4

    cmpl-float v23, v17, v4

    if-lez v23, :cond_4

    div-float v23, v4, v17

    const/high16 v24, 0x42c80000    # 100.0f

    mul-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v14, v0

    .line 347
    .local v14, "percentage":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    move-object/from16 v23, v0

    int-to-float v0, v14

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setGaugeValue(F)V

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    sget-object v23, Lcom/navdy/hud/app/view/ETAGaugePresenter$2;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->maneuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v24

    aget v23, v23, v24

    packed-switch v23, :pswitch_data_1

    goto/16 :goto_2

    .line 351
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->metersLabel:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 338
    .end local v4    # "coveredDistanceInMeters":F
    .end local v14    # "percentage":I
    .end local v15    # "remainingDistanceInMeters":F
    .end local v17    # "totalDistanceInMeters":F
    .end local v20    # "tta":J
    :cond_3
    const-wide/16 v20, 0x0

    goto/16 :goto_3

    .line 346
    .restart local v4    # "coveredDistanceInMeters":F
    .restart local v15    # "remainingDistanceInMeters":F
    .restart local v17    # "totalDistanceInMeters":F
    .restart local v20    # "tta":J
    :cond_4
    const/4 v14, 0x0

    goto :goto_4

    .line 354
    .restart local v14    # "percentage":I
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->kiloMetersLabel:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 357
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->milesLabel:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 360
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->feetLabel:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 349
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
