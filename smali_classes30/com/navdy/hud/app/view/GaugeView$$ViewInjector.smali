.class public Lcom/navdy/hud/app/view/GaugeView$$ViewInjector;
.super Ljava/lang/Object;
.source "GaugeView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/GaugeView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/GaugeView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 9
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetView$$ViewInjector;->inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/DashboardWidgetView;Ljava/lang/Object;)V

    .line 12
    const v1, 0x7f0e00be

    const-string v2, "field \'mTvValue\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e00bd

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    .line 16
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/GaugeView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/GaugeView;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-static {p0}, Lcom/navdy/hud/app/view/DashboardWidgetView$$ViewInjector;->reset(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    .line 22
    iput-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    .line 23
    return-void
.end method
