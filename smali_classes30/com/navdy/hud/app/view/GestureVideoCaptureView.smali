.class public Lcom/navdy/hud/app/view/GestureVideoCaptureView;
.super Landroid/widget/RelativeLayout;
.source "GestureVideoCaptureView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;,
        Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;,
        Lcom/navdy/hud/app/view/GestureVideoCaptureView$ChoiceInfo;,
        Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;
    }
.end annotation


# static fields
.field public static final TAG_EXIT:I = 0x0

.field public static final TAG_NEXT:I = 0x1

.field private static final UPLOAD_TIMEOUT:J = 0x493e0L

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private emptyChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private exitChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mainHandler:Landroid/os/Handler;

.field mainImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mainText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0088
    .end annotation
.end field

.field private nextAndExitChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private prevState:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field private recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

.field private recordingsToUpload:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private registered:Z

.field secondaryText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field private serialExecutor:Ljava/util/concurrent/ExecutorService;

.field private sessionName:Ljava/lang/String;

.field sideImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d1
    .end annotation
.end field

.field private state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field private uploadTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 161
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 165
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 166
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    const v5, 0x7f0900fd

    const/4 v4, 0x0

    .line 169
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 81
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->registered:Z

    .line 95
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    .line 132
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->emptyChoices:Ljava/util/List;

    .line 134
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainHandler:Landroid/os/Handler;

    .line 170
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 173
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->serialExecutor:Ljava/util/concurrent/ExecutorService;

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextAndExitChoices:Ljava/util/List;

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextAndExitChoices:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v2, 0x7f0901d8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextAndExitChoices:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->exitChoices:Ljava/util/List;

    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->exitChoices:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/GestureVideoCaptureView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView;
    .param p1, "x1"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    return-void
.end method

.method private choicesForState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)Ljava/util/List;
    .locals 2
    .param p1, "state"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->emptyChoices:Ljava/util/List;

    :goto_0
    return-object v0

    .line 284
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextAndExitChoices:Ljava/util/List;

    goto :goto_0

    .line 289
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->exitChoices:Ljava/util/List;

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private createSession()V
    .locals 7

    .prologue
    .line 200
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->setCalibrationEnabled(Z)V

    .line 201
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    .line 202
    .local v1, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 203
    sget-object v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->NOT_PAIRED:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    .line 211
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 206
    .local v4, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd-HH-mm-ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 207
    .local v2, "format":Ljava/text/DateFormat;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "-"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    .line 209
    sget-object v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_LEFT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    goto :goto_0
.end method

.method private nextState()Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 296
    sget-object v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State:[I

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 316
    :goto_0
    :pswitch_0
    return-object v0

    .line 301
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SAVING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    goto :goto_0

    .line 303
    :pswitch_2
    sget-object v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State:[I

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->prevState:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 305
    :pswitch_3
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_RIGHT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    goto :goto_0

    .line 307
    :pswitch_4
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_1:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    goto :goto_0

    .line 309
    :pswitch_5
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_2:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    goto :goto_0

    .line 311
    :pswitch_6
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    goto :goto_0

    .line 296
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 303
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private resetSession()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 214
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->setCalibrationEnabled(Z)V

    .line 215
    iput-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 219
    iput-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    .line 221
    :cond_0
    return-void
.end method

.method private setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V
    .locals 4
    .param p1, "newState"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->prevState:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 236
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 237
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    sget-object v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SAVING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    if-ne v0, v1, :cond_1

    .line 240
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->secondaryText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget v1, v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->detailText:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 249
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->choicesForState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 252
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State:[I

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 276
    :goto_2
    return-void

    .line 231
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->stopRecording()V

    goto :goto_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget v1, v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->mainText:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 243
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget v0, v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->detailText:I

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->secondaryText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget v1, v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->detailText:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->secondaryText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 257
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->startRecording()V

    goto :goto_2

    .line 260
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$1;-><init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 269
    :cond_3
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;-><init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 252
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startRecording()V
    .locals 3

    .prologue
    .line 423
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Start recording"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 424
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;-><init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Ljava/lang/String;Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    .line 425
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->startRecordingVideo()V

    .line 426
    return-void
.end method

.method private stopRecording()V
    .locals 3

    .prologue
    .line 418
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stop recording for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget-object v2, v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->stopRecordingVideo()V

    .line 420
    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 343
    packed-switch p2, :pswitch_data_0

    .line 352
    :goto_0
    return-void

    .line 345
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->hideCaptureView()V

    goto :goto_0

    .line 348
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextState()Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    move-result-object v0

    .line 349
    .local v0, "nextState":Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    goto :goto_0

    .line 343
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 431
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 185
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 186
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sideImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainImageView:Landroid/widget/ImageView;

    const v1, 0x7f0201db

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    const v0, 0x7f0e0089

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 193
    const v0, 0x7f0e008b

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->secondaryText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->secondaryText:Landroid/widget/TextView;

    const/high16 v1, 0x41880000    # 17.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setHighlightPersistent(Z)V

    .line 197
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 440
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$3;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 442
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 445
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 448
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 440
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRecordingSaved(Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;)V
    .locals 9
    .param p1, "recordingSaved"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 356
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Recording saved"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 357
    iget-object v6, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    .line 358
    .local v6, "context":Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    if-nez v0, :cond_0

    .line 359
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore recording "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;->path:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " due to missing context."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 392
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    iget-object v0, v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;->sessionName:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Recording session "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;->sessionName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " doesn\'t match current session "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 364
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore recording "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;->path:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 367
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingToSave:Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;

    .line 368
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->nextState()Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    .line 370
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/storage/PathManager;->getGestureVideosSyncFolder()Ljava/lang/String;

    move-result-object v7

    .line 371
    .local v7, "videosSyncFolderPath":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v6, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;->sessionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 372
    .local v2, "sessionPath":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Session Path : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 373
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Video archive name to be saved : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v6, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->filename:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v6, Lcom/navdy/hud/app/view/GestureVideoCaptureView$RecordingSavingContext;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    iget-object v1, v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->filename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 375
    .local v4, "videoArchivePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 376
    iget-object v5, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    .line 377
    .local v5, "userTag":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->serialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;-><init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Ljava/lang/String;Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0
.end method

.method public onUploadFinished(Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;)V
    .locals 3
    .param p1, "result"    # Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 396
    iget-object v0, p1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->userTag:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->userTag:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    iget-boolean v0, p1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->succeeded:Z

    if-nez v0, :cond_2

    .line 402
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to upload: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 403
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING_FAILURE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    goto :goto_0

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    iget-object v1, p1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->filePath:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 406
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Remaining files to be uploaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 407
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->recordingsToUpload:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->state:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    sget-object v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    if-ne v0, v1, :cond_0

    .line 408
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 409
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->mainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->uploadTimeoutRunnable:Ljava/lang/Runnable;

    .line 412
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ALL_DONE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 322
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 323
    if-nez p1, :cond_1

    .line 324
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->setRecordMode(Z)V

    .line 325
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->createSession()V

    .line 326
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->registered:Z

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 328
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->registered:Z

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->stopRecordingVideo()V

    .line 332
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->resetSession()V

    .line 333
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->setRecordMode(Z)V

    .line 334
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->registered:Z

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 336
    iput-boolean v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->registered:Z

    goto :goto_0
.end method
