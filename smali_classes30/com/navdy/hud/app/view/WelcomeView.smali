.class public Lcom/navdy/hud/app/view/WelcomeView;
.super Landroid/widget/FrameLayout;
.source "WelcomeView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final DOWNLOAD_APP_STAY_INTERVAL:I

.field private static final DOWNLOAD_APP_TRANSITION_INTERVAL:I

.field public static final STATE_DOWNLOAD_APP:I = 0x1

.field public static final STATE_PICKER:I = 0x2

.field public static final STATE_UNKNOWN:I = -0x1

.field public static final STATE_WELCOME:I = 0x3

.field private static appStoreIcons:[I

.field private static downloadColor1:I

.field private static downloadColor2:I

.field private static downloadText1:[Ljava/lang/String;

.field private static downloadText2:[Ljava/lang/String;

.field private static playStoreIcons:[I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private animationRunnable:Ljava/lang/Runnable;

.field public animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d8
    .end annotation
.end field

.field public appStoreImage1:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d4
    .end annotation
.end field

.field public appStoreImage2:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d5
    .end annotation
.end field

.field public carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01db
    .end annotation
.end field

.field public choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field private currentDownloadIndex:I

.field private currentDownloadView:I

.field public downloadAppContainer:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00b0
    .end annotation
.end field

.field public downloadAppTextView1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d1
    .end annotation
.end field

.field public downloadAppTextView2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d2
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private initialState:I

.field public leftDot:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d9
    .end annotation
.end field

.field public list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;"
        }
    .end annotation
.end field

.field public messageView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e010a
    .end annotation
.end field

.field public playStoreImage1:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d6
    .end annotation
.end field

.field public playStoreImage2:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01d7
    .end annotation
.end field

.field public presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public rightDot:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01da
    .end annotation
.end field

.field public rootContainer:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014b
    .end annotation
.end field

.field roundTransformation:Lcom/squareup/picasso/Transformation;

.field private searching:Z

.field private state:I

.field public subTitleView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0111
    .end annotation
.end field

.field public titleView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00c3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/WelcomeView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/WelcomeView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 70
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I

    .line 71
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sget v1, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I

    sub-int/2addr v0, v1

    sput v0, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_STAY_INTERVAL:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/WelcomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/WelcomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v0, -0x1

    .line 210
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 130
    iput v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->state:I

    .line 133
    iput v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    .line 134
    iput v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->list:Ljava/util/List;

    .line 137
    new-instance v0, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v0}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;

    .line 141
    new-instance v0, Lcom/navdy/hud/app/view/WelcomeView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/WelcomeView$1;-><init>(Lcom/navdy/hud/app/view/WelcomeView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;

    .line 211
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/WelcomeView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 215
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/WelcomeView;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 216
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/WelcomeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/view/WelcomeView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    return p1
.end method

.method static synthetic access$008(Lcom/navdy/hud/app/view/WelcomeView;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/WelcomeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/view/WelcomeView;Ljava/io/File;Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # Lcom/squareup/picasso/Callback;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/view/WelcomeView;->setImage(Ljava/io/File;Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/view/WelcomeView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/view/WelcomeView;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/WelcomeView;->updateInfo(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I

    return v0
.end method

.method static synthetic access$300()[I
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/view/WelcomeView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/view/WelcomeView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_STAY_INTERVAL:I

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/view/WelcomeView;Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadText(Landroid/widget/TextView;I)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/view/WelcomeView;Landroid/widget/ImageView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V

    return-void
.end method

.method static synthetic access$900()[I
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    return-object v0
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 219
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->WelcomeView:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 225
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->initialState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 229
    return-void

    .line 227
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private initializeCarousel()V
    .locals 3

    .prologue
    .line 274
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;-><init>()V

    .line 275
    .local v0, "initParams":Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->list:Ljava/util/List;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->model:Ljava/util/List;

    .line 276
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->rootContainer:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->rootContainer:Landroid/view/View;

    .line 277
    new-instance v1, Lcom/navdy/hud/app/view/WelcomeView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/WelcomeView$3;-><init>(Lcom/navdy/hud/app/view/WelcomeView;)V

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    .line 349
    const v1, 0x7f030007

    iput v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->infoLayoutResourceId:I

    .line 350
    const v1, 0x7f030010

    iput v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->imageLytResourceId:I

    .line 351
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 352
    new-instance v1, Lcom/navdy/hud/app/ui/component/carousel/ShrinkAnimator;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/carousel/ShrinkAnimator;-><init>()V

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    .line 353
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->init(Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;)V

    .line 355
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    new-instance v2, Lcom/navdy/hud/app/view/WelcomeView$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/WelcomeView$4;-><init>(Lcom/navdy/hud/app/view/WelcomeView;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setListener(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;)V

    .line 391
    return-void
.end method

.method private setDownloadImage(Landroid/widget/ImageView;I)V
    .locals 0
    .param p1, "image"    # Landroid/widget/ImageView;
    .param p2, "resId"    # I

    .prologue
    .line 548
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 549
    return-void
.end method

.method private setDownloadText(Landroid/widget/TextView;I)V
    .locals 6
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "index"    # I

    .prologue
    const/16 v5, 0x21

    .line 536
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 537
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    sget-object v2, Lcom/navdy/hud/app/view/WelcomeView;->downloadText1:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 538
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    sget v3, Lcom/navdy/hud/app/view/WelcomeView;->downloadColor1:I

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 539
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 540
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 541
    .local v1, "start":I
    sget-object v2, Lcom/navdy/hud/app/view/WelcomeView;->downloadText2:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 542
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    sget v3, Lcom/navdy/hud/app/view/WelcomeView;->downloadColor2:I

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 543
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 544
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    return-void
.end method

.method private setImage(Ljava/io/File;Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "callback"    # Lcom/squareup/picasso/Callback;

    .prologue
    .line 412
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 413
    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noPlaceholder()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 415
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 416
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 418
    invoke-virtual {v0, p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 419
    return-void
.end method

.method private startDownloadAppAnimation()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 488
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "startDownloadAppAnimation"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/WelcomeView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 490
    .local v3, "resources":Landroid/content/res/Resources;
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadText1:[Ljava/lang/String;

    if-nez v5, :cond_2

    .line 491
    const v5, 0x7f0a0007

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadText1:[Ljava/lang/String;

    .line 492
    const v5, 0x7f0a0008

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadText2:[Ljava/lang/String;

    .line 493
    const v5, 0x7f0d0011

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sput v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadColor1:I

    .line 494
    const v5, 0x7f0d0012

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sput v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadColor2:I

    .line 496
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0009

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 497
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadText1:[Ljava/lang/String;

    array-length v5, v5

    new-array v5, v5, [I

    sput-object v5, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    .line 498
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 499
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    aput v6, v5, v0

    .line 498
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 501
    :cond_0
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 504
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 505
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->downloadText1:[Ljava/lang/String;

    array-length v5, v5

    new-array v5, v5, [I

    sput-object v5, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    .line 506
    const/4 v0, 0x0

    :goto_1
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 507
    sget-object v5, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    aput v6, v5, v0

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 509
    :cond_1
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 513
    .end local v0    # "i":I
    .end local v4    # "typedArray":Landroid/content/res/TypedArray;
    :cond_2
    iput v7, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    .line 514
    iput v7, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I

    .line 515
    iget v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    .line 516
    .local v1, "index1":I
    iget v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    add-int/lit8 v2, v5, 0x1

    .line 517
    .local v2, "index2":I
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    invoke-direct {p0, v5, v1}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadText(Landroid/widget/TextView;I)V

    .line 518
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    invoke-direct {p0, v5, v2}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadText(Landroid/widget/TextView;I)V

    .line 519
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    sget-object v6, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    aget v6, v6, v1

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V

    .line 520
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    sget-object v6, Lcom/navdy/hud/app/view/WelcomeView;->appStoreIcons:[I

    aget v6, v6, v2

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V

    .line 521
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    sget-object v6, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    aget v6, v6, v1

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V

    .line 522
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    sget-object v6, Lcom/navdy/hud/app/view/WelcomeView;->playStoreIcons:[I

    aget v6, v6, v2

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/view/WelcomeView;->setDownloadImage(Landroid/widget/ImageView;I)V

    .line 523
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setAlpha(F)V

    .line 524
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setAlpha(F)V

    .line 525
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 526
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 527
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 528
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 529
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 530
    iget v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    .line 531
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 532
    iget-object v5, p0, Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;

    sget v7, Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_STAY_INTERVAL:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 533
    return-void
.end method

.method private stopDownloadAppAnimation()V
    .locals 2

    .prologue
    .line 552
    sget-object v0, Lcom/navdy/hud/app/view/WelcomeView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopDownloadAppAnimation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->animationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 554
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 555
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 556
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 557
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 558
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 559
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 560
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppContainer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 561
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I

    .line 562
    return-void
.end method

.method private updateInfo(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;)V
    .locals 5
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    .prologue
    .line 395
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    .line 396
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v4, 0x7f0e00c3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 397
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v4, 0x7f0e0111

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 398
    .local v1, "subTitle":Ljava/lang/String;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v4, 0x7f0e010a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 399
    .local v0, "message":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/view/WelcomeView;->titleView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .end local v2    # "title":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v3, p0, Lcom/navdy/hud/app/view/WelcomeView;->subTitleView:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .end local v1    # "subTitle":Ljava/lang/String;
    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v3, p0, Lcom/navdy/hud/app/view/WelcomeView;->messageView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .end local v0    # "message":Ljava/lang/String;
    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :cond_0
    return-void

    .line 399
    .restart local v0    # "message":Ljava/lang/String;
    .restart local v1    # "subTitle":Ljava/lang/String;
    .restart local v2    # "title":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 400
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 401
    .end local v1    # "subTitle":Ljava/lang/String;
    :cond_3
    const-string v0, ""

    goto :goto_2
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 484
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 423
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 424
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 427
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->searching:Z

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    .line 430
    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 434
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 435
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->searching:Z

    .line 436
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 437
    invoke-direct {p0}, Lcom/navdy/hud/app/view/WelcomeView;->stopDownloadAppAnimation()V

    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->presenter:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 440
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 234
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 235
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 236
    iget v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->initialState:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/WelcomeView;->setState(I)V

    .line 237
    invoke-direct {p0}, Lcom/navdy/hud/app/view/WelcomeView;->initializeCarousel()V

    .line 239
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 240
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090300

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    new-instance v3, Lcom/navdy/hud/app/view/WelcomeView$2;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/view/WelcomeView$2;-><init>(Lcom/navdy/hud/app/view/WelcomeView;)V

    invoke-virtual {v1, v2, v0, v4, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 250
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 457
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 462
    iget v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->state:I

    if-ne v1, v0, :cond_0

    .line 463
    sget-object v1, Lcom/navdy/hud/app/view/WelcomeView$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 476
    const/4 v0, 0x0

    .line 478
    :goto_0
    return v0

    .line 465
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 469
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 473
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSearching(Z)V
    .locals 2
    .param p1, "searching"    # Z

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->searching:Z

    if-eq p1, v0, :cond_0

    .line 444
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/WelcomeView;->searching:Z

    .line 445
    if-nez p1, :cond_1

    .line 446
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 447
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 450
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    goto :goto_0
.end method

.method public setState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/16 v1, 0x8

    .line 253
    iget v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->state:I

    if-ne v0, p1, :cond_0

    .line 271
    :goto_0
    return-void

    .line 256
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/view/WelcomeView;->state:I

    .line 257
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->rootContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 258
    invoke-direct {p0}, Lcom/navdy/hud/app/view/WelcomeView;->stopDownloadAppAnimation()V

    .line 259
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->leftDot:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->rightDot:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 264
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/WelcomeView;->startDownloadAppAnimation()V

    goto :goto_0

    .line 268
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->rootContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
