.class public Lcom/navdy/hud/app/view/SpeedLimitSignView;
.super Landroid/widget/FrameLayout;
.source "SpeedLimitSignView.java"


# instance fields
.field protected euSpeedLimitSignView:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e2
    .end annotation
.end field

.field private speedLimit:I

.field private speedLimitTextView:Landroid/widget/TextView;

.field private speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

.field protected usSpeedLimitSignView:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e0
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 40
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 45
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03006e

    invoke-static {v0, v1, p0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03006d

    invoke-static {v0, v1, p0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 48
    return-void
.end method


# virtual methods
.method public getSpeedLimitUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimitUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V

    .line 54
    return-void
.end method

.method public setSpeedLimit(I)V
    .locals 2
    .param p1, "speedLimit"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimit:I

    if-eq v0, p1, :cond_0

    .line 78
    iput p1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimit:I

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_0
    return-void
.end method

.method public setSpeedLimitUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V
    .locals 4
    .param p1, "speedLimitUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    const/16 v3, 0x8

    .line 57
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    if-eq v1, p1, :cond_0

    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "speedLimitView":Landroid/view/ViewGroup;
    sget-object v1, Lcom/navdy/hud/app/view/SpeedLimitSignView$1;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    iget-object v2, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 70
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 71
    const v1, 0x7f0e01e1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimitTextView:Landroid/widget/TextView;

    .line 72
    iget v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->speedLimit:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimit(I)V

    .line 74
    .end local v0    # "speedLimitView":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 62
    .restart local v0    # "speedLimitView":Landroid/view/ViewGroup;
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->usSpeedLimitSignView:Landroid/view/ViewGroup;

    .line 63
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->euSpeedLimitSignView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->euSpeedLimitSignView:Landroid/view/ViewGroup;

    .line 67
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignView;->usSpeedLimitSignView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
