.class public Lcom/navdy/hud/app/view/DialUpdateConfirmationView;
.super Landroid/widget/RelativeLayout;
.source "DialUpdateConfirmationView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final UPDATE_MESSAGE_MAX_WIDTH:I = 0x17c

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private isReminder:Z

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field mInfoText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field mLefttSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d3
    .end annotation
.end field

.field mMainTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mRightSwipe:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d4
    .end annotation
.end field

.field mScreenTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mSummaryText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008b
    .end annotation
.end field

.field mainSection:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cd
    .end annotation
.end field

.field private updateTargetVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    const-string v0, "1.0.60"

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->updateTargetVersion:Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isReminder:Z

    .line 78
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    const/4 v2, 0x1

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 162
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->install()V

    .line 163
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isReminder:Z

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->updateTargetVersion:Ljava/lang/String;

    invoke-static {v2, v2, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordUpdatePrompt(ZZLjava/lang/String;)V

    goto :goto_0

    .line 168
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->finish()V

    .line 169
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isReminder:Z

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->updateTargetVersion:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordUpdatePrompt(ZZLjava/lang/String;)V

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 178
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 137
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 5

    .prologue
    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 142
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 143
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    const/4 v0, 0x1

    .line 144
    .local v0, "enableNotif":Z
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v2, v3, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 147
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableNotif:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " screen["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v1, :cond_3

    const-string v2, "none"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 148
    if-eqz v0, :cond_1

    .line 149
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 150
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 153
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    if-eqz v2, :cond_2

    .line 154
    iget-object v2, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    invoke-virtual {v2, p0}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 156
    :cond_2
    return-void

    .line 147
    :cond_3
    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 14

    .prologue
    const/16 v13, 0x17c

    const/16 v12, 0x8

    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 89
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 90
    const-string v4, "1.0.1"

    .line 91
    .local v4, "updateFromVersion":Ljava/lang/String;
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 92
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    if-eqz v7, :cond_0

    .line 93
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    invoke-virtual {v7, p0}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 94
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;

    invoke-virtual {v7}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->isReminder()Z

    move-result v7

    iput-boolean v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isReminder:Z

    .line 95
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v7

    invoke-virtual {v7, v11}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 96
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 97
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v6

    .line 98
    .local v6, "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1.0."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v6, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v8, v8, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->updateTargetVersion:Ljava/lang/String;

    .line 99
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1.0."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v6, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v8, v8, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 101
    .end local v6    # "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mScreenTitleText:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    const v7, 0x7f0e0088

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 103
    .local v3, "title1":Landroid/view/View;
    invoke-virtual {v3, v12}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v8, 0x7f0900d6

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 106
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mainSection:Landroid/widget/RelativeLayout;

    const/16 v8, 0xa

    invoke-static {v7, v10, v10, v10, v8}, Lcom/navdy/hud/app/util/ViewUtil;->adjustPadding(Landroid/view/View;IIII)V

    .line 107
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    const v8, 0x7f0a0014

    invoke-static {v7, v9, v13, v8}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;III)V

    .line 108
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mMainTitleText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mIcon:Landroid/widget/ImageView;

    const v8, 0x7f020103

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 110
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mInfoText:Landroid/widget/TextView;

    const v8, 0x7f0900d5

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 111
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    const v7, 0x7f0e00bf

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 113
    .local v1, "maxWidthLayout":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    invoke-virtual {v1, v13}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 114
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 115
    .local v2, "res":Landroid/content/res/Resources;
    const v7, 0x7f0900d9

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->updateTargetVersion:Ljava/lang/String;

    aput-object v9, v8, v10

    aput-object v4, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 118
    .local v5, "updateMessage":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mSummaryText:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mSummaryText:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-boolean v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->isReminder:Z

    if-eqz v7, :cond_1

    .line 122
    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v8, 0x7f09018b

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v8, 0x7f09018a

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :goto_0
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v7, v8, v0, v10, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 129
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mRightSwipe:Landroid/widget/ImageView;

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    iget-object v7, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mLefttSwipe:Landroid/widget/ImageView;

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 131
    return-void

    .line 125
    :cond_1
    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v8, 0x7f090188

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v10}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v8, 0x7f09004f

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_0

    .line 183
    sget-object v2, Lcom/navdy/hud/app/view/DialUpdateConfirmationView$1;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 193
    :goto_0
    return v0

    .line 185
    :pswitch_0
    invoke-virtual {p0, v1, v1}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 189
    :pswitch_1
    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->executeItem(II)V

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 199
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 200
    sget-object v1, Lcom/navdy/hud/app/view/DialUpdateConfirmationView$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 213
    :goto_0
    return v0

    .line 202
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 205
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 208
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateConfirmationView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
