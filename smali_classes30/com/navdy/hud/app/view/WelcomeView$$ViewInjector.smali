.class public Lcom/navdy/hud/app/view/WelcomeView$$ViewInjector;
.super Ljava/lang/Object;
.source "WelcomeView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/WelcomeView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/WelcomeView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00c3

    const-string v2, "field \'titleView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->titleView:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0111

    const-string v2, "field \'subTitleView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->subTitleView:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e010a

    const-string v2, "field \'messageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->messageView:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e01d8

    const-string v2, "field \'animatorView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .line 18
    const v1, 0x7f0e01db

    const-string v2, "field \'carousel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .line 20
    const v1, 0x7f0e014b

    const-string v2, "field \'rootContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->rootContainer:Landroid/view/View;

    .line 22
    const v1, 0x7f0e00b0

    const-string v2, "field \'downloadAppContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/RelativeLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppContainer:Landroid/widget/RelativeLayout;

    .line 24
    const v1, 0x7f0e01d1

    const-string v2, "field \'downloadAppTextView1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f0e01d2

    const-string v2, "field \'downloadAppTextView2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    .line 28
    const v1, 0x7f0e01d4

    const-string v2, "field \'appStoreImage1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    .line 30
    const v1, 0x7f0e01d5

    const-string v2, "field \'appStoreImage2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    .line 32
    const v1, 0x7f0e01d6

    const-string v2, "field \'playStoreImage1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    .line 34
    const v1, 0x7f0e01d7

    const-string v2, "field \'playStoreImage2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    .line 36
    const v1, 0x7f0e00d2

    const-string v2, "field \'choiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 38
    const v1, 0x7f0e01d9

    const-string v2, "field \'leftDot\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->leftDot:Landroid/widget/ImageView;

    .line 40
    const v1, 0x7f0e01da

    const-string v2, "field \'rightDot\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/WelcomeView;->rightDot:Landroid/widget/ImageView;

    .line 42
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/WelcomeView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->titleView:Landroid/widget/TextView;

    .line 46
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->subTitleView:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->messageView:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->animatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .line 49
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .line 50
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->rootContainer:Landroid/view/View;

    .line 51
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppContainer:Landroid/widget/RelativeLayout;

    .line 52
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    .line 53
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    .line 54
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    .line 55
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    .line 56
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    .line 57
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    .line 58
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 59
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->leftDot:Landroid/widget/ImageView;

    .line 60
    iput-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView;->rightDot:Landroid/widget/ImageView;

    .line 61
    return-void
.end method
