.class Lcom/navdy/hud/app/view/DialManagerView$6;
.super Ljava/lang/Object;
.source "DialManagerView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/DialManagerView;->showDialPairingState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/DialManagerView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/DialManagerView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/navdy/hud/app/view/DialManagerView$6;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 387
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView$6;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    # ++operator for: Lcom/navdy/hud/app/view/DialManagerView;->completeCount:I
    invoke-static {v0}, Lcom/navdy/hud/app/view/DialManagerView;->access$204(Lcom/navdy/hud/app/view/DialManagerView;)I

    .line 388
    # getter for: Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/DialManagerView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "completed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/DialManagerView$6;->this$0:Lcom/navdy/hud/app/view/DialManagerView;

    # getter for: Lcom/navdy/hud/app/view/DialManagerView;->completeCount:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/DialManagerView;->access$200(Lcom/navdy/hud/app/view/DialManagerView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 389
    return-void
.end method
