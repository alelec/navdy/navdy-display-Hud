.class public Lcom/navdy/hud/app/view/IndicatorView;
.super Landroid/view/View;
.source "IndicatorView.java"


# instance fields
.field private indicatorPaint:Landroid/graphics/Paint;

.field private indicatorPath:Landroid/graphics/Path;

.field private mCurveRadius:F

.field private mIndicatorHeight:I

.field private mIndicatorHeightAttribute:Lcom/navdy/hud/app/util/CustomDimension;

.field private mIndicatorStrokeWidth:F

.field private mIndicatorWidth:I

.field private mIndicatorWidthAttribute:Lcom/navdy/hud/app/util/CustomDimension;

.field private mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/IndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/IndicatorView;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public static drawIndicatorPath(Landroid/graphics/Path;FFFFFF)V
    .locals 8
    .param p0, "path"    # Landroid/graphics/Path;
    .param p1, "left"    # F
    .param p2, "bottom"    # F
    .param p3, "curveRadius"    # F
    .param p4, "indicatorWidth"    # F
    .param p5, "indicatorHeight"    # F
    .param p6, "width"    # F

    .prologue
    .line 48
    const/high16 v1, 0x3f000000    # 0.5f

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/view/IndicatorView;->drawIndicatorPath(Landroid/graphics/Path;FFFFFFF)V

    .line 49
    return-void
.end method

.method public static drawIndicatorPath(Landroid/graphics/Path;FFFFFFF)V
    .locals 14
    .param p0, "path"    # Landroid/graphics/Path;
    .param p1, "value"    # F
    .param p2, "left"    # F
    .param p3, "bottom"    # F
    .param p4, "curveRadius"    # F
    .param p5, "indicatorWidth"    # F
    .param p6, "indicatorHeight"    # F
    .param p7, "width"    # F

    .prologue
    .line 52
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 53
    add-float v7, p2, p7

    .line 55
    .local v7, "right":F
    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, p5, v10

    div-float v5, p6, v10

    .line 56
    .local v5, "indicatorSlope":F
    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    .line 57
    .local v2, "angle":D
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    move/from16 v0, p4

    float-to-double v12, v0

    mul-double/2addr v10, v12

    double-to-float v8, v10

    .line 58
    .local v8, "rise":F
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    move/from16 v0, p4

    float-to-double v12, v0

    mul-double/2addr v10, v12

    double-to-float v9, v10

    .line 59
    .local v9, "run":F
    const/high16 v10, 0x40000000    # 2.0f

    div-float v4, p5, v10

    .line 60
    .local v4, "indicatorRadius":F
    add-float v10, p2, v4

    sub-float v11, p7, p5

    mul-float/2addr v11, p1

    add-float v6, v10, v11

    .line 62
    .local v6, "middle":F
    sub-float v10, v6, v4

    sub-float v10, v10, p4

    move/from16 v0, p3

    invoke-virtual {p0, v10, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 63
    sub-float v10, v6, v4

    sub-float v11, v6, v4

    add-float/2addr v11, v9

    sub-float v12, p3, v8

    move/from16 v0, p3

    invoke-virtual {p0, v10, v0, v11, v12}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 64
    sub-float v10, p3, p6

    invoke-virtual {p0, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 65
    add-float v10, v6, v4

    sub-float/2addr v10, v9

    sub-float v11, p3, v8

    invoke-virtual {p0, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 66
    add-float v10, v6, v4

    add-float v11, v6, v4

    add-float v11, v11, p4

    move/from16 v0, p3

    move/from16 v1, p3

    invoke-virtual {p0, v10, v0, v11, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 67
    move/from16 v0, p3

    invoke-virtual {p0, v7, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    return-void
.end method

.method private initDrawingTools()V
    .locals 17

    .prologue
    .line 99
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPath:Landroid/graphics/Path;

    .line 100
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorStrokeWidth:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 106
    const/4 v2, 0x4

    new-array v6, v2, [I

    fill-array-data v6, :array_0

    .line 107
    .local v6, "colors":[I
    const/4 v2, 0x4

    new-array v7, v2, [F

    fill-array-data v7, :array_1

    .line 109
    .local v7, "positions":[F
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getPaddingLeft()I

    move-result v16

    .line 111
    .local v16, "left":I
    new-instance v1, Landroid/graphics/LinearGradient;

    move/from16 v0, v16

    int-to-float v2, v0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getWidth()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 112
    .local v1, "shader":Landroid/graphics/Shader;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 114
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/view/IndicatorView;->mValue:F

    move/from16 v0, v16

    int-to-float v10, v0

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorStrokeWidth:F

    sub-float v11, v2, v3

    move-object/from16 v0, p0

    iget v12, v0, Lcom/navdy/hud/app/view/IndicatorView;->mCurveRadius:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidth:I

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorHeight:I

    int-to-float v14, v2

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/IndicatorView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v15, v2

    invoke-static/range {v8 .. v15}, Lcom/navdy/hud/app/view/IndicatorView;->drawIndicatorPath(Landroid/graphics/Path;FFFFFFF)V

    .line 115
    return-void

    .line 106
    :array_0
    .array-data 4
        0x0
        -0x1
        -0x1
        0x0
    .end array-data

    .line 107
    :array_1
    .array-data 4
        0x0
        0x3ee66666    # 0.45f
        0x3f0ccccd    # 0.55f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->Indicator:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 77
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/hud/app/util/CustomDimension;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidthAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    .line 78
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/hud/app/util/CustomDimension;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorHeightAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    .line 79
    const/4 v1, 0x2

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mCurveRadius:F

    .line 80
    const/4 v1, 0x3

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorStrokeWidth:F

    .line 81
    const/4 v1, 0x4

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mValue:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 85
    return-void

    .line 83
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method


# virtual methods
.method public evaluateDimensions(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorHeightAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    int-to-float v1, p1

    invoke-virtual {v0, p0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorHeight:I

    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidthAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    int-to-float v1, p1

    invoke-virtual {v0, p0, v1, v2}, Lcom/navdy/hud/app/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidth:I

    .line 90
    return-void
.end method

.method public getCurveRadius()F
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mCurveRadius:F

    return v0
.end method

.method public getIndicatorHeight()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorHeight:I

    return v0
.end method

.method public getIndicatorWidth()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidth:I

    return v0
.end method

.method public getIndicatorWidthAttribute()Lcom/navdy/hud/app/util/CustomDimension;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorWidthAttribute:Lcom/navdy/hud/app/util/CustomDimension;

    return-object v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mIndicatorStrokeWidth:F

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mValue:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/navdy/hud/app/view/IndicatorView;->indicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 142
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/view/IndicatorView;->evaluateDimensions(II)V

    .line 95
    invoke-direct {p0}, Lcom/navdy/hud/app/view/IndicatorView;->initDrawingTools()V

    .line 96
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 128
    iget v0, p0, Lcom/navdy/hud/app/view/IndicatorView;->mValue:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 129
    iput p1, p0, Lcom/navdy/hud/app/view/IndicatorView;->mValue:F

    .line 130
    invoke-direct {p0}, Lcom/navdy/hud/app/view/IndicatorView;->initDrawingTools()V

    .line 131
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/IndicatorView;->invalidate()V

    .line 133
    :cond_0
    return-void
.end method
