.class Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;
.super Ljava/lang/Object;
.source "ShutDownConfirmationView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ShutDownConfirmationView;->startCountDown()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ShutDownConfirmationView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ShutDownConfirmationView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ShutDownConfirmationView;

    .prologue
    .line 448
    iput-object p1, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;->this$0:Lcom/navdy/hud/app/view/ShutDownConfirmationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 451
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 452
    .local v1, "value":I
    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;->this$0:Lcom/navdy/hud/app/view/ShutDownConfirmationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900d4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "installationBeginMessage":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/view/ShutDownConfirmationView$4;->this$0:Lcom/navdy/hud/app/view/ShutDownConfirmationView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/ShutDownConfirmationView;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    return-void
.end method
