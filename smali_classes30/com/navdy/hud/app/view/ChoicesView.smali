.class public abstract Lcom/navdy/hud/app/view/ChoicesView;
.super Landroid/widget/RelativeLayout;
.source "ChoicesView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;


# instance fields
.field protected animateItemOnExecution:Z

.field protected animateItemOnSelection:Z

.field protected detector:Lcom/navdy/hud/app/gesture/GestureDetector;

.field protected executeAnimation:Landroid/view/animation/Animation;

.field protected lastTouchX:I

.field protected lastTouchY:I

.field private listener:Landroid/view/animation/Animation$AnimationListener;

.field protected logger:Lcom/navdy/service/library/log/Logger;

.field protected selectedItem:I

.field protected wrapAround:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/ChoicesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/ChoicesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->wrapAround:Z

    .line 54
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/ChoicesView;->animateItemOnExecution:Z

    .line 57
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/ChoicesView;->animateItemOnSelection:Z

    .line 188
    new-instance v0, Lcom/navdy/hud/app/view/ChoicesView$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/ChoicesView$3;-><init>(Lcom/navdy/hud/app/view/ChoicesView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->listener:Landroid/view/animation/Animation$AnimationListener;

    .line 72
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureDetector;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/gesture/GestureDetector;-><init>(Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->detector:Lcom/navdy/hud/app/gesture/GestureDetector;

    .line 73
    return-void
.end method

.method private buildBounceAnim(FF)Landroid/animation/Animator;
    .locals 5
    .param p1, "edgeValue"    # F
    .param p2, "backToValue"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 323
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 324
    .local v0, "s":Landroid/animation/AnimatorSet;
    const-string v1, "value"

    new-array v2, v3, [F

    aput p1, v2, v4

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    const-string v2, "value"

    new-array v3, v3, [F

    aput p2, v3, v4

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 325
    return-object v0
.end method

.method private moveSelectionLeft()V
    .locals 2

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v0

    .line 307
    .local v0, "itemCount":I
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    if-lez v1, :cond_0

    if-lez v0, :cond_0

    .line 308
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v0

    rem-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->setSelectedItem(I)V

    .line 310
    :cond_0
    return-void
.end method

.method private moveSelectionRight()V
    .locals 3

    .prologue
    .line 313
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v0

    .line 317
    .local v0, "itemCount":I
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v0, :cond_0

    .line 318
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    add-int/lit8 v1, v1, 0x1

    rem-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->setSelectedItem(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract animateDown()V
.end method

.method protected abstract animateUp()V
.end method

.method public executeSelectedItem(Z)V
    .locals 4
    .param p1, "animated"    # Z

    .prologue
    .line 207
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "USER_STUDY executing option "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 209
    if-eqz p1, :cond_2

    .line 211
    iget-object v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->executeAnimation:Landroid/view/animation/Animation;

    if-nez v1, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->executeAnimation:Landroid/view/animation/Animation;

    .line 213
    iget-object v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->executeAnimation:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ChoicesView;->listener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 215
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->getItemAt(I)Landroid/view/View;

    move-result-object v0

    .line 216
    .local v0, "childView":Landroid/view/View;
    iget-object v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->executeAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 221
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 218
    :cond_2
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->onExecuteItem(I)V

    goto :goto_0
.end method

.method public abstract getItemAt(I)Landroid/view/View;
.end method

.method public abstract getItemCount()I
.end method

.method public getSelectedItem()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    return v0
.end method

.method public abstract getValue()F
.end method

.method public indexForValue(F)I
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v0

    .line 175
    .local v0, "itemCount":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 176
    const/4 v1, 0x0

    .line 178
    :goto_0
    return v1

    :cond_0
    add-int/lit8 v1, v0, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    goto :goto_0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 330
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 79
    new-instance v0, Lcom/navdy/hud/app/view/ChoicesView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/ChoicesView$1;-><init>(Lcom/navdy/hud/app/view/ChoicesView;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ChoicesView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 90
    new-instance v0, Lcom/navdy/hud/app/view/ChoicesView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/ChoicesView$2;-><init>(Lcom/navdy/hud/app/view/ChoicesView;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ChoicesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    return-void
.end method

.method public onClick()V
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ChoicesView;->animateItemOnExecution:Z

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ChoicesView;->executeSelectedItem(Z)V

    .line 226
    return-void
.end method

.method protected onDeselectItem(Landroid/view/View;I)V
    .locals 0
    .param p1, "choice"    # Landroid/view/View;
    .param p2, "newSelectionIndex"    # I

    .prologue
    .line 135
    return-void
.end method

.method public abstract onExecuteItem(I)V
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 6
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 235
    iget-object v4, p0, Lcom/navdy/hud/app/view/ChoicesView;->detector:Lcom/navdy/hud/app/gesture/GestureDetector;

    invoke-virtual {v4, p1}, Lcom/navdy/hud/app/gesture/GestureDetector;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    .line 236
    iget-object v1, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 237
    .local v1, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    sget-object v4, Lcom/navdy/hud/app/view/ChoicesView$4;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_0
    move v2, v3

    .line 273
    :goto_1
    return v2

    .line 240
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->animateUp()V

    goto :goto_0

    .line 244
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->animateDown()V

    .line 245
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/view/ChoicesView;->setSelectedItem(I)V

    goto :goto_0

    .line 249
    :pswitch_2
    iget-boolean v4, p0, Lcom/navdy/hud/app/view/ChoicesView;->wrapAround:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    if-nez v4, :cond_0

    .line 251
    const/4 v4, 0x0

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/view/ChoicesView;->valueForIndex(I)F

    move-result v3

    invoke-direct {p0, v4, v3}, Lcom/navdy/hud/app/view/ChoicesView;->buildBounceAnim(FF)Landroid/animation/Animator;

    move-result-object v0

    .line 252
    .local v0, "animator":Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1

    .line 254
    .end local v0    # "animator":Landroid/animation/Animator;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ChoicesView;->moveSelectionLeft()V

    goto :goto_1

    .line 259
    :pswitch_3
    iget-boolean v3, p0, Lcom/navdy/hud/app/view/ChoicesView;->wrapAround:Z

    if-nez v3, :cond_1

    iget v3, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 261
    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/view/ChoicesView;->valueForIndex(I)F

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/navdy/hud/app/view/ChoicesView;->buildBounceAnim(FF)Landroid/animation/Animator;

    move-result-object v0

    .line 262
    .restart local v0    # "animator":Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1

    .line 264
    .end local v0    # "animator":Landroid/animation/Animator;
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ChoicesView;->moveSelectionRight()V

    goto :goto_1

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 278
    iget v2, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->animateUp()V

    move v0, v1

    .line 302
    :goto_0
    return v0

    .line 282
    :cond_0
    const/4 v0, 0x0

    .line 283
    .local v0, "handled":Z
    sget-object v2, Lcom/navdy/hud/app/view/ChoicesView$4;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 285
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ChoicesView;->moveSelectionLeft()V

    .line 286
    const/4 v0, 0x1

    .line 287
    goto :goto_0

    .line 291
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ChoicesView;->moveSelectionRight()V

    .line 292
    const/4 v0, 0x1

    .line 293
    goto :goto_0

    .line 297
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->executeSelectedItem(Z)V

    .line 298
    const/4 v0, 0x1

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onSelectItem(Landroid/view/View;I)V
    .locals 0
    .param p1, "choice"    # Landroid/view/View;
    .param p2, "oldSelectionIndex"    # I

    .prologue
    .line 137
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "position"    # F

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/ChoicesView;->setValue(F)V

    .line 231
    return-void
.end method

.method public setSelectedItem(I)V
    .locals 9
    .param p1, "index"    # I

    .prologue
    const/4 v8, -0x1

    .line 144
    iget v5, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    if-eq p1, v5, :cond_4

    .line 145
    if-gez p1, :cond_5

    const-string v3, "none"

    .line 146
    .local v3, "option":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/navdy/hud/app/view/ChoicesView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selecting option - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 148
    const/4 v2, 0x0

    .line 149
    .local v2, "oldView":Landroid/view/View;
    const/4 v0, 0x0

    .line 150
    .local v0, "newView":Landroid/view/View;
    iget v5, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    if-eq v5, v8, :cond_0

    .line 151
    iget v5, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/view/ChoicesView;->getItemAt(I)Landroid/view/View;

    move-result-object v2

    .line 153
    :cond_0
    if-eq p1, v8, :cond_1

    .line 154
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/ChoicesView;->getItemAt(I)Landroid/view/View;

    move-result-object v0

    .line 156
    :cond_1
    if-eqz v2, :cond_2

    .line 157
    invoke-virtual {p0, v2, p1}, Lcom/navdy/hud/app/view/ChoicesView;->onDeselectItem(Landroid/view/View;I)V

    .line 160
    :cond_2
    iget v1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    .line 161
    .local v1, "oldIndex":I
    iput p1, p0, Lcom/navdy/hud/app/view/ChoicesView;->selectedItem:I

    .line 163
    if-eqz v0, :cond_3

    .line 164
    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/view/ChoicesView;->onSelectItem(Landroid/view/View;I)V

    .line 166
    :cond_3
    if-eq p1, v8, :cond_4

    .line 167
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/ChoicesView;->valueForIndex(I)F

    move-result v4

    .line 168
    .local v4, "targetValue":F
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/view/ChoicesView;->setValue(F)V

    .line 171
    .end local v0    # "newView":Landroid/view/View;
    .end local v1    # "oldIndex":I
    .end local v2    # "oldView":Landroid/view/View;
    .end local v3    # "option":Ljava/lang/String;
    .end local v4    # "targetValue":F
    :cond_4
    return-void

    .line 145
    :cond_5
    add-int/lit8 v5, p1, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public abstract setValue(F)V
.end method

.method public valueForIndex(I)F
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v0

    .line 183
    .local v0, "itemCount":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    const/4 v1, 0x0

    .line 185
    :goto_0
    return v1

    :cond_0
    int-to-float v1, p1

    add-int/lit8 v2, v0, -0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_0
.end method
