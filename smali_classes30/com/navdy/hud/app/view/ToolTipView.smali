.class public Lcom/navdy/hud/app/view/ToolTipView;
.super Landroid/widget/LinearLayout;
.source "ToolTipView.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private iconWidth:I

.field private margin:I

.field private maxIcons:I

.field private maxTextWidth:I

.field private minTextWidth:I

.field private padding:I

.field private sideMargin:I

.field toolTipContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0221
    .end annotation
.end field

.field toolTipScrim:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0223
    .end annotation
.end field

.field toolTipTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0222
    .end annotation
.end field

.field toolTipTriangle:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0224
    .end annotation
.end field

.field private totalWidth:I

.field private triangleWidth:I

.field private widthCalcTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/ToolTipView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/ToolTipView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/ToolTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/ToolTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ToolTipView;->init()V

    .line 63
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 66
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ToolTipView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->widthCalcTextView:Landroid/widget/TextView;

    .line 68
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->widthCalcTextView:Landroid/widget/TextView;

    const v3, 0x7f0c002f

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 69
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 70
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b0193

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->maxTextWidth:I

    .line 71
    const v2, 0x7f0b0194

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->minTextWidth:I

    .line 72
    const v2, 0x7f0b0195

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->padding:I

    .line 73
    const v2, 0x7f0b0192

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->sideMargin:I

    .line 74
    const v2, 0x7f0b0196

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/ToolTipView;->triangleWidth:I

    .line 75
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/view/ToolTipView;->setOrientation(I)V

    .line 76
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030082

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 77
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 78
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 165
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ToolTipView;->setVisibility(I)V

    .line 166
    return-void
.end method

.method public setParams(III)V
    .locals 2
    .param p1, "iconWidth"    # I
    .param p2, "margin"    # I
    .param p3, "maxIcons"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/navdy/hud/app/view/ToolTipView;->iconWidth:I

    .line 82
    iput p2, p0, Lcom/navdy/hud/app/view/ToolTipView;->margin:I

    .line 83
    iput p3, p0, Lcom/navdy/hud/app/view/ToolTipView;->maxIcons:I

    .line 84
    mul-int v0, p1, p3

    add-int/lit8 v1, p3, -0x1

    mul-int/2addr v1, p2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/ToolTipView;->totalWidth:I

    .line 85
    return-void
.end method

.method public show(ILjava/lang/String;)V
    .locals 18
    .param p1, "posIndex"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->widthCalcTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 90
    .local v3, "textPaint":Landroid/text/TextPaint;
    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/ToolTipView;->maxTextWidth:I

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 92
    .local v1, "layout":Landroid/text/StaticLayout;
    const/4 v12, 0x0

    .line 93
    .local v12, "scrimOn":Z
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 95
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 96
    .local v16, "width":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->minTextWidth:I

    move/from16 v0, v16

    if-ge v0, v2, :cond_0

    .line 97
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/view/ToolTipView;->minTextWidth:I

    .line 108
    .end local v16    # "width":I
    .local v13, "textWidth":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/ToolTipView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 109
    .local v11, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->padding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v13, v2

    .line 110
    iput v13, v11, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 113
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->iconWidth:I

    mul-int v2, v2, p1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/ToolTipView;->margin:I

    mul-int v4, v4, p1

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/ToolTipView;->iconWidth:I

    div-int/lit8 v4, v4, 0x2

    add-int v14, v2, v4

    .line 114
    .local v14, "triangleMiddlePos":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->triangleWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int v15, v14, v2

    .line 118
    .local v15, "triangleStartPos":I
    packed-switch p1, :pswitch_data_0

    .line 141
    const/16 v17, 0x0

    .line 145
    .local v17, "x":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTriangle:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .end local v11    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 146
    .restart local v11    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v2, v15, v17

    iput v2, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTextView:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->requestLayout()V

    .line 153
    if-eqz v12, :cond_4

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipScrim:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :goto_2
    move/from16 v0, v17

    int-to-float v2, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ToolTipView;->setX(F)V

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/ToolTipView;->requestLayout()V

    .line 161
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/view/ToolTipView;->setVisibility(I)V

    .line 162
    return-void

    .line 99
    .end local v11    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v13    # "textWidth":I
    .end local v14    # "triangleMiddlePos":I
    .end local v15    # "triangleStartPos":I
    .end local v17    # "x":I
    .restart local v16    # "width":I
    :cond_0
    move/from16 v13, v16

    .restart local v13    # "textWidth":I
    goto :goto_0

    .line 103
    .end local v13    # "textWidth":I
    .end local v16    # "width":I
    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/view/ToolTipView;->maxTextWidth:I

    .line 104
    .restart local v13    # "textWidth":I
    const/4 v12, 0x1

    goto :goto_0

    .line 121
    .restart local v11    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v14    # "triangleMiddlePos":I
    .restart local v15    # "triangleStartPos":I
    :pswitch_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->sideMargin:I

    sub-int v9, v14, v2

    .line 122
    .local v9, "distanceLeft":I
    div-int/lit8 v2, v13, 0x2

    if-le v2, v9, :cond_2

    .line 123
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/view/ToolTipView;->sideMargin:I

    move/from16 v17, v0

    .restart local v17    # "x":I
    goto :goto_1

    .line 125
    .end local v17    # "x":I
    :cond_2
    div-int/lit8 v2, v13, 0x2

    sub-int v17, v14, v2

    .line 127
    .restart local v17    # "x":I
    goto :goto_1

    .line 132
    .end local v9    # "distanceLeft":I
    .end local v17    # "x":I
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->totalWidth:I

    sub-int/2addr v2, v14

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/ToolTipView;->sideMargin:I

    sub-int v10, v2, v4

    .line 133
    .local v10, "distanceRight":I
    div-int/lit8 v2, v13, 0x2

    if-le v2, v10, :cond_3

    .line 134
    sub-int v2, v13, v10

    sub-int v17, v14, v2

    .restart local v17    # "x":I
    goto :goto_1

    .line 136
    .end local v17    # "x":I
    :cond_3
    div-int/lit8 v2, v13, 0x2

    sub-int v17, v14, v2

    .line 138
    .restart local v17    # "x":I
    goto :goto_1

    .line 156
    .end local v10    # "distanceRight":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipScrim:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
