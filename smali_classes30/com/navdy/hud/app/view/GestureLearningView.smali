.class public Lcom/navdy/hud/app/view/GestureLearningView;
.super Landroid/widget/RelativeLayout;
.source "GestureLearningView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field public static final MOVE_IN_DURATION:I = 0x258

.field public static final MOVE_OUT_DURATION:I = 0xfa

.field private static final PROGRESS_INDICATOR_HIDE_INTERVAL:I = 0x64

.field private static final SCROLL_DURATION:I = 0x3e8

.field private static final SCROLL_INTERVAL:I = 0xfa0

.field private static final SENSOR_BLOCKED_MESSAGE_DELAY:I = 0x7530

.field private static final SMOOTHING_COEFF_KEY:Ljava/lang/String; = "persist.sys.gesture_smooth"

.field public static final TAG_CAPTURE:I = 0x2

.field public static final TAG_DONE:I = 0x0

.field public static final TAG_TIPS:I = 0x1


# instance fields
.field private TIPS:[Ljava/lang/String;

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mCenterImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00fc
    .end annotation
.end field

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field private mCurrentSmallTipIndex:I

.field private mGestureIndicatorHalfWidth:F

.field private mGestureIndicatorProgressSpan:F

.field mGestureProgressIndicator:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f7
    .end annotation
.end field

.field private mHandIconMargin:F

.field private mHandler:Landroid/os/Handler;

.field private mLeftGestureAnimation:Landroid/animation/AnimatorSet;

.field mLeftSwipeLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00fd
    .end annotation
.end field

.field private final mLowPassCoeff:F

.field private mNeutralX:F

.field mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mProgressIndicatorRunnable:Ljava/lang/Runnable;

.field private mRightGestureAnimation:Landroid/animation/AnimatorSet;

.field mRightSwipeLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0101
    .end annotation
.end field

.field private mScrollAnimator:Landroid/animation/ValueAnimator;

.field private mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

.field private mSmoothedProgress:F

.field private mStopAnimation:Z

.field mTipsScroller:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f9
    .end annotation
.end field

.field mTipsScrollerHeight:I

.field private mTipsScrollerRunnable:Ljava/lang/Runnable;

.field mTipsTextView1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00fa
    .end annotation
.end field

.field mTipsTextView2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00fb
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/GestureLearningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/GestureLearningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v0, 0x0

    .line 120
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 93
    iput v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I

    .line 101
    iput-boolean v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mStopAnimation:Z

    .line 106
    const-string v0, "persist.sys.gesture_smooth"

    const-string v1, "1.0"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLowPassCoeff:F

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mSmoothedProgress:F

    .line 121
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 123
    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/view/GestureLearningView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mNeutralX:F

    return p1
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/view/GestureLearningView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorProgressSpan:F

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/GestureLearningView;)F
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/GestureLearningView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorHalfWidth:F

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/view/GestureLearningView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/view/GestureLearningView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/view/GestureLearningView;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/view/GestureLearningView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mStopAnimation:Z

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/view/GestureLearningView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/view/GestureLearningView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/view/GestureLearningView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method private prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;
    .locals 10
    .param p1, "rightSide"    # Z
    .param p2, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 294
    if-eqz p1, :cond_0

    .line 295
    const v7, 0x7f0e0104

    invoke-virtual {p2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 296
    .local v2, "directionText":Landroid/widget/TextView;
    const v7, 0x7f0e0103

    invoke-virtual {p2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 301
    .local v6, "overLay":Landroid/widget/ImageView;
    :goto_0
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 302
    .local v1, "animatorSet":Landroid/animation/AnimatorSet;
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    .line 303
    .local v4, "moveOutAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v8, 0xfa

    invoke-virtual {v4, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 304
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0094

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 305
    .local v3, "marginDistance":I
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    iget v9, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    float-to-int v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    float-to-int v9, v9

    add-int/2addr v9, v3

    aput v9, v7, v8

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 306
    new-instance v7, Lcom/navdy/hud/app/view/GestureLearningView$7;

    invoke-direct {v7, p0, p2, p1}, Lcom/navdy/hud/app/view/GestureLearningView$7;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/view/ViewGroup;Z)V

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 320
    new-instance v5, Landroid/animation/ValueAnimator;

    invoke-direct {v5}, Landroid/animation/ValueAnimator;-><init>()V

    .line 321
    .local v5, "moveRightAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v8, 0x258

    invoke-virtual {v5, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 322
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    iget v9, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    float-to-int v9, v9

    add-int/2addr v9, v3

    aput v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    float-to-int v9, v9

    aput v9, v7, v8

    invoke-virtual {v5, v7}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 323
    new-instance v7, Lcom/navdy/hud/app/view/GestureLearningView$8;

    invoke-direct {v7, p0, p2, p1}, Lcom/navdy/hud/app/view/GestureLearningView$8;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/view/ViewGroup;Z)V

    invoke-virtual {v5, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 337
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 338
    .local v0, "alphaAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v8, 0x258

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 339
    const/4 v7, 0x2

    new-array v7, v7, [F

    fill-array-data v7, :array_0

    invoke-virtual {v0, v7}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 340
    new-instance v7, Lcom/navdy/hud/app/view/GestureLearningView$9;

    invoke-direct {v7, p0, v2, v6}, Lcom/navdy/hud/app/view/GestureLearningView$9;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 347
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 348
    new-instance v7, Lcom/navdy/hud/app/view/GestureLearningView$10;

    invoke-direct {v7, p0, v6, v2}, Lcom/navdy/hud/app/view/GestureLearningView$10;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    invoke-virtual {v1, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 371
    return-object v1

    .line 298
    .end local v0    # "alphaAnimator":Landroid/animation/ValueAnimator;
    .end local v1    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v2    # "directionText":Landroid/widget/TextView;
    .end local v3    # "marginDistance":I
    .end local v4    # "moveOutAnimator":Landroid/animation/ValueAnimator;
    .end local v5    # "moveRightAnimator":Landroid/animation/ValueAnimator;
    .end local v6    # "overLay":Landroid/widget/ImageView;
    :cond_0
    const v7, 0x7f0e00fe

    invoke-virtual {p2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 299
    .restart local v2    # "directionText":Landroid/widget/TextView;
    const v7, 0x7f0e0100

    invoke-virtual {p2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .restart local v6    # "overLay":Landroid/widget/ImageView;
    goto/16 :goto_0

    .line 339
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public executeItem(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 273
    packed-switch p2, :pswitch_data_0

    .line 284
    :goto_0
    return-void

    .line 275
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->finish()V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->showTips()V

    goto :goto_0

    .line 281
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->showCaptureView()V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 289
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 228
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mStopAnimation:Z

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 231
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 232
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 234
    return-void
.end method

.method protected onFinishInflate()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 127
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 128
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 129
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v4, v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setHighlightPersistent(Z)V

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 131
    .local v1, "resources":Landroid/content/res/Resources;
    const v4, 0x7f0b0098

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerHeight:I

    .line 132
    const v4, 0x7f0b0095

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    iput v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F

    .line 133
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLeftSwipeLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v8, v4}, Lcom/navdy/hud/app/view/GestureLearningView;->prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLeftGestureAnimation:Landroid/animation/AnimatorSet;

    .line 134
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mRightSwipeLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v9, v4}, Lcom/navdy/hud/app/view/GestureLearningView;->prepareGestureAnimation(ZLandroid/view/ViewGroup;)Landroid/animation/AnimatorSet;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mRightGestureAnimation:Landroid/animation/AnimatorSet;

    .line 135
    const v4, 0x7f0a0001

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;

    .line 136
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    .line 137
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    .line 138
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    const/4 v5, 0x2

    new-array v5, v5, [I

    aput v8, v5, v8

    iget v6, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerHeight:I

    neg-int v6, v6

    aput v6, v5, v9

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 139
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 140
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/view/GestureLearningView$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/view/GestureLearningView$1;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 151
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/navdy/hud/app/view/GestureLearningView$2;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/view/GestureLearningView$2;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 159
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mScrollAnimator:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/navdy/hud/app/view/GestureLearningView$3;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/view/GestureLearningView$3;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 190
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;

    iget v5, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I

    aget-object v2, v4, v5

    .line 191
    .local v2, "tipText1":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;

    iget v5, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;

    array-length v6, v6

    rem-int/2addr v5, v6

    aget-object v3, v4, v5

    .line 192
    .local v3, "tipText2":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView1:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView2:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    new-instance v4, Lcom/navdy/hud/app/view/GestureLearningView$4;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/view/GestureLearningView$4;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerRunnable:Ljava/lang/Runnable;

    .line 200
    new-instance v4, Lcom/navdy/hud/app/view/GestureLearningView$5;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/view/GestureLearningView$5;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mProgressIndicatorRunnable:Ljava/lang/Runnable;

    .line 206
    new-instance v4, Lcom/navdy/hud/app/view/GestureLearningView$6;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/view/GestureLearningView$6;-><init>(Lcom/navdy/hud/app/view/GestureLearningView;)V

    iput-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    .line 212
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0xfa0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0900df

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v8}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f09028f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v4, v5, v0, v8, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 220
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/GestureLearningView;->isInEditMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 221
    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v4, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 223
    :cond_0
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 239
    sget-object v0, Lcom/navdy/hud/app/view/GestureLearningView$11;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 241
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLeftGestureAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 244
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mRightGestureAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onGestureProgress(Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;)V
    .locals 6
    .param p1, "gestureProgress"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 388
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mProgressIndicatorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 389
    iget v1, p1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;->progress:F

    .line 390
    .local v1, "progress":F
    iget-object v2, p1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;->direction:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    sget-object v3, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->LEFT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    if-ne v2, v3, :cond_0

    .line 391
    const/high16 v2, -0x40800000    # -1.0f

    mul-float/2addr v1, v2

    .line 393
    :cond_0
    iget v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLowPassCoeff:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mSmoothedProgress:F

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLowPassCoeff:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float v0, v2, v3

    .line 394
    .local v0, "newSmoothedProgress":F
    iget v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mSmoothedProgress:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 395
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 396
    iput v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mSmoothedProgress:F

    .line 397
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    iget v3, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mNeutralX:F

    iget v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorProgressSpan:F

    iget v5, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mSmoothedProgress:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorHalfWidth:F

    sub-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    .line 399
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mProgressIndicatorRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 400
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 252
    sget-object v0, Lcom/navdy/hud/app/view/GestureLearningView$11;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 254
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 257
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 260
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 377
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 378
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    if-nez p1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 382
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mShowSensorBlockedMessageRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
