.class public Lcom/navdy/hud/app/view/MainView$$ViewInjector;
.super Ljava/lang/Object;
.source "MainView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/MainView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/MainView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e0132

    const-string v2, "field \'containerView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/ContainerView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    .line 12
    const v1, 0x7f0e013a

    const-string v2, "field \'notificationView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/NotificationView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    .line 14
    const v1, 0x7f0e013b

    const-string v2, "field \'notifIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 16
    const v1, 0x7f0e013c

    const-string v2, "field \'notifScrollIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .line 18
    const v1, 0x7f0e0137

    const-string v2, "field \'expandedNotificationView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/FrameLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->expandedNotificationView:Landroid/widget/FrameLayout;

    .line 20
    const v1, 0x7f0e0138

    const-string v2, "field \'notificationExtensionView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/FrameLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->notificationExtensionView:Landroid/widget/FrameLayout;

    .line 22
    const v1, 0x7f0e0139

    const-string v2, "field \'mainLowerView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    .line 24
    const v1, 0x7f0e0136

    const-string v2, "field \'expandedNotificationCoverView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->expandedNotificationCoverView:Landroid/view/View;

    .line 26
    const v1, 0x7f0e0131

    const-string v2, "field \'splitterView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    check-cast v0, Landroid/widget/FrameLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    .line 28
    const v1, 0x7f0e0134

    const-string v2, "field \'notificationColorView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .line 30
    const v1, 0x7f0e0133

    const-string v2, "field \'screenContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/FrameLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->screenContainer:Landroid/widget/FrameLayout;

    .line 32
    const v1, 0x7f0e0135

    const-string v2, "field \'systemTray\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->systemTray:Lcom/navdy/hud/app/ui/component/SystemTrayView;

    .line 34
    const v1, 0x7f0e013d

    const-string v2, "field \'toastView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/ToastView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/MainView;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 36
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/MainView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    .line 40
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    .line 41
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 42
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .line 43
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->expandedNotificationView:Landroid/widget/FrameLayout;

    .line 44
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationExtensionView:Landroid/widget/FrameLayout;

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    .line 46
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->expandedNotificationCoverView:Landroid/view/View;

    .line 47
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    .line 48
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .line 49
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->screenContainer:Landroid/widget/FrameLayout;

    .line 50
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->systemTray:Lcom/navdy/hud/app/ui/component/SystemTrayView;

    .line 51
    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 52
    return-void
.end method
