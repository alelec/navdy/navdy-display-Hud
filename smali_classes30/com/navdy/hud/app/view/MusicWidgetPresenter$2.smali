.class Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;
.super Ljava/lang/Object;
.source "MusicWidgetPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MusicWidgetPresenter;->onAlbumArtUpdate(Lokio/ByteString;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

.field final synthetic val$photo:Lokio/ByteString;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MusicWidgetPresenter;Lokio/ByteString;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    iput-object p2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->val$photo:Lokio/ByteString;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 165
    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PhotoUpdate runnable "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->val$photo:Lokio/ByteString;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 166
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->val$photo:Lokio/ByteString;

    invoke-virtual {v4}, Lokio/ByteString;->toByteArray()[B

    move-result-object v1

    .line 168
    .local v1, "byteArray":[B
    if-eqz v1, :cond_0

    array-length v4, v1

    if-gtz v4, :cond_2

    .line 169
    :cond_0
    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Received photo has null or empty byte array"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 170
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->resetArtwork()V
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$100(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V

    .line 171
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->redrawOnMainThread()V
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$200(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V

    .line 192
    :cond_1
    :goto_0
    return-void

    .line 175
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00e0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 177
    .local v3, "size":I
    const/4 v0, 0x0

    .line 180
    .local v0, "artwork":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v4, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {v1, v3, v3, v4}, Lcom/navdy/service/library/util/ScalingUtilities;->decodeByteArray([BIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 181
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    sget-object v5, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {v0, v3, v3, v5}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmap(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v5

    # setter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$302(Lcom/navdy/hud/app/view/MusicWidgetPresenter;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 182
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->redrawOnMainThread()V
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$200(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$300(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eq v0, v4, :cond_1

    .line 189
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 183
    :catch_0
    move-exception v2

    .line 184
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Error updating the art work received "

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->resetArtwork()V
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$100(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V

    .line 186
    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->redrawOnMainThread()V
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$200(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$300(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eq v0, v4, :cond_1

    .line 189
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 188
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;->this$0:Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    # getter for: Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->access$300(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eq v0, v5, :cond_3

    .line 189
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v4
.end method
