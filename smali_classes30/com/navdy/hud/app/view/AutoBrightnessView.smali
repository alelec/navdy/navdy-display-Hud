.class public Lcom/navdy/hud/app/view/AutoBrightnessView;
.super Landroid/widget/FrameLayout;
.source "AutoBrightnessView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# instance fields
.field autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field autoBrightnessIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cf
    .end annotation
.end field

.field autoBrightnessTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field brightnessTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0089
    .end annotation
.end field

.field brightnessTitleDesc:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e008a
    .end annotation
.end field

.field infoContainer:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00bf
    .end annotation
.end field

.field presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field text:Ljava/lang/String;

.field title1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0088
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/AutoBrightnessView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/AutoBrightnessView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/AutoBrightnessView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method public executeSelectedItem()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    .line 172
    return-void
.end method

.method public moveSelectionLeft()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    .line 164
    return-void
.end method

.method public moveSelectionRight()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    .line 168
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 137
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 116
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 123
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 81
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 82
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 83
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/AutoBrightnessView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 84
    .local v2, "resources":Landroid/content/res/Resources;
    const v5, 0x7f0b0029

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v4, v5

    .line 85
    .local v4, "width":I
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->infoContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 86
    .local v1, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 87
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->infoContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->setMinimumWidth(I)V

    .line 89
    const v5, 0x7f0b0023

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 90
    .local v0, "marginBottom":I
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 91
    .restart local v1    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 93
    const-string v5, "sans-serif-medium"

    invoke-static {v5, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 94
    .local v3, "typeface":Landroid/graphics/Typeface;
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 96
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->title1:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    const-string v5, "sans-serif-light"

    invoke-static {v5, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 98
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 100
    const-string v5, "sans-serif"

    invoke-static {v5, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 101
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 103
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->brightnessTitleDesc:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 104
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->brightnessTitleDesc:Landroid/widget/TextView;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 105
    iget-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->brightnessTitleDesc:Landroid/widget/TextView;

    const v6, 0x7f090024

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const v5, 0x7f090029

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->text:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->presenter:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setChoices(Ljava/util/List;Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V
    .locals 3
    .param p2, "choicesListener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "choicesList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessChoices:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 160
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    return-void
.end method

.method public setStatusLabel(Ljava/lang/String;)V
    .locals 6
    .param p1, "statusLabel"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x21

    .line 145
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 146
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    iget-object v2, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->text:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 147
    new-instance v2, Landroid/text/style/TypefaceSpan;

    const-string v3, "sans-serif-light"

    invoke-direct {v2, v3}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 148
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 149
    .local v1, "start":I
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 150
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 151
    iget-object v2, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->brightnessTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/view/AutoBrightnessView;->autoBrightnessTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    return-void
.end method
