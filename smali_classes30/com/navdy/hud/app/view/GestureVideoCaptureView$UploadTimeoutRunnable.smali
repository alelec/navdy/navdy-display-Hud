.class Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;
.super Ljava/lang/Object;
.source "GestureVideoCaptureView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/GestureVideoCaptureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadTimeoutRunnable"
.end annotation


# instance fields
.field private final sessionName:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Ljava/lang/String;)V
    .locals 0
    .param p2, "sessionName"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;->this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;->sessionName:Ljava/lang/String;

    .line 139
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Upload has timed out."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;->sessionName:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;->this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    # getter for: Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sessionName:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->access$000(Lcom/navdy/hud/app/view/GestureVideoCaptureView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$UploadTimeoutRunnable;->this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    sget-object v1, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING_FAILURE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    # invokes: Lcom/navdy/hud/app/view/GestureVideoCaptureView;->setState(Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->access$100(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;)V

    goto :goto_0
.end method
