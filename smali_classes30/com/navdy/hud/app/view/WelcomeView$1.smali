.class Lcom/navdy/hud/app/view/WelcomeView$1;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/WelcomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/WelcomeView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    .line 144
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadIndex:I
    invoke-static {v9}, Lcom/navdy/hud/app/view/WelcomeView;->access$000(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v9

    const/4 v10, -0x1

    if-ne v9, v10, :cond_0

    .line 198
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v3, 0x0

    .local v3, "current":Landroid/widget/TextView;
    const/4 v4, 0x0

    .line 148
    .local v4, "next":Landroid/widget/TextView;
    const/4 v0, 0x0

    .local v0, "appStoreCurrent":Landroid/widget/ImageView;
    const/4 v1, 0x0

    .line 149
    .local v1, "appStoreNext":Landroid/widget/ImageView;
    const/4 v5, 0x0

    .local v5, "playStoreCurrent":Landroid/widget/ImageView;
    const/4 v6, 0x0

    .line 150
    .local v6, "playStoreNext":Landroid/widget/ImageView;
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I
    invoke-static {v9}, Lcom/navdy/hud/app/view/WelcomeView;->access$100(Lcom/navdy/hud/app/view/WelcomeView;)I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 173
    :goto_1
    move-object v8, v3

    .line 174
    .local v8, "txtView":Landroid/widget/TextView;
    move-object v2, v0

    .line 175
    .local v2, "appView":Landroid/widget/ImageView;
    move-object v7, v5

    .line 177
    .local v7, "playView":Landroid/widget/ImageView;
    invoke-virtual {v3}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 178
    invoke-virtual {v4}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    new-instance v10, Lcom/navdy/hud/app/view/WelcomeView$1$1;

    invoke-direct {v10, p0, v8, v2, v7}, Lcom/navdy/hud/app/view/WelcomeView$1$1;-><init>(Lcom/navdy/hud/app/view/WelcomeView$1;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v9, v10}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 193
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 194
    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 196
    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 197
    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    # getter for: Lcom/navdy/hud/app/view/WelcomeView;->DOWNLOAD_APP_TRANSITION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/view/WelcomeView;->access$200()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 152
    .end local v2    # "appView":Landroid/widget/ImageView;
    .end local v7    # "playView":Landroid/widget/ImageView;
    .end local v8    # "txtView":Landroid/widget/TextView;
    :pswitch_0
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v3, v9, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    .line 153
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v4, v9, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    .line 154
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v0, v9, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    .line 155
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v9, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    .line 156
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v5, v9, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    .line 157
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v6, v9, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    .line 158
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    const/4 v10, 0x1

    # setter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I
    invoke-static {v9, v10}, Lcom/navdy/hud/app/view/WelcomeView;->access$102(Lcom/navdy/hud/app/view/WelcomeView;I)I

    goto/16 :goto_1

    .line 162
    :pswitch_1
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v3, v9, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView2:Landroid/widget/TextView;

    .line 163
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v4, v9, Lcom/navdy/hud/app/view/WelcomeView;->downloadAppTextView1:Landroid/widget/TextView;

    .line 164
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v0, v9, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage2:Landroid/widget/ImageView;

    .line 165
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, v9, Lcom/navdy/hud/app/view/WelcomeView;->appStoreImage1:Landroid/widget/ImageView;

    .line 166
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v5, v9, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage2:Landroid/widget/ImageView;

    .line 167
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v6, v9, Lcom/navdy/hud/app/view/WelcomeView;->playStoreImage1:Landroid/widget/ImageView;

    .line 168
    iget-object v9, p0, Lcom/navdy/hud/app/view/WelcomeView$1;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    const/4 v10, 0x0

    # setter for: Lcom/navdy/hud/app/view/WelcomeView;->currentDownloadView:I
    invoke-static {v9, v10}, Lcom/navdy/hud/app/view/WelcomeView;->access$102(Lcom/navdy/hud/app/view/WelcomeView;I)I

    goto/16 :goto_1

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
