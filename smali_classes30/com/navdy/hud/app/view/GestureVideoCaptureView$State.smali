.class final enum Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
.super Ljava/lang/Enum;
.source "GestureVideoCaptureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/GestureVideoCaptureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum ALL_DONE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum DOUBLE_TAP_1:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum DOUBLE_TAP_2:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum NOT_PAIRED:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum SAVING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum SWIPE_LEFT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum SWIPE_RIGHT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum UPLOADING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

.field public static final enum UPLOADING_FAILURE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;


# instance fields
.field detailText:I

.field filename:Ljava/lang/String;

.field mainText:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 110
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "NOT_PAIRED"

    const/4 v2, 0x0

    const v3, 0x7f090207

    const v4, 0x7f090208

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->NOT_PAIRED:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 111
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "SWIPE_LEFT"

    const/4 v2, 0x1

    const v3, 0x7f090285

    const/4 v4, 0x0

    const-string v5, "swipe-left"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_LEFT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 112
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "SWIPE_RIGHT"

    const/4 v2, 0x2

    const v3, 0x7f090286

    const/4 v4, 0x0

    const-string v5, "swipe-right"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_RIGHT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 113
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "DOUBLE_TAP_1"

    const/4 v2, 0x3

    const v3, 0x7f0900e3

    const v4, 0x7f0900e5

    const-string v5, "tap"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_1:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 114
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "DOUBLE_TAP_2"

    const/4 v2, 0x4

    const v3, 0x7f0900e4

    const v4, 0x7f0900e6

    const-string v5, "tap2"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_2:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 115
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "SAVING"

    const/4 v2, 0x5

    const/4 v3, 0x0

    const v4, 0x7f090250

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SAVING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 116
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "UPLOADING"

    const/4 v2, 0x6

    const v3, 0x7f0902e5

    const v4, 0x7f0902e6

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 117
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "ALL_DONE"

    const/4 v2, 0x7

    const v3, 0x7f0902e7

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ALL_DONE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 118
    new-instance v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const-string v1, "UPLOADING_FAILURE"

    const/16 v2, 0x8

    const v3, 0x7f0902e3

    const v4, 0x7f0902e4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING_FAILURE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    .line 109
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->NOT_PAIRED:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_LEFT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SWIPE_RIGHT:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_1:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->DOUBLE_TAP_2:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->SAVING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->ALL_DONE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->UPLOADING_FAILURE:Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->$VALUES:[Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .param p3, "mainText"    # I
    .param p4, "detailText"    # I
    .param p5, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput p3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->mainText:I

    .line 122
    iput p4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->detailText:I

    .line 123
    iput-object p5, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->filename:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->$VALUES:[Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/view/GestureVideoCaptureView$State;

    return-object v0
.end method
