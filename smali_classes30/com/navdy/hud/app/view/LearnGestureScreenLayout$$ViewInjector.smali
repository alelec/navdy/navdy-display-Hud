.class public Lcom/navdy/hud/app/view/LearnGestureScreenLayout$$ViewInjector;
.super Ljava/lang/Object;
.source "LearnGestureScreenLayout$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/LearnGestureScreenLayout;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e01ca

    const-string v2, "field \'mGestureLearningView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/GestureLearningView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    .line 12
    const v1, 0x7f0e01cb

    const-string v2, "field \'mScrollableTextPresenter\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    .line 14
    const v1, 0x7f0e01cc

    const-string v2, "field \'mCameraBlockedMessage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    .line 16
    const v1, 0x7f0e01cd

    const-string v2, "field \'mVideoCaptureInstructionsLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .line 18
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/LearnGestureScreenLayout;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mGestureLearningView:Lcom/navdy/hud/app/view/GestureLearningView;

    .line 22
    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mScrollableTextPresenter:Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    .line 23
    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mCameraBlockedMessage:Landroid/view/ViewGroup;

    .line 24
    iput-object v0, p0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->mVideoCaptureInstructionsLayout:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .line 25
    return-void
.end method
