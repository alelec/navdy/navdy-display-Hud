.class public Lcom/navdy/hud/app/view/ToolTipView$$ViewInjector;
.super Ljava/lang/Object;
.source "ToolTipView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/ToolTipView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/ToolTipView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e0221

    const-string v2, "field \'toolTipContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ToolTipView;->toolTipContainer:Landroid/view/ViewGroup;

    .line 12
    const v1, 0x7f0e0222

    const-string v2, "field \'toolTipTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTextView:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e0223

    const-string v2, "field \'toolTipScrim\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ToolTipView;->toolTipScrim:Landroid/view/View;

    .line 16
    const v1, 0x7f0e0224

    const-string v2, "field \'toolTipTriangle\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    iput-object v0, p1, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTriangle:Landroid/view/View;

    .line 18
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/ToolTipView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/ToolTipView;

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipContainer:Landroid/view/ViewGroup;

    .line 22
    iput-object v0, p0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTextView:Landroid/widget/TextView;

    .line 23
    iput-object v0, p0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipScrim:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/navdy/hud/app/view/ToolTipView;->toolTipTriangle:Landroid/view/View;

    .line 25
    return-void
.end method
