.class public Lcom/navdy/hud/app/view/ToastView;
.super Landroid/widget/FrameLayout;
.source "ToastView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field public gestureOn:Z

.field inputManager:Lcom/navdy/hud/app/manager/InputManager;

.field private mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

.field mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00bb
    .end annotation
.end field

.field private volatile sendShowEvent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/ToastView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/ToastView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/ToastView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 60
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInputManager()Lcom/navdy/hud/app/manager/InputManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ToastView;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    .line 61
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;

    .line 62
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/ToastView;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/view/ToastView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ToastView;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/ToastView;->sendShowEvent:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/ToastView;)Lcom/navdy/hud/app/ui/activity/Main;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/ui/activity/Main;)Lcom/navdy/hud/app/ui/activity/Main;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ToastView;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/activity/Main;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    return-object p1
.end method


# virtual methods
.method public animateIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "screenName"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "noStartDelay"    # Z

    .prologue
    const/4 v2, 0x0

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ToastView;->sendShowEvent:Z

    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    sget v1, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->ANIMATION_TRANSLATION:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setTranslationY(F)V

    .line 154
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setAlpha(F)V

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 156
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 157
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz p4, :cond_0

    const-wide/16 v0, 0x0

    .line 158
    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 159
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/ToastView$2;

    invoke-direct {v1, p0, p2}, Lcom/navdy/hud/app/view/ToastView$2;-><init>(Lcom/navdy/hud/app/view/ToastView;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/ToastView$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/navdy/hud/app/view/ToastView$1;-><init>(Lcom/navdy/hud/app/view/ToastView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 191
    return-void

    .line 157
    :cond_0
    const-wide/16 v0, 0x64

    goto :goto_0
.end method

.method public animateOut(Z)V
    .locals 4
    .param p1, "quickly"    # Z

    .prologue
    .line 195
    if-eqz p1, :cond_1

    .line 196
    const/16 v0, 0x32

    .line 201
    .local v0, "duration":I
    :goto_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/ToastView;->sendShowEvent:Z

    if-nez v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;

    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->getCurrentId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 203
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/ToastView;->sendShowEvent:Z

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 207
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->ANIMATION_TRANSLATION:I

    int-to-float v2, v2

    .line 208
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 209
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/view/ToastView$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ToastView$4;-><init>(Lcom/navdy/hud/app/view/ToastView;)V

    .line 210
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/view/ToastView$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ToastView$3;-><init>(Lcom/navdy/hud/app/view/ToastView;)V

    .line 225
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 244
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 245
    return-void

    .line 198
    .end local v0    # "duration":I
    :cond_1
    const/16 v0, 0x64

    .restart local v0    # "duration":I
    goto :goto_0
.end method

.method public dismissToast()V
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ToastView;->animateOut(Z)V

    .line 149
    :cond_0
    return-void
.end method

.method public getConfirmation()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    return-object v0
.end method

.method public getMainLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    return-object v0
.end method

.method public getView()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    return-object v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 67
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 68
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 6
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/ToastView;->gestureOn:Z

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_3

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->getCurrentCallback()Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-result-object v0

    .line 78
    .local v0, "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    if-nez v0, :cond_0

    move v2, v3

    .line 101
    .end local v0    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    :goto_0
    return v2

    .line 82
    .restart local v0    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getChoices()Ljava/util/List;

    move-result-object v1

    .line 83
    .local v1, "choice":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v2, v3

    .line 84
    goto :goto_0

    .line 87
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/view/ToastView$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v5, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .end local v0    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .end local v1    # "choice":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_3
    move v2, v3

    .line 101
    goto :goto_0

    .line 89
    .restart local v0    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .restart local v1    # "choice":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :pswitch_0
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-interface {v0, v3, v2}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->executeChoiceItem(II)V

    move v2, v4

    .line 90
    goto :goto_0

    .line 93
    :pswitch_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 94
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-interface {v0, v3, v2}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->executeChoiceItem(II)V

    :goto_1
    move v2, v4

    .line 98
    goto :goto_0

    .line 96
    :cond_4
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-interface {v0, v4, v2}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->executeChoiceItem(II)V

    goto :goto_1

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 106
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->getCurrentCallback()Lcom/navdy/hud/app/framework/toast/IToastCallback;

    move-result-object v0

    .line 107
    .local v0, "cb":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/framework/toast/IToastCallback;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    :goto_0
    return v1

    .line 111
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/view/ToastView$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 113
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->hasTimeout()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    goto :goto_0

    .line 124
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 130
    :pswitch_3
    const/4 v1, 0x0

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public revertToOriginal()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView;->mainView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->fluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ToastView;->gestureOn:Z

    .line 250
    return-void
.end method
