.class public Lcom/navdy/hud/app/view/FuelGaugePresenter2$$ViewInjector;
.super Ljava/lang/Object;
.source "FuelGaugePresenter2$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/FuelGaugePresenter2;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/FuelGaugePresenter2;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00f2

    const-string v2, "field \'lowFuelIndicatorLeft\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorLeft:Landroid/widget/ImageView;

    .line 12
    const v1, 0x7f0e00f6

    const-string v2, "field \'lowFuelIndicatorRight\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorRight:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f0e00f3

    const-string v2, "field \'fuelTankIndicatorLeft\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorLeft:Landroid/widget/ImageView;

    .line 16
    const v1, 0x7f0e00f5

    const-string v2, "field \'fuelTankIndicatorRight\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorRight:Landroid/widget/ImageView;

    .line 18
    const v1, 0x7f0e00f4

    const-string v2, "field \'fuelTypeIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTypeIndicator:Landroid/widget/ImageView;

    .line 20
    const v1, 0x7f0e00be

    const-string v2, "field \'rangeText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeText:Landroid/widget/TextView;

    .line 22
    const v1, 0x7f0e00bd

    const-string v2, "field \'rangeUnitText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeUnitText:Landroid/widget/TextView;

    .line 24
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/FuelGaugePresenter2;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/FuelGaugePresenter2;

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorLeft:Landroid/widget/ImageView;

    .line 28
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorRight:Landroid/widget/ImageView;

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorLeft:Landroid/widget/ImageView;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorRight:Landroid/widget/ImageView;

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTypeIndicator:Landroid/widget/ImageView;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeText:Landroid/widget/TextView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeUnitText:Landroid/widget/TextView;

    .line 34
    return-void
.end method
