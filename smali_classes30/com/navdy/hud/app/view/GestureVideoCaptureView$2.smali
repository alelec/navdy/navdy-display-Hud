.class Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;
.super Ljava/lang/Object;
.source "GestureVideoCaptureView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureVideoCaptureView;->onRecordingSaved(Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

.field final synthetic val$recordingSaved:Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;

.field final synthetic val$sessionPath:Ljava/lang/String;

.field final synthetic val$userTag:Ljava/lang/String;

.field final synthetic val$videoArchivePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureVideoCaptureView;Ljava/lang/String;Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$sessionPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$recordingSaved:Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;

    iput-object p4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$videoArchivePath:Ljava/lang/String;

    iput-object p5, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$userTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 380
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$sessionPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 381
    .local v2, "sessionDirectory":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 382
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 384
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$recordingSaved:Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;

    iget-object v3, v3, Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;->path:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 385
    .local v1, "recordedPath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 386
    .local v0, "recordedFiles":[Ljava/io/File;
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->this$0:Lcom/navdy/hud/app/view/GestureVideoCaptureView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$videoArchivePath:Ljava/lang/String;

    invoke-static {v3, v0, v4}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 387
    sget-object v3, Lcom/navdy/hud/app/view/GestureVideoCaptureView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Compressed file : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$videoArchivePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 388
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$videoArchivePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureVideoCaptureView$2;->val$userTag:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->addGestureVideoToUploadQueue(Ljava/io/File;Ljava/lang/String;)V

    .line 389
    invoke-static {}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->syncNow()V

    .line 390
    return-void
.end method
