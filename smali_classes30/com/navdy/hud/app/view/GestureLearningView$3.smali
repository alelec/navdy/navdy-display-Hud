.class Lcom/navdy/hud/app/view/GestureLearningView$3;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 183
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 167
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/GestureLearningView;->access$400(Lcom/navdy/hud/app/view/GestureLearningView;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/view/GestureLearningView;->access$500(Lcom/navdy/hud/app/view/GestureLearningView;)[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    rem-int/2addr v4, v5

    # setter for: Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I
    invoke-static {v3, v4}, Lcom/navdy/hud/app/view/GestureLearningView;->access$402(Lcom/navdy/hud/app/view/GestureLearningView;I)I

    .line 168
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/view/GestureLearningView;->access$500(Lcom/navdy/hud/app/view/GestureLearningView;)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/GestureLearningView;->access$400(Lcom/navdy/hud/app/view/GestureLearningView;)I

    move-result v4

    aget-object v1, v3, v4

    .line 169
    .local v1, "tipText1":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/view/GestureLearningView;->access$500(Lcom/navdy/hud/app/view/GestureLearningView;)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mCurrentSmallTipIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/GestureLearningView;->access$400(Lcom/navdy/hud/app/view/GestureLearningView;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->TIPS:[Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/view/GestureLearningView;->access$500(Lcom/navdy/hud/app/view/GestureLearningView;)[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    rem-int/2addr v4, v5

    aget-object v2, v3, v4

    .line 170
    .local v2, "tipText2":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView1:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 172
    .local v0, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v3, 0x0

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 173
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView2:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mStopAnimation:Z
    invoke-static {v3}, Lcom/navdy/hud/app/view/GestureLearningView;->access$600(Lcom/navdy/hud/app/view/GestureLearningView;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 176
    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/view/GestureLearningView;->access$800(Lcom/navdy/hud/app/view/GestureLearningView;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/view/GestureLearningView$3;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScrollerRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/view/GestureLearningView;->access$700(Lcom/navdy/hud/app/view/GestureLearningView;)Ljava/lang/Runnable;

    move-result-object v4

    const-wide/16 v6, 0xfa0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 178
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 188
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 163
    return-void
.end method
