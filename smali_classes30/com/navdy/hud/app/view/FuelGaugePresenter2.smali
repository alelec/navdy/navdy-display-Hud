.class public Lcom/navdy/hud/app/view/FuelGaugePresenter2;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "FuelGaugePresenter2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;
    }
.end annotation


# static fields
.field public static final LOW_FUEL_WARNING_LIMIT:I = 0x19

.field public static final VERY_LOW_FUEL_WARNING_LIMIT:I = 0xd


# instance fields
.field fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

.field private fuelGaugeName:Ljava/lang/String;

.field fuelTankIndicatorLeft:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f3
    .end annotation
.end field

.field fuelTankIndicatorRight:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f5
    .end annotation
.end field

.field fuelTypeIndicator:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f4
    .end annotation
.end field

.field lowFuelIndicatorLeft:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f2
    .end annotation
.end field

.field lowFuelIndicatorRight:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00f6
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mFuelLevel:I

.field private mFuelRange:I

.field private mFuelRangeUnit:Ljava/lang/String;

.field private mLeftOriented:Z

.field private mTanksSide:Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

.field rangeText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00be
    .end annotation
.end field

.field rangeUnitText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00bd
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 26
    sget-object v0, Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;->UNKNOWN:Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mTanksSide:Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRangeUnit:Ljava/lang/String;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mLeftOriented:Z

    .line 62
    iput-object p1, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mContext:Landroid/content/Context;

    .line 63
    new-instance v0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    const v1, 0x7f0a000e

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->setMinValue(F)V

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->setMaxGaugeValue(F)V

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090315

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeName:Ljava/lang/String;

    .line 67
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string v0, "FUEL_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method public setFuelLevel(I)V
    .locals 0
    .param p1, "mFuelLevel"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelLevel:I

    .line 71
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->reDraw()V

    .line 72
    return-void
.end method

.method public setFuelRange(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRange:I

    .line 80
    return-void
.end method

.method public setFuelRangeUnit(Ljava/lang/String;)V
    .locals 0
    .param p1, "unitText"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRangeUnit:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setFuelTankSide(Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;)V
    .locals 0
    .param p1, "tankSide"    # Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mTanksSide:Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

    .line 76
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 88
    const v1, 0x7f03001c

    .line 89
    .local v1, "layoutResourceId":I
    if-eqz p2, :cond_0

    .line 90
    const-string v2, "EXTRA_GRAVITY"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 91
    .local v0, "gravity":I
    packed-switch v0, :pswitch_data_0

    .line 102
    .end local v0    # "gravity":I
    :cond_0
    :goto_0
    :pswitch_0
    if-eqz p1, :cond_1

    .line 103
    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 104
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 106
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->reDraw()V

    .line 109
    return-void

    .line 93
    .restart local v0    # "gravity":I
    :pswitch_1
    const v1, 0x7f03001c

    .line 94
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mLeftOriented:Z

    goto :goto_0

    .line 97
    :pswitch_2
    const v1, 0x7f03001d

    .line 98
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mLeftOriented:Z

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected updateGauge()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 118
    iget-object v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v4, :cond_2

    .line 119
    iget v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelLevel:I

    const/16 v5, 0x19

    if-gt v4, v5, :cond_3

    move v0, v2

    .line 120
    .local v0, "lowFuel":Z
    :goto_0
    iget v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelLevel:I

    const/16 v5, 0xd

    if-gt v4, v5, :cond_4

    move v1, v2

    .line 121
    .local v1, "veryLowFuel":Z
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    iget v5, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelLevel:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->setGaugeValue(F)V

    .line 122
    iget-object v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    iget-boolean v5, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mLeftOriented:Z

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->setLeftOriented(Z)V

    .line 123
    iget-object v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelGaugeDrawable2:Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;

    if-eqz v0, :cond_5

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    :cond_0
    :goto_2
    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->setState(I)V

    .line 124
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 126
    sget-object v2, Lcom/navdy/hud/app/view/FuelGaugePresenter2$1;->$SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide:[I

    iget-object v4, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mTanksSide:Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/FuelGaugePresenter2$FuelTankSide;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 155
    :cond_1
    :goto_3
    iget v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRange:I

    if-lez v2, :cond_6

    .line 156
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeText:Landroid/widget/TextView;

    iget v3, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRange:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :goto_4
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeUnitText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->mFuelRangeUnit:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    .end local v0    # "lowFuel":Z
    .end local v1    # "veryLowFuel":Z
    :cond_2
    return-void

    :cond_3
    move v0, v3

    .line 119
    goto :goto_0

    .restart local v0    # "lowFuel":Z
    :cond_4
    move v1, v3

    .line 120
    goto :goto_1

    .restart local v1    # "veryLowFuel":Z
    :cond_5
    move v2, v3

    .line 123
    goto :goto_2

    .line 128
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    if-eqz v1, :cond_1

    .line 132
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 137
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    if-eqz v1, :cond_1

    .line 141
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 146
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->fuelTankIndicatorRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    if-eqz v1, :cond_1

    .line 150
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->lowFuelIndicatorLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 158
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->rangeText:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
