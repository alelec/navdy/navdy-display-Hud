.class public Lcom/navdy/hud/app/view/ETAGaugePresenter$$ViewInjector;
.super Ljava/lang/Object;
.source "ETAGaugePresenter$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/ETAGaugePresenter;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/ETAGaugePresenter;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00e5

    const-string v2, "field \'ttaText1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText1:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e00e6

    const-string v2, "field \'ttaText2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText2:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e00e7

    const-string v2, "field \'ttaText3\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText3:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e00e8

    const-string v2, "field \'ttaText4\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText4:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e00ea

    const-string v2, "field \'remainingDistanceText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceText:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e00eb

    const-string v2, "field \'remainingDistanceUnitText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    .line 22
    const v1, 0x7f0e00ec

    const-string v2, "field \'etaText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaText:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f0e00ed

    const-string v2, "field \'etaAmPmText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaAmPmText:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f0e00e3

    const-string v2, "field \'etaView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    .line 28
    const v1, 0x7f0e00ee

    const-string v2, "field \'tripOpenMapView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    .line 30
    const v1, 0x7f0e00f0

    const-string v2, "field \'tripDistanceView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripDistanceView:Landroid/widget/TextView;

    .line 32
    const v1, 0x7f0e00ef

    const-string v2, "field \'tripTimeView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f0e00f1

    const-string v2, "field \'tripIconView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripIconView:Landroid/widget/ImageView;

    .line 36
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/ETAGaugePresenter;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/ETAGaugePresenter;

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText1:Landroid/widget/TextView;

    .line 40
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText2:Landroid/widget/TextView;

    .line 41
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText3:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->ttaText4:Landroid/widget/TextView;

    .line 43
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceText:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->remainingDistanceUnitText:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaText:Landroid/widget/TextView;

    .line 46
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaAmPmText:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->etaView:Landroid/view/ViewGroup;

    .line 48
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripOpenMapView:Landroid/support/constraint/ConstraintLayout;

    .line 49
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripDistanceView:Landroid/widget/TextView;

    .line 50
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripTimeView:Landroid/widget/TextView;

    .line 51
    iput-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter;->tripIconView:Landroid/widget/ImageView;

    .line 52
    return-void
.end method
