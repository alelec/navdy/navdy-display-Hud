.class public Lcom/navdy/hud/app/view/CompassPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "CompassPresenter.java"

# interfaces
.implements Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;


# instance fields
.field private animator:Lcom/navdy/hud/app/view/SerialValueAnimator;

.field private compassGaugeName:Ljava/lang/String;

.field mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

.field mHeadingAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 20
    new-instance v0, Lcom/navdy/hud/app/view/CompassDrawable;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/view/CompassDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/CompassDrawable;->setMaxGaugeValue(F)V

    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/CompassDrawable;->setMinValue(F)V

    .line 23
    new-instance v0, Lcom/navdy/hud/app/view/SerialValueAnimator;

    const/16 v1, 0x32

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/view/SerialValueAnimator;-><init>(Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->animator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->compassGaugeName:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public animationComplete(F)V
    .locals 0
    .param p1, "newValue"    # F

    .prologue
    .line 83
    return-void
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mHeadingAngle:F

    return v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "COMPASS_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->compassGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method public setHeadingAngle(D)V
    .locals 5
    .param p1, "angle"    # D

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->isDashActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->isWidgetVisibleToUser:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-nez v0, :cond_1

    .line 42
    :cond_0
    double-to-float v0, p1

    iput v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mHeadingAngle:F

    .line 52
    :goto_0
    return-void

    .line 46
    :cond_1
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_2

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mHeadingAngle:F

    .line 48
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CompassPresenter;->reDraw()V

    goto :goto_0

    .line 50
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->animator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    const-wide v2, 0x4076800000000000L    # 360.0

    rem-double v2, p1, v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/SerialValueAnimator;->setValue(F)V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "newValue"    # F

    .prologue
    .line 76
    iput p1, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mHeadingAngle:F

    .line 77
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CompassPresenter;->reDraw()V

    .line 78
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 1
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 29
    if-eqz p1, :cond_0

    .line 30
    const v0, 0x7f03006a

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 32
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 33
    return-void
.end method

.method protected updateGauge()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mCompassDrawable:Lcom/navdy/hud/app/view/CompassDrawable;

    iget v1, p0, Lcom/navdy/hud/app/view/CompassPresenter;->mHeadingAngle:F

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/CompassDrawable;->setGaugeValue(F)V

    .line 57
    return-void
.end method
