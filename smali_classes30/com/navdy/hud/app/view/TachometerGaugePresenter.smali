.class public Lcom/navdy/hud/app/view/TachometerGaugePresenter;
.super Lcom/navdy/hud/app/view/GaugeViewPresenter;
.source "TachometerGaugePresenter.java"

# interfaces
.implements Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;


# static fields
.field public static final ID:Ljava/lang/String; = "TACHOMETER_GAUGE_IDENTIFIER"

.field private static final MAX_MARKER_ANIMATION_DELAY:I = 0x1f4

.field private static final MAX_MARKER_ANIMATION_DURATION:I = 0x3e8

.field private static final MAX_RPM_VALUE:I = 0x1f40

.field private static final RPM_DIFFERENCE:I = 0x64

.field private static final RPM_WARNING_THRESHOLD:I = 0x1b58

.field public static final VALUE_ANIMATION_DURATION:I = 0xc8


# instance fields
.field animatedRpm:I

.field private animationRunnable:Ljava/lang/Runnable;

.field private fullMode:Z

.field private handler:Landroid/os/Handler;

.field private final kmhString:Ljava/lang/String;

.field private lastRPM:I

.field mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

.field private maxMarkerAlpha:I

.field private maxRpmValue:I

.field private final mphString:Ljava/lang/String;

.field rpm:I

.field private serialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

.field private showingMaxMarkerAnimation:Z

.field speed:I

.field private speedFastWarningColor:I

.field speedLimit:I

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private speedSafeColor:I

.field private speedVeryFastWarningColor:I

.field private valueAnimator:Landroid/animation/ValueAnimator;

.field private widgetName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GaugeViewPresenter;-><init>()V

    .line 50
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 54
    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->lastRPM:I

    .line 57
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->handler:Landroid/os/Handler;

    .line 58
    new-instance v1, Lcom/navdy/hud/app/view/SerialValueAnimator;

    const/16 v2, 0xc8

    invoke-direct {v1, p0, v2}, Lcom/navdy/hud/app/view/SerialValueAnimator;-><init>(Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;I)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->serialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    .line 59
    new-instance v1, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    .line 60
    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    const/high16 v2, 0x45fa0000    # 8000.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->setMaxGaugeValue(F)V

    .line 61
    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    const/16 v2, 0x1b58

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->setRPMWarningLevel(I)V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedSafeColor:I

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedFastWarningColor:I

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedVeryFastWarningColor:I

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->widgetName:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0901c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mphString:Ljava/lang/String;

    .line 69
    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->kmhString:Ljava/lang/String;

    .line 71
    new-instance v1, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;-><init>(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->animationRunnable:Ljava/lang/Runnable;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/TachometerGaugePresenter;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/view/TachometerGaugePresenter;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/view/TachometerGaugePresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxMarkerAlpha:I

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/view/TachometerGaugePresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->showingMaxMarkerAnimation:Z

    return p1
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/TachometerGaugePresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I

    return p1
.end method


# virtual methods
.method public animationComplete(F)V
    .locals 0
    .param p1, "newValue"    # F

    .prologue
    .line 252
    return-void
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    int-to-float v0, v0

    return v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    const-string v0, "TACHOMETER_GAUGE_IDENTIFIER"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return-object v0
.end method

.method public setRPM(I)V
    .locals 3
    .param p1, "rpm"    # I

    .prologue
    .line 115
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->isDashActive:Z

    if-nez v1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->lastRPM:I

    if-eq v1, p1, :cond_0

    .line 121
    iget v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->lastRPM:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 122
    .local v0, "diff":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 125
    iput p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->lastRPM:I

    .line 126
    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->serialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    int-to-float v2, p1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/SerialValueAnimator;->setValue(F)V

    goto :goto_0
.end method

.method public setSpeed(I)V
    .locals 1
    .param p1, "speed"    # I

    .prologue
    .line 130
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    if-ne v0, p1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iput p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    .line 134
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->isDashActive:Z

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->reDraw()V

    goto :goto_0
.end method

.method public setSpeedLimit(I)V
    .locals 1
    .param p1, "speedLimit"    # I

    .prologue
    .line 141
    iget v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    if-ne v0, p1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iput p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    .line 145
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->isDashActive:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->reDraw()V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 4
    .param p1, "newValue"    # F

    .prologue
    const/4 v2, 0x0

    .line 230
    float-to-int v0, p1

    iput v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    .line 231
    iget v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    iget v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I

    if-le v0, v1, :cond_0

    .line 232
    const/16 v0, 0x1f40

    iget v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I

    .line 233
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->animationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 234
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->showingMaxMarkerAnimation:Z

    .line 235
    iput v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxMarkerAlpha:I

    .line 236
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->valueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 240
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    iget v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->showingMaxMarkerAnimation:Z

    if-nez v0, :cond_1

    .line 241
    const/16 v0, 0xff

    iput v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxMarkerAlpha:I

    .line 242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->showingMaxMarkerAnimation:Z

    .line 243
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->animationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->animationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 246
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->reDraw()V

    .line 247
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 5
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 210
    if-eqz p1, :cond_0

    .line 211
    const v2, 0x7f03007a

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 213
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/GaugeViewPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 214
    check-cast p1, Lcom/navdy/hud/app/view/GaugeView;

    .end local p1    # "dashboardWidgetView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    iput-object p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    .line 215
    iget-object v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    if-eqz v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 216
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 217
    .local v0, "outValue":Landroid/util/TypedValue;
    iget-object v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0150

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 218
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    .line 219
    .local v1, "spacing":F
    iget-object v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getValueTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLetterSpacing(F)V

    .line 221
    .end local v0    # "outValue":Landroid/util/TypedValue;
    .end local v1    # "spacing":F
    :cond_1
    return-void
.end method

.method protected updateGauge()V
    .locals 10

    .prologue
    const v9, 0x7f090275

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 158
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    if-eqz v3, :cond_0

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->rpm:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->setGaugeValue(F)V

    .line 160
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->setMaxMarkerValue(I)V

    .line 161
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mTachometerGaugeDrawable:Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxMarkerAlpha:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->setMaxMarkerAlpha(I)V

    .line 162
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/GaugeView;->setValueText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v1

    .line 166
    .local v1, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    const/4 v2, 0x0

    .line 167
    .local v2, "unitText":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/view/TachometerGaugePresenter$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 170
    iget-object v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mphString:Ljava/lang/String;

    .line 171
    const/16 v0, 0x8

    .line 179
    .local v0, "speedLimitThreshold":I
    :goto_0
    iget v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    if-lez v3, :cond_3

    .line 180
    iget v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    add-int/2addr v4, v0

    if-lt v3, v4, :cond_1

    .line 181
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget-object v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/GaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v2, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getUnitTextView()Landroid/widget/TextView;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedVeryFastWarningColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 196
    .end local v0    # "speedLimitThreshold":I
    .end local v1    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .end local v2    # "unitText":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 174
    .restart local v1    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .restart local v2    # "unitText":Ljava/lang/String;
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->kmhString:Ljava/lang/String;

    .line 175
    const/16 v0, 0xd

    .restart local v0    # "speedLimitThreshold":I
    goto :goto_0

    .line 183
    :cond_1
    iget v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    if-lt v3, v4, :cond_2

    .line 184
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget-object v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/GaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedLimit:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v2, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getUnitTextView()Landroid/widget/TextView;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedFastWarningColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 187
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getUnitTextView()Landroid/widget/TextView;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedSafeColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 188
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 191
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speed:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/GaugeView;->setValueText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v3, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getUnitTextView()Landroid/widget/TextView;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->speedSafeColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 167
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
