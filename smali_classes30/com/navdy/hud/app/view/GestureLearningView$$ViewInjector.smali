.class public Lcom/navdy/hud/app/view/GestureLearningView$$ViewInjector;
.super Ljava/lang/Object;
.source "GestureLearningView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/GestureLearningView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/GestureLearningView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00d2

    const-string v2, "field \'mChoiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 12
    const v1, 0x7f0e00f9

    const-string v2, "field \'mTipsScroller\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/LinearLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    .line 14
    const v1, 0x7f0e00fa

    const-string v2, "field \'mTipsTextView1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView1:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e00fb

    const-string v2, "field \'mTipsTextView2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView2:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e00fc

    const-string v2, "field \'mCenterImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mCenterImage:Landroid/widget/ImageView;

    .line 20
    const v1, 0x7f0e00f7

    const-string v2, "field \'mGestureProgressIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    .line 22
    const v1, 0x7f0e00fd

    const-string v2, "field \'mLeftSwipeLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/LinearLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mLeftSwipeLayout:Landroid/widget/LinearLayout;

    .line 24
    const v1, 0x7f0e0101

    const-string v2, "field \'mRightSwipeLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/LinearLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/GestureLearningView;->mRightSwipeLayout:Landroid/widget/LinearLayout;

    .line 26
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/GestureLearningView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsScroller:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView1:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mTipsTextView2:Landroid/widget/TextView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mCenterImage:Landroid/widget/ImageView;

    .line 34
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mLeftSwipeLayout:Landroid/widget/LinearLayout;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/view/GestureLearningView;->mRightSwipeLayout:Landroid/widget/LinearLayout;

    .line 37
    return-void
.end method
