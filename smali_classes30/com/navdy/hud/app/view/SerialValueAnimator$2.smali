.class Lcom/navdy/hud/app/view/SerialValueAnimator$2;
.super Ljava/lang/Object;
.source "SerialValueAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/SerialValueAnimator;->animate(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/SerialValueAnimator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/SerialValueAnimator;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$2;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 71
    .local v0, "val":F
    iget-object v1, p0, Lcom/navdy/hud/app/view/SerialValueAnimator$2;->this$0:Lcom/navdy/hud/app/view/SerialValueAnimator;

    # getter for: Lcom/navdy/hud/app/view/SerialValueAnimator;->mAdapter:Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;
    invoke-static {v1}, Lcom/navdy/hud/app/view/SerialValueAnimator;->access$000(Lcom/navdy/hud/app/view/SerialValueAnimator;)Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;->setValue(F)V

    .line 72
    return-void
.end method
