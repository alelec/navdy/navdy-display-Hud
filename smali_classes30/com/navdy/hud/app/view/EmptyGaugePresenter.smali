.class public Lcom/navdy/hud/app/view/EmptyGaugePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "EmptyGaugePresenter.java"


# static fields
.field public static final ALPHA_ANIMATION_DURATION:I = 0x7d0

.field public static final ANIMATION_DELAY:I = 0x3e8


# instance fields
.field private emptyGaugeName:Ljava/lang/String;

.field private mAnimationRunnable:Ljava/lang/Runnable;

.field mEmptyGaugeText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00e1
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mPlayedAnimation:Z

.field private mTextAlphaAnimator:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mPlayedAnimation:Z

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mHandler:Landroid/os/Handler;

    .line 35
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 38
    const v0, 0x7f090313

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->emptyGaugeName:Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/EmptyGaugePresenter$1;-><init>(Lcom/navdy/hud/app/view/EmptyGaugePresenter;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 48
    new-instance v0, Lcom/navdy/hud/app/view/EmptyGaugePresenter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/EmptyGaugePresenter$2;-><init>(Lcom/navdy/hud/app/view/EmptyGaugePresenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mAnimationRunnable:Ljava/lang/Runnable;

    .line 54
    return-void

    .line 36
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/EmptyGaugePresenter;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string v0, "EMPTY_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->emptyGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 58
    if-eqz p1, :cond_2

    .line 59
    const v1, 0x7f030016

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 60
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 61
    if-eqz p2, :cond_0

    .line 62
    const-string v1, "EXTRA_IS_ACTIVE"

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 63
    .local v0, "isActive":Z
    if-eqz v0, :cond_1

    .line 64
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mPlayedAnimation:Z

    if-nez v1, :cond_0

    .line 65
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mPlayedAnimation:Z

    .line 66
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 67
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 68
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mEmptyGaugeText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 69
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mAnimationRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 78
    .end local v0    # "isActive":Z
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 83
    :goto_1
    return-void

    .line 72
    .restart local v0    # "isActive":Z
    :cond_1
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mPlayedAnimation:Z

    .line 73
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mTextAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    iget-object v1, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mEmptyGaugeText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0

    .line 80
    .end local v0    # "isActive":Z
    :cond_2
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;->mPlayedAnimation:Z

    .line 81
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method protected updateGauge()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method
