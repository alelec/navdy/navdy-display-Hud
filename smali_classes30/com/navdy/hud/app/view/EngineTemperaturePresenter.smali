.class public final Lcom/navdy/hud/app/view/EngineTemperaturePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "EngineTemperaturePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0006\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\u0008\u0010E\u001a\u00020\u0018H\u0016J\u0008\u0010F\u001a\u00020\u0018H\u0016J\u0008\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\u0008\u0010M\u001a\u0004\u0018\u00010N2\u0008\u0010O\u001a\u0004\u0018\u00010PH\u0016J\u0008\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008\"\u0004\u0008\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0008\"\u0004\u0008\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$R$\u0010\'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010/\"\u0004\u00080\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00083\u0010\u001a\"\u0004\u00084\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008:\u0010;\"\u0004\u0008<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008?\u0010;\"\u0004\u0008@\u0010=R\u0011\u0010A\u001a\u00020\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008B\u0010\u001a\u00a8\u0006S"
    }
    d2 = {
        "Lcom/navdy/hud/app/view/EngineTemperaturePresenter;",
        "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "STATE_COLD",
        "",
        "getSTATE_COLD",
        "()I",
        "setSTATE_COLD",
        "(I)V",
        "STATE_HOT",
        "getSTATE_HOT",
        "setSTATE_HOT",
        "STATE_NORMAL",
        "getSTATE_NORMAL",
        "setSTATE_NORMAL",
        "bus",
        "Lcom/squareup/otto/Bus;",
        "getBus",
        "()Lcom/squareup/otto/Bus;",
        "setBus",
        "(Lcom/squareup/otto/Bus;)V",
        "celsiusLabel",
        "",
        "getCelsiusLabel",
        "()Ljava/lang/String;",
        "setCelsiusLabel",
        "(Ljava/lang/String;)V",
        "getContext",
        "()Landroid/content/Context;",
        "driverProfileManager",
        "Lcom/navdy/hud/app/profile/DriverProfileManager;",
        "getDriverProfileManager",
        "()Lcom/navdy/hud/app/profile/DriverProfileManager;",
        "setDriverProfileManager",
        "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V",
        "value",
        "",
        "engineTemperature",
        "getEngineTemperature",
        "()D",
        "setEngineTemperature",
        "(D)V",
        "engineTemperatureDrawable",
        "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;",
        "getEngineTemperatureDrawable",
        "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;",
        "setEngineTemperatureDrawable",
        "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V",
        "fahrenheitLabel",
        "getFahrenheitLabel",
        "setFahrenheitLabel",
        "mLeftOriented",
        "",
        "registered",
        "tempText",
        "Landroid/widget/TextView;",
        "getTempText",
        "()Landroid/widget/TextView;",
        "setTempText",
        "(Landroid/widget/TextView;)V",
        "tempUnitText",
        "getTempUnitText",
        "setTempUnitText",
        "widgetNameString",
        "getWidgetNameString",
        "getDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "getWidgetIdentifier",
        "getWidgetName",
        "isRegisteringToBusRequired",
        "onSpeedUnitChanged",
        "",
        "speedUnitChangedEvent",
        "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;",
        "setView",
        "dashboardWidgetView",
        "Lcom/navdy/hud/app/view/DashboardWidgetView;",
        "arguments",
        "Landroid/os/Bundle;",
        "updateGauge",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

# The value of this static final field might be set in the static constructor
.field private static final TEMPERATURE_GAUGE_COLD_THRESHOLD:D = 45.7

# The value of this static final field might be set in the static constructor
.field private static final TEMPERATURE_GAUGE_HOT_THRESHOLD:D = 105.0

# The value of this static final field might be set in the static constructor
.field private static final TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS:D = 38.0

# The value of this static final field might be set in the static constructor
.field private static final TEMPERATURE_GAUGE_MID_POINT_CELSIUS:D = 76.5

# The value of this static final field might be set in the static constructor
.field private static final TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS:D = 115.0


# instance fields
.field private STATE_COLD:I

.field private STATE_HOT:I

.field private STATE_NORMAL:I

.field public bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private celsiusLabel:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final context:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private engineTemperature:D

.field private engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private fahrenheitLabel:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private mLeftOriented:Z

.field private registered:Z

.field public tempText:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public tempUnitText:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final widgetNameString:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    .line 28
    const-wide/high16 v0, 0x4043000000000000L    # 38.0

    sput-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS:D

    .line 29
    const-wide v0, 0x405cc00000000000L    # 115.0

    sput-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS:D

    .line 30
    const-wide v0, 0x4053200000000000L    # 76.5

    sput-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_MID_POINT_CELSIUS:D

    .line 31
    const-wide v0, 0x4046d9999999999aL    # 45.7

    sput-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_COLD_THRESHOLD:D

    .line 32
    const-wide v0, 0x405a400000000000L    # 105.0

    sput-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_HOT_THRESHOLD:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const-string v1, "context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->context:Landroid/content/Context;

    .line 36
    iput v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_COLD:I

    .line 37
    const/4 v1, 0x2

    iput v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_HOT:I

    .line 41
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->mLeftOriented:Z

    .line 56
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->fahrenheitLabel:Ljava/lang/String;

    .line 57
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->celsiusLabel:Ljava/lang/String;

    .line 63
    new-instance v1, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    iget-object v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->context:Landroid/content/Context;

    const v3, 0x7f0a000d

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    .line 65
    iget-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    if-eqz v1, :cond_0

    sget-object v2, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->setMinValue(F)V

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    if-eqz v1, :cond_1

    sget-object v2, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->setMaxGaugeValue(F)V

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 69
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09028d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026perature_unit_fahrenheit)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->fahrenheitLabel:Ljava/lang/String;

    .line 70
    const v1, 0x7f09028c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026temperature_unit_celsius)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->celsiusLabel:Ljava/lang/String;

    .line 71
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 72
    const v1, 0x7f090314

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026idget_engine_temperature)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->widgetNameString:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp()D
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_COLD_THRESHOLD:D

    return-wide v0
.end method

.method public static final synthetic access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp()D
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_HOT_THRESHOLD:D

    return-wide v0
.end method

.method public static final synthetic access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp()D
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS:D

    return-wide v0
.end method

.method public static final synthetic access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp()D
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_MID_POINT_CELSIUS:D

    return-wide v0
.end method

.method public static final synthetic access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp()D
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS:D

    return-wide v0
.end method


# virtual methods
.method public final getBus()Lcom/squareup/otto/Bus;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->bus:Lcom/squareup/otto/Bus;

    if-nez v0, :cond_0

    const-string v1, "bus"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getCelsiusLabel()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->celsiusLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-nez v0, :cond_0

    const-string v1, "driverProfileManager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getEngineTemperature()D
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    return-wide v0
.end method

.method public final getEngineTemperatureDrawable()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    return-object v0
.end method

.method public final getFahrenheitLabel()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->fahrenheitLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final getSTATE_COLD()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_COLD:I

    return v0
.end method

.method public final getSTATE_HOT()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_HOT:I

    return v0
.end method

.method public final getSTATE_NORMAL()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_NORMAL:I

    return v0
.end method

.method public final getTempText()Landroid/widget/TextView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "tempText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getTempUnitText()Landroid/widget/TextView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempUnitText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "tempUnitText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 136
    const-string v0, "ENGINE_TEMPERATURE_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->widgetNameString:Ljava/lang/String;

    return-object v0
.end method

.method public final getWidgetNameString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->widgetNameString:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method

.method public final onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 1
    .param p1, "speedUnitChangedEvent"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "speedUnitChangedEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->reDraw()V

    .line 132
    return-void
.end method

.method public final setBus(Lcom/squareup/otto/Bus;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/squareup/otto/Bus;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->bus:Lcom/squareup/otto/Bus;

    return-void
.end method

.method public final setCelsiusLabel(Ljava/lang/String;)V
    .locals 1
    .param p1, "<set-?>"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->celsiusLabel:Ljava/lang/String;

    return-void
.end method

.method public final setDriverProfileManager(Lcom/navdy/hud/app/profile/DriverProfileManager;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    return-void
.end method

.method public final setEngineTemperature(D)V
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    .line 46
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->reDraw()V

    .line 47
    return-void
.end method

.method public final setEngineTemperatureDrawable(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V
    .locals 0
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 26
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    return-void
.end method

.method public final setFahrenheitLabel(Ljava/lang/String;)V
    .locals 1
    .param p1, "<set-?>"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->fahrenheitLabel:Ljava/lang/String;

    return-void
.end method

.method public final setSTATE_COLD(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_COLD:I

    return-void
.end method

.method public final setSTATE_HOT(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_HOT:I

    return-void
.end method

.method public final setSTATE_NORMAL(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_NORMAL:I

    return-void
.end method

.method public final setTempText(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempText:Landroid/widget/TextView;

    return-void
.end method

.method public final setTempUnitText(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempUnitText:Landroid/widget/TextView;

    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "arguments"    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    const v1, 0x7f030017

    .line 80
    .local v1, "layoutResourceId":I
    if-eqz p2, :cond_0

    .line 81
    const-string v2, "EXTRA_GRAVITY"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 82
    .local v0, "gravity":I
    packed-switch v0, :pswitch_data_0

    .line 91
    .end local v0    # "gravity":I
    :cond_0
    :goto_0
    :pswitch_0
    if-eqz p1, :cond_3

    .line 94
    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 95
    const v2, 0x7f0e00be

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Lkotlin/TypeCastException;

    const-string v3, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v2, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 84
    .restart local v0    # "gravity":I
    :pswitch_1
    const v1, 0x7f030017

    .line 85
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->mLeftOriented:Z

    goto :goto_0

    .line 88
    :pswitch_2
    const v1, 0x7f030018

    .line 89
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->mLeftOriented:Z

    goto :goto_0

    .line 95
    .end local v0    # "gravity":I
    :cond_1
    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempText:Landroid/widget/TextView;

    .line 96
    const v2, 0x7f0e00bd

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v2, Lkotlin/TypeCastException;

    const-string v3, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v2, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempUnitText:Landroid/widget/TextView;

    .line 98
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->reDraw()V

    .line 100
    return-void

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected updateGauge()V
    .locals 9

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v0, :cond_a

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-nez v0, :cond_0

    const-string v1, "driverProfileManager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 109
    .local v7, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :goto_0
    iget-object v8, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    if-eqz v8, :cond_1

    iget-wide v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    sget-object v2, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_COLD_THRESHOLD()D

    move-result-wide v2

    sget-object v4, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()D

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ExtensionsKt;->clamp(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->setGaugeValue(F)V

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->mLeftOriented:Z

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->setLeftOriented(Z)V

    .line 111
    :cond_2
    iget v6, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_NORMAL:I

    .line 112
    .local v6, "state":I
    iget-wide v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    sget-object v2, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_COLD_THRESHOLD()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    .line 113
    iget v6, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_COLD:I

    .line 116
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperatureDrawable:Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;

    if-eqz v0, :cond_4

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->setState(I)V

    .line 118
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempText:Landroid/widget/TextView;

    if-nez v1, :cond_5

    const-string v0, "tempText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    if-eqz v1, :cond_8

    sget-object v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v7}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 120
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 108
    .end local v6    # "state":I
    .end local v7    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :cond_6
    sget-object v7, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    goto :goto_0

    .line 114
    .restart local v6    # "state":I
    .restart local v7    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :cond_7
    iget-wide v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    sget-object v2, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_HOT_THRESHOLD()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 115
    iget v6, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->STATE_HOT:I

    goto :goto_1

    .line 119
    :pswitch_0
    iget-wide v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    double-to-int v0, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 118
    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_8
    iget-object v1, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->tempUnitText:Landroid/widget/TextView;

    if-nez v1, :cond_9

    const-string v0, "tempUnitText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    if-eqz v1, :cond_a

    sget-object v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v7}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 124
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 120
    :pswitch_1
    iget-wide v2, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->engineTemperature:D

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ExtensionsKt;->celsiusToFahrenheit(D)D

    move-result-wide v2

    double-to-int v0, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_2

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->celsiusLabel:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    .line 122
    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    .end local v6    # "state":I
    .end local v7    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :cond_a
    return-void

    .line 124
    .restart local v6    # "state":I
    .restart local v7    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->fahrenheitLabel:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_3

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 122
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
