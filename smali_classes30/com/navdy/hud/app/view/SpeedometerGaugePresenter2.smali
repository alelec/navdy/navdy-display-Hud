.class public Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;
.super Lcom/navdy/hud/app/view/GaugeViewPresenter;
.source "SpeedometerGaugePresenter2.java"

# interfaces
.implements Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;


# static fields
.field private static final ALWAYS_SHOW_SPEED_LIMIT:Z = true

.field public static final ID:Ljava/lang/String; = "SPEEDO_METER_WIDGET_2"

.field private static final MAX_ANIMATION_DURATION:I = 0x64

.field public static final MAX_SPEED:I = 0x9e

.field private static final MIN_ANIMATION_DURATION:I = 0x14

.field public static final RESAMPLE_INTERVAL:I = 0x3e8

.field public static final SAFE:I = 0x0

.field private static final SPEED_LIMIT_ANIMATION_DELAY:I = 0x1f4

.field private static final SPEED_LIMIT_ANIMATION_DURATION:I = 0x3e8

.field public static final SPEED_LIMIT_EXCEEDED:I = 0x1

.field public static final SPEED_LIMIT_TRESHOLD_EXCEEDED:I = 0x2


# instance fields
.field private animationDuration:I

.field private handler:Landroid/os/Handler;

.field private final kmhString:Ljava/lang/String;

.field private lastSpeed:I

.field private lastSpeedSampleArrivalTime:J

.field private mSerialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

.field private mSpeed:I

.field private mSpeedLimit:I

.field private mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

.field private final mphString:Ljava/lang/String;

.field private showingSpeedLimitAnimation:Z

.field private speedLimitAlpha:I

.field private speedLimitFadeOutAnimationRunnable:Ljava/lang/Runnable;

.field private speedLimitFadeOutAnimator:Landroid/animation/ValueAnimator;

.field private speedLimitMarkerNeedsTobeRemoved:Z

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private final speedoMeterWidgetName:Ljava/lang/String;

.field private textSize2Chars:I

.field private textSize3Chars:I

.field private updateText:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x0

    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/view/GaugeViewPresenter;-><init>()V

    .line 35
    iput v4, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    .line 41
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitMarkerNeedsTobeRemoved:Z

    .line 42
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->updateText:Z

    .line 47
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 52
    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeed:I

    .line 53
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeedSampleArrivalTime:J

    .line 54
    iput v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->animationDuration:I

    .line 59
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->handler:Landroid/os/Handler;

    .line 60
    new-instance v1, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    const v2, 0x7f0a0012

    const v3, 0x7f0b014c

    invoke-direct {v1, p1, v4, v2, v3}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;-><init>(Landroid/content/Context;III)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    .line 61
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    const/high16 v2, 0x431e0000    # 158.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->setMaxGaugeValue(F)V

    .line 62
    new-instance v1, Lcom/navdy/hud/app/view/SerialValueAnimator;

    invoke-direct {v1, p0, v5}, Lcom/navdy/hud/app/view/SerialValueAnimator;-><init>(Lcom/navdy/hud/app/view/SerialValueAnimator$SerialValueAnimatorAdapter;I)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSerialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    .line 63
    new-instance v1, Landroid/animation/ValueAnimator;

    invoke-direct {v1}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitFadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 66
    new-instance v1, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$1;-><init>(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitFadeOutAnimationRunnable:Ljava/lang/Runnable;

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 110
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0901c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mphString:Ljava/lang/String;

    .line 111
    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->kmhString:Ljava/lang/String;

    .line 112
    const v1, 0x7f0b0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->textSize2Chars:I

    .line 113
    const v1, 0x7f0b0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->textSize3Chars:I

    .line 114
    const v1, 0x7f09010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedoMeterWidgetName:Ljava/lang/String;

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitFadeOutAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitAlpha:I

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showingSpeedLimitAnimation:Z

    return p1
.end method

.method private setSpeedValueInternal(F)V
    .locals 2
    .param p1, "speed"    # F

    .prologue
    .line 303
    iget v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    iget v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    if-lt v0, v1, :cond_1

    .line 305
    invoke-direct {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showSpeedLimitText()V

    .line 311
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->reDraw()V

    .line 312
    return-void

    .line 307
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showingSpeedLimitAnimation:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitMarkerNeedsTobeRemoved:Z

    if-eqz v0, :cond_0

    .line 308
    invoke-direct {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showSpeedLimitTextAndFadeOut()V

    goto :goto_0
.end method

.method private showSpeedLimitText()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitFadeOutAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 249
    const/16 v0, 0xff

    iput v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitAlpha:I

    .line 251
    return-void
.end method

.method private showSpeedLimitTextAndFadeOut()V
    .locals 1

    .prologue
    .line 261
    const/16 v0, 0xff

    iput v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitAlpha:I

    .line 263
    return-void
.end method


# virtual methods
.method public animationComplete(F)V
    .locals 1
    .param p1, "newValue"    # F

    .prologue
    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->updateText:Z

    .line 317
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setSpeedValueInternal(F)V

    .line 318
    return-void
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    int-to-float v0, v0

    return v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const-string v0, "SPEEDO_METER_WIDGET_2"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedoMeterWidgetName:Ljava/lang/String;

    return-object v0
.end method

.method public setSpeed(I)V
    .locals 10
    .param p1, "speed"    # I

    .prologue
    .line 119
    const/4 v5, -0x1

    if-ne p1, v5, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 124
    .local v2, "now":J
    iget-wide v6, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeedSampleArrivalTime:J

    sub-long v0, v2, v6

    .line 125
    .local v0, "newTimeDifferenceBetweenSamples":J
    iput-wide v2, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeedSampleArrivalTime:J

    .line 126
    const-wide/16 v6, 0x3e8

    cmp-long v5, v0, v6

    if-lez v5, :cond_2

    .line 127
    const/16 v5, 0x64

    iput v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->animationDuration:I

    .line 132
    :goto_1
    iget v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeed:I

    if-eq v5, p1, :cond_0

    .line 136
    iput p1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->lastSpeed:I

    .line 138
    const/16 v5, 0x9e

    if-le p1, v5, :cond_3

    .line 139
    const/16 v4, 0x9e

    .line 143
    .local v4, "speedVal":I
    :goto_2
    iget-object v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSerialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    iget v6, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->animationDuration:I

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/view/SerialValueAnimator;->setDuration(I)V

    .line 144
    iget-object v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSerialValueAnimator:Lcom/navdy/hud/app/view/SerialValueAnimator;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/view/SerialValueAnimator;->setValue(F)V

    goto :goto_0

    .line 129
    .end local v4    # "speedVal":I
    :cond_2
    const-wide/16 v6, 0x14

    iget v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->animationDuration:I

    int-to-long v8, v5

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    long-to-int v5, v6

    iput v5, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->animationDuration:I

    goto :goto_1

    .line 141
    :cond_3
    move v4, p1

    .restart local v4    # "speedVal":I
    goto :goto_2
.end method

.method public setSpeedLimit(I)V
    .locals 2
    .param p1, "speedLimit"    # I

    .prologue
    .line 225
    iget v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    if-ne p1, v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 229
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    .line 230
    invoke-direct {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showSpeedLimitText()V

    .line 231
    iget v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    iget v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    if-ge v0, v1, :cond_1

    .line 232
    invoke-direct {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->showSpeedLimitTextAndFadeOut()V

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->reDraw()V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "newValue"    # F

    .prologue
    .line 294
    float-to-int v0, p1

    .line 295
    .local v0, "newSpeed":I
    iget v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    if-eq v0, v1, :cond_0

    .line 296
    iput v0, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    .line 297
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->updateText:Z

    .line 298
    iget v1, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setSpeedValueInternal(F)V

    .line 300
    :cond_0
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 5
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 267
    if-eqz p1, :cond_0

    .line 268
    const v2, 0x7f030070

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 270
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/GaugeViewPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 271
    iget-object v2, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    if-eqz v2, :cond_2

    .line 272
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 273
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 274
    .local v0, "outValue":Landroid/util/TypedValue;
    iget-object v2, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0150

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 275
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    .line 276
    .local v1, "spacing":F
    iget-object v2, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getValueTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLetterSpacing(F)V

    .line 278
    .end local v0    # "outValue":Landroid/util/TypedValue;
    .end local v1    # "spacing":F
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->reDraw()V

    .line 280
    :cond_2
    return-void
.end method

.method public updateGauge()V
    .locals 13

    .prologue
    const/4 v10, 0x2

    .line 150
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    if-eqz v8, :cond_2

    .line 151
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/GaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 152
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->setGaugeValue(F)V

    .line 153
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->setSpeedLimit(I)V

    .line 154
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v4

    .line 155
    .local v4, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget-object v7, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mphString:Ljava/lang/String;

    .line 157
    .local v7, "unitText":Ljava/lang/String;
    sget-object v8, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 160
    const/16 v3, 0x8

    .line 161
    .local v3, "speedLimitThreshold":I
    iget-object v7, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mphString:Ljava/lang/String;

    .line 168
    :goto_0
    iget-boolean v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->updateText:Z

    if-eqz v8, :cond_0

    .line 169
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/GaugeView;->getValueTextView()Landroid/widget/TextView;

    move-result-object v6

    .line 170
    .local v6, "tvValue":Landroid/widget/TextView;
    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    const/16 v9, 0x64

    if-lt v8, v9, :cond_3

    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->textSize3Chars:I

    int-to-float v8, v8

    :goto_1
    invoke-virtual {v6, v10, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 171
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/GaugeView;->setValueText(Ljava/lang/CharSequence;)V

    .line 173
    .end local v6    # "tvValue":Landroid/widget/TextView;
    :cond_0
    const/4 v5, 0x0

    .line 174
    .local v5, "state":I
    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    if-nez v8, :cond_4

    .line 176
    const/4 v5, 0x0

    .line 185
    :cond_1
    :goto_2
    const v0, 0x106000b

    .line 186
    .local v0, "colorRes":I
    const/4 v2, 0x0

    .line 187
    .local v2, "showSpeedLimitLabel":Z
    packed-switch v5, :pswitch_data_1

    .line 200
    :goto_3
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/GaugeView;->getUnitTextView()Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 201
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    invoke-virtual {v8, v5}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->setState(I)V

    .line 202
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedoMeterGaugeDrawable:Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->speedLimitAlpha:I

    iput v9, v8, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->speedLimitTextAlpha:I

    .line 206
    if-eqz v2, :cond_6

    .line 207
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    const v9, 0x7f090275

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v7, v10, v11

    invoke-virtual {v1, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    .line 212
    .end local v0    # "colorRes":I
    .end local v1    # "res":Landroid/content/res/Resources;
    .end local v2    # "showSpeedLimitLabel":Z
    .end local v3    # "speedLimitThreshold":I
    .end local v4    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .end local v5    # "state":I
    .end local v7    # "unitText":Ljava/lang/String;
    :cond_2
    :goto_4
    return-void

    .line 164
    .restart local v1    # "res":Landroid/content/res/Resources;
    .restart local v4    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .restart local v7    # "unitText":Ljava/lang/String;
    :pswitch_0
    iget-object v7, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->kmhString:Ljava/lang/String;

    .line 165
    const/16 v3, 0xd

    .restart local v3    # "speedLimitThreshold":I
    goto :goto_0

    .line 170
    .restart local v6    # "tvValue":Landroid/widget/TextView;
    :cond_3
    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->textSize2Chars:I

    int-to-float v8, v8

    goto :goto_1

    .line 178
    .end local v6    # "tvValue":Landroid/widget/TextView;
    .restart local v5    # "state":I
    :cond_4
    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    if-le v8, v9, :cond_5

    .line 179
    const/4 v5, 0x1

    .line 181
    :cond_5
    iget v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeed:I

    iget v9, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mSpeedLimit:I

    add-int/2addr v9, v3

    if-le v8, v9, :cond_1

    .line 182
    const/4 v5, 0x2

    goto :goto_2

    .line 189
    .restart local v0    # "colorRes":I
    .restart local v2    # "showSpeedLimitLabel":Z
    :pswitch_1
    const v0, 0x106000b

    .line 190
    goto :goto_3

    .line 192
    :pswitch_2
    const v0, 0x7f0d000f

    .line 193
    const/4 v2, 0x1

    .line 194
    goto :goto_3

    .line 196
    :pswitch_3
    const v0, 0x7f0d00a6

    .line 197
    const/4 v2, 0x1

    goto :goto_3

    .line 209
    :cond_6
    iget-object v8, p0, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v8, v7}, Lcom/navdy/hud/app/view/GaugeView;->setUnitText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 187
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
