.class Lcom/navdy/hud/app/view/WelcomeView$3;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/WelcomeView;->initializeCarousel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/WelcomeView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/WelcomeView;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$3;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V
    .locals 14
    .param p1, "largeImage"    # Z
    .param p2, "model"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p3, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 293
    move-object/from16 v6, p3

    check-cast v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 294
    .local v6, "initialsImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    move-object/from16 v0, p2

    iget v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    const v2, 0x7f0e0094

    if-eq v1, v2, :cond_0

    move-object/from16 v0, p2

    iget v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    const v2, 0x7f0e0096

    if-ne v1, v2, :cond_1

    .line 297
    :cond_0
    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v6, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 344
    :goto_0
    return-void

    .line 300
    :cond_1
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->extras:Ljava/lang/Object;

    check-cast v9, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;

    .line 301
    .local v9, "device":Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;
    move-object/from16 v0, p2

    iget v1, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    const/16 v2, 0x64

    if-lt v1, v2, :cond_2

    const/4 v11, 0x1

    .line 302
    .local v11, "missingDevice":Z
    :goto_1
    if-eqz v11, :cond_3

    if-nez p1, :cond_3

    const/high16 v1, 0x3f000000    # 0.5f

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 304
    iget-object v4, v9, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 305
    .local v4, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 306
    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    .line 307
    .local v10, "infoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v10, :cond_4

    const v1, 0x7f0e0111

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    .line 308
    .local v12, "title":Ljava/lang/String;
    :goto_3
    invoke-static {v12}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 309
    .local v7, "initials":Ljava/lang/String;
    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v6, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 311
    iget-object v1, v9, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverImageFile()Ljava/io/File;

    move-result-object v5

    .line 312
    .local v5, "imagePath":Ljava/io/File;
    invoke-static {v5}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 313
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_5

    .line 314
    invoke-virtual {v6, v8}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 301
    .end local v4    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v5    # "imagePath":Ljava/io/File;
    .end local v7    # "initials":Ljava/lang/String;
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v10    # "infoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v11    # "missingDevice":Z
    .end local v12    # "title":Ljava/lang/String;
    :cond_2
    const/4 v11, 0x0

    goto :goto_1

    .line 302
    .restart local v11    # "missingDevice":Z
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_2

    .line 307
    .restart local v4    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v10    # "infoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_4
    const-string v12, ""

    goto :goto_3

    .line 316
    .restart local v5    # "imagePath":Ljava/io/File;
    .restart local v7    # "initials":Ljava/lang/String;
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "title":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v13

    new-instance v1, Lcom/navdy/hud/app/view/WelcomeView$3$1;

    move-object v2, p0

    move-object/from16 v3, p3

    invoke-direct/range {v1 .. v7}, Lcom/navdy/hud/app/view/WelcomeView$3$1;-><init>(Lcom/navdy/hud/app/view/WelcomeView$3;Landroid/widget/ImageView;Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v13, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method


# virtual methods
.method public processInfoView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;I)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I

    .prologue
    .line 347
    return-void
.end method

.method public processLargeImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 287
    move-object v0, p2

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 288
    .local v0, "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-direct {p0, v2, p1, v1}, Lcom/navdy/hud/app/view/WelcomeView$3;->updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V

    .line 289
    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-direct {p0, v2, p1, v1}, Lcom/navdy/hud/app/view/WelcomeView$3;->updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V

    .line 290
    return-void
.end method

.method public processSmallImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 280
    move-object v0, p2

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 281
    .local v0, "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-direct {p0, v2, p1, v1}, Lcom/navdy/hud/app/view/WelcomeView$3;->updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V

    .line 282
    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-direct {p0, v2, p1, v1}, Lcom/navdy/hud/app/view/WelcomeView$3;->updateInitials(ZLcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/widget/ImageView;)V

    .line 283
    return-void
.end method
