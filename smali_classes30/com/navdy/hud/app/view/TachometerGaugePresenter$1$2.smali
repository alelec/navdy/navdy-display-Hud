.class Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;
.super Ljava/lang/Object;
.source "TachometerGaugePresenter.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;->this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 102
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;->this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # setter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxMarkerAlpha:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$102(Lcom/navdy/hud/app/view/TachometerGaugePresenter;I)I

    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;->this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # setter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->showingMaxMarkerAnimation:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$202(Lcom/navdy/hud/app/view/TachometerGaugePresenter;Z)Z

    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;->this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    # setter for: Lcom/navdy/hud/app/view/TachometerGaugePresenter;->maxRpmValue:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->access$302(Lcom/navdy/hud/app/view/TachometerGaugePresenter;I)I

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1$2;->this$1:Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/TachometerGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->reDraw()V

    .line 97
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 107
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 89
    return-void
.end method
