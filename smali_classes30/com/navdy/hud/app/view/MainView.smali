.class public Lcom/navdy/hud/app/view/MainView;
.super Landroid/widget/FrameLayout;
.source "MainView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;,
        Lcom/navdy/hud/app/view/MainView$AnimationMode;
    }
.end annotation


# static fields
.field private static final COMPACT_MODE_SCALE_PROPERTY:Ljava/lang/String; = "persist.sys.compact.scale"

.field private static final DEFAULT_COMPACT_MODE_SCALE:F = 0.87f

.field private static final SHOW_OPTION_LAG:I = 0x15e

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private animationQueue:Lcom/navdy/hud/app/ui/framework/AnimationQueue;

.field private basePaddingTop:I

.field private brightnessControl:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field containerView:Lcom/navdy/hud/app/view/ContainerView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0132
    .end annotation
.end field

.field expandedNotificationCoverView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0136
    .end annotation
.end field

.field expandedNotificationView:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0137
    .end annotation
.end field

.field private format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

.field private handler:Landroid/os/Handler;

.field private isNotificationCollapsing:Z

.field private isNotificationExpanding:Z

.field private isScreenAnimating:Z

.field mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0139
    .end annotation
.end field

.field private mainPanelWidth:I

.field notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013b
    .end annotation
.end field

.field notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013c
    .end annotation
.end field

.field private notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

.field notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0134
    .end annotation
.end field

.field notificationExtensionView:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0138
    .end annotation
.end field

.field notificationView:Lcom/navdy/hud/app/view/NotificationView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013a
    .end annotation
.end field

.field preferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rightPanelStart:I

.field private screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

.field screenContainer:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0133
    .end annotation
.end field

.field private showingNotificationExtension:Z

.field private sidePanelWidth:I

.field splitterView:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0131
    .end annotation
.end field

.field systemTray:Lcom/navdy/hud/app/ui/component/SystemTrayView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0135
    .end annotation
.end field

.field toastView:Lcom/navdy/hud/app/view/ToastView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013d
    .end annotation
.end field

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private verticalOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/MainView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/MainView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 199
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/MainView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 203
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 141
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    .line 152
    new-instance v0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/framework/AnimationQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->animationQueue:Lcom/navdy/hud/app/ui/framework/AnimationQueue;

    .line 154
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->handler:Landroid/os/Handler;

    .line 162
    new-instance v0, Lcom/navdy/hud/app/view/MainView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/MainView$1;-><init>(Lcom/navdy/hud/app/view/MainView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .line 182
    new-instance v0, Lcom/navdy/hud/app/view/MainView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/MainView$2;-><init>(Lcom/navdy/hud/app/view/MainView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .line 204
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 209
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->initLayout()V

    .line 210
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/view/MainView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationExpanding:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/view/MainView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/MainView;->postNotificationCollapseEvent(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/view/MainView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationCollapsing:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/view/MainView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/MainView;->showingNotificationExtension:Z

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/view/MainView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/MainView;->isScreenAnimating:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/MainView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/MainView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/view/MainView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->verticalOffset:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/view/MainView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/view/MainView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/view/MainView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/view/MainView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I

    return p1
.end method

.method static synthetic access$700()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/view/MainView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->rightPanelStart:I

    return v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/view/MainView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/navdy/hud/app/view/MainView;->rightPanelStart:I

    return p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/view/MainView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private addDebugMarkers(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, -0x1

    .line 460
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 461
    .local v4, "resources":Landroid/content/res/Resources;
    const v7, 0x106000b

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 462
    .local v0, "color":I
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 463
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "window"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 464
    .local v6, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 465
    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v7, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    float-to-int v1, v7

    .line 467
    .local v1, "dip":I
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v9, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 468
    .local v3, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v7, 0x30

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 469
    new-instance v5, Landroid/view/View;

    invoke-direct {v5, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 470
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 471
    iget-object v7, p0, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v5, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 473
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    .end local v3    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v3, v9, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 474
    .restart local v3    # "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v7, 0x50

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 475
    new-instance v5, Landroid/view/View;

    .end local v5    # "view":Landroid/view/View;
    invoke-direct {v5, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 476
    .restart local v5    # "view":Landroid/view/View;
    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 477
    iget-object v7, p0, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v5, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 478
    return-void
.end method

.method private compactScale()F
    .locals 2

    .prologue
    .line 307
    const-string v0, "persist.sys.compact.scale"

    const v1, 0x3f5eb852    # 0.87f

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method private getRemoveNotificationExtensionAnimator()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getNotificationExtensionView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 576
    .local v0, "notificationExtension":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x10b0001

    invoke-static {v2, v3}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    .line 578
    .local v1, "objectAnimator":Landroid/animation/Animator;
    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 579
    new-instance v2, Lcom/navdy/hud/app/view/MainView$7;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/view/MainView$7;-><init>(Lcom/navdy/hud/app/view/MainView;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 591
    return-object v1
.end method

.method private getXPositionAnimator(Landroid/view/ViewGroup;I)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "xPos"    # I

    .prologue
    .line 439
    const-string v0, "x"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    int-to-float v3, p2

    aput v3, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/MainView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/MainView$3;-><init>(Lcom/navdy/hud/app/view/MainView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 295
    return-void
.end method

.method private middleAnimate(Lcom/navdy/hud/app/view/MainView$AnimationMode;)Landroid/animation/Animator;
    .locals 9
    .param p1, "animationMode"    # Lcom/navdy/hud/app/view/MainView$AnimationMode;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 604
    const/4 v1, 0x0

    .line 605
    .local v1, "customAnimator":Landroid/animation/Animator;
    const/4 v0, 0x0

    .line 606
    .local v0, "customAnimationMode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    sget-object v4, Lcom/navdy/hud/app/view/MainView$AnimationMode;->MOVE_LEFT_SHRINK:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    if-ne p1, v4, :cond_2

    .line 607
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 612
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 613
    iget-object v4, p0, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/view/ContainerView;->getCustomContainerAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)Landroid/animation/Animator;

    move-result-object v1

    .line 615
    :cond_1
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 617
    .local v2, "set":Landroid/animation/AnimatorSet;
    sget-object v4, Lcom/navdy/hud/app/view/MainView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$AnimationMode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 641
    const/4 v2, 0x0

    .line 644
    .end local v2    # "set":Landroid/animation/AnimatorSet;
    :goto_1
    return-object v2

    .line 608
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/view/MainView$AnimationMode;->RIGHT_EXPAND:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    if-ne p1, v4, :cond_0

    .line 609
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    goto :goto_0

    .line 620
    .restart local v2    # "set":Landroid/animation/AnimatorSet;
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    iget v5, p0, Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I

    neg-int v5, v5

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/view/MainView;->getXPositionAnimator(Landroid/view/ViewGroup;I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 621
    .local v3, "xAnimator":Landroid/animation/Animator;
    if-eqz v1, :cond_3

    .line 622
    new-array v4, v8, [Landroid/animation/Animator;

    aput-object v3, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_1

    .line 624
    :cond_3
    new-array v4, v7, [Landroid/animation/Animator;

    aput-object v3, v4, v6

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_1

    .line 631
    .end local v3    # "xAnimator":Landroid/animation/Animator;
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    invoke-direct {p0, v4, v6}, Lcom/navdy/hud/app/view/MainView;->getXPositionAnimator(Landroid/view/ViewGroup;I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 632
    .restart local v3    # "xAnimator":Landroid/animation/Animator;
    if-eqz v1, :cond_4

    .line 633
    new-array v4, v8, [Landroid/animation/Animator;

    aput-object v3, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_1

    .line 635
    :cond_4
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 617
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private postNotificationCollapseEvent(Z)V
    .locals 5
    .param p1, "starting"    # Z

    .prologue
    .line 668
    const/4 v1, 0x0

    .line 669
    .local v1, "s":Ljava/lang/String;
    const/4 v2, 0x0

    .line 670
    .local v2, "t":Lcom/navdy/hud/app/framework/notifications/NotificationType;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    .line 671
    .local v0, "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-eqz v0, :cond_0

    .line 672
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v1

    .line 673
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v2

    .line 675
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    sget-object v4, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v3, p1, v1, v2, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 676
    return-void
.end method

.method private rightCollapse()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    iget v1, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/view/MainView;->getXPositionAnimator(Landroid/view/ViewGroup;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method private rightExpand()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    iget v1, p0, Lcom/navdy/hud/app/view/MainView;->rightPanelStart:I

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/view/MainView;->getXPositionAnimator(Landroid/view/ViewGroup;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addNotificationExtensionView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 533
    sget-object v2, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "addNotificationExtensionView"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 534
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getNotificationExtensionView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 535
    .local v0, "notificationExtensionView":Landroid/view/ViewGroup;
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 536
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x10b0000

    invoke-static {v2, v3}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    .line 538
    .local v1, "objectAnimator":Landroid/animation/Animator;
    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 539
    new-instance v2, Lcom/navdy/hud/app/view/MainView$6;

    invoke-direct {v2, p0, v0, p1}, Lcom/navdy/hud/app/view/MainView$6;-><init>(Lcom/navdy/hud/app/view/MainView;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 556
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 557
    return-void
.end method

.method public dismissNotification()V
    .locals 4

    .prologue
    .line 509
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 510
    .local v1, "set":Landroid/animation/AnimatorSet;
    sget-object v2, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "dismissNotification"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 511
    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->getX()F

    move-result v2

    iget v3, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 512
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->rightCollapse()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/view/MainView$AnimationMode;->RIGHT_EXPAND:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/MainView;->middleAnimate(Lcom/navdy/hud/app/view/MainView$AnimationMode;)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 513
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/MainView;->showingNotificationExtension:Z

    if-eqz v2, :cond_0

    .line 514
    sget-object v2, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "hiding notification extension along with notification"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 515
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->getRemoveNotificationExtensionAnimator()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 518
    .end local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    :cond_0
    new-instance v2, Lcom/navdy/hud/app/view/MainView$5;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/MainView$5;-><init>(Lcom/navdy/hud/app/view/MainView;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 529
    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView;->animationQueue:Lcom/navdy/hud/app/ui/framework/AnimationQueue;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->startAnimation(Landroid/animation/AnimatorSet;)V

    .line 530
    return-void
.end method

.method public ejectMainLowerView()V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->ejectView()V

    .line 684
    return-void
.end method

.method public getBasePaddingTop()I
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    return v0
.end method

.method public getBoxView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getContainerView()Lcom/navdy/hud/app/view/ContainerView;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    return-object v0
.end method

.method public getExpandedNotificationCoverView()Landroid/view/View;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->expandedNotificationCoverView:Landroid/view/View;

    return-object v0
.end method

.method public getExpandedNotificationView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->expandedNotificationView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getNotificationColorVisibility()I
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getNotificationExtensionView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationExtensionView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    return-object v0
.end method

.method public getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    return-object v0
.end method

.method public getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    return-object v0
.end method

.method public getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->systemTray:Lcom/navdy/hud/app/ui/component/SystemTrayView;

    return-object v0
.end method

.method public getToastView()Lcom/navdy/hud/app/view/ToastView;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->toastView:Lcom/navdy/hud/app/view/ToastView;

    return-object v0
.end method

.method public getVerticalOffset()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/navdy/hud/app/view/MainView;->verticalOffset:I

    return v0
.end method

.method public injectMainLowerView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 679
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->injectView(Landroid/view/View;)V

    .line 680
    return-void
.end method

.method public isExpandedNotificationViewShowing()Z
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->expandedNotificationView:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMainLowerViewVisible()Z
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->mainLowerView:Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMainUIShrunk()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationExpanding:Z

    if-nez v0, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->isNotificationViewShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationCollapsing:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotificationCollapsing()Z
    .locals 1

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationCollapsing:Z

    return v0
.end method

.method public isNotificationExpanding()Z
    .locals 1

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/MainView;->isNotificationExpanding:Z

    return v0
.end method

.method public isNotificationViewShowing()Z
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/NotificationView;->getX()F

    move-result v0

    iget v1, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScreenAnimating()Z
    .locals 1

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/MainView;->isScreenAnimating:Z

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 8

    .prologue
    .line 353
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 354
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->takeView(Ljava/lang/Object;)V

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->preferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    .line 358
    sget-boolean v0, Lcom/navdy/hud/app/settings/HUDSettings;->USE_ADAPTIVE_AUTOBRIGHTNESS:Z

    if-eqz v0, :cond_2

    .line 359
    new-instance v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .line 360
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "screen.brightness"

    const-string v5, "screen.auto_brightness"

    const-string v6, "screen.auto_brightness_adj"

    const-string v7, "screen.led_brightness"

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->brightnessControl:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 370
    :cond_1
    :goto_0
    return-void

    .line 364
    :cond_2
    new-instance v0, Lcom/navdy/hud/app/settings/BrightnessControl;

    .line 365
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "screen.brightness"

    const-string v5, "screen.auto_brightness"

    const-string v6, "screen.auto_brightness_adj"

    const-string v7, "screen.led_brightness"

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/settings/BrightnessControl;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->brightnessControl:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 374
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->dropView(Landroid/view/View;)V

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->preferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView;->brightnessControl:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/MainView;->brightnessControl:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 383
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 338
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 339
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 341
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 342
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/MainView;->addDebugMarkers(Landroid/content/Context;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setToastView(Lcom/navdy/hud/app/view/ToastView;)V

    .line 346
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V

    .line 347
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView;->notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V

    .line 349
    :cond_1
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 445
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 450
    const/4 v0, 0x0

    return v0
.end method

.method public removeNotificationExtensionView()V
    .locals 5

    .prologue
    .line 560
    sget-object v2, Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addNotificationExtensionView, showing extension ? : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/view/MainView;->showingNotificationExtension:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 561
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getNotificationExtensionView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 562
    .local v0, "notificationExtension":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 563
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/MainView;->showingNotificationExtension:Z

    if-eqz v2, :cond_1

    .line 565
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->getRemoveNotificationExtensionAnimator()Landroid/animation/Animator;

    move-result-object v1

    .line 566
    .local v1, "objectAnimator":Landroid/animation/Animator;
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 572
    .end local v1    # "objectAnimator":Landroid/animation/Animator;
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView;->notificationExtensionView:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 569
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0
.end method

.method public setDisplayFormat(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)V
    .locals 2
    .param p1, "format"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .prologue
    .line 298
    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView;->format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    if-eq p1, v1, :cond_0

    .line 299
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView;->format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 300
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    if-ne p1, v1, :cond_1

    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->compactScale()F

    move-result v0

    .line 301
    .local v0, "scale":F
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/MainView;->setScaleX(F)V

    .line 302
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/MainView;->setScaleY(F)V

    .line 304
    .end local v0    # "scale":F
    :cond_0
    return-void

    .line 300
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public setNotificationColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 649
    return-void
.end method

.method public setNotificationColorVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 652
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView;->notificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setVisibility(I)V

    .line 653
    return-void
.end method

.method public setVerticalOffset(I)V
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 319
    iget v3, p0, Lcom/navdy/hud/app/view/MainView;->verticalOffset:I

    if-eq p1, v3, :cond_0

    .line 320
    iput p1, p0, Lcom/navdy/hud/app/view/MainView;->verticalOffset:I

    .line 322
    iget v3, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 323
    iget v3, p0, Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I

    iget v4, p0, Lcom/navdy/hud/app/view/MainView;->verticalOffset:I

    add-int v1, v3, v4

    .line 324
    .local v1, "newPadding":I
    if-ltz v1, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MainView;->getBoxView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 326
    .local v0, "boxView":Landroid/widget/FrameLayout;
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 327
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 328
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 333
    .end local v0    # "boxView":Landroid/widget/FrameLayout;
    .end local v1    # "newPadding":I
    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public showNotification(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;)V
    .locals 7
    .param p1, "notifId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 482
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 483
    .local v1, "set":Landroid/animation/AnimatorSet;
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 484
    .local v2, "set2":Landroid/animation/AnimatorSet;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    invoke-direct {p0}, Lcom/navdy/hud/app/view/MainView;->rightExpand()Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v3, v5

    sget-object v4, Lcom/navdy/hud/app/view/MainView$AnimationMode;->MOVE_LEFT_SHRINK:Lcom/navdy/hud/app/view/MainView$AnimationMode;

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/view/MainView;->middleAnimate(Lcom/navdy/hud/app/view/MainView$AnimationMode;)Landroid/animation/Animator;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 485
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/NotificationView;->getX()F

    move-result v3

    iget v4, p0, Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 486
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    sget-object v4, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v3, v6, p1, p2, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 487
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    sget-object v4, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v3, v5, p1, p2, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 506
    :goto_0
    return-void

    .line 491
    :cond_0
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 493
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 494
    .local v0, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    new-instance v3, Lcom/navdy/hud/app/view/MainView$4;

    invoke-direct {v3, p0, p1, p2}, Lcom/navdy/hud/app/view/MainView$4;-><init>(Lcom/navdy/hud/app/view/MainView;Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;)V

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 505
    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView;->animationQueue:Lcom/navdy/hud/app/ui/framework/AnimationQueue;

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->startAnimation(Landroid/animation/AnimatorSet;)V

    goto :goto_0
.end method
