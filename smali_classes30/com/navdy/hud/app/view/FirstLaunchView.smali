.class public Lcom/navdy/hud/app/view/FirstLaunchView;
.super Landroid/widget/RelativeLayout;
.source "FirstLaunchView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public firstLaunchLogo:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0180
    .end annotation
.end field

.field presenter:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/FirstLaunchView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/FirstLaunchView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/FirstLaunchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/FirstLaunchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/FirstLaunchView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/hud/app/view/FirstLaunchView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onAttachToWindow"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 57
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 58
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/FirstLaunchView;->presenter:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/view/FirstLaunchView;->presenter:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 62
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/view/FirstLaunchView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDetachToWindow"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 68
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/view/FirstLaunchView;->presenter:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/view/FirstLaunchView;->presenter:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 72
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 51
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 52
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method
