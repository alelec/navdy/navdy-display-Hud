.class Lcom/navdy/hud/app/view/ChoicesView$2;
.super Ljava/lang/Object;
.source "ChoicesView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ChoicesView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ChoicesView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ChoicesView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ChoicesView;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 93
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 94
    .local v1, "hitRect":Landroid/graphics/Rect;
    new-array v3, v8, [I

    .line 95
    .local v3, "myPos":[I
    new-array v5, v8, [I

    .line 96
    .local v5, "parentPos":[I
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-virtual {v8, v3}, Lcom/navdy/hud/app/view/ChoicesView;->getLocationInWindow([I)V

    .line 100
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/ChoicesView;->getItemCount()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 101
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-virtual {v8, v2}, Lcom/navdy/hud/app/view/ChoicesView;->getItemAt(I)Landroid/view/View;

    move-result-object v0

    .line 102
    .local v0, "childView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 103
    .local v4, "parent":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 104
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    iget v8, v8, Lcom/navdy/hud/app/view/ChoicesView;->lastTouchX:I

    aget v9, v5, v11

    aget v10, v3, v11

    sub-int/2addr v9, v10

    sub-int v6, v8, v9

    .line 105
    .local v6, "touchX":I
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    iget v8, v8, Lcom/navdy/hud/app/view/ChoicesView;->lastTouchY:I

    aget v9, v5, v12

    aget v10, v3, v12

    sub-int/2addr v9, v10

    sub-int v7, v8, v9

    .line 107
    .local v7, "touchY":I
    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 108
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 109
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/ChoicesView;->getSelectedItem()I

    move-result v8

    if-ne v2, v8, :cond_1

    .line 110
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    iget-object v9, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    iget-boolean v9, v9, Lcom/navdy/hud/app/view/ChoicesView;->animateItemOnExecution:Z

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/view/ChoicesView;->executeSelectedItem(Z)V

    .line 117
    .end local v0    # "childView":Landroid/view/View;
    .end local v4    # "parent":Landroid/view/View;
    .end local v6    # "touchX":I
    .end local v7    # "touchY":I
    :cond_0
    :goto_1
    return-void

    .line 112
    .restart local v0    # "childView":Landroid/view/View;
    .restart local v4    # "parent":Landroid/view/View;
    .restart local v6    # "touchX":I
    .restart local v7    # "touchY":I
    :cond_1
    iget-object v8, p0, Lcom/navdy/hud/app/view/ChoicesView$2;->this$0:Lcom/navdy/hud/app/view/ChoicesView;

    invoke-virtual {v8, v2}, Lcom/navdy/hud/app/view/ChoicesView;->setSelectedItem(I)V

    goto :goto_1

    .line 100
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
