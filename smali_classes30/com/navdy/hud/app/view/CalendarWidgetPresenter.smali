.class public Lcom/navdy/hud/app/view/CalendarWidgetPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "CalendarWidgetPresenter.java"


# static fields
.field private static final MILLISECONDS_IN_A_DAY:J = 0x5265c00L

.field private static final REFRESH_INTERVAL:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private calendarGaugeName:Ljava/lang/String;

.field calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;

.field private context:Landroid/content/Context;

.field private formattedText:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private nextEventExists:Z

.field private paddingLeft:I

.field private refreshRunnable:Ljava/lang/Runnable;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->REFRESH_INTERVAL:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 48
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->context:Landroid/content/Context;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->paddingLeft:I

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09030d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->calendarGaugeName:Ljava/lang/String;

    .line 52
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCalendarManager()Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->handler:Landroid/os/Handler;

    .line 54
    new-instance v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter$1;-><init>(Lcom/navdy/hud/app/view/CalendarWidgetPresenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshRunnable:Ljava/lang/Runnable;

    .line 60
    return-void
.end method

.method private formatTime(J)Ljava/lang/String;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .local v0, "amPmMarker":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3, v0}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "formattedTime":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .end local v1    # "formattedTime":Ljava/lang/String;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const-string v0, "CALENDAR_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->calendarGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public onCalendarManagerEvent(Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 174
    sget-object v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$framework$calendar$CalendarManager$CalendarManagerEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 179
    :goto_0
    return-void

    .line 176
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshCalendarEvent()V

    goto :goto_0

    .line 174
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClockChanged(Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;)V
    .locals 0
    .param p1, "updateClock"    # Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshCalendarEvent()V

    .line 170
    return-void
.end method

.method public refreshCalendarEvent()V
    .locals 18

    .prologue
    .line 83
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    invoke-virtual {v14}, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->getCalendarEvents()Ljava/util/List;

    move-result-object v2

    .line 84
    .local v2, "calendarEvents":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/calendars/CalendarEvent;>;"
    const/4 v12, 0x0

    .line 85
    .local v12, "nextEvent":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 86
    .local v4, "currentTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v14}, Lcom/navdy/hud/app/common/TimeHelper;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v13

    .line 87
    .local v13, "timeZone":Ljava/util/TimeZone;
    invoke-static {v13}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 88
    .local v3, "currentTimeCalendar":Ljava/util/Calendar;
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v14}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 89
    if-eqz v2, :cond_1

    .line 90
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    .line 91
    .local v6, "event":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    sget-object v14, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Event :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", From : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", To : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", All day : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 92
    iget-object v14, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    const-wide/16 v16, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 93
    .local v8, "eventEndTime":J
    iget-object v14, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    const-wide/16 v16, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 94
    .local v10, "eventStartTime":J
    invoke-static {v13}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v7

    .line 95
    .local v7, "eventStartTimeCalendar":Ljava/util/Calendar;
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v14}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 96
    cmp-long v14, v8, v4

    if-lez v14, :cond_0

    const/4 v14, 0x1

    .line 97
    invoke-virtual {v3, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    move/from16 v0, v16

    if-ne v14, v0, :cond_0

    const/4 v14, 0x6

    .line 98
    invoke-virtual {v3, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->get(I)I

    move-result v16

    move/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 100
    iget-object v14, v6, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    if-nez v14, :cond_2

    .line 102
    move-object v12, v6

    .line 113
    .end local v6    # "event":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    .end local v7    # "eventStartTimeCalendar":Ljava/util/Calendar;
    .end local v8    # "eventEndTime":J
    .end local v10    # "eventStartTime":J
    :cond_1
    if-eqz v12, :cond_3

    .line 114
    sget-object v14, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Next event shown : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v12, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 115
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->nextEventExists:Z

    .line 116
    iget-object v14, v12, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->formatTime(J)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->formattedText:Ljava/lang/String;

    .line 117
    iget-object v14, v12, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->title:Ljava/lang/String;

    .line 124
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v14, v15}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshRunnable:Ljava/lang/Runnable;

    sget v16, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->REFRESH_INTERVAL:I

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-virtual/range {v14 .. v17}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->reDraw()V

    .line 127
    return-void

    .line 104
    .restart local v6    # "event":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    .restart local v7    # "eventStartTimeCalendar":Ljava/util/Calendar;
    .restart local v8    # "eventEndTime":J
    .restart local v10    # "eventStartTime":J
    :cond_2
    if-nez v12, :cond_0

    .line 107
    move-object v12, v6

    goto/16 :goto_0

    .line 119
    .end local v6    # "event":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    .end local v7    # "eventStartTimeCalendar":Ljava/util/Calendar;
    .end local v8    # "eventEndTime":J
    .end local v10    # "eventStartTime":J
    :cond_3
    sget-object v14, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "No event shown"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 120
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->nextEventExists:Z

    .line 121
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->formattedText:Ljava/lang/String;

    .line 122
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->title:Ljava/lang/String;

    goto :goto_1
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 64
    if-eqz p1, :cond_2

    .line 65
    if-eqz p2, :cond_0

    const-string v3, "EXTRA_GRAVITY"

    invoke-virtual {p2, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 66
    .local v0, "gravity":I
    :goto_0
    const v3, 0x7f030004

    invoke-virtual {p1, v3}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 67
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    iget v1, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->paddingLeft:I

    .line 68
    .local v1, "paddingLeft":I
    :goto_1
    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 69
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 70
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshCalendarEvent()V

    .line 74
    .end local v0    # "gravity":I
    .end local v1    # "paddingLeft":I
    :goto_2
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 75
    return-void

    :cond_0
    move v0, v2

    .line 65
    goto :goto_0

    .restart local v0    # "gravity":I
    :cond_1
    move v1, v2

    .line 67
    goto :goto_1

    .line 72
    .end local v0    # "gravity":I
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method protected updateGauge()V
    .locals 4

    .prologue
    .line 142
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-nez v2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    const v3, 0x7f0e00bd

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 146
    .local v0, "dateTextView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    const v3, 0x7f0e00be

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 147
    .local v1, "titleTextView":Landroid/widget/TextView;
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->nextEventExists:Z

    if-eqz v2, :cond_1

    .line 148
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->formattedText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v2, p0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 151
    :cond_1
    const v2, 0x7f090293

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 152
    const v2, 0x7f0901da

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
