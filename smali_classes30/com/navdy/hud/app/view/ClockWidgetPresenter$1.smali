.class Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;
.super Ljava/lang/Object;
.source "ClockWidgetPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ClockWidgetPresenter;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTime()V
    invoke-static {v0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->access$000(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)V

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    # getter for: Lcom/navdy/hud/app/view/ClockWidgetPresenter;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->access$300(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    # getter for: Lcom/navdy/hud/app/view/ClockWidgetPresenter;->updateTimeRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->access$100(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/ClockWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    # invokes: Lcom/navdy/hud/app/view/ClockWidgetPresenter;->nextUpdateInterval()I
    invoke-static {v2}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;->access$200(Lcom/navdy/hud/app/view/ClockWidgetPresenter;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 84
    return-void
.end method
