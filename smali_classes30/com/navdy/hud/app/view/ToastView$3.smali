.class Lcom/navdy/hud/app/view/ToastView$3;
.super Ljava/lang/Object;
.source "ToastView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ToastView;->animateOut(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ToastView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 228
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "toast:animationOut"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;
    invoke-static {v0}, Lcom/navdy/hud/app/view/ToastView;->access$300(Lcom/navdy/hud/app/view/ToastView;)Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v1

    # setter for: Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/ToastView;->access$302(Lcom/navdy/hud/app/view/ToastView;Lcom/navdy/hud/app/ui/activity/Main;)Lcom/navdy/hud/app/ui/activity/Main;

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;
    invoke-static {v0}, Lcom/navdy/hud/app/view/ToastView;->access$300(Lcom/navdy/hud/app/view/ToastView;)Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/ToastView;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 235
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;
    invoke-static {v0}, Lcom/navdy/hud/app/view/ToastView;->access$300(Lcom/navdy/hud/app/view/ToastView;)Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->setInputFocus()V

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/ToastView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/view/ToastView$3;->this$0:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ToastView;->revertToOriginal()V

    .line 240
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->clear()V

    .line 241
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->setToastDisplayFlag(Z)V

    .line 242
    return-void
.end method
