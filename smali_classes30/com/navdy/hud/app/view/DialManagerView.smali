.class public Lcom/navdy/hud/app/view/DialManagerView;
.super Landroid/widget/RelativeLayout;
.source "DialManagerView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final CONNECTED_TIMEOUT:I = 0x7530

.field public static final DIAL_CONNECTED:I = 0x5

.field public static final DIAL_CONNECTING:I = 0x4

.field public static final DIAL_CONNECTION_FAILED:I = 0x6

.field public static final DIAL_PAIRED:I = 0x3

.field public static final DIAL_PAIRING:I = 0x1

.field public static final DIAL_SEARCHING:I = 0x2

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final POST_OTA_TIMEOUT:J

.field private static final POS_DISMISS:I = 0x0

.field private static final RESTART_INTERVAL:I = 0x7d0

.field private static firstLaunchVideoUri:Landroid/net/Uri;

.field private static repairVideoUri:Landroid/net/Uri;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private batteryLevel:I

.field private completeCount:I

.field private connectedChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private connectedTimeoutRunnable:Ljava/lang/Runnable;

.field public connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0179
    .end annotation
.end field

.field private dialName:Ljava/lang/String;

.field private firstTime:Z

.field private foundDial:Z

.field private handler:Landroid/os/Handler;

.field private incrementalVersion:I

.field private initialState:I

.field presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public rePairTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0178
    .end annotation
.end field

.field private registered:Z

.field private restartScanRunnable:Ljava/lang/Runnable;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinner:Landroid/animation/ObjectAnimator;

.field private state:I

.field public videoContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0176
    .end annotation
.end field

.field private videoUri:Landroid/net/Uri;

.field public videoView:Landroid/widget/VideoView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0177
    .end annotation
.end field

.field private videoViewInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 67
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/DialManagerView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 76
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/view/DialManagerView;->POST_OTA_TIMEOUT:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/DialManagerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/DialManagerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    iput v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->initialState:I

    .line 85
    iput v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->state:I

    .line 118
    const-string v1, "Navdy Dial"

    iput-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    .line 124
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    .line 129
    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$1;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    .line 137
    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$2;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 152
    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$3;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->restartScanRunnable:Ljava/lang/Runnable;

    .line 172
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 173
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 175
    sget-object v1, Lcom/navdy/hud/app/view/DialManagerView;->firstLaunchVideoUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 176
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "packageName":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/raw/dial_pairing_flow"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/view/DialManagerView;->firstLaunchVideoUri:Landroid/net/Uri;

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/raw/dial_repairing_flow"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/view/DialManagerView;->repairVideoUri:Landroid/net/Uri;

    .line 183
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/DialManagerView;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 184
    return-void

    .line 181
    :cond_1
    invoke-static {p1}, Lcom/navdy/hud/app/HudApplication;->setContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/DialManagerView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/DialManagerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->completeCount:I

    return v0
.end method

.method static synthetic access$204(Lcom/navdy/hud/app/view/DialManagerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->completeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->completeCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/DialManagerView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/DialManagerView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private clearConnectedTimeout()V
    .locals 2

    .prologue
    .line 594
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 595
    .local v0, "handler":Landroid/os/Handler;
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 596
    return-void
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->DialManagerScreen:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 193
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->initialState:I

    .line 194
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->batteryLevel:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 198
    return-void

    .line 196
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private isInfoViewVisible()Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    .line 600
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetConnectedTimeout()V
    .locals 4

    .prologue
    .line 588
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 589
    .local v0, "handler":Landroid/os/Handler;
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 590
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 591
    return-void
.end method

.method private resetConnectedView()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    .line 424
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 425
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c002b

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 426
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c002c

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 429
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 431
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 432
    .local v0, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 434
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v3, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 435
    return-void
.end method

.method private setConnectingUI()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 633
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 634
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const v1, 0x7f0200fc

    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 635
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 636
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 637
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 638
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const v1, 0x7f0201e5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 639
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->startSpinner()V

    .line 641
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 642
    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 326
    iget v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->state:I

    if-eq v0, p1, :cond_0

    .line 327
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Switching to state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 329
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/view/DialManagerView;->state:I

    .line 330
    iget v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->state:I

    packed-switch v0, :pswitch_data_0

    .line 350
    :goto_0
    return-void

    .line 332
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialPairingState()V

    goto :goto_0

    .line 335
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialSearchingState()V

    goto :goto_0

    .line 338
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showPairedState()V

    goto :goto_0

    .line 341
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->setConnectingUI()V

    goto :goto_0

    .line 344
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialConnectedState()V

    goto :goto_0

    .line 347
    :pswitch_5
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showConnectionFailedState()V

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setVideoURI(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 411
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoUri:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 412
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoUri:Landroid/net/Uri;

    .line 421
    return-void

    .line 413
    :cond_1
    if-nez p1, :cond_0

    .line 414
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private showConnectionFailedState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 645
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 646
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const v1, 0x7f0200fc

    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 647
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const v1, 0x7f0200f6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 649
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 651
    return-void
.end method

.method private showDialConnectedState()V
    .locals 18

    .prologue
    .line 438
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 440
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 441
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 442
    .local v9, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const v14, 0x7f0200fc

    const/4 v15, 0x0

    sget-object v16, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual/range {v13 .. v16}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 443
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const v14, 0x7f0201e6

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 444
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 445
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 446
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    const v14, 0x7f0c002c

    invoke-virtual {v13, v3, v14}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 447
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    const v14, 0x7f0900c9

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    const v14, 0x7f0c002b

    invoke-virtual {v13, v3, v14}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 449
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    if-eqz v13, :cond_1

    .line 450
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    :goto_0
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->batteryLevel:I

    invoke-static {v13}, Lcom/navdy/hud/app/device/dial/DialManager;->getBatteryLevelCategory(I)Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    move-result-object v8

    .line 455
    .local v8, "reason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    sget-object v13, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    invoke-virtual {v8}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 458
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    const v14, 0x7f0900b8

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 476
    :goto_1
    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/view/DialManagerView;->incrementalVersion:I

    .line 478
    .local v6, "incremental":I
    const/4 v13, -0x1

    if-ne v6, v13, :cond_2

    .line 479
    const-string v4, ""

    .line 483
    .local v4, "fw":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    const v14, 0x7f0900bb

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    invoke-virtual {v9, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 486
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v13}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 487
    .local v7, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const v13, 0x7f0b0078

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    iput v13, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 489
    new-instance v2, Ljava/util/ArrayList;

    const/4 v13, 0x2

    invoke-direct {v2, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 490
    .local v2, "choiceList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v13, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v14, 0x7f0900c5

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v13, v14, v15}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 492
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v14, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v2, v15, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 493
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 495
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v13, :cond_0

    .line 496
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v13}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getHandler()Landroid/os/Handler;

    move-result-object v5

    .line 497
    .local v5, "handler":Landroid/os/Handler;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v13}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 498
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v14, 0x7530

    invoke-virtual {v5, v13, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 500
    .end local v5    # "handler":Landroid/os/Handler;
    :cond_0
    return-void

    .line 452
    .end local v2    # "choiceList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    .end local v4    # "fw":Ljava/lang/String;
    .end local v6    # "incremental":I
    .end local v7    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v8    # "reason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 461
    .restart local v8    # "reason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    :pswitch_0
    new-instance v10, Landroid/text/SpannableStringBuilder;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0900b7

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v9, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 462
    .local v10, "ss1":Landroid/text/SpannableStringBuilder;
    new-instance v13, Landroid/text/style/ImageSpan;

    const v14, 0x7f0200fd

    const/4 v15, 0x1

    invoke-direct {v13, v3, v14, v15}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 463
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 466
    .end local v10    # "ss1":Landroid/text/SpannableStringBuilder;
    :pswitch_1
    new-instance v11, Landroid/text/SpannableStringBuilder;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0900b9

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v9, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 467
    .local v11, "ss2":Landroid/text/SpannableStringBuilder;
    new-instance v13, Landroid/text/style/ImageSpan;

    const v14, 0x7f0200fe

    const/4 v15, 0x1

    invoke-direct {v13, v3, v14, v15}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v11, v13, v14, v15, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 468
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v13, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 471
    .end local v11    # "ss2":Landroid/text/SpannableStringBuilder;
    :pswitch_2
    new-instance v12, Landroid/text/SpannableStringBuilder;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0900b6

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v9, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 472
    .local v12, "ss3":Landroid/text/SpannableStringBuilder;
    new-instance v13, Landroid/text/style/ImageSpan;

    const v14, 0x7f0200fe

    const/4 v15, 0x1

    invoke-direct {v13, v3, v14, v15}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x21

    invoke-virtual/range {v12 .. v16}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 473
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 481
    .end local v12    # "ss3":Landroid/text/SpannableStringBuilder;
    .restart local v6    # "incremental":I
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "1.0."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "fw":Ljava/lang/String;
    goto/16 :goto_2

    .line 455
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showDialPairingState()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 369
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->firstTime:Z

    .line 371
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 372
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->resetConnectedView()V

    .line 374
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoViewInitialized:Z

    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoView:Landroid/widget/VideoView;

    new-instance v3, Lcom/navdy/hud/app/view/DialManagerView$5;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/view/DialManagerView$5;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 384
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoView:Landroid/widget/VideoView;

    new-instance v3, Lcom/navdy/hud/app/view/DialManagerView$6;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/view/DialManagerView$6;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 392
    iput-boolean v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoViewInitialized:Z

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->sharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "first_run_dial_video_shown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->repairVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setVideoURI(Landroid/net/Uri;)V

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->rePairTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0901cf

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->rePairTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    :goto_1
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting scanning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 406
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->startScan()V

    .line 407
    return-void

    :cond_1
    move v0, v2

    .line 369
    goto :goto_0

    .line 401
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->firstLaunchVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setVideoURI(Landroid/net/Uri;)V

    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->rePairTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private showDialSearchingState()V
    .locals 4

    .prologue
    .line 503
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->firstTime:Z

    .line 504
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->resetConnectedView()V

    .line 505
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->setConnectingUI()V

    .line 506
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting scanning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 507
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->startScan()V

    .line 508
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->restartScanRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lcom/navdy/hud/app/view/DialManagerView;->POST_OTA_TIMEOUT:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 509
    return-void
.end method

.method private showPairedState()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 675
    sget-object v3, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showPairingSuccessUI="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 676
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 677
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 678
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 679
    .local v2, "resources":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 680
    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    .line 681
    .local v1, "layoutTransition":Landroid/animation/LayoutTransition;
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 682
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const v4, 0x7f0200fc

    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 683
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 684
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const v4, 0x7f0201e6

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 685
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 686
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 687
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    const v4, 0x7f0c000a

    invoke-virtual {v3, v0, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 688
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    const v4, 0x7f0901cd

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 689
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialAddressPart(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    .line 690
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 691
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    const v4, 0x7f0901ce

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 697
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 698
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 699
    return-void

    .line 694
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showPairingSuccessUI(Ljava/lang/String;)V
    .locals 4
    .param p1, "dialName"    # Ljava/lang/String;

    .prologue
    .line 654
    iput-object p1, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    .line 655
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    .line 657
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/LocalSpeechRequest;

    new-instance v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;-><init>()V

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_CONNECTED:Ljava/lang/String;

    .line 658
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v2

    .line 659
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->language(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_NOTIFICATION:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 660
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/LocalSpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V

    .line 657
    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 662
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$8;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$8;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 672
    return-void
.end method

.method private startSpinner()V
    .locals 5

    .prologue
    .line 604
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    .line 606
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 607
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 608
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$7;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$7;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 621
    :cond_1
    return-void
.end method

.method private stopSpinner()V
    .locals 2

    .prologue
    .line 624
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 626
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 627
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->spinner:Landroid/animation/ObjectAnimator;

    .line 630
    :cond_0
    return-void
.end method

.method private stopVideo()V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setVideoURI(Landroid/net/Uri;)V

    .line 514
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopped video"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 518
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 519
    return-void

    .line 516
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "video not playing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 214
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onAttachToWindow"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 215
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v0, :cond_0

    .line 217
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 218
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 222
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->registered:Z

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->initialState:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 232
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDetachToWindow"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 233
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->clearConnectedTimeout()V

    .line 235
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->videoContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 236
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stop dial scan"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 237
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->stopScan(Z)V

    .line 238
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 241
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 242
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 244
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 245
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 247
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->registered:Z

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 252
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopSpinner()V

    .line 254
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInfoViewVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/view/DialManagerView;->firstTime:Z

    iget-boolean v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->foundDial:Z

    const/16 v3, 0x2710

    invoke-static {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendLocalyticsEvent(Landroid/os/Handler;ZZIZ)V

    .line 257
    :cond_2
    return-void
.end method

.method public onDialConnectionStatus(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 533
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInfoViewVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 585
    :goto_0
    return-void

    .line 538
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDialConnectionStatus:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->status:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 540
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->foundDial:Z

    if-eqz v2, :cond_1

    .line 541
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "already found dial"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 545
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    .line 546
    .local v0, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    iget-object v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->restartScanRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 547
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopSpinner()V

    .line 549
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status:[I

    iget-object v3, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->status:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v3}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 551
    :pswitch_0
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dial connected:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 552
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->foundDial:Z

    .line 553
    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/device/dial/DialManager;->stopScan(Z)V

    .line 554
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v1

    .line 555
    .local v1, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 556
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 557
    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 558
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "show connected state"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 559
    iget-object v2, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/DialManagerView;->showPairingSuccessUI(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 563
    .end local v1    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dial connnecting:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 564
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->stopVideo()V

    .line 565
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->setConnectingUI()V

    goto/16 :goto_0

    .line 569
    :pswitch_2
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dial connection disconnected:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 570
    iget-object v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->restartScanRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 575
    :pswitch_3
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dial connection failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 576
    const/4 v2, 0x6

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    .line 577
    iget-object v2, p0, Lcom/navdy/hud/app/view/DialManagerView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/view/DialManagerView;->restartScanRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 581
    :pswitch_4
    sget-object v2, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no dials found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 582
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialPairing()V

    goto/16 :goto_0

    .line 549
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 202
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 203
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 206
    const v2, 0x7f0e00bf

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/view/DialManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 207
    .local v1, "maxWidthLayout":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialManagerView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 208
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 209
    .local v0, "maxWidth":I
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 210
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 268
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView$9;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 315
    :cond_0
    :goto_0
    return v2

    .line 271
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->clearConnectedTimeout()V

    .line 272
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dismiss()V

    goto :goto_0

    .line 276
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInfoViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->resetConnectedTimeout()V

    .line 278
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 283
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInfoViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->resetConnectedTimeout()V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0

    .line 290
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->isInfoViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->clearConnectedTimeout()V

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->connectedView:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 297
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DialManagerView;->clearConnectedTimeout()V

    .line 298
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "pwrbtn long press in connected mode"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/DialManagerView$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/DialManagerView$4;-><init>(Lcom/navdy/hud/app/view/DialManagerView;)V

    invoke-virtual {v0, v2, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->forgetAllDials(ZLcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V

    goto :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onShowToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 523
    sget-object v0, Lcom/navdy/hud/app/view/DialManagerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got toast:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 524
    iget-object v0, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;->name:Ljava/lang/String;

    const-string v1, "dial-connect"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dismiss()V

    .line 529
    :cond_0
    return-void
.end method

.method public showDialConnected()V
    .locals 1

    .prologue
    .line 357
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    .line 358
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownBatteryLevel()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->batteryLevel:I

    .line 359
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v0, v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    iput v0, p0, Lcom/navdy/hud/app/view/DialManagerView;->incrementalVersion:I

    .line 360
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    .line 361
    return-void
.end method

.method public showDialPairing()V
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    .line 354
    return-void
.end method

.method public showDialSearching(Ljava/lang/String;)V
    .locals 1
    .param p1, "dialName"    # Ljava/lang/String;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/navdy/hud/app/view/DialManagerView;->dialName:Ljava/lang/String;

    .line 365
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/view/DialManagerView;->setState(I)V

    .line 366
    return-void
.end method
