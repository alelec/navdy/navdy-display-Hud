.class public final enum Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
.super Ljava/lang/Enum;
.source "MainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/MainView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CustomAnimationMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field public static final enum EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field public static final enum SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field public static final enum SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    new-instance v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    const-string v1, "SHRINK_LEFT"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 77
    new-instance v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 80
    new-instance v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    const-string v1, "SHRINK_MODE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->$VALUES:[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->$VALUES:[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    return-object v0
.end method
