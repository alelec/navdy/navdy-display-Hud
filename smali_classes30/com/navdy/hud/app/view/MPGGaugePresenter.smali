.class public Lcom/navdy/hud/app/view/MPGGaugePresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "MPGGaugePresenter.java"


# static fields
.field private static final GAUGE_UPDATE_INTERVAL:I = 0x3e8

.field private static final MPG_AVERAGE_TIME_WINDOW:J


# instance fields
.field private mContext:Landroid/content/Context;

.field mFuelConsumptionUnit:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00bd
    .end annotation
.end field

.field mFuelConsumptionValue:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00be
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mKmplLabel:Ljava/lang/String;

.field private mMpg:D

.field private mMpgLabel:Ljava/lang/String;

.field private mRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->MPG_AVERAGE_TIME_WINDOW:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mContext:Landroid/content/Context;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 41
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0901c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpgLabel:Ljava/lang/String;

    .line 42
    const v1, 0x7f090197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mKmplLabel:Ljava/lang/String;

    .line 43
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mHandler:Landroid/os/Handler;

    .line 44
    new-instance v1, Lcom/navdy/hud/app/view/MPGGaugePresenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/MPGGaugePresenter$1;-><init>(Lcom/navdy/hud/app/view/MPGGaugePresenter;)V

    iput-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mRunnable:Ljava/lang/Runnable;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/MPGGaugePresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MPGGaugePresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/MPGGaugePresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MPGGaugePresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "MPG_AVG_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v0

    .line 107
    .local v0, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    sget-object v1, Lcom/navdy/hud/app/view/MPGGaugePresenter$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 113
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 109
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mKmplLabel:Ljava/lang/String;

    goto :goto_0

    .line 111
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpgLabel:Ljava/lang/String;

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setCurrentMPG(D)V
    .locals 1
    .param p1, "mpgValue"    # D

    .prologue
    .line 67
    iput-wide p1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpg:D

    .line 68
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MPGGaugePresenter;->reDraw()V

    .line 69
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 55
    if-eqz p1, :cond_0

    .line 56
    const v0, 0x7f03001b

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 57
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 64
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected updateGauge()V
    .locals 6

    .prologue
    .line 78
    iget-object v3, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v3, :cond_0

    .line 79
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v2

    .line 80
    .local v2, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    const-wide/16 v0, 0x0

    .line 81
    .local v0, "runningAverageValueRounded":J
    sget-object v3, Lcom/navdy/hud/app/view/MPGGaugePresenter$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 91
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    .line 92
    iget-object v3, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mFuelConsumptionValue:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    .end local v0    # "runningAverageValueRounded":J
    .end local v2    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :cond_0
    :goto_1
    return-void

    .line 83
    .restart local v0    # "runningAverageValueRounded":J
    .restart local v2    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :pswitch_0
    iget-wide v4, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpg:D

    invoke-static {v4, v5}, Lcom/navdy/hud/app/util/ConversionUtil;->convertLpHundredKmToKMPL(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 84
    iget-object v3, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mFuelConsumptionUnit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mKmplLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 87
    :pswitch_1
    iget-wide v4, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpg:D

    invoke-static {v4, v5}, Lcom/navdy/hud/app/util/ConversionUtil;->convertLpHundredKmToMPG(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 88
    iget-object v3, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mFuelConsumptionUnit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mMpgLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/view/MPGGaugePresenter;->mFuelConsumptionValue:Landroid/widget/TextView;

    const-string v4, "- -"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
