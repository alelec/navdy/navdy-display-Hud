.class Lcom/navdy/hud/app/view/GestureLearningView$1;
.super Ljava/lang/Object;
.source "GestureLearningView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/GestureLearningView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/GestureLearningView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/GestureLearningView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/GestureLearningView;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 143
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GestureLearningView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    # setter for: Lcom/navdy/hud/app/view/GestureLearningView;->mNeutralX:F
    invoke-static {v1, v2}, Lcom/navdy/hud/app/view/GestureLearningView;->access$002(Lcom/navdy/hud/app/view/GestureLearningView;F)F

    .line 144
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/GestureLearningView;->mCenterImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/GestureLearningView;->mCenterImage:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v0, v1, v2

    .line 145
    .local v0, "y":F
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v1, v1, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setY(F)V

    .line 146
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    # getter for: Lcom/navdy/hud/app/view/GestureLearningView;->mHandIconMargin:F
    invoke-static {v2}, Lcom/navdy/hud/app/view/GestureLearningView;->access$200(Lcom/navdy/hud/app/view/GestureLearningView;)F

    move-result v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mCenterImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v3, v3, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    # setter for: Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorProgressSpan:F
    invoke-static {v1, v2}, Lcom/navdy/hud/app/view/GestureLearningView;->access$102(Lcom/navdy/hud/app/view/GestureLearningView;F)F

    .line 147
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/GestureLearningView;->mGestureProgressIndicator:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    # setter for: Lcom/navdy/hud/app/view/GestureLearningView;->mGestureIndicatorHalfWidth:F
    invoke-static {v1, v2}, Lcom/navdy/hud/app/view/GestureLearningView;->access$302(Lcom/navdy/hud/app/view/GestureLearningView;F)F

    .line 148
    iget-object v1, p0, Lcom/navdy/hud/app/view/GestureLearningView$1;->this$0:Lcom/navdy/hud/app/view/GestureLearningView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/GestureLearningView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 149
    return-void
.end method
