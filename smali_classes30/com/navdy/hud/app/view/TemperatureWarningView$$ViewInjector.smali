.class public Lcom/navdy/hud/app/view/TemperatureWarningView$$ViewInjector;
.super Ljava/lang/Object;
.source "TemperatureWarningView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/TemperatureWarningView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/TemperatureWarningView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00cc

    const-string v2, "field \'mScreenTitleText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mScreenTitleText:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0088

    const-string v2, "field \'mTitle1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mTitle1:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e0089

    const-string v2, "field \'mMainTitleText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mMainTitleText:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e008a

    const-string v2, "field \'mWarningMessage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mWarningMessage:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e00d2

    const-string v2, "field \'mChoiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 20
    const v1, 0x7f0e00d4

    const-string v2, "field \'mRightSwipe\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mRightSwipe:Landroid/widget/ImageView;

    .line 22
    const v1, 0x7f0e00cf

    const-string v2, "field \'mIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/TemperatureWarningView;->mIcon:Landroid/widget/ImageView;

    .line 24
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/TemperatureWarningView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/TemperatureWarningView;

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mScreenTitleText:Landroid/widget/TextView;

    .line 28
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mTitle1:Landroid/widget/TextView;

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mMainTitleText:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mWarningMessage:Landroid/widget/TextView;

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mRightSwipe:Landroid/widget/ImageView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/view/TemperatureWarningView;->mIcon:Landroid/widget/ImageView;

    .line 34
    return-void
.end method
