.class Lcom/navdy/hud/app/view/MainView$3;
.super Ljava/lang/Object;
.source "MainView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MainView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/MainView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MainView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 19

    .prologue
    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 222
    .local v18, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b0070

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v15, v2

    .line 223
    .local v15, "measuredHt":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v13, v2

    .line 224
    .local v13, "boxHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    div-int/lit8 v3, v15, 0x2

    div-int/lit8 v4, v13, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/MainView;->access$302(Lcom/navdy/hud/app/view/MainView;I)I

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->splitterView:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/FrameLayout$LayoutParams;

    .line 226
    .local v17, "params":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$300(Lcom/navdy/hud/app/view/MainView;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->verticalOffset:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$400(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    const v3, 0x7f0b0071

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    # setter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/MainView;->access$502(Lcom/navdy/hud/app/view/MainView;I)I

    .line 229
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    const v3, 0x7f0b0125

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    # setter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/MainView;->access$602(Lcom/navdy/hud/app/view/MainView;I)I

    .line 235
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setMainPanelWidth(I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setSidePanelWidth(I)V

    .line 238
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "side panel width="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    sub-int/2addr v3, v4

    # setter for: Lcom/navdy/hud/app/view/MainView;->rightPanelStart:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/MainView;->access$802(Lcom/navdy/hud/app/view/MainView;I)I

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    .line 241
    .local v16, "metrics":Landroid/util/DisplayMetrics;
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Density:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", dpi:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", width:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 243
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onGlobalLayout mainPanelWidth = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    .line 244
    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sidePanelWidth = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    .line 245
    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rightPanelStart = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->rightPanelStart:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$800(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " paddingTop = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    .line 246
    # getter for: Lcom/navdy/hud/app/view/MainView;->basePaddingTop:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$300(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mh ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/ContainerView;->setX(F)V

    .line 250
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "containerView x: 0"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->containerView:Lcom/navdy/hud/app/view/ContainerView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/ContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    .end local v17    # "params":Landroid/widget/FrameLayout$LayoutParams;
    check-cast v17, Landroid/widget/FrameLayout$LayoutParams;

    .line 252
    .restart local v17    # "params":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/NotificationView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    .end local v17    # "params":Landroid/widget/FrameLayout$LayoutParams;
    check-cast v17, Landroid/widget/FrameLayout$LayoutParams;

    .line 255
    .restart local v17    # "params":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notificationView:Lcom/navdy/hud/app/view/NotificationView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/NotificationView;->setX(F)V

    .line 257
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifView x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->expandedNotificationView:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setX(F)V

    .line 260
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "expand notifView x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v4}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$600(Lcom/navdy/hud/app/view/MainView;)I

    move-result v2

    const v3, 0x7f0b0087

    .line 263
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    const v3, 0x7f0b0086

    .line 264
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int v14, v2, v3

    .line 266
    .local v14, "expandX":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    int-to-float v3, v14

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setX(F)V

    .line 268
    # getter for: Lcom/navdy/hud/app/view/MainView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/MainView;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notif indicator x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingRoundSize:I

    sget v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorProgressSize:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget v7, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    sget v8, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    const/4 v9, 0x1

    sget v10, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorPadding:I

    const/4 v11, -0x1

    const/4 v12, -0x1

    invoke-virtual/range {v2 .. v12}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setProperties(IIIIIIZIII)V

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->notifScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setItemCount(I)V

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/view/MainView;->access$900(Lcom/navdy/hud/app/view/MainView;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/view/MainView$3$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/view/MainView$3$1;-><init>(Lcom/navdy/hud/app/view/MainView$3;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->createScreens()V

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v2, v2, Lcom/navdy/hud/app/view/MainView;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setInputFocus()V

    .line 293
    return-void

    .line 232
    .end local v14    # "expandX":I
    .end local v16    # "metrics":Landroid/util/DisplayMetrics;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/view/MainView$3;->this$0:Lcom/navdy/hud/app/view/MainView;

    # getter for: Lcom/navdy/hud/app/view/MainView;->mainPanelWidth:I
    invoke-static {v3}, Lcom/navdy/hud/app/view/MainView;->access$500(Lcom/navdy/hud/app/view/MainView;)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    # setter for: Lcom/navdy/hud/app/view/MainView;->sidePanelWidth:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/view/MainView;->access$602(Lcom/navdy/hud/app/view/MainView;I)I

    goto/16 :goto_0
.end method
