.class Lcom/navdy/hud/app/view/MainView$4;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "MainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MainView;->showNotification(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/MainView;

.field final synthetic val$notifId:Ljava/lang/String;

.field final synthetic val$type:Lcom/navdy/hud/app/framework/notifications/NotificationType;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MainView;Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 494
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView$4;->this$0:Lcom/navdy/hud/app/view/MainView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/MainView$4;->val$notifId:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/view/MainView$4;->val$type:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$4;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView$4;->val$notifId:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView$4;->val$type:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    sget-object v4, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 503
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$4;->this$0:Lcom/navdy/hud/app/view/MainView;

    iget-object v0, v0, Lcom/navdy/hud/app/view/MainView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/navdy/hud/app/view/MainView$4;->val$notifId:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/view/MainView$4;->val$type:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    sget-object v4, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    .line 498
    return-void
.end method
