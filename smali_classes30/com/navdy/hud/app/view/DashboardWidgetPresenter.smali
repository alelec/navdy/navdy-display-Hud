.class public abstract Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.super Ljava/lang/Object;
.source "DashboardWidgetPresenter.java"


# static fields
.field public static final EXTRA_GRAVITY:Ljava/lang/String; = "EXTRA_GRAVITY"

.field public static final EXTRA_IS_ACTIVE:Ljava/lang/String; = "EXTRA_IS_ACTIVE"

.field public static final GRAVITY_CENTER:I = 0x1

.field public static final GRAVITY_LEFT:I = 0x0

.field public static final GRAVITY_RIGHT:I = 0x2


# instance fields
.field protected isDashActive:Z

.field protected isRegistered:Z

.field protected isWidgetVisibleToUser:Z

.field protected logger:Lcom/navdy/service/library/log/Logger;

.field protected mCustomView:Landroid/view/View;

.field protected mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegistered:Z

    .line 36
    return-void
.end method


# virtual methods
.method public canDraw()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isDashActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isWidgetVisibleToUser:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getWidgetIdentifier()Ljava/lang/String;
.end method

.method public abstract getWidgetName()Ljava/lang/String;
.end method

.method public getWidgetView()Lcom/navdy/hud/app/view/DashboardWidgetView;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetActive()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isDashActive:Z

    .line 121
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->canDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "widget::onPause:"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isDashActive:Z

    .line 128
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->canDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "widget::onResume:"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->reDraw()V

    .line 133
    :cond_0
    return-void
.end method

.method public reDraw()V
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->canDraw()Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->updateGauge()V

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mCustomView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mCustomView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method protected registerToBus()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/HudApplication;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 3
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    .line 40
    iget-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v1, :cond_2

    .line 41
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getCustomView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mCustomView:Landroid/view/View;

    .line 42
    iget-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mCustomView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->mCustomView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegisteringToBusRequired()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegistered:Z

    if-nez v1, :cond_1

    .line 47
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->registerToBus()V

    .line 48
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegistered:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->reDraw()V

    .line 64
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "re":Ljava/lang/RuntimeException;
    iget-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Runtime exception registering to the bus "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    .end local v0    # "re":Ljava/lang/RuntimeException;
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegistered:Z

    if-eqz v1, :cond_1

    .line 56
    :try_start_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->unregisterToBus()V

    .line 57
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isRegistered:Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 58
    :catch_1
    move-exception v0

    .line 59
    .restart local v0    # "re":Ljava/lang/RuntimeException;
    iget-object v1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Runtime exception unregistering from the bus "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 80
    return-void
.end method

.method public setWidgetVisibleToUser(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->isWidgetVisibleToUser:Z

    .line 137
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->canDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "widget::visible:reDraw"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->reDraw()V

    .line 142
    :cond_0
    return-void
.end method

.method protected unregisterToBus()V
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/HudApplication;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method protected abstract updateGauge()V
.end method
