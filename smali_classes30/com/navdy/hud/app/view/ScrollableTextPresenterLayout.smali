.class public Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;
.super Landroid/widget/RelativeLayout;
.source "ScrollableTextPresenterLayout.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
.implements Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field public static final TAG_BACK:I


# instance fields
.field bottomScrub:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e010c
    .end annotation
.end field

.field mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field mContentNeedsScrolling:Z

.field private mContentReachedBottom:Z

.field private mContentReachedTop:Z

.field private mCount:I

.field private mCurrentItem:I

.field mMainImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0112
    .end annotation
.end field

.field mMainTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00cc
    .end annotation
.end field

.field mMessageText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e010a
    .end annotation
.end field

.field mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013b
    .end annotation
.end field

.field mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e013c
    .end annotation
.end field

.field mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0109
    .end annotation
.end field

.field mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mTextContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field mTitleText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00c3
    .end annotation
.end field

.field private scrollColor:I

.field topScrub:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e010b
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTextContents:Ljava/util/ArrayList;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    .line 97
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 99
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->displayScrollIndicator(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->layoutScrollIndicator()V

    return-void
.end method

.method private displayScrollIndicator(Z)V
    .locals 3
    .param p1, "initial"    # Z

    .prologue
    .line 191
    iget v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCount:I

    if-lez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setVisibility(I)V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    iget v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->scrollColor:I

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(II)V

    .line 195
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-eqz v0, :cond_2

    .line 196
    if-eqz p1, :cond_1

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$2;-><init>(Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 210
    :goto_0
    return-void

    .line 205
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->layoutScrollIndicator()V

    goto :goto_0

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private handleLeft()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 298
    const/4 v0, 0x0

    .line 299
    .local v0, "handled":Z
    iget-boolean v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-eqz v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/ObservableScrollView;->arrowScroll(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedTop:Z

    if-nez v2, :cond_2

    move v0, v1

    .line 302
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 303
    iget v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    if-lez v2, :cond_1

    .line 304
    iget v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2, v1, v1}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setCurrentItem(IZZ)V

    .line 307
    :cond_1
    return-void

    .line 300
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleRight()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310
    const/4 v0, 0x0

    .line 311
    .local v0, "handled":Z
    iget-boolean v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-eqz v3, :cond_0

    .line 312
    iget-object v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v4, 0x82

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->arrowScroll(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedBottom:Z

    if-nez v3, :cond_2

    move v0, v1

    .line 314
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 315
    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    iget v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCount:I

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_1

    .line 316
    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3, v1, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setCurrentItem(IZZ)V

    .line 319
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 312
    goto :goto_0
.end method

.method private layoutScrollIndicator()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 213
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemPos(I)Landroid/graphics/RectF;

    move-result-object v1

    .line 214
    .local v1, "rectF":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 215
    iget v2, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    iget v2, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setCurrentItem(I)V

    .line 219
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 220
    .local v0, "parent":Landroid/view/View;
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getX()F

    move-result v2

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorLeftPadding:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setX(F)V

    .line 221
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getY()F

    move-result v2

    iget v3, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorCircleSize:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorHeight:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setY(F)V

    .line 222
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setCurrentItem(IZZ)V
    .locals 6
    .param p1, "item"    # I
    .param p2, "animate"    # Z
    .param p3, "previous"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 155
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCount:I

    if-lt p1, v2, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iput p1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    .line 159
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTextContents:Ljava/util/ArrayList;

    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 160
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->needsScrollLayout(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    .line 161
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/ObservableScrollView;->fullScroll(I)Z

    .line 162
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMessageText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->topScrub:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->bottomScrub:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 168
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedTop:Z

    .line 169
    iput-boolean v5, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedBottom:Z

    .line 170
    if-eqz p2, :cond_3

    .line 171
    iget-object v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    iget v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->scrollColor:I

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 172
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    if-eqz v0, :cond_2

    .line 173
    new-instance v2, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$1;-><init>(Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;)V

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 180
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 182
    :cond_2
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->displayScrollIndicator(Z)V

    goto :goto_0

    .line 185
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    :cond_3
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->displayScrollIndicator(Z)V

    goto :goto_0
.end method


# virtual methods
.method public executeItem(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 329
    packed-switch p2, :pswitch_data_0

    .line 333
    :goto_0
    return-void

    .line 331
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mPresenter:Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->hideTips()V

    goto :goto_0

    .line 329
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 338
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBottom()V
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-nez v0, :cond_0

    .line 246
    :goto_0
    return-void

    .line 243
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedTop:Z

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedBottom:Z

    .line 245
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->onPosChange(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 142
    return-void
.end method

.method protected onFinishInflate()V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v9, -0x1

    const/4 v3, 0x0

    .line 103
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 104
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setHighlightPersistent(Z)V

    .line 106
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d009f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->scrollColor:I

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/view/ObservableScrollView;->setScrollListener(Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;)V

    .line 108
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v0, v1, v11, v3, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainImageView:Landroid/widget/ImageView;

    const v1, 0x7f0201da

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainTitleText:Landroid/widget/TextView;

    const v1, 0x7f090112

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingRoundSize:I

    sget v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorProgressSize:I

    sget v5, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    sget v6, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorWhite:I

    sget v8, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->scrollingIndicatorPadding:I

    move v4, v3

    move v10, v9

    invoke-virtual/range {v0 .. v10}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setProperties(IIIIIIZIII)V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setItemCount(I)V

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    iget v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->scrollColor:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setBackgroundColor(I)V

    .line 129
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 283
    sget-object v1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$3;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 294
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 285
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->handleLeft()V

    goto :goto_0

    .line 288
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->handleRight()V

    goto :goto_0

    .line 291
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPosChange(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    const/16 v1, 0x64

    .line 262
    if-gtz p1, :cond_1

    .line 263
    const/4 p1, 0x1

    .line 267
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getCurrentItem()I

    move-result v0

    if-ne v0, p1, :cond_2

    .line 274
    :goto_1
    return-void

    .line 264
    :cond_1
    if-le p1, v1, :cond_0

    .line 265
    const/16 p1, 0x64

    goto :goto_0

    .line 270
    :cond_2
    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    if-ne p1, v1, :cond_3

    .line 273
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setCurrentItem(I)V

    goto :goto_1
.end method

.method public onScroll(IIII)V
    .locals 8
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    const/4 v4, 0x0

    .line 250
    iget-boolean v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-nez v3, :cond_0

    .line 259
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->topScrub:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedTop:Z

    .line 255
    iput-boolean v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedBottom:Z

    .line 256
    iget-object v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/ObservableScrollView;->getHeight()I

    move-result v4

    sub-int v2, v3, v4

    .line 257
    .local v2, "range":I
    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    int-to-double v6, v2

    div-double v0, v4, v6

    .line 258
    .local v0, "pos":D
    double-to-int v3, v0

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->onPosChange(I)V

    goto :goto_0
.end method

.method public onTop()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 229
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentNeedsScrolling:Z

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->topScrub:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 233
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedTop:Z

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mContentReachedBottom:Z

    .line 235
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->onPosChange(I)V

    goto :goto_0
.end method

.method public setTextContents([Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "textContents"    # [Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTextContents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 146
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, p1, v1

    .line 147
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTextContents:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "text":Ljava/lang/CharSequence;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTextContents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCount:I

    .line 150
    iget-object v1, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget v3, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCount:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setItemCount(I)V

    .line 151
    invoke-direct {p0, v2, v2, v2}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setCurrentItem(IZZ)V

    .line 152
    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 134
    if-nez p1, :cond_0

    .line 135
    iget v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mCurrentItem:I

    invoke-direct {p0, v0, v1, v1}, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->setCurrentItem(IZZ)V

    .line 137
    :cond_0
    return-void
.end method
