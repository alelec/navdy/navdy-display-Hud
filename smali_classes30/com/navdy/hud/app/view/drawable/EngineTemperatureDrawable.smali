.class public final Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "EngineTemperatureDrawable.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u000e\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;",
        "Lcom/navdy/hud/app/view/drawable/GaugeDrawable;",
        "context",
        "Landroid/content/Context;",
        "stateColorsResId",
        "",
        "(Landroid/content/Context;I)V",
        "mBackgroundColor",
        "mFuelGaugeWidth",
        "mLeftOriented",
        "",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "setLeftOriented",
        "leftOriented",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private mBackgroundColor:I

.field private mFuelGaugeWidth:I

.field private mLeftOriented:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "stateColorsResId"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;II)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mFuelGaugeWidth:I

    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mColorTable:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mBackgroundColor:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v10, 0x5a

    const/4 v13, 0x1

    const/high16 v12, 0x40a00000    # 5.0f

    const/4 v4, 0x0

    .line 30
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 31
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 32
    .local v6, "bounds":Landroid/graphics/Rect;
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    if-eqz v0, :cond_3

    .line 34
    new-instance v1, Landroid/graphics/RectF;

    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v2, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v3, v5

    int-to-float v3, v3

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-direct {v1, v0, v2, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 37
    .local v1, "fuelGaugeArcBounds":Landroid/graphics/RectF;
    :goto_0
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mFuelGaugeWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mFuelGaugeWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mFuelGaugeWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mBackgroundColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    if-eqz v0, :cond_4

    move v8, v10

    .line 45
    .local v8, "startAngle":I
    :goto_1
    const/16 v9, 0xb4

    .line 46
    .local v9, "sweepAngle":I
    add-int v7, v8, v9

    .line 47
    .local v7, "endAngle":I
    if-eqz p1, :cond_0

    int-to-float v2, v8

    int-to-float v3, v9

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mDefaultColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mValue:F

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mMinValue:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mMaxValue:F

    iget v3, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mMinValue:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    int-to-float v2, v9

    mul-float/2addr v0, v2

    float-to-int v11, v0

    .line 52
    .local v11, "valueSweepAngle":I
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    if-eqz v0, :cond_5

    .line 53
    .local v10, "valueStartAngle":I
    :goto_2
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    .line 54
    if-ne v0, v13, :cond_6

    if-eqz p1, :cond_1

    int-to-float v2, v10

    int-to-float v0, v11

    sub-float v3, v0, v12

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 56
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    .line 62
    if-ne v0, v13, :cond_7

    if-eqz p1, :cond_2

    int-to-float v0, v10

    int-to-float v2, v11

    add-float/2addr v2, v0

    const/high16 v3, -0x3f600000    # -5.0f

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 64
    :cond_2
    :goto_4
    return-void

    .line 36
    .end local v1    # "fuelGaugeArcBounds":Landroid/graphics/RectF;
    .end local v7    # "endAngle":I
    .end local v8    # "startAngle":I
    .end local v9    # "sweepAngle":I
    .end local v10    # "valueStartAngle":I
    .end local v11    # "valueSweepAngle":I
    :cond_3
    new-instance v1, Landroid/graphics/RectF;

    iget v0, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget v2, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-direct {v1, v0, v2, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v1    # "fuelGaugeArcBounds":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 44
    :cond_4
    const/16 v8, 0x10e

    goto :goto_1

    .line 52
    .restart local v7    # "endAngle":I
    .restart local v8    # "startAngle":I
    .restart local v9    # "sweepAngle":I
    .restart local v11    # "valueSweepAngle":I
    :cond_5
    rsub-int v10, v11, 0x1c2

    goto :goto_2

    .line 55
    .restart local v10    # "valueStartAngle":I
    :cond_6
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    int-to-float v0, v10

    add-float v2, v0, v12

    int-to-float v3, v11

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_3

    .line 63
    :cond_7
    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    int-to-float v2, v10

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method public final setLeftOriented(Z)V
    .locals 0
    .param p1, "leftOriented"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;->mLeftOriented:Z

    .line 27
    return-void
.end method
