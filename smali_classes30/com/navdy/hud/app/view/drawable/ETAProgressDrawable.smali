.class public Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "ETAProgressDrawable.java"


# instance fields
.field private backgroundColor:I

.field private foreGroundColor:I

.field private progressDrawable:Landroid/graphics/drawable/Drawable;

.field private verticalMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;I)V

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 25
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->backgroundColor:I

    .line 26
    const v1, 0x7f0d002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->foreGroundColor:I

    .line 27
    const v1, 0x7f02027e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->progressDrawable:Landroid/graphics/drawable/Drawable;

    .line 28
    const v1, 0x7f0b007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->verticalMargin:I

    .line 29
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 33
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 34
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    .line 35
    .local v7, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v9, v1

    .line 36
    .local v9, "height":F
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v14, v1

    .line 37
    .local v14, "width":F
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13, v7}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 38
    .local v13, "progressBounds":Landroid/graphics/Rect;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->verticalMargin:I

    invoke-virtual {v13, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 39
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->backgroundColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    iget v1, v13, Landroid/graphics/Rect;->left:I

    int-to-float v2, v1

    iget v1, v13, Landroid/graphics/Rect;->top:I

    int-to-float v3, v1

    iget v1, v13, Landroid/graphics/Rect;->right:I

    int-to-float v4, v1

    iget v1, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 42
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mValue:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mMaxValue:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mMinValue:F

    sub-float/2addr v2, v3

    div-float v8, v1, v2

    .line 43
    .local v8, "fraction":F
    mul-float v1, v14, v8

    float-to-int v15, v1

    .line 44
    .local v15, "widthFraction":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->foreGroundColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 45
    iget v1, v13, Landroid/graphics/Rect;->left:I

    int-to-float v2, v1

    iget v1, v13, Landroid/graphics/Rect;->top:I

    int-to-float v3, v1

    iget v1, v13, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v15

    int-to-float v4, v1

    iget v1, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 46
    move v12, v9

    .line 47
    .local v12, "imageWidth":F
    iget v1, v13, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v15

    int-to-float v1, v1

    sub-float v2, v14, v12

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v10, v1

    .line 48
    .local v10, "imageLeft":I
    int-to-float v1, v10

    add-float/2addr v1, v12

    float-to-int v11, v1

    .line 49
    .local v11, "imageRight":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->progressDrawable:Landroid/graphics/drawable/Drawable;

    iget v2, v7, Landroid/graphics/Rect;->top:I

    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v10, v2, v11, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 50
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->progressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 51
    return-void
.end method
