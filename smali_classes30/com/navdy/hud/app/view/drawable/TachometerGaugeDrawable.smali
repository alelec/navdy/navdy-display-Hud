.class public Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "TachometerGaugeDrawable.java"


# static fields
.field private static final GAUGE_SLICES_START_ANGLE:F = 146.8f

.field private static final GAUGE_SLICE_ANGLE:F = 30.8f

.field private static final GAUGE_START_ANGLE:F = 146.8f

.field private static final GROOVES:I = 0x8

.field private static final LAST_MAX_MARKER_SWEEP_ANGLE:F = 2.0f

.field private static final TOTAL_SWEEP_ANGLE:F = 246.4f


# instance fields
.field private fullModeGaugeBackgroundBottomOffset:I

.field private fullModeGaugeInternalMarginBottom:I

.field private fullModeGaugeInternalMarginLeft:I

.field private fullModeGaugeInternalMarginRight:I

.field private fullModeGaugeInternalMarginTop:I

.field private gaugeBackgroundBorderWidth:F

.field private gaugeBackgroundBottomOffset:I

.field private gaugeInternalMarginBottom:I

.field private gaugeInternalMarginLeft:I

.field private gaugeInternalMarginRight:I

.field private gaugeInternalMarginTop:I

.field private grooveTextOffset:I

.field private grooveTextSize:F

.field private maxMarkerAlpha:I

.field private maxMarkerColor:I

.field private maxMarkerValue:I

.field private regularColor:I

.field private rpmWarningLevel:I

.field tachoMeterBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private valueRingWidth:I

.field private warningColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundResource"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;I)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerAlpha:I

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0162

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginTop:I

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginLeft:I

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginBottom:I

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginRight:I

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b015e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeBackgroundBottomOffset:I

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeBackgroundBorderWidth:F

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->regularColor:I

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->warningColor:I

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerColor:I

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->valueRingWidth:I

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->tachoMeterBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextSize:F

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0165

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextOffset:I

    .line 71
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginTop:I

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginTop:I

    .line 72
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginLeft:I

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginLeft:I

    .line 73
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginBottom:I

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginBottom:I

    .line 74
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeInternalMarginRight:I

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginRight:I

    .line 75
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->fullModeGaugeBackgroundBottomOffset:I

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeBackgroundBottomOffset:I

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundResource"    # I
    .param p3, "stateColorsResId"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;II)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerAlpha:I

    .line 80
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 33
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 84
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v15

    .line 86
    .local v15, "bounds":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    iget v2, v15, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginLeft:I

    add-int/2addr v2, v4

    iget v4, v15, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginTop:I

    add-int/2addr v4, v6

    iget v6, v15, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginRight:I

    sub-int/2addr v6, v7

    iget v7, v15, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeInternalMarginBottom:I

    sub-int/2addr v7, v8

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 87
    .local v18, "gaugeBounds":Landroid/graphics/Rect;
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v18

    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeBackgroundBottomOffset:I

    sub-int/2addr v7, v8

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 88
    .local v17, "gaugeBackgroundBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->tachoMeterBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->tachoMeterBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 90
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->valueRingWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->gaugeBackgroundBorderWidth:F

    add-float v22, v2, v4

    .line 91
    .local v22, "innerRingInset":F
    new-instance v3, Landroid/graphics/RectF;

    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    add-float v2, v2, v22

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float v4, v4, v22

    move-object/from16 v0, v18

    iget v6, v0, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float v6, v6, v22

    move-object/from16 v0, v18

    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    sub-float v7, v7, v22

    invoke-direct {v3, v2, v4, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 92
    .local v3, "valueRingBounds":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mValue:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mMaxValue:F

    div-float/2addr v2, v4

    const v4, 0x43766666    # 246.4f

    mul-float v5, v2, v4

    .line 93
    .local v5, "meterReadingAngle":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->valueRingWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 95
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mValue:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->rpmWarningLevel:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_2

    .line 98
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->warningColor:I

    move/from16 v16, v0

    .line 102
    .local v16, "color":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    const v4, 0x4312cccd    # 146.8f

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 104
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextOffset:I

    add-int v25, v2, v4

    .line 105
    .local v25, "radius":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextSize:F

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 109
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mMaxValue:F

    const/high16 v4, 0x41000000    # 8.0f

    div-float v26, v2, v4

    .line 110
    .local v26, "sliceValue":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mValue:F

    div-float v2, v2, v26

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v21, v0

    .line 111
    .local v21, "index":I
    const/16 v2, 0x8

    move/from16 v0, v21

    if-ge v0, v2, :cond_3

    add-int/lit8 v29, v21, 0x2

    .line 112
    .local v29, "upperBound":I
    :goto_1
    move/from16 v20, v21

    .local v20, "i":I
    :goto_2
    move/from16 v0, v20

    move/from16 v1, v29

    if-ge v0, v1, :cond_5

    .line 113
    const v2, 0x4312cccd    # 146.8f

    move/from16 v0, v20

    int-to-float v4, v0

    const v6, 0x41f66666    # 30.8f

    mul-float/2addr v4, v6

    add-float v13, v2, v4

    .line 115
    .local v13, "angle":F
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    .line 116
    .local v27, "text":Ljava/lang/String;
    new-instance v28, Landroid/graphics/Rect;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Rect;-><init>()V

    .line 117
    .local v28, "textBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v2, v0, v4, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 118
    const/4 v14, 0x0

    .line 119
    .local v14, "bottomOffset":F
    const/16 v23, 0x0

    .line 120
    .local v23, "leftOffset":F
    if-eqz v20, :cond_0

    const/16 v2, 0x8

    move/from16 v0, v20

    if-ne v0, v2, :cond_1

    .line 121
    :cond_0
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v14, v2

    .line 122
    if-nez v20, :cond_4

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v0, v2

    move/from16 v23, v0

    .line 124
    :cond_1
    :goto_3
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextOffset:I

    sub-int/2addr v2, v4

    add-int v2, v2, v25

    int-to-double v6, v2

    move/from16 v0, v25

    int-to-double v8, v0

    float-to-double v10, v13

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    move/from16 v0, v23

    float-to-double v8, v0

    sub-double/2addr v6, v8

    double-to-int v0, v6

    move/from16 v31, v0

    .line 125
    .local v31, "x":I
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->grooveTextOffset:I

    sub-int/2addr v2, v4

    add-int v2, v2, v25

    int-to-double v6, v2

    move/from16 v0, v25

    int-to-double v8, v0

    float-to-double v10, v13

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    float-to-double v8, v14

    sub-double/2addr v6, v8

    double-to-int v0, v6

    move/from16 v32, v0

    .line 126
    .local v32, "y":I
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    move-result v30

    .line 127
    .local v30, "width":I
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v19

    .line 128
    .local v19, "height":I
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    div-int/lit8 v4, v30, 0x2

    sub-int v4, v31, v4

    int-to-float v4, v4

    div-int/lit8 v6, v19, 0x2

    add-int v6, v6, v32

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 112
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_2

    .line 100
    .end local v13    # "angle":F
    .end local v14    # "bottomOffset":F
    .end local v16    # "color":I
    .end local v19    # "height":I
    .end local v20    # "i":I
    .end local v21    # "index":I
    .end local v23    # "leftOffset":F
    .end local v25    # "radius":I
    .end local v26    # "sliceValue":F
    .end local v27    # "text":Ljava/lang/String;
    .end local v28    # "textBounds":Landroid/graphics/Rect;
    .end local v29    # "upperBound":I
    .end local v30    # "width":I
    .end local v31    # "x":I
    .end local v32    # "y":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->regularColor:I

    move/from16 v16, v0

    .restart local v16    # "color":I
    goto/16 :goto_0

    .line 111
    .restart local v21    # "index":I
    .restart local v25    # "radius":I
    .restart local v26    # "sliceValue":F
    :cond_3
    add-int/lit8 v29, v21, 0x1

    goto/16 :goto_1

    .line 122
    .restart local v13    # "angle":F
    .restart local v14    # "bottomOffset":F
    .restart local v20    # "i":I
    .restart local v23    # "leftOffset":F
    .restart local v27    # "text":Ljava/lang/String;
    .restart local v28    # "textBounds":Landroid/graphics/Rect;
    .restart local v29    # "upperBound":I
    :cond_4
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    move-result v2

    neg-int v2, v2

    int-to-float v0, v2

    move/from16 v23, v0

    goto :goto_3

    .line 132
    .end local v13    # "angle":F
    .end local v14    # "bottomOffset":F
    .end local v23    # "leftOffset":F
    .end local v27    # "text":Ljava/lang/String;
    .end local v28    # "textBounds":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerValue:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mValue:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getAlpha()I

    move-result v12

    .line 134
    .local v12, "alpha":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerAlpha:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->valueRingWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 138
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerValue:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mMaxValue:F

    div-float/2addr v2, v4

    const v4, 0x43766666    # 246.4f

    mul-float v24, v2, v4

    .line 139
    .local v24, "maxMarkerAngle":F
    const v2, 0x4312cccd    # 146.8f

    add-float v2, v2, v24

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v8, v2, v4

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move-object v7, v3

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 142
    .end local v12    # "alpha":I
    .end local v24    # "maxMarkerAngle":F
    :cond_6
    return-void
.end method

.method public getMaxMarkerAlpha()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerAlpha:I

    return v0
.end method

.method public getMaxMarkerValue()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerValue:I

    return v0
.end method

.method public setMaxMarkerAlpha(I)V
    .locals 0
    .param p1, "maxMarkerAlpha"    # I

    .prologue
    .line 161
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerAlpha:I

    .line 162
    return-void
.end method

.method public setMaxMarkerValue(I)V
    .locals 0
    .param p1, "maxMarkerValue"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->maxMarkerValue:I

    .line 154
    return-void
.end method

.method public setRPMWarningLevel(I)V
    .locals 0
    .param p1, "mRPMWarningLevel"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/TachometerGaugeDrawable;->rpmWarningLevel:I

    .line 146
    return-void
.end method
