.class public Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "FuelGaugeDrawable2.java"


# static fields
.field private static final TICKS_COUNT:I = 0x4

.field private static final TICK_ANGLE_WIDTH:I = 0x2


# instance fields
.field private mBackgroundColor:I

.field private mFuelGaugeWidth:I

.field private mLeftOriented:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stateColorTable"    # I

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;II)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mLeftOriented:Z

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mFuelGaugeWidth:I

    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mColorTable:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mBackgroundColor:I

    .line 26
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 34
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 35
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    .line 37
    .local v7, "bounds":Landroid/graphics/Rect;
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mLeftOriented:Z

    if-eqz v0, :cond_0

    .line 38
    new-instance v1, Landroid/graphics/RectF;

    iget v0, v7, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v2, v7, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 42
    .local v1, "fuelGaugeArcBounds":Landroid/graphics/RectF;
    :goto_0
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mFuelGaugeWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mFuelGaugeWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mFuelGaugeWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mBackgroundColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mLeftOriented:Z

    if-eqz v0, :cond_1

    const/16 v11, 0x5a

    .line 49
    .local v11, "startAngle":I
    :goto_1
    const/16 v12, 0xb4

    .line 50
    .local v12, "sweepAngle":I
    add-int v8, v11, v12

    .line 51
    .local v8, "endAngle":I
    int-to-float v2, v11

    int-to-float v3, v12

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mDefaultColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mValue:F

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mMaxValue:F

    div-float/2addr v0, v2

    int-to-float v2, v12

    mul-float/2addr v0, v2

    float-to-int v14, v0

    .line 56
    .local v14, "valueSweepAngle":I
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mLeftOriented:Z

    if-eqz v0, :cond_2

    const/16 v13, 0x5a

    .line 57
    .local v13, "valueStartAngle":I
    :goto_2
    int-to-float v2, v13

    int-to-float v3, v14

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 60
    div-int/lit8 v10, v12, 0x4

    .line 61
    .local v10, "slice":I
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    const/4 v9, 0x1

    .local v9, "i":I
    :goto_3
    const/4 v0, 0x4

    if-ge v9, v0, :cond_3

    .line 63
    mul-int/lit8 v0, v9, 0x2d

    add-int/2addr v0, v11

    rem-int/lit16 v6, v0, 0x168

    .line 64
    .local v6, "angle":I
    int-to-float v2, v6

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 62
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 40
    .end local v1    # "fuelGaugeArcBounds":Landroid/graphics/RectF;
    .end local v6    # "angle":I
    .end local v8    # "endAngle":I
    .end local v9    # "i":I
    .end local v10    # "slice":I
    .end local v11    # "startAngle":I
    .end local v12    # "sweepAngle":I
    .end local v13    # "valueStartAngle":I
    .end local v14    # "valueSweepAngle":I
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    iget v0, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget v2, v7, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v1    # "fuelGaugeArcBounds":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 48
    :cond_1
    const/16 v11, 0x10e

    goto :goto_1

    .line 56
    .restart local v8    # "endAngle":I
    .restart local v11    # "startAngle":I
    .restart local v12    # "sweepAngle":I
    .restart local v14    # "valueSweepAngle":I
    :cond_2
    rsub-int v13, v14, 0x1c2

    goto :goto_2

    .line 66
    .restart local v9    # "i":I
    .restart local v10    # "slice":I
    .restart local v13    # "valueStartAngle":I
    :cond_3
    return-void
.end method

.method public setLeftOriented(Z)V
    .locals 0
    .param p1, "leftOriented"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/navdy/hud/app/view/drawable/FuelGaugeDrawable2;->mLeftOriented:Z

    .line 30
    return-void
.end method
