.class public Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "SpeedoMeterGaugeDrawable2.java"


# static fields
.field private static final GAUGE_START_ANGLE:F = 146.2f

.field private static final SPEED_LIMIT_MARKER_ANGLE_WIDTH:F = 4.0f

.field public static final TEXT_MARGIN:I = 0xf


# instance fields
.field private mGaugeMargin:I

.field private mOuterRingColor:I

.field private mSpeedLimit:I

.field private mSpeedLimitMarkerColor:I

.field private mSpeedLimitTextColor:I

.field private mSpeedLimitTextSize:I

.field private mValueRingWidth:I

.field public speedLimitTextAlpha:I


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundResource"    # I
    .param p3, "stateColorsResId"    # I
    .param p4, "valueRingWidth"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;II)V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->speedLimitTextAlpha:I

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mValueRingWidth:I

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b014e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mGaugeMargin:I

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitTextColor:I

    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mColorTable:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mOuterRingColor:I

    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mColorTable:[I

    const/4 v1, 0x4

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitMarkerColor:I

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b014f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitTextSize:I

    .line 41
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 29
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 65
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->getBounds()Landroid/graphics/Rect;

    move-result-object v17

    .line 67
    .local v17, "bounds":Landroid/graphics/Rect;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v26

    .line 68
    .local v26, "width":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mValueRingWidth:I

    div-int/lit8 v18, v2, 0x2

    .line 69
    .local v18, "innerRingInset":I
    new-instance v3, Landroid/graphics/RectF;

    move-object/from16 v0, v17

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int v2, v2, v18

    int-to-float v2, v2

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int v4, v4, v18

    int-to-float v4, v4

    move-object/from16 v0, v17

    iget v6, v0, Landroid/graphics/Rect;->right:I

    sub-int v6, v6, v18

    int-to-float v6, v6

    move-object/from16 v0, v17

    iget v7, v0, Landroid/graphics/Rect;->top:I

    add-int v7, v7, v26

    sub-int v7, v7, v18

    int-to-float v7, v7

    invoke-direct {v3, v2, v4, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 70
    .local v3, "oval":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mGaugeMargin:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mGaugeMargin:I

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 71
    const v5, 0x4377999a    # 247.6f

    .line 73
    .local v5, "totalSweepAngle":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mValueRingWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mOuterRingColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 76
    const v4, 0x43123333    # 146.2f

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mValueRingWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 78
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 81
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mDefaultColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mValue:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mMaxValue:F

    div-float/2addr v2, v4

    mul-float v9, v2, v5

    .line 83
    .local v9, "speedoMeterReadingAngle":F
    const v8, 0x43123333    # 146.2f

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move-object v7, v3

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 84
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimit:I

    if-lez v2, :cond_0

    .line 86
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitMarkerColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimit:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mMaxValue:F

    div-float/2addr v2, v4

    mul-float v20, v2, v5

    .line 88
    .local v20, "speedLimitSweepAngle":F
    const v2, 0x43123333    # 146.2f

    add-float v2, v2, v20

    const/high16 v4, 0x40000000    # 2.0f

    sub-float/2addr v2, v4

    const/high16 v4, 0x43b40000    # 360.0f

    rem-float v12, v2, v4

    .line 89
    .local v12, "speedLimitMarkerAngle":F
    const/high16 v13, 0x40800000    # 4.0f

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v10, p1

    move-object v11, v3

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 92
    new-instance v24, Landroid/graphics/RectF;

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 93
    .local v24, "textOval":Landroid/graphics/RectF;
    const/high16 v2, 0x41700000    # 15.0f

    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 94
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v19, v0

    .line 95
    .local v19, "radius":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getAlpha()I

    move-result v16

    .line 96
    .local v16, "alpha":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitTextColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->speedLimitTextAlpha:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 101
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimit:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    .line 102
    .local v21, "text":Ljava/lang/String;
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 103
    .local v22, "textBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v4, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 104
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    float-to-double v6, v2

    move/from16 v0, v19

    int-to-double v10, v0

    float-to-double v14, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v6, v10

    double-to-int v0, v6

    move/from16 v27, v0

    .line 105
    .local v27, "x":I
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    float-to-double v6, v2

    move/from16 v0, v19

    int-to-double v10, v0

    float-to-double v14, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v10, v14

    add-double/2addr v6, v10

    double-to-int v0, v6

    move/from16 v28, v0

    .line 106
    .local v28, "y":I
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v25

    .line 107
    .local v25, "textWidth":I
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->height()I

    move-result v23

    .line 108
    .local v23, "textHeight":I
    div-int/lit8 v2, v25, 0x2

    sub-int v2, v27, v2

    int-to-float v2, v2

    div-int/lit8 v4, v23, 0x2

    add-int v4, v4, v28

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2, v4, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mPaint:Landroid/graphics/Paint;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 111
    .end local v12    # "speedLimitMarkerAngle":F
    .end local v16    # "alpha":I
    .end local v19    # "radius":I
    .end local v20    # "speedLimitSweepAngle":F
    .end local v21    # "text":Ljava/lang/String;
    .end local v22    # "textBounds":Landroid/graphics/Rect;
    .end local v23    # "textHeight":I
    .end local v24    # "textOval":Landroid/graphics/RectF;
    .end local v25    # "textWidth":I
    .end local v27    # "x":I
    .end local v28    # "y":I
    :cond_0
    return-void
.end method

.method public setSpeedLimit(I)V
    .locals 0
    .param p1, "mSpeedLimit"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimit:I

    .line 45
    return-void
.end method

.method public setState(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x4

    .line 49
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->setState(I)V

    .line 50
    packed-switch p1, :pswitch_data_0

    .line 61
    :goto_0
    return-void

    .line 52
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mColorTable:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitMarkerColor:I

    goto :goto_0

    .line 55
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mColorTable:[I

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitMarkerColor:I

    goto :goto_0

    .line 58
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mColorTable:[I

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SpeedoMeterGaugeDrawable2;->mSpeedLimitMarkerColor:I

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
