.class public final Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "DriveScoreGaugeDrawable.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;",
        "Lcom/navdy/hud/app/view/drawable/GaugeDrawable;",
        "context",
        "Landroid/content/Context;",
        "stateColorTable",
        "",
        "(Landroid/content/Context;I)V",
        "START_ANGLE",
        "",
        "SWEEP_ANGLE",
        "backgroundColor",
        "driveScoreGauge_Width",
        "warningColor",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final START_ANGLE:F

.field private final SWEEP_ANGLE:F

.field private final backgroundColor:I

.field private final context:Landroid/content/Context;

.field private final driveScoreGauge_Width:I

.field private final warningColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "stateColorTable"    # I

    .prologue
    const/4 v2, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1, v2, p2}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->context:Landroid/content/Context;

    .line 17
    const v0, 0x431d8000    # 157.5f

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->START_ANGLE:F

    .line 18
    const/high16 v0, 0x43610000    # 225.0f

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->SWEEP_ANGLE:F

    .line 20
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->driveScoreGauge_Width:I

    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mColorTable:[I

    aget v0, v0, v2

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mDefaultColor:I

    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mColorTable:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->warningColor:I

    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mColorTable:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->backgroundColor:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x2

    const/high16 v8, 0x40a00000    # 5.0f

    const/4 v4, 0x0

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 28
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 29
    .local v6, "bounds":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/RectF;

    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v2, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-direct {v1, v0, v2, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 31
    .local v1, "driveScoreGaugeBounds":Landroid/graphics/RectF;
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->driveScoreGauge_Width:I

    int-to-float v0, v0

    int-to-float v2, v9

    div-float/2addr v0, v2

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->driveScoreGauge_Width:I

    int-to-float v2, v2

    int-to-float v3, v9

    div-float/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->driveScoreGauge_Width:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->backgroundColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 37
    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->START_ANGLE:F

    iget v3, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->SWEEP_ANGLE:F

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 38
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mValue:F

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mMaxValue:F

    div-float/2addr v0, v2

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->SWEEP_ANGLE:F

    mul-float/2addr v0, v2

    float-to-int v7, v0

    .line 40
    .local v7, "valueSweepAngle":I
    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mValue:F

    .line 41
    const/4 v3, 0x1

    new-instance v0, Lkotlin/ranges/IntRange;

    const/16 v5, 0x63

    invoke-direct {v0, v3, v5}, Lkotlin/ranges/IntRange;-><init>(II)V

    check-cast v0, Lkotlin/ranges/ClosedRange;

    invoke-static {v0, v2}, Lkotlin/ranges/RangesKt;->intRangeContains(Lkotlin/ranges/ClosedRange;F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mDefaultColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->START_ANGLE:F

    int-to-float v0, v7

    sub-float v3, v0, v8

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->START_ANGLE:F

    int-to-float v2, v7

    add-float/2addr v0, v2

    sub-float v2, v0, v8

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const/high16 v0, 0x42c80000    # 100.0f

    cmpg-float v0, v2, v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mDefaultColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget v2, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->START_ANGLE:F

    int-to-float v3, v7

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_0
.end method
