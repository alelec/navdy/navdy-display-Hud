.class public Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;
.super Lcom/navdy/hud/app/view/drawable/CustomDrawable;
.source "SmallAnalogClockDrawable.java"


# instance fields
.field private frameColor:I

.field private frameStrokeWidth:I

.field private hour:I

.field private hourHandColor:I

.field private hourHandStrokeWidth:I

.field private minute:I

.field private minuteHandColor:I

.field private minuteHandStrokeWidth:I

.field private seconds:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameColor:I

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameStrokeWidth:I

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hourHandColor:I

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0128

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hourHandStrokeWidth:I

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0129

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minuteHandStrokeWidth:I

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minuteHandColor:I

    .line 36
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 46
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 47
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    .line 48
    .local v8, "bounds":Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9, v8}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 50
    .local v9, "boundsRectF":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameStrokeWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v8}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 56
    .local v1, "frameBounds":Landroid/graphics/RectF;
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameStrokeWidth:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v0, v2

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->frameStrokeWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 57
    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hourHandColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hourHandStrokeWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 64
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hour:I

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minute:I

    invoke-static {v0, v2}, Lcom/navdy/hud/app/util/DateUtil;->getClockAngleForHour(II)F

    move-result v10

    .line 65
    .local v10, "hourAngle":F
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x7

    int-to-float v12, v0

    .line 66
    .local v12, "radius":F
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-double v2, v0

    float-to-double v4, v12

    float-to-double v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v13, v2

    .line 67
    .local v13, "x":I
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-double v2, v0

    float-to-double v4, v12

    float-to-double v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v14, v2

    .line 68
    .local v14, "y":I
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v4, v0

    int-to-float v5, v13

    int-to-float v6, v14

    iget-object v7, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minuteHandColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minuteHandStrokeWidth:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 76
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minute:I

    invoke-static {v0}, Lcom/navdy/hud/app/util/DateUtil;->getClockAngleForMinutes(I)F

    move-result v11

    .line 77
    .local v11, "minuteAngle":F
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x6

    int-to-float v12, v0

    .line 78
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-double v2, v0

    float-to-double v4, v12

    float-to-double v6, v11

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v13, v2

    .line 79
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-double v2, v0

    float-to-double v4, v12

    float-to-double v6, v11

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v14, v2

    .line 80
    invoke-virtual {v8}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v4, v0

    int-to-float v5, v13

    int-to-float v6, v14

    iget-object v7, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 81
    return-void
.end method

.method public setTime(III)V
    .locals 0
    .param p1, "hour"    # I
    .param p2, "minute"    # I
    .param p3, "seconds"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->hour:I

    .line 40
    iput p2, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->minute:I

    .line 41
    iput p3, p0, Lcom/navdy/hud/app/view/drawable/SmallAnalogClockDrawable;->seconds:I

    .line 42
    return-void
.end method
