.class public Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;
.super Lcom/navdy/hud/app/view/drawable/CustomDrawable;
.source "AnalogClockDrawable.java"


# instance fields
.field private centerPointWidth:I

.field private dateTextMargin:I

.field private dateTextSize:I

.field private dayOfMonth:I

.field private frameColor:I

.field private hour:I

.field private hourHandColor:I

.field private hourHandLengthFraction:F

.field private hourHandStrokeWidth:I

.field private minute:I

.field private minuteHandColor:I

.field private minuteHandLengthFraction:F

.field private minuteHandStrokeWidth:I

.field private seconds:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;-><init>()V

    .line 32
    const/high16 v0, 0x3f400000    # 0.75f

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hourHandLengthFraction:F

    .line 33
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandLengthFraction:F

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->frameColor:I

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hourHandColor:I

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hourHandStrokeWidth:I

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandStrokeWidth:I

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->centerPointWidth:I

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandColor:I

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dateTextSize:I

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dateTextMargin:I

    .line 46
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 57
    invoke-super/range {p0 .. p1}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v14

    .line 59
    .local v14, "bounds":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v14}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 61
    .local v3, "boundsRectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->frameColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000    # 360.0f

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hourHandColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hourHandStrokeWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v5, 0x41200000    # 10.0f

    const/high16 v6, 0x41200000    # 10.0f

    const/high16 v7, -0x1000000

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 73
    const/16 v16, 0x0

    .line 74
    .local v16, "hourAngle":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hour:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    invoke-static {v2, v4}, Lcom/navdy/hud/app/util/DateUtil;->getClockAngleForHour(II)F

    move-result v16

    .line 75
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0xf

    int-to-float v0, v2

    move/from16 v18, v0

    .line 76
    .local v18, "radius":F
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    move/from16 v0, v16

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v21, v0

    .line 77
    .local v21, "x":I
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    move/from16 v0, v16

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 78
    .local v22, "y":I
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v5, v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v6, v2

    move/from16 v0, v21

    int-to-float v7, v0

    move/from16 v0, v22

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 79
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->getColor()I

    move-result v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandStrokeWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 86
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    invoke-static {v2}, Lcom/navdy/hud/app/util/DateUtil;->getClockAngleForMinutes(I)F

    move-result v17

    .line 87
    .local v17, "minuteAngle":F
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0xa

    int-to-float v0, v2

    move/from16 v18, v0

    .line 88
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    move/from16 v0, v17

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v21, v0

    .line 89
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    move/from16 v0, v17

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 90
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v5, v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v6, v2

    move/from16 v0, v21

    int-to-float v7, v0

    move/from16 v0, v22

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 95
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minuteHandColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->centerPointWidth:I

    sub-int/2addr v2, v4

    int-to-float v5, v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->centerPointWidth:I

    sub-int/2addr v2, v4

    int-to-float v6, v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->centerPointWidth:I

    add-int/2addr v2, v4

    int-to-float v7, v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->centerPointWidth:I

    add-int/2addr v2, v4

    int-to-float v8, v2

    const/4 v9, 0x0

    const/high16 v10, 0x43b40000    # 360.0f

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/graphics/Canvas;->drawArc(FFFFFFZLandroid/graphics/Paint;)V

    .line 101
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dateTextMargin:I

    sub-int/2addr v2, v4

    int-to-float v0, v2

    move/from16 v18, v0

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dateTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 105
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dayOfMonth:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    .line 106
    .local v20, "text":Ljava/lang/String;
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 107
    .local v19, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v4, v5, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 108
    const/4 v13, 0x0

    .line 109
    .local v13, "angle":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hour:I

    rem-int/lit8 v15, v2, 0xc

    .line 110
    .local v15, "hour12Format":I
    const/4 v2, 0x7

    if-lt v15, v2, :cond_0

    const/16 v2, 0xb

    if-lt v15, v2, :cond_3

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/16 v4, 0x23

    if-lt v2, v4, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/16 v4, 0x37

    if-lt v2, v4, :cond_3

    .line 111
    :cond_1
    const/high16 v13, 0x43340000    # 180.0f

    .line 112
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v21, v0

    .line 113
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 114
    move/from16 v0, v21

    int-to-float v2, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v22

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 127
    :cond_2
    :goto_0
    return-void

    .line 115
    :cond_3
    const/4 v2, 0x1

    if-lt v15, v2, :cond_4

    const/4 v2, 0x4

    if-lt v15, v2, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/4 v4, 0x5

    if-lt v2, v4, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/16 v4, 0x14

    if-lt v2, v4, :cond_6

    .line 116
    :cond_5
    const/4 v13, 0x0

    .line 117
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v21, v0

    .line 118
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 119
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int v2, v21, v2

    int-to-float v2, v2

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v22

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 120
    :cond_6
    const/4 v2, 0x4

    if-lt v15, v2, :cond_7

    const/4 v2, 0x7

    if-lt v15, v2, :cond_2

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/16 v4, 0x14

    if-lt v2, v4, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    const/16 v4, 0x23

    if-lt v2, v4, :cond_2

    .line 121
    :cond_8
    const/high16 v13, 0x42b40000    # 90.0f

    .line 122
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v21, v0

    .line 123
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-double v4, v2

    move/from16 v0, v18

    float-to-double v6, v0

    float-to-double v8, v13

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 124
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v21, v2

    int-to-float v2, v2

    move/from16 v0, v22

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public setTime(IIII)V
    .locals 0
    .param p1, "day"    # I
    .param p2, "hour"    # I
    .param p3, "minute"    # I
    .param p4, "seconds"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->dayOfMonth:I

    .line 50
    iput p2, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->hour:I

    .line 51
    iput p3, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->minute:I

    .line 52
    iput p4, p0, Lcom/navdy/hud/app/view/drawable/AnalogClockDrawable;->seconds:I

    .line 53
    return-void
.end method
