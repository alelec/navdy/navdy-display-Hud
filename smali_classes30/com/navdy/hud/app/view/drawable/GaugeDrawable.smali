.class public Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.super Lcom/navdy/hud/app/view/drawable/CustomDrawable;
.source "GaugeDrawable.java"


# instance fields
.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field protected mColorTable:[I

.field protected mDefaultColor:I

.field protected mMaxValue:F

.field protected mMinValue:F

.field protected mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundResource"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;-><init>()V

    .line 20
    if-eqz p2, :cond_0

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundResource"    # I
    .param p3, "stateColorsResId"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;I)V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mDefaultColor:I

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 54
    :cond_0
    return-void
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 76
    .local v0, "rect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 79
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 84
    .local v0, "rect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 87
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBounds(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/view/drawable/CustomDrawable;->setBounds(IIII)V

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 62
    :cond_0
    return-void
.end method

.method public setGaugeValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 35
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mValue:F

    .line 36
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMaxValue:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 37
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMaxValue:F

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mValue:F

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMinValue:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 39
    iget v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMinValue:F

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mValue:F

    goto :goto_0
.end method

.method public setMaxGaugeValue(F)V
    .locals 0
    .param p1, "maxValue"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMaxValue:F

    .line 45
    return-void
.end method

.method public setMinValue(F)V
    .locals 0
    .param p1, "mMinValue"    # F

    .prologue
    .line 91
    iput p1, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mMinValue:F

    .line 92
    return-void
.end method

.method public setState(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mColorTable:[I

    aget v0, v0, p1

    iput v0, p0, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->mDefaultColor:I

    .line 72
    return-void

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
