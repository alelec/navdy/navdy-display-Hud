.class public abstract Lcom/navdy/hud/app/view/GaugeViewPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "GaugeViewPresenter.java"


# instance fields
.field protected mGaugeView:Lcom/navdy/hud/app/view/GaugeView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 2
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 14
    if-nez p1, :cond_0

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/GaugeViewPresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    .line 23
    .end local p1    # "dashboardWidgetView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    :goto_0
    return-void

    .line 18
    .restart local p1    # "dashboardWidgetView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    :cond_0
    instance-of v0, p1, Lcom/navdy/hud/app/view/GaugeView;

    if-nez v0, :cond_1

    .line 19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The view has to be of type GaugeView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_1
    check-cast p1, Lcom/navdy/hud/app/view/GaugeView;

    .end local p1    # "dashboardWidgetView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    iput-object p1, p0, Lcom/navdy/hud/app/view/GaugeViewPresenter;->mGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    goto :goto_0
.end method
