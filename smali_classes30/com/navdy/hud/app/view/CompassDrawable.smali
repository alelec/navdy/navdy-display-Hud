.class public Lcom/navdy/hud/app/view/CompassDrawable;
.super Lcom/navdy/hud/app/view/drawable/GaugeDrawable;
.source "CompassDrawable.java"


# static fields
.field private static final SEGMENT_ANGLE:D = 22.5

.field private static final TEXT_PADDING:I = 0x6

.field private static final TOTAL_SEGMENTS:I = 0x10

.field private static headings:[Ljava/lang/String;

.field private static majorTickHeight:I

.field private static minorTickHeight:I

.field private static widths:[I


# instance fields
.field private mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

.field private mIndicatorHeight:I

.field private mIndicatorTopMargin:I

.field private mIndicatorWidth:I

.field private mPath:Landroid/graphics/Path;

.field private mStripHeight:I

.field private mStripTopMargin:I

.field private mStripWidth:I

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0xa

    sput v0, Lcom/navdy/hud/app/view/CompassDrawable;->majorTickHeight:I

    .line 25
    const/4 v0, 0x5

    sput v0, Lcom/navdy/hud/app/view/CompassDrawable;->minorTickHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 41
    invoke-direct {p0, p1, v6}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;-><init>(Landroid/content/Context;I)V

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 43
    .local v2, "resources":Landroid/content/res/Resources;
    const v3, 0x7f02010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    .line 44
    const v3, 0x7f0b005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorWidth:I

    .line 45
    const v3, 0x7f0b0059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorHeight:I

    .line 46
    const v3, 0x7f0b005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorTopMargin:I

    .line 47
    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripWidth:I

    .line 48
    const v3, 0x7f0b005f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripTopMargin:I

    .line 49
    const v3, 0x7f0b005e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripHeight:I

    .line 51
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 52
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPaint:Landroid/graphics/Paint;

    const v4, 0x7f0d000e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    .line 56
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 59
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 60
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const-string v4, "sans-serif"

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 61
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const v4, 0x7f0b0061

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 63
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPath:Landroid/graphics/Path;

    .line 65
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 66
    .local v1, "measuringRect":Landroid/graphics/Rect;
    sget-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    if-nez v3, :cond_0

    .line 67
    const v3, 0x7f0b005c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/navdy/hud/app/view/CompassDrawable;->majorTickHeight:I

    .line 68
    const v3, 0x7f0b005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/navdy/hud/app/view/CompassDrawable;->minorTickHeight:I

    .line 69
    const/high16 v3, 0x7f0a0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    .line 70
    sget-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->widths:[I

    .line 71
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->widths:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 72
    iget-object v3, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    aget-object v4, v4, v0

    sget-object v5, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v6, v5, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 73
    sget-object v3, Lcom/navdy/hud/app/view/CompassDrawable;->widths:[I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    aput v4, v3, v0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private drawCompassPoints(Landroid/graphics/Canvas;FIIIIZ)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "heading"    # F
    .param p3, "left"    # I
    .param p4, "top"    # I
    .param p5, "right"    # I
    .param p6, "bottom"    # I
    .param p7, "showHeadings"    # Z

    .prologue
    .line 93
    sub-int v22, p5, p3

    .line 94
    .local v22, "width":I
    div-int/lit8 v2, v22, 0x2

    add-int v2, v2, p3

    int-to-float v8, v2

    .line 95
    .local v8, "center":F
    const/high16 v2, 0x43b40000    # 360.0f

    rem-float p2, p2, v2

    .line 96
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripWidth:I

    div-int/lit8 v16, v2, 0x10

    .line 97
    .local v16, "segmentVisualSize":I
    div-int v2, v22, v16

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v17, v2, 0x1

    .line 99
    .local v17, "segmentsToDraw":I
    const/high16 v2, 0x41b40000    # 22.5f

    div-float v15, p2, v2

    .line 100
    .local v15, "segment":F
    float-to-int v0, v15

    move/from16 v19, v0

    .line 101
    .local v19, "tick":I
    move/from16 v0, v19

    int-to-float v2, v0

    sub-float v21, v15, v2

    .line 102
    .local v21, "tickOffset":F
    move/from16 v0, v17

    neg-int v2, v0

    div-int/lit8 v12, v2, 0x2

    .line 103
    .local v12, "leftTick":I
    div-int/lit8 v14, v17, 0x2

    .line 105
    .local v14, "rightTick":I
    move v11, v12

    .local v11, "i":I
    :goto_0
    if-ge v11, v14, :cond_3

    .line 106
    int-to-float v2, v11

    add-float v2, v2, v21

    move/from16 v0, v16

    int-to-float v3, v0

    mul-float/2addr v2, v3

    add-float/2addr v2, v8

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v0, v2

    move/from16 v23, v0

    .line 108
    .local v23, "x":I
    sub-int v2, v19, v11

    add-int/lit8 v2, v2, 0x10

    rem-int/lit8 v20, v2, 0x10

    .line 109
    .local v20, "tickIndex":I
    and-int/lit8 v2, v20, 0x1

    if-nez v2, :cond_1

    const/4 v13, 0x1

    .line 110
    .local v13, "majorTick":Z
    :goto_1
    if-eqz v13, :cond_2

    sget v10, Lcom/navdy/hud/app/view/CompassDrawable;->majorTickHeight:I

    .line 111
    .local v10, "height":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 112
    move/from16 v0, v23

    int-to-float v3, v0

    sub-int v2, p6, v10

    int-to-float v4, v2

    move/from16 v0, v23

    int-to-float v5, v0

    move/from16 v0, p6

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 113
    if-eqz v13, :cond_0

    if-eqz p7, :cond_0

    .line 114
    div-int/lit8 v9, v20, 0x2

    .line 115
    .local v9, "headingIndex":I
    sget-object v2, Lcom/navdy/hud/app/view/CompassDrawable;->headings:[Ljava/lang/String;

    aget-object v18, v2, v9

    .line 116
    .local v18, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 117
    sget-object v2, Lcom/navdy/hud/app/view/CompassDrawable;->widths:[I

    aget v2, v2, v9

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v23, v2

    int-to-float v2, v2

    sub-int v3, p6, v10

    add-int/lit8 v3, v3, -0x6

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/view/CompassDrawable;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 105
    .end local v9    # "headingIndex":I
    .end local v18    # "text":Ljava/lang/String;
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 109
    .end local v10    # "height":I
    .end local v13    # "majorTick":Z
    :cond_1
    const/4 v13, 0x0

    goto :goto_1

    .line 110
    .restart local v13    # "majorTick":Z
    :cond_2
    sget v10, Lcom/navdy/hud/app/view/CompassDrawable;->minorTickHeight:I

    goto :goto_2

    .line 120
    .end local v13    # "majorTick":Z
    .end local v20    # "tickIndex":I
    .end local v23    # "x":I
    :cond_3
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 125
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/CompassDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    .line 126
    .local v9, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v13

    .line 127
    .local v13, "width":I
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v10

    .line 128
    .local v10, "height":I
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 129
    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-float v3, v13

    int-to-float v4, v10

    const/4 v5, 0x0

    const/high16 v6, 0x43b40000    # 360.0f

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Canvas;->drawArc(FFFFFFZLandroid/graphics/Paint;)V

    .line 132
    iget v0, v9, Landroid/graphics/Rect;->left:I

    div-int/lit8 v1, v13, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorWidth:I

    div-int/lit8 v1, v1, 0x2

    sub-int v11, v0, v1

    .line 133
    .local v11, "left":I
    iget v0, v9, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorTopMargin:I

    add-int v4, v0, v1

    .line 134
    .local v4, "top":I
    iget v0, v9, Landroid/graphics/Rect;->left:I

    div-int/lit8 v1, v13, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int v12, v0, v1

    .line 135
    .local v12, "right":I
    iget v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorHeight:I

    add-int v6, v4, v0

    .line 136
    .local v6, "bottom":I
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v11, v4, v12, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 141
    iget v4, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripTopMargin:I

    .line 142
    iget v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripTopMargin:I

    iget v1, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mStripHeight:I

    add-int v6, v0, v1

    .line 143
    iget v2, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mValue:F

    const/4 v3, 0x0

    iget v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mValue:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v5, v13

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/view/CompassDrawable;->drawCompassPoints(Landroid/graphics/Canvas;FIIIIZ)V

    .line 144
    return-void

    .line 143
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/drawable/GaugeDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/view/CompassDrawable;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v3, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v4, v2

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addOval(FFFFLandroid/graphics/Path$Direction;)V

    .line 84
    return-void
.end method
