.class public Lcom/navdy/hud/app/view/BrightnessControlGauge;
.super Lcom/navdy/hud/app/view/Gauge;
.source "BrightnessControlGauge.java"


# static fields
.field private static final PIVOT_ANGLE:I = 0x5

.field private static final THUMB_ANGLE:I = 0xa


# instance fields
.field private mPivotIndicatorColor:I

.field private mPivotIndicatorPaint:Landroid/graphics/Paint;

.field private mPivotValue:I

.field private mThumbColor:I

.field private mThumbPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/view/Gauge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 38
    .local v1, "resources":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/R$styleable;->BrightnessControlGauge:[I

    invoke-virtual {v2, p2, v3, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 42
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    .line 43
    const v2, 0x7f0d00b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbColor:I

    .line 44
    const v2, 0x7f0d0097

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorColor:I

    .line 45
    return-void
.end method


# virtual methods
.method protected drawGauge(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->getWidth()I

    move-result v0

    int-to-float v13, v0

    .line 68
    .local v13, "width":F
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->getHeight()I

    move-result v0

    int-to-float v6, v0

    .line 69
    .local v6, "height":F
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->getRadius()F

    move-result v7

    .line 70
    .local v7, "radius":F
    const/high16 v3, 0x40a00000    # 5.0f

    .line 71
    .local v3, "sweep":F
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mMinValue:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->deltaToAngle(I)F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v3, v2

    sub-float v12, v0, v2

    .line 73
    .local v12, "startAngle":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v13, v0

    sub-float v9, v0, v7

    .line 74
    .local v9, "rectLeft":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v6, v0

    sub-float v11, v0, v7

    .line 75
    .local v11, "rectTop":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v13, v0

    add-float v10, v0, v7

    .line 76
    .local v10, "rectRight":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v6, v0

    add-float v8, v0, v7

    .line 78
    .local v8, "rectBottom":F
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 79
    .local v1, "rect":Landroid/graphics/RectF;
    invoke-virtual {v1, v9, v11, v10, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 81
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mStartAngle:I

    int-to-float v0, v0

    add-float v2, v0, v12

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 84
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    if-le v0, v2, :cond_1

    .line 85
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mValue:I

    invoke-virtual {p0, p1, v0, v2}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->drawIndicator(Landroid/graphics/Canvas;II)V

    .line 91
    :cond_0
    :goto_0
    const/high16 v3, 0x41200000    # 10.0f

    .line 92
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mMinValue:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->deltaToAngle(I)F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v3, v2

    sub-float v12, v0, v2

    .line 93
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mStartAngle:I

    int-to-float v0, v0

    add-float v2, v0, v12

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 94
    return-void

    .line 86
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    if-ge v0, v2, :cond_0

    .line 87
    iget v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mValue:I

    iget v2, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotValue:I

    invoke-virtual {p0, p1, v0, v2}, Lcom/navdy/hud/app/view/BrightnessControlGauge;->drawIndicator(Landroid/graphics/Canvas;II)V

    goto :goto_0
.end method

.method protected initDrawingTools()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 49
    invoke-super {p0}, Lcom/navdy/hud/app/view/Gauge;->initDrawingTools()V

    .line 50
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mBackgroundThickness:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mPivotIndicatorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThickness:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/view/BrightnessControlGauge;->mThumbPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 62
    return-void
.end method
