.class public Lcom/navdy/hud/app/view/GaugeView;
.super Lcom/navdy/hud/app/view/DashboardWidgetView;
.source "GaugeView.java"


# instance fields
.field mTvUnit:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00bd
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field mTvValue:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00be
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/GaugeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/view/GaugeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/view/DashboardWidgetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    sget-object v2, Lcom/navdy/hud/app/R$styleable;->GaugeView:[I

    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "customAttributes":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 41
    .local v1, "pixelSize":I
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/GaugeView;->setValueTextSize(I)V

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    .end local v1    # "pixelSize":I
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-super {p0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->clear()V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method public getUnitTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    return-object v0
.end method

.method public getUnitView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    return-object v0
.end method

.method public getValueTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    return-object v0
.end method

.method public getValueView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetView;->onMeasure(II)V

    .line 55
    return-void
.end method

.method public setUnitText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvUnit:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_0
    return-void
.end method

.method public setValueText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_0
    return-void
.end method

.method public setValueTextSize(I)V
    .locals 3
    .param p1, "pixelSize"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/view/GaugeView;->mTvValue:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 50
    return-void
.end method
