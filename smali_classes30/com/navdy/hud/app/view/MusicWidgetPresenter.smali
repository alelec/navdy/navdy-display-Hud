.class public Lcom/navdy/hud/app/view/MusicWidgetPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "MusicWidgetPresenter.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;


# static fields
.field private static final ARTWORK_ALPHA_NOT_PLAYING:F = 0.5f

.field private static final ARTWORK_ALPHA_PLAYING:F = 1.0f

.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private albumArt:Landroid/graphics/Bitmap;

.field private animateNextArtworkTransition:Z

.field private artistText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

.field private handler:Landroid/os/Handler;

.field private image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

.field private lastAlbumArtHash:Ljava/lang/CharSequence;

.field private musicGaugeName:Ljava/lang/String;

.field musicManager:Lcom/navdy/hud/app/manager/MusicManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private stateText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

.field private titleText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

.field private trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->handler:Landroid/os/Handler;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090318

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->musicGaugeName:Ljava/lang/String;

    .line 61
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->resetArtwork()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->redrawOnMainThread()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/view/MusicWidgetPresenter;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/MusicWidgetPresenter;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private redrawOnMainThread()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter$3;-><init>(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 208
    return-void
.end method

.method private resetArtwork()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 197
    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;

    .line 198
    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->lastAlbumArtHash:Ljava/lang/CharSequence;

    .line 199
    return-void
.end method

.method private updateMetadata()V
    .locals 4

    .prologue
    .line 103
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->titleText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 104
    .local v1, "currentTitle":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->titleText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    iget-object v3, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->artistText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 108
    .local v0, "currentArtist":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 109
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->artistText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    iget-object v3, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :cond_3
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "getWidgetIdentifier"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 116
    const-string v0, "MUSIC_WIDGET"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->musicGaugeName:Ljava/lang/String;

    return-object v0
.end method

.method public onAlbumArtUpdate(Lokio/ByteString;Z)V
    .locals 4
    .param p1, "photo"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 143
    sget-object v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAlbumArtUpdate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 144
    iput-boolean p2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->animateNextArtworkTransition:Z

    .line 146
    if-nez p1, :cond_0

    .line 147
    sget-object v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ByteString image to set in notification is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 148
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->resetArtwork()V

    .line 149
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->reDraw()V

    .line 194
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p1}, Lokio/ByteString;->md5()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "hash":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->lastAlbumArtHash:Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 155
    sget-object v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Already have this artwork, ignoring"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_1
    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->lastAlbumArtHash:Ljava/lang/CharSequence;

    .line 162
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/view/MusicWidgetPresenter$2;-><init>(Lcom/navdy/hud/app/view/MusicWidgetPresenter;Lokio/ByteString;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
    .locals 4
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p3, "willOpenNotification"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p2, "currentControls":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/hud/app/manager/MusicManager$MediaControl;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 127
    if-eqz p3, :cond_0

    .line 128
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onTrackUpdated, delayed!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/view/MusicWidgetPresenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter$1;-><init>(Lcom/navdy/hud/app/view/MusicWidgetPresenter;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 139
    :goto_0
    return-void

    .line 136
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onTrackUpdated (immediate)"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->reDraw()V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 67
    if-eqz p1, :cond_0

    .line 68
    const v0, 0x7f030034

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 70
    const v0, 0x7f0e0143

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/OutlineTextView;

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->titleText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    .line 71
    const v0, 0x7f0e0142

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/OutlineTextView;

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->artistText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    .line 72
    const v0, 0x7f0e0144

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/OutlineTextView;

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->stateText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    .line 73
    const v0, 0x7f0e0141

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .line 75
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 76
    return-void
.end method

.method public setWidgetVisibleToUser(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 212
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWidgetVisibleToUser "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 213
    if-eqz p1, :cond_0

    .line 214
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setWidgetVisibleToUser: add music update listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager;->addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 221
    :goto_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    .line 222
    return-void

    .line 218
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setWidgetVisibleToUser: remove music update listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager;->removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    goto :goto_0
.end method

.method protected updateGauge()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->albumArt:Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->animateNextArtworkTransition:Z

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setArtworkBitmap(Landroid/graphics/Bitmap;Z)V

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->EMPTY_TRACK:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eq v0, v1, :cond_1

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setAlpha(F)V

    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->stateText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setVisibility(I)V

    .line 94
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->updateMetadata()V

    .line 100
    :goto_1
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setAlpha(F)V

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->stateText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setVisibility(I)V

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->titleText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->artistText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;->stateText:Lcom/navdy/hud/app/framework/music/OutlineTextView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/music/OutlineTextView;->setVisibility(I)V

    goto :goto_1
.end method
