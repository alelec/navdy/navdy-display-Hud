.class Lcom/navdy/hud/app/view/MainView$7;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "MainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MainView;->getRemoveNotificationExtensionAnimator()Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/MainView;

.field final synthetic val$notificationExtension:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MainView;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 579
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView$7;->this$0:Lcom/navdy/hud/app/view/MainView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/MainView$7;->val$notificationExtension:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 582
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 583
    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView$7;->val$notificationExtension:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 584
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;

    if-eqz v1, :cond_0

    .line 585
    check-cast v0, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;

    .end local v0    # "view":Landroid/view/View;
    invoke-interface {v0}, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;->onStop()V

    .line 587
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView$7;->val$notificationExtension:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 588
    iget-object v1, p0, Lcom/navdy/hud/app/view/MainView$7;->val$notificationExtension:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 589
    return-void
.end method
