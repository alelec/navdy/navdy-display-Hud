.class Lcom/navdy/hud/app/view/ToastView$1;
.super Ljava/lang/Object;
.source "ToastView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ToastView;->animateIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ToastView;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$screenName:Ljava/lang/String;

.field final synthetic val$tts:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ToastView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/hud/app/view/ToastView$1;->this$0:Lcom/navdy/hud/app/view/ToastView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$tts:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$id:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$screenName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 173
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$tts:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$tts:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$screenName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 178
    :try_start_0
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in dismiss screen:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$screenName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/view/ToastView;->access$100(Lcom/navdy/hud/app/view/ToastView;)Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/view/ToastView$1;->val$screenName:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/service/library/events/ui/Screen;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 180
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->clearScreenName()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->this$0:Lcom/navdy/hud/app/view/ToastView;

    # getter for: Lcom/navdy/hud/app/view/ToastView;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/view/ToastView;->access$100(Lcom/navdy/hud/app/view/ToastView;)Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;

    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->getCurrentId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager$ShowToast;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 187
    iget-object v1, p0, Lcom/navdy/hud/app/view/ToastView$1;->this$0:Lcom/navdy/hud/app/view/ToastView;

    const/4 v2, 0x1

    # setter for: Lcom/navdy/hud/app/view/ToastView;->sendShowEvent:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/view/ToastView;->access$202(Lcom/navdy/hud/app/view/ToastView;Z)Z

    .line 188
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/view/ToastView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/view/ToastView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
