.class public Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "SpeedLimitSignPresenter.java"


# instance fields
.field private speedLimit:I

.field protected speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01de
    .end annotation
.end field

.field private speedLimitSignWidgetName:Ljava/lang/String;

.field protected speedLimitUnavailableText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01df
    .end annotation
.end field

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 28
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090319

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignWidgetName:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, "SPEED_LIMIT_SIGN_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignWidgetName:Ljava/lang/String;

    return-object v0
.end method

.method protected isRegisteringToBusRequired()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 0
    .param p1, "unitChanged"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->reDraw()V

    .line 88
    return-void
.end method

.method public setSpeedLimit(I)V
    .locals 0
    .param p1, "speedLimit"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimit:I

    .line 37
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->reDraw()V

    .line 38
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 42
    if-eqz p1, :cond_0

    .line 43
    const v0, 0x7f03006c

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 44
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 47
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 48
    return-void
.end method

.method protected updateGauge()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 62
    iget v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimit:I

    if-lez v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitUnavailableText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setVisibility(I)V

    .line 65
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v0

    .line 66
    .local v0, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimitUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V

    .line 67
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    iget v2, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimit:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimit(I)V

    .line 73
    .end local v0    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitUnavailableText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    iget-object v1, p0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setVisibility(I)V

    goto :goto_0
.end method
