.class public Lcom/navdy/hud/app/view/DialUpdateProgressView;
.super Landroid/widget/RelativeLayout;
.source "DialUpdateProgressView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final UPDATE_TIMEOUT:J = 0x75300L

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private handler:Landroid/os/Handler;

.field mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mProgress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e017d
    .end annotation
.end field

.field private timeout:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/DialUpdateProgressView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->handler:Landroid/os/Handler;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/view/DialUpdateProgressView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/DialUpdateProgressView$1;-><init>(Lcom/navdy/hud/app/view/DialUpdateProgressView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->timeout:Ljava/lang/Runnable;

    .line 50
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialUpdateProgressView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 52
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->handler:Landroid/os/Handler;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/view/DialUpdateProgressView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/DialUpdateProgressView$1;-><init>(Lcom/navdy/hud/app/view/DialUpdateProgressView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->timeout:Ljava/lang/Runnable;

    .line 56
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/DialUpdateProgressView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 58
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 81
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 86
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 87
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 92
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 63
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 68
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 69
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mPresenter:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->startUpdate()V

    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->timeout:Ljava/lang/Runnable;

    const-wide/32 v2, 0x75300

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 75
    :cond_0
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/view/DialUpdateProgressView;->setProgress(I)V

    .line 76
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "percentage"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/view/DialUpdateProgressView;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 96
    return-void
.end method
