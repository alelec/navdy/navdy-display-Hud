.class final enum Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;
.super Ljava/lang/Enum;
.source "LearnGestureScreenLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

.field public static final enum CAPTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

.field public static final enum GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

.field public static final enum SENSOR_BLOCKED:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

.field public static final enum TIPS:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    const-string v1, "GESTURE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 38
    new-instance v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    const-string v1, "SENSOR_BLOCKED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->SENSOR_BLOCKED:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 39
    new-instance v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    const-string v1, "TIPS"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->TIPS:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    const-string v1, "CAPTURE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->CAPTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    sget-object v1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->GESTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->SENSOR_BLOCKED:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->TIPS:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->CAPTURE:Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->$VALUES:[Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->$VALUES:[Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/view/LearnGestureScreenLayout$Mode;

    return-object v0
.end method
