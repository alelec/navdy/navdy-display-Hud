.class public Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
.super Landroid/widget/LinearLayout;
.source "MaxWidthLinearLayout.java"


# instance fields
.field private maxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->MaxWidthLinearLayout:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 24
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    .line 25
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 26
    return-void
.end method


# virtual methods
.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 30
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 31
    .local v1, "measuredWidth":I
    iget v2, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    if-ge v2, v1, :cond_0

    .line 32
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 33
    .local v0, "measureMode":I
    iget v2, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 35
    .end local v0    # "measureMode":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 36
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->maxWidth:I

    .line 40
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->invalidate()V

    .line 41
    return-void
.end method
