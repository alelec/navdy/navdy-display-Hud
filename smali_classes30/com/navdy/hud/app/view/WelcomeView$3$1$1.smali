.class Lcom/navdy/hud/app/view/WelcomeView$3$1$1;
.super Ljava/lang/Object;
.source "WelcomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/WelcomeView$3$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

.field final synthetic val$imageExists:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/WelcomeView$3$1;Z)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/view/WelcomeView$3$1;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iput-boolean p2, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->val$imageExists:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$view:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-eq v0, v1, :cond_0

    .line 338
    :goto_0
    return-void

    .line 329
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->val$imageExists:Z

    if-nez v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$initialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$initials:Ljava/lang/String;

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$3$1;->this$1:Lcom/navdy/hud/app/view/WelcomeView$3;

    iget-object v0, v0, Lcom/navdy/hud/app/view/WelcomeView$3;->this$0:Lcom/navdy/hud/app/view/WelcomeView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v1, v1, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$imagePath:Ljava/io/File;

    iget-object v2, p0, Lcom/navdy/hud/app/view/WelcomeView$3$1$1;->this$2:Lcom/navdy/hud/app/view/WelcomeView$3$1;

    iget-object v2, v2, Lcom/navdy/hud/app/view/WelcomeView$3$1;->val$initialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/view/WelcomeView;->setImage(Ljava/io/File;Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V
    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/view/WelcomeView;->access$1000(Lcom/navdy/hud/app/view/WelcomeView;Ljava/io/File;Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    goto :goto_0
.end method
