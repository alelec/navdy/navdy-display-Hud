.class public Lcom/navdy/hud/app/view/ObservableScrollView;
.super Landroid/widget/ScrollView;
.source "ObservableScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;
    }
.end annotation


# instance fields
.field private child:Landroid/view/View;

.field private listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 4
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 39
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->child:Landroid/view/View;

    if-nez v1, :cond_0

    .line 40
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/view/ObservableScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->child:Landroid/view/View;

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->child:Landroid/view/View;

    if-nez v1, :cond_1

    .line 43
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 68
    :goto_0
    return-void

    .line 47
    :cond_1
    if-gtz p2, :cond_3

    .line 48
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    if-eqz v1, :cond_2

    .line 49
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    invoke-interface {v1}, Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;->onTop()V

    .line 51
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    goto :goto_0

    .line 55
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->child:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ObservableScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/navdy/hud/app/view/ObservableScrollView;->getScrollY()I

    move-result v3

    add-int/2addr v2, v3

    sub-int v0, v1, v2

    .line 56
    .local v0, "diff":I
    if-nez v0, :cond_5

    .line 57
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    if-eqz v1, :cond_4

    .line 58
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    invoke-interface {v1}, Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;->onBottom()V

    .line 60
    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    goto :goto_0

    .line 64
    :cond_5
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    if-eqz v1, :cond_6

    .line 65
    iget-object v1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;->onScroll(IIII)V

    .line 67
    :cond_6
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    goto :goto_0
.end method

.method public setScrollListener(Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/hud/app/view/ObservableScrollView;->listener:Lcom/navdy/hud/app/view/ObservableScrollView$IScrollListener;

    .line 35
    return-void
.end method
