.class Lcom/navdy/hud/app/view/MainView$6;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "MainView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/MainView;->addNotificationExtensionView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/MainView;

.field final synthetic val$notificationExtensionView:Landroid/view/ViewGroup;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/MainView;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/MainView;

    .prologue
    .line 539
    iput-object p1, p0, Lcom/navdy/hud/app/view/MainView$6;->this$0:Lcom/navdy/hud/app/view/MainView;

    iput-object p2, p0, Lcom/navdy/hud/app/view/MainView$6;->val$notificationExtensionView:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/navdy/hud/app/view/MainView$6;->val$view:Landroid/view/View;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 550
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 551
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$6;->val$view:Landroid/view/View;

    instance-of v0, v0, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$6;->val$view:Landroid/view/View;

    check-cast v0, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;->onStart()V

    .line 554
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 543
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$6;->val$notificationExtensionView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/navdy/hud/app/view/MainView$6;->this$0:Lcom/navdy/hud/app/view/MainView;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/view/MainView;->showingNotificationExtension:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/view/MainView;->access$1102(Lcom/navdy/hud/app/view/MainView;Z)Z

    .line 546
    return-void
.end method
