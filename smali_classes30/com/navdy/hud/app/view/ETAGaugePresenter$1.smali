.class Lcom/navdy/hud/app/view/ETAGaugePresenter$1;
.super Ljava/lang/Object;
.source "ETAGaugePresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/view/ETAGaugePresenter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/view/ETAGaugePresenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/view/ETAGaugePresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/view/ETAGaugePresenter;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/ETAGaugePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/ETAGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/ETAGaugePresenter;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->access$200(Lcom/navdy/hud/app/view/ETAGaugePresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/ETAGaugePresenter;

    # getter for: Lcom/navdy/hud/app/view/ETAGaugePresenter;->mTripInfoUpdateRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->access$000(Lcom/navdy/hud/app/view/ETAGaugePresenter;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/view/ETAGaugePresenter;->TRIP_INFO_UPDATE_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->access$100()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/view/ETAGaugePresenter$1;->this$0:Lcom/navdy/hud/app/view/ETAGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;->reDraw()V

    .line 142
    return-void
.end method
