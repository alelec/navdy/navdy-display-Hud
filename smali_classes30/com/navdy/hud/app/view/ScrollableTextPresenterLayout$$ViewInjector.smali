.class public Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout$$ViewInjector;
.super Ljava/lang/Object;
.source "ScrollableTextPresenterLayout$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00cc

    const-string v2, "field \'mMainTitleText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainTitleText:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0112

    const-string v2, "field \'mMainImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainImageView:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f0e0109

    const-string v2, "field \'mObservableScrollView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/ObservableScrollView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    .line 16
    const v1, 0x7f0e00c3

    const-string v2, "field \'mTitleText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTitleText:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e010a

    const-string v2, "field \'mMessageText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMessageText:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e010b

    const-string v2, "field \'topScrub\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->topScrub:Landroid/view/View;

    .line 22
    const v1, 0x7f0e010c

    const-string v2, "field \'bottomScrub\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->bottomScrub:Landroid/view/View;

    .line 24
    const v1, 0x7f0e013b

    const-string v2, "field \'mNotificationIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 26
    const v1, 0x7f0e013c

    const-string v2, "field \'mNotificationScrollIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .line 28
    const v1, 0x7f0e00d2

    const-string v2, "field \'mChoiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 30
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;

    .prologue
    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainTitleText:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMainImageView:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mObservableScrollView:Lcom/navdy/hud/app/view/ObservableScrollView;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mTitleText:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mMessageText:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->topScrub:Landroid/view/View;

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->bottomScrub:Landroid/view/View;

    .line 40
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 41
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mNotificationScrollIndicator:Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    .line 42
    iput-object v0, p0, Lcom/navdy/hud/app/view/ScrollableTextPresenterLayout;->mChoiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 43
    return-void
.end method
