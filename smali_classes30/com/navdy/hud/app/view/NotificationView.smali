.class public Lcom/navdy/hud/app/view/NotificationView;
.super Landroid/widget/RelativeLayout;
.source "NotificationView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;


# static fields
.field private static final CLICK_COUNT:I = 0x2

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014a
    .end annotation
.end field

.field private borderListener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

.field private clickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

.field private clickGestureListener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

.field customNotificationContainer:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0149
    .end annotation
.end field

.field private defaultColor:I

.field private initialized:Z

.field public nextNotificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0148
    .end annotation
.end field

.field private notification:Lcom/navdy/hud/app/framework/notifications/INotification;

.field private notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field presenter:Lcom/navdy/hud/app/presenter/NotificationPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/view/NotificationView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/view/NotificationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    new-instance v0, Lcom/navdy/hud/app/view/NotificationView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/NotificationView$1;-><init>(Lcom/navdy/hud/app/view/NotificationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->clickGestureListener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    .line 91
    new-instance v0, Lcom/navdy/hud/app/view/NotificationView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/NotificationView$2;-><init>(Lcom/navdy/hud/app/view/NotificationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->borderListener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

    .line 108
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/NotificationView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 110
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 111
    new-instance v0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->clickGestureListener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;-><init>(ILcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V

    iput-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->clickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    .line 113
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/view/NotificationView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/view/NotificationView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/view/NotificationView;->takeNotificationAction(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/INotification;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/view/NotificationView;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/view/NotificationView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/view/NotificationView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/NotificationView;->initialized:Z

    return v0
.end method

.method private takeNotificationAction(Z)V
    .locals 4
    .param p1, "quickly"    # Z

    .prologue
    .line 250
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 252
    :cond_0
    const/4 v0, 0x0

    .line 253
    .local v0, "completely":Z
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCurrentItemDeleteAll()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    const-string v2, "Glance_Dismiss"

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    const-string v1, "swipe"

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x1

    .line 256
    const/4 p1, 0x1

    .line 260
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1, v0, p1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 266
    .end local v0    # "completely":Z
    :cond_1
    :goto_2
    return-void

    .line 254
    .restart local v0    # "completely":Z
    :cond_2
    const-string v1, "doubleclick"

    goto :goto_0

    .line 258
    :cond_3
    const-string v2, "Glance_Open_Mini"

    iget-object v3, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz p1, :cond_4

    const-string v1, "swipe"

    :goto_3
    invoke-static {v2, v3, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v1, "doubleclick"

    goto :goto_3

    .line 262
    .end local v0    # "completely":Z
    :cond_5
    const-string v2, "Glance_Dismiss"

    iget-object v3, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz p1, :cond_6

    const-string v1, "swipe"

    :goto_4
    invoke-static {v2, v3, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotification()Z

    goto :goto_2

    .line 262
    :cond_6
    const-string v1, "doubleclick"

    goto :goto_4
.end method


# virtual methods
.method public addCustomView(Lcom/navdy/hud/app/framework/notifications/INotification;Landroid/view/View;)V
    .locals 2
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x1

    .line 216
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/NotificationView;->removeCustomView()V

    .line 217
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 218
    .local v0, "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    iput-object p1, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 220
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/view/NotificationView;->initialized:Z

    .line 221
    return-void
.end method

.method public clearNotification()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 213
    return-void
.end method

.method public getCurrentNotificationViewChild()Landroid/view/View;
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 272
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNotificationContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public hideNextNotificationColor()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->nextNotificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setVisibility(I)V

    .line 247
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 119
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/NotificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/view/NotificationView;->defaultColor:I

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->presenter:Lcom/navdy/hud/app/presenter/NotificationPresenter;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->presenter:Lcom/navdy/hud/app/presenter/NotificationPresenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/presenter/NotificationPresenter;->takeView(Ljava/lang/Object;)V

    .line 122
    :cond_0
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->presenter:Lcom/navdy/hud/app/presenter/NotificationPresenter;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->presenter:Lcom/navdy/hud/app/presenter/NotificationPresenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/presenter/NotificationPresenter;->dropView(Landroid/view/View;)V

    .line 129
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 135
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    iget-object v1, p0, Lcom/navdy/hud/app/view/NotificationView;->borderListener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->setListener(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;)V

    .line 137
    invoke-virtual {p0}, Lcom/navdy/hud/app/view/NotificationView;->resetNextNotificationColor()V

    .line 138
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 6
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 148
    iget-boolean v4, p0, Lcom/navdy/hud/app/view/NotificationView;->initialized:Z

    if-nez v4, :cond_1

    .line 197
    :cond_0
    :goto_0
    return v2

    .line 152
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz v4, :cond_0

    .line 156
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v4, :cond_0

    .line 157
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isAnimating()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 159
    sget-object v2, Lcom/navdy/hud/app/view/NotificationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "ignore gesture, animation in progress"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v2, v3

    .line 160
    goto :goto_0

    .line 163
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/view/NotificationView$3;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v4, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    :cond_3
    :goto_1
    move v2, v3

    .line 194
    goto :goto_0

    .line 165
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v2

    if-nez v2, :cond_5

    .line 166
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2, p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    .line 167
    .local v0, "handled":Z
    if-eqz v0, :cond_4

    .line 168
    sget-object v2, Lcom/navdy/hud/app/view/NotificationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gesture handled by notif:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 171
    :cond_4
    const-string v2, "Glance_Open_Full"

    iget-object v4, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    const-string v5, "swipe"

    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlanceAction(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/INotification;Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotification;->expandNotification()Z

    move-result v1

    .line 173
    .local v1, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/view/NotificationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gesture expanded:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 175
    .end local v0    # "handled":Z
    .end local v1    # "ret":Z
    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 177
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->moveNext(Z)V

    goto :goto_1

    .line 183
    :pswitch_1
    const/4 v0, 0x0

    .line 184
    .restart local v0    # "handled":Z
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v2

    if-nez v2, :cond_7

    .line 185
    iget-object v2, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v2, p1}, Lcom/navdy/hud/app/framework/notifications/INotification;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    .line 187
    :cond_7
    if-nez v0, :cond_8

    .line 188
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/view/NotificationView;->takeNotificationAction(Z)V

    goto/16 :goto_1

    .line 190
    :cond_8
    sget-object v2, Lcom/navdy/hud/app/view/NotificationView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gesture handled by notif:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    invoke-interface {v5}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/navdy/hud/app/view/NotificationView;->initialized:Z

    if-nez v0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 205
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->clickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 144
    return-void
.end method

.method public removeCustomView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->customNotificationContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 225
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->border:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->stopTimeout(ZLjava/lang/Runnable;)V

    .line 226
    iput-boolean v2, p0, Lcom/navdy/hud/app/view/NotificationView;->initialized:Z

    .line 227
    return-void
.end method

.method public resetNextNotificationColor()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->nextNotificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    iget v1, p0, Lcom/navdy/hud/app/view/NotificationView;->defaultColor:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 239
    return-void
.end method

.method public setNextNotificationColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 234
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->nextNotificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 235
    return-void
.end method

.method public showNextNotificationColor()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/navdy/hud/app/view/NotificationView;->nextNotificationColorView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setVisibility(I)V

    .line 243
    return-void
.end method

.method public switchNotfication(Lcom/navdy/hud/app/framework/notifications/INotification;)V
    .locals 0
    .param p1, "notification"    # Lcom/navdy/hud/app/framework/notifications/INotification;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/navdy/hud/app/view/NotificationView;->notification:Lcom/navdy/hud/app/framework/notifications/INotification;

    .line 231
    return-void
.end method
