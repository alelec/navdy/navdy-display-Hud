.class public Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "FirstLaunchScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/FirstLaunchScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/FirstLaunchView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private registered:Z

.field private stateChecked:Z

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkState()V

    return-void
.end method

.method private checkForMediaService()V
    .locals 3

    .prologue
    .line 196
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2;-><init>(Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 225
    return-void
.end method

.method private checkState()V
    .locals 4

    .prologue
    .line 150
    iget-boolean v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->stateChecked:Z

    if-eqz v1, :cond_0

    .line 151
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "state already checked"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 154
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->stateChecked:Z

    .line 155
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    const-string v1, "service.bootanim.exit"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v0

    .line 161
    .local v0, "n":I
    if-lez v0, :cond_2

    .line 162
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bonded dial count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , go to next screen, stop boot animation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 164
    const-string v1, "service.bootanim.exit"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->exitScreen()V

    goto :goto_0

    .line 167
    :cond_2
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "checkForMediaService"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 168
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkForMediaService()V

    goto :goto_0
.end method

.method private exitScreen()V
    .locals 7

    .prologue
    .line 182
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 183
    .local v1, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 184
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "go to welcome screen"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 185
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v0, "args":Landroid/os/Bundle;
    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v3, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v5, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v0, v6}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 193
    .end local v0    # "args":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 189
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "go to default screen"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 190
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 191
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    iget-object v3, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private launchDialPairing()V
    .locals 4

    .prologue
    .line 174
    const-string v1, "service.bootanim.exit"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v0

    .line 177
    .local v0, "n":I
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bonded dial count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , go to dial pairing screen"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 179
    return-void
.end method


# virtual methods
.method public onDialManagerInitEvent(Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 123
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "got dialmanager init event"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 124
    iget-boolean v1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->registered:Z

    if-eqz v1, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/FirstLaunchView;

    .line 126
    .local v0, "view":Lcom/navdy/hud/app/view/FirstLaunchView;
    if-eqz v0, :cond_0

    .line 127
    iget-object v1, v0, Lcom/navdy/hud/app/view/FirstLaunchView;->firstLaunchLogo:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/framework/toast/ToastPresenter;->ANIMATION_TRANSLATION:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 132
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 139
    .end local v0    # "view":Lcom/navdy/hud/app/view/FirstLaunchView;
    :goto_0
    return-void

    .line 134
    .restart local v0    # "view":Lcom/navdy/hud/app/view/FirstLaunchView;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkState()V

    goto :goto_0

    .line 137
    .end local v0    # "view":Lcom/navdy/hud/app/view/FirstLaunchView;
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkState()V

    goto :goto_0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 93
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onLoad"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->registered:Z

    .line 96
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "already inititalized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkState()V

    .line 103
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 105
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "systemtray:invisible"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 106
    return-void

    .line 101
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "show view"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onMediaServerUp(Lcom/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 143
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "media service is up, stop boot-animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->launchDialPairing()V

    .line 145
    return-void
.end method

.method protected onUnload()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 110
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onUnload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 111
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->registered:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 114
    :cond_0
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 117
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "systemtray:visible"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 118
    return-void
.end method
