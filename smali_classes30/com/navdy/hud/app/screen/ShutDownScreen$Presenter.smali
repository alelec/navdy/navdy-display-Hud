.class public Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "ShutDownScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/ShutDownScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/ShutDownConfirmationView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field obdManager:Lcom/navdy/hud/app/obd/ObdManager;

.field shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 4

    .prologue
    .line 154
    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 155
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$State;->CANCELED:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;Lcom/navdy/hud/app/event/Shutdown$State;)V

    .line 156
    .local v0, "event":Lcom/navdy/hud/app/event/Shutdown;
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Posting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public getCurrentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->getCurrentVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShutDownCause()Lcom/navdy/hud/app/event/Shutdown$Reason;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    return-object v0
.end method

.method public getUpdateVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->getSWUpdateVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public installAndShutDown()V
    .locals 6

    .prologue
    .line 117
    const-string v0, "INSTALL_UPDATE_SHUTDOWN"

    .line 119
    .local v0, "command":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getConnectionType()Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->OBD:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    if-ne v4, v5, :cond_0

    .line 120
    iget-object v4, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v2

    .line 121
    .local v2, "voltage":D
    const-wide v4, 0x402a333340000000L    # 13.100000381469727

    cmpg-double v4, v2, v4

    if-gez v4, :cond_0

    .line 123
    const-string v0, "INSTALL_UPDATE_REBOOT_QUIET"

    .line 126
    .end local v2    # "voltage":D
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "COMMAND"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 129
    return-void
.end method

.method public installDialUpdateAndShutDown()V
    .locals 5

    .prologue
    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "PROGRESS_CAUSE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method public isDialFirmwareUpdatePending()Z
    .locals 2

    .prologue
    .line 165
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    .line 166
    .local v0, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSoftwareUpdatePending()Z
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->isUpdateAvailable()Z

    move-result v0

    return v0
.end method

.method public onDriving(Lcom/navdy/hud/app/event/DrivingStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DrivingStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 148
    iget-boolean v0, p1, Lcom/navdy/hud/app/event/DrivingStateChange;->driving:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->getShutDownCause()Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    if-ne v0, v1, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->finish()V

    .line 151
    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 79
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 81
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 82
    iget-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 85
    if-eqz p1, :cond_0

    .line 86
    :try_start_0
    const-string v2, "SHUTDOWN_CAUSE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "reason":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    .end local v1    # "reason":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    if-nez v2, :cond_1

    .line 95
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "defaulting to inactivity reason"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 96
    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    iput-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 99
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 100
    iget-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 101
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 102
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->disableInactivityShutdown(Z)V

    .line 103
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to parse shutdown reason "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    iget-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    if-nez v2, :cond_1

    .line 95
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "defaulting to inactivity reason"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 96
    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    iput-object v2, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    goto :goto_0

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    if-nez v3, :cond_2

    .line 95
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "defaulting to inactivity reason"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 96
    sget-object v3, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    iput-object v3, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    :cond_2
    throw v2
.end method

.method protected onUnload()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 107
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onUnload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 110
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 111
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->disableInactivityShutdown(Z)V

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 113
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 114
    return-void
.end method

.method public shutDown()V
    .locals 4

    .prologue
    .line 132
    new-instance v0, Lcom/navdy/hud/app/event/Shutdown;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->shutDownCause:Lcom/navdy/hud/app/event/Shutdown$Reason;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMED:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;Lcom/navdy/hud/app/event/Shutdown$State;)V

    .line 133
    .local v0, "event":Lcom/navdy/hud/app/event/Shutdown;
    # getter for: Lcom/navdy/hud/app/screen/ShutDownScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/ShutDownScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Posting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 135
    return-void
.end method
