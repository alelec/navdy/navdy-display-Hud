.class final enum Lcom/navdy/hud/app/screen/WelcomeScreen$State;
.super Ljava/lang/Enum;
.source "WelcomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/screen/WelcomeScreen$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum CONNECTING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum CONNECTION_FAILED:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum LAUNCHING_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum UNKNOWN:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field public static final enum WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->UNKNOWN:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 110
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "DOWNLOAD_APP"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 112
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 114
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "PICKING"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 116
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 118
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "LAUNCHING_APP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->LAUNCHING_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 120
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "CONNECTION_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTION_FAILED:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 122
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    const-string v1, "WELCOME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 107
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->UNKNOWN:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->LAUNCHING_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTION_FAILED:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->$VALUES:[Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/screen/WelcomeScreen$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 107
    const-class v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/screen/WelcomeScreen$State;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->$VALUES:[Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/screen/WelcomeScreen$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    return-object v0
.end method
