.class public Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "AutoBrightnessScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/AutoBrightnessScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/AutoBrightnessView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final POS_ACTION:I


# instance fields
.field private autoBrightnessOffListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private autoBrightnessOnListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private resources:Landroid/content/res/Resources;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private view:Lcom/navdy/hud/app/view/AutoBrightnessView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    .line 74
    new-instance v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->autoBrightnessOnListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 86
    new-instance v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;-><init>(Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->autoBrightnessOffListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->setAutoBrightness(Z)V

    return-void
.end method

.method private getChoicesOff()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v2, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090023

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "choiceEnable":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "choiceCancel":Ljava/lang/String;
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-direct {v3, v1, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-direct {v3, v0, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    return-object v2
.end method

.method private getChoicesOn()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 151
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v2, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090022

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "choiceDisable":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "choiceCancel":Ljava/lang/String;
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-direct {v3, v1, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-direct {v3, v0, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    return-object v2
.end method

.method private init(Lcom/navdy/hud/app/view/AutoBrightnessView;)V
    .locals 6
    .param p1, "view"    # Lcom/navdy/hud/app/view/AutoBrightnessView;

    .prologue
    .line 125
    const-string v2, "true"

    iget-object v3, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "screen.auto_brightness"

    const-string v5, ""

    .line 126
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 125
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 131
    .local v1, "isAutoBrightnessOn":Z
    if-eqz v1, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->getChoicesOn()Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "autoBrightnessOnChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setTitle(Ljava/lang/String;)V

    .line 135
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setStatusLabel(Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f020170

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 137
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->autoBrightnessOnListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {p1, v0, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setChoices(Ljava/util/List;Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 147
    :goto_0
    iput-object p1, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->view:Lcom/navdy/hud/app/view/AutoBrightnessView;

    .line 148
    return-void

    .line 139
    .end local v0    # "autoBrightnessOnChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->getChoicesOff()Ljava/util/List;

    move-result-object v0

    .line 141
    .restart local v0    # "autoBrightnessOnChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setTitle(Ljava/lang/String;)V

    .line 142
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setStatusLabel(Ljava/lang/String;)V

    .line 143
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f020171

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 144
    iget-object v2, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->autoBrightnessOffListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {p1, v0, v2}, Lcom/navdy/hud/app/view/AutoBrightnessView;->setChoices(Ljava/util/List;Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    goto :goto_0
.end method

.method private setAutoBrightness(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 100
    iget-object v1, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 101
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "screen.auto_brightness"

    if-eqz p1, :cond_0

    const-string v1, "true"

    :goto_0
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 104
    const-string v2, "persist.sys.autobrightness"

    if-eqz p1, :cond_1

    const-string v1, "enabled"

    :goto_1
    invoke-static {v2, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void

    .line 101
    :cond_0
    const-string v1, "false"

    goto :goto_0

    .line 104
    :cond_1
    const-string v1, "disabled"

    goto :goto_1
.end method


# virtual methods
.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 187
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 175
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->view:Lcom/navdy/hud/app/view/AutoBrightnessView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/AutoBrightnessView;->moveSelectionLeft()V

    goto :goto_0

    .line 179
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->view:Lcom/navdy/hud/app/view/AutoBrightnessView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/AutoBrightnessView;->moveSelectionRight()V

    goto :goto_0

    .line 183
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->view:Lcom/navdy/hud/app/view/AutoBrightnessView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/AutoBrightnessView;->executeSelectedItem()V

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->resources:Landroid/content/res/Resources;

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 113
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->updateView()V

    .line 114
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 115
    return-void
.end method

.method protected updateView()V
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/AutoBrightnessView;

    .line 119
    .local v0, "view":Lcom/navdy/hud/app/view/AutoBrightnessView;
    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->init(Lcom/navdy/hud/app/view/AutoBrightnessView;)V

    .line 122
    :cond_0
    return-void
.end method
