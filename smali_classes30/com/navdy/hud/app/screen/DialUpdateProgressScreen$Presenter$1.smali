.class Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$1;
.super Ljava/lang/Object;
.source "DialUpdateProgressScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->finish(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    iget-object v0, v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/Shutdown;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->DIAL_OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 111
    return-void
.end method
