.class public Lcom/navdy/hud/app/screen/WelcomeScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "WelcomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;,
        Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;,
        Lcom/navdy/hud/app/screen/WelcomeScreen$State;,
        Lcom/navdy/hud/app/screen/WelcomeScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030063
.end annotation


# static fields
.field public static ACTION_RECONNECT:Ljava/lang/String; = null

.field public static ACTION_SWITCH_PHONE:Ljava/lang/String; = null

.field private static final APP_LAUNCH_TIMEOUT:I = 0x2ee0

.field public static ARG_ACTION:Ljava/lang/String; = null

.field private static final CONNECTION_TIMEOUT:I = 0x1388

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/WelcomeScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;

    .line 65
    const-string v0, "action"

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    .line 69
    const-string v0, "switch"

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_SWITCH_PHONE:Ljava/lang/String;

    .line 72
    const-string v0, "reconnect"

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 95
    const/high16 v0, 0x10a0000

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 100
    const v0, 0x10a0001

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Module;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
