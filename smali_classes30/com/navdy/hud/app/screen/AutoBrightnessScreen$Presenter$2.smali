.class Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;
.super Ljava/lang/Object;
.source "AutoBrightnessScreen.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    const/4 v1, 0x1

    # invokes: Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->setAutoBrightness(Z)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->access$000(Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;Z)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;

    iget-object v0, v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 96
    return-void
.end method
