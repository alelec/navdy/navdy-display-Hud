.class public Lcom/navdy/hud/app/screen/TemperatureWarningScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "TemperatureWarningScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Presenter;,
        Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030081
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/TemperatureWarningScreen$Module;-><init>(Lcom/navdy/hud/app/screen/TemperatureWarningScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/navdy/hud/app/screen/TemperatureWarningScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
