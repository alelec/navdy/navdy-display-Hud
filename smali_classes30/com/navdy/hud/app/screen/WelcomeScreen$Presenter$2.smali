.class Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;
.super Ljava/lang/Object;
.source "WelcomeScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 396
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appLaunch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appConnected:Z
    invoke-static {v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$300(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appConnected:Z
    invoke-static {v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$300(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_VALID:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    if-eq v0, v1, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "showing app dis-connected toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 401
    invoke-static {v3}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showDisconnectedToast(Z)V

    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # setter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunched:Z
    invoke-static {v0, v3}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$402(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Z)Z

    goto :goto_0
.end method
