.class Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1$1;
.super Ljava/lang/Object;
.source "FactoryResetScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;->executeItem(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1$1;->this$1:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 100
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/os/RecoverySystem;->rebootWipeUserData(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/screen/FactoryResetScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FactoryResetScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Failed to do factory reset"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 103
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1$1;->this$1:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;

    iget-object v1, v1, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    iget-object v1, v1, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
