.class Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;
.super Ljava/lang/Object;
.source "WelcomeScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 384
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checking connection timeout - connected:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connected:Z
    invoke-static {v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$100(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connected:Z
    invoke-static {v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$100(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTION_FAILED:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    # invokes: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$200(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    goto :goto_0
.end method
