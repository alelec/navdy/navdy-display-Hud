.class Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;
.super Ljava/lang/Object;
.source "DialUpdateProgressScreen.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->startUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;->this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
    .param p2, "detail"    # Ljava/lang/String;

    .prologue
    .line 160
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFinished:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 161
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->access$000()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3$2;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 167
    return-void
.end method

.method public onProgress(I)V
    .locals 2
    .param p1, "percentage"    # I

    .prologue
    .line 147
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->access$000()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3$1;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method
