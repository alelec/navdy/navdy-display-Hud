.class public Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "WelcomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/WelcomeView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final MESSAGE_TIMEOUT:I = 0x9c4

.field public static final MISSING_DEVICES_OFFSET:I = 0x64


# instance fields
.field private appConnected:Z

.field private appLaunchTimeout:Ljava/lang/Runnable;

.field private appLaunched:Z

.field private appWaitTimeout:Ljava/lang/Runnable;

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private connected:Z

.field private connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

.field connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private currentItem:I

.field private deviceConnectTimeout:Ljava/lang/Runnable;

.field private deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

.field private dirty:Z

.field private failedConnection:Lcom/navdy/service/library/device/NavdyDeviceId;

.field private firstUpdate:Z

.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private ledUpdated:Z

.field private mGreetingPending:Ljava/util/concurrent/atomic/AtomicBoolean;

.field pairingManager:Lcom/navdy/hud/app/manager/PairingManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field profileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searching:Z

.field private state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

.field private stateTimeout:Ljava/lang/Runnable;

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    .line 163
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .line 168
    iput-boolean v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->searching:Z

    .line 169
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->UNKNOWN:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    .line 175
    iput-boolean v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 183
    iput-boolean v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->ledUpdated:Z

    .line 184
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->mGreetingPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 381
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceConnectTimeout:Ljava/lang/Runnable;

    .line 393
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$2;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunchTimeout:Ljava/lang/Runnable;

    .line 406
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$3;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appWaitTimeout:Ljava/lang/Runnable;

    .line 415
    new-instance v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connected:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->searching:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appConnected:Z

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunched:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Lcom/navdy/hud/app/screen/WelcomeScreen$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
    .param p1, "x1"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private buildAddDriverModel(Z)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 6
    .param p1, "driversFound"    # Z

    .prologue
    const v4, 0x7f0200cc

    .line 716
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/WelcomeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 717
    .local v1, "resources":Landroid/content/res/Resources;
    if-eqz p1, :cond_0

    const v2, 0x7f09030b

    .line 719
    .local v2, "title":I
    :goto_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;-><init>()V

    .line 720
    .local v0, "addDriver":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    const v3, 0x7f0e0094

    iput v3, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    .line 721
    iput v4, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageRes:I

    .line 722
    iput v4, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->largeImageRes:I

    .line 723
    new-instance v3, Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    .line 724
    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v4, 0x7f0e00c3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v4, 0x7f0e0111

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0x7f0902fe

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    return-object v0

    .line 717
    .end local v0    # "addDriver":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .end local v2    # "title":I
    :cond_0
    const v2, 0x7f090307

    goto :goto_0
.end method

.method private buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;I)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 1
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "i"    # I

    .prologue
    const/4 v0, 0x0

    .line 676
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;III)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v0

    return-object v0
.end method

.method private buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;II)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 1
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "i"    # I
    .param p3, "titleResId"    # I

    .prologue
    .line 680
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;III)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v0

    return-object v0
.end method

.method private buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;III)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 9
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "i"    # I
    .param p3, "titleResId"    # I
    .param p4, "messageResId"    # I

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/view/WelcomeView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 685
    .local v5, "resources":Landroid/content/res/Resources;
    new-instance v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;-><init>()V

    .line 686
    .local v1, "driver":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    iput p2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    .line 687
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 688
    .local v0, "deviceName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getDriverImageResId(Ljava/lang/String;)I

    move-result v4

    .line 689
    .local v4, "resId":I
    iput v4, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageRes:I

    .line 690
    iput v4, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->largeImageRes:I

    .line 691
    new-instance v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;

    iget-object v6, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->profileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v6, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v6

    invoke-direct {v2, p1, v6}, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 692
    .local v2, "metaData":Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;
    iput-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->extras:Ljava/lang/Object;

    .line 693
    new-instance v6, Ljava/util/HashMap;

    const/4 v7, 0x4

    invoke-direct {v6, v7}, Ljava/util/HashMap;-><init>(I)V

    iput-object v6, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    .line 694
    iget-object v6, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v7, 0x7f0e00c3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    iget-object v6, v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v6, :cond_1

    iget-object v6, v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v3

    .line 696
    .local v3, "profileName":Ljava/lang/String;
    :goto_0
    iget-object v6, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v7, 0x7f0e0111

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    if-eqz p4, :cond_0

    .line 698
    iget-object v6, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v7, 0x7f0e010a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    :cond_0
    return-object v1

    .end local v3    # "profileName":Ljava/lang/String;
    :cond_1
    move-object v3, v0

    .line 695
    goto :goto_0
.end method

.method private buildSearchingModel()Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 5

    .prologue
    const v3, 0x7f0200e8

    .line 704
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/WelcomeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/WelcomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 705
    .local v0, "resources":Landroid/content/res/Resources;
    new-instance v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;-><init>()V

    .line 706
    .local v1, "searching":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    const v2, 0x7f0e0096

    iput v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    .line 707
    iput v3, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageRes:I

    .line 708
    iput v3, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->largeImageRes:I

    .line 709
    new-instance v2, Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    .line 710
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v3, 0x7f0e00c3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f090306

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    const v3, 0x7f0e0111

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 712
    return-object v1
.end method

.method private clearCallbacks()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceConnectTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 308
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunchTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appWaitTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 310
    return-void
.end method

.method private clearLaunchNotification()V
    .locals 2

    .prologue
    .line 313
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunched:Z

    .line 314
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->clearCallbacks()V

    .line 317
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 318
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v1, "disconnection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 319
    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 320
    return-void
.end method

.method private hasPaired()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->hasPaired()Z

    move-result v0

    return v0
.end method

.method private knownDevices()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/device/NavdyDeviceId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v2

    .line 667
    .local v2, "registry":Lcom/navdy/service/library/device/RemoteDeviceRegistry;
    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getPairedConnections()Ljava/util/List;

    move-result-object v1

    .line 668
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 669
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/NavdyDeviceId;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 670
    .local v0, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 672
    .end local v0    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_0
    return-object v3
.end method

.method private sayWelcome(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 6
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 349
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->mGreetingPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v3, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 351
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f09030a

    new-array v3, v3, [Ljava/lang/Object;

    .line 352
    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getFirstName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 351
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "message":Ljava/lang/String;
    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_WELCOME_MESSAGE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 355
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "r":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method private selectDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 4
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 735
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to select "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 737
    .local v0, "currentDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 738
    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 739
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 749
    :goto_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 750
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    .line 751
    return-void

    .line 741
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 742
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$7;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$7;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 1
    .param p1, "device"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 586
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->failedConnection:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 589
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    .line 590
    return-void
.end method

.method private setSearching(Z)V
    .locals 1
    .param p1, "searching"    # Z

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->searching:Z

    if-eq v0, p1, :cond_0

    .line 580
    iput-boolean p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->searching:Z

    .line 581
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 583
    :cond_0
    return-void
.end method

.method private setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V
    .locals 6
    .param p1, "state"    # Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .prologue
    const-wide/16 v4, 0x9c4

    const/4 v3, 0x1

    .line 262
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-eq v0, p1, :cond_1

    .line 263
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    .line 265
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->pairingManager:Lcom/navdy/hud/app/manager/PairingManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/PairingManager;->setAutoPairing(Z)V

    .line 267
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->clearCallbacks()V

    .line 268
    iput-boolean v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 270
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switching to state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 271
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateViewState()V

    .line 303
    :cond_1
    return-void

    .line 273
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 274
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 277
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->mGreetingPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 278
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 279
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 282
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->stopSearch()V

    .line 283
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/Disconnect;

    invoke-direct {v1}, Lcom/navdy/hud/app/event/Disconnect;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->pairingManager:Lcom/navdy/hud/app/manager/PairingManager;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/manager/PairingManager;->setAutoPairing(Z)V

    goto :goto_0

    .line 288
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->deviceConnectTimeout:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 291
    :pswitch_4
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunchTimeout:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 295
    :pswitch_5
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->failedConnection:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    goto :goto_0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method private startSearchIfNeeded()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 251
    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->serviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->searchForDevices()V

    .line 255
    :cond_0
    return-void
.end method

.method private updatePairingStatusOnLED(Z)V
    .locals 4
    .param p1, "pairing"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 777
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->ledUpdated:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 778
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    .line 779
    iput-boolean v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->ledUpdated:Z

    .line 784
    :cond_0
    :goto_0
    return-void

    .line 780
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->ledUpdated:Z

    if-eqz v0, :cond_0

    .line 781
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    .line 782
    iput-boolean v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->ledUpdated:Z

    goto :goto_0
.end method

.method private updateViewState()V
    .locals 4

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/WelcomeView;

    .line 359
    .local v0, "view":Lcom/navdy/hud/app/view/WelcomeView;
    if-eqz v0, :cond_0

    .line 360
    const/4 v1, -0x1

    .line 361
    .local v1, "viewState":I
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    iget-object v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v3}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 375
    :goto_0
    :pswitch_0
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 376
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/WelcomeView;->setState(I)V

    .line 379
    .end local v1    # "viewState":I
    :cond_0
    return-void

    .line 363
    .restart local v1    # "viewState":I
    :pswitch_1
    const/4 v1, 0x1

    .line 364
    goto :goto_0

    .line 366
    :pswitch_2
    const/4 v1, 0x2

    .line 367
    goto :goto_0

    .line 372
    :pswitch_3
    const/4 v1, 0x3

    goto :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->stopSearch()V

    .line 439
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->finish()V

    .line 440
    return-void
.end method

.method public executeItem(II)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 754
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "execute: id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pos:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 756
    const v4, 0x7f0e0096

    if-eq p1, v4, :cond_0

    const v4, 0x7f0e0095

    if-ne p1, v4, :cond_1

    .line 774
    :cond_0
    :goto_0
    return-void

    .line 759
    :cond_1
    const v4, 0x7f0e0094

    if-ne p1, v4, :cond_2

    .line 760
    sget-object v4, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_0

    .line 762
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/WelcomeView;

    .line 763
    .local v3, "view":Lcom/navdy/hud/app/view/WelcomeView;
    if-eqz v3, :cond_0

    .line 766
    iget-object v4, v3, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v4, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getModel(I)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v1

    .line 768
    .local v1, "model":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    :try_start_0
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->extras:Ljava/lang/Object;

    check-cast v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;

    .line 769
    .local v2, "selectedDevice":Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;
    iget-object v4, v2, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->selectDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 770
    .end local v2    # "selectedDevice":Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;
    :catch_0
    move-exception v0

    .line 771
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to select device at pos "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public finish()V
    .locals 4

    .prologue
    .line 444
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 445
    .local v0, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    if-eq v1, v2, :cond_0

    .line 446
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finish: not switching: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 452
    :goto_0
    return-void

    .line 450
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finish: switching: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 451
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 7
    .param p1, "stateChange"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 456
    const/4 v0, 0x0

    .line 458
    .local v0, "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :try_start_0
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v2, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .local v1, "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    move-object v0, v1

    .line 462
    .end local v1    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v0    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connection state change:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 463
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 508
    :goto_1
    return-void

    .line 465
    :pswitch_0
    iput-boolean v5, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connected:Z

    .line 469
    if-nez v0, :cond_1

    .line 470
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->startSearchIfNeeded()V

    .line 507
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    goto :goto_1

    .line 471
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 472
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->clearLaunchNotification()V

    .line 474
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_2

    .line 478
    :pswitch_1
    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    goto :goto_2

    .line 481
    :pswitch_2
    iput-boolean v6, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appConnected:Z

    .line 482
    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 483
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_2

    .line 488
    :pswitch_3
    if-eqz v0, :cond_0

    .line 489
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_2

    .line 496
    :pswitch_4
    iput-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 497
    iput-boolean v6, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connected:Z

    .line 498
    iput-boolean v5, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appConnected:Z

    .line 499
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updatePairingStatusOnLED(Z)V

    .line 500
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 501
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->LAUNCHING_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_2

    .line 459
    :catch_0
    move-exception v2

    goto/16 :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onConnectionStatus(Lcom/navdy/service/library/events/connection/ConnectionStatus;)V
    .locals 7
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 512
    const/4 v0, 0x0

    .line 514
    .local v0, "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :try_start_0
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v2, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .local v1, "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    move-object v0, v1

    .line 518
    .end local v1    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v0    # "remoteDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectionStatus: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 520
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 555
    :goto_1
    return-void

    .line 522
    :pswitch_0
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setSearching(Z)V

    .line 523
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updatePairingStatusOnLED(Z)V

    .line 554
    :cond_0
    :goto_2
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    goto :goto_1

    .line 526
    :pswitch_2
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setSearching(Z)V

    .line 527
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updatePairingStatusOnLED(Z)V

    .line 528
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-ne v2, v3, :cond_0

    .line 530
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/ConnectionHandler;->stopSearch()V

    .line 531
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->finish()V

    goto :goto_2

    .line 535
    :pswitch_3
    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-ne v2, v3, :cond_0

    .line 537
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->selectDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    goto :goto_2

    .line 544
    :pswitch_4
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$5;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$5;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_1

    .line 515
    :catch_0
    move-exception v2

    goto :goto_0

    .line 520
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public onCurrentItemChanged(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 730
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCurrentItemChanged id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 731
    iput p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    .line 732
    return-void
.end method

.method public onDisconnect(Lcom/navdy/hud/app/event/Disconnect;)V
    .locals 2
    .param p1, "disconnect"    # Lcom/navdy/hud/app/event/Disconnect;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunched:Z

    .line 342
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->clearCallbacks()V

    .line 343
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-eq v0, v1, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->finish()V

    .line 346
    :cond_0
    return-void
.end method

.method public onDismissToast(Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 559
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunched:Z

    if-eqz v0, :cond_0

    const-string v0, "disconnection#toast"

    iget-object v1, p1, Lcom/navdy/hud/app/framework/toast/ToastManager$DismissedToast;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "launching app timeout"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 562
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appLaunchTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 563
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appWaitTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 564
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->appWaitTimeout:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 566
    :cond_0
    return-void
.end method

.method public onDriverProfileUpdated(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 324
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->profileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 325
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-ne v1, v2, :cond_0

    .line 326
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sayWelcome profile["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] FN["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getFirstName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    .line 328
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->sayWelcome(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 329
    iget-object v1, p1, Lcom/navdy/hud/app/event/DriverProfileUpdated;->state:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    sget-object v2, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    if-ne v1, v2, :cond_0

    .line 331
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 332
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    const-wide/16 v4, 0x9c4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 335
    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 190
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 191
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 193
    const/4 v0, 0x0

    .line 194
    .local v0, "action":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 195
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoad - action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 199
    iput-boolean v4, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->firstUpdate:Z

    .line 200
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 202
    iput v5, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    .line 203
    iput-boolean v4, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 205
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_SWITCH_PHONE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 207
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->hasPaired()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 240
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    .line 241
    :goto_1
    return-void

    .line 210
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_0

    .line 212
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->UNKNOWN:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    if-ne v1, v2, :cond_5

    .line 213
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->hasPaired()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 214
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setSearching(Z)V

    .line 215
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 216
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->startSearchIfNeeded()V

    goto :goto_0

    .line 218
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    goto :goto_0

    .line 222
    :cond_5
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 227
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-nez v1, :cond_6

    .line 229
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->finish()V

    goto :goto_1

    .line 232
    :cond_6
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 233
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->stateTimeout:Ljava/lang/Runnable;

    const-wide/16 v4, 0x9c4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 236
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->startSearchIfNeeded()V

    goto :goto_0

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPhotoDownload(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 570
    iget-object v1, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v2, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne v1, v2, :cond_0

    iget-boolean v1, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    if-nez v1, :cond_0

    .line 571
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/WelcomeView;

    .line 572
    .local v0, "view":Lcom/navdy/hud/app/view/WelcomeView;
    if-eqz v0, :cond_0

    .line 573
    iget-object v1, v0, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->reload()V

    .line 576
    .end local v0    # "view":Lcom/navdy/hud/app/view/WelcomeView;
    :cond_0
    return-void
.end method

.method protected onUnload()V
    .locals 2

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updatePairingStatusOnLED(Z)V

    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 247
    return-void
.end method

.method protected updateView()V
    .locals 15

    .prologue
    const/4 v8, 0x1

    const v14, 0x7f0e0095

    const/4 v9, 0x0

    .line 593
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/view/WelcomeView;

    .line 594
    .local v7, "view":Lcom/navdy/hud/app/view/WelcomeView;
    if-eqz v7, :cond_1

    .line 595
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateViewState()V

    .line 596
    iget-boolean v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    if-eqz v10, :cond_1

    .line 597
    iget-boolean v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->firstUpdate:Z

    if-eqz v10, :cond_2

    .line 598
    iput-boolean v9, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->firstUpdate:Z

    .line 599
    iget-object v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->handler:Landroid/os/Handler;

    new-instance v11, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$6;

    invoke-direct {v11, p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$6;-><init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V

    const-wide/16 v12, 0x3e8

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 611
    :goto_0
    iput-boolean v9, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->dirty:Z

    .line 612
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 614
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    sget-object v10, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    iget-object v11, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v11}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 660
    :cond_0
    :goto_1
    :pswitch_0
    iget-object v8, v7, Lcom/navdy/hud/app/view/WelcomeView;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    invoke-virtual {v8, v4, v10, v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setModel(Ljava/util/List;IZ)V

    .line 663
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    :cond_1
    return-void

    .line 609
    :cond_2
    iget-boolean v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->searching:Z

    invoke-virtual {v7, v10}, Lcom/navdy/hud/app/view/WelcomeView;->setSearching(Z)V

    goto :goto_0

    .line 617
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    :pswitch_1
    iget-object v8, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    const v10, 0x7f090302

    invoke-direct {p0, v8, v14, v10}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;II)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 622
    :pswitch_2
    iget-object v8, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    const v10, 0x7f090301

    const v11, 0x7f0902ff

    invoke-direct {p0, v8, v14, v10, v11}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;III)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 628
    :pswitch_3
    iget-object v8, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;

    const v10, 0x7f090308

    invoke-direct {p0, v8, v14, v10}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;II)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 633
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildSearchingModel()Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 636
    :pswitch_5
    invoke-direct {p0, v8}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildAddDriverModel(Z)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638
    const/4 v3, 0x1

    .line 639
    .local v3, "i":I
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->knownDevices()Ljava/util/List;

    move-result-object v2

    .line 640
    .local v2, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/NavdyDeviceId;>;"
    iget-object v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v10}, Lcom/navdy/hud/app/service/ConnectionHandler;->getConnectedDevice()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 642
    .local v0, "connectedDevice":Lcom/navdy/service/library/device/NavdyDeviceId;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_4

    :goto_2
    iput v8, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    .line 643
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 644
    .local v1, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    const v6, 0x7f09030b

    .line 645
    .local v6, "title":I
    const/4 v5, 0x0

    .line 646
    .local v5, "message":I
    iget-object v10, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->failedConnection:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 648
    iput v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->currentItem:I

    .line 649
    const v6, 0x7f090301

    .line 650
    const v5, 0x7f0902ff

    .line 655
    :cond_3
    :goto_4
    invoke-direct {p0, v1, v3, v6, v5}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->buildDriverModel(Lcom/navdy/service/library/device/NavdyDeviceId;III)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 656
    add-int/lit8 v3, v3, 0x1

    .line 657
    goto :goto_3

    .end local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v5    # "message":I
    .end local v6    # "title":I
    :cond_4
    move v8, v9

    .line 642
    goto :goto_2

    .line 651
    .restart local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v5    # "message":I
    .restart local v6    # "title":I
    :cond_5
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 652
    const v6, 0x7f090303

    goto :goto_4

    .line 614
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
