.class public Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "OSUpdateConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Presenter;,
        Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030062
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen$Module;-><init>(Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
