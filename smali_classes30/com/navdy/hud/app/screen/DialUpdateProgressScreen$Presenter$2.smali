.class Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$2;
.super Ljava/lang/Object;
.source "DialUpdateProgressScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->finish(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "OtaDialNameKey"

    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getDialName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    iget-object v1, v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 129
    return-void
.end method
