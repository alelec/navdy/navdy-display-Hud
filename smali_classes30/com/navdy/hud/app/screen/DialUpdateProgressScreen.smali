.class public Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "DialUpdateProgressScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;,
        Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f03004a
.end annotation


# static fields
.field private static final DIAL_UPDATE_ERROR_TOAST_ID:Ljava/lang/String; = "dial-fw-update-err"

.field public static final EXTRA_PROGRESS_CAUSE:Ljava/lang/String; = "PROGRESS_CAUSE"

.field public static final POST_OTA_PAIRING_DELAY:I = 0x1770

.field public static final POST_OTA_SHUTDOWN_DELAY:I = 0x7530

.field public static final SETTINGS_SCREEN:I = 0x1

.field public static final SHUTDOWN_SCREEN:I = 0x2

.field private static handler:Landroid/os/Handler;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field public static updateStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;

    .line 74
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->updateStarted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
