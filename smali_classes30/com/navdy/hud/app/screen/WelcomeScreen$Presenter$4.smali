.class Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;
.super Ljava/lang/Object;
.source "WelcomeScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    .prologue
    .line 415
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 418
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # invokes: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$500(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/WelcomeView;

    .line 419
    .local v0, "view":Lcom/navdy/hud/app/view/WelcomeView;
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "State timeout fired - current state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;
    invoke-static {v3}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$600(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " current view:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 420
    if-eqz v0, :cond_0

    .line 421
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    iget-object v2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->state:Lcom/navdy/hud/app/screen/WelcomeScreen$State;
    invoke-static {v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$600(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;)Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 434
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 423
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    # invokes: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->setState(Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$200(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/hud/app/screen/WelcomeScreen$State;)V

    .line 424
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->updateView()V

    goto :goto_0

    .line 427
    :pswitch_2
    # getter for: Lcom/navdy/hud/app/screen/WelcomeScreen;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Welcome timed out - going back"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->connectingDevice:Lcom/navdy/service/library/device/NavdyDeviceId;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->access$702(Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 429
    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$Presenter;->finish()V

    goto :goto_0

    .line 421
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
