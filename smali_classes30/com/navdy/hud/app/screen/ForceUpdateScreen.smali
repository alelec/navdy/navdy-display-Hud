.class public Lcom/navdy/hud/app/screen/ForceUpdateScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "ForceUpdateScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;,
        Lcom/navdy/hud/app/screen/ForceUpdateScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f03001a
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/ForceUpdateScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/ForceUpdateScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Module;-><init>(Lcom/navdy/hud/app/screen/ForceUpdateScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/navdy/hud/app/screen/ForceUpdateScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
