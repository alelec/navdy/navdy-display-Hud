.class public final Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "ShutDownScreen$Presenter$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private gestureServiceConnector:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/gesture/GestureServiceConnector;",
            ">;"
        }
    .end annotation
.end field

.field private mBus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private supertype:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/BasePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 32
    const-string v0, "com.navdy.hud.app.screen.ShutDownScreen$Presenter"

    const-string v1, "members/com.navdy.hud.app.screen.ShutDownScreen$Presenter"

    const/4 v2, 0x1

    const-class v3, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 33
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 6
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 42
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mBus:Ldagger/internal/Binding;

    .line 43
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mPreferences:Ldagger/internal/Binding;

    .line 44
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    const-class v1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    .line 45
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-class v1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    .line 46
    const-string v1, "members/com.navdy.hud.app.ui.framework.BasePresenter"

    const-class v2, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;ZZ)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    .line 47
    return-void
.end method

.method public get()Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-direct {v0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;-><init>()V

    .line 69
    .local v0, "result":Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;)V

    .line 70
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->get()Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mBus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mPreferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mBus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->mPreferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->mPreferences:Landroid/content/SharedPreferences;

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iput-object v0, p1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-virtual {v0, p1}, Ldagger/internal/Binding;->injectMembers(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/screen/ShutDownScreen$Presenter;)V

    return-void
.end method
