.class public Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "DialUpdateConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/DialUpdateConfirmationView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private isReminder:Z

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 75
    return-void
.end method

.method public install()V
    .locals 5

    .prologue
    .line 67
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "PROGRESS_CAUSE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public isReminder()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->isReminder:Z

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 57
    if-eqz p1, :cond_0

    .line 58
    const-string v0, "UPDATE_REMINDER"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->isReminder:Z

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "null bundle passed to onLoad()"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 61
    iput-boolean v2, p0, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen$Presenter;->isReminder:Z

    goto :goto_0
.end method
