.class public Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "DialManagerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/DialManagerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/DialManagerView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private isLedTurnedBlue:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private scanningMode:Z

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    .line 88
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->handler:Landroid/os/Handler;

    .line 91
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->isLedTurnedBlue:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private exitScreen()V
    .locals 7

    .prologue
    .line 170
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 171
    .local v1, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "go to welcome screen"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 173
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v0, "args":Landroid/os/Bundle;
    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v3, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v5, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v0, v6}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 181
    .end local v0    # "args":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 177
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "go to default screen"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 179
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    iget-object v3, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->exitScreen()V

    .line 150
    return-void
.end method

.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public isScanningMode()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->scanningMode:Z

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 99
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "onLoad"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 100
    # setter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;
    invoke-static {p0}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$102(Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;)Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    .line 101
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 102
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 103
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 104
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "systemtray:invisible"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x0

    .line 106
    .local v0, "dialName":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 107
    const-string v1, "OtaDialNameKey"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    :cond_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->updateView(Ljava/lang/String;)V

    .line 110
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 111
    return-void
.end method

.method protected onUnload()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 115
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onUnload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x0

    # setter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;
    invoke-static {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$102(Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;)Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    .line 117
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->showPairingLed(Z)V

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 120
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 121
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "systemtray:visible"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 122
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 123
    return-void
.end method

.method public showPairingLed(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 153
    if-eqz p1, :cond_1

    .line 154
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->isLedTurnedBlue:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->isLedTurnedBlue:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    goto :goto_0
.end method

.method public updateView(Ljava/lang/String;)V
    .locals 5
    .param p1, "dialName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 127
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/DialManagerView;

    .line 128
    .local v0, "view":Lcom/navdy/hud/app/view/DialManagerView;
    if-eqz v0, :cond_0

    .line 129
    if-eqz p1, :cond_1

    .line 130
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "show searching for dial view"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    iput-boolean v3, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->scanningMode:Z

    .line 132
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 133
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/DialManagerView;->showDialSearching(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->showPairingLed(Z)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "show dial connected view"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialConnected()V

    goto :goto_0

    .line 139
    :cond_2
    # getter for: Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialManagerScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "show dial pairing view"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 140
    iput-boolean v3, p0, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->scanningMode:Z

    .line 141
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 142
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DialManagerView;->showDialPairing()V

    .line 143
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->showPairingLed(Z)V

    goto :goto_0
.end method
