.class public final Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;
.super Ldagger/internal/ModuleAdapter;
.source "WelcomeScreen$Module$$ModuleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ModuleAdapter",
        "<",
        "Lcom/navdy/hud/app/screen/WelcomeScreen$Module;",
        ">;"
    }
.end annotation


# static fields
.field private static final INCLUDES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INJECTS:[Ljava/lang/String;

.field private static final STATIC_INJECTIONS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "members/com.navdy.hud.app.view.WelcomeView"

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    .line 12
    new-array v0, v2, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    .line 13
    new-array v0, v2, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 16
    const-class v1, Lcom/navdy/hud/app/screen/WelcomeScreen$Module;

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    sget-object v5, Lcom/navdy/hud/app/screen/WelcomeScreen$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    const/4 v6, 0x1

    move-object v0, p0

    move v7, v4

    invoke-direct/range {v0 .. v7}, Ldagger/internal/ModuleAdapter;-><init>(Ljava/lang/Class;[Ljava/lang/String;[Ljava/lang/Class;Z[Ljava/lang/Class;ZZ)V

    .line 17
    return-void
.end method
