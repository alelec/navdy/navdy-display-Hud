.class public Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "DialUpdateProgressScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/DialUpdateProgressView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private cause:I

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mScreen:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private showDialUpdateErrorToast(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V
    .locals 8
    .param p1, "error"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .prologue
    const/4 v4, 0x1

    .line 177
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 178
    .local v6, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 179
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x2710

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    const-string v0, "8"

    const v1, 0x7f020103

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    const-string v0, "1"

    const v1, 0x7f0900d3

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "4"

    invoke-virtual {p1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v0, "12"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v7

    .line 186
    .local v7, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v0, "dial-fw-update-err"

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 187
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "dial-fw-update-err"

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 188
    return-void
.end method


# virtual methods
.method public cancelUpdate()V
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->cancelUpdate()V

    .line 174
    return-void
.end method

.method public finish(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V
    .locals 5
    .param p1, "error"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .prologue
    const/4 v4, 0x0

    .line 102
    sput-boolean v4, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->updateStarted:Z

    .line 103
    iget v0, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->cause:I

    packed-switch v0, :pswitch_data_0

    .line 115
    :goto_0
    :pswitch_0
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/service/ShutdownMonitor;->disableScreenDim(Z)V

    .line 116
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->NONE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    if-eq p1, v0, :cond_0

    .line 117
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 118
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 120
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->cancelUpdate()V

    .line 121
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->showDialUpdateErrorToast(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;)V

    .line 133
    :goto_1
    return-void

    .line 107
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->access$000()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 123
    :cond_0
    # getter for: Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->access$000()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$2;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v0

    return-object v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 89
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->mScreen:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->mScreen:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    iget-object v0, v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->arguments:Landroid/os/Bundle;

    .line 91
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 92
    const-string v1, "PROGRESS_CAUSE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;->cause:I

    .line 95
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public startUpdate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 140
    sget-boolean v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->updateStarted:Z

    if-nez v0, :cond_0

    .line 141
    sput-boolean v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->updateStarted:Z

    .line 142
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->disableScreenDim(Z)V

    .line 143
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter$3;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->startUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;)Z

    .line 170
    :cond_0
    return-void
.end method
