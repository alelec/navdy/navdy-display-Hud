.class Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;
.super Ljava/lang/Object;
.source "FactoryResetScreen.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 85
    packed-switch p1, :pswitch_data_0

    .line 109
    :goto_0
    return-void

    .line 87
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;->this$0:Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;

    iget-object v0, v0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->FACTORY_RESET:Lcom/navdy/hud/app/event/Shutdown$Reason;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordShutdown(Lcom/navdy/hud/app/event/Shutdown$Reason;Z)V

    .line 93
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->requestDialForgetKeys()V

    .line 94
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1$1;-><init>(Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 112
    return-void
.end method
