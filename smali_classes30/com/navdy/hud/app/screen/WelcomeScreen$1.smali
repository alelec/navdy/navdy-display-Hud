.class synthetic Lcom/navdy/hud/app/screen/WelcomeScreen$1;
.super Ljava/lang/Object;
.source "WelcomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 520
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->values()[Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_STARTED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_FOUND:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_LOST:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRED_DEVICES_CHANGED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    .line 463
    :goto_4
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    .line 222
    :goto_9
    invoke-static {}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->values()[Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->WELCOME:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->LAUNCHING_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->CONNECTION_FAILED:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->SEARCHING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->DOWNLOAD_APP:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen$1;->$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State:[I

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->PICKING:Lcom/navdy/hud/app/screen/WelcomeScreen$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/WelcomeScreen$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_10
    return-void

    :catch_0
    move-exception v0

    goto :goto_10

    :catch_1
    move-exception v0

    goto :goto_f

    :catch_2
    move-exception v0

    goto :goto_e

    :catch_3
    move-exception v0

    goto :goto_d

    :catch_4
    move-exception v0

    goto :goto_c

    :catch_5
    move-exception v0

    goto :goto_b

    :catch_6
    move-exception v0

    goto :goto_a

    .line 463
    :catch_7
    move-exception v0

    goto :goto_9

    :catch_8
    move-exception v0

    goto :goto_8

    :catch_9
    move-exception v0

    goto :goto_7

    :catch_a
    move-exception v0

    goto/16 :goto_6

    :catch_b
    move-exception v0

    goto/16 :goto_5

    .line 520
    :catch_c
    move-exception v0

    goto/16 :goto_4

    :catch_d
    move-exception v0

    goto/16 :goto_3

    :catch_e
    move-exception v0

    goto/16 :goto_2

    :catch_f
    move-exception v0

    goto/16 :goto_1

    :catch_10
    move-exception v0

    goto/16 :goto_0
.end method
