.class public Lcom/navdy/hud/app/screen/AutoBrightnessScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "AutoBrightnessScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Presenter;,
        Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030045
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/AutoBrightnessScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/AutoBrightnessScreen$Module;-><init>(Lcom/navdy/hud/app/screen/AutoBrightnessScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
