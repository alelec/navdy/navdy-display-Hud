.class public final Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;
.super Ldagger/internal/ModuleAdapter;
.source "DialUpdateProgressScreen$Module$$ModuleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ModuleAdapter",
        "<",
        "Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;",
        ">;"
    }
.end annotation


# static fields
.field private static final INCLUDES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INJECTS:[Ljava/lang/String;

.field private static final STATIC_INJECTIONS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "members/com.navdy.hud.app.view.DialUpdateProgressView"

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    .line 15
    new-array v0, v2, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    .line 16
    new-array v0, v2, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 19
    const-class v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    sget-object v2, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    sget-object v5, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    const/4 v6, 0x1

    move-object v0, p0

    move v7, v4

    invoke-direct/range {v0 .. v7}, Ldagger/internal/ModuleAdapter;-><init>(Ljava/lang/Class;[Ljava/lang/String;[Ljava/lang/Class;Z[Ljava/lang/Class;ZZ)V

    .line 20
    return-void
.end method


# virtual methods
.method public getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;)V
    .locals 2
    .param p1, "bindings"    # Ldagger/internal/BindingsGroup;
    .param p2, "module"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    .prologue
    .line 28
    const-string v0, "com.navdy.hud.app.screen.DialUpdateProgressScreen"

    new-instance v1, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;-><init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 29
    return-void
.end method

.method public bridge synthetic getBindings(Ldagger/internal/BindingsGroup;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p2, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;->getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;)V

    return-void
.end method
