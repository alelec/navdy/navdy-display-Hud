.class public Lcom/navdy/hud/app/screen/DialManagerScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "DialManagerScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;,
        Lcom/navdy/hud/app/screen/DialManagerScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030048
.end annotation


# static fields
.field private static presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/DialManagerScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/screen/DialManagerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;)Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    .prologue
    .line 38
    sput-object p0, Lcom/navdy/hud/app/screen/DialManagerScreen;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    return-object p0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 63
    const/4 v0, -0x1

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 68
    const/4 v0, -0x1

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/navdy/hud/app/screen/DialManagerScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Module;-><init>(Lcom/navdy/hud/app/screen/DialManagerScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method public isScanningMode()Z
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/navdy/hud/app/screen/DialManagerScreen;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/navdy/hud/app/screen/DialManagerScreen;->presenter:Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen$Presenter;->isScanningMode()Z

    move-result v0

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
