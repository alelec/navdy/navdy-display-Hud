.class Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2;
.super Ljava/lang/Object;
.source "FirstLaunchScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->checkForMediaService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 199
    const/4 v0, 0x1

    .line 201
    .local v0, "counter":I
    :goto_0
    const/4 v4, 0x0

    .line 203
    .local v4, "mediaPlayer":Landroid/media/MediaPlayer;
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 204
    .local v2, "l1":J
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "creating media player counter="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "counter":I
    .local v1, "counter":I
    :try_start_1
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 205
    new-instance v5, Landroid/media/MediaPlayer;

    invoke-direct {v5}, Landroid/media/MediaPlayer;-><init>()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 206
    .end local v4    # "mediaPlayer":Landroid/media/MediaPlayer;
    .local v5, "mediaPlayer":Landroid/media/MediaPlayer;
    :try_start_2
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->release()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 207
    const/4 v4, 0x0

    .line 208
    .end local v5    # "mediaPlayer":Landroid/media/MediaPlayer;
    .restart local v4    # "mediaPlayer":Landroid/media/MediaPlayer;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "created media player time="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    sub-long/2addr v10, v2

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    iget-object v8, p0, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2;->this$0:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;

    iget-object v8, v8, Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v9, Lcom/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Lcom/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp;-><init>(Lcom/navdy/hud/app/screen/FirstLaunchScreen$1;)V

    invoke-virtual {v8, v9}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 210
    return-void

    .line 211
    .end local v1    # "counter":I
    .end local v2    # "l1":J
    .restart local v0    # "counter":I
    :catch_0
    move-exception v6

    .line 212
    .local v6, "t":Ljava/lang/Throwable;
    :goto_1
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "checkForMediaService"

    invoke-virtual {v8, v9, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 213
    if-eqz v4, :cond_0

    .line 215
    :try_start_4
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 220
    :cond_0
    :goto_2
    const/16 v8, 0x3e8

    invoke-static {v8}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    goto :goto_0

    .line 216
    :catch_1
    move-exception v7

    .line 217
    .local v7, "t1":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/screen/FirstLaunchScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 211
    .end local v0    # "counter":I
    .end local v6    # "t":Ljava/lang/Throwable;
    .end local v7    # "t1":Ljava/lang/Throwable;
    .restart local v1    # "counter":I
    .restart local v2    # "l1":J
    :catch_2
    move-exception v6

    move v0, v1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_1

    .end local v0    # "counter":I
    .end local v4    # "mediaPlayer":Landroid/media/MediaPlayer;
    .restart local v1    # "counter":I
    .restart local v5    # "mediaPlayer":Landroid/media/MediaPlayer;
    :catch_3
    move-exception v6

    move-object v4, v5

    .end local v5    # "mediaPlayer":Landroid/media/MediaPlayer;
    .restart local v4    # "mediaPlayer":Landroid/media/MediaPlayer;
    move v0, v1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_1
.end method
