.class public Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "FactoryResetScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/FactoryResetScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/FactoryResetView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final CANCEL_POSITION:I = 0x1

.field private static final CONFIRMATION_CHOICES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final FACTORY_RESET_POSITION:I


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private confirmationListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

.field private resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->CONFIRMATION_CHOICES:Ljava/util/List;

    .line 72
    sget-object v0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->CONFIRMATION_CHOICES:Ljava/util/List;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->CONFIRMATION_CHOICES:Ljava/util/List;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    .line 82
    new-instance v0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->confirmationListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    return-void
.end method


# virtual methods
.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->resources:Landroid/content/res/Resources;

    .line 119
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->updateView()V

    .line 120
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 121
    return-void
.end method

.method protected updateView()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 128
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/FactoryResetView;

    .line 130
    .local v0, "view":Lcom/navdy/hud/app/view/FactoryResetView;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/FactoryResetView;->getConfirmation()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090102

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 137
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090100

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v2, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0201d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    iget-object v1, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->factoryResetConfirmation:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    sget-object v2, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->CONFIRMATION_CHOICES:Ljava/util/List;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/navdy/hud/app/screen/FactoryResetScreen$Presenter;->confirmationListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 141
    :cond_0
    return-void
.end method
