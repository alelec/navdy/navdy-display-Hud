.class public Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "ForceUpdateScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/ForceUpdateScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/ForceUpdateView;",
        ">;"
    }
.end annotation


# instance fields
.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mScreen:Lcom/navdy/hud/app/screen/ForceUpdateScreen;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 90
    return-void
.end method

.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public install()V
    .locals 3

    .prologue
    .line 93
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "COMMAND"

    const-string v2, "INSTALL_UPDATE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 97
    return-void
.end method

.method public isSoftwareUpdatePending()Z
    .locals 1

    .prologue
    .line 104
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->isUpdateAvailable()Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 79
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 80
    return-void
.end method

.method protected onUnload()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableNotificationColor(Z)V

    .line 85
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 86
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 87
    return-void
.end method

.method public shutDown()V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/hud/app/screen/ForceUpdateScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/Shutdown;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->FORCED_UPDATE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 101
    return-void
.end method
