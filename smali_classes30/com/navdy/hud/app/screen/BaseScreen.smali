.class public abstract Lcom/navdy/hud/app/screen/BaseScreen;
.super Ljava/lang/Object;
.source "BaseScreen.java"

# interfaces
.implements Lmortar/Blueprint;


# static fields
.field public static final ANIMATION_NONE:I = -0x1

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected arguments:Landroid/os/Bundle;

.field protected arguments2:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/screen/BaseScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getInstanceState(Lmortar/MortarScope;)Landroid/os/Bundle;
    .locals 4
    .param p1, "scope"    # Lmortar/MortarScope;

    .prologue
    .line 93
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "latestSavedInstanceState"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 94
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 95
    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-object v2

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/screen/BaseScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "failed to get screen instance state"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setInstanceState(Lmortar/MortarScope;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "scope"    # Lmortar/MortarScope;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 104
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "latestSavedInstanceState"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 105
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 106
    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/screen/BaseScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "failed to set screen instance state"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateInstanceState()V
    .locals 6

    .prologue
    .line 76
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    .line 77
    .local v1, "scope":Lmortar/MortarScope;
    const-class v4, Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    .line 78
    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-interface {v1, p0}, Lmortar/MortarScope;->requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;

    move-result-object v3

    .line 82
    .local v3, "screenScope":Lmortar/MortarScope;
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/screen/BaseScreen;->getInstanceState(Lmortar/MortarScope;)Landroid/os/Bundle;

    move-result-object v0

    .line 83
    .local v0, "latestState":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 84
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "latestState":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 85
    .restart local v0    # "latestState":Landroid/os/Bundle;
    invoke-direct {p0, v3, v0}, Lcom/navdy/hud/app/screen/BaseScreen;->setInstanceState(Lmortar/MortarScope;Landroid/os/Bundle;)V

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/BaseScreen;->getMortarScopeName()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "screenName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "$Presenter"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/screen/BaseScreen;->arguments:Landroid/os/Bundle;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 37
    sget-object v0, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    if-ne p1, v0, :cond_0

    const v0, 0x7f040002

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f040005

    goto :goto_0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 41
    sget-object v0, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    if-ne p1, v0, :cond_0

    const v0, 0x7f040006

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f040009

    goto :goto_0
.end method

.method public getArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/screen/BaseScreen;->arguments:Landroid/os/Bundle;

    return-object v0
.end method

.method public getArguments2()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/screen/BaseScreen;->arguments2:Ljava/lang/Object;

    return-object v0
.end method

.method public abstract getScreen()Lcom/navdy/service/library/events/ui/Screen;
.end method

.method public onAnimationInEnd()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onAnimationInStart()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public onAnimationOutEnd()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onAnimationOutStart()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public setArguments(Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "arg"    # Ljava/lang/Object;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/screen/BaseScreen;->arguments:Landroid/os/Bundle;

    .line 56
    iput-object p2, p0, Lcom/navdy/hud/app/screen/BaseScreen;->arguments2:Ljava/lang/Object;

    .line 57
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;->updateInstanceState()V

    .line 58
    return-void
.end method
