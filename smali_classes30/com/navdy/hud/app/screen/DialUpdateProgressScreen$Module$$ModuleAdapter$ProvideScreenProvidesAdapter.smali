.class public final Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "DialUpdateProgressScreen$Module$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideScreenProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    .prologue
    const/4 v3, 0x0

    .line 43
    const-string v0, "com.navdy.hud.app.screen.DialUpdateProgressScreen"

    const-string v1, "com.navdy.hud.app.screen.DialUpdateProgressScreen.Module"

    const-string v2, "provideScreen"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;->module:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    .line 45
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;->setLibrary(Z)V

    .line 46
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;->module:Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module;->provideScreen()Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter;->get()Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    move-result-object v0

    return-object v0
.end method
