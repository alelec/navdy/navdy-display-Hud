.class public Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;
.super Ljava/lang/Object;
.source "WelcomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/WelcomeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceMetadata"
.end annotation


# instance fields
.field public final deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field public final driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "driverProfile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 131
    iput-object p2, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 132
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceMetadata{deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", driverProfile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/screen/WelcomeScreen$DeviceMetadata;->driverProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
