.class public interface abstract Lcom/navdy/hud/app/screen/IDashboardGaugesManager;
.super Ljava/lang/Object;
.source "IDashboardGaugesManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/screen/IDashboardGaugesManager$GaugePositioning;
    }
.end annotation


# virtual methods
.method public abstract getOptionIconResourceForGauge(I)I
.end method

.method public abstract getOptionLabelForGauge(I)Ljava/lang/String;
.end method

.method public abstract getSmartDashGauges()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWidgetPresenterForGauge(IZ)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.end method

.method public abstract isGaugeWorking(I)Z
.end method

.method public abstract showGauge(ILcom/navdy/hud/app/view/DashboardWidgetView;)V
.end method

.method public abstract updateGauge(ILjava/lang/Object;)V
.end method

.method public abstract updateUserPreference(Lcom/navdy/hud/app/screen/IDashboardGaugesManager$GaugePositioning;I)V
.end method
