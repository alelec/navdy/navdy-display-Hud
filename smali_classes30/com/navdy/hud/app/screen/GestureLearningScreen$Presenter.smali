.class public Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "GestureLearningScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/screen/GestureLearningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/LearnGestureScreenLayout;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tipsBundle:Landroid/os/Bundle;

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 131
    return-void
.end method

.method public hideCameraSensorBlocked()V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 110
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->hideSensorBlocked()V

    .line 113
    :cond_0
    return-void
.end method

.method public hideCaptureView()V
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 124
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->hideCaptureGestureVideosView()V

    .line 127
    :cond_0
    return-void
.end method

.method public hideTips()V
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 96
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->hideTips()V

    .line 99
    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 71
    # getter for: Lcom/navdy/hud/app/screen/GestureLearningScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/GestureLearningScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onLoad"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 74
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 76
    return-void
.end method

.method protected onUnload()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 80
    # getter for: Lcom/navdy/hud/app/screen/GestureLearningScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/screen/GestureLearningScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onUnload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->enableSystemTray(Z)V

    .line 82
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 83
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 84
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 85
    return-void
.end method

.method public showCameraSensorBlocked()V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 103
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->showSensorBlocked()V

    .line 106
    :cond_0
    return-void
.end method

.method public showCaptureView()V
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 117
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->showCaptureGestureVideosView()V

    .line 120
    :cond_0
    return-void
.end method

.method public showTips()V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/navdy/hud/app/screen/GestureLearningScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;

    .line 89
    .local v0, "view":Lcom/navdy/hud/app/view/LearnGestureScreenLayout;
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/LearnGestureScreenLayout;->showTips()V

    .line 92
    :cond_0
    return-void
.end method
