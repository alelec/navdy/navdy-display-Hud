.class Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;
.super Ljava/lang/Object;
.source "ConnectionServiceProxy.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ConnectionServiceProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 176
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Connected to IEventSource service"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 177
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-static {p2}, Lcom/navdy/hud/app/IEventSource$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/hud/app/IEventSource;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    .line 179
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->eventListener:Lcom/navdy/hud/app/IEventListener$Stub;
    invoke-static {v3}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$100(Lcom/navdy/hud/app/service/ConnectionServiceProxy;)Lcom/navdy/hud/app/IEventListener$Stub;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/IEventSource;->addEventListener(Lcom/navdy/hud/app/IEventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Starting connection service state machine"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 184
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    const-class v3, Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 187
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/InitEvents$ConnectionServiceStarted;

    invoke-direct {v3}, Lcom/navdy/hud/app/event/InitEvents$ConnectionServiceStarted;-><init>()V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 188
    return-void

    .line 180
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Failed to connect event listener"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 192
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Disconnected from IEventSource service"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 194
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    const-string v1, ""

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    .line 195
    .local v0, "connectionStateChange":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v1, v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 196
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    .line 197
    return-void
.end method
