.class Lcom/navdy/hud/app/service/HudConnectionService$5;
.super Ljava/lang/Object;
.source "HudConnectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/HudConnectionService;->processLocalEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/HudConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 514
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/service/HudConnectionService;->access$1100(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Ending search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v0}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v0}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/DeviceSearch;->close()V

    .line 520
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->access$202(Lcom/navdy/hud/app/service/HudConnectionService;Lcom/navdy/hud/app/service/DeviceSearch;)Lcom/navdy/hud/app/service/DeviceSearch;

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$5;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 523
    return-void
.end method
