.class Lcom/navdy/hud/app/service/FileTransferHandler$2;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 116
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # getter for: Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$100(Lcom/navdy/hud/app/service/FileTransferHandler;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "onFileTransferRequest"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 117
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v2, v2, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkBandwidthLevel()I

    move-result v2

    if-gtz v2, :cond_1

    .line 118
    sget-object v2, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Link bandwidth is low"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 119
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    if-eqz v2, :cond_0

    .line 120
    sget-object v2, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "FileTransferRequest (PUSH) : rejecting file transfer request to avoid link traffic"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 121
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v1

    .line 122
    .local v1, "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v2, v2, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 130
    .end local v1    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v2, v2, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/file/FileTransferSessionManager;->handleFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v0

    .line 129
    .local v0, "fileTransferResponse":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$2;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v2, v2, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0
.end method
