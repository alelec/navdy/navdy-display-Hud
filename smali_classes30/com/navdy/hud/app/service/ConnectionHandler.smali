.class public Lcom/navdy/hud/app/service/ConnectionHandler;
.super Ljava/lang/Object;
.source "ConnectionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;,
        Lcom/navdy/hud/app/service/ConnectionHandler$ApplicationLaunchAttempted;
    }
.end annotation


# static fields
.field static CAPABILITIES:Lcom/navdy/service/library/events/Capabilities; = null

.field private static final DEVICE_FULL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

.field private static final DEVICE_MINIMAL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

.field public static final IOS_APP_LAUNCH_TIMEOUT:I = 0x1388

.field private static LEGACY_CAPABILITIES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;"
        }
    .end annotation
.end field

.field private static final LINK_LOST_TIME_OUT_AFTER_DISCONNECTED:I = 0x3e8

.field private static final REDOWNLOAD_THRESHOLD:I = 0x7530

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field protected context:Landroid/content/Context;

.field protected disconnecting:Z

.field private volatile forceFullUpdate:Z

.field private volatile isNetworkLinkReady:Z

.field private lastConnectedDeviceId:Ljava/lang/String;

.field protected lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

.field private lastDisconnectTime:J

.field private mAppClosed:Z

.field private mAppLaunchAttempted:Z

.field private mAppLaunchAttemptedRunnable:Ljava/lang/Runnable;

.field protected mCustomNotificationServiceHandler:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

.field protected mDisconnectRunnable:Ljava/lang/Runnable;

.field protected mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field protected mHandler:Landroid/os/Handler;

.field private mPowerManager:Lcom/navdy/hud/app/device/PowerManager;

.field protected mReconnectTimedOutRunnable:Ljava/lang/Runnable;

.field protected mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

.field protected proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private volatile triggerDownload:Z

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 77
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->LEGACY_CAPABILITIES:Ljava/util/List;

    .line 107
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->LEGACY_CAPABILITIES:Ljava/util/List;

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->LEGACY_CAPABILITIES:Ljava/util/List;

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_COMPACT_UI:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->LEGACY_CAPABILITIES:Ljava/util/List;

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_LOCAL_MUSIC_BROWSER:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v0, Lcom/navdy/service/library/events/Capabilities$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/Capabilities$Builder;-><init>()V

    .line 113
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->compactUi(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 114
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->localMusicBrowser(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 115
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearch(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 116
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearchNewIOSPauseBehaviors(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 117
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->musicArtworkCache(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 118
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->searchResultList(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 119
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->supportsIosSms()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->cannedResponseToSms(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 120
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->customDialLongPress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/navdy/service/library/events/Capabilities$Builder;->build()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->CAPABILITIES:Lcom/navdy/service/library/events/Capabilities;

    .line 145
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    invoke-direct {v0, v2}, Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->DEVICE_FULL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    .line 146
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->DEVICE_MINIMAL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/hud/app/service/ConnectionServiceProxy;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/common/TimeHelper;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "proxy"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;
    .param p3, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .param p4, "driverProfileManager"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
    .param p5, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .param p6, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->forceFullUpdate:Z

    .line 101
    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady:Z

    .line 155
    iput-boolean v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->triggerDownload:Z

    .line 162
    iput-boolean v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    .line 163
    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttempted:Z

    .line 171
    iput-object p1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    .line 172
    iput-object p2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    .line 173
    iput-object p3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mPowerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    .line 175
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 176
    new-instance v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;-><init>(Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mCustomNotificationServiceHandler:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mCustomNotificationServiceHandler:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->start()V

    .line 178
    iput-object p4, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 179
    iput-object p5, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 180
    iput-object p6, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 181
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating connectionHandler:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 182
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    .line 183
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/ConnectionHandler$1;-><init>(Lcom/navdy/hud/app/service/ConnectionHandler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDisconnectRunnable:Ljava/lang/Runnable;

    .line 190
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/ConnectionHandler$2;-><init>(Lcom/navdy/hud/app/service/ConnectionHandler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mReconnectTimedOutRunnable:Ljava/lang/Runnable;

    .line 196
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/ConnectionHandler$3;-><init>(Lcom/navdy/hud/app/service/ConnectionHandler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttemptedRunnable:Ljava/lang/Runnable;

    .line 203
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/ConnectionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ConnectionHandler;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->handleDisconnectWithoutLinkLoss()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private getSystemVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 401
    const/16 v1, 0xbe9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "appVersion":Ljava/lang/String;
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 404
    const-string v1, "1.3.3052-ce5463a"

    .line 406
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleDisconnectWithoutLinkLoss()V
    .locals 3

    .prologue
    .line 444
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 445
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v1

    .line 446
    .local v1, "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    if-eqz v1, :cond_0

    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 447
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    .line 449
    :cond_0
    return-void
.end method

.method private onConnect(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 5
    .param p1, "connectingDevice"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 358
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-nez v2, :cond_0

    .line 359
    new-instance v2, Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v4, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    invoke-direct {v2, v3, v4, p1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;-><init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    iput-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    .line 361
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->loadProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 363
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    .line 366
    .local v1, "displayDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    new-instance v2, Lcom/navdy/service/library/events/DeviceInfo$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/DeviceInfo$Builder;-><init>()V

    .line 367
    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    const-string v3, "1.3.3052-ce5463a"

    .line 368
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->clientVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    .line 369
    invoke-virtual {v3}, Lcom/navdy/service/library/Version;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->protocolVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    .line 370
    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceName(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    .line 371
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->getSystemVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 372
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->model(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 373
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceUuid(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 374
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemApiLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    const-string v3, "os.version"

    .line 375
    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->kernelVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 376
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->platform(Lcom/navdy/service/library/events/DeviceInfo$Platform;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    .line 377
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->buildType(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    const-string v3, "Navdy"

    .line 378
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceMake(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->forceFullUpdate:Z

    .line 379
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->forceFullUpdate(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/service/ConnectionHandler;->LEGACY_CAPABILITIES:Ljava/util/List;

    .line 380
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->legacyCapabilities(Ljava/util/List;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/service/ConnectionHandler;->CAPABILITIES:Lcom/navdy/service/library/events/Capabilities;

    .line 381
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->capabilities(Lcom/navdy/service/library/events/Capabilities;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v2

    .line 382
    invoke-virtual {v2}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->build()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 384
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConnect - sending device info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 386
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 387
    return-void
.end method

.method private declared-synchronized onDisconnect(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 4
    .param p1, "navdyDeviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 411
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDisconnect:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 412
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady:Z

    .line 413
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkNotAvailable()V

    .line 414
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->setCurrentProfile(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 415
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDisconnectRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    monitor-exit p0

    return-void

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onLinkEstablished(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 3
    .param p1, "connectingDevice"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mPowerManager:Lcom/navdy/hud/app/device/PowerManager;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->PHONE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/PowerManager;->wakeUp(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V

    .line 342
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v0, :cond_0

    .line 353
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    invoke-direct {v0, v1, v2, p1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;-><init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    .line 354
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mReconnectTimedOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 355
    return-void
.end method

.method private declared-synchronized onLinkLost(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 6
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 419
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLinkLost:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 420
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady:Z

    .line 422
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDisconnectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 423
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mReconnectTimedOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 425
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v2, :cond_1

    .line 426
    .local v0, "wasConnected":Z
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    .line 427
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->clearCache()V

    .line 430
    if-eqz v0, :cond_0

    .line 431
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleDisconnect(Z)V

    .line 432
    iget-boolean v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnecting:Z

    if-nez v1, :cond_0

    .line 433
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mReconnectTimedOutRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 436
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnecting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    monitor-exit p0

    return-void

    .end local v0    # "wasConnected":Z
    :cond_1
    move v0, v1

    .line 425
    goto :goto_0

    .line 419
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private requestIAPUpdateBasedOnPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 6
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 611
    if-eqz p1, :cond_0

    .line 612
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 613
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 614
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 617
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public connect()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->connect()V

    .line 212
    return-void
.end method

.method public connectToDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 3
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 236
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_SELECT:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    if-eqz p1, :cond_0

    .line 237
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendLocalMessage(Lcom/squareup/wire/Message;)V

    .line 238
    return-void

    .line 237
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v0, :cond_0

    .line 262
    iput-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnecting:Z

    .line 264
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->connectToDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 266
    :cond_0
    return-void
.end method

.method public getConnectedDevice()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 228
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDriverRecordingEvent()Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->getDriverRecordingEvent()Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    move-result-object v0

    .line 657
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    return-object v0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    return-object v0
.end method

.method public isAppClosed()Z
    .locals 1

    .prologue
    .line 646
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    return v0
.end method

.method public isAppLaunchAttempted()Z
    .locals 1

    .prologue
    .line 662
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttempted:Z

    return v0
.end method

.method public isNetworkLinkReady()Z
    .locals 1

    .prologue
    .line 650
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady:Z

    return v0
.end method

.method public onApplicationLaunchAttempted(Lcom/navdy/hud/app/service/ConnectionHandler$ApplicationLaunchAttempted;)V
    .locals 4
    .param p1, "applicationLaunchAttempted"    # Lcom/navdy/hud/app/service/ConnectionHandler$ApplicationLaunchAttempted;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 331
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Attempting to launch the App, Will wait and see"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 332
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttempted:Z

    .line 334
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttemptedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttemptedRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    :cond_0
    return-void
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 7
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 298
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectionStateChange - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 299
    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    .line 300
    .local v1, "remoteDeviceId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 302
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    iput-boolean v6, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttempted:Z

    .line 303
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppLaunchAttemptedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 304
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler$4;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 327
    :goto_1
    return-void

    .line 300
    .end local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :cond_0
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    .restart local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :pswitch_0
    iput-boolean v5, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    .line 307
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->onLinkEstablished(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    goto :goto_1

    .line 310
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->onConnect(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 311
    iput-boolean v6, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    goto :goto_1

    .line 314
    :pswitch_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastDisconnectTime:J

    .line 315
    iput-boolean v5, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    .line 316
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->onDisconnect(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    goto :goto_1

    .line 319
    :pswitch_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastDisconnectTime:J

    .line 320
    iput-boolean v5, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mAppClosed:Z

    .line 321
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->onLinkLost(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    goto :goto_1

    .line 304
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDateTimeConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V
    .locals 9
    .param p1, "configurationEvent"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 539
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Received timestamp from the client: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 542
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 544
    iget-object v6, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    const-string v7, "alarm"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 545
    .local v0, "am":Landroid/app/AlarmManager;
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setting date timezone["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] time["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 546
    iget-object v6, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 547
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    .line 548
    .local v1, "current":Ljava/util/TimeZone;
    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 549
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "timezone already set to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 563
    .end local v1    # "current":Ljava/util/TimeZone;
    :goto_0
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setting time ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 564
    iget-object v6, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/app/AlarmManager;->setTime(J)V

    .line 565
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "set time ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 566
    iget-object v6, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    sget-object v7, Lcom/navdy/hud/app/common/TimeHelper;->DATE_TIME_AVAILABLE_EVENT:Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 567
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "post date complete"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 569
    .end local v0    # "am":Landroid/app/AlarmManager;
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v2

    .line 570
    .local v2, "driverProfileManager":Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getSessionPreferences()Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V

    .line 572
    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v3

    .line 573
    .local v3, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    new-instance v6, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    invoke-direct {v6, v3}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    iget-object v7, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 574
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->clockFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    move-result-object v6

    .line 575
    invoke-virtual {v6}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v3

    .line 576
    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/profile/DriverProfileManager;->updateLocalPreferences(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    .line 580
    .end local v2    # "driverProfileManager":Lcom/navdy/hud/app/profile/DriverProfileManager;
    .end local v3    # "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    :goto_1
    return-void

    .line 551
    .restart local v0    # "am":Landroid/app/AlarmManager;
    .restart local v1    # "current":Ljava/util/TimeZone;
    :cond_1
    iget-object v6, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    .line 552
    .local v5, "timeZone":Ljava/util/TimeZone;
    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 553
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "timezone not found on HUD:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 577
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v1    # "current":Ljava/util/TimeZone;
    .end local v5    # "timeZone":Ljava/util/TimeZone;
    :catch_0
    move-exception v4

    .line 578
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Cannot update time on device"

    invoke-virtual {v6, v7, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 555
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v0    # "am":Landroid/app/AlarmManager;
    .restart local v1    # "current":Ljava/util/TimeZone;
    .restart local v5    # "timeZone":Ljava/util/TimeZone;
    :cond_2
    :try_start_1
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "timezone found on HUD:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 556
    iget-object v6, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 560
    .end local v1    # "current":Ljava/util/TimeZone;
    .end local v5    # "timeZone":Ljava/util/TimeZone;
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "timezone not provided"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 14
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 453
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Received deviceInfo:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 454
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v9, :cond_1

    .line 455
    iput-boolean v8, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->triggerDownload:Z

    .line 456
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    if-eqz v9, :cond_4

    .line 457
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastDisconnectTime:J

    sub-long v6, v10, v12

    .line 458
    .local v6, "time":J
    const-wide/16 v10, 0x7530

    cmp-long v9, v6, v10

    if-gez v9, :cond_3

    .line 460
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "threshold met="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 461
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    iget-object v10, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 462
    iput-boolean v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->triggerDownload:Z

    .line 463
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "same device connected:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 478
    .end local v6    # "time":J
    :goto_0
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    invoke-virtual {v9}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v0

    .line 482
    .local v0, "currentDeviceId":Ljava/lang/String;
    new-instance v9, Lcom/navdy/service/library/events/DeviceInfo$Builder;

    invoke-direct {v9, p1}, Lcom/navdy/service/library/events/DeviceInfo$Builder;-><init>(Lcom/navdy/service/library/events/DeviceInfo;)V

    .line 483
    invoke-virtual {v9, v0}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v9

    .line 484
    invoke-virtual {v9}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->build()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v4

    .line 485
    .local v4, "newDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    move-object p1, v4

    .line 486
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v5, v9, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    .line 489
    .local v5, "oldDeviceId":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    move v1, v8

    .line 490
    .local v1, "newDevice":Z
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 491
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    invoke-virtual {v9, p1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->setDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V

    .line 492
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v10, Lcom/navdy/hud/app/event/DeviceInfoAvailable;

    invoke-direct {v10, p1}, Lcom/navdy/hud/app/event/DeviceInfoAvailable;-><init>(Lcom/navdy/service/library/events/DeviceInfo;)V

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 493
    iget-boolean v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->triggerDownload:Z

    if-eqz v9, :cond_6

    .line 494
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "trigger device sync"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 495
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    sget-object v10, Lcom/navdy/hud/app/service/ConnectionHandler;->DEVICE_FULL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 504
    :goto_2
    if-eqz v1, :cond_8

    .line 505
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    .line 516
    .end local v0    # "currentDeviceId":Ljava/lang/String;
    .end local v1    # "newDevice":Z
    .end local v4    # "newDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v5    # "oldDeviceId":Ljava/lang/String;
    :cond_1
    :goto_3
    return-void

    .line 465
    .restart local v6    # "time":J
    :cond_2
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "different device connected last["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] current["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 467
    iget-object v9, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    goto :goto_0

    .line 470
    :cond_3
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "threshold not met="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 471
    iget-object v9, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    goto/16 :goto_0

    .line 474
    .end local v6    # "time":J
    :cond_4
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "no last device"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 475
    iget-object v9, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastConnectedDeviceId:Ljava/lang/String;

    goto/16 :goto_0

    .line 486
    .restart local v0    # "currentDeviceId":Ljava/lang/String;
    .restart local v4    # "newDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 496
    .restart local v1    # "newDevice":Z
    .restart local v5    # "oldDeviceId":Ljava/lang/String;
    :cond_6
    iget-object v9, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v10, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v9, v10, :cond_7

    .line 497
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "triggering minimal sync"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 498
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    sget-object v10, Lcom/navdy/hud/app/service/ConnectionHandler;->DEVICE_MINIMAL_SYNC_EVENT:Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_2

    .line 500
    :cond_7
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "don\'t trigger device sync"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 508
    :cond_8
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleConnect()V

    .line 509
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->hideConnectionToast(Z)V

    .line 511
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->lastDisconnectTime:J

    sub-long v2, v10, v12

    .line 512
    .local v2, "elapsedTime":J
    sget-object v9, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Tracking a reconnect that took "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "ms"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 513
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPhoneDisconnect(ZLjava/lang/String;)V

    goto/16 :goto_3
.end method

.method public onDisconnect(Lcom/navdy/hud/app/event/Disconnect;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/Disconnect;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnect()V

    .line 272
    return-void
.end method

.method public onDriverProfileUpdated(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 3
    .param p1, "profileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 584
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    .line 585
    .local v1, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    .line 586
    .local v0, "preferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    :goto_0
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->requestIAPUpdateBasedOnPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 587
    return-void

    .line 585
    .end local v0    # "preferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onIncrementalOTAFailureDetected(Lcom/navdy/hud/app/util/OTAUpdateService$IncrementalOTAFailureDetected;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/util/OTAUpdateService$IncrementalOTAFailureDetected;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 283
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Incremental OTA failure detected, indicate the remote device to force full update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->forceFullUpdate:Z

    .line 285
    return-void
.end method

.method public onLinkPropertiesChanged(Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;)V
    .locals 2
    .param p1, "linkPropertiesChanged"    # Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 289
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;->bandwidthLevel:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;->bandwidthLevel:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->setLinkBandwidthLevel(I)V

    .line 294
    :cond_0
    return-void
.end method

.method public onLocalSpeechRequest(Lcom/navdy/hud/app/event/LocalSpeechRequest;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/LocalSpeechRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 527
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->isPhoneCallInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    :goto_0
    return-void

    .line 530
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[tts-outbound] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/event/LocalSpeechRequest;->speechRequest:Lcom/navdy/service/library/events/audio/SpeechRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/event/LocalSpeechRequest;->speechRequest:Lcom/navdy/service/library/events/audio/SpeechRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 534
    :cond_1
    new-instance v0, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v1, p1, Lcom/navdy/hud/app/event/LocalSpeechRequest;->speechRequest:Lcom/navdy/service/library/events/audio/SpeechRequest;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->onRemoteEvent(Lcom/navdy/hud/app/event/RemoteEvent;)V

    goto :goto_0
.end method

.method public onNetworkLinkReady(Lcom/navdy/service/library/events/connection/NetworkLinkReady;)V
    .locals 2
    .param p1, "networkLinkReady"    # Lcom/navdy/service/library/events/connection/NetworkLinkReady;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 391
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "DummyNetNetworkFactory:onConnect"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady:Z

    .line 393
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkAvailable()V

    .line 394
    const-wide/16 v0, 0x7530

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->scheduleWithDelay(J)V

    .line 395
    const-wide/32 v0, 0xafc8

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->scheduleWithDelay(J)V

    .line 396
    return-void
.end method

.method public onNotificationPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 0
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 591
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/ConnectionHandler;->requestIAPUpdateBasedOnPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 592
    return-void
.end method

.method public onRemoteEvent(Lcom/navdy/hud/app/event/RemoteEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/RemoteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 520
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    invoke-virtual {p1}, Lcom/navdy/hud/app/event/RemoteEvent;->getMessage()Lcom/squareup/wire/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 523
    :cond_0
    return-void
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 276
    iget-object v0, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->SHUTTING_DOWN:Lcom/navdy/hud/app/event/Shutdown$State;

    if-ne v0, v1, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->shutdown()V

    .line 279
    :cond_0
    return-void
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 596
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v1

    .line 597
    .local v1, "n":I
    if-nez v1, :cond_0

    .line 598
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "go to dial pairing screen"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 599
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 608
    :goto_0
    return-void

    .line 600
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->mRemoteDevice:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    if-nez v2, :cond_1

    .line 601
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "go to welcome screen"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 602
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 603
    .local v0, "args":Landroid/os/Bundle;
    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 606
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    iget-object v4, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public searchForDevices()V
    .locals 3

    .prologue
    .line 241
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendLocalMessage(Lcom/squareup/wire/Message;)V

    .line 243
    return-void
.end method

.method public sendConnectionNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 625
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 626
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->isAppClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629
    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleDisconnect(Z)V

    .line 630
    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showHideToast(Z)V

    .line 639
    :goto_0
    return-void

    .line 632
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleConnect()V

    .line 633
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showHideToast(Z)V

    goto :goto_0

    .line 636
    :cond_1
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->handleDisconnect(Z)V

    .line 637
    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showHideToast(Z)V

    goto :goto_0
.end method

.method public sendLocalMessage(Lcom/squareup/wire/Message;)V
    .locals 4
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    .line 254
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v3

    .line 253
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->postRemoteEvent(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/events/NavdyEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Failed to send local event"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public serviceConnected()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->connected()Z

    move-result v0

    return v0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnect()V

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionHandler;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->disconnect()V

    .line 218
    return-void
.end method

.method public stopSearch()V
    .locals 3

    .prologue
    .line 246
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_STOP_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendLocalMessage(Lcom/squareup/wire/Message;)V

    .line 248
    return-void
.end method
