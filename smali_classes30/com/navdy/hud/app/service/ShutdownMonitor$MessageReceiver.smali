.class Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;
.super Ljava/lang/Thread;
.source "ShutdownMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 1

    .prologue
    .line 195
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    .line 196
    const-string v0, "ShutdownMonitor MessageReceiver thread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 197
    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 201
    const/16 v16, 0x64

    move/from16 v0, v16

    new-array v2, v0, [B

    .line 202
    .local v2, "buf":[B
    const-string v16, "US-ASCII"

    invoke-static/range {v16 .. v16}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    .line 203
    .local v5, "cs":Ljava/nio/charset/Charset;
    const/4 v12, 0x0

    .line 204
    .local v12, "retries":I
    const-string v10, ""

    .line 205
    .local v10, "prevData":Ljava/lang/String;
    const/16 v4, 0x2710

    .line 208
    .local v4, "connectionRetryInterval":I
    :goto_0
    const/4 v11, -0x1

    .line 210
    .local v11, "readCount":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->getSocketInputStream()Ljava/io/InputStream;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$000(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/io/InputStream;

    move-result-object v8

    .line 211
    .local v8, "is":Ljava/io/InputStream;
    if-eqz v8, :cond_0

    .line 212
    const/4 v12, 0x0

    .line 213
    invoke-virtual {v8, v2}, Ljava/io/InputStream;->read([B)I

    move-result v11

    .line 214
    if-gez v11, :cond_0

    .line 215
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    const-string v17, "end of file reading shutdownd socket"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->closeSocketConnection()V
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    .end local v8    # "is":Ljava/io/InputStream;
    :cond_0
    :goto_1
    if-gez v11, :cond_2

    .line 224
    add-int/lit8 v12, v12, 0x1

    const/16 v16, 0x3

    move/from16 v0, v16

    if-le v12, v0, :cond_1

    .line 290
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    const-string v17, "MessageReceiver thread terminating"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 291
    return-void

    .line 219
    :catch_0
    move-exception v7

    .line 220
    .local v7, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    const-string v17, "exception reading shutdownd socket"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->closeSocketConnection()V
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    goto :goto_1

    .line 228
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    mul-int v16, v12, v4

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    :try_start_1
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 229
    :catch_1
    move-exception v16

    goto :goto_0

    .line 234
    :cond_2
    :try_start_2
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    new-instance v17, Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v2, v1, v11, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 235
    .local v6, "data":Ljava/lang/String;
    :goto_2
    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_e

    .line 236
    const-string v16, "\n"

    const/16 v17, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v13

    .line 237
    .local v13, "split":[Ljava/lang/String;
    const/16 v16, 0x0

    aget-object v3, v13, v16

    .line 238
    .local v3, "command":Ljava/lang/String;
    array-length v0, v13

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_3

    const/16 v16, 0x1

    aget-object v6, v13, v16

    .line 239
    :goto_3
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "got command["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 241
    const-string v16, "powerclick"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInputManager:Lcom/navdy/hud/app/manager/InputManager;

    move-object/from16 v16, v0

    sget-object v17, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/hud/app/manager/InputManager;->injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 285
    .end local v3    # "command":Ljava/lang/String;
    .end local v6    # "data":Ljava/lang/String;
    .end local v13    # "split":[Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 286
    .restart local v7    # "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    const-string v17, "exception while processing data from shutdownd"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    const-string v10, ""

    goto/16 :goto_0

    .line 238
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v3    # "command":Ljava/lang/String;
    .restart local v6    # "data":Ljava/lang/String;
    .restart local v13    # "split":[Ljava/lang/String;
    :cond_3
    :try_start_3
    const-string v6, ""

    goto :goto_3

    .line 244
    :cond_4
    const-string v16, "powerdoubleclick"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInputManager:Lcom/navdy/hud/app/manager/InputManager;

    move-object/from16 v16, v0

    sget-object v17, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/hud/app/manager/InputManager;->injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V

    goto/16 :goto_2

    .line 247
    :cond_5
    const-string v16, "powerlongpress"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInputManager:Lcom/navdy/hud/app/manager/InputManager;

    move-object/from16 v16, v0

    sget-object v17, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/hud/app/manager/InputManager;->injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V

    goto/16 :goto_2

    .line 250
    :cond_6
    const-string v16, "powerstate"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 251
    const-string v16, "powerstate"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    .line 252
    .local v14, "state":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "State : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 253
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    move-result v16

    const/16 v17, 0x30

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_9

    const/4 v15, 0x1

    .line 254
    .local v15, "usbOn":Z
    :goto_4
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    move-result v16

    const/16 v17, 0x30

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_a

    const/4 v9, 0x1

    .line 255
    .local v9, "mainOn":Z
    :goto_5
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "USB on : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , Main on : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 256
    if-nez v15, :cond_b

    if-nez v9, :cond_b

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v16

    if-nez v16, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$400(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 259
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    new-instance v17, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;)V

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;
    invoke-static/range {v16 .. v17}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$502(Lcom/navdy/hud/app/service/ShutdownMonitor;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v17, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;
    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$500(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/lang/Runnable;

    move-result-object v17

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->POWER_DISCONECT_SMOOTHING_TIMEOUT:J
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$600()J

    move-result-wide v18

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z
    invoke-static {v0, v9}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$302(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z
    invoke-static {v0, v15}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$402(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z

    goto/16 :goto_2

    .line 253
    .end local v9    # "mainOn":Z
    .end local v15    # "usbOn":Z
    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    .line 254
    .restart local v15    # "usbOn":Z
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 268
    .restart local v9    # "mainOn":Z
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v16

    if-nez v16, :cond_8

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$400(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v16

    if-nez v16, :cond_c

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$500(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/lang/Runnable;

    move-result-object v16

    if-eqz v16, :cond_c

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v17, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;
    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$500(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/lang/Runnable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;
    invoke-static/range {v16 .. v17}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$502(Lcom/navdy/hud/app/service/ShutdownMonitor;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 276
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-object/from16 v16, v0

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/obd/ObdManager;->updateListener()V

    goto :goto_6

    .line 281
    .end local v9    # "mainOn":Z
    .end local v14    # "state":Ljava/lang/String;
    .end local v15    # "usbOn":Z
    :cond_d
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "invalid command from shutdownd socket: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2

    .line 284
    .end local v3    # "command":Ljava/lang/String;
    .end local v13    # "split":[Ljava/lang/String;
    :cond_e
    move-object v10, v6

    goto/16 :goto_0
.end method
