.class Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor$2;->onLocationChanged(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

.field final synthetic val$copy:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor$2;Landroid/location/Location;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iput-object p2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->val$copy:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 482
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->val$copy:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "provider":Ljava/lang/String;
    if-nez v4, :cond_0

    const-string v4, "unknown"

    .line 484
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMovementBases:Landroid/util/ArrayMap;
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/util/ArrayMap;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Location;

    .line 485
    .local v3, "movementBase":Landroid/location/Location;
    if-nez v3, :cond_2

    .line 486
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMovementBases:Landroid/util/ArrayMap;
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/util/ArrayMap;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->val$copy:Landroid/location/Location;

    invoke-virtual {v5, v4, v6}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->onMovement()V
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    .line 503
    :cond_1
    :goto_0
    return-void

    .line 489
    :cond_2
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->val$copy:Landroid/location/Location;

    invoke-virtual {v3, v5}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v2

    .line 490
    .local v2, "dist":F
    const/high16 v5, 0x42700000    # 60.0f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_1

    .line 491
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMovementBases:Landroid/util/ArrayMap;
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/util/ArrayMap;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->val$copy:Landroid/location/Location;

    invoke-virtual {v5, v4, v6}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->onMovement()V
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    .line 494
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v0

    .line 495
    .local v0, "curTime":J
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2400(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    .line 496
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J
    invoke-static {v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2400(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v6

    sub-long v6, v0, v6

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->REPORT_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2500()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    .line 497
    :cond_3
    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$2$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    iget-object v5, v5, Lcom/navdy/hud/app/service/ShutdownMonitor$2;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J
    invoke-static {v5, v0, v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2402(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 498
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Moved significant distance - based on "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " provider"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method
