.class synthetic Lcom/navdy/hud/app/service/pandora/PandoraManager$2;
.super Ljava/lang/Object;
.source "PandoraManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/pandora/PandoraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 484
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    .line 304
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->values()[Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value:[I

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PLAYING:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value:[I

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PAUSED:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    .line 484
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
