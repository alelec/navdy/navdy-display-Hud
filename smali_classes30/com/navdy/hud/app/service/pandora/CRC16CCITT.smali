.class Lcom/navdy/hud/app/service/pandora/CRC16CCITT;
.super Ljava/lang/Object;
.source "CRC16CCITT.java"


# static fields
.field private static final CRC_INITIAL_VALUE:I = 0xffff


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculate(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 5
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    const v4, 0xffff

    .line 21
    const v0, 0xffff

    .line 23
    .local v0, "crc":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 24
    ushr-int/lit8 v2, v0, 0x8

    shl-int/lit8 v3, v0, 0x8

    or-int/2addr v2, v3

    and-int v0, v2, v4

    .line 25
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    xor-int/2addr v0, v2

    .line 26
    and-int/lit16 v2, v0, 0xff

    shr-int/lit8 v2, v2, 0x4

    xor-int/2addr v0, v2

    .line 27
    shl-int/lit8 v2, v0, 0xc

    and-int/2addr v2, v4

    xor-int/2addr v0, v2

    .line 28
    and-int/lit16 v2, v0, 0xff

    shl-int/lit8 v2, v2, 0x5

    and-int/2addr v2, v4

    xor-int/2addr v0, v2

    .line 23
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30
    :cond_0
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    int-to-short v3, v0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v2

    return-object v2
.end method

.method public static checkCRC([B[B)Z
    .locals 1
    .param p0, "data"    # [B
    .param p1, "crc"    # [B

    .prologue
    .line 34
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/CRC16CCITT;->calculate(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0
.end method
