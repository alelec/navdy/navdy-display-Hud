.class public Lcom/navdy/hud/app/service/pandora/DataFrameMessage;
.super Lcom/navdy/hud/app/service/pandora/FrameMessage;
.source "DataFrameMessage.java"


# direct methods
.method public constructor <init>(Z[B)V
    .locals 2
    .param p1, "isSequence0"    # Z
    .param p2, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/service/pandora/FrameMessage;-><init>(Z[B)V

    .line 11
    array-length v0, p2

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Empty payload in data frame is not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_0
    return-void
.end method

.method protected static parseDataFrame([B)Lcom/navdy/hud/app/service/pandora/DataFrameMessage;
    .locals 13
    .param p0, "dataFrame"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v12, 0x0

    const/4 v10, 0x1

    .line 30
    aget-byte v7, p0, v12

    const/16 v8, 0x7e

    if-ne v7, v8, :cond_0

    array-length v7, p0

    add-int/lit8 v7, v7, -0x1

    aget-byte v7, p0, v7

    const/16 v8, 0x7c

    if-ne v7, v8, :cond_0

    aget-byte v7, p0, v10

    if-eqz v7, :cond_1

    .line 32
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Data frame start/stop/type bytes not in place"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 37
    :cond_1
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->unescapeBytes([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 38
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 40
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v6

    .line 42
    .local v6, "sequence":B
    if-nez v6, :cond_2

    .line 43
    const/4 v3, 0x1

    .line 51
    .local v3, "isSequence0":Z
    :goto_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 52
    .local v4, "length":I
    if-nez v4, :cond_4

    .line 53
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Data frame has 0 as payload\'s length"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 44
    .end local v3    # "isSequence0":Z
    .end local v4    # "length":I
    :cond_2
    if-ne v6, v10, :cond_3

    .line 45
    const/4 v3, 0x0

    .restart local v3    # "isSequence0":Z
    goto :goto_0

    .line 47
    .end local v3    # "isSequence0":Z
    :cond_3
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown message frame sequence: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%02X"

    new-array v10, v10, [Ljava/lang/Object;

    .line 48
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 56
    .restart local v3    # "isSequence0":Z
    .restart local v4    # "length":I
    :cond_4
    new-array v5, v4, [B

    .line 57
    .local v5, "payload":[B
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 59
    new-array v1, v9, [B

    .line 60
    .local v1, "crc":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 64
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v7

    if-eq v7, v10, :cond_5

    .line 65
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Corrupted message frame"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 69
    :cond_5
    add-int/lit8 v7, v4, 0x6

    new-array v2, v7, [B

    .line 70
    .local v2, "crcData":[B
    invoke-virtual {v0, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 71
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 72
    invoke-static {v2, v1}, Lcom/navdy/hud/app/service/pandora/CRC16CCITT;->checkCRC([B[B)Z

    move-result v7

    if-nez v7, :cond_6

    .line 73
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "CRC fail"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 76
    :cond_6
    new-instance v7, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;

    invoke-direct {v7, v3, v5}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;-><init>(Z[B)V

    return-object v7
.end method


# virtual methods
.method public buildFrame()[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 18
    iget-object v2, p0, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->payload:[B

    array-length v0, v2

    .line 19
    .local v0, "length":I
    add-int/lit8 v2, v0, 0x6

    .line 20
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 21
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->isSequence0:Z

    if-eqz v3, :cond_0

    .line 22
    :goto_0
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 23
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->payload:[B

    .line 24
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 19
    invoke-static {v1}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->buildFrameFromCRCPart(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    return-object v1

    .line 21
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
