.class abstract Lcom/navdy/hud/app/service/pandora/FrameMessage;
.super Ljava/lang/Object;
.source "FrameMessage.java"


# static fields
.field protected static final ACK_PAYLOAD:[B

.field protected static final ADDITIONAL_BYTES_LENGTH:I = 0x6

.field protected static final CRC_BYTES_LENGTH:I = 0x2

.field protected static final FRAME_TYPE_POSITION:I = 0x1

.field protected static final PNDR_FRAME_END:B = 0x7ct

.field private static final PNDR_FRAME_ESCAPE:B = 0x7dt

.field private static final PNDR_FRAME_ESCAPE_MAPPING:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PNDR_FRAME_SEQUENCE_0:B = 0x0t

.field protected static final PNDR_FRAME_SEQUENCE_1:B = 0x1t

.field protected static final PNDR_FRAME_START:B = 0x7et

.field protected static final PNDR_FRAME_TYPE_ACK:B = 0x1t

.field protected static final PNDR_FRAME_TYPE_DATA:B

.field private static final PNDR_FRAME_UNESCAPE_MAPPING:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected isSequence0:Z

.field protected payload:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 18
    const/4 v1, 0x0

    new-array v1, v1, [B

    sput-object v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->ACK_PAYLOAD:[B

    .line 31
    new-instance v1, Lcom/navdy/hud/app/service/pandora/FrameMessage$1;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/FrameMessage$1;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_ESCAPE_MAPPING:Ljava/util/Map;

    .line 37
    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_ESCAPE_MAPPING:Ljava/util/Map;

    .line 38
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_UNESCAPE_MAPPING:Ljava/util/Map;

    .line 40
    sget-object v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_ESCAPE_MAPPING:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;Ljava/lang/Byte;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;Ljava/lang/Byte;>;"
    check-cast v0, Ljava/util/Map$Entry;

    .line 41
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;Ljava/lang/Byte;>;"
    sget-object v2, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_UNESCAPE_MAPPING:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method

.method protected constructor <init>(Z[B)V
    .locals 0
    .param p1, "isSequence0"    # Z
    .param p2, "payload"    # [B

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/pandora/FrameMessage;->isSequence0:Z

    .line 50
    iput-object p2, p0, Lcom/navdy/hud/app/service/pandora/FrameMessage;->payload:[B

    .line 51
    return-void
.end method

.method protected static buildFrameFromCRCPart(Ljava/nio/ByteBuffer;)[B
    .locals 5
    .param p0, "bufferToCrc"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 100
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/CRC16CCITT;->calculate(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 102
    .local v0, "crc":Ljava/nio/ByteBuffer;
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/FrameMessage;->escapeBytes(Ljava/nio/ByteBuffer;)[B

    move-result-object v2

    .line 103
    .local v2, "escapedDataBytes":[B
    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/FrameMessage;->escapeBytes(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    .line 105
    .local v1, "escapedCRCBytes":[B
    array-length v3, v2

    array-length v4, v1

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x2

    .line 106
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/16 v4, 0x7e

    .line 107
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 108
    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 109
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/16 v4, 0x7c

    .line 110
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 111
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    return-object v3
.end method

.method protected static escapeBytes(Ljava/nio/ByteBuffer;)[B
    .locals 6
    .param p0, "in"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 54
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 56
    .local v1, "byteStream":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 57
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    .line 58
    .local v0, "b":B
    sget-object v4, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_ESCAPE_MAPPING:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    .line 59
    .local v2, "escapedB":Ljava/lang/Byte;
    if-eqz v2, :cond_0

    .line 60
    const/16 v4, 0x7d

    invoke-virtual {v1, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 61
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-virtual {v1, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 56
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_1

    .line 67
    .end local v0    # "b":B
    .end local v2    # "escapedB":Ljava/lang/Byte;
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method public static parseFrame([B)Lcom/navdy/hud/app/service/pandora/FrameMessage;
    .locals 7
    .param p0, "frame"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 115
    aget-byte v0, p0, v4

    .line 116
    .local v0, "type":B
    if-ne v0, v4, :cond_0

    .line 117
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->parseAckFrame([B)Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    move-result-object v1

    .line 119
    :goto_0
    return-object v1

    .line 118
    :cond_0
    if-nez v0, :cond_1

    .line 119
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->parseDataFrame([B)Lcom/navdy/hud/app/service/pandora/DataFrameMessage;

    move-result-object v1

    goto :goto_0

    .line 121
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown message frame type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%02X"

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 122
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static unescapeBytes([B)Ljava/nio/ByteBuffer;
    .locals 7
    .param p0, "in"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 71
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 73
    .local v1, "byteStream":Ljava/io/ByteArrayOutputStream;
    array-length v4, p0

    .line 74
    .local v4, "length":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 75
    aget-byte v0, p0, v3

    .line 76
    .local v0, "b":B
    const/16 v5, 0x7d

    if-ne v0, v5, :cond_0

    .line 77
    add-int/lit8 v3, v3, 0x1

    .line 79
    :try_start_0
    sget-object v5, Lcom/navdy/hud/app/service/pandora/FrameMessage;->PNDR_FRAME_UNESCAPE_MAPPING:Ljava/util/Map;

    aget-byte v6, p0, v3

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 86
    :cond_0
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Escape byte not followed by a byte"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 82
    .end local v2    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v2

    .line 83
    .local v2, "e":Ljava/lang/NullPointerException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Escape byte not followed by a proper byte"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 89
    .end local v0    # "b":B
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public abstract buildFrame()[B
.end method
