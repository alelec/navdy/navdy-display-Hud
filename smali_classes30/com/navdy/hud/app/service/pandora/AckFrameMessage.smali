.class public Lcom/navdy/hud/app/service/pandora/AckFrameMessage;
.super Lcom/navdy/hud/app/service/pandora/FrameMessage;
.source "AckFrameMessage.java"


# static fields
.field private static final ACK_FRAME_BASE:Ljava/nio/ByteBuffer;

.field public static final ACK_WITH_SEQUENCE_0:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

.field private static final ACK_WITH_SEQUENCE_0_FRAME:[B

.field public static final ACK_WITH_SEQUENCE_1:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

.field private static final ACK_WITH_SEQUENCE_1_FRAME:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 10
    new-instance v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    .line 11
    new-instance v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    invoke-direct {v0, v2}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    .line 13
    const/4 v0, 0x6

    .line 14
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 16
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 17
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_FRAME_BASE:Ljava/nio/ByteBuffer;

    .line 18
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_FRAME_BASE:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->buildFrameFromCRCPart(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0_FRAME:[B

    .line 19
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_FRAME_BASE:Ljava/nio/ByteBuffer;

    .line 20
    invoke-virtual {v0, v1, v1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 19
    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->buildFrameFromCRCPart(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1_FRAME:[B

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "isSequence0"    # Z

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_PAYLOAD:[B

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/service/pandora/FrameMessage;-><init>(Z[B)V

    .line 24
    return-void
.end method

.method protected static parseAckFrame([B)Lcom/navdy/hud/app/service/pandora/AckFrameMessage;
    .locals 2
    .param p0, "ackFrame"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0_FRAME:[B

    invoke-static {p0, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    .line 36
    :goto_0
    return-object v0

    .line 35
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1_FRAME:[B

    invoke-static {p0, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    goto :goto_0

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a valid ACK message frame"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public buildFrame()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->isSequence0:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0_FRAME:[B

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1_FRAME:[B

    goto :goto_0
.end method
