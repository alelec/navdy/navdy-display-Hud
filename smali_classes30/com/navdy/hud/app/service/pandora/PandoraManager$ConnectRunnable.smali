.class Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;
.super Ljava/lang/Object;
.source "PandoraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/pandora/PandoraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectRunnable"
.end annotation


# static fields
.field private static final MAX_CONNECTING_RETRIES:I = 0xa

.field private static final PAUSE_BETWEEN_CONNECTING_RETRIES:I = 0x7d0


# instance fields
.field private isStartMusicOn:Z

.field final synthetic this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/service/pandora/PandoraManager;Z)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .param p2, "isStartMusicOn"    # Z

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->isStartMusicOn:Z

    .line 106
    iput-boolean p2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->isStartMusicOn:Z

    .line 107
    return-void
.end method

.method private safeSleepPause()V
    .locals 4

    .prologue
    .line 111
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e1":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Interrupted while pausing between Pandora connection retries"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 120
    :try_start_0
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # invokes: Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnected()Z
    invoke-static {v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$000(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 121
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    .line 124
    :cond_0
    new-instance v7, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v8, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # getter for: Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$100(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "btAddress":Ljava/lang/String;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 126
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v5, 0x0

    .line 127
    .local v5, "tempSocket":Landroid/bluetooth/BluetoothSocket;
    const/4 v3, 0x0

    .local v3, "retry":I
    :goto_0
    const/16 v7, 0xa

    if-ge v3, v7, :cond_1

    .line 129
    :try_start_1
    # getter for: Lcom/navdy/hud/app/service/pandora/PandoraManager;->PANDORA_UUID:Ljava/util/UUID;
    invoke-static {}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$200()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v5

    .line 130
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :cond_1
    :try_start_2
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # invokes: Lcom/navdy/hud/app/service/pandora/PandoraManager;->setSocket(Landroid/bluetooth/BluetoothSocket;)V
    invoke-static {v7, v5}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$300(Lcom/navdy/hud/app/service/pandora/PandoraManager;Landroid/bluetooth/BluetoothSocket;)V

    .line 141
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # invokes: Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnected()Z
    invoke-static {v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$000(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 142
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    sget-object v8, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/SessionStart;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    .line 143
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    sget-object v8, Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    .line 144
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    sget-object v8, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->ENABLED:Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    .line 145
    iget-boolean v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->isStartMusicOn:Z

    if-eqz v7, :cond_2

    .line 146
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    sget-object v8, Lcom/navdy/hud/app/service/pandora/messages/EventTrackPlay;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/EventTrackPlay;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    .line 149
    :cond_2
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;

    iget-object v8, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-direct {v7, v8}, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;-><init>(Lcom/navdy/hud/app/service/pandora/PandoraManager;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 150
    .local v6, "thread":Ljava/lang/Thread;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Pandora-Read-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/navdy/hud/app/service/pandora/PandoraManager;->READ_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$400()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 154
    .end local v6    # "thread":Ljava/lang/Thread;
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # getter for: Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnectingInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$500(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 159
    .end local v0    # "btAddress":Ljava/lang/String;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "retry":I
    .end local v5    # "tempSocket":Landroid/bluetooth/BluetoothSocket;
    :goto_1
    sget-object v7, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "exiting thread:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 160
    return-void

    .line 132
    .restart local v0    # "btAddress":Ljava/lang/String;
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .restart local v3    # "retry":I
    .restart local v5    # "tempSocket":Landroid/bluetooth/BluetoothSocket;
    :catch_0
    move-exception v2

    .line 133
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v7, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot connect to Pandora on remote device: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->this$0:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    # getter for: Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->access$100(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 134
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 136
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;->safeSleepPause()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 127
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 155
    .end local v0    # "btAddress":Ljava/lang/String;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "retry":I
    .end local v5    # "tempSocket":Landroid/bluetooth/BluetoothSocket;
    :catch_1
    move-exception v4

    .line 156
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
