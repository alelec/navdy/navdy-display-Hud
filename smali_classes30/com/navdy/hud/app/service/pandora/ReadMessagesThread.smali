.class public Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;
.super Ljava/lang/Object;
.source "ReadMessagesThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/service/pandora/PandoraManager;)V
    .locals 0
    .param p1, "parentManager"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .line 22
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 26
    const/4 v4, 0x0

    .line 27
    .local v4, "is":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 29
    .local v1, "buffer":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->getSocket()Landroid/bluetooth/BluetoothSocket;

    move-result-object v5

    .line 30
    .local v5, "socket":Landroid/bluetooth/BluetoothSocket;
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 31
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .local v0, "b":I
    move-object v2, v1

    .line 32
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .local v2, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_0
    if-ltz v0, :cond_3

    if-eqz v5, :cond_3

    .line 33
    if-eqz v2, :cond_0

    .line 34
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 35
    const/16 v6, 0x7c

    if-ne v0, v6, :cond_2

    .line 36
    iget-object v6, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onFrameReceived([B)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 37
    const/4 v1, 0x0

    .line 45
    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    move-object v2, v1

    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    goto :goto_0

    .line 39
    :cond_0
    const/16 v6, 0x7e

    if-ne v0, v6, :cond_1

    .line 40
    :try_start_3
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 41
    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    :try_start_4
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 48
    .end local v0    # "b":I
    .end local v5    # "socket":Landroid/bluetooth/BluetoothSocket;
    :catch_0
    move-exception v3

    .line 49
    .local v3, "e":Ljava/lang/Throwable;
    :goto_2
    :try_start_5
    sget-object v6, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 51
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 52
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 53
    iget-object v6, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    .line 56
    .end local v3    # "e":Ljava/lang/Throwable;
    :goto_3
    sget-object v6, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "exiting thread:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 57
    return-void

    .line 43
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "b":I
    .restart local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "socket":Landroid/bluetooth/BluetoothSocket;
    :cond_1
    :try_start_6
    sget-object v6, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected byte received - skipping: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%02X"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    :cond_2
    move-object v1, v2

    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .line 47
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "End of input read from the stream"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 51
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 52
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 53
    iget-object v6, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    move-object v1, v2

    .line 54
    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    goto :goto_3

    .line 51
    .end local v0    # "b":I
    .end local v5    # "socket":Landroid/bluetooth/BluetoothSocket;
    :catchall_0
    move-exception v6

    :goto_4
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 52
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 53
    iget-object v7, p0, Lcom/navdy/hud/app/service/pandora/ReadMessagesThread;->parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    throw v6

    .line 51
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "b":I
    .restart local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "socket":Landroid/bluetooth/BluetoothSocket;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    goto :goto_4

    .line 48
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    :catch_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    goto/16 :goto_2
.end method
