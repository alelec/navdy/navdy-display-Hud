.class public Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "ReturnTrackAlbumArtSegment.java"


# instance fields
.field public data:[B

.field public segmentIndex:B

.field public totalSegments:B

.field public trackToken:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 3
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 20
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 22
    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;-><init>()V

    .line 23
    .local v1, "result":Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->trackToken:I

    .line 24
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    iput-byte v2, v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    .line 25
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    iput-byte v2, v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->totalSegments:B

    .line 27
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->data:[B

    .line 28
    iget-object v2, v1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->data:[B

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 30
    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got artwork\'s segment "

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    add-int/lit8 v1, v1, 0x1

    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->totalSegments:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for track with token "

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->trackToken:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
