.class public Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;
.source "GetTrackAlbumArt.java"


# static fields
.field public static final INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildPayload()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-super {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->buildPayload()[B

    move-result-object v0

    return-object v0
.end method

.method protected putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p1, "os"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 21
    const/16 v0, 0x14

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 22
    const/16 v0, 0x2341

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;->putInt(Ljava/io/ByteArrayOutputStream;I)Ljava/io/ByteArrayOutputStream;

    .line 23
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "Requesting album artwork"

    return-object v0
.end method
