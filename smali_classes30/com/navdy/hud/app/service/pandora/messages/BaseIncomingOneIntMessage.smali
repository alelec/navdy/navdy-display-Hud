.class public Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "BaseIncomingOneIntMessage.java"


# static fields
.field private static MESSAGE_LENGTH:I

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public value:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 14
    const/4 v0, 0x5

    sput v0, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->MESSAGE_LENGTH:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    .line 19
    iput p1, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->value:I

    .line 20
    return-void
.end method

.method protected static parseIntValue([B)I
    .locals 3
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 24
    array-length v1, p0

    sget v2, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->MESSAGE_LENGTH:I

    if-eq v1, v2, :cond_0

    .line 25
    new-instance v1, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;-><init>()V

    throw v1

    .line 27
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 28
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    return v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "toString not overwritten in BaseIncomingOneIntMessage class"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "One int message received with value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;->value:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
