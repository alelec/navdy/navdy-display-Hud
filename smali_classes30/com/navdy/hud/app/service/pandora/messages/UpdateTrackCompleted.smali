.class public Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;
.source "UpdateTrackCompleted.java"


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingOneIntMessage;-><init>(I)V

    .line 12
    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 2
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;

    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;->parseIntValue([B)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Track ended - token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;->value:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
