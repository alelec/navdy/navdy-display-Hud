.class final Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;
.super Ljava/util/HashMap;
.source "UpdateStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/Byte;",
        "Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 27
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 28
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PLAYING:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PAUSED:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INCOMPATIBLE_API_VERSION:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->UNKNOWN_ERROR:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATION_ACTIVE:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INSUFFICIENT_CONNECTIVITY:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->LISTENING_RESTRICTIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INVALID_LOGIN:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method
