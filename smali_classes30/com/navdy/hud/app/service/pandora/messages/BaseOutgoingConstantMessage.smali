.class abstract Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
.source "BaseOutgoingConstantMessage.java"


# instance fields
.field private preBuiltPayload:[B


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->preBuiltPayload:[B

    return-void
.end method


# virtual methods
.method public buildPayload()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->preBuiltPayload:[B

    if-nez v0, :cond_0

    .line 23
    invoke-super {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->buildPayload()[B

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->preBuiltPayload:[B

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->preBuiltPayload:[B

    return-object v0
.end method
