.class public Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "UpdateTrackElapsed.java"


# instance fields
.field public elapsed:S

.field public trackToken:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 3
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 20
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 22
    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;-><init>()V

    .line 23
    .local v1, "result":Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->trackToken:I

    .line 24
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    iput-short v2, v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->elapsed:S

    .line 26
    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got track\'s elapsed time update - seconds: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->elapsed:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
