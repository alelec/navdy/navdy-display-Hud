.class public abstract Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;
.super Ljava/lang/Object;
.source "BaseMessage.java"


# static fields
.field public static final ACCESSORY_ID:Ljava/lang/String; = "CFDA92FC"

.field public static final API_VERSION:S = 0x3s

.field protected static final FIXED_LENGTH_STRING_ENCODING:Ljava/nio/charset/Charset;

.field protected static final INT_BYTES_SIZE:I = 0x4

.field public static final MAX_ARTWORK_PAYLOAD_SIZE:I = 0x2341

.field protected static final MAX_STRING_BYTE_LENGTH:I = 0xf7

.field private static final PNDR_API_VERSION_1:S = 0x1s

.field private static final PNDR_API_VERSION_3:S = 0x3s

.field public static final PNDR_EVENT_TRACK_PLAY:B = 0x30t

.field protected static final PNDR_FALSE:B = 0x0t

.field public static final PNDR_GET_TRACK_ALBUM_ART:B = 0x14t

.field public static final PNDR_GET_TRACK_INFO_EXTENDED:B = 0x16t

.field private static final PNDR_IMAGE_JPEG:B = 0x1t

.field private static final PNDR_IMAGE_NONE:B = 0x0t

.field private static final PNDR_IMAGE_PNG:B = 0x2t

.field private static final PNDR_IMAGE_RGB565:B = 0x3t

.field public static final PNDR_RETURN_TRACK_ALBUM_ART_SEGMENT:B = -0x6bt

.field public static final PNDR_RETURN_TRACK_INFO_EXTENDED:B = -0x63t

.field private static final PNDR_SESSION_FLAG_PAUSE_ON_START:B = 0x2t

.field public static final PNDR_SESSION_START:B = 0x0t

.field public static final PNDR_SESSION_TERMINATE:B = 0x5t

.field public static final PNDR_SET_TRACK_ELAPSED_POLLING:B = 0x15t

.field public static final PNDR_STATUS_INCOMPATIBLE_API_VERSION:B = 0x3t

.field public static final PNDR_STATUS_INSUFFICIENT_CONNECTIVITY:B = 0x7t

.field public static final PNDR_STATUS_INVALID_LOGIN:B = 0x9t

.field public static final PNDR_STATUS_LICENSING_RESTRICTIONS:B = 0x8t

.field public static final PNDR_STATUS_NO_STATIONS:B = 0x5t

.field public static final PNDR_STATUS_NO_STATION_ACTIVE:B = 0x6t

.field public static final PNDR_STATUS_PAUSED:B = 0x2t

.field public static final PNDR_STATUS_PLAYING:B = 0x1t

.field public static final PNDR_STATUS_UNKNOWN_ERROR:B = 0x4t

.field public static final PNDR_TRACK_FLAG_ALLOW_SKIP:B = 0x2t

.field public static final PNDR_TRACK_NONE:I = 0x0

.field protected static final PNDR_TRUE:B = 0x1t

.field public static final PNDR_UPDATE_STATION_ACTIVE:B = -0x46t

.field public static final PNDR_UPDATE_STATUS:B = -0x7ft

.field public static final PNDR_UPDATE_TRACK:B = -0x70t

.field public static final PNDR_UPDATE_TRACK_ALBUM_ART:B = -0x6at

.field public static final PNDR_UPDATE_TRACK_COMPLETED:B = -0x62t

.field public static final PNDR_UPDATE_TRACK_ELAPSED:B = -0x69t

.field public static final REQUIRED_ARTWORK_IMAGE_TYPE:B = 0x1t

.field public static final REQUIRED_ARTWORK_SIZE:S = 0x64s

.field public static final REQUIRED_STATION_ARTWORK_SIZE:S = 0x0s

.field public static final SESSION_START_FLAGS:B = 0x2t

.field protected static final SHORT_BYTES_SIZE:I = 0x2

.field protected static final STRING_BORDER_BYTE:B

.field protected static final STRING_ENCODING:Ljava/nio/charset/Charset;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;->STRING_ENCODING:Ljava/nio/charset/Charset;

    .line 58
    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;->FIXED_LENGTH_STRING_ENCODING:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract toString()Ljava/lang/String;
.end method
