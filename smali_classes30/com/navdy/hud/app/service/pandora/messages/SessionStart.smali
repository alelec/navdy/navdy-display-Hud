.class public Lcom/navdy/hud/app/service/pandora/messages/SessionStart;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;
.source "SessionStart.java"


# static fields
.field private static final ACCESSORY_ID_FIELD_LENGTH:I = 0x8

.field public static final INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/SessionStart;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/SessionStart;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildPayload()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-super {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->buildPayload()[B

    move-result-object v0

    return-object v0
.end method

.method public putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 3
    .param p1, "os"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-static {p1, v2}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 23
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putShort(Ljava/io/ByteArrayOutputStream;S)Ljava/io/ByteArrayOutputStream;

    .line 24
    const/16 v0, 0x8

    const-string v1, "CFDA92FC"

    invoke-static {p1, v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putFixedLengthASCIIString(Ljava/io/ByteArrayOutputStream;ILjava/lang/String;)Ljava/io/ByteArrayOutputStream;

    .line 25
    const/16 v0, 0x64

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putShort(Ljava/io/ByteArrayOutputStream;S)Ljava/io/ByteArrayOutputStream;

    .line 26
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 27
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 28
    invoke-static {p1, v2}, Lcom/navdy/hud/app/service/pandora/messages/SessionStart;->putShort(Ljava/io/ByteArrayOutputStream;S)Ljava/io/ByteArrayOutputStream;

    .line 29
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "Session Start"

    return-object v0
.end method
