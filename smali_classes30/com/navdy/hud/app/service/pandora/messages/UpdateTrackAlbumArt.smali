.class public Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "UpdateTrackAlbumArt.java"


# static fields
.field private static MESSAGE_LENGTH:I


# instance fields
.field public imageLength:I

.field public trackToken:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/16 v0, 0x9

    sput v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->MESSAGE_LENGTH:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 4
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 19
    array-length v2, p0

    sget v3, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->MESSAGE_LENGTH:I

    if-eq v2, v3, :cond_0

    .line 20
    new-instance v2, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;

    invoke-direct {v2}, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;-><init>()V

    throw v2

    .line 22
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 23
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 25
    new-instance v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;-><init>()V

    .line 26
    .local v1, "result":Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->trackToken:I

    .line 27
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->imageLength:I

    .line 28
    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Artwork image loaded on client for track: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->trackToken:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
