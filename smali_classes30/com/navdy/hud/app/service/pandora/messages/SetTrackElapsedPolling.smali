.class public Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;
.source "SetTrackElapsedPolling.java"


# static fields
.field public static final DISABLED:Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

.field public static final ENABLED:Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;


# instance fields
.field public isOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->ENABLED:Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

    .line 15
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->DISABLED:Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0
    .param p1, "isOn"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;-><init>()V

    .line 20
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->isOn:Z

    .line 21
    return-void
.end method


# virtual methods
.method public bridge synthetic buildPayload()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-super {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;->buildPayload()[B

    move-result-object v0

    return-object v0
.end method

.method protected putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p1, "os"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 27
    const/16 v0, 0x15

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 28
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->isOn:Z

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->putBoolean(Ljava/io/ByteArrayOutputStream;Z)Ljava/io/ByteArrayOutputStream;

    .line 29
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Setting track elapsed polling to: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/service/pandora/messages/SetTrackElapsedPolling;->isOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
