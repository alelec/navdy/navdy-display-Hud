.class public Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "ReturnTrackInfoExtended.java"


# instance fields
.field public album:Ljava/lang/String;

.field public albumArtLength:I

.field public artist:Ljava/lang/String;

.field public duration:S

.field public elapsed:S

.field public identityFlags:B

.field public permissionFlags:B

.field public rating:B

.field public title:Ljava/lang/String;

.field public trackToken:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 4
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 28
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    .line 30
    new-instance v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;

    invoke-direct {v2}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;-><init>()V

    .line 31
    .local v2, "result":Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->trackToken:I

    .line 32
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->albumArtLength:I

    .line 33
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    iput-short v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->duration:S

    .line 34
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    iput-short v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->elapsed:S

    .line 35
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    iput-byte v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->rating:B

    .line 36
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    iput-byte v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->permissionFlags:B

    .line 37
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    iput-byte v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->identityFlags:B

    .line 39
    :try_start_0
    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->title:Ljava/lang/String;

    .line 40
    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->artist:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->album:Ljava/lang/String;
    :try_end_0
    .catch Lcom/navdy/hud/app/service/pandora/exceptions/UnterminatedStringException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    return-object v2

    .line 42
    :catch_0
    move-exception v1

    .line 43
    .local v1, "e":Ljava/lang/Exception;
    :goto_0
    new-instance v3, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;

    invoke-direct {v3}, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;-><init>()V

    throw v3

    .line 42
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got extended track\'s info for track with token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->trackToken:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
