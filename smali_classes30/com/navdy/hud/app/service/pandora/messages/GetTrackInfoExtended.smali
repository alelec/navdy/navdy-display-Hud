.class public Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;
.source "GetTrackInfoExtended.java"


# static fields
.field public static final INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildPayload()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-super {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;->buildPayload()[B

    move-result-object v0

    return-object v0
.end method

.method protected getMessageType()B
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x16

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "Getting extended track\'s info"

    return-object v0
.end method
