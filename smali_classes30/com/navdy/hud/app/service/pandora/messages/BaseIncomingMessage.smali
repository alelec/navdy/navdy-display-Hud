.class public abstract Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;
.source "BaseIncomingMessage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;-><init>()V

    return-void
.end method

.method public static buildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 2
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnsupportedMessageReceivedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 78
    aget-byte v0, p0, v1

    sparse-switch v0, :sswitch_data_0

    .line 96
    new-instance v0, Lcom/navdy/hud/app/service/pandora/exceptions/UnsupportedMessageReceivedException;

    aget-byte v1, p0, v1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/service/pandora/exceptions/UnsupportedMessageReceivedException;-><init>(B)V

    throw v0

    .line 80
    :sswitch_0
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 82
    :sswitch_1
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 84
    :sswitch_2
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 86
    :sswitch_3
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStationActive;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 88
    :sswitch_4
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 90
    :sswitch_5
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 92
    :sswitch_6
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 94
    :sswitch_7
    invoke-static {p0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v0

    goto :goto_0

    .line 78
    :sswitch_data_0
    .sparse-switch
        -0x7f -> :sswitch_0
        -0x70 -> :sswitch_1
        -0x6b -> :sswitch_6
        -0x6a -> :sswitch_5
        -0x69 -> :sswitch_7
        -0x63 -> :sswitch_4
        -0x62 -> :sswitch_2
        -0x46 -> :sswitch_3
    .end sparse-switch
.end method

.method protected static getBoolean(Ljava/nio/ByteBuffer;)Z
    .locals 1
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 18
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static getFixedLengthASCIIString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
    .param p1, "fixedLength"    # I

    .prologue
    .line 23
    const/4 v1, 0x0

    .line 24
    .local v1, "byteArrayOS":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 25
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 26
    .local v0, "b":B
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 37
    .end local v0    # "b":B
    :cond_0
    const-string v3, ""

    .line 38
    .local v3, "result":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 39
    new-instance v3, Ljava/lang/String;

    .end local v3    # "result":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;->FIXED_LENGTH_STRING_ENCODING:Ljava/nio/charset/Charset;

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 40
    .restart local v3    # "result":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 42
    :cond_1
    return-object v3

    .line 29
    .end local v3    # "result":Ljava/lang/String;
    .restart local v0    # "b":B
    :cond_2
    if-eqz v0, :cond_4

    .line 30
    if-nez v1, :cond_3

    .line 31
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .end local v1    # "byteArrayOS":Ljava/io/ByteArrayOutputStream;
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 33
    .restart local v1    # "byteArrayOS":Ljava/io/ByteArrayOutputStream;
    :cond_3
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 24
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected static getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/UnterminatedStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 48
    .local v2, "byteArrayOS":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 50
    .local v3, "currentLength":I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 51
    .local v0, "b":B
    :goto_0
    if-eqz v0, :cond_2

    .line 52
    if-gez v0, :cond_0

    .line 53
    new-instance v4, Lcom/navdy/hud/app/service/pandora/exceptions/UnterminatedStringException;

    invoke-direct {v4}, Lcom/navdy/hud/app/service/pandora/exceptions/UnterminatedStringException;-><init>()V

    throw v4

    .line 56
    :cond_0
    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 58
    add-int/lit8 v3, v3, 0x1

    .line 59
    const/16 v4, 0xf7

    if-le v3, v4, :cond_1

    .line 60
    new-instance v4, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;

    invoke-direct {v4}, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;-><init>()V

    throw v4

    .line 63
    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    goto :goto_0

    .line 66
    :cond_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 67
    .local v1, "byteArray":[B
    new-instance v4, Ljava/lang/String;

    sget-object v5, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;->STRING_ENCODING:Ljava/nio/charset/Charset;

    invoke-direct {v4, v1, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v4
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 1
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;-><init>()V

    throw v0
.end method
