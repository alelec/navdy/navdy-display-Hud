.class public abstract Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;
.source "BaseOutgoingMessage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseMessage;-><init>()V

    return-void
.end method

.method protected static putBoolean(Ljava/io/ByteArrayOutputStream;Z)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 19
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 20
    return-object p0

    .line 19
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;
    .locals 0
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 27
    return-object p0
.end method

.method protected static putFixedLengthASCIIString(Ljava/io/ByteArrayOutputStream;ILjava/lang/String;)Ljava/io/ByteArrayOutputStream;
    .locals 3
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "fixedLength"    # I
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 47
    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->FIXED_LENGTH_STRING_ENCODING:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 48
    .local v0, "bytes":[B
    array-length v2, v0

    if-le v2, p1, :cond_0

    .line 49
    new-instance v2, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;

    invoke-direct {v2}, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;-><init>()V

    throw v2

    .line 52
    :cond_0
    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 54
    array-length v1, v0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 55
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_1
    return-object p0
.end method

.method protected static putInt(Ljava/io/ByteArrayOutputStream;I)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 41
    return-object p0
.end method

.method protected static putShort(Ljava/io/ByteArrayOutputStream;S)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "value"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 34
    return-object p0
.end method

.method protected static putString(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)Ljava/io/ByteArrayOutputStream;
    .locals 3
    .param p0, "outStream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xf7

    if-le v0, v1, :cond_0

    .line 65
    new-instance v0, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;-><init>()V

    throw v0

    .line 67
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 68
    new-instance v0, Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;-><init>()V

    throw v0

    .line 71
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->STRING_ENCODING:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 72
    invoke-virtual {p0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 73
    return-object p0
.end method


# virtual methods
.method public buildPayload()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 91
    .local v0, "os":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    .line 92
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 94
    .local v1, "result":[B
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 96
    return-object v1

    .line 94
    .end local v1    # "result":[B
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method protected putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p1, "os"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;-><init>()V

    throw v0
.end method
