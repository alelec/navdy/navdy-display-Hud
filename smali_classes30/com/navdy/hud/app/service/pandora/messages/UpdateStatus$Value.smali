.class public final enum Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;
.super Ljava/lang/Enum;
.source "UpdateStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Value"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum INCOMPATIBLE_API_VERSION:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum INSUFFICIENT_CONNECTIVITY:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum INVALID_LOGIN:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum LISTENING_RESTRICTIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum NO_STATIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum NO_STATION_ACTIVE:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum PAUSED:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum PLAYING:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

.field public static final enum UNKNOWN_ERROR:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PLAYING:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 15
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PAUSED:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 16
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "INCOMPATIBLE_API_VERSION"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INCOMPATIBLE_API_VERSION:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 17
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->UNKNOWN_ERROR:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "NO_STATIONS"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "NO_STATION_ACTIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATION_ACTIVE:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 20
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "INSUFFICIENT_CONNECTIVITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INSUFFICIENT_CONNECTIVITY:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "LISTENING_RESTRICTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->LISTENING_RESTRICTIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    const-string v1, "INVALID_LOGIN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INVALID_LOGIN:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 13
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PLAYING:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->PAUSED:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INCOMPATIBLE_API_VERSION:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->UNKNOWN_ERROR:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->NO_STATION_ACTIVE:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INSUFFICIENT_CONNECTIVITY:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->LISTENING_RESTRICTIONS:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->INVALID_LOGIN:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->$VALUES:[Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->$VALUES:[Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    return-object v0
.end method
