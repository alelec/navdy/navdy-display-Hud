.class public Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
.source "UpdateStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;
    }
.end annotation


# static fields
.field private static MESSAGE_LENGTH:I

.field private static SINGLETONS_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public value:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x2

    sput v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->MESSAGE_LENGTH:I

    .line 27
    new-instance v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->SINGLETONS_MAP:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;)V
    .locals 0
    .param p1, "value"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->value:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;
    .param p2, "x1"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;-><init>(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;)V

    return-void
.end method

.method protected static innerBuildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    .locals 3
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;
        }
    .end annotation

    .prologue
    .line 48
    array-length v1, p0

    sget v2, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->MESSAGE_LENGTH:I

    if-eq v1, v2, :cond_0

    .line 49
    new-instance v1, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;-><init>()V

    throw v1

    .line 51
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->SINGLETONS_MAP:Ljava/util/Map;

    const/4 v2, 0x1

    aget-byte v2, p0, v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    .line 52
    .local v0, "result":Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;
    if-nez v0, :cond_1

    .line 53
    new-instance v1, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;

    invoke-direct {v1}, Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException;-><init>()V

    throw v1

    .line 55
    :cond_1
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update Status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->value:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
