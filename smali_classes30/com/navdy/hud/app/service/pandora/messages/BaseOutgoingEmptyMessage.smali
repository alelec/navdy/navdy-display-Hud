.class abstract Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;
.super Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;
.source "BaseOutgoingEmptyMessage.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingConstantMessage;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getMessageType()B
.end method

.method protected putThis(Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 1
    .param p1, "os"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/StringOverflowException;,
            Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;->getMessageType()B

    move-result v0

    invoke-static {p1, v0}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingEmptyMessage;->putByte(Ljava/io/ByteArrayOutputStream;B)Ljava/io/ByteArrayOutputStream;

    .line 22
    return-object p1
.end method
