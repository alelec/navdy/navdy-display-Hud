.class public Lcom/navdy/hud/app/service/pandora/exceptions/UnexpectedEndOfStringException;
.super Ljava/lang/Exception;
.source "UnexpectedEndOfStringException.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "Zero bytes (used to mark the end of string) is not allowed in strings"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 9
    return-void
.end method
