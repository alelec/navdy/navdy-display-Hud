.class public Lcom/navdy/hud/app/service/pandora/PandoraManager;
.super Ljava/lang/Object;
.source "PandoraManager.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;
    }
.end annotation


# static fields
.field private static final ACK_WAITING_TIME:I = 0x2ee

.field private static final CONNECT_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final INITIAL_TRACK_INFO_BUILDER:Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

.field private static final MAX_SEND_MESSAGE_RETRIES:I = 0xa

.field private static final PANDORA_UUID:Ljava/util/UUID;

.field private static final READ_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final handler:Landroid/os/Handler;

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

.field private adTitle:Ljava/lang/String;

.field private bus:Lcom/squareup/otto/Bus;

.field private currentArtwork:Ljava/io/ByteArrayOutputStream;

.field private currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

.field private expectedArtworkSegmentIndex:B

.field private isConnectingInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isCurrentSequence0:Z

.field private msgQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;",
            ">;"
        }
    .end annotation
.end field

.field private remoteDeviceId:Ljava/lang/String;

.field private socket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 65
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 66
    const-string v0, "453994D5-D58B-96F9-6616-B37F586BA2EC"

    .line 67
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->PANDORA_UUID:Ljava/util/UUID;

    .line 69
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->CONNECT_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 70
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->READ_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 74
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->handler:Landroid/os/Handler;

    .line 75
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 77
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 78
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->INITIAL_TRACK_INFO_BUILDER:Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Landroid/content/res/Resources;)V
    .locals 3
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnectingInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isCurrentSequence0:Z

    .line 84
    iput-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    .line 86
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->msgQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 87
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->INITIAL_TRACK_INFO_BUILDER:Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 88
    iput-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    .line 89
    iput-byte v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->expectedArtworkSegmentIndex:B

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->adTitle:Ljava/lang/String;

    .line 95
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->bus:Lcom/squareup/otto/Bus;

    .line 96
    const v0, 0x7f0901ca

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->adTitle:Ljava/lang/String;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->PANDORA_UUID:Ljava/util/UUID;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/service/pandora/PandoraManager;Landroid/bluetooth/BluetoothSocket;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setSocket(Landroid/bluetooth/BluetoothSocket;)V

    return-void
.end method

.method static synthetic access$400()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->READ_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/service/pandora/PandoraManager;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnectingInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/service/pandora/PandoraManager;[BI)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .param p1, "x1"    # [B
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendDataMessage([BI)V

    return-void
.end method

.method private isConnected()Z
    .locals 2

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->getSocket()Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    .line 565
    .local v0, "tempSocket":Landroid/bluetooth/BluetoothSocket;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isFlagInField(BB)Z
    .locals 1
    .param p1, "flag"    # B
    .param p2, "flagsField"    # B

    .prologue
    .line 279
    and-int v0, p2, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onMessageReceived(Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;)V
    .locals 3
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    .prologue
    .line 283
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    if-eqz v0, :cond_0

    .line 284
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onUpdateStatus(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;)V

    .line 300
    :goto_0
    return-void

    .line 285
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_0
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;

    if-eqz v0, :cond_1

    .line 286
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onUpdateTrack(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;)V

    goto :goto_0

    .line 287
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_1
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;

    if-eqz v0, :cond_2

    .line 288
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onUpdateTrackCompleted(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;)V

    goto :goto_0

    .line 289
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_2
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;

    if-eqz v0, :cond_3

    .line 290
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onReturnTrackInfoExtended(Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;)V

    goto :goto_0

    .line 291
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_3
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;

    if-eqz v0, :cond_4

    .line 292
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onUpdateTrackAlbumArt(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;)V

    goto :goto_0

    .line 293
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_4
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;

    if-eqz v0, :cond_5

    .line 294
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onReturnTrackAlbumArtSegment(Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;)V

    goto :goto_0

    .line 295
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_5
    instance-of v0, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;

    if-eqz v0, :cond_6

    .line 296
    check-cast p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;

    .end local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onUpdateTrackElapsed(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;)V

    goto :goto_0

    .line 298
    .restart local p1    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_6
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unsupported message from Pandora - ignoring: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onReturnTrackAlbumArtSegment(Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;)V
    .locals 6
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;

    .prologue
    .line 377
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    iget v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->trackToken:I

    int-to-long v2, v1

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-byte v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    iget-byte v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->expectedArtworkSegmentIndex:B

    if-eq v1, v2, :cond_3

    .line 383
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Wrong artwork\'s segment sent - ignoring it"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    if-nez v1, :cond_5

    .line 388
    iget-byte v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    if-eqz v1, :cond_4

    .line 390
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Wrong initial artwork\'s segment sent - ignoring it"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 393
    :cond_4
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    .line 397
    :cond_5
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    iget-object v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->data:[B

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 398
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    iget-byte v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->expectedArtworkSegmentIndex:B

    .line 407
    iget-byte v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->expectedArtworkSegmentIndex:B

    iget-byte v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->totalSegments:B

    if-ne v1, v2, :cond_0

    .line 408
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;-><init>()V

    iget v3, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->trackToken:I

    .line 409
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    .line 410
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo(Lokio/ByteString;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 411
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    move-result-object v2

    .line 412
    invoke-virtual {v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdate;

    move-result-object v2

    .line 408
    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 413
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while processing artwork chunk: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackAlbumArtSegment;->segmentIndex:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 401
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    goto/16 :goto_0
.end method

.method private onReturnTrackInfoExtended(Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;)V
    .locals 4
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;

    .prologue
    .line 341
    iget-object v0, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->title:Ljava/lang/String;

    .line 342
    .local v0, "title":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->album:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->artist:Ljava/lang/String;

    .line 343
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-short v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->duration:S

    if-lez v1, :cond_1

    .line 345
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->adTitle:Ljava/lang/String;

    .line 346
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    .line 351
    :cond_0
    :goto_0
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    iget v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->trackToken:I

    int-to-long v2, v2

    .line 352
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->index(Ljava/lang/Long;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 353
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->album:Ljava/lang/String;

    .line 354
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->artist:Ljava/lang/String;

    .line 355
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-short v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->duration:S

    mul-int/lit16 v2, v2, 0x3e8

    .line 356
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-short v2, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->elapsed:S

    .line 357
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->secondsToMilliseconds(S)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 358
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    const/4 v2, 0x2

    iget-byte v3, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->permissionFlags:B

    .line 360
    invoke-direct {p0, v2, v3}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isFlagInField(BB)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 359
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 361
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isOnlineStream(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 362
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    .line 351
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setCurrentTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 363
    return-void

    .line 347
    :cond_1
    iget v1, p1, Lcom/navdy/hud/app/service/pandora/messages/ReturnTrackInfoExtended;->albumArtLength:I

    if-lez v1, :cond_0

    .line 348
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->requestArtwork()V

    goto :goto_0
.end method

.method private onUpdateStatus(Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;)V
    .locals 4
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;

    .prologue
    .line 303
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 304
    .local v0, "resultBuilder":Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value:[I

    iget-object v2, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->value:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 312
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    .line 315
    :goto_0
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setCurrentTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 317
    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    if-ne v1, v2, :cond_0

    .line 318
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive bad status from Pandora: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus;->value:Lcom/navdy/hud/app/service/pandora/messages/UpdateStatus$Value;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    .line 321
    :cond_0
    return-void

    .line 306
    :pswitch_0
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    goto :goto_0

    .line 309
    :pswitch_1
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    goto :goto_0

    .line 304
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onUpdateTrack(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;)V
    .locals 3
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;

    .prologue
    .line 328
    iget v1, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrack;->value:I

    if-nez v1, :cond_0

    .line 338
    :goto_0
    return-void

    .line 332
    :cond_0
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackInfoExtended;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot send request message for track\'s info - disconnecting"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    goto :goto_0
.end method

.method private onUpdateTrackAlbumArt(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;)V
    .locals 4
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget v2, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackAlbumArt;->trackToken:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 370
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Received update for artwork on the wrong song - ignoring"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 372
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->requestArtwork()V

    goto :goto_0
.end method

.method private onUpdateTrackCompleted(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;)V
    .locals 0
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackCompleted;

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    .line 325
    return-void
.end method

.method private onUpdateTrackElapsed(Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;)V
    .locals 6
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;

    .prologue
    .line 419
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iget v1, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->trackToken:I

    int-to-long v2, v1

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 423
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Elapsed time received for wrong song - ignoring"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_2
    iget-short v1, p1, Lcom/navdy/hud/app/service/pandora/messages/UpdateTrackElapsed;->elapsed:S

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->secondsToMilliseconds(S)I

    move-result v0

    .line 428
    .local v0, "elapsed":I
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 429
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 430
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 431
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    .line 429
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setCurrentTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    goto :goto_0
.end method

.method private requestArtwork()V
    .locals 3

    .prologue
    .line 453
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    .line 455
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/GetTrackAlbumArt;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :goto_0
    return-void

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while requesting pre-loaded artwork - ignoring"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private resetCurrentArtwork()V
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentArtwork:Ljava/io/ByteArrayOutputStream;

    .line 449
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->expectedArtworkSegmentIndex:B

    .line 450
    return-void
.end method

.method private secondsToMilliseconds(S)I
    .locals 1
    .param p1, "seconds"    # S

    .prologue
    .line 436
    mul-int/lit16 v0, p1, 0x3e8

    return v0
.end method

.method private sendAckMessage(Z)V
    .locals 5
    .param p1, "isAckSequence0"    # Z

    .prologue
    .line 219
    if-eqz p1, :cond_1

    sget-object v2, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_0:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    .line 220
    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->buildFrame()[B

    move-result-object v0

    .line 223
    .local v0, "ackMsgFrame":[B
    :goto_0
    :try_start_0
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendByteArray([B)V

    .line 224
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ack message sent to Pandora - is sequence 0: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :cond_0
    :goto_1
    return-void

    .line 220
    .end local v0    # "ackMsgFrame":[B
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->ACK_WITH_SEQUENCE_1:Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    .line 221
    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;->buildFrame()[B

    move-result-object v0

    goto :goto_0

    .line 227
    .restart local v0    # "ackMsgFrame":[B
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Cannot send ACK message frame through the socket - disconnecting"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 229
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    goto :goto_1
.end method

.method private sendByteArray([B)V
    .locals 2
    .param p1, "frame"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->getSocket()Landroid/bluetooth/BluetoothSocket;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 165
    .local v0, "os":Ljava/io/OutputStream;
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 166
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 167
    return-void
.end method

.method private sendDataMessage([BI)V
    .locals 6
    .param p1, "frame"    # [B
    .param p2, "leftAttempts"    # I

    .prologue
    .line 170
    if-gtz p2, :cond_0

    .line 171
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Max number of retries for the message achieved - closing connection"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    .line 192
    :goto_0
    return-void

    .line 175
    :cond_0
    add-int/lit8 v1, p2, -0x1

    .line 178
    .local v1, "newValueLeftAttempts":I
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendByteArray([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    new-instance v2, Lcom/navdy/hud/app/service/pandora/PandoraManager$1;

    invoke-direct {v2, p0, p1, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager$1;-><init>(Lcom/navdy/hud/app/service/pandora/PandoraManager;[BI)V

    iput-object v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    .line 191
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2ee

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Cannot send message frame through the socket - disconnecting"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    goto :goto_0
.end method

.method private sendNextMessage()V
    .locals 6

    .prologue
    .line 203
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->msgQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;

    .line 204
    .local v2, "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
    if-nez v2, :cond_0

    .line 205
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Queue empty: no new message waiting to be sent"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 216
    .end local v2    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
    :goto_0
    return-void

    .line 208
    .restart local v2    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending message to Pandora: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 209
    new-instance v1, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;

    iget-boolean v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isCurrentSequence0:Z

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;->buildPayload()[B

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;-><init>(Z[B)V

    .line 210
    .local v1, "frameMsg":Lcom/navdy/hud/app/service/pandora/DataFrameMessage;
    invoke-virtual {v1}, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;->buildFrame()[B

    move-result-object v3

    const/16 v4, 0xa

    invoke-direct {p0, v3, v4}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendDataMessage([BI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    .end local v1    # "frameMsg":Lcom/navdy/hud/app/service/pandora/DataFrameMessage;
    .end local v2    # "msg":Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Cannot send data message - disconnecting"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 213
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    goto :goto_0
.end method

.method private setCurrentTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 2
    .param p1, "currentTrack"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 442
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 444
    :cond_0
    return-void
.end method

.method private declared-synchronized setSocket(Landroid/bluetooth/BluetoothSocket;)V
    .locals 1
    .param p1, "newSocket"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 555
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;

    if-eq v0, p1, :cond_1

    .line 556
    if-nez p1, :cond_0

    .line 557
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 559
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    :cond_1
    monitor-exit p0

    return-void

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private startConnecting(Z)V
    .locals 4
    .param p1, "isStartMusicOn"    # Z

    .prologue
    .line 541
    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnectingInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/pandora/PandoraManager$ConnectRunnable;-><init>(Lcom/navdy/hud/app/service/pandora/PandoraManager;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 543
    .local v0, "thread":Ljava/lang/Thread;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pandora-Connect-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->CONNECT_THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 544
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 548
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 546
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Connecting thread is already running - ignoring the request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 468
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 469
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 470
    iput-object v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    .line 473
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->resetCurrentArtwork()V

    .line 474
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->INITIAL_TRACK_INFO_BUILDER:Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setCurrentTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 476
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 477
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->setSocket(Landroid/bluetooth/BluetoothSocket;)V

    .line 479
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->msgQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 480
    return-void
.end method

.method public declared-synchronized getSocket()Landroid/bluetooth/BluetoothSocket;
    .locals 1

    .prologue
    .line 551
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->socket:Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDeviceConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "connectionStateChange"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 484
    sget-object v0, Lcom/navdy/hud/app/service/pandora/PandoraManager$2;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 493
    :goto_0
    return-void

    .line 486
    :pswitch_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;

    goto :goto_0

    .line 489
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;

    .line 490
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    goto :goto_0

    .line 484
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFrameReceived([B)V
    .locals 6
    .param p1, "frame"    # [B

    .prologue
    const/4 v4, 0x2

    .line 237
    :try_start_0
    invoke-static {p1}, Lcom/navdy/hud/app/service/pandora/FrameMessage;->parseFrame([B)Lcom/navdy/hud/app/service/pandora/FrameMessage;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 243
    .local v1, "msg":Lcom/navdy/hud/app/service/pandora/FrameMessage;
    instance-of v3, v1, Lcom/navdy/hud/app/service/pandora/AckFrameMessage;

    if-eqz v3, :cond_3

    .line 244
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ack message sent to Pandora - is sequence 0: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->isSequence0:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 247
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isCurrentSequence0:Z

    iget-boolean v4, v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->isSequence0:Z

    if-eq v3, v4, :cond_2

    .line 248
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 250
    iget-boolean v3, v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->isSequence0:Z

    iput-boolean v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isCurrentSequence0:Z

    .line 251
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    .line 252
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendNextMessage()V

    .line 276
    .end local v1    # "msg":Lcom/navdy/hud/app/service/pandora/FrameMessage;
    :cond_1
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Cannot parse received message frame"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 254
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "msg":Lcom/navdy/hud/app/service/pandora/FrameMessage;
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Invalid ACK message received - ignoring it"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_3
    instance-of v3, v1, Lcom/navdy/hud/app/service/pandora/DataFrameMessage;

    if-eqz v3, :cond_6

    .line 257
    iget-boolean v3, v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->isSequence0:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendAckMessage(Z)V

    .line 259
    const/4 v2, 0x0

    .line 261
    .local v2, "parsedMsg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :try_start_1
    iget-object v3, v1, Lcom/navdy/hud/app/service/pandora/FrameMessage;->payload:[B

    invoke-static {v3}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;->buildFromPayload([B)Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;

    move-result-object v2

    .line 262
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 263
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received (and ACKed) message from Pandora: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/navdy/hud/app/service/pandora/exceptions/MessageWrongWayException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/navdy/hud/app/service/pandora/exceptions/CorruptedPayloadException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/navdy/hud/app/service/pandora/exceptions/UnsupportedMessageReceivedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 270
    :cond_4
    :goto_2
    if-eqz v2, :cond_1

    .line 271
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->onMessageReceived(Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;)V

    goto :goto_0

    .line 257
    .end local v2    # "parsedMsg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 265
    .restart local v2    # "parsedMsg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :catch_1
    move-exception v0

    .line 267
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - ignoring the message (already ACKed)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_2

    .line 274
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "parsedMsg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :cond_6
    sget-object v3, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unsupported message type"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 265
    .restart local v2    # "parsedMsg":Lcom/navdy/hud/app/service/pandora/messages/BaseIncomingMessage;
    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method public onNotificationsStatusUpdate(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 497
    sget-object v2, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_PANDORA:Lcom/navdy/service/library/events/notification/ServiceType;

    iget-object v3, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/ServiceType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 527
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->remoteDeviceId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 502
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Received Pandora app state message while remote device ID is unknown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 507
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v1

    .line 508
    .local v1, "remotePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 509
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Pandora is supported only for Android clients for now"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 514
    :cond_2
    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_ENABLED:Lcom/navdy/service/library/events/notification/NotificationsState;

    iget-object v3, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 515
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsState;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 516
    .local v0, "isClientsPandoraRunning":Z
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received Pandora\'s running status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 518
    if-eqz v0, :cond_4

    .line 519
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 520
    sget-object v2, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Pandora is already connected - restarting connection"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 521
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    .line 523
    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->startConnecting(Z)V

    goto :goto_0

    .line 525
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->terminateAndClose()V

    goto :goto_0
.end method

.method public sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V
    .locals 1
    .param p1, "msg"    # Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->msgQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/service/pandora/PandoraManager;->activeAckMessageTimeoutCounter:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendNextMessage()V

    .line 199
    :cond_0
    return-void
.end method

.method public startAndPlay()V
    .locals 1

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->startConnecting(Z)V

    .line 535
    :goto_0
    return-void

    .line 533
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/pandora/messages/EventTrackPlay;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/EventTrackPlay;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    goto :goto_0
.end method

.method protected terminateAndClose()V
    .locals 1

    .prologue
    .line 462
    sget-object v0, Lcom/navdy/hud/app/service/pandora/messages/SessionTerminate;->INSTANCE:Lcom/navdy/hud/app/service/pandora/messages/SessionTerminate;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->sendOrQueueMessage(Lcom/navdy/hud/app/service/pandora/messages/BaseOutgoingMessage;)V

    .line 463
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->close()V

    .line 464
    return-void
.end method
