.class Lcom/navdy/hud/app/service/FileTransferHandler$3;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/file/FileTransferData;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->val$request:Lcom/navdy/service/library/events/file/FileTransferData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 168
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # getter for: Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$100(Lcom/navdy/hud/app/service/FileTransferHandler;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onFileTransferData"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 169
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v4, v4, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkBandwidthLevel()I

    move-result v4

    if-gtz v4, :cond_1

    .line 170
    new-instance v4, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;-><init>()V

    .line 171
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v4

    .line 172
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 173
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v3

    .line 174
    invoke-virtual {v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v2

    .line 175
    .local v2, "fs":Lcom/navdy/service/library/events/file/FileTransferStatus;
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 185
    .end local v2    # "fs":Lcom/navdy/service/library/events/file/FileTransferStatus;
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v4, v4, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->val$request:Lcom/navdy/service/library/events/file/FileTransferData;

    iget-object v5, v5, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/file/FileTransferSessionManager;->absolutePathForTransferId(I)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "absolutePath":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v4, v4, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->val$request:Lcom/navdy/service/library/events/file/FileTransferData;

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/file/FileTransferSessionManager;->handleFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v1

    .line 180
    .local v1, "fileTransferStatus":Lcom/navdy/service/library/events/file/FileTransferStatus;
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v4, v4, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 181
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v3, 0x1

    :cond_2
    # invokes: Lcom/navdy/hud/app/service/FileTransferHandler;->setDownloadingProperty(Z)V
    invoke-static {v4, v3}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$200(Lcom/navdy/hud/app/service/FileTransferHandler;Z)V

    .line 182
    if-eqz v0, :cond_0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 183
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$3;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # invokes: Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferDone(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$300(Lcom/navdy/hud/app/service/FileTransferHandler;Ljava/lang/String;)V

    goto :goto_0
.end method
