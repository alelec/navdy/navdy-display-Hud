.class Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;
.super Ljava/lang/Object;
.source "CustomNotificationServiceHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->onShowCustomNotification(Lcom/navdy/service/library/events/notification/ShowCustomNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

.field final synthetic val$t1:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    .prologue
    .line 465
    iput-object p1, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;->this$0:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    iput-wide p2, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;->val$t1:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V
    .locals 7
    .param p1, "result"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .prologue
    .line 468
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 469
    .local v0, "t2":J
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIND_ROUTE_TO_CLOSEST_GAS_STATION: success: time  ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;->val$t1:J

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 470
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIND_ROUTE_TO_CLOSEST_GAS_STATION route id["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] label["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] via["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] address["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    const-string v5, "\n"

    const-string v6, ""

    .line 473
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] length["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] duration["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] freeFlowDuration["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 470
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 477
    return-void

    .line 473
    :cond_0
    const-string v2, "null"

    goto :goto_0
.end method

.method public onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
    .locals 6
    .param p1, "error"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    .prologue
    .line 481
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 482
    .local v0, "t2":J
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIND_ROUTE_TO_CLOSEST_GAS_STATION: failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time to error ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;->val$t1:J

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 483
    return-void
.end method
