.class public Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;
.super Lcom/navdy/hud/app/service/S3FileUploadService;
.source "ObdCANBusDataUploadService.java"


# static fields
.field private static final MAX_FILES_TO_UPLOAD:I = 0x5

.field public static final NAVDY_GESTURE_VIDEOS_BUCKET:Ljava/lang/String; = "navdy-obd-data"

.field private static isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static mIsInitialized:Z

.field private static sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static sObdDatFilesFolder:Ljava/lang/String;

.field private static final sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 35
    sput-boolean v2, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->mIsInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/S3FileUploadService;-><init>(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public static addObdDataFileToQueue(Ljava/io/File;)V
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 101
    invoke-static {p0}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->initializeIfNecessary(Ljava/io/File;)V

    .line 102
    sget-object v1, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    monitor-enter v1

    .line 103
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    new-instance v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    const-string v3, ""

    invoke-direct {v2, p0, v3}, Lcom/navdy/hud/app/service/S3FileUploadService$Request;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 104
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Queue size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v3}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v0

    const/4 v2, 0x5

    if-le v0, v2, :cond_0

    .line 107
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    return-void
.end method

.method private static initializeIfNecessary()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->initializeIfNecessary(Ljava/io/File;)V

    .line 115
    return-void
.end method

.method private static initializeIfNecessary(Ljava/io/File;)V
    .locals 9
    .param p0, "fileToIgnore"    # Ljava/io/File;

    .prologue
    .line 118
    sget-object v4, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    monitor-enter v4

    .line 119
    :try_start_0
    sget-boolean v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->mIsInitialized:Z

    if-nez v3, :cond_3

    .line 120
    sget-object v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Not initialized , initializing now"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 121
    sget-boolean v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->mIsInitialized:Z

    if-nez v3, :cond_3

    .line 122
    const-string v3, "/sdcard/.canBusLogs/upload"

    sput-object v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDatFilesFolder:Ljava/lang/String;

    .line 123
    new-instance v3, Ljava/io/File;

    sget-object v5, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDatFilesFolder:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 124
    .local v0, "files":[Ljava/io/File;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v1, "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    array-length v5, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v2, v0, v3

    .line 126
    .local v2, "obdCanBusDataBundle":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".zip"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 125
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 129
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ObdCanBus Data bundle :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 136
    .end local v0    # "files":[Ljava/io/File;
    .end local v1    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v2    # "obdCanBusDataBundle":Ljava/io/File;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 132
    .restart local v0    # "files":[Ljava/io/File;
    .restart local v1    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_2
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    const/4 v5, 0x5

    invoke-static {v1, v3, v5}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->populateFilesQueue(Ljava/util/ArrayList;Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;I)V

    .line 133
    const/4 v3, 0x1

    sput-boolean v3, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->mIsInitialized:Z

    .line 136
    .end local v0    # "files":[Ljava/io/File;
    .end local v1    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    return-void
.end method

.method public static populateFilesQueue(Ljava/util/ArrayList;Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;I)V
    .locals 4
    .param p1, "filesQueue"    # Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    .param p2, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    if-eqz p0, :cond_1

    .line 88
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 89
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    new-instance v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/navdy/hud/app/service/S3FileUploadService$Request;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 91
    invoke-virtual {p1}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    goto :goto_0

    .line 97
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public static scheduleWithDelay(J)V
    .locals 6
    .param p0, "delay"    # J

    .prologue
    .line 177
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "SYNC"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x7b

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 180
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 181
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p0

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 182
    return-void
.end method

.method public static syncNow()V
    .locals 3

    .prologue
    .line 155
    sget-object v1, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "synNow"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 159
    return-void
.end method


# virtual methods
.method public canCompleteRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)Z
    .locals 1
    .param p1, "request"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method protected getAWSBucket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "navdy-obd-data"

    return-object v0
.end method

.method protected getCurrentRequest()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    return-object v0
.end method

.method protected getIsUploading()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method protected getKeyPrefix(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 78
    const-string v0, "CANBus"

    return-object v0
.end method

.method protected getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method protected getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sObdDataFilesUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    return-object v0
.end method

.method protected initialize()V
    .locals 0

    .prologue
    .line 53
    invoke-static {}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->initializeIfNecessary()V

    .line 54
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 48
    invoke-super {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->onCreate()V

    .line 49
    return-void
.end method

.method public reSchedule()V
    .locals 2

    .prologue
    .line 162
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reschedule"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 163
    const-wide/16 v0, 0x2710

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->scheduleWithDelay(J)V

    .line 164
    return-void
.end method

.method protected setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V
    .locals 0
    .param p1, "currentRequest"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 68
    sput-object p1, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .line 69
    return-void
.end method

.method public sync()V
    .locals 0

    .prologue
    .line 151
    invoke-static {}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->syncNow()V

    .line 152
    return-void
.end method

.method protected uploadFinished(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "succeeded"    # Z
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "userTag"    # Ljava/lang/String;

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getObdCanBusRecordingPolicy()Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onFileUploaded(Ljava/io/File;)V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File upload failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
