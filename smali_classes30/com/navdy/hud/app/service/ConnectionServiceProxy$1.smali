.class Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;
.super Ljava/lang/Object;
.source "ConnectionServiceProxy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/ConnectionServiceProxy;->postRemoteEvent(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/events/NavdyEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

.field final synthetic val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field final synthetic val$event:Lcom/navdy/service/library/events/NavdyEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iput-object p2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    iput-object p3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 143
    const/4 v3, -0x1

    .line 144
    .local v3, "len":I
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v9, v9, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    if-eqz v9, :cond_2

    .line 146
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 147
    .local v4, "l1":J
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v1

    .line 148
    .local v1, "eventData":[B
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 149
    .local v6, "l2":J
    array-length v3, v1

    .line 150
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const/4 v12, 0x2

    invoke-virtual {v9, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 151
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "NAVDY-PACKET [H2P-Outgoing-Event] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 153
    :cond_0
    sub-long v10, v6, v4

    .line 154
    .local v10, "time":J
    const-wide/16 v12, 0x64

    cmp-long v9, v10, v12

    if-ltz v9, :cond_1

    .line 155
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "NAVDY-PACKET [H2P-Outgoing-Event]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    iget-object v13, v13, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v13}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " took: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " len:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 157
    :cond_1
    iget-object v9, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->this$0:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v9, v9, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    iget-object v12, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v12}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v9, v12, v1}, Lcom/navdy/hud/app/IEventSource;->postRemoteEvent(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 169
    .end local v1    # "eventData":[B
    .end local v4    # "l1":J
    .end local v6    # "l2":J
    .end local v10    # "time":J
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v2

    .line 159
    .local v2, "ex":Landroid/os/DeadObjectException;
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v12, "Exception posting remote event, server died"

    invoke-virtual {v9, v12, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 160
    .end local v2    # "ex":Landroid/os/DeadObjectException;
    :catch_1
    move-exception v0

    .line 161
    .local v0, "e":Landroid/os/TransactionTooLargeException;
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception posting remote event, large payload type["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    iget-object v13, v13, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] size["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 163
    .end local v0    # "e":Landroid/os/TransactionTooLargeException;
    :catch_2
    move-exception v8

    .line 164
    .local v8, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v12, "Exception posting remote event"

    invoke-virtual {v9, v12, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 167
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_2
    # getter for: Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to send event - service connection broken"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
