.class Lcom/navdy/hud/app/service/HudConnectionService$6;
.super Ljava/lang/Object;
.source "HudConnectionService.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/HudConnectionService;->createProxyService()Lcom/navdy/service/library/device/connection/tunnel/Tunnel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/HudConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$6;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 618
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2Link;->proxyEASession:Lcom/navdy/hud/mfi/EASession;

    if-eqz v2, :cond_0

    .line 619
    new-instance v2, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;

    sget-object v3, Lcom/navdy/hud/device/connection/iAP2Link;->proxyEASession:Lcom/navdy/hud/mfi/EASession;

    invoke-direct {v2, v3}, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;-><init>(Lcom/navdy/hud/mfi/EASession;)V

    .line 627
    :goto_0
    return-object v2

    .line 621
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService$6;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/service/HudConnectionService;->access$1200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v2

    if-nez v2, :cond_1

    .line 622
    new-instance v2, Ljava/io/IOException;

    const-string v3, "can\'t create proxy tunnel because HUD is not connected to remote device"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 624
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService$6;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/service/HudConnectionService;->access$1300(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    .line 625
    .local v0, "btAddr":Ljava/lang/String;
    new-instance v1, Lcom/navdy/service/library/network/BTSocketFactory;

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService;->NAVDY_PROXY_TUNNEL_UUID:Ljava/util/UUID;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/network/BTSocketFactory;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    .line 627
    .local v1, "connector":Lcom/navdy/service/library/network/BTSocketFactory;
    invoke-virtual {v1}, Lcom/navdy/service/library/network/BTSocketFactory;->build()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v2

    goto :goto_0
.end method
