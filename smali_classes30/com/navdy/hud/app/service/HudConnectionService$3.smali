.class Lcom/navdy/hud/app/service/HudConnectionService$3;
.super Landroid/content/BroadcastReceiver;
.source "HudConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/HudConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private cancelledDevice:Landroid/bluetooth/BluetoothDevice;

.field private cancelledTime:J

.field final synthetic this$0:Lcom/navdy/hud/app/service/HudConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 185
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 188
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v3, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 189
    const-string v3, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v4, -0x80000000

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 191
    .local v1, "bondState":I
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$100(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bond state change - device:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 192
    const/16 v3, 0xb

    if-ne v1, v3, :cond_2

    .line 195
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/service/DeviceSearch;->forgetDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    .line 196
    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$300(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    .line 197
    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$400(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v3

    .line 198
    invoke-virtual {v3}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v3

    .line 199
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 200
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$500(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Unexpected bonding with known device - removing bond"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 201
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->forgetPairedDevice(Landroid/bluetooth/BluetoothDevice;)V

    .line 202
    invoke-static {v2}, Lcom/navdy/hud/app/util/BluetoothUtil;->cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)V

    .line 203
    invoke-static {v2}, Lcom/navdy/hud/app/util/BluetoothUtil;->removeBond(Landroid/bluetooth/BluetoothDevice;)V

    .line 204
    iput-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->cancelledDevice:Landroid/bluetooth/BluetoothDevice;

    .line 205
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->cancelledTime:J

    .line 206
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$700(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->reconnectRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/service/HudConnectionService;->access$600(Lcom/navdy/hud/app/service/HudConnectionService;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 207
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v3, v8}, Lcom/navdy/hud/app/service/HudConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;)Z

    .line 224
    .end local v1    # "bondState":I
    :cond_2
    :goto_0
    return-void

    .line 210
    :cond_3
    const-string v3, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 211
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->cancelledDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 212
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->cancelledTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0xfa

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 213
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$800(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Aborting pairing request for cancelled bonding"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService$3;->abortBroadcast()V

    .line 216
    :cond_4
    iput-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->cancelledDevice:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0

    .line 217
    :cond_5
    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 220
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 221
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService$3;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v3}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/service/DeviceSearch;->handleDisconnect(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method
