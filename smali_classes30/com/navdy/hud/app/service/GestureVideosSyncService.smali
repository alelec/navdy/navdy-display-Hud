.class public Lcom/navdy/hud/app/service/GestureVideosSyncService;
.super Lcom/navdy/hud/app/service/S3FileUploadService;
.source "GestureVideosSyncService.java"


# static fields
.field private static final MAX_GESTURE_VIDEOS_TO_UPLOAD:I = 0x64

.field public static final NAVDY_GESTURE_VIDEOS_BUCKET:Ljava/lang/String; = "navdy-gesture-videos"

.field private static isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static mIsInitialized:Z

.field private static sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

.field private static sGestureVideosFolder:Ljava/lang/String;

.field private static final sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/GestureVideosSyncService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 36
    sput-boolean v2, Lcom/navdy/hud/app/service/GestureVideosSyncService;->mIsInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "GESTURE_VIDEOS_SYNC"

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/S3FileUploadService;-><init>(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static addGestureVideoToUploadQueue(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p0, "file"    # Ljava/io/File;
    .param p1, "userTag"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->initializeIfNecessary(Ljava/io/File;)V

    .line 95
    sget-object v1, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    monitor-enter v1

    .line 96
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add gesture video to upload "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    new-instance v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/service/S3FileUploadService$Request;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 99
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Queue size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v3}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 100
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v0

    const/16 v2, 0x64

    if-le v0, v2, :cond_0

    .line 102
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Number of videos to upload exceeded "

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .line 106
    :cond_0
    monitor-exit v1

    .line 107
    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static initializeIfNecessary()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->initializeIfNecessary(Ljava/io/File;)V

    .line 111
    return-void
.end method

.method private static initializeIfNecessary(Ljava/io/File;)V
    .locals 15
    .param p0, "fileToIgnore"    # Ljava/io/File;

    .prologue
    const/4 v7, 0x0

    .line 114
    sget-object v9, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    monitor-enter v9

    .line 115
    :try_start_0
    sget-boolean v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->mIsInitialized:Z

    if-nez v6, :cond_6

    .line 116
    sget-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Not initialized , initializing now"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 117
    sget-boolean v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->mIsInitialized:Z

    if-nez v6, :cond_6

    .line 118
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/storage/PathManager;->getGestureVideosSyncFolder()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosFolder:Ljava/lang/String;

    .line 119
    new-instance v6, Ljava/io/File;

    sget-object v8, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosFolder:Ljava/lang/String;

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 120
    .local v0, "directories":[Ljava/io/File;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v2, "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    array-length v10, v0

    move v8, v7

    :goto_0
    if-ge v8, v10, :cond_5

    aget-object v5, v0, v8

    .line 122
    .local v5, "sessionDirectory":Ljava/io/File;
    sget-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Session directory :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 123
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 124
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 125
    .local v4, "gestureVideoFiles":[Ljava/io/File;
    array-length v6, v4

    if-nez v6, :cond_0

    .line 126
    sget-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Remove empty directory:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 127
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 129
    :cond_0
    array-length v11, v4

    move v6, v7

    :goto_1
    if-ge v6, v11, :cond_4

    aget-object v3, v4, v6

    .line 130
    .local v3, "gestureVideoFile":Ljava/io/File;
    sget-object v12, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Gesture video :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    if-nez v12, :cond_2

    .line 129
    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 134
    :cond_2
    if-eqz p0, :cond_3

    .line 136
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v12

    if-nez v12, :cond_1

    .line 143
    :cond_3
    :try_start_2
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 152
    .end local v0    # "directories":[Ljava/io/File;
    .end local v2    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v3    # "gestureVideoFile":Ljava/io/File;
    .end local v4    # "gestureVideoFiles":[Ljava/io/File;
    .end local v5    # "sessionDirectory":Ljava/io/File;
    :catchall_0
    move-exception v6

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 139
    .restart local v0    # "directories":[Ljava/io/File;
    .restart local v2    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v3    # "gestureVideoFile":Ljava/io/File;
    .restart local v4    # "gestureVideoFiles":[Ljava/io/File;
    .restart local v5    # "sessionDirectory":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 140
    .local v1, "e":Ljava/io/IOException;
    goto :goto_2

    .line 121
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "gestureVideoFile":Ljava/io/File;
    .end local v4    # "gestureVideoFiles":[Ljava/io/File;
    :cond_4
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto/16 :goto_0

    .line 147
    .end local v5    # "sessionDirectory":Ljava/io/File;
    :cond_5
    :try_start_3
    sget-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Number of Gesture videos :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 148
    sget-object v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    const/16 v7, 0x64

    invoke-static {v2, v6, v7}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->populateFilesQueue(Ljava/util/ArrayList;Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;I)V

    .line 149
    const/4 v6, 0x1

    sput-boolean v6, Lcom/navdy/hud/app/service/GestureVideosSyncService;->mIsInitialized:Z

    .line 152
    .end local v0    # "directories":[Ljava/io/File;
    .end local v2    # "filesToPopulate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_6
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 153
    return-void
.end method

.method public static scheduleWithDelay(J)V
    .locals 6
    .param p0, "delay"    # J

    .prologue
    .line 188
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/hud/app/service/GestureVideosSyncService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "SYNC"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x7b

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 191
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 192
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p0

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 193
    return-void
.end method

.method public static syncNow()V
    .locals 3

    .prologue
    .line 166
    sget-object v1, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "synNow"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 167
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/service/GestureVideosSyncService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 168
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 170
    return-void
.end method


# virtual methods
.method public canCompleteRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)Z
    .locals 1
    .param p1, "request"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method protected getAWSBucket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "navdy-gesture-videos"

    return-object v0
.end method

.method protected getCurrentRequest()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    return-object v0
.end method

.method protected getIsUploading()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method protected getKeyPrefix(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 81
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 82
    .local v0, "parent":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "preFix":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "archives"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method protected getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    return-object v0
.end method

.method protected initialize()V
    .locals 0

    .prologue
    .line 56
    invoke-static {}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->initializeIfNecessary()V

    .line 57
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 47
    invoke-super {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->onCreate()V

    .line 48
    return-void
.end method

.method public reSchedule()V
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reschedule"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 174
    const-wide/16 v0, 0x2710

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->scheduleWithDelay(J)V

    .line 175
    return-void
.end method

.method protected setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V
    .locals 0
    .param p1, "currentRequest"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 71
    sput-object p1, Lcom/navdy/hud/app/service/GestureVideosSyncService;->sCurrentRequest:Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .line 72
    return-void
.end method

.method public sync()V
    .locals 0

    .prologue
    .line 162
    invoke-static {}, Lcom/navdy/hud/app/service/GestureVideosSyncService;->syncNow()V

    .line 163
    return-void
.end method

.method protected uploadFinished(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "succeeded"    # Z
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "userTag"    # Ljava/lang/String;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/service/GestureVideosSyncService;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;

    invoke-direct {v1, p1, p2, p3}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 158
    return-void
.end method
