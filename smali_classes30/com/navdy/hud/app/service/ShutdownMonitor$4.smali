.class synthetic Lcom/navdy/hud/app/service/ShutdownMonitor$4;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 570
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->values()[Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_OBD_ACTIVE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_ACTIVE_USE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_QUIET_MODE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    .line 408
    :goto_5
    invoke-static {}, Lcom/navdy/hud/app/event/Shutdown$State;->values()[Lcom/navdy/hud/app/event/Shutdown$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->CANCELED:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMED:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    .line 371
    :goto_7
    invoke-static {}, Lcom/navdy/hud/app/event/Shutdown$Reason;->values()[Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_LOSS:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->CRITICAL_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->LOW_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->TIMEOUT:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_c
    return-void

    :catch_0
    move-exception v0

    goto :goto_c

    :catch_1
    move-exception v0

    goto :goto_b

    :catch_2
    move-exception v0

    goto :goto_a

    :catch_3
    move-exception v0

    goto :goto_9

    :catch_4
    move-exception v0

    goto :goto_8

    .line 408
    :catch_5
    move-exception v0

    goto :goto_7

    :catch_6
    move-exception v0

    goto :goto_6

    .line 570
    :catch_7
    move-exception v0

    goto :goto_5

    :catch_8
    move-exception v0

    goto :goto_4

    :catch_9
    move-exception v0

    goto/16 :goto_3

    :catch_a
    move-exception v0

    goto/16 :goto_2

    :catch_b
    move-exception v0

    goto/16 :goto_1

    :catch_c
    move-exception v0

    goto/16 :goto_0
.end method
