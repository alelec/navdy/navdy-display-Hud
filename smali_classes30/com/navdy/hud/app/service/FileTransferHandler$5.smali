.class Lcom/navdy/hud/app/service/FileTransferHandler$5;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferDone(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

.field final synthetic val$absolutePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/FileTransferHandler;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler$5;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$5;->val$absolutePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 240
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$5;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # getter for: Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$100(Lcom/navdy/hud/app/service/FileTransferHandler;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onFileTransferDone"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 241
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$5;->val$absolutePath:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 242
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v4

    if-nez v4, :cond_1

    .line 243
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$5;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # getter for: Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$100(Lcom/navdy/hud/app/service/FileTransferHandler;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot read from downloaded OTA file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 252
    :goto_0
    return-void

    .line 246
    :cond_1
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.navdy.hud.app.service.OTA_DOWNLOAD"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "path"

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 249
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    .line 250
    .local v2, "handle":Landroid/os/UserHandle;
    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method
