.class public Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;
.super Ljava/lang/Object;
.source "CustomNotificationServiceHandler.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    .line 50
    invoke-virtual {p1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public onShowCustomNotification(Lcom/navdy/service/library/events/notification/ShowCustomNotification;)V
    .locals 29
    .param p1, "event"    # Lcom/navdy/service/library/events/notification/ShowCustomNotification;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 59
    sget-object v5, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onShowCustomNotification: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/notification/ShowCustomNotification;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v24

    .line 61
    .local v24, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/notification/ShowCustomNotification;->id:Ljava/lang/String;

    const/4 v5, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 491
    :cond_1
    :goto_1
    return-void

    .line 61
    :sswitch_0
    const-string v7, "GENERIC_1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :sswitch_1
    const-string v7, "GENERIC_2"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :sswitch_2
    const-string v7, "INCOMING_PHONE_CALL_323"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x2

    goto :goto_0

    :sswitch_3
    const-string v7, "END_PHONE_CALL_323"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x3

    goto :goto_0

    :sswitch_4
    const-string v7, "PHONE_BATTERY_LOW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x4

    goto :goto_0

    :sswitch_5
    const-string v7, "PHONE_BATTERY_EXTREMELY_LOW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x5

    goto :goto_0

    :sswitch_6
    const-string v7, "PHONE_BATTERY_OK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x6

    goto :goto_0

    :sswitch_7
    const-string v7, "TEXT_NOTIFICAION_WITH_REPLY_408"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x7

    goto :goto_0

    :sswitch_8
    const-string v7, "TEXT_NOTIFICAION_WITH_NO_REPLY_510"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x8

    goto :goto_0

    :sswitch_9
    const-string v7, "TEXT_NOTIFICAION_WITH_REPLY_999"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x9

    goto :goto_0

    :sswitch_a
    const-string v7, "DIAL_CONNECTED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xa

    goto :goto_0

    :sswitch_b
    const-string v7, "DIAL_DISCONNECTED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xb

    goto :goto_0

    :sswitch_c
    const-string v7, "DIAL_FORGOTTEN_SINGLE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v7, "DIAL_FORGOTTEN_MULTIPLE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v7, "DIAL_BATTERY_LOW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v7, "DIAL_BATTERY_EXTREMELY_LOW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v7, "DIAL_BATTERY_VERY_LOW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v7, "DIAL_BATTERY_OK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v7, "MUSIC"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v7, "VOICE_ASSIST"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v7, "BRIGHTNESS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v7, "TRAFFIC_REROUTE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v7, "CLEAR_TRAFFIC_REROUTE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v7, "TRAFFIC_JAM"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v7, "CLEAR_TRAFFIC_JAM"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v7, "TRAFFIC_INCIDENT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v7, "CLEAR_TRAFFIC_INCIDENT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v7, "TRAFFIC_DELAY"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v7, "CLEAR_TRAFFIC_DELAY"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v7, "PHONE_DISCONNECTED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v7, "PHONE_CONNECTED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v7, "PHONE_APP_DISCONNECTED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v7, "CLEAR_CURRENT_TOAST"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v7, "CLEAR_ALL_TOAST"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v7, "CLEAR_ALL_TOAST_AND_CURRENT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v7, "GOOGLE_CALENDAR_1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v7, "APPLE_CALENDAR_1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v7, "LOW_FUEL_LEVEL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v7, "LOW_FUEL_LEVEL_NOCHECK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v7, "LOW_FUEL_LEVEL_CLEAR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v7, "OBD_LOW_FUEL_LEVEL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v7, "CLEAR_OBD_LOW_FUEL_LEVEL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v7, "FUEL_ADDED_TEST"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v7, "FIND_GAS_STATION"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v7, "FIND_ROUTE_TO_CLOSEST_GAS_STATION"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v5, 0x2c

    goto/16 :goto_0

    .line 63
    :pswitch_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v17, "data_1":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "John Doe"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Message Test"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_NAVDY_MAIN:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_MESSAGE_SIDE_BLUE:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 71
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "blah"

    .line 73
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 74
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v20

    .line 76
    .local v20, "glanceEvent_1":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 80
    .end local v17    # "data_1":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v20    # "glanceEvent_1":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_1
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v18, "data_2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Jone Doe"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Message Test"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 85
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "blah"

    .line 87
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 88
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v21

    .line 90
    .local v21, "glanceEvent_2":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 94
    .end local v18    # "data_2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v21    # "glanceEvent_2":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;-><init>()V

    const-string v7, "323-222-1111"

    .line 95
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 96
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    const-string v7, "Al Jazeera Al Bin Al Salaad"

    .line 97
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    .line 98
    invoke-virtual {v6}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v6

    .line 94
    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 102
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;-><init>()V

    const-string v7, "323-222-1111"

    .line 103
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 104
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    const-string v7, "Al Jazeera"

    .line 105
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v6

    .line 106
    invoke-virtual {v6}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v6

    .line 102
    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 110
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const/16 v8, 0xc

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 114
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 118
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const/16 v8, 0x24

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 122
    :pswitch_7
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    .local v4, "actionsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    sget-object v5, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v16, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "John Doe"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "4081111111"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_IS_SMS:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 132
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.navdy.sms"

    .line 134
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 135
    invoke-virtual {v5, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 136
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 138
    .local v19, "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 143
    .end local v4    # "actionsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_8
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Santa Singh"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "999-999-0999"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_IS_SMS:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 174
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.navdy.sms"

    .line 176
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 177
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 179
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 184
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_9
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 185
    .restart local v4    # "actionsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    sget-object v5, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Barack Obama"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "9999999999"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Hi how are u doing"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_IS_SMS:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 194
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 195
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.navdy.sms"

    .line 196
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 197
    invoke-virtual {v5, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 198
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 200
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 204
    .end local v4    # "actionsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_a
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->showConnectedToast()V

    goto/16 :goto_1

    .line 208
    :pswitch_b
    const-string v5, "Navdy Dial (test)"

    invoke-static {v5}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDisconnectedToast(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 212
    :pswitch_c
    const/4 v5, 0x0

    const-string v6, "Navdy Dial (test)"

    invoke-static {v5, v6}, Lcom/navdy/hud/app/device/dial/DialNotification;->showForgottenToast(ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 216
    :pswitch_d
    const/4 v5, 0x1

    const-string v6, "Navdy Dial (AAAA), Navdy Dial (BBBB)"

    invoke-static {v5, v6}, Lcom/navdy/hud/app/device/dial/DialNotification;->showForgottenToast(ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 220
    :pswitch_e
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->showLowBatteryToast()V

    goto/16 :goto_1

    .line 224
    :pswitch_f
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->showExtremelyLowBatteryToast()V

    goto/16 :goto_1

    .line 229
    :pswitch_10
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->showVeryLowBatteryToast()V

    goto/16 :goto_1

    .line 234
    :pswitch_11
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->dismissAllBatteryToasts()V

    goto/16 :goto_1

    .line 239
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 243
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 247
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 251
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    const-string v6, "Heaven"

    const-string v7, "Heaven, Hell"

    const-wide/16 v8, 0x5dc

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/32 v12, 0x493e0

    add-long/2addr v10, v12

    const-wide/16 v12, 0x2710

    const-wide/16 v14, 0x0

    invoke-direct/range {v5 .. v15}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJ)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 255
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 259
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    const/16 v7, 0x384

    invoke-direct {v6, v7}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;-><init>(I)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 263
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 267
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    sget-object v7, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->INCIDENT:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    sget-object v8, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    invoke-direct {v6, v7, v8}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 271
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 275
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    const-wide/16 v8, 0x1a4

    invoke-direct {v6, v8, v9}, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;-><init>(J)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 279
    :pswitch_1c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 283
    :pswitch_1d
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showDisconnectedToast(Z)V

    goto/16 :goto_1

    .line 287
    :pswitch_1e
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showConnectedToast()V

    goto/16 :goto_1

    .line 292
    :pswitch_1f
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showDisconnectedToast(Z)V

    goto/16 :goto_1

    .line 296
    :pswitch_20
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    goto/16 :goto_1

    .line 301
    :pswitch_21
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    goto/16 :goto_1

    .line 306
    :pswitch_22
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 307
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    goto/16 :goto_1

    .line 312
    :pswitch_23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xa

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long v22, v6, v8

    .line 313
    .local v22, "meetingTime":J
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 314
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Meeting with Obama"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\u200e\u202a12:00 \u2013 12:10 PM\u202c\u200e"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "575 7th Street, San Francisco, CA 94103"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 327
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 328
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.google.android.calendar"

    .line 329
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 330
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 332
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 337
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v22    # "meetingTime":J
    :pswitch_24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 338
    .restart local v22    # "meetingTime":J
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Huddle"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "1:30 pm - 1:45 pm"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 344
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 345
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.apple.mobilecal"

    .line 346
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 347
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 349
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 353
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v22    # "meetingTime":J
    :pswitch_25
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isFuelNotificationEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "14"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 361
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 362
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.navdy.fuel"

    .line 363
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 364
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 366
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 370
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_26
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .restart local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v5, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v6, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v6

    const-string v7, "14"

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    new-instance v5, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 374
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 375
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    const-string v6, "com.navdy.fuel"

    .line 376
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    .line 377
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 379
    .restart local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 383
    .end local v16    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v19    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_27
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 387
    :pswitch_28
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$TestObdLowFuelLevel;

    invoke-direct {v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$TestObdLowFuelLevel;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 391
    :pswitch_29
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$ClearTestObdLowFuelLevel;

    invoke-direct {v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$ClearTestObdLowFuelLevel;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 395
    :pswitch_2a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelAddedTestEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelAddedTestEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 400
    :pswitch_2b
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v5

    if-nez v5, :cond_2

    .line 401
    sget-object v5, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "FIND_GAS_STATION: here maps not initialized"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 453
    :catch_0
    move-exception v25

    .line 454
    .local v25, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "FIND_GAS_STATION"

    move-object/from16 v0, v25

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 404
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v26

    .line 405
    .local v26, "t1":J
    sget-object v5, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->GAS_CATEGORY:Lcom/here/android/mpa/search/CategoryFilter;

    const/4 v6, 0x3

    new-instance v7, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v7, v0, v1, v2}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;-><init>(Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;J)V

    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 460
    .end local v26    # "t1":J
    :pswitch_2c
    :try_start_2
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v5

    if-nez v5, :cond_3

    .line 461
    sget-object v5, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "FIND_ROUTE_TO_CLOSEST_GAS_STATION: here maps not initialized"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 485
    :catch_1
    move-exception v25

    .line 486
    .restart local v25    # "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "FIND_ROUTE_TO_CLOSEST_GAS_STATION"

    move-object/from16 v0, v25

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 464
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_3
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v26

    .line 465
    .restart local v26    # "t1":J
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v5

    new-instance v6, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v6, v0, v1, v2}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$2;-><init>(Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;J)V

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->findNearestGasStation(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 61
    :sswitch_data_0
    .sparse-switch
        -0x798c746d -> :sswitch_e
        -0x6cefe81c -> :sswitch_d
        -0x6c2741fa -> :sswitch_29
        -0x6b6bdb3a -> :sswitch_1a
        -0x6770a69e -> :sswitch_16
        -0x603bb94c -> :sswitch_28
        -0x5b05c08c -> :sswitch_15
        -0x584d3737 -> :sswitch_22
        -0x57c385f2 -> :sswitch_26
        -0x4fc49054 -> :sswitch_10
        -0x4c8a4aca -> :sswitch_23
        -0x4a95b674 -> :sswitch_1d
        -0x48c9f057 -> :sswitch_0
        -0x48c9f056 -> :sswitch_1
        -0x455e7ea3 -> :sswitch_2
        -0x377d0a11 -> :sswitch_1c
        -0x2dc07fa0 -> :sswitch_2c
        -0x260786c8 -> :sswitch_1e
        -0x18371a7a -> :sswitch_25
        -0x146fe263 -> :sswitch_11
        -0x3cbc77f -> :sswitch_1b
        -0xb403f8 -> :sswitch_2b
        -0x5941c9 -> :sswitch_21
        0x291a4ff -> :sswitch_6
        0x39b5f1c -> :sswitch_c
        0x464f605 -> :sswitch_12
        0x5010582 -> :sswitch_18
        0xc28d20b -> :sswitch_5
        0x13df9236 -> :sswitch_13
        0x158600f4 -> :sswitch_19
        0x1ee5c494 -> :sswitch_17
        0x1fabc6ba -> :sswitch_2a
        0x25d27ada -> :sswitch_8
        0x3b3c4a34 -> :sswitch_27
        0x3b880028 -> :sswitch_3
        0x3d936aad -> :sswitch_f
        0x438c1851 -> :sswitch_14
        0x49068195 -> :sswitch_24
        0x4e572e8a -> :sswitch_1f
        0x4fa2f071 -> :sswitch_4
        0x551974da -> :sswitch_a
        0x56d6e10f -> :sswitch_20
        0x58704f2e -> :sswitch_7
        0x5870630b -> :sswitch_9
        0x58ad12aa -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
    .end packed-switch
.end method

.method public start()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method
