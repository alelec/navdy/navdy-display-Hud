.class public Lcom/navdy/hud/app/service/FileTransferHandler;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"


# static fields
.field public static final ACTION_OTA_DOWNLOAD:Ljava/lang/String; = "com.navdy.hud.app.service.OTA_DOWNLOAD"

.field public static final OTA_DOWLOADING_PROPERTY:Ljava/lang/String; = "navdy.ota.downloading"

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private closed:Z

.field private context:Landroid/content/Context;

.field fileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

.field fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 48
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    .line 49
    new-instance v0, Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v1, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    invoke-direct {v0, p1, v1}, Lcom/navdy/service/library/file/FileTransferSessionManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/file/IFileTransferAuthority;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    .line 50
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->context:Landroid/content/Context;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/FileTransferHandler;I)Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/FileTransferHandler;
    .param p1, "x1"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler;->sendNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/service/FileTransferHandler;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/service/FileTransferHandler;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/FileTransferHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler;->setDownloadingProperty(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/service/FileTransferHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/FileTransferHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferDone(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized onFileTransferDone(Ljava/lang/String;)V
    .locals 3
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler$5;-><init>(Lcom/navdy/hud/app/service/FileTransferHandler;Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private sendNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 2
    .param p1, "transferId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "data":Lcom/navdy/service/library/events/file/FileTransferData;
    iget-object v1, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    invoke-virtual {v1, p1}, Lcom/navdy/service/library/file/FileTransferSessionManager;->getNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    iget-object v1, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 148
    :cond_0
    return-object v0
.end method

.method private setDownloadingProperty(Z)V
    .locals 6
    .param p1, "downloading"    # Z

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "time":I
    if-eqz p1, :cond_0

    .line 156
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 158
    :cond_0
    const-string v1, "navdy.ota.downloading"

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->closed:Z

    if-eqz v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->closed:Z

    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    invoke-virtual {v0}, Lcom/navdy/service/library/file/FileTransferSessionManager;->stop()V

    goto :goto_0
.end method

.method public onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 258
    return-void
.end method

.method public onDeviceDisconnected()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 262
    return-void
.end method

.method public onFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/file/FileTransferData;

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->closed:Z

    if-eqz v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler$3;-><init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferData;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/file/FileTransferRequest;

    .prologue
    const/4 v2, 0x5

    .line 54
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->closed:Z

    if-eqz v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 57
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    sget-object v1, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    if-ne v0, v1, :cond_1

    .line 58
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler$1;-><init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferRequest;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 113
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler$2;-><init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferRequest;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 3
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/FileTransferHandler;->closed:Z

    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler$4;-><init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferStatus;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
