.class public Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport$IAPListenerReceiver;
.super Ljava/lang/Object;
.source "ConnectionServiceAnalyticsSupport.java"

# interfaces
.implements Lcom/navdy/hud/mfi/IAPListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IAPListenerReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoprocessorStatusCheckFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "desc"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "errorCode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 65
    const-string v0, "Mobile_iAP_Failure"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Description"

    aput-object v3, v1, v2

    aput-object p1, v1, v4

    const/4 v2, 0x2

    const-string v3, "Status"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    const-string v3, "Error_Code"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p3, v1, v2

    # invokes: Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V
    invoke-static {v0, v4, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->access$000(Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onDeviceAuthenticationSuccess(I)V
    .locals 5
    .param p1, "retries"    # I

    .prologue
    const/4 v4, 0x1

    .line 70
    if-lez p1, :cond_0

    .line 71
    const-string v0, "Mobile_iAP_Retry_Success"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Retries"

    aput-object v3, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    # invokes: Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V
    invoke-static {v0, v4, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->access$000(Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 73
    :cond_0
    return-void
.end method
