.class public abstract Lcom/navdy/hud/app/service/S3FileUploadService;
.super Landroid/app/IntentService;
.source "S3FileUploadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;,
        Lcom/navdy/hud/app/service/S3FileUploadService$RequestTimeComparator;,
        Lcom/navdy/hud/app/service/S3FileUploadService$Request;,
        Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;
    }
.end annotation


# static fields
.field public static final ACTION_SYNC:Ljava/lang/String; = "SYNC"

.field private static final AWS_ACCOUNT_ID:Ljava/lang/String; = "AWS_ACCESS_KEY_ID"

.field private static final AWS_SECRET:Ljava/lang/String; = "AWS_SECRET_ACCESS_KEY"

.field public static final RETRY_DELAY:I = 0x2710

.field public static final S3_CONNECTION_TIMEOUT:I = 0x3a98

.field public static final S3_UPLOAD_TIMEOUT:I = 0x493e0


# instance fields
.field protected logger:Lcom/navdy/service/library/log/Logger;

.field private s3Client:Lcom/amazonaws/services/s3/AmazonS3Client;

.field private transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->getLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/S3FileUploadService;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/S3FileUploadService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    return-object v0
.end method

.method public static populateFilesQueue(Ljava/util/ArrayList;Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;I)V
    .locals 4
    .param p1, "filesQueue"    # Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    .param p2, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    if-eqz p0, :cond_1

    .line 129
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 130
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    new-instance v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/navdy/hud/app/service/S3FileUploadService$Request;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 132
    invoke-virtual {p1}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 133
    invoke-virtual {p1}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    goto :goto_0

    .line 138
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract canCompleteRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)Z
.end method

.method public createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;
    .locals 6

    .prologue
    .line 101
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "AWS_ACCESS_KEY_ID"

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "awsAccount":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "AWS_SECRET_ACCESS_KEY"

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "awsSecret":Ljava/lang/String;
    new-instance v3, Lcom/amazonaws/auth/BasicAWSCredentials;

    invoke-direct {v3, v0, v1}, Lcom/amazonaws/auth/BasicAWSCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .local v3, "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    new-instance v2, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v2}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    .line 108
    .local v2, "config":Lcom/amazonaws/ClientConfiguration;
    const/16 v4, 0x3a98

    invoke-virtual {v2, v4}, Lcom/amazonaws/ClientConfiguration;->setConnectionTimeout(I)V

    .line 109
    const v4, 0x493e0

    invoke-virtual {v2, v4}, Lcom/amazonaws/ClientConfiguration;->setSocketTimeout(I)V

    .line 110
    new-instance v4, Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-direct {v4, v3, v2}, Lcom/amazonaws/services/s3/AmazonS3Client;-><init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V

    return-object v4
.end method

.method protected abstract getAWSBucket()Ljava/lang/String;
.end method

.method protected abstract getCurrentRequest()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
.end method

.method protected abstract getIsUploading()Ljava/util/concurrent/atomic/AtomicBoolean;
.end method

.method protected abstract getKeyPrefix(Ljava/io/File;)Ljava/lang/String;
.end method

.method protected abstract getLogger()Lcom/navdy/service/library/log/Logger;
.end method

.method protected abstract getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
.end method

.method protected abstract initialize()V
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->s3Client:Lcom/amazonaws/services/s3/AmazonS3Client;

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->s3Client:Lcom/amazonaws/services/s3/AmazonS3Client;

    .line 118
    new-instance v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    iget-object v1, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->s3Client:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;-><init>(Lcom/amazonaws/services/s3/AmazonS3;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    .line 120
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 125
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 154
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const-string v11, "SYNC"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 156
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    .line 157
    .local v1, "connectedNetwork":Z
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Performing sync , connected to network ? : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 158
    if-nez v1, :cond_2

    .line 275
    .end local v1    # "connectedNetwork":Z
    :cond_0
    :goto_1
    return-void

    .line 154
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 161
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "connectedNetwork":Z
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->initialize()V

    .line 162
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    move-result-object v5

    .line 163
    .local v5, "gestureUploadQueue":Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    monitor-enter v5

    .line 164
    :try_start_0
    invoke-virtual {v5}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v11

    if-lez v11, :cond_8

    .line 165
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->getIsUploading()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    .line 166
    .local v7, "isUploading":Ljava/util/concurrent/atomic/AtomicBoolean;
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {v7, v11, v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 167
    const/4 v2, 0x0

    .line 169
    .local v2, "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    :cond_3
    if-eqz v2, :cond_4

    .line 170
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "File to upload "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", Does not exist anymore"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 172
    :cond_4
    invoke-virtual {v5}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->size()I

    move-result v11

    if-lez v11, :cond_6

    .line 173
    invoke-virtual {v5}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    move-result-object v2

    .line 178
    iget-object v11, v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 179
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/service/S3FileUploadService;->setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 180
    iget-object v4, v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    .line 181
    .local v4, "file":Ljava/io/File;
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->getKeyPrefix(Ljava/io/File;)Ljava/lang/String;

    move-result-object v10

    .line 182
    .local v10, "prefix":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 183
    .local v8, "key":Ljava/lang/String;
    :goto_2
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Trying to upload : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", Under :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", Key :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :try_start_1
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->getAWSBucket()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12, v8, v4}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->upload(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 194
    .local v9, "observer":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
    :try_start_2
    invoke-virtual {v9}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getId()I

    move-result v6

    .line 195
    .local v6, "id":I
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Transfer id "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 196
    new-instance v11, Lcom/navdy/hud/app/service/S3FileUploadService$1;

    invoke-direct {v11, p0, v7, v4, v9}, Lcom/navdy/hud/app/service/S3FileUploadService$1;-><init>(Lcom/navdy/hud/app/service/S3FileUploadService;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/io/File;Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;)V

    invoke-virtual {v9, v11}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->setTransferListener(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V

    .line 273
    .end local v2    # "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "id":I
    .end local v7    # "isUploading":Ljava/util/concurrent/atomic/AtomicBoolean;
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "observer":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
    .end local v10    # "prefix":Ljava/lang/String;
    :cond_5
    :goto_3
    monitor-exit v5

    goto/16 :goto_1

    :catchall_0
    move-exception v11

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v11

    .line 175
    .restart local v2    # "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .restart local v7    # "isUploading":Ljava/util/concurrent/atomic/AtomicBoolean;
    :cond_6
    const/4 v11, 0x0

    :try_start_3
    invoke-virtual {v7, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 176
    monitor-exit v5

    goto/16 :goto_1

    .line 182
    .restart local v4    # "file":Ljava/io/File;
    .restart local v10    # "prefix":Ljava/lang/String;
    :cond_7
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 187
    .restart local v8    # "key":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 188
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 189
    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 190
    const/4 v11, 0x0

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    iget-object v13, v2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->userTag:Ljava/lang/String;

    invoke-virtual {p0, v11, v12, v13}, Lcom/navdy/hud/app/service/S3FileUploadService;->uploadFinished(ZLjava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/S3FileUploadService;->sync()V

    .line 192
    monitor-exit v5

    goto/16 :goto_1

    .line 271
    .end local v2    # "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    .end local v4    # "file":Ljava/io/File;
    .end local v7    # "isUploading":Ljava/util/concurrent/atomic/AtomicBoolean;
    .end local v8    # "key":Ljava/lang/String;
    .end local v10    # "prefix":Ljava/lang/String;
    :cond_8
    iget-object v11, p0, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "Nothing to upload"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method

.method public abstract reSchedule()V
.end method

.method protected abstract setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V
.end method

.method public abstract sync()V
.end method

.method protected abstract uploadFinished(ZLjava/lang/String;Ljava/lang/String;)V
.end method
