.class final enum Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;
.super Ljava/lang/Enum;
.source "ShutdownMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MonitorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_ACTIVE_USE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_OBD_ACTIVE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_QUIET_MODE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field public static final enum STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 139
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_BOOT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 140
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_OBD_ACTIVE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_OBD_ACTIVE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 141
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_ACTIVE_USE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_ACTIVE_USE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 142
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_PENDING_SHUTDOWN"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_SHUTDOWN_PROMPT"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 144
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    const-string v1, "STATE_QUIET_MODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_QUIET_MODE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 138
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_OBD_ACTIVE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_ACTIVE_USE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_QUIET_MODE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->$VALUES:[Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 138
    const-class v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->$VALUES:[Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    return-object v0
.end method
