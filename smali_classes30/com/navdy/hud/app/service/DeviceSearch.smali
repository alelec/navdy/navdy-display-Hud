.class Lcom/navdy/hud/app/service/DeviceSearch;
.super Ljava/lang/Object;
.source "DeviceSearch.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDevice$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/DeviceSearch$EventSink;
    }
.end annotation


# static fields
.field private static final SEARCH_TIMEOUT:I = 0x3a98


# instance fields
.field private connectedDevices:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;"
        }
    .end annotation
.end field

.field private connectingDevices:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;"
        }
    .end annotation
.end field

.field private eventSink:Lcom/navdy/hud/app/service/DeviceSearch$EventSink;

.field private final handler:Landroid/os/Handler;

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field private remoteDevices:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;"
        }
    .end annotation
.end field

.field private startTime:J

.field private started:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/hud/app/service/DeviceSearch$EventSink;Z)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventSink"    # Lcom/navdy/hud/app/service/DeviceSearch$EventSink;
    .param p3, "parseEvents"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v5, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    .line 29
    new-instance v5, Ljava/util/ArrayDeque;

    invoke-direct {v5}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    .line 30
    new-instance v5, Ljava/util/ArrayDeque;

    invoke-direct {v5}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    .line 31
    new-instance v5, Ljava/util/ArrayDeque;

    invoke-direct {v5}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    .line 35
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->started:Z

    .line 36
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/navdy/hud/app/service/DeviceSearch;->startTime:J

    .line 46
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->handler:Landroid/os/Handler;

    .line 47
    iput-object p2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->eventSink:Lcom/navdy/hud/app/service/DeviceSearch$EventSink;

    .line 48
    invoke-static {p1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v4

    .line 49
    .local v4, "registry":Lcom/navdy/service/library/device/RemoteDeviceRegistry;
    invoke-virtual {v4}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getPairedConnections()Ljava/util/List;

    move-result-object v3

    .line 50
    .local v3, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 51
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 52
    .local v2, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {v0, p1, v2, p3}, Lcom/navdy/service/library/device/RemoteDevice;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 53
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 54
    iget-object v5, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v5, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v2    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/DeviceSearch;)Lcom/navdy/hud/app/service/DeviceSearch$EventSink;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/DeviceSearch;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->eventSink:Lcom/navdy/hud/app/service/DeviceSearch$EventSink;

    return-object v0
.end method

.method private closeAll(Ljava/util/Deque;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "queue":Ljava/util/Deque;, "Ljava/util/Deque<Lcom/navdy/service/library/device/RemoteDevice;>;"
    invoke-interface {p1}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/RemoteDevice;

    .line 135
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 136
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->queueDisconnect(Lcom/navdy/service/library/device/RemoteDevice;)V

    goto :goto_0

    .line 138
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    return-void
.end method

.method private findDevice(Landroid/bluetooth/BluetoothDevice;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;)",
            "Lcom/navdy/service/library/device/RemoteDevice;"
        }
    .end annotation

    .prologue
    .line 151
    .local p2, "queue":Ljava/util/Deque;, "Ljava/util/Deque<Lcom/navdy/service/library/device/RemoteDevice;>;"
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "bluetoothAddress":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Ljava/lang/String;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    return-object v1
.end method

.method private findDevice(Ljava/lang/String;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 4
    .param p1, "bluetoothAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Deque",
            "<",
            "Lcom/navdy/service/library/device/RemoteDevice;",
            ">;)",
            "Lcom/navdy/service/library/device/RemoteDevice;"
        }
    .end annotation

    .prologue
    .line 141
    .local p2, "queue":Ljava/util/Deque;, "Ljava/util/Deque<Lcom/navdy/service/library/device/RemoteDevice;>;"
    invoke-interface {p2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/device/RemoteDevice;

    .line 142
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 143
    .local v0, "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 147
    .end local v0    # "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v1    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private queueDisconnect(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 156
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/service/DeviceSearch$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/DeviceSearch$1;-><init>(Lcom/navdy/hud/app/service/DeviceSearch;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 164
    :cond_0
    return-void
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->eventSink:Lcom/navdy/hud/app/service/DeviceSearch$EventSink;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/service/DeviceSearch$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/DeviceSearch$2;-><init>(Lcom/navdy/hud/app/service/DeviceSearch;Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 238
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/DeviceSearch;->stop()V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->closeAll(Ljava/util/Deque;)V

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->closeAll(Ljava/util/Deque;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized forgetDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v1, 0x1

    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceSearch forgetting:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-direct {p0, p1, v2}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Landroid/bluetooth/BluetoothDevice;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 173
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 174
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "DeviceSearch removing device from connecting devices"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 175
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 176
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->queueDisconnect(Lcom/navdy/service/library/device/RemoteDevice;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :goto_0
    monitor-exit p0

    return v1

    .line 179
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-direct {p0, p1, v2}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Landroid/bluetooth/BluetoothDevice;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_1

    .line 181
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "DeviceSearch removing device from remote devices"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 182
    iget-object v2, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 171
    .end local v0    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 185
    .restart local v0    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized handleDisconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Landroid/bluetooth/BluetoothDevice;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->queueDisconnect(Lcom/navdy/service/library/device/RemoteDevice;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    monitor-exit p0

    return-void

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized next()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/service/DeviceSearch;->startTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3a98

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/DeviceSearch;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    monitor-exit p0

    return v1

    .line 81
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/DeviceSearch;->start()V

    .line 83
    iget-object v1, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/RemoteDevice;

    .line 84
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->connect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    :goto_1
    move v1, v2

    .line 89
    goto :goto_0

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 77
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 90
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->size()I

    move-result v3

    if-lez v3, :cond_3

    move v1, v2

    .line 92
    goto :goto_0

    .line 96
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/DeviceSearch;->stop()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceSearch connect failure:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    :cond_0
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceSearch connected:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_FOUND:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 200
    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    .line 198
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->sendEvent(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    monitor-exit p0

    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDeviceConnecting(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 190
    return-void
.end method

.method public declared-synchronized onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceSearch disconnected:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 216
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_LOST:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 218
    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    .line 216
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->sendEvent(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 227
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;[B)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # [B

    .prologue
    .line 223
    return-void
.end method

.method public declared-synchronized select(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 5
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "bluetoothAddress":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-direct {p0, v0, v4}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Ljava/lang/String;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v3

    .line 105
    .local v3, "missingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-direct {p0, v0, v4}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Ljava/lang/String;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v2

    .line 106
    .local v2, "connectingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-direct {p0, v0, v4}, Lcom/navdy/hud/app/service/DeviceSearch;->findDevice(Ljava/lang/String;Ljava/util/Deque;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 107
    .local v1, "connectedDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v3, :cond_0

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->remoteDevices:Ljava/util/Deque;

    invoke-interface {v4, v3}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 109
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    .end local v3    # "missingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 112
    .restart local v3    # "missingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    if-eqz v2, :cond_1

    .line 113
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectingDevices:Ljava/util/Deque;

    invoke-interface {v4, v2}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 114
    invoke-virtual {v2, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    move-object v3, v2

    .line 115
    goto :goto_0

    .line 117
    :cond_1
    if-eqz v1, :cond_2

    .line 118
    iget-object v4, p0, Lcom/navdy/hud/app/service/DeviceSearch;->connectedDevices:Ljava/util/Deque;

    invoke-interface {v4, v1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {v1, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v1

    .line 120
    goto :goto_0

    .line 123
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 103
    .end local v0    # "bluetoothAddress":Ljava/lang/String;
    .end local v1    # "connectedDevice":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v2    # "connectingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v3    # "missingDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public start()V
    .locals 3

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->started:Z

    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->started:Z

    .line 61
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_STARTED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 64
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->started:Z

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/DeviceSearch;->started:Z

    .line 69
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/DeviceSearch;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 72
    :cond_0
    return-void
.end method
