.class synthetic Lcom/navdy/hud/app/service/HudConnectionService$7;
.super Ljava/lang/Object;
.source "HudConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/HudConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 501
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->values()[Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_SELECT:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_16

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_15

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_STOP_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_14

    .line 361
    :goto_2
    invoke-static {}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->values()[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Coordinate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_13

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_12

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_11

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriveRecordingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_10

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_f

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_e

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_d

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_c

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferData:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_a

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AudioStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_9

    .line 427
    :goto_d
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_8

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_7

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_6

    .line 231
    :goto_10
    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->values()[Lcom/navdy/service/library/device/connection/ConnectionService$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    :try_start_11
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->START:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_5

    :goto_11
    :try_start_12
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_4

    :goto_12
    :try_start_13
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_3

    :goto_13
    :try_start_14
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_2

    :goto_14
    :try_start_15
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_1

    :goto_15
    :try_start_16
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_0

    :goto_16
    return-void

    :catch_0
    move-exception v0

    goto :goto_16

    :catch_1
    move-exception v0

    goto :goto_15

    :catch_2
    move-exception v0

    goto :goto_14

    :catch_3
    move-exception v0

    goto :goto_13

    :catch_4
    move-exception v0

    goto :goto_12

    :catch_5
    move-exception v0

    goto :goto_11

    .line 427
    :catch_6
    move-exception v0

    goto :goto_10

    :catch_7
    move-exception v0

    goto :goto_f

    :catch_8
    move-exception v0

    goto :goto_e

    .line 361
    :catch_9
    move-exception v0

    goto/16 :goto_d

    :catch_a
    move-exception v0

    goto/16 :goto_c

    :catch_b
    move-exception v0

    goto/16 :goto_b

    :catch_c
    move-exception v0

    goto/16 :goto_a

    :catch_d
    move-exception v0

    goto/16 :goto_9

    :catch_e
    move-exception v0

    goto/16 :goto_8

    :catch_f
    move-exception v0

    goto/16 :goto_7

    :catch_10
    move-exception v0

    goto/16 :goto_6

    :catch_11
    move-exception v0

    goto/16 :goto_5

    :catch_12
    move-exception v0

    goto/16 :goto_4

    :catch_13
    move-exception v0

    goto/16 :goto_3

    .line 501
    :catch_14
    move-exception v0

    goto/16 :goto_2

    :catch_15
    move-exception v0

    goto/16 :goto_1

    :catch_16
    move-exception v0

    goto/16 :goto_0
.end method
