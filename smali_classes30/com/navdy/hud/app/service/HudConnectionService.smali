.class public Lcom/navdy/hud/app/service/HudConnectionService;
.super Lcom/navdy/service/library/device/connection/ConnectionService;
.source "HudConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;
    }
.end annotation


# static fields
.field public static final ACTION_DEVICE_FORCE_RECONNECT:Ljava/lang/String; = "com.navdy.hud.app.force_reconnect"

.field private static final APP_TERMINATION_RECONNECT_DELAY_MS:Ljava/lang/String; = "persist.sys.app_reconnect_ms"

.field private static final AUTO_DRIVERECORD_PROP:Ljava/lang/String; = "persist.sys.driverecord.auto"

.field public static final EXTRA_HUD_FORCE_RECONNECT_REASON:Ljava/lang/String; = "force_reconnect_reason"

.field private static final PROXY_ENTRY_LOCAL_PORT:I = 0xbb8

.field public static final REASON_CONNECTION_DISCONNECTED:Ljava/lang/String; = "CONNECTION_DISCONNECTED"


# instance fields
.field private bluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private debugReceiver:Landroid/content/BroadcastReceiver;

.field private deviceReconnectRunnable:Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

.field private fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

.field private final gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

.field private iAPLinkFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

.field private isSimulatingGpsCoordinates:Z

.field private needAutoSearch:Z

.field private final routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

.field private search:Lcom/navdy/hud/app/service/DeviceSearch;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;-><init>()V

    .line 74
    new-instance v0, Lcom/navdy/hud/app/service/HudConnectionService$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/HudConnectionService$1;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->iAPLinkFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    .line 81
    invoke-static {}, Lcom/navdy/hud/app/debug/RouteRecorder;->getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    .line 82
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->needAutoSearch:Z

    .line 179
    new-instance v0, Lcom/navdy/hud/app/service/HudConnectionService$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/HudConnectionService$3;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    .line 492
    new-instance v0, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceReconnectRunnable:Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/HudConnectionService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->setBandwidthLevel(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/service/HudConnectionService;Lcom/squareup/wire/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;
    .param p1, "x1"    # Lcom/squareup/wire/Message;

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/service/HudConnectionService;Lcom/navdy/hud/app/service/DeviceSearch;)Lcom/navdy/hud/app/service/DeviceSearch;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;
    .param p1, "x1"    # Lcom/navdy/hud/app/service/DeviceSearch;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/service/HudConnectionService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->reconnectRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/connection/ConnectionService$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    return-object v0
.end method

.method private parseStartDrivePlaybackEvent([B)Z
    .locals 7
    .param p1, "eventData"    # [B

    .prologue
    const/4 v3, 0x0

    .line 552
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v5, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v4, p1, v5}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/NavdyEvent;

    .line 554
    .local v0, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v0}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    .line 555
    .local v1, "startDrivePlaybackEvent":Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;
    iget-object v4, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    iget-object v5, v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->label:Ljava/lang/String;

    iget-object v6, v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->playSecondaryLocation:Ljava/lang/Boolean;

    if-eqz v6, :cond_0

    iget-object v3, v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->playSecondaryLocation:Ljava/lang/Boolean;

    .line 556
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    :cond_0
    const/4 v6, 0x0

    .line 555
    invoke-virtual {v4, v5, v3, v6}, Lcom/navdy/hud/app/debug/RouteRecorder;->startPlayback(Ljava/lang/String;ZZ)V

    .line 558
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartDrivePlayback, name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->label:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    .end local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v1    # "startDrivePlaybackEvent":Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 559
    :catch_0
    move-exception v2

    .line 560
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private parseStartDriveRecordingEvent([B)Z
    .locals 8
    .param p1, "eventData"    # [B

    .prologue
    .line 567
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v6, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v5, p1, v6}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/NavdyEvent;

    .line 569
    .local v2, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v2}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    .line 571
    .local v3, "startDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    if-eqz v3, :cond_1

    .line 572
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    iget-object v6, v3, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;->label:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/app/debug/RouteRecorder;->startRecording(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 573
    .local v1, "fileName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 576
    new-instance v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;-><init>(Ljava/lang/String;)V

    .line 577
    .local v0, "event":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 578
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStartDriveRecording, name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;->label:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 588
    .end local v0    # "event":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v3    # "startDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    :goto_0
    const/4 v5, 0x1

    return v5

    .line 580
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v2    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .restart local v3    # "startDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "startDriveRecording event fileName is empty"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 585
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v3    # "startDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    :catch_0
    move-exception v4

    .line 586
    .local v4, "t":Ljava/lang/Throwable;
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 583
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v2    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .restart local v3    # "startDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "startDriveRecording event is null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private selectDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 5
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 593
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    if-eqz v2, :cond_1

    .line 594
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Connecting to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 595
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/service/DeviceSearch;->select(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 596
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 597
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found corresponding remote device:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 598
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;)Z

    .line 607
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    :goto_0
    return-void

    .line 601
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v2, p1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->findDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v1

    .line 602
    .local v1, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v1, :cond_0

    .line 603
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found corresponding connection info:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 604
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->connect(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public broadcastReconnectingIntent(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 671
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.navdy.hud.app.force_reconnect"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 672
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "force_reconnect_reason"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 673
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->sendBroadcast(Landroid/content/Intent;)V

    .line 674
    return-void
.end method

.method protected bridge synthetic createProxyService()Lcom/navdy/service/library/device/connection/ProxyService;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->createProxyService()Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

    move-result-object v0

    return-object v0
.end method

.method protected createProxyService()Lcom/navdy/service/library/device/connection/tunnel/Tunnel;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 613
    new-instance v0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

    new-instance v1, Lcom/navdy/service/library/network/TCPSocketAcceptor;

    const/16 v2, 0xbb8

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/navdy/service/library/network/TCPSocketAcceptor;-><init>(IZ)V

    new-instance v2, Lcom/navdy/hud/app/service/HudConnectionService$6;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/service/HudConnectionService$6;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;-><init>(Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/network/SocketFactory;)V

    return-object v0
.end method

.method protected enterState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .prologue
    const/4 v2, 0x0

    .line 246
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->enterState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 302
    return-void

    .line 248
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 255
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->isPromiscuous()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->startBroadcasters()V

    .line 258
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->startListeners()V

    .line 259
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    if-nez v0, :cond_2

    .line 260
    new-instance v0, Lcom/navdy/hud/app/service/DeviceSearch;

    new-instance v1, Lcom/navdy/hud/app/service/HudConnectionService$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/service/HudConnectionService$4;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    iget-boolean v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->inProcess:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/service/DeviceSearch;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/service/DeviceSearch$EventSink;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    .line 276
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/DeviceSearch;->next()Z

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    :cond_3
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 286
    :pswitch_2
    iput-boolean v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->needAutoSearch:Z

    .line 287
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    if-eqz v0, :cond_4

    .line 288
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/DeviceSearch;->close()V

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->findDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 292
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->recordNewDevice()V

    goto :goto_0

    .line 297
    :pswitch_3
    iput-boolean v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->needAutoSearch:Z

    goto :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected exitState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->exitState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 231
    sget-object v1, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 241
    :goto_0
    return-void

    .line 234
    :pswitch_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 235
    .local v0, "filter":Landroid/content/IntentFilter;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 236
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 237
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected forgetPairedDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/service/DeviceSearch;->forgetDevice(Landroid/bluetooth/BluetoothDevice;)Z

    .line 329
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->forgetPairedDevice(Landroid/bluetooth/BluetoothDevice;)V

    .line 330
    return-void
.end method

.method protected getConnectionListeners(Landroid/content/Context;)[Lcom/navdy/service/library/device/connection/ConnectionListener;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 155
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    new-array v0, v2, [Lcom/navdy/service/library/device/connection/ConnectionListener;

    new-instance v1, Lcom/navdy/service/library/device/connection/AcceptorListener;

    new-instance v2, Lcom/navdy/service/library/network/BTSocketAcceptor;

    const-string v3, "Navdy"

    sget-object v4, Lcom/navdy/hud/app/service/HudConnectionService;->NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v1, p1, v2, v3}, Lcom/navdy/service/library/device/connection/AcceptorListener;-><init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/navdy/service/library/device/connection/AcceptorListener;

    new-instance v2, Lcom/navdy/service/library/network/BTSocketAcceptor;

    const-string v3, "Navdy iAP"

    sget-object v4, Lcom/navdy/hud/app/service/HudConnectionService;->ACCESSORY_IAP2:Ljava/util/UUID;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v1, p1, v2, v3}, Lcom/navdy/service/library/device/connection/AcceptorListener;-><init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    aput-object v1, v0, v6

    .line 176
    .local v0, "connectionListeners":[Lcom/navdy/service/library/device/connection/ConnectionListener;
    :goto_0
    return-object v0

    .line 166
    .end local v0    # "connectionListeners":[Lcom/navdy/service/library/device/connection/ConnectionListener;
    :cond_0
    new-array v0, v2, [Lcom/navdy/service/library/device/connection/ConnectionListener;

    new-instance v1, Lcom/navdy/service/library/device/connection/AcceptorListener;

    new-instance v2, Lcom/navdy/service/library/network/TCPSocketAcceptor;

    const/16 v3, 0x5335

    invoke-direct {v2, v3}, Lcom/navdy/service/library/network/TCPSocketAcceptor;-><init>(I)V

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v1, p1, v2, v3}, Lcom/navdy/service/library/device/connection/AcceptorListener;-><init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/navdy/service/library/device/connection/AcceptorListener;

    new-instance v2, Lcom/navdy/service/library/network/BTSocketAcceptor;

    const-string v3, "Navdy"

    sget-object v4, Lcom/navdy/hud/app/service/HudConnectionService;->NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v1, p1, v2, v3}, Lcom/navdy/service/library/device/connection/AcceptorListener;-><init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    aput-object v1, v0, v6

    .restart local v0    # "connectionListeners":[Lcom/navdy/service/library/device/connection/ConnectionListener;
    goto :goto_0
.end method

.method public getDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 661
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 666
    :cond_0
    :goto_0
    return-object v1

    .line 664
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v2

    iget-object v1, v2, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 665
    :catch_0
    move-exception v0

    .line 666
    .local v0, "t":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method protected getRemoteDeviceBroadcasters()[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    new-array v0, v3, [Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    new-instance v1, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;

    invoke-direct {v1}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;-><init>()V

    aput-object v1, v0, v2

    .line 149
    .local v0, "remoteDeviceBroadcasters":[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
    :goto_0
    return-object v0

    .line 144
    .end local v0    # "remoteDeviceBroadcasters":[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    new-instance v1, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;

    invoke-direct {v1}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Lcom/navdy/service/library/device/discovery/TCPRemoteDeviceBroadcaster;

    .line 146
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/device/discovery/TCPRemoteDeviceBroadcaster;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    .restart local v0    # "remoteDeviceBroadcasters":[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
    goto :goto_0
.end method

.method protected handleDeviceDisconnect(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 342
    invoke-super {p0, p1, p2}, Lcom/navdy/service/library/device/connection/ConnectionService;->handleDeviceDisconnect(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 343
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HUDConnectionService: handleDeviceDisconnect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Cause :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-ne v0, p1, :cond_0

    .line 345
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ABORTED:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serverMode:Z

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 347
    sget-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->BLUETOOTH_DISCONNECTED:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    invoke-static {v0}, Lcom/navdy/hud/app/util/CrashReportService;->dumpCrashReportAsync(Lcom/navdy/hud/app/util/CrashReportService$CrashType;)V

    .line 351
    :cond_0
    return-void
.end method

.method protected heartBeat()V
    .locals 2

    .prologue
    .line 311
    sget-object v0, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 318
    :goto_0
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->heartBeat()V

    .line 319
    return-void

    .line 315
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/DeviceSearch;->next()Z

    goto :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public isPromiscuous()Z
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Lcom/navdy/hud/app/device/PowerManager;->isAwake()Z

    move-result v0

    return v0
.end method

.method public needAutoSearch()Z
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->needAutoSearch:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/device/PowerManager;->isAwake()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->onCreate()V

    .line 93
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->iAPLinkFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    invoke-static {v1, v2}, Lcom/navdy/service/library/device/link/LinkManager;->registerFactory(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;)V

    .line 94
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    iget-object v2, p0, Lcom/navdy/hud/app/service/HudConnectionService;->iAPLinkFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    invoke-static {v1, v2}, Lcom/navdy/service/library/device/link/LinkManager;->registerFactory(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;)V

    .line 96
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->setConnectionService(Lcom/navdy/hud/app/service/HudConnectionService;)V

    .line 97
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->setConnectionService(Lcom/navdy/hud/app/service/HudConnectionService;)V

    .line 99
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serverMode:Z

    .line 100
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->inProcess:Z

    .line 102
    invoke-static {}, Lcom/navdy/service/library/util/NetworkActivityTracker;->getInstance()Lcom/navdy/service/library/util/NetworkActivityTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/util/NetworkActivityTracker;->start()V

    .line 103
    new-instance v1, Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/service/FileTransferHandler;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    .line 104
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "LINK_BANDWIDTH_LEVEL_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    const-string v1, "NAVDY_LINK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 108
    new-instance v1, Lcom/navdy/hud/app/service/HudConnectionService$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/service/HudConnectionService$2;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    iput-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->debugReceiver:Landroid/content/BroadcastReceiver;

    .line 115
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->debugReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 117
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->onDestroy()V

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopRecording(Z)V

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->shutdown()V

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->debugReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->debugReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 130
    :cond_0
    return-void
.end method

.method public onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 334
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/service/FileTransferHandler;->onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 337
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/HudConnectionService;->startProxyService()V

    .line 338
    return-void
.end method

.method public onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 1
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 355
    invoke-super {p0, p1, p2}, Lcom/navdy/service/library/device/connection/ConnectionService;->onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/FileTransferHandler;->onDeviceDisconnected()V

    .line 357
    return-void
.end method

.method protected processEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z
    .locals 12
    .param p1, "eventData"    # [B
    .param p2, "messageType"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 361
    sget-object v9, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    invoke-virtual {p2}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :cond_0
    :goto_0
    move v7, v8

    .line 474
    :cond_1
    :goto_1
    return v7

    .line 365
    :pswitch_0
    :try_start_0
    iget-boolean v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->isSimulatingGpsCoordinates:Z

    if-nez v8, :cond_1

    .line 366
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v8, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 367
    .local v5, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/location/Coordinate;

    .line 368
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->feedLocation(Lcom/navdy/service/library/events/location/Coordinate;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 370
    .end local v0    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_0
    move-exception v6

    .line 371
    .local v6, "t":Ljava/lang/Throwable;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 376
    .end local v6    # "t":Ljava/lang/Throwable;
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->parseStartDriveRecordingEvent([B)Z

    move-result v7

    goto :goto_1

    .line 378
    :pswitch_2
    iget-object v9, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v9, v8}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopRecording(Z)V

    .line 379
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "onStopDriveRecording"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 383
    :pswitch_3
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v8}, Lcom/navdy/hud/app/debug/RouteRecorder;->requestRecordings()V

    .line 384
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "onDriveRecordingsRequest"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 388
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->parseStartDrivePlaybackEvent([B)Z

    move v7, v8

    .line 389
    goto :goto_1

    .line 392
    :pswitch_5
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v7}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    .line 393
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "onStopDrivePlayback"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v7, v8

    .line 394
    goto :goto_1

    .line 398
    :pswitch_6
    :try_start_1
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v8, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 399
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/file/FileTransferRequest;

    .line 400
    .local v4, "message":Lcom/navdy/service/library/events/file/FileTransferRequest;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 401
    .end local v4    # "message":Lcom/navdy/service/library/events/file/FileTransferRequest;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_1
    move-exception v6

    .line 402
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 407
    .end local v6    # "t":Ljava/lang/Throwable;
    :pswitch_7
    :try_start_2
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v8, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 408
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/file/FileTransferStatus;

    .line 409
    .local v4, "message":Lcom/navdy/service/library/events/file/FileTransferStatus;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 410
    .end local v4    # "message":Lcom/navdy/service/library/events/file/FileTransferStatus;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_2
    move-exception v6

    .line 411
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 416
    .end local v6    # "t":Ljava/lang/Throwable;
    :pswitch_8
    :try_start_3
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v8, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 417
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/file/FileTransferData;

    .line 418
    .local v4, "message":Lcom/navdy/service/library/events/file/FileTransferData;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->fileTransferHandler:Lcom/navdy/hud/app/service/FileTransferHandler;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 419
    .end local v4    # "message":Lcom/navdy/service/library/events/file/FileTransferData;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_3
    move-exception v6

    .line 420
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v8, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 425
    .end local v6    # "t":Ljava/lang/Throwable;
    :pswitch_9
    :try_start_4
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v7, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 426
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    .line 427
    .local v4, "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    sget-object v7, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v9, v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_1

    .end local v4    # "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :goto_2
    move v7, v8

    .line 443
    goto/16 :goto_1

    .line 430
    .restart local v4    # "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :pswitch_a
    const-string v7, "persist.sys.app_reconnect_ms"

    const/16 v9, 0x3e8

    invoke-static {v7, v9}, Lcom/navdy/hud/app/util/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 431
    .local v1, "delay":I
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceReconnectRunnable:Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

    const-string v9, "CONNECTION_DISCONNECTED"

    invoke-virtual {v7, v9}, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;->setReason(Ljava/lang/String;)V

    .line 432
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v9, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceReconnectRunnable:Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

    int-to-long v10, v1

    invoke-virtual {v7, v9, v10, v11}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    .line 440
    .end local v1    # "delay":I
    .end local v4    # "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_4
    move-exception v6

    .line 441
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 437
    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v4    # "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :pswitch_b
    :try_start_5
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v9, p0, Lcom/navdy/hud/app/service/HudConnectionService;->deviceReconnectRunnable:Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_2

    .line 446
    .end local v4    # "message":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :pswitch_c
    :try_start_6
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v9, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v7, p1, v9}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/NavdyEvent;

    .line 447
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/audio/AudioStatus;

    .line 449
    .local v4, "message":Lcom/navdy/service/library/events/audio/AudioStatus;
    if-eqz v4, :cond_0

    .line 450
    iget-object v7, v4, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    sget-object v9, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_HFP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    if-ne v7, v9, :cond_0

    .line 451
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "AudioStatus for HFP received"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 452
    iget-object v7, v4, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 453
    .local v3, "isConnected":Z
    if-eqz v3, :cond_3

    .line 454
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "AudioStatus: HFP connected"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 455
    iget-object v7, v4, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 456
    .local v2, "hasScoConnection":Z
    if-eqz v2, :cond_2

    .line 457
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "AudioStatus: has SCO connection"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 458
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/service/HudConnectionService;->setBandwidthLevel(I)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_0

    .line 470
    .end local v2    # "hasScoConnection":Z
    .end local v3    # "isConnected":Z
    .end local v4    # "message":Lcom/navdy/service/library/events/audio/AudioStatus;
    .end local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_5
    move-exception v6

    .line 471
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 460
    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v2    # "hasScoConnection":Z
    .restart local v3    # "isConnected":Z
    .restart local v4    # "message":Lcom/navdy/service/library/events/audio/AudioStatus;
    .restart local v5    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :cond_2
    :try_start_7
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "AudioStatus: does not have SCO connection"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 461
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/service/HudConnectionService;->setBandwidthLevel(I)V

    goto/16 :goto_0

    .line 464
    .end local v2    # "hasScoConnection":Z
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "AudioStatus: HFP disconnected"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 465
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/service/HudConnectionService;->setBandwidthLevel(I)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_c
    .end packed-switch

    .line 427
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method protected processLocalEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z
    .locals 7
    .param p1, "eventData"    # [B
    .param p2, "messageType"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 496
    sget-object v5, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne p2, v5, :cond_3

    .line 498
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v6, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v5, p1, v6}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/NavdyEvent;

    .line 499
    .local v0, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    sget-object v5, Lcom/navdy/service/library/events/Ext_NavdyEvent;->connectionRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    .line 500
    .local v1, "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    if-eqz v1, :cond_0

    .line 501
    sget-object v5, Lcom/navdy/hud/app/service/HudConnectionService$7;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action:[I

    iget-object v6, v1, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 547
    .end local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v1    # "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    :cond_0
    :goto_0
    return v3

    .line 503
    .restart local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .restart local v1    # "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    :pswitch_0
    iget-object v5, v1, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 504
    new-instance v5, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v6, v1, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/navdy/hud/app/service/HudConnectionService;->selectDevice(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 529
    .end local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v1    # "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    :catch_0
    move-exception v2

    .line 530
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_1
    move v3, v4

    .line 547
    goto :goto_0

    .line 507
    .restart local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .restart local v1    # "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    :cond_2
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/service/HudConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;)Z

    goto :goto_0

    .line 511
    :pswitch_1
    sget-object v5, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/service/HudConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 514
    :pswitch_2
    iget-object v5, p0, Lcom/navdy/hud/app/service/HudConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    new-instance v6, Lcom/navdy/hud/app/service/HudConnectionService$5;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/service/HudConnectionService$5;-><init>(Lcom/navdy/hud/app/service/HudConnectionService;)V

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 532
    .end local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v1    # "request":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    :cond_3
    sget-object v5, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne p2, v5, :cond_4

    .line 533
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->parseStartDrivePlaybackEvent([B)Z

    .line 534
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally([B)V

    goto :goto_0

    .line 536
    :cond_4
    sget-object v5, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne p2, v5, :cond_5

    .line 537
    iget-object v4, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    .line 538
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally([B)V

    .line 539
    iget-object v4, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "onStopDrivePlayback"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 541
    :cond_5
    sget-object v3, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne p2, v3, :cond_6

    .line 542
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->parseStartDriveRecordingEvent([B)Z

    move-result v3

    goto :goto_0

    .line 543
    :cond_6
    sget-object v3, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne p2, v3, :cond_1

    .line 544
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->routeRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopRecording(Z)V

    .line 545
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally([B)V

    goto :goto_1

    .line 501
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized reconnect(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 655
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->broadcastReconnectingIntent(Ljava/lang/String;)V

    .line 656
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnect(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 657
    monitor-exit p0

    return-void

    .line 655
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reconnectAfterDeadConnection()Z
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    return v0
.end method

.method protected sendEventsOnLocalConnect()V
    .locals 6

    .prologue
    .line 639
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "sendEventsOnLocalConnect"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 640
    invoke-static {}, Lcom/navdy/hud/app/debug/RouteRecorder;->getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v2

    .line 641
    .local v2, "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 642
    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->getLabel()Ljava/lang/String;

    move-result-object v1

    .line 643
    .local v1, "label":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 644
    new-instance v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;-><init>(Ljava/lang/String;)V

    .line 645
    .local v0, "autoDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 646
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendEventsOnLocalConnect: send recording event:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 651
    .end local v0    # "autoDriveRecordingEvent":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .end local v1    # "label":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 648
    .restart local v1    # "label":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/service/HudConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "sendEventsOnLocalConnect: invalid label"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSimulatingGpsCoordinates(Z)V
    .locals 0
    .param p1, "isSimulatingGpsCoordinates"    # Z

    .prologue
    .line 635
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/HudConnectionService;->isSimulatingGpsCoordinates:Z

    .line 636
    return-void
.end method
