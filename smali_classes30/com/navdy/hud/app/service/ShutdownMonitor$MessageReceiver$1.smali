.class Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$400(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver$1;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/Shutdown;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_LOSS:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 264
    :cond_0
    return-void
.end method
