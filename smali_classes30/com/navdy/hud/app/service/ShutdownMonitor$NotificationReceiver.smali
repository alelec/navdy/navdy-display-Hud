.class Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationReceiver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;


# direct methods
.method private constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor;Lcom/navdy/hud/app/service/ShutdownMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p2, "x1"    # Lcom/navdy/hud/app/service/ShutdownMonitor$1;

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;
    .param p1, "x1"    # Lcom/navdy/hud/app/event/Shutdown;

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->triggerShutdown(Lcom/navdy/hud/app/event/Shutdown;)V

    return-void
.end method

.method private triggerShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;

    .prologue
    .line 403
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;Lcom/navdy/hud/app/event/Shutdown;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 404
    return-void
.end method


# virtual methods
.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 431
    iget-boolean v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;->connected:Z

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1502(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 433
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1602(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 440
    :goto_0
    return-void

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J
    invoke-static {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1502(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 437
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_TIMEOUT:J
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1800()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onAccelerateShutdown(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 455
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "received AccelerateShutdown, reason = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 456
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J
    invoke-static {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1602(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 457
    return-void
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 444
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_1

    .line 445
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J
    invoke-static {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1902(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 446
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J
    invoke-static {v0, v4, v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1602(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    .line 447
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->onWakeEvent()V
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2000(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 449
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J
    invoke-static {v0, v4, v5}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1902(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J

    goto :goto_0
.end method

.method public onDrivingStateChange(Lcom/navdy/hud/app/event/DrivingStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DrivingStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 461
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-boolean v1, p1, Lcom/navdy/hud/app/event/DrivingStateChange;->driving:Z

    # setter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2102(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z

    .line 462
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2100(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->onMovement()V
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$2200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    .line 465
    :cond_0
    return-void
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 408
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    iget-object v1, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 422
    :goto_0
    return-void

    .line 410
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    goto :goto_0

    .line 414
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$1;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;Lcom/navdy/hud/app/event/Shutdown;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # invokes: Lcom/navdy/hud/app/service/ShutdownMonitor;->enterBootState()V
    invoke-static {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$1400(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    .line 427
    return-void
.end method
