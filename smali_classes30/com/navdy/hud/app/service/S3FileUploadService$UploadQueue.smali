.class public Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
.super Ljava/lang/Object;
.source "S3FileUploadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/S3FileUploadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadQueue"
.end annotation


# instance fields
.field private final internalQueue:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/navdy/hud/app/service/S3FileUploadService$Request;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xf

    new-instance v2, Lcom/navdy/hud/app/service/S3FileUploadService$RequestTimeComparator;

    invoke-direct {v2}, Lcom/navdy/hud/app/service/S3FileUploadService$RequestTimeComparator;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->internalQueue:Ljava/util/PriorityQueue;

    return-void
.end method


# virtual methods
.method public add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V
    .locals 1
    .param p1, "request"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->internalQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public peek()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->internalQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    return-object v0
.end method

.method public pop()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->internalQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->internalQueue:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    return v0
.end method
