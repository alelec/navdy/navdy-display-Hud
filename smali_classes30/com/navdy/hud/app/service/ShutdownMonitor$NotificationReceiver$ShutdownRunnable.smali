.class Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShutdownRunnable"
.end annotation


# instance fields
.field private final event:Lcom/navdy/hud/app/event/Shutdown;

.field final synthetic this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 0
    .param p2, "r"    # Lcom/navdy/hud/app/event/Shutdown;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    iput-object p2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->event:Lcom/navdy/hud/app/event/Shutdown;

    .line 358
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 361
    iget-object v8, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->event:Lcom/navdy/hud/app/event/Shutdown;

    iget-object v5, v8, Lcom/navdy/hud/app/event/Shutdown;->reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 362
    .local v5, "reason":Lcom/navdy/hud/app/event/Shutdown$Reason;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/navdy/hud/app/HudApplication;->setShutdownReason(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    .line 365
    iget-object v8, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v8, v8, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v8, v8, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/device/PowerManager;->quietModeEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    move v4, v6

    .line 366
    .local v4, "fullShutdown":Z
    :goto_0
    iget-object v8, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v8, v8, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v8}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v2

    .line 367
    .local v2, "batteryVoltage":D
    iget-object v8, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v8, v8, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v8}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/obd/ObdManager;->getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->isAutoOnEnabled()Z

    move-result v0

    .line 369
    .local v0, "autoOnEnabled":Z
    const-wide v8, 0x402a333340000000L    # 13.100000381469727

    cmpl-double v8, v2, v8

    if-ltz v8, :cond_2

    move v1, v6

    .line 371
    .local v1, "charging":Z
    :goto_1
    sget-object v6, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason:[I

    invoke-virtual {v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 383
    :goto_2
    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Shutting down, auto-on:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " voltage:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " charging:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 385
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 389
    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v6, v6, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v6}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->isSleeping()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 390
    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v6, v6, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v6}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->wakeup()V

    .line 391
    const/16 v6, 0x3e8

    invoke-static {v6}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 393
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v6, v6, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    # getter for: Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v6}, Lcom/navdy/hud/app/service/ShutdownMonitor;->access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/navdy/hud/app/obd/ObdManager;->sleep(Z)V

    .line 398
    :goto_3
    iget-object v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver$ShutdownRunnable;->this$1:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    iget-object v6, v6, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;->this$0:Lcom/navdy/hud/app/service/ShutdownMonitor;

    iget-object v6, v6, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v6, v5, v4}, Lcom/navdy/hud/app/device/PowerManager;->androidShutdown(Lcom/navdy/hud/app/event/Shutdown$Reason;Z)V

    .line 399
    return-void

    .end local v0    # "autoOnEnabled":Z
    .end local v1    # "charging":Z
    .end local v2    # "batteryVoltage":D
    .end local v4    # "fullShutdown":Z
    :cond_1
    move v4, v7

    .line 365
    goto/16 :goto_0

    .restart local v0    # "autoOnEnabled":Z
    .restart local v2    # "batteryVoltage":D
    .restart local v4    # "fullShutdown":Z
    :cond_2
    move v1, v7

    .line 369
    goto :goto_1

    .line 379
    .restart local v1    # "charging":Z
    :pswitch_0
    const/4 v4, 0x1

    goto :goto_2

    .line 395
    :cond_3
    const/4 v4, 0x1

    goto :goto_3

    .line 371
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
