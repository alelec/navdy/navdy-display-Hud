.class Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;
.super Ljava/lang/Object;
.source "CustomNotificationServiceHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->onShowCustomNotification(Lcom/navdy/service/library/events/notification/ShowCustomNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

.field final synthetic val$t1:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;->this$0:Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;

    iput-wide p2, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;->val$t1:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/util/List;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410
    .local p1, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 411
    .local v10, "t2":J
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "FIND_GAS_STATION: found gas station ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") time to find ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;->val$t1:J

    sub-long v14, v10, v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 412
    const/4 v3, 0x1

    .line 414
    .local v3, "counter":I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteStartPoint()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 415
    .local v4, "currentPos":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v4, :cond_1

    .line 416
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 422
    :goto_0
    if-nez v4, :cond_2

    .line 423
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "FIND_GAS_STATION no current position"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 445
    .end local v3    # "counter":I
    .end local v4    # "currentPos":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "t2":J
    :cond_0
    :goto_1
    return-void

    .line 418
    .restart local v3    # "counter":I
    .restart local v4    # "currentPos":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v10    # "t2":J
    :cond_1
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "using debug start point:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 442
    .end local v3    # "counter":I
    .end local v4    # "currentPos":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "t2":J
    :catch_0
    move-exception v9

    .line 443
    .local v9, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "FIND_GAS_STATION"

    invoke-virtual {v12, v13, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 427
    .end local v9    # "t":Ljava/lang/Throwable;
    .restart local v3    # "counter":I
    .restart local v4    # "currentPos":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v10    # "t2":J
    :cond_2
    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/here/android/mpa/search/Place;

    .line 428
    .local v8, "place":Lcom/here/android/mpa/search/Place;
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v7

    .line 429
    .local v7, "location":Lcom/here/android/mpa/search/Location;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->getPlaceEntry(Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    .line 430
    .local v6, "gasNavGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v13

    invoke-virtual {v13}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "<br/>"

    const-string v15, ", "

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "\n"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 431
    .local v2, "address":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    .line 432
    .local v5, "gasDisplayGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FIND_GAS_STATION ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " name ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 433
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " address ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " distance ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 435
    invoke-virtual {v4, v6}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] meters"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " displayPos ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " navPos ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 432
    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 440
    add-int/lit8 v3, v3, 0x1

    .line 441
    goto/16 :goto_2
.end method

.method public onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    .locals 6
    .param p1, "error"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    .prologue
    .line 449
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 450
    .local v0, "t2":J
    # getter for: Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FIND_GAS_STATION: could not find gas station:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time to error ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/hud/app/service/CustomNotificationServiceHandler$1;->val$t1:J

    sub-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 451
    return-void
.end method
