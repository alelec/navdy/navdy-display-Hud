.class public Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;
.super Ljava/lang/Object;
.source "ConnectionServiceAnalyticsSupport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport$IAPListenerReceiver;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Z
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 19
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V

    return-void
.end method

.method public static recordGpsAccuracy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "minimum"    # Ljava/lang/String;
    .param p1, "maximum"    # Ljava/lang/String;
    .param p2, "average"    # Ljava/lang/String;

    .prologue
    .line 22
    const-string v0, "GPS_Accuracy"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Min_Accuracy"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "Max_Accuracy"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string v3, "Average_Accuracy"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public static recordGpsAcquireLocation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "time"    # Ljava/lang/String;
    .param p1, "accuracy"    # Ljava/lang/String;

    .prologue
    .line 29
    const-string v0, "GPS_Acquired_Location"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "time"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "accuracy"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public static recordGpsAttemptAcquireLocation()V
    .locals 2

    .prologue
    .line 35
    const-string v0, "GPS_Attempt_Acquire_Location"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public static recordGpsLostLocation(Ljava/lang/String;)V
    .locals 4
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 39
    const-string v0, "GPS_Lost_Signal"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "time"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static recordNewDevice()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "Analytics_New_Device"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method private static varargs sendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "includeDeviceInfo"    # Z
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 51
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.navdy.hud.app.analytics.AnalyticsEvent"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "tag"

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v3, "arguments"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    if-eqz p1, :cond_0

    .line 55
    const-string v3, "include_device_info"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 59
    .local v1, "handle":Landroid/os/UserHandle;
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 60
    return-void
.end method

.method private static varargs sendEvent(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->sendEvent(Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 49
    return-void
.end method
