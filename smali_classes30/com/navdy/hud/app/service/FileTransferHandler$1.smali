.class Lcom/navdy/hud/app/service/FileTransferHandler$1;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 61
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v5, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v5}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkBandwidthLevel()I

    move-result v5

    if-gtz v5, :cond_1

    .line 62
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Link bandwidth is low"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 63
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    if-eqz v5, :cond_0

    .line 64
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "FileTransferRequest (pull) : rejecting file transfer request to avoid link traffic"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 65
    new-instance v5, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v5

    sget-object v6, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v3

    .line 66
    .local v3, "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v5, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v5, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 110
    .end local v3    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v5, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v6, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/file/FileTransferSessionManager;->handleFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v1

    .line 73
    .local v1, "fileTransferResponse":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v5, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v5, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 74
    iget-object v5, v1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 75
    iget-object v5, v1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 77
    .local v2, "mTransferId":I
    const/4 v0, 0x0

    .line 80
    .local v0, "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :try_start_0
    iget-object v5, v1, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_5

    .line 83
    :cond_2
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # invokes: Lcom/navdy/hud/app/service/FileTransferHandler;->sendNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    invoke-static {v5, v2}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$000(Lcom/navdy/hud/app/service/FileTransferHandler;I)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_4

    .line 85
    const-wide/16 v6, 0x3e8

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 91
    if-eqz v0, :cond_3

    iget-object v5, v0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 92
    :cond_3
    :goto_1
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 93
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v5, v5, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    iget-object v6, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->val$request:Lcom/navdy/service/library/events/file/FileTransferRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-interface {v5, v6}, Lcom/navdy/service/library/file/IFileTransferAuthority;->onFileSent(Lcom/navdy/service/library/events/file/FileType;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v4

    .line 107
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Exception while reading the data and sending it across"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 88
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_4
    :try_start_1
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error sending the chunk because data was null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :cond_5
    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$1;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    # invokes: Lcom/navdy/hud/app/service/FileTransferHandler;->sendNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    invoke-static {v5, v2}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$000(Lcom/navdy/hud/app/service/FileTransferHandler;I)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_6

    .line 98
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Send the first chunk"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 104
    :goto_2
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Flow control enabled, waiting for the FileTransferStatus"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_6
    sget-object v5, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error sending the first chunk because data was null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
