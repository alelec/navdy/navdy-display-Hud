.class Lcom/navdy/hud/app/service/HudConnectionService$4;
.super Ljava/lang/Object;
.source "HudConnectionService.java"

# interfaces
.implements Lcom/navdy/hud/app/service/DeviceSearch$EventSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/HudConnectionService;->enterState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/HudConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/squareup/wire/Message;)V
    .locals 3
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 263
    instance-of v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 264
    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    .line 265
    .local v0, "connectionStatus":Lcom/navdy/service/library/events/connection/ConnectionStatus;
    iget-object v1, v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;
    invoke-static {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->access$900(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/service/library/device/connection/ConnectionService$State;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v1, v2, :cond_1

    .line 266
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # getter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->access$200(Lcom/navdy/hud/app/service/HudConnectionService;)Lcom/navdy/hud/app/service/DeviceSearch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/DeviceSearch;->close()V

    .line 268
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/service/HudConnectionService;->search:Lcom/navdy/hud/app/service/DeviceSearch;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->access$202(Lcom/navdy/hud/app/service/HudConnectionService;Lcom/navdy/hud/app/service/DeviceSearch;)Lcom/navdy/hud/app/service/DeviceSearch;

    .line 270
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 273
    .end local v0    # "connectionStatus":Lcom/navdy/service/library/events/connection/ConnectionStatus;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$4;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    # invokes: Lcom/navdy/hud/app/service/HudConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V
    invoke-static {v1, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->access$1000(Lcom/navdy/hud/app/service/HudConnectionService;Lcom/squareup/wire/Message;)V

    .line 274
    return-void
.end method
