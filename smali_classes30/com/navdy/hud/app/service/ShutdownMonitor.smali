.class public Lcom/navdy/hud/app/service/ShutdownMonitor;
.super Ljava/lang/Object;
.source "ShutdownMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;,
        Lcom/navdy/hud/app/service/ShutdownMonitor$MessageReceiver;,
        Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;
    }
.end annotation


# static fields
.field private static final ACCELERATED_TIMEOUT:J

.field private static final ACTIVE_USE_TIMEOUT:J

.field private static final BOOT_MODE_TIMEOUT:J

.field private static final MOVEMENT_THRESHOLD:F = 60.0f

.field private static final OBD_ACTIVE_TIMEOUT:J

.field private static final OBD_DISCONNECT_DELAY:J

.field private static final OBD_DISCONNECT_TIMEOUT:J

.field private static final PENDING_SHUTDOWN_TIMEOUT:J

.field private static final POWER_DISCONECT_SMOOTHING_TIMEOUT:J

.field private static REPORT_INTERVAL:J = 0x0L

.field private static final SAMPLING_INTERVAL:J

.field private static final SHUTDOWND_REQUEST_CLICK:Ljava/lang/String; = "powerclick"

.field private static final SHUTDOWND_REQUEST_DOUBLE_CLICK:Ljava/lang/String; = "powerdoubleclick"

.field private static final SHUTDOWND_REQUEST_LONG_PRESS:Ljava/lang/String; = "powerlongpress"

.field private static final SHUTDOWND_REQUEST_POWER_STATE:Ljava/lang/String; = "powerstate"

.field private static SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String; = null

.field private static final SOCKET_NAME:Ljava/lang/String; = "shutdownd"

.field private static TEMPORARY_SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String;

.field private static sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private lastWakeReason:Ljava/lang/String;

.field private mAccelerateTime:J

.field private mDialLastActivityTimeMsecs:J

.field private mDialManager:Lcom/navdy/hud/app/device/dial/DialManager;

.field private mDriving:Z

.field private mHandler:Landroid/os/Handler;

.field private mInactivityShutdownDisabled:Z

.field mInputManager:Lcom/navdy/hud/app/manager/InputManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mIsDialConnected:Z

.field private mLastInputEventTimeMsecs:J

.field private mLastMovementReport:J

.field private mLastMovementTimeMsecs:J

.field private mLocationListener:Landroid/location/LocationListener;

.field private mMainPowerOn:Z

.field private mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

.field private mMovementBases:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mObdDisconnectTimeMsecs:J

.field private mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

.field private mRemoteDeviceConnectTimeMsecs:J

.field private mScreenDimmingDisabled:Z

.field private mSocket:Landroid/net/LocalSocket;

.field private mSocketInputStream:Ljava/io/InputStream;

.field private mSocketOutputStream:Ljava/io/OutputStream;

.field private mStateChangeTime:J

.field private mUsbPowerOn:Z

.field private maxVoltage:D

.field private final notifyReceiver:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

.field private powerLossRunnable:Ljava/lang/Runnable;

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1e

    const-wide/16 v6, 0x5

    const-wide/16 v4, 0x3

    .line 56
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/ShutdownMonitor;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;

    .line 63
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->SAMPLING_INTERVAL:J

    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->BOOT_MODE_TIMEOUT:J

    .line 66
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_ACTIVE_TIMEOUT:J

    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_TIMEOUT:J

    .line 68
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->ACCELERATED_TIMEOUT:J

    .line 69
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->PENDING_SHUTDOWN_TIMEOUT:J

    .line 70
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->ACTIVE_USE_TIMEOUT:J

    .line 71
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_DELAY:J

    .line 107
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->POWER_DISCONECT_SMOOTHING_TIMEOUT:J

    .line 112
    const-string v0, "persist.sys.noautoshutdown"

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String;

    .line 113
    const-string v0, "sys.powerctl.noautoshutdown"

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->TEMPORARY_SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String;

    .line 474
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->REPORT_INTERVAL:J

    return-void
.end method

.method private constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J

    .line 87
    iput-boolean v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mIsDialConnected:Z

    .line 88
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialLastActivityTimeMsecs:J

    .line 89
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastInputEventTimeMsecs:J

    .line 93
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J

    .line 94
    iput-boolean v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    .line 95
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->maxVoltage:D

    .line 106
    iput-object v8, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;

    .line 109
    iput-boolean v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mScreenDimmingDisabled:Z

    .line 110
    iput-boolean v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInactivityShutdownDisabled:Z

    .line 147
    sget-object v3, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 149
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mStateChangeTime:J

    .line 150
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    .line 471
    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMovementBases:Landroid/util/ArrayMap;

    .line 472
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementTimeMsecs:J

    .line 473
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J

    .line 476
    new-instance v3, Lcom/navdy/hud/app/service/ShutdownMonitor$2;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/service/ShutdownMonitor$2;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLocationListener:Landroid/location/LocationListener;

    .line 297
    new-instance v3, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    invoke-direct {v3, p0, v8}, Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor;Lcom/navdy/hud/app/service/ShutdownMonitor$1;)V

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->notifyReceiver:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    .line 298
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v3

    if-nez v3, :cond_0

    .line 299
    sget-object v3, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "not a Navdy device, ShutdownMonitor will not run"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    .line 303
    .local v0, "curTimeMsecs":J
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    .line 304
    .local v2, "looper":Landroid/os/Looper;
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;

    .line 305
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 306
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 308
    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastInputEventTimeMsecs:J

    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialLastActivityTimeMsecs:J

    .line 309
    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J

    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementTimeMsecs:J

    iput-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J

    .line 311
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/hud/app/service/ShutdownMonitor$1;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/service/ShutdownMonitor$1;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/io/InputStream;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getSocketInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->establishSocketConnection()V

    return-void
.end method

.method static synthetic access$1100()J
    .locals 2

    .prologue
    .line 55
    sget-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->SAMPLING_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterBootState()V

    return-void
.end method

.method static synthetic access$1502(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J

    return-wide p1
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    return-wide p1
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1800()J
    .locals 2

    .prologue
    .line 55
    sget-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_TIMEOUT:J

    return-wide v0
.end method

.method static synthetic access$1902(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->closeSocketConnection()V

    return-void
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->onWakeEvent()V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->onMovement()V

    return-void
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/util/ArrayMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMovementBases:Landroid/util/ArrayMap;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/service/ShutdownMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J

    return-wide v0
.end method

.method static synthetic access$2402(Lcom/navdy/hud/app/service/ShutdownMonitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementReport:J

    return-wide p1
.end method

.method static synthetic access$2500()J
    .locals 2

    .prologue
    .line 55
    sget-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->REPORT_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$2600(Lcom/navdy/hud/app/service/ShutdownMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->updateStats()V

    return-void
.end method

.method static synthetic access$2700(Lcom/navdy/hud/app/service/ShutdownMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->evaluateState()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMainPowerOn:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/service/ShutdownMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/service/ShutdownMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/service/ShutdownMonitor;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/service/ShutdownMonitor;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerLossRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$600()J
    .locals 2

    .prologue
    .line 55
    sget-wide v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->POWER_DISCONECT_SMOOTHING_TIMEOUT:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/service/ShutdownMonitor;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/service/ShutdownMonitor;)Lcom/navdy/hud/app/obd/ObdManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ShutdownMonitor;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    return-object v0
.end method

.method private canDoShutdown()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 324
    sget-object v3, Lcom/navdy/hud/app/service/ShutdownMonitor;->SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/navdy/hud/app/service/ShutdownMonitor;->TEMPORARY_SHUTDOWN_OVERRIDE_SETTING:Ljava/lang/String;

    .line 325
    invoke-static {v3, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 328
    .local v0, "shutdownDisabled":Z
    :goto_0
    if-nez v0, :cond_2

    :goto_1
    return v1

    .end local v0    # "shutdownDisabled":Z
    :cond_1
    move v0, v2

    .line 325
    goto :goto_0

    .restart local v0    # "shutdownDisabled":Z
    :cond_2
    move v1, v2

    .line 328
    goto :goto_1
.end method

.method private checkObdConnection()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 566
    iget-wide v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdDisconnectTimeMsecs:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private declared-synchronized closeSocketConnection()V
    .locals 1

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketOutputStream:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketInputStream:Ljava/io/InputStream;

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private enterActiveUseState()V
    .locals 1

    .prologue
    .line 592
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_ACTIVE_USE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 593
    return-void
.end method

.method private enterBootState()V
    .locals 1

    .prologue
    .line 584
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_BOOT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 585
    return-void
.end method

.method private enterObdActiveState()V
    .locals 1

    .prologue
    .line 588
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_OBD_ACTIVE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 589
    return-void
.end method

.method private enterPendingShutdownState()V
    .locals 2

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->canDoShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    const-string v0, "0"

    const-string v1, "/sys/dlpc/led_enable"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToSysfs(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 603
    :goto_0
    return-void

    .line 601
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "pending shutdown state disabled by property setting"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private enterQuiteModeState()V
    .locals 1

    .prologue
    .line 634
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_QUIET_MODE:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 635
    return-void
.end method

.method private enterShutdownPromptState(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
    .locals 2
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    .line 611
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/Shutdown;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 612
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V

    .line 613
    return-void
.end method

.method private enterShutdownPromptStateIfNotDisabled(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
    .locals 6
    .param p1, "reason"    # Lcom/navdy/hud/app/event/Shutdown$Reason;

    .prologue
    .line 616
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->canDoShutdown()Z

    move-result v0

    .line 617
    .local v0, "shutdownAllowed":Z
    iget-boolean v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInactivityShutdownDisabled:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 618
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterShutdownPromptState(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    .line 627
    :goto_0
    return-void

    .line 620
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "shutdown prompt state disabled, shutdownAllowed = %b, mInactivityShutdownDisabled = %b, mUsbPowerOn = %b"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 622
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInactivityShutdownDisabled:Z

    .line 623
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-boolean v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mUsbPowerOn:Z

    .line 624
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    .line 620
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized establishSocketConnection()V
    .locals 4

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/net/LocalSocket;

    invoke-direct {v2}, Landroid/net/LocalSocket;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    .line 159
    new-instance v0, Landroid/net/LocalSocketAddress;

    const-string v2, "shutdownd"

    sget-object v3, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v0, v2, v3}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    .local v0, "addr":Landroid/net/LocalSocketAddress;
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2, v0}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 162
    iget-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketOutputStream:Ljava/io/OutputStream;

    .line 163
    iget-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketInputStream:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    :goto_0
    monitor-exit p0

    return-void

    .line 164
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v2, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "exception while attempting to connect to shutdownd socket"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 166
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->closeSocketConnection()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 158
    .end local v0    # "addr":Landroid/net/LocalSocketAddress;
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private evaluateState()J
    .locals 20

    .prologue
    .line 659
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v2

    .line 660
    .local v2, "curTime":J
    sget-wide v6, Lcom/navdy/hud/app/service/ShutdownMonitor;->SAMPLING_INTERVAL:J

    .line 661
    .local v6, "nextTimeout":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v14}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v4

    .line 663
    .local v4, "curVoltage":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->maxVoltage:D

    cmpl-double v14, v4, v14

    if-lez v14, :cond_0

    .line 664
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->maxVoltage:D

    .line 666
    :cond_0
    sget-object v14, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v15}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    .line 737
    :cond_1
    :goto_0
    :pswitch_0
    return-wide v6

    .line 668
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v14}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 669
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterObdActiveState()V

    goto :goto_0

    .line 670
    :cond_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mStateChangeTime:J

    sub-long v14, v2, v14

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->BOOT_MODE_TIMEOUT:J

    cmp-long v14, v14, v16

    if-lez v14, :cond_1

    .line 671
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    goto :goto_0

    .line 675
    :pswitch_2
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->checkObdConnection()J

    move-result-wide v8

    .line 676
    .local v8, "obdDiscMsec":J
    sget-wide v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_TIMEOUT:J

    cmp-long v14, v8, v14

    if-ltz v14, :cond_6

    .line 677
    sget-object v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "OBD disconnected, driving = %b, battery voltage = %5.1f, max voltage = %5.1f"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    move/from16 v18, v0

    .line 679
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->maxVoltage:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    aput-object v18, v16, v17

    .line 677
    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 680
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    if-nez v14, :cond_3

    const-wide/high16 v14, -0x4010000000000000L    # -1.0

    cmpl-double v14, v4, v14

    if-eqz v14, :cond_3

    const-wide v14, 0x4029ccccc0000000L    # 12.899999618530273

    cmpg-double v14, v4, v14

    if-gtz v14, :cond_3

    .line 683
    sget-object v14, Lcom/navdy/hud/app/event/Shutdown$Reason;->ENGINE_OFF:Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterShutdownPromptStateIfNotDisabled(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    goto :goto_0

    .line 684
    :cond_3
    sget-wide v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_DELAY:J

    cmp-long v14, v8, v14

    if-ltz v14, :cond_5

    .line 685
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    if-eqz v14, :cond_4

    .line 687
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    goto/16 :goto_0

    .line 689
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterPendingShutdownState()V

    goto/16 :goto_0

    .line 693
    :cond_5
    sget-object v14, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v16, 0x5

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v14

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_DISCONNECT_DELAY:J

    sub-long v16, v16, v8

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto/16 :goto_0

    .line 696
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeEventTime()J

    move-result-wide v14

    sub-long v14, v2, v14

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->OBD_ACTIVE_TIMEOUT:J

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    if-nez v14, :cond_1

    .line 697
    sget-object v14, Lcom/navdy/hud/app/event/Shutdown$Reason;->ENGINE_OFF:Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterShutdownPromptStateIfNotDisabled(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    goto/16 :goto_0

    .line 702
    .end local v8    # "obdDiscMsec":J
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v14}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 703
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterObdActiveState()V

    goto/16 :goto_0

    .line 704
    :cond_7
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_9

    .line 705
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeEventTime()J

    move-result-wide v12

    .line 706
    .local v12, "wt":J
    sub-long v14, v2, v12

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->ACCELERATED_TIMEOUT:J

    cmp-long v14, v14, v16

    if-ltz v14, :cond_8

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    if-nez v14, :cond_8

    .line 707
    sget-object v14, Lcom/navdy/hud/app/event/Shutdown$Reason;->ACCELERATE_SHUTDOWN:Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterShutdownPromptStateIfNotDisabled(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    goto/16 :goto_0

    .line 709
    :cond_8
    sget-wide v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->ACCELERATED_TIMEOUT:J

    add-long/2addr v14, v12

    sub-long v6, v14, v2

    goto/16 :goto_0

    .line 711
    .end local v12    # "wt":J
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mScreenDimmingDisabled:Z

    if-nez v14, :cond_1

    .line 712
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeEventTime()J

    move-result-wide v14

    sub-long v14, v2, v14

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->ACTIVE_USE_TIMEOUT:J

    cmp-long v14, v14, v16

    if-lez v14, :cond_1

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDriving:Z

    if-nez v14, :cond_1

    .line 713
    sget-object v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Active use timeout - last wake event was "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 714
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterPendingShutdownState()V

    goto/16 :goto_0

    .line 718
    :pswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeEventTime()J

    move-result-wide v10

    .line 719
    .local v10, "wakeEventTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v14}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 720
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterObdActiveState()V

    goto/16 :goto_0

    .line 721
    :cond_a
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mStateChangeTime:J

    cmp-long v14, v10, v14

    if-lez v14, :cond_b

    .line 722
    sget-object v14, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Wake reason:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 723
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    goto/16 :goto_0

    .line 724
    :cond_b
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_c

    sub-long v14, v2, v10

    sget-wide v16, Lcom/navdy/hud/app/service/ShutdownMonitor;->PENDING_SHUTDOWN_TIMEOUT:J

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1

    .line 726
    :cond_c
    sget-object v14, Lcom/navdy/hud/app/event/Shutdown$Reason;->INACTIVITY:Lcom/navdy/hud/app/event/Shutdown$Reason;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterShutdownPromptStateIfNotDisabled(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    goto/16 :goto_0

    .line 666
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getCurrentTimeMsecs()J
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;
    .locals 2

    .prologue
    .line 131
    const-class v1, Lcom/navdy/hud/app/service/ShutdownMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Lcom/navdy/hud/app/service/ShutdownMonitor;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;

    .line 133
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;

    invoke-direct {v0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->init()V

    .line 135
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sInstance:Lcom/navdy/hud/app/service/ShutdownMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized getSocketInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketInputStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->establishSocketConnection()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketInputStream:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getSocketOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketOutputStream:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 181
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->establishSocketConnection()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mSocketOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private init()V
    .locals 8

    .prologue
    .line 332
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 333
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 334
    .local v6, "looper":Landroid/os/Looper;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "location"

    .line 335
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 337
    .local v0, "locationManager":Landroid/location/LocationManager;
    :try_start_0
    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 338
    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->notifyReceiver:Lcom/navdy/hud/app/service/ShutdownMonitor$NotificationReceiver;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 343
    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterQuiteModeState()V

    .line 349
    :goto_1
    return-void

    .line 339
    :catch_0
    move-exception v7

    .line 340
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "failed to register with GPS or Network provider"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 346
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterBootState()V

    goto :goto_1
.end method

.method private lastWakeEventTime()J
    .locals 14

    .prologue
    .line 747
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->updateStats()V

    .line 748
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    .line 750
    .local v0, "currentTime":J
    iget-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mStateChangeTime:J

    .line 751
    .local v6, "lastWakeEventTime":J
    const-string v9, "stateChange"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 753
    iget-wide v10, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialLastActivityTimeMsecs:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_0

    .line 754
    iget-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialLastActivityTimeMsecs:J

    .line 755
    const-string v9, "dial"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 758
    :cond_0
    iget-wide v10, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastInputEventTimeMsecs:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_1

    .line 759
    iget-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastInputEventTimeMsecs:J

    .line 760
    const-string v9, "input"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 763
    :cond_1
    iget-wide v10, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_2

    .line 764
    iget-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mRemoteDeviceConnectTimeMsecs:J

    .line 765
    const-string v9, "phone"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 768
    :cond_2
    iget-wide v10, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementTimeMsecs:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_3

    .line 769
    iget-wide v6, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementTimeMsecs:J

    .line 770
    const-string v9, "movement"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 773
    :cond_3
    iget-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mObdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/obd/ObdManager;->getDriveRecorder()Lcom/navdy/hud/app/debug/DriveRecorder;

    move-result-object v8

    .line 774
    .local v8, "odr":Lcom/navdy/hud/app/debug/DriveRecorder;
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/navdy/hud/app/debug/DriveRecorder;->isDemoPlaying()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 775
    move-wide v6, v0

    .line 776
    const-string v9, "recording"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 782
    :cond_4
    const-string v9, "navdy.ota.downloading"

    const-string v10, "0"

    invoke-static {v9, v10}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 784
    .local v2, "downloading":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    int-to-long v10, v9

    const-wide/16 v12, 0x3e8

    mul-long v4, v10, v12

    .line 789
    .local v4, "lastDownloadTime":J
    :goto_0
    cmp-long v9, v4, v6

    if-lez v9, :cond_5

    .line 790
    move-wide v6, v4

    .line 791
    const-string v9, "ota"

    iput-object v9, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->lastWakeReason:Ljava/lang/String;

    .line 794
    :cond_5
    return-wide v6

    .line 785
    .end local v4    # "lastDownloadTime":J
    :catch_0
    move-exception v3

    .line 786
    .local v3, "e":Ljava/lang/NumberFormatException;
    sget-object v9, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "%s property has invalid value: %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "navdy.ota.downloading"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v2, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 787
    const-wide/16 v4, 0x0

    .restart local v4    # "lastDownloadTime":J
    goto :goto_0
.end method

.method private leavePendingShutdownState()V
    .locals 2

    .prologue
    .line 607
    const-string v0, "1"

    const-string v1, "/sys/dlpc/led_enable"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToSysfs(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    return-void
.end method

.method private leaveShutdownPromptState()V
    .locals 2

    .prologue
    .line 630
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    .line 631
    return-void
.end method

.method private onMovement()V
    .locals 4

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    .line 522
    .local v0, "curTime":J
    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastMovementTimeMsecs:J

    .line 523
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mAccelerateTime:J

    .line 524
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->onWakeEvent()V

    .line 525
    return-void
.end method

.method private onWakeEvent()V
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_SHUTDOWN_PROMPT:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    if-ne v0, v1, :cond_1

    .line 533
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    .line 535
    :cond_1
    return-void
.end method

.method private setMonitorState(Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;)V
    .locals 4
    .param p1, "state"    # Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .prologue
    .line 570
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor$4;->$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState:[I

    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 578
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setting monitor state to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 579
    iput-object p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    .line 580
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mStateChangeTime:J

    .line 581
    return-void

    .line 572
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->leavePendingShutdownState()V

    goto :goto_0

    .line 575
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->leaveShutdownPromptState()V

    goto :goto_0

    .line 570
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateStats()V
    .locals 4

    .prologue
    .line 551
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v2

    .line 554
    .local v2, "curTimeMsecs":J
    iget-object v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v0

    .line 555
    .local v0, "connected":Z
    if-eqz v0, :cond_0

    .line 556
    iget-boolean v1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mIsDialConnected:Z

    if-nez v1, :cond_0

    .line 557
    iput-wide v2, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mDialLastActivityTimeMsecs:J

    .line 560
    :cond_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mIsDialConnected:Z

    .line 562
    return-void
.end method


# virtual methods
.method public disableInactivityShutdown(Z)V
    .locals 0
    .param p1, "disable"    # Z

    .prologue
    .line 649
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mInactivityShutdownDisabled:Z

    .line 650
    return-void
.end method

.method public disableScreenDim(Z)V
    .locals 5
    .param p1, "disable"    # Z

    .prologue
    .line 653
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mScreenDimmingDisabled:Z

    .line 654
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->recordInputEvent()V

    .line 655
    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "screen dimming %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    const-string v0, "disabled"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 656
    return-void

    .line 655
    :cond_0
    const-string v0, "enabled"

    goto :goto_0
.end method

.method public recordInputEvent()V
    .locals 2

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getCurrentTimeMsecs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mLastInputEventTimeMsecs:J

    .line 542
    iget-object v0, p0, Lcom/navdy/hud/app/service/ShutdownMonitor;->mMonitorState:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    sget-object v1, Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;->STATE_PENDING_SHUTDOWN:Lcom/navdy/hud/app/service/ShutdownMonitor$MonitorState;

    if-ne v0, v1, :cond_0

    .line 543
    invoke-direct {p0}, Lcom/navdy/hud/app/service/ShutdownMonitor;->enterActiveUseState()V

    .line 545
    :cond_0
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 639
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/ShutdownMonitor$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/service/ShutdownMonitor$3;-><init>(Lcom/navdy/hud/app/service/ShutdownMonitor;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 646
    return-void
.end method
