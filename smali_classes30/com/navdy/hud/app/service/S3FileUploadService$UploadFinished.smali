.class public Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;
.super Ljava/lang/Object;
.source "S3FileUploadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/S3FileUploadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadFinished"
.end annotation


# instance fields
.field public final filePath:Ljava/lang/String;

.field public final succeeded:Z

.field public final userTag:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "succeeded"    # Z
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "userTag"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean p1, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->succeeded:Z

    .line 42
    iput-object p2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->filePath:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/navdy/hud/app/service/S3FileUploadService$UploadFinished;->userTag:Ljava/lang/String;

    .line 44
    return-void
.end method
