.class Lcom/navdy/hud/app/service/S3FileUploadService$1;
.super Ljava/lang/Object;
.source "S3FileUploadService.java"

# interfaces
.implements Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/S3FileUploadService;->onHandleIntent(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$observer:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/S3FileUploadService;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/io/File;Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/S3FileUploadService;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iput-object p2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$file:Ljava/io/File;

    iput-object p4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$observer:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 255
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v2, v2, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onError , while uploading, id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 256
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/S3FileUploadService;->getCurrentRequest()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    move-result-object v0

    .line 257
    .local v0, "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/S3FileUploadService;->getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    move-result-object v1

    .line 258
    .local v1, "gestureUploadQueue":Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    monitor-enter v1

    .line 259
    if-eqz v0, :cond_0

    .line 260
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 261
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/S3FileUploadService;->setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 263
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 265
    iget-object v2, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/S3FileUploadService;->reSchedule()V

    .line 266
    return-void

    .line 263
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onProgressChanged(IJJ)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "bytesCurrent"    # J
    .param p4, "bytesTotal"    # J

    .prologue
    .line 251
    return-void
.end method

.method public onStateChanged(ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V
    .locals 10
    .param p1, "id"    # I
    .param p2, "state"    # Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 199
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "State changed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 200
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->getCurrentRequest()Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    move-result-object v0

    .line 201
    .local v0, "currentRequest":Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    const/4 v2, 0x1

    .line 202
    .local v2, "shouldPutBack":Z
    const/4 v3, 0x1

    .line 203
    .local v3, "shouldReschedule":Z
    sget-object v4, Lcom/navdy/hud/app/service/S3FileUploadService$2;->$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState:[I

    invoke-virtual {p2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 231
    :goto_0
    if-eqz v2, :cond_0

    .line 232
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->getUploadQueue()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;

    move-result-object v1

    .line 233
    .local v1, "gestureUploadQueue":Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    monitor-enter v1

    .line 234
    if-eqz v0, :cond_2

    .line 235
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;->add(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 236
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/service/S3FileUploadService;->setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 240
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v9, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 242
    if-eqz v3, :cond_0

    .line 243
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->reSchedule()V

    .line 246
    .end local v1    # "gestureUploadQueue":Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    :cond_0
    return-void

    .line 205
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Upload completed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 206
    const/4 v2, 0x0

    .line 207
    if-eqz v0, :cond_1

    .line 208
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 209
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 210
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v5, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->userTag:Ljava/lang/String;

    invoke-virtual {v4, v9, v5, v6}, Lcom/navdy/hud/app/service/S3FileUploadService;->uploadFinished(ZLjava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v4, v7}, Lcom/navdy/hud/app/service/S3FileUploadService;->setCurrentRequest(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V

    .line 213
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->sync()V

    goto :goto_0

    .line 216
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Upload canceled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Waiting for network"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 220
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    # getter for: Lcom/navdy/hud/app/service/S3FileUploadService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;
    invoke-static {v4}, Lcom/navdy/hud/app/service/S3FileUploadService;->access$000(Lcom/navdy/hud/app/service/S3FileUploadService;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->cancel(I)Z

    .line 221
    const/4 v3, 0x0

    .line 222
    goto :goto_0

    .line 224
    :pswitch_3
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "In progress "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->val$observer:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v6}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getBytesTotal()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 225
    const/4 v2, 0x0

    .line 226
    goto/16 :goto_0

    .line 228
    :pswitch_4
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Failed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    .restart local v1    # "gestureUploadQueue":Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/service/S3FileUploadService$1;->this$0:Lcom/navdy/hud/app/service/S3FileUploadService;

    iget-object v4, v4, Lcom/navdy/hud/app/service/S3FileUploadService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "request object in upload callback is null, network anomaly?"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 240
    :catchall_0
    move-exception v4

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
