.class public Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;
.super Ljava/lang/Object;
.source "HudConnectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/HudConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ReconnectRunnable"
.end annotation


# instance fields
.field private reason:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/hud/app/service/HudConnectionService;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 478
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;->this$0:Lcom/navdy/hud/app/service/HudConnectionService;

    iget-object v1, p0, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->reconnect(Ljava/lang/String;)V

    .line 485
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/navdy/hud/app/service/HudConnectionService$ReconnectRunnable;->reason:Ljava/lang/String;

    .line 489
    return-void
.end method
