.class public Lcom/navdy/hud/app/service/ConnectionServiceProxy;
.super Ljava/lang/Object;
.source "ConnectionServiceProxy.java"


# static fields
.field private static final VERBOSE:Z = true

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected bus:Lcom/squareup/otto/Bus;

.field private driveRecordingEvent:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

.field private eventListener:Lcom/navdy/hud/app/IEventListener$Stub;

.field protected mContext:Landroid/content/Context;

.field protected mEventSource:Lcom/navdy/hud/app/IEventSource;

.field protected mWire:Lcom/squareup/wire/Wire;

.field private serviceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy$2;-><init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->serviceConnection:Landroid/content/ServiceConnection;

    .line 200
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy$3;-><init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->eventListener:Lcom/navdy/hud/app/IEventListener$Stub;

    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    .line 56
    invoke-virtual {p2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/squareup/wire/Wire;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mWire:Lcom/squareup/wire/Wire;

    .line 59
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating connection service proxy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/service/ConnectionServiceProxy;)Lcom/navdy/hud/app/IEventListener$Stub;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->eventListener:Lcom/navdy/hud/app/IEventListener$Stub;

    return-object v0
.end method


# virtual methods
.method public connect()V
    .locals 4

    .prologue
    .line 63
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    if-nez v1, :cond_0

    .line 64
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Starting connection service"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 65
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    const-class v2, Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 67
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Binding to connection service"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 68
    const-class v1, Lcom/navdy/hud/app/IEventSource;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->serviceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 71
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public connected()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 4

    .prologue
    .line 74
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    if-eqz v2, :cond_0

    .line 76
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->eventListener:Lcom/navdy/hud/app/IEventListener$Stub;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/IEventSource;->removeEventListener(Lcom/navdy/hud/app/IEventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 81
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    const-class v3, Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 83
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mEventSource:Lcom/navdy/hud/app/IEventSource;

    .line 85
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to remove event listener"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public getDriverRecordingEvent()Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->driveRecordingEvent:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    return-object v0
.end method

.method public onEvent(Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v0

    .line 100
    .local v0, "message":Lcom/squareup/wire/Message;
    if-eqz v0, :cond_1

    .line 101
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 106
    sget-object v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy$4;->$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 116
    .end local v0    # "message":Lcom/squareup/wire/Message;
    :cond_1
    :goto_0
    return-void

    .line 108
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    :pswitch_0
    check-cast v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    iput-object v0, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->driveRecordingEvent:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    goto :goto_0

    .line 112
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    :pswitch_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->driveRecordingEvent:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public postEvent([B)V
    .locals 6
    .param p1, "bytes"    # [B

    .prologue
    .line 121
    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    if-eqz v3, :cond_0

    .line 123
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->mWire:Lcom/squareup/wire/Wire;

    const-class v4, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v3, p1, v4}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 125
    .local v1, "event":Lcom/navdy/service/library/events/NavdyEvent;
    iget-object v3, p0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v3, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v1    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    :cond_0
    :goto_0
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Throwable;
    const/4 v2, -0x1

    .line 129
    .local v2, "eventTypeIndex":I
    :try_start_1
    invoke-static {p1}, Lcom/navdy/service/library/events/WireUtil;->getEventTypeIndex([B)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 133
    :goto_1
    sget-object v3, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring invalid navdy event["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 130
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public postRemoteEvent(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 3
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 140
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/service/ConnectionServiceProxy$1;-><init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 171
    return-void
.end method
