.class Lcom/navdy/hud/app/service/FileTransferHandler$4;
.super Ljava/lang/Object;
.source "FileTransferHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/FileTransferHandler;->onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

.field final synthetic val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/FileTransferHandler;Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/FileTransferHandler;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 196
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    if-eqz v3, :cond_0

    .line 197
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v3, v3, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v3, v3, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 198
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File Transfer complete "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v5, v5, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 200
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v4, v4, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/file/FileTransferSessionManager;->getFileType(I)Lcom/navdy/service/library/events/file/FileType;

    move-result-object v1

    .line 201
    .local v1, "fileType":Lcom/navdy/service/library/events/file/FileType;
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v4, v4, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4, v6}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(IZ)V

    .line 202
    if-eqz v1, :cond_1

    .line 203
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    invoke-interface {v3, v1}, Lcom/navdy/service/library/file/IFileTransferAuthority;->onFileSent(Lcom/navdy/service/library/events/file/FileType;)V

    .line 231
    .end local v1    # "fileType":Lcom/navdy/service/library/events/file/FileType;
    :cond_0
    :goto_0
    return-void

    .line 205
    .restart local v1    # "fileType":Lcom/navdy/service/library/events/file/FileType;
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot find FileType associated with transfer ID :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v5, v5, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    .end local v1    # "fileType":Lcom/navdy/service/library/events/file/FileType;
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkBandwidthLevel()I

    move-result v3

    if-gtz v3, :cond_3

    .line 212
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Link bandwidth is low, Not sending the next chunk. Ending the file transfer"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 213
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v4, v4, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(IZ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v2

    .line 227
    .local v2, "throwable":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while reading the data and sending it across"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 216
    .end local v2    # "throwable":Ljava/lang/Throwable;
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/file/FileTransferSessionManager;->handleFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 217
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v4, v4, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    # invokes: Lcom/navdy/hud/app/service/FileTransferHandler;->sendNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/service/FileTransferHandler;->access$000(Lcom/navdy/hud/app/service/FileTransferHandler;I)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    .line 218
    .local v0, "data":Lcom/navdy/service/library/events/file/FileTransferData;
    if-eqz v0, :cond_4

    .line 219
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Sent chunk"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/service/FileTransferHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Failed to get the data for the transfer session"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    .end local v0    # "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :cond_5
    iget-object v3, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->this$0:Lcom/navdy/hud/app/service/FileTransferHandler;

    iget-object v3, v3, Lcom/navdy/hud/app/service/FileTransferHandler;->fileTransferManager:Lcom/navdy/service/library/file/FileTransferSessionManager;

    iget-object v4, p0, Lcom/navdy/hud/app/service/FileTransferHandler$4;->val$fileTransferStatus:Lcom/navdy/service/library/events/file/FileTransferStatus;

    iget-object v4, v4, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(IZ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
