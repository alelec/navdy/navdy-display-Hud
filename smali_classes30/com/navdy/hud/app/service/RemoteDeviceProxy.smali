.class public Lcom/navdy/hud/app/service/RemoteDeviceProxy;
.super Lcom/navdy/service/library/device/RemoteDevice;
.source "RemoteDeviceProxy.java"


# instance fields
.field private volatile bandwidthLevel:I

.field private connected:Z

.field protected proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 2
    .param p1, "proxy"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0, p2, p3, v1}, Lcom/navdy/service/library/device/RemoteDevice;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;Z)V

    .line 23
    iput v1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->bandwidthLevel:I

    .line 28
    iput-object p1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 32
    iput v1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->bandwidthLevel:I

    .line 34
    iput-boolean v1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->connected:Z

    .line 35
    return-void
.end method

.method private notSupported()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t call this method on remoteDeviceProxy"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public connect()Z
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->notSupported()V

    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method public disconnect()Z
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->notSupported()V

    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method protected dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 55
    new-instance v0, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;-><init>(Lcom/navdy/hud/app/service/RemoteDeviceProxy;Lcom/navdy/service/library/events/NavdyEvent;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 61
    return-void
.end method

.method public getLinkBandwidthLevel()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->bandwidthLevel:I

    return v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->connected:Z

    return v0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/app/service/RemoteDeviceProxy$2;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    :goto_0
    return-void

    .line 41
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->connected:Z

    .line 42
    invoke-virtual {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->dispatchConnectEvent()V

    goto :goto_0

    .line 45
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->connected:Z

    .line 46
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 67
    return-void
.end method

.method public postEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->postRemoteEvent(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public postEvent(Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;
    .param p2, "postEventHandler"    # Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

    .prologue
    .line 77
    if-eqz p2, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->notSupported()V

    .line 79
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->proxy:Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    invoke-virtual {p0}, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;->postRemoteEvent(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLinkBandwidthLevel(I)V
    .locals 0
    .param p1, "bandwidthLevel"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy;->bandwidthLevel:I

    .line 114
    return-void
.end method
