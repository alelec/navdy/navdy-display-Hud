.class public Lcom/navdy/hud/app/service/S3FileUploadService$RequestTimeComparator;
.super Ljava/lang/Object;
.source "S3FileUploadService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/service/S3FileUploadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RequestTimeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/navdy/hud/app/service/S3FileUploadService$Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/navdy/hud/app/service/S3FileUploadService$Request;Lcom/navdy/hud/app/service/S3FileUploadService$Request;)I
    .locals 4
    .param p1, "a"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;
    .param p2, "b"    # Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    .prologue
    .line 64
    iget-object v0, p2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iget-object v2, p1, Lcom/navdy/hud/app/service/S3FileUploadService$Request;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 59
    check-cast p1, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    check-cast p2, Lcom/navdy/hud/app/service/S3FileUploadService$Request;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/service/S3FileUploadService$RequestTimeComparator;->compare(Lcom/navdy/hud/app/service/S3FileUploadService$Request;Lcom/navdy/hud/app/service/S3FileUploadService$Request;)I

    move-result v0

    return v0
.end method
