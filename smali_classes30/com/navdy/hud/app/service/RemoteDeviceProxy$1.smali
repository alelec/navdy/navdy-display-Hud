.class Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;
.super Ljava/lang/Object;
.source "RemoteDeviceProxy.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDevice$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/service/RemoteDeviceProxy;->dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

.field final synthetic val$event:Lcom/navdy/service/library/events/NavdyEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/service/RemoteDeviceProxy;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;->this$0:Lcom/navdy/hud/app/service/RemoteDeviceProxy;

    iput-object p2, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/RemoteDevice$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "listener"    # Lcom/navdy/service/library/device/RemoteDevice$Listener;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;->val$event:Lcom/navdy/service/library/events/NavdyEvent;

    invoke-interface {p2, p1, v0}, Lcom/navdy/service/library/device/RemoteDevice$Listener;->onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 59
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 55
    check-cast p1, Lcom/navdy/service/library/device/RemoteDevice;

    check-cast p2, Lcom/navdy/service/library/device/RemoteDevice$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/service/RemoteDeviceProxy$1;->dispatchEvent(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/RemoteDevice$Listener;)V

    return-void
.end method
