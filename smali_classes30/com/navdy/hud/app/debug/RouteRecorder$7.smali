.class Lcom/navdy/hud/app/debug/RouteRecorder$7;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;->startPlayback(Ljava/lang/String;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$local:Z

.field final synthetic val$secondary:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$fileName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$secondary:Z

    iput-boolean p4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$local:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 342
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$fileName:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$secondary:Z

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->bPrepare(Ljava/lang/String;Z)Z

    move-result v0

    .line 343
    .local v0, "prepared":Z
    if-nez v0, :cond_1

    .line 344
    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$local:Z

    if-nez v2, :cond_0

    .line 345
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v2

    new-instance v3, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 347
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    .line 364
    .end local v0    # "prepared":Z
    :goto_0
    return-void

    .line 350
    .restart local v0    # "prepared":Z
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Prepare succeeded"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 351
    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$local:Z

    if-nez v2, :cond_2

    .line 352
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v2

    new-instance v3, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 354
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Starting playback"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 355
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$302(Lcom/navdy/hud/app/debug/RouteRecorder;I)I

    .line 356
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$700(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    .end local v0    # "prepared":Z
    :catch_0
    move-exception v1

    .line 358
    .local v1, "t":Ljava/lang/Throwable;
    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->val$local:Z

    if-nez v2, :cond_3

    .line 359
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v2

    new-instance v3, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 361
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$7;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    .line 362
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
