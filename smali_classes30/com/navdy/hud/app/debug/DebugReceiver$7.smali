.class Lcom/navdy/hud/app/debug/DebugReceiver$7;
.super Ljava/lang/Object;
.source "DebugReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DebugReceiver;->dnsLookup(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

.field final synthetic val$host:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DebugReceiver;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DebugReceiver;

    .prologue
    .line 1225
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$7;->this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/DebugReceiver$7;->val$host:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1229
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$7;->val$host:Ljava/lang/String;

    .line 1230
    .local v1, "hostName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1231
    const-string v1, "analytics.localytics.com"

    .line 1233
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dnslookup :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1234
    invoke-static {v1}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    .line 1235
    .local v0, "addresses":[Ljava/net/InetAddress;
    if-eqz v0, :cond_1

    .line 1236
    sget-object v4, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dnslookup total:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1237
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_2

    .line 1238
    sget-object v4, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dnslookup address :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v2

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1237
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1241
    .end local v2    # "i":I
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "dnslookup no address"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1246
    .end local v0    # "addresses":[Ljava/net/InetAddress;
    .end local v1    # "hostName":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 1243
    :catch_0
    move-exception v3

    .line 1244
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
