.class Lcom/navdy/hud/app/debug/DebugReceiver$3;
.super Ljava/lang/Object;
.source "DebugReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DebugReceiver;->testProxyFileUpload(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

.field final synthetic val$size:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DebugReceiver;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DebugReceiver;

    .prologue
    .line 1050
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$3;->this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

    iput p2, p0, Lcom/navdy/hud/app/debug/DebugReceiver$3;->val$size:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 26

    .prologue
    .line 1054
    const/4 v7, 0x0

    .line 1055
    .local v7, "in":Ljava/io/InputStream;
    const-wide/16 v14, 0x0

    .line 1056
    .local v14, "startTime":J
    const-wide/16 v18, 0x0

    .line 1057
    .local v18, "totalBytesRead":J
    const-wide/16 v16, 0x0

    .line 1059
    .local v16, "timeElapsed":D
    :try_start_0
    new-instance v13, Ljava/net/URL;

    const-string v21, "http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file"

    move-object/from16 v0, v21

    invoke-direct {v13, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1060
    .local v13, "url":Ljava/net/URL;
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v20

    check-cast v20, Ljava/net/HttpURLConnection;

    .line 1061
    .local v20, "urlConnection":Ljava/net/HttpURLConnection;
    const/16 v21, 0x7530

    invoke-virtual/range {v20 .. v21}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 1062
    const/16 v21, 0x7530

    invoke-virtual/range {v20 .. v21}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 1063
    const-string v21, "POST"

    invoke-virtual/range {v20 .. v21}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1065
    invoke-virtual/range {v20 .. v20}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    .line 1067
    .local v8, "os":Ljava/io/OutputStream;
    const/4 v6, 0x0

    .line 1068
    .local v6, "finalSize":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/debug/DebugReceiver$3;->val$size:I

    move/from16 v21, v0

    if-ltz v21, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/debug/DebugReceiver$3;->val$size:I

    move/from16 v21, v0

    const/16 v22, 0x7d0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 1069
    :cond_0
    const v6, 0x186a0

    .line 1073
    :goto_0
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Uploading "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v23

    int-to-long v0, v6

    move-wide/from16 v24, v0

    invoke-static/range {v23 .. v25}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1074
    new-array v4, v6, [B

    .line 1075
    .local v4, "data":[B
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    .line 1076
    .local v9, "rand":Ljava/util/Random;
    invoke-virtual {v9, v4}, Ljava/util/Random;->nextBytes([B)V

    .line 1077
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 1078
    invoke-virtual/range {v20 .. v20}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v10

    .line 1079
    .local v10, "responseCode":I
    const/16 v21, 0xc8

    move/from16 v0, v21

    if-ne v10, v0, :cond_1

    .line 1080
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "POST succeeded"

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1089
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1090
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x447a0000    # 1000.0f

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 1091
    .local v12, "speedBps":F
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Upload finished, Size : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", Time Taken :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " milliseconds , RawSpeed :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " kBps"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1092
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1094
    .end local v4    # "data":[B
    .end local v6    # "finalSize":I
    .end local v8    # "os":Ljava/io/OutputStream;
    .end local v9    # "rand":Ljava/util/Random;
    .end local v10    # "responseCode":I
    .end local v13    # "url":Ljava/net/URL;
    .end local v20    # "urlConnection":Ljava/net/HttpURLConnection;
    :goto_1
    return-void

    .line 1071
    .end local v12    # "speedBps":F
    .restart local v6    # "finalSize":I
    .restart local v8    # "os":Ljava/io/OutputStream;
    .restart local v13    # "url":Ljava/net/URL;
    .restart local v20    # "urlConnection":Ljava/net/HttpURLConnection;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/debug/DebugReceiver$3;->val$size:I

    move/from16 v21, v0
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move/from16 v0, v21

    mul-int/lit16 v6, v0, 0x3e8

    goto/16 :goto_0

    .line 1082
    .end local v6    # "finalSize":I
    .end local v8    # "os":Ljava/io/OutputStream;
    .end local v13    # "url":Ljava/net/URL;
    .end local v20    # "urlConnection":Ljava/net/HttpURLConnection;
    :catch_0
    move-exception v11

    .line 1083
    .local v11, "se":Ljava/net/SocketTimeoutException;
    :try_start_2
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Socket timed out Exception "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1089
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1090
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x447a0000    # 1000.0f

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 1091
    .restart local v12    # "speedBps":F
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Upload finished, Size : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", Time Taken :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " milliseconds , RawSpeed :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " kBps"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1092
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 1084
    .end local v11    # "se":Ljava/net/SocketTimeoutException;
    .end local v12    # "speedBps":F
    :catch_1
    move-exception v5

    .line 1085
    .local v5, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "IOException "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1089
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1090
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x447a0000    # 1000.0f

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 1091
    .restart local v12    # "speedBps":F
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Upload finished, Size : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", Time Taken :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " milliseconds , RawSpeed :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " kBps"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1092
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 1086
    .end local v5    # "e":Ljava/io/IOException;
    .end local v12    # "speedBps":F
    :catch_2
    move-exception v5

    .line 1087
    .local v5, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Exception "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1089
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1090
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x447a0000    # 1000.0f

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 1091
    .restart local v12    # "speedBps":F
    sget-object v21, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Upload finished, Size : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", Time Taken :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " milliseconds , RawSpeed :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " kBps"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1092
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 1089
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v12    # "speedBps":F
    :catchall_0
    move-exception v21

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1090
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v12, v0

    .line 1091
    .restart local v12    # "speedBps":F
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Upload finished, Size : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Time Taken :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " milliseconds , RawSpeed :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " kBps"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1092
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1093
    throw v21
.end method
