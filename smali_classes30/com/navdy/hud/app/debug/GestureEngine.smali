.class public Lcom/navdy/hud/app/debug/GestureEngine;
.super Landroid/widget/LinearLayout;
.source "GestureEngine.java"


# instance fields
.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/debug/GestureEngine;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/GestureEngine;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 42
    const v1, 0x7f0e00d8

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/debug/GestureEngine;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 43
    .local v0, "gesture":Landroid/widget/CheckBox;
    iget-object v1, p0, Lcom/navdy/hud/app/debug/GestureEngine;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->isRunning()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 44
    return-void
.end method

.method public onToggleDiscreteMode(Z)V
    .locals 1
    .param p1, "checked"    # Z
    .annotation build Lbutterknife/OnCheckedChanged;
        value = {
            0x7f0e00da
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/debug/GestureEngine;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->setDiscreteMode(Z)V

    .line 63
    return-void
.end method

.method onToggleGesture(Z)V
    .locals 3
    .param p1, "checked"    # Z
    .annotation build Lbutterknife/OnCheckedChanged;
        value = {
            0x7f0e00d8
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/GestureEngine$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/debug/GestureEngine$1;-><init>(Lcom/navdy/hud/app/debug/GestureEngine;Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 58
    return-void
.end method

.method public onTogglePreview(Z)V
    .locals 1
    .param p1, "checked"    # Z
    .annotation build Lbutterknife/OnCheckedChanged;
        value = {
            0x7f0e00d9
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/debug/GestureEngine;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->enablePreview(Z)V

    .line 68
    return-void
.end method
