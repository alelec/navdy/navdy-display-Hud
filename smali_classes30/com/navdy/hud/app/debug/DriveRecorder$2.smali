.class Lcom/navdy/hud/app/debug/DriveRecorder$2;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->prepare(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$2;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$2;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$2;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->bPrepare(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Prepare succeeded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Prepare failed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
