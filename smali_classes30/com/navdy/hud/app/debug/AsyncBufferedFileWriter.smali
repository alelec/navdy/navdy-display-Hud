.class public final Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;
.super Ljava/lang/Object;
.source "AsyncBufferedFileWriter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eJ\u001a\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u0013H\u0007R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;",
        "",
        "filePath",
        "",
        "executor",
        "Lcom/navdy/hud/app/debug/SerialExecutor;",
        "bufferSize",
        "",
        "(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V",
        "bufferedWriter",
        "Ljava/io/BufferedWriter;",
        "getBufferedWriter",
        "()Ljava/io/BufferedWriter;",
        "flush",
        "",
        "flushAndClose",
        "write",
        "data",
        "forceToDisk",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final bufferedWriter:Ljava/io/BufferedWriter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final executor:Lcom/navdy/hud/app/debug/SerialExecutor;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "executor"    # Lcom/navdy/hud/app/debug/SerialExecutor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "bufferSize"    # I

    .prologue
    const-string v1, "filePath"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "executor"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->executor:Lcom/navdy/hud/app/debug/SerialExecutor;

    .line 14
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15
    .local v0, "file":Ljava/io/File;
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    check-cast v1, Ljava/io/OutputStream;

    const-string v4, "utf-8"

    invoke-direct {v2, v1, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    move-object v1, v2

    check-cast v1, Ljava/io/Writer;

    invoke-direct {v3, v1, p3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    iput-object v3, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->bufferedWriter:Ljava/io/BufferedWriter;

    return-void
.end method

.method public static bridge synthetic write$default(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 1
    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 21
    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final flush()V
    .locals 2

    .prologue
    .line 31
    iget-object v1, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->executor:Lcom/navdy/hud/app/debug/SerialExecutor;

    new-instance v0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$flush$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$flush$1;-><init>(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v1, v0}, Lcom/navdy/hud/app/debug/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    .line 34
    return-void
.end method

.method public final flushAndClose()V
    .locals 2

    .prologue
    .line 37
    iget-object v1, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->executor:Lcom/navdy/hud/app/debug/SerialExecutor;

    new-instance v0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$flushAndClose$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$flushAndClose$1;-><init>(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v1, v0}, Lcom/navdy/hud/app/debug/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    .line 41
    return-void
.end method

.method public final getBufferedWriter()Ljava/io/BufferedWriter;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->bufferedWriter:Ljava/io/BufferedWriter;

    return-object v0
.end method

.method public final write(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write$default(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;Ljava/lang/String;ZILjava/lang/Object;)V

    return-void
.end method

.method public final write(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "data"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "forceToDisk"    # Z
    .annotation build Lkotlin/jvm/JvmOverloads;
    .end annotation

    .prologue
    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v1, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->executor:Lcom/navdy/hud/app/debug/SerialExecutor;

    new-instance v0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;-><init>(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;Ljava/lang/String;Z)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v1, v0}, Lcom/navdy/hud/app/debug/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    .line 28
    return-void
.end method
