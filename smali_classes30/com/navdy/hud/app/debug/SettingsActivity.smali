.class public Lcom/navdy/hud/app/debug/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/debug/SettingsActivity$DeveloperPreferencesFragment;
    }
.end annotation


# static fields
.field public static final RESTART_ON_CRASH_KEY:Ljava/lang/String; = "restart_on_crash_preference"

.field public static final START_ON_BOOT_KEY:Ljava/lang/String; = "start_on_boot_preference"

.field public static final START_VIDEO_ON_BOOT_KEY:Ljava/lang/String; = "start_video_on_boot_preference"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "target":Ljava/util/List;, "Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    const v0, 0x7f060001

    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/app/debug/SettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 46
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    return-void
.end method
