.class Lcom/navdy/hud/app/debug/RouteRecorder$10;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;->writeMarker(Ljava/lang/String;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

.field final synthetic val$marker:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$10;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$10;->val$marker:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 485
    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$10;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z
    invoke-static {v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1400(Lcom/navdy/hud/app/debug/RouteRecorder;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 494
    :goto_0
    return-void

    .line 490
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$10;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->driveLogWriter:Ljava/io/BufferedWriter;
    invoke-static {v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/io/BufferedWriter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$10;->val$marker:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 491
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
