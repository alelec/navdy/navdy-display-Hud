.class final Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;
.super Ljava/lang/Object;
.source "AsyncBufferedFileWriter.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic $data:Ljava/lang/String;

.field final synthetic $forceToDisk:Z

.field final synthetic this$0:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->this$0:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->$data:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->$forceToDisk:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->this$0:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->getBufferedWriter()Ljava/io/BufferedWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->$data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 24
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->$forceToDisk:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter$write$1;->this$0:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->getBufferedWriter()Ljava/io/BufferedWriter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 26
    :cond_0
    return-void
.end method
