.class public Lcom/navdy/hud/app/debug/RouteRecorder;
.super Ljava/lang/Object;
.source "RouteRecorder.java"


# static fields
.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field public static final SECONDARY_LOCATION_TAG:Ljava/lang/String; = "[secondary] "

.field private static final sInstance:Lcom/navdy/hud/app/debug/RouteRecorder;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

.field private final context:Landroid/content/Context;

.field private volatile currentInjectionIndex:I

.field private currentLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field private driveLogWriter:Ljava/io/BufferedWriter;

.field private final gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

.field private final handler:Landroid/os/Handler;

.field private final injectFakeLocationRunnable:Ljava/lang/Runnable;

.field private volatile isLooping:Z

.field private volatile isRecording:Z

.field lastReportedDST:Z

.field private lastReportedTimeZone:Ljava/util/TimeZone;

.field private final locationListener:Landroid/location/LocationListener;

.field private final locationManager:Landroid/location/LocationManager;

.field private final pathManager:Lcom/navdy/hud/app/storage/PathManager;

.field private playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

.field private volatile prepared:Z

.field private volatile preparedFileLastModifiedTime:J

.field private volatile preparedFileName:Ljava/lang/String;

.field private recordingLabel:Ljava/lang/String;

.field private startLocationUpdates:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 52
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'_\'HH_mm_ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 55
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-direct {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sInstance:Lcom/navdy/hud/app/debug/RouteRecorder;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedTimeZone:Ljava/util/TimeZone;

    .line 73
    iput-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedDST:Z

    .line 76
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 83
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/debug/RouteRecorder$1;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->startLocationUpdates:Ljava/lang/Runnable;

    .line 95
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/debug/RouteRecorder$2;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    .line 130
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/debug/RouteRecorder$3;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->locationListener:Landroid/location/LocationListener;

    .line 150
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->context:Landroid/content/Context;

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->context:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;

    .line 152
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    .line 153
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    .line 154
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->pathManager:Lcom/navdy/hud/app/storage/PathManager;

    .line 156
    iput-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    .line 157
    iput v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    .line 158
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->locationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/storage/PathManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->pathManager:Lcom/navdy/hud/app/storage/PathManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/io/BufferedWriter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->driveLogWriter:Ljava/io/BufferedWriter;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/io/BufferedWriter;)Ljava/io/BufferedWriter;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;
    .param p1, "x1"    # Ljava/io/BufferedWriter;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->driveLogWriter:Ljava/io/BufferedWriter;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->startLocationUpdates:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/debug/RouteRecorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/debug/RouteRecorder;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    return p1
.end method

.method static synthetic access$308(Lcom/navdy/hud/app/debug/RouteRecorder;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/device/gps/GpsManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/debug/RouteRecorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isLooping:Z

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/debug/RouteRecorder;Landroid/location/Location;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;
    .param p1, "x1"    # Landroid/location/Location;
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/debug/RouteRecorder;->persistLocationToFile(Landroid/location/Location;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/debug/RouteRecorder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/RouteRecorder;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/RouteRecorder;->bStopRecording(Z)V

    return-void
.end method

.method private bStopRecording(Z)V
    .locals 3
    .param p1, "local"    # Z

    .prologue
    .line 199
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->driveLogWriter:Ljava/io/BufferedWriter;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->recordingLabel:Ljava/lang/String;

    .line 202
    if-nez p1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    new-instance v1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 205
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "recording stopped"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method private createDriveRecord(Ljava/lang/String;Z)Ljava/lang/Runnable;
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "local"    # Z

    .prologue
    .line 433
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$8;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/hud/app/debug/RouteRecorder$8;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sInstance:Lcom/navdy/hud/app/debug/RouteRecorder;

    return-object v0
.end method

.method private persistLocationToFile(Landroid/location/Location;Z)V
    .locals 11
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "secondary"    # Z

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 499
    iget-boolean v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    if-nez v5, :cond_0

    .line 528
    :goto_0
    return-void

    .line 503
    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    .line 504
    .local v2, "provider":Ljava/lang/String;
    const-string v5, "NAVDY_GPS_PROVIDER"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 505
    const-string v2, "gps"

    .line 508
    :cond_1
    const-string v4, ""

    .line 509
    .local v4, "zoneString":Ljava/lang/String;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    .line 510
    .local v3, "tz":Ljava/util/TimeZone;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v5}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    .line 511
    .local v0, "inDST":Z
    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v3, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedDST:Z

    if-eq v0, v5, :cond_3

    .line 512
    :cond_2
    const-string v6, "#zone=\"%s\", dst=%s\n"

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v9

    if-eqz v0, :cond_4

    const-string v5, "T"

    :goto_1
    aput-object v5, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 513
    iput-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedTimeZone:Ljava/util/TimeZone;

    .line 514
    iput-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->lastReportedDST:Z

    .line 515
    sget-object v5, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "setting zoneinfo:  %s dst=%b"

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 517
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_5

    const-string v5, "[secondary] "

    :goto_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 518
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 519
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 520
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 521
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 522
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 523
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 524
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 527
    .local v1, "locationString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/navdy/hud/app/debug/RouteRecorder;->writeLocation(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v6

    const/16 v7, 0x9

    invoke-virtual {v5, v6, v7}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 512
    .end local v1    # "locationString":Ljava/lang/String;
    :cond_4
    const-string v5, "F"

    goto/16 :goto_1

    .line 517
    :cond_5
    const-string v5, ""

    goto/16 :goto_2
.end method

.method private writeLocation(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "locationString"    # Ljava/lang/String;

    .prologue
    .line 464
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$9;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/debug/RouteRecorder$9;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;)V

    return-object v0
.end method

.method private writeMarker(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "marker"    # Ljava/lang/String;

    .prologue
    .line 482
    new-instance v0, Lcom/navdy/hud/app/debug/RouteRecorder$10;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/debug/RouteRecorder$10;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized bPrepare(Ljava/lang/String;Z)Z
    .locals 18
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 254
    .local v4, "driveLogsDir":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 255
    .local v3, "driveLogFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    .line 256
    .local v10, "modifiedTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileLastModifiedTime:J

    cmp-long v14, v14, v10

    if-nez v14, :cond_0

    .line 257
    sget-object v14, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "Already prepared, ready for playback"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    const/4 v14, 0x1

    .line 320
    :goto_0
    monitor-exit p0

    return v14

    .line 260
    :cond_0
    :try_start_1
    sget-object v14, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Preparing for playback from file : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileName:Ljava/lang/String;

    .line 262
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileLastModifiedTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    const/4 v12, 0x0

    .line 265
    .local v12, "reader":Ljava/io/BufferedReader;
    :try_start_2
    new-instance v13, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    new-instance v15, Ljava/io/FileInputStream;

    invoke-direct {v15, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v16, "utf-8"

    invoke-direct/range {v14 .. v16}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v13, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 267
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .local v13, "reader":Ljava/io/BufferedReader;
    :try_start_3
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    .line 268
    :cond_1
    :goto_1
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .local v8, "line":Ljava/lang/String;
    if-eqz v8, :cond_7

    .line 269
    const-string v14, "#"

    invoke-virtual {v8, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 272
    const-string v14, "[secondary] "

    invoke-virtual {v8, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 273
    .local v7, "index":I
    if-ltz v7, :cond_4

    const/4 v6, 0x1

    .line 274
    .local v6, "hasSecondaryTag":Z
    :goto_2
    if-eqz v6, :cond_2

    .line 275
    const-string v14, "[secondary] "

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v8, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 277
    :cond_2
    if-nez p2, :cond_5

    .line 279
    if-nez v6, :cond_1

    .line 288
    :cond_3
    const-string v14, ","

    invoke-virtual {v8, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 289
    .local v9, "lineSplit":[Ljava/lang/String;
    array-length v14, v9

    const/16 v15, 0x8

    if-ne v14, v15, :cond_6

    .line 290
    new-instance v14, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v14}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    const/4 v15, 0x0

    aget-object v15, v9, v15

    .line 291
    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x1

    aget-object v15, v9, v15

    .line 292
    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x2

    aget-object v15, v9, v15

    .line 293
    invoke-static {v15}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x3

    aget-object v15, v9, v15

    .line 294
    invoke-static {v15}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x4

    aget-object v15, v9, v15

    .line 295
    invoke-static {v15}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x5

    aget-object v15, v9, v15

    .line 296
    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x6

    aget-object v15, v9, v15

    .line 297
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    .line 298
    invoke-virtual {v14}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 300
    .local v2, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    .line 316
    .end local v2    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v6    # "hasSecondaryTag":Z
    .end local v7    # "index":I
    .end local v8    # "line":Ljava/lang/String;
    .end local v9    # "lineSplit":[Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v12, v13

    .line 317
    .end local v13    # "reader":Ljava/io/BufferedReader;
    .local v5, "e":Ljava/lang/Exception;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    :goto_3
    :try_start_4
    sget-object v14, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "Error parsing the file, prepare failed"

    invoke-virtual {v14, v15, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 318
    const/4 v14, 0x0

    .line 320
    :try_start_5
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 253
    .end local v3    # "driveLogFile":Ljava/io/File;
    .end local v4    # "driveLogsDir":Ljava/io/File;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v10    # "modifiedTime":J
    .end local v12    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 273
    .restart local v3    # "driveLogFile":Ljava/io/File;
    .restart local v4    # "driveLogsDir":Ljava/io/File;
    .restart local v7    # "index":I
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v10    # "modifiedTime":J
    .restart local v13    # "reader":Ljava/io/BufferedReader;
    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 284
    .restart local v6    # "hasSecondaryTag":Z
    :cond_5
    if-nez v6, :cond_3

    goto/16 :goto_1

    .line 301
    .restart local v9    # "lineSplit":[Ljava/lang/String;
    :cond_6
    :try_start_6
    array-length v14, v9

    const/4 v15, 0x6

    if-ne v14, v15, :cond_1

    .line 302
    new-instance v14, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v14}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    const/4 v15, 0x0

    aget-object v15, v9, v15

    .line 303
    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x1

    aget-object v15, v9, v15

    .line 304
    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x2

    aget-object v15, v9, v15

    .line 305
    invoke-static {v15}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x3

    aget-object v15, v9, v15

    .line 306
    invoke-static {v15}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/high16 v15, 0x3f800000    # 1.0f

    .line 307
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const-wide/16 v16, 0x0

    .line 308
    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    const/4 v15, 0x4

    aget-object v15, v9, v15

    .line 309
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v14

    .line 310
    invoke-virtual {v14}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 312
    .restart local v2    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 320
    .end local v2    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v6    # "hasSecondaryTag":Z
    .end local v7    # "index":I
    .end local v8    # "line":Ljava/lang/String;
    .end local v9    # "lineSplit":[Ljava/lang/String;
    :catchall_1
    move-exception v14

    move-object v12, v13

    .end local v13    # "reader":Ljava/io/BufferedReader;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    :goto_4
    :try_start_7
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v14

    .line 315
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v13    # "reader":Ljava/io/BufferedReader;
    :cond_7
    const/4 v14, 0x1

    .line 320
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .end local v8    # "line":Ljava/lang/String;
    .end local v13    # "reader":Ljava/io/BufferedReader;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v14

    goto :goto_4

    .line 316
    :catch_1
    move-exception v5

    goto/16 :goto_3
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->recordingLabel:Ljava/lang/String;

    return-object v0
.end method

.method public injectLocation(Landroid/location/Location;Z)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "secondary"    # Z

    .prologue
    .line 531
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/debug/RouteRecorder;->persistLocationToFile(Landroid/location/Location;Z)V

    .line 532
    return-void
.end method

.method public injectMarker(Ljava/lang/String;)V
    .locals 3
    .param p1, "marker"    # Ljava/lang/String;

    .prologue
    .line 535
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    if-nez v0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 539
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/RouteRecorder;->writeMarker(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 543
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    return v0
.end method

.method public isStopped()Z
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pausePlayback()V
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 370
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already stopped, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 377
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 374
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxReporting()V

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->setSimulatingGpsCoordinates(Z)V

    goto :goto_0
.end method

.method public prepare(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Playback is not stopped , current state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/DriveRecorder$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 250
    :goto_0
    return-void

    .line 239
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/RouteRecorder$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/debug/RouteRecorder$6;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;Z)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public declared-synchronized release()V
    .locals 2

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 404
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;

    .line 406
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileName:Ljava/lang/String;

    .line 407
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->preparedFileLastModifiedTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    monitor-exit p0

    return-void

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public requestRecordings()V
    .locals 3

    .prologue
    .line 209
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/RouteRecorder$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/RouteRecorder$5;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 228
    return-void
.end method

.method public restartPlayback()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 414
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 415
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 417
    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/service/HudConnectionService;->setSimulatingGpsCoordinates(Z)V

    .line 419
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 420
    iput v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    .line 421
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 423
    const/4 v0, 0x1

    .line 425
    :cond_2
    return v0
.end method

.method public resumePlayback()V
    .locals 2

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 382
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 383
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->setSimulatingGpsCoordinates(Z)V

    .line 384
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 386
    :cond_0
    return-void
.end method

.method public setConnectionService(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "connectionService"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 429
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    .line 430
    return-void
.end method

.method public startPlayback(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z
    .param p3, "local"    # Z

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/debug/RouteRecorder;->startPlayback(Ljava/lang/String;ZZZ)V

    .line 232
    return-void
.end method

.method public startPlayback(Ljava/lang/String;ZZZ)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z
    .param p3, "loop"    # Z
    .param p4, "local"    # Z

    .prologue
    const/4 v2, 0x1

    .line 326
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    if-eqz v0, :cond_2

    .line 327
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already busy, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 328
    if-nez p4, :cond_1

    .line 329
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    new-instance v1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 366
    :cond_1
    :goto_0
    return-void

    .line 333
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 334
    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isLooping:Z

    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->setSimulatingGpsCoordinates(Z)V

    .line 338
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/RouteRecorder$7;

    invoke-direct {v1, p0, p1, p2, p4}, Lcom/navdy/hud/app/debug/RouteRecorder$7;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;ZZ)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public startRecording(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "local"    # Z

    .prologue
    .line 161
    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 162
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "already busy"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 163
    if-nez p2, :cond_1

    .line 164
    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    new-instance v3, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 166
    :cond_1
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    .line 168
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Starting the drive recording"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 169
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    .line 170
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->recordingLabel:Ljava/lang/String;

    .line 171
    sget-object v2, Lcom/navdy/hud/app/debug/RouteRecorder;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "formattedDate":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/navdy/hud/app/util/GenericUtil;->normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/debug/RouteRecorder;->createDriveRecord(Ljava/lang/String;Z)Ljava/lang/Runnable;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public stopPlayback()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 389
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isStopped()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already stopped, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 399
    :goto_0
    return-void

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 394
    iput v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I

    .line 395
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 396
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxReporting()V

    .line 397
    iput-boolean v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isLooping:Z

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->setSimulatingGpsCoordinates(Z)V

    goto :goto_0
.end method

.method public stopRecording(Z)V
    .locals 2
    .param p1, "local"    # Z

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording:Z

    if-nez v0, :cond_0

    .line 179
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already stopped, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    :goto_0
    return-void

    .line 182
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stopping the drive recording"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/debug/RouteRecorder$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/debug/RouteRecorder$4;-><init>(Lcom/navdy/hud/app/debug/RouteRecorder;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
