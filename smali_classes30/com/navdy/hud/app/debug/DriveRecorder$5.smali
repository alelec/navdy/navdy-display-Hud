.class Lcom/navdy/hud/app/debug/DriveRecorder$5;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 347
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$600(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$200(Lcom/navdy/hud/app/debug/DriveRecorder;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    if-le v6, v7, :cond_1

    .line 348
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$600(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$200(Lcom/navdy/hud/app/debug/DriveRecorder;)I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 349
    .local v0, "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/List<Lcom/navdy/obd/Pid;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 350
    .local v4, "timeStamp":J
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-virtual {v8, v6, v7}, Lcom/navdy/hud/app/obd/ObdManager;->injectObdData(Ljava/util/List;Ljava/util/List;)V

    .line 351
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$600(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$200(Lcom/navdy/hud/app/debug/DriveRecorder;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    if-le v6, v7, :cond_0

    .line 352
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$600(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # ++operator for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$204(Lcom/navdy/hud/app/debug/DriveRecorder;)I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 353
    .local v1, "nextData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/List<Lcom/navdy/obd/Pid;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v2, v6, v4

    .line 354
    .local v2, "timeDiff":J
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$400(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$500(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 363
    .end local v0    # "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/List<Lcom/navdy/obd/Pid;>;>;"
    .end local v1    # "nextData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/List<Lcom/navdy/obd/Pid;>;>;"
    .end local v2    # "timeDiff":J
    .end local v4    # "timeStamp":J
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isLooping:Z
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$700(Lcom/navdy/hud/app/debug/DriveRecorder;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 357
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v6, v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$202(Lcom/navdy/hud/app/debug/DriveRecorder;I)I

    .line 358
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$400(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$500(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 360
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Done playing the recorded data. Stopping the playback"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 361
    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$5;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    goto :goto_0
.end method
