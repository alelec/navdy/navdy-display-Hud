.class Lcom/navdy/hud/app/debug/DriveRecorder$3;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->startPlayback(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 251
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->val$fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->bPrepare(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to prepare for playback , file : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->val$fileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 253
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 267
    :goto_0
    return-void

    .line 256
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Prepare succeeded"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 258
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I
    invoke-static {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$202(Lcom/navdy/hud/app/debug/DriveRecorder;I)I

    .line 259
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "ObdPlayback"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$302(Lcom/navdy/hud/app/debug/DriveRecorder;Landroid/os/HandlerThread;)Landroid/os/HandlerThread;

    .line 260
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$300(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/HandlerThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 261
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;
    invoke-static {v3}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$300(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/HandlerThread;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$402(Lcom/navdy/hud/app/debug/DriveRecorder;Landroid/os/Handler;)Landroid/os/Handler;

    .line 262
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$400(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$500(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$3;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 265
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
