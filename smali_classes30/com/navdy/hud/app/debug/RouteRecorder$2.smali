.class Lcom/navdy/hud/app/debug/RouteRecorder$2;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 98
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    move-result v5

    if-le v4, v5, :cond_2

    .line 99
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/location/Coordinate;

    .line 102
    .local v1, "original":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>(Lcom/navdy/service/library/events/location/Coordinate;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 103
    .local v0, "c":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->gpsManager:Lcom/navdy/hud/app/device/gps/GpsManager;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$400(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/device/gps/GpsManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->feedLocation(Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 105
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    if-le v4, v5, :cond_0

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    .line 108
    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$300(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v2, v6, v4

    .line 109
    .local v2, "timeDiff":J
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # operator++ for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$308(Lcom/navdy/hud/app/debug/RouteRecorder;)I

    .line 111
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    .end local v0    # "c":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v1    # "original":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v2    # "timeDiff":J
    :goto_0
    return-void

    .line 114
    .restart local v0    # "c":Lcom/navdy/service/library/events/location/Coordinate;
    .restart local v1    # "original":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->isLooping:Z
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$600(Lcom/navdy/hud/app/debug/RouteRecorder;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # setter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v4, v8}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$302(Lcom/navdy/hud/app/debug/RouteRecorder;I)I

    .line 116
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$700(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 118
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    goto :goto_0

    .line 121
    .end local v0    # "c":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v1    # "original":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->isLooping:Z
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$600(Lcom/navdy/hud/app/debug/RouteRecorder;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentLocations:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$200(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 122
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # setter for: Lcom/navdy/hud/app/debug/RouteRecorder;->currentInjectionIndex:I
    invoke-static {v4, v8}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$302(Lcom/navdy/hud/app/debug/RouteRecorder;I)I

    .line 123
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->injectFakeLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$700(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 125
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$2;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-virtual {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    goto :goto_0
.end method
