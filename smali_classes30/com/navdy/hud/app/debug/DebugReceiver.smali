.class public Lcom/navdy/hud/app/debug/DebugReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DebugReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/debug/DebugReceiver$PrinterImpl;
    }
.end annotation


# static fields
.field public static final ACTION_ANR:Ljava/lang/String; = "com.navdy.app.debug.ANR"

.field public static final ACTION_BROADCAST_ANR:Ljava/lang/String; = "com.navdy.app.debug.BROADCAST_ANR"

.field public static final ACTION_CHANGE_HEADING:Ljava/lang/String; = "com.navdy.app.debug.CHANGE_HEADING"

.field public static final ACTION_CHECK_FOR_MAP_UPDATES:Ljava/lang/String; = "com.navdy.app.debug.CHECK_FOR_MAP_DATA_UPDATE"

.field public static final ACTION_CLEAR_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.CLEAR_MANEUVER"

.field public static final ACTION_CRASH:Ljava/lang/String; = "com.navdy.app.debug.CRASH"

.field public static final ACTION_DNS_LOOKUP:Ljava/lang/String; = "com.navdy.app.debug.DNS_LOOKUP"

.field public static final ACTION_DNS_LOOKUP_TEST:Ljava/lang/String; = "com.navdy.app.debug.DNS_LOOKUP_TEST"

.field public static final ACTION_DUMP_THREAD_STACK:Ljava/lang/String; = "com.navdy.hud.debug.DUMP_THREAD_STACK"

.field public static final ACTION_EJECT_JUNCTION_VIEW:Ljava/lang/String; = "com.navdy.app.debug.EJECT_JUNC_VIEW"

.field public static final ACTION_GET_GOOGLE:Ljava/lang/String; = "com.navdy.app.debug.GET_GOOGLE"

.field public static final ACTION_HIDE_LANE_INFO:Ljava/lang/String; = "com.navdy.app.debug.HIDE_LANE_INFO"

.field public static final ACTION_HIDE_RECALC:Ljava/lang/String; = "com.navdy.app.debug.HIDE_RECALC"

.field public static final ACTION_INJECT_JUNCTION_VIEW:Ljava/lang/String; = "com.navdy.app.debug.INJECT_JUNC_VIEW"

.field public static final ACTION_LAUNCH_MUSIC_DETAILS:Ljava/lang/String; = "com.navdy.app.debug.MUSIC_DETAILS"

.field public static final ACTION_LAUNCH_PICKER:Ljava/lang/String; = "com.navdy.app.debug.LAUNCH_PICKER"

.field public static final ACTION_NATIVE_CRASH:Ljava/lang/String; = "com.navdy.app.debug.NATIVE_CRASH"

.field public static final ACTION_NATIVE_CRASH_SEPARATE_THREAD:Ljava/lang/String; = "com.navdy.app.debug.NATIVE_CRASH_SEPARATE_THREAD"

.field public static final ACTION_NETSTAT:Ljava/lang/String; = "com.navdy.app.debug.NETSTAT"

.field public static final ACTION_PLAYBACK_ROUTE:Ljava/lang/String; = "com.navdy.app.debug.PLAYBACK_ROUTE"

.field public static final ACTION_PLAYBACK_ROUTE_SECONDARY:Ljava/lang/String; = "com.navdy.app.debug.PLAYBACK_ROUTE_SECONDARY"

.field public static final ACTION_PRINT_BUS_REGISTRATION:Ljava/lang/String; = "com.navdy.app.debug.PRINT_BUS_REGISTRATION"

.field public static final ACTION_PRINT_MAP_NAV_MODE:Ljava/lang/String; = "com.navdy.app.debug.PRINT_MAP_NAV_MODE"

.field public static final ACTION_PRINT_MAP_ZOOM:Ljava/lang/String; = "com.navdy.app.debug.PRINT_MAP_ZOOM"

.field public static final ACTION_SCALE_DISPLAY:Ljava/lang/String; = "com.navdy.app.debug.SCALE_DISPLAY"

.field public static final ACTION_SEND_NOW_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.NOW_MANEUVER"

.field public static final ACTION_SEND_SOON_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.SOON_MANEUVER"

.field public static final ACTION_SEND_STAY_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.STAY_MANEUVER"

.field public static final ACTION_SET_DISTANCE_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.DISTANCE_MANEUVER"

.field public static final ACTION_SET_ICON_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.ICON_MANEUVER"

.field public static final ACTION_SET_INSTRUCTION_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.INSTRUCTION_MANEUVER"

.field public static final ACTION_SET_SIMULATION_SPEED:Ljava/lang/String; = "com.navdy.app.debug.SET_SIM_SPEED"

.field public static final ACTION_SET_START_ROUTE_CALC_POINT:Ljava/lang/String; = "com.navdy.app.debug.START_ROUTE_POINT"

.field public static final ACTION_SET_THEN_MANEUVER:Ljava/lang/String; = "com.navdy.app.debug.THEN_MANEUVER"

.field public static final ACTION_SHOW_LANE_INFO:Ljava/lang/String; = "com.navdy.app.debug.SHOW_LANE_INFO"

.field public static final ACTION_SHOW_RECALC:Ljava/lang/String; = "com.navdy.app.debug.SHOW_RECALC"

.field public static final ACTION_START_BUS_PROFILING:Ljava/lang/String; = "com.navdy.app.debug.START_BUS_PROFILING"

.field public static final ACTION_START_CPU_HOG:Ljava/lang/String; = "com.navdy.app.debug.START_CPU_HOG"

.field public static final ACTION_START_MAIN_THREAD_PROFILING:Ljava/lang/String; = "com.navdy.app.debug.START_MAIN_THREAD_PROFILING"

.field public static final ACTION_START_MAP_RENDERING:Ljava/lang/String; = "com.navdy.app.debug.START_MAP_RENDERING"

.field public static final ACTION_STOP_BUS_PROFILING:Ljava/lang/String; = "com.navdy.app.debug.STOP_BUS_PROFILING"

.field public static final ACTION_STOP_CPU_HOG:Ljava/lang/String; = "com.navdy.app.debug.STOP_CPU_HOG"

.field public static final ACTION_STOP_MAIN_THREAD_PROFILING:Ljava/lang/String; = "com.navdy.app.debug.STOP_MAIN_THREAD_PROFILING"

.field public static final ACTION_STOP_MAP_RENDERING:Ljava/lang/String; = "com.navdy.app.debug.STOP_MAP_RENDERING"

.field public static final ACTION_STOP_PLAYBACK_ROUTE:Ljava/lang/String; = "com.navdy.app.debug.STOP_PLAYBACK_ROUTE"

.field public static final ACTION_TEST:Ljava/lang/String; = "com.navdy.hud.debug.TEST"

.field public static final ACTION_TEST_TBT_TTS:Ljava/lang/String; = "com.navdy.app.debug.TEST_TBT_TTS"

.field public static final ACTION_TEST_TBT_TTS_CANCEL:Ljava/lang/String; = "com.navdy.app.debug.TEST_TBT_TTS_CANCEL"

.field public static final ACTION_UPDATE_MAP_DATA:Ljava/lang/String; = "com.navdy.app.debug.UPDATE_MAP_DATA"

.field public static final ACTION_WAKEUP:Ljava/lang/String; = "com.navdy.app.debug.WAKEUP"

.field public static final CONNECTION_TIMEOUT_MILLIS:I = 0x7530

.field public static final DESTINATION_SUGGESTION_TYPE_FAVORITE:I = 0x2

.field public static final DESTINATION_SUGGESTION_TYPE_HOME:I = 0x0

.field public static final DESTINATION_SUGGESTION_TYPE_NEW_PLACE:I = 0x3

.field public static final DESTINATION_SUGGESTION_TYPE_WORK:I = 0x1

.field public static final DISABLE_HERE_LOC_DBG:Ljava/lang/String; = "com.navdy.app.debug.DISABLE_HERE_LOC_DBG"

.field public static final DNS_LOOKUP_HOST_NAME:Ljava/lang/String; = "DNS_LOOKUP_HOST_NAME"

.field private static final ENABLED:Z = true

.field public static final ENABLE_HERE_LOC_DBG:Ljava/lang/String; = "com.navdy.app.debug.ENABLE_HERE_LOC_DBG"

.field public static final EXTRA_BANDWIDTH_LEVEL:Ljava/lang/String; = "BANDWIDTH_LEVEL"

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "COMMAND"

.field public static final EXTRA_DEEP:Ljava/lang/String; = "DEEP"

.field public static final EXTRA_DESTINATION_SUGGESTION_TYPE:Ljava/lang/String; = "DESTINATION_SUGGESTION_TYPE"

.field public static final EXTRA_KEY:Ljava/lang/String; = "KEY"

.field public static final EXTRA_LIGHT:Ljava/lang/String; = "LIGHT"

.field public static final EXTRA_MANEUVER_DISTANCE:Ljava/lang/String; = "EXTRA_MANEUVER_DISTANCE"

.field public static final EXTRA_MANEUVER_ICON:Ljava/lang/String; = "EXTRA_MANEUVER_ICON"

.field public static final EXTRA_MANEUVER_INSTRUCTION:Ljava/lang/String; = "EXTRA_MANEUVER_INSTRUCTION"

.field public static final EXTRA_MANEUVER_THEN_ICON:Ljava/lang/String; = "EXTRA_MANEUVER_THEN_ICON"

.field public static final EXTRA_MODE:Ljava/lang/String; = "MODE"

.field public static final EXTRA_NOTIFICATION_EVENT_APP_ID:Ljava/lang/String; = "appId"

.field public static final EXTRA_NOTIFICATION_EVENT_APP_NAME:Ljava/lang/String; = "appName"

.field public static final EXTRA_NOTIFICATION_EVENT_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_NOTIFICATION_EVENT_MESSAGE:Ljava/lang/String; = "message"

.field public static final EXTRA_NOTIFICATION_EVENT_SUB_TITLE:Ljava/lang/String; = "subtitle"

.field public static final EXTRA_NOTIFICATION_EVENT_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_OBD_CONFIG:Ljava/lang/String; = "CONFIG"

.field public static final EXTRA_SIZE:Ljava/lang/String; = "SIZE"

.field public static final EXTRA_TEST:Ljava/lang/String; = "TEST"

.field public static final EXTRA_URL:Ljava/lang/String; = "URL"

.field public static final HEADING:Ljava/lang/String; = "HEADING"

.field private static final MAIN_THREAD_DEFAULT_PROFILING_THRESHOLD:I = 0x0

.field public static final MAIN_THREAD_EXTRA_PROFILING_THRESHOLD:Ljava/lang/String; = "PROFILING_THRESHOLD"

.field public static final READ_TIMEOUT_MILLIS:I = 0x7530

.field public static final SPEED:Ljava/lang/String; = "SPEED"

.field public static final TEST_DESTINATION_SUGGESTION:Ljava/lang/String; = "TEST_DESTINATION_SUGGESTION"

.field public static final TEST_DIAL:Ljava/lang/String; = "TEST_DIAL"

.field public static final TEST_DISABLE_OBD_PIDS_SCANNING:Ljava/lang/String; = "TEST_DISABLE_OBD"

.field public static final TEST_ENABLE_OBD_PIDS_SCANNING:Ljava/lang/String; = "TEST_ENABLE_OBD"

.field public static final TEST_HID:Ljava/lang/String; = "HID"

.field public static final TEST_LAUNCH_APP:Ljava/lang/String; = "LAUNCH_APP"

.field public static final TEST_LIGHT:Ljava/lang/String; = "TEST_LIGHT"

.field public static final TEST_OBD_CONFIG:Ljava/lang/String; = "TEST_OBD"

.field public static final TEST_OBD_FLASH_NEW_FIRMWARE:Ljava/lang/String; = "TEST_OBD_FLASH_NEW_FIRMWARE"

.field public static final TEST_OBD_FLASH_OLD_FIRMWARE:Ljava/lang/String; = "TEST_OBD_FLASH_OLD_FIRMWARE"

.field public static final TEST_OBD_RESET:Ljava/lang/String; = "TEST_OBD_RESET"

.field public static final TEST_OBD_SET_MODE_J1939:Ljava/lang/String; = "TEST_OBD_SET_MODE_J1939"

.field public static final TEST_OBD_SET_MODE_OBD2:Ljava/lang/String; = "TEST_OBD_SET_MODE_OBD2"

.field public static final TEST_OBD_SLEEP:Ljava/lang/String; = "TEST_OBD_SLEEP"

.field public static final TEST_PROXY_FILE_DOWNLOAD:Ljava/lang/String; = "TEST_PROXY_FILE_DOWNLOAD"

.field public static final TEST_PROXY_FILE_UPLOAD:Ljava/lang/String; = "TEST_PROXY_FILE_UPLOAD"

.field public static final TEST_SEND_NOTIFICATION_EVENT:Ljava/lang/String; = "TEST_SEND_NOTIFICATION_EVENT"

.field public static final TEST_SHUT_DOWN:Ljava/lang/String; = "SHUTDOWN"

.field public static final TEST_SWITCH_BANDWIDTH_LEVEL:Ljava/lang/String; = "TEST_SWITCH_BANDWIDTH"

.field public static final TEST_TTS:Ljava/lang/String; = "TEST_TTS"

.field private static final TTS_TEXTS:[Ljava/lang/String;

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mBus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mainThreadProfilingPrinter:Lcom/navdy/hud/app/debug/DebugReceiver$PrinterImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Turn left after 100 feet"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Turn right after 500 feet. 1, 2, 3, 4, 5, 6, 7, 8,9,10"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "There is a slight traffic ahead, be on the right lane."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Hi how are you doing today? Testing a long long long long text"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Testing testing still testing."

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/debug/DebugReceiver;->TTS_TEXTS:[Ljava/lang/String;

    .line 241
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/debug/DebugReceiver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 252
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 250
    new-instance v0, Lcom/navdy/hud/app/debug/DebugReceiver$PrinterImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/debug/DebugReceiver$PrinterImpl;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver$1;)V

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mainThreadProfilingPrinter:Lcom/navdy/hud/app/debug/DebugReceiver$PrinterImpl;

    .line 253
    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/debug/DebugReceiver;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DebugReceiver;
    .param p1, "x1"    # I

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/DebugReceiver;->busywait(I)V

    return-void
.end method

.method private busywait(I)V
    .locals 8
    .param p1, "ms"    # I

    .prologue
    .line 1173
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1174
    .local v2, "start":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1175
    .local v0, "now":J
    :goto_0
    sub-long v4, v0, v2

    int-to-long v6, p1

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 1176
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_0

    .line 1178
    :cond_0
    return-void
.end method

.method private dnsLookup(Ljava/lang/String;)V
    .locals 3
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 1225
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DebugReceiver$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/debug/DebugReceiver$7;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1248
    return-void
.end method

.method private dnsLookupTest()V
    .locals 3

    .prologue
    .line 1269
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DebugReceiver$8;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DebugReceiver$8;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1307
    return-void
.end method

.method private getGoogleHomePage()V
    .locals 3

    .prologue
    .line 1181
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DebugReceiver$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DebugReceiver$6;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1222
    return-void
.end method

.method public static setHereDebugLocation(Z)V
    .locals 5
    .param p0, "b"    # Z

    .prologue
    .line 868
    :try_start_0
    const-class v2, Landroid/os/Looper;

    const-string v3, "enableHereLocationDebugging"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 869
    .local v0, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    invoke-virtual {v0, v2, p0}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 870
    sget-object v2, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setHereDebugLocation set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 874
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 871
    :catch_0
    move-exception v1

    .line 872
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setHereDebugLocation"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private testAppLaunch(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 893
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/input/LaunchAppEvent;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 894
    return-void
.end method

.method private testDestinationSuggestion(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 966
    const-string v3, "DESTINATION_SUGGESTION_TYPE"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 967
    .local v1, "destinationSuggestionType":I
    const/4 v0, 0x0

    .line 968
    .local v0, "destination":Lcom/navdy/service/library/events/destination/Destination;
    const/4 v2, 0x0

    .line 970
    .local v2, "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    packed-switch v1, :pswitch_data_0

    .line 1036
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v3, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1037
    return-void

    .line 972
    :pswitch_0
    new-instance v3, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e3aa9ea49b55L    # 37.7786444

    .line 973
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa16370f246031aL    # -122.4462313

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e3aa9ea49b55L    # 37.7786444

    .line 974
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa16370f246031aL    # -122.4462313

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "201 8th St, San Francisco, CA 94103"

    .line 975
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "Home"

    .line 976
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, ""

    .line 977
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 978
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "1"

    .line 979
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 980
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 981
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 982
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    .line 983
    invoke-virtual {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 985
    new-instance v2, Lcom/navdy/service/library/events/places/SuggestedDestination;

    .end local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    const/16 v3, 0x258

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {v2, v0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 986
    .restart local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    goto/16 :goto_0

    .line 988
    :pswitch_1
    new-instance v3, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e3024db819b7L    # 37.7735078

    .line 989
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa1660aee708b83L    # -122.4055828

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e3024db819b7L    # 37.7735078

    .line 990
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa1660aee708b83L    # -122.4055828

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "575 7th St, San Francisco, CA 94103"

    .line 991
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "Work"

    .line 992
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, ""

    .line 993
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 994
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "2"

    .line 995
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 996
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 997
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 998
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    .line 999
    invoke-virtual {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 1001
    new-instance v2, Lcom/navdy/service/library/events/places/SuggestedDestination;

    .end local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    const/16 v3, 0xe10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {v2, v0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 1002
    .restart local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    goto/16 :goto_0

    .line 1004
    :pswitch_2
    new-instance v3, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e4db21236637L    # 37.7879373

    .line 1005
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa165c7b104196aL    # -122.4096868

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e4db21236637L    # 37.7879373

    .line 1006
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa165c7b104196aL    # -122.4096868

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "Union Square, San Francisco, CA 94108"

    .line 1007
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "Union Square"

    .line 1008
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, ""

    .line 1009
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 1010
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "3"

    .line 1011
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 1012
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 1013
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 1014
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    .line 1015
    invoke-virtual {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 1017
    new-instance v2, Lcom/navdy/service/library/events/places/SuggestedDestination;

    .end local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {v2, v0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 1018
    .restart local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    goto/16 :goto_0

    .line 1020
    :pswitch_3
    new-instance v3, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e36aa15e89f9L    # 37.7766916

    .line 1021
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa16696cda1dee2L    # -122.3970457

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/location/LatLong;

    const-wide v6, 0x4042e36aa15e89f9L    # 37.7766916

    .line 1022
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const-wide v6, -0x3fa16696cda1dee2L    # -122.3970457

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "700 4th St, San Francisco, CA 94107"

    .line 1023
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, ""

    .line 1024
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, ""

    .line 1025
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 1026
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-string v4, "4"

    .line 1027
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 1028
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 1029
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 1030
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v3

    .line 1031
    invoke-virtual {v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 1033
    new-instance v2, Lcom/navdy/service/library/events/places/SuggestedDestination;

    .end local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {v2, v0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .restart local v2    # "suggestedDestination":Lcom/navdy/service/library/events/places/SuggestedDestination;
    goto/16 :goto_0

    .line 970
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private testDial(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 943
    const-string v1, "COMMAND"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 944
    .local v0, "cmd":Ljava/lang/String;
    const-string v1, "PowerButtonDoubleClick"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 945
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInputManager()Lcom/navdy/hud/app/manager/InputManager;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/manager/InputManager;->injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 946
    :cond_1
    const-string v1, "rebond"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 947
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/dial/DialBondRequest;

    sget-object v3, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 948
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/dial/DialBondRequest;

    sget-object v3, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private testHID(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 878
    :try_start_0
    const-string v3, "KEY"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 879
    .local v1, "mediaKey":I
    iget-object v3, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v5, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    invoke-static {}, Lcom/navdy/service/library/events/input/MediaRemoteKey;->values()[Lcom/navdy/service/library/events/input/MediaRemoteKey;

    move-result-object v6

    aget-object v6, v6, v1

    sget-object v7, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_DOWN:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {v5, v6, v7}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 880
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    .line 881
    .local v2, "timer":Ljava/util/Timer;
    new-instance v3, Lcom/navdy/hud/app/debug/DebugReceiver$2;

    invoke-direct {v3, p0, v1}, Lcom/navdy/hud/app/debug/DebugReceiver$2;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;I)V

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 890
    .end local v1    # "mediaKey":I
    .end local v2    # "timer":Ljava/util/Timer;
    :goto_0
    return-void

    .line 887
    :catch_0
    move-exception v0

    .line 888
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception getting the extra"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private testLight(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 903
    const-string v2, "LIGHT"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 904
    .local v0, "light":I
    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    .line 905
    .local v1, "manager":Lcom/navdy/hud/app/device/light/LightManager;
    packed-switch v0, :pswitch_data_0

    .line 934
    :goto_0
    return-void

    .line 907
    :pswitch_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    goto :goto_0

    .line 910
    :pswitch_1
    invoke-static {v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showShutDown(Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 913
    :pswitch_2
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Debug"

    invoke-static {v2, v1, v3}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureDetectionEnabled(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings;

    goto :goto_0

    .line 916
    :pswitch_3
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureNotRecognized(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 919
    :pswitch_4
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureDetected(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 922
    :pswitch_5
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showUSBPowerOn(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 925
    :pswitch_6
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showUSBPowerShutDown(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 928
    :pswitch_7
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showUSBTransfer(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 931
    :pswitch_8
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showError(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_0

    .line 905
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private testObdConfig(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 953
    const-string v1, "CONFIG"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 954
    .local v0, "config":I
    packed-switch v0, :pswitch_data_0

    .line 963
    :goto_0
    return-void

    .line 956
    :pswitch_0
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v1

    const-string v2, "debug"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setConfigurationFile(Ljava/lang/String;)V

    goto :goto_0

    .line 959
    :pswitch_1
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v1

    const-string v2, "default_on"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setConfigurationFile(Ljava/lang/String;)V

    goto :goto_0

    .line 954
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private testProxyFileDownload(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1100
    const-string v4, "SIZE"

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1101
    .local v3, "size":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file?size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1103
    .local v1, "defaultAddress":Ljava/lang/String;
    const-string v4, "URL"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1104
    .local v2, "extra":Ljava/lang/String;
    if-eqz v2, :cond_0

    move-object v0, v2

    .line 1105
    .local v0, "address":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/debug/DebugReceiver$4;

    invoke-direct {v5, p0, v3, v0}, Lcom/navdy/hud/app/debug/DebugReceiver$4;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;ILjava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1148
    return-void

    .end local v0    # "address":Ljava/lang/String;
    :cond_0
    move-object v0, v1

    .line 1104
    goto :goto_0
.end method

.method private testProxyFileUpload(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1049
    const-string v1, "SIZE"

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1050
    .local v0, "size":I
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/debug/DebugReceiver$3;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/debug/DebugReceiver$3;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;I)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1096
    return-void
.end method

.method private testSendNotificationEvent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1252
    :try_start_0
    new-instance v1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;-><init>()V

    .line 1253
    .local v1, "notificationEventBuilder":Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    if-eqz p1, :cond_0

    .line 1254
    const-string v2, "id"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->id(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1255
    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->category(Lcom/navdy/service/library/events/notification/NotificationCategory;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1256
    const-string v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->title(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1257
    const-string v2, "subtitle"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1258
    const-string v2, "message"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->message(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1259
    const-string v2, "appId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appId(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1260
    const-string v2, "appName"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appName(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .line 1261
    iget-object v2, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1266
    .end local v1    # "notificationEventBuilder":Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    :cond_0
    :goto_0
    return-void

    .line 1263
    :catch_0
    move-exception v0

    .line 1264
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while sending test Notification event "

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private testShutDown()V
    .locals 5

    .prologue
    .line 898
    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$Reason;->asBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 899
    .local v0, "args":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DebugReceiver;->mBus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 900
    return-void
.end method

.method private testSwitchBandwidthLevel(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1040
    const-string v2, "BANDWIDTH_LEVEL"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1041
    .local v0, "bandwidthLevel":I
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1042
    .local v1, "boradcast":Landroid/content/Intent;
    const-string v2, "LINK_BANDWIDTH_LEVEL_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1043
    const-string v2, "NAVDY_LINK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1044
    const-string v2, "EXTRA_BANDWIDTH_MODE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1045
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1046
    return-void
.end method

.method private testTTS(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 937
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 938
    .local v0, "random":Ljava/util/Random;
    sget-object v2, Lcom/navdy/hud/app/debug/DebugReceiver;->TTS_TEXTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/debug/DebugReceiver;->TTS_TEXTS:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    aget-object v1, v2, v3

    .line 939
    .local v1, "text":Ljava/lang/String;
    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 940
    return-void
.end method

.method private triggerANR()V
    .locals 2

    .prologue
    .line 1157
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1158
    .local v0, "h":Landroid/os/Handler;
    new-instance v1, Lcom/navdy/hud/app/debug/DebugReceiver$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DebugReceiver$5;-><init>(Lcom/navdy/hud/app/debug/DebugReceiver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1164
    return-void
.end method

.method private triggerBroadcastANR()V
    .locals 1

    .prologue
    .line 1169
    const/16 v0, 0x2ee0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/debug/DebugReceiver;->busywait(I)V

    .line 1170
    return-void
.end method

.method private triggerCrash()V
    .locals 2

    .prologue
    .line 1151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Oops"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 258
    return-void
.end method
