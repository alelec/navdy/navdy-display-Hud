.class Lcom/navdy/hud/app/debug/RouteRecorder$6;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;->prepare(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$secondary:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->val$fileName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->val$secondary:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iget-object v2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->val$fileName:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$6;->val$secondary:Z

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/debug/RouteRecorder;->bPrepare(Ljava/lang/String;Z)Z

    move-result v0

    .line 243
    .local v0, "prepared":Z
    if-nez v0, :cond_0

    .line 244
    sget-object v1, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Prepare failed "

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 248
    :goto_0
    return-void

    .line 246
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Prepare succeeded "

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
