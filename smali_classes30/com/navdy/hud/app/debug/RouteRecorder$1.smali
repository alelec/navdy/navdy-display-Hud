.class Lcom/navdy/hud/app/debug/RouteRecorder$1;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$000(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "NAVDY_GPS_PROVIDER"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$000(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "NAVDY_GPS_PROVIDER"

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationListener:Landroid/location/LocationListener;
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$100(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$000(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$000(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->locationListener:Landroid/location/LocationListener;
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$100(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 92
    :cond_1
    return-void
.end method
