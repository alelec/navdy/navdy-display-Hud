.class Lcom/navdy/hud/app/debug/DriveRecorder$7;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->createDriveRecord(Ljava/lang/String;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 439
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 442
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->val$fileName:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 443
    .local v1, "driveLogsDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 444
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 447
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->val$fileName:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 449
    .local v0, "driveLogFile":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".obd"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 450
    .local v4, "obdDataLogFile":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".drive"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 452
    .local v3, "gforceLogFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 453
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    new-instance v6, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    const/16 v9, 0xfa

    invoke-direct {v6, v7, v8, v9}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V

    iput-object v6, v5, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    .line 454
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/obd/ObdManager;->setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V

    .line 455
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->bRecordSupportedPids()V
    invoke-static {v5}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$800(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    .line 457
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->pidListener:Lcom/navdy/obd/IPidListener;
    invoke-static {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$900(Lcom/navdy/hud/app/debug/DriveRecorder;)Lcom/navdy/obd/IPidListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/obd/ObdManager;->setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V

    .line 459
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 460
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    new-instance v6, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    const/16 v9, 0x1f4

    invoke-direct {v6, v7, v8, v9}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V

    iput-object v6, v5, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :cond_2
    :goto_0
    return-void

    .line 462
    :catch_0
    move-exception v2

    .line 463
    .local v2, "e":Ljava/io/IOException;
    sget-object v5, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 464
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z
    invoke-static {v5, v10}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$002(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z

    .line 465
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder$7;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z
    invoke-static {v5, v10}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$102(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z

    goto :goto_0
.end method
