.class Lcom/navdy/hud/app/debug/DebugReceiver$4;
.super Ljava/lang/Object;
.source "DebugReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DebugReceiver;->testProxyFileDownload(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

.field final synthetic val$address:Ljava/lang/String;

.field final synthetic val$size:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DebugReceiver;ILjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DebugReceiver;

    .prologue
    .line 1105
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$4;->this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

    iput p2, p0, Lcom/navdy/hud/app/debug/DebugReceiver$4;->val$size:I

    iput-object p3, p0, Lcom/navdy/hud/app/debug/DebugReceiver$4;->val$address:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 1109
    const/4 v8, 0x0

    .line 1110
    .local v8, "in":Ljava/io/InputStream;
    const-wide/16 v14, 0x0

    .line 1111
    .local v14, "startTime":J
    const-wide/16 v18, 0x0

    .line 1112
    .local v18, "totalBytesRead":J
    const-wide/16 v16, 0x0

    .line 1114
    .local v16, "timeElapsed":D
    :try_start_0
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Intended Size to be dowloaded is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/debug/DebugReceiver$4;->val$size:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1115
    new-instance v20, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DebugReceiver$4;->val$address:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1116
    .local v20, "url":Ljava/net/URL;
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v21

    check-cast v21, Ljava/net/HttpURLConnection;

    .line 1117
    .local v21, "urlConnection":Ljava/net/HttpURLConnection;
    const-string v22, "GET"

    invoke-virtual/range {v21 .. v22}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1118
    const/16 v22, 0x7530

    invoke-virtual/range {v21 .. v22}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 1119
    const/16 v22, 0x7530

    invoke-virtual/range {v21 .. v22}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 1120
    invoke-virtual/range {v21 .. v21}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v9

    .line 1121
    .local v9, "responseCode":I
    const/16 v22, 0xc8

    move/from16 v0, v22

    if-ne v9, v0, :cond_0

    .line 1122
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v23, "Starting to download"

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1123
    invoke-virtual/range {v21 .. v21}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 1124
    const v22, 0x19000

    move/from16 v0, v22

    new-array v4, v0, [B

    .line 1125
    .local v4, "buffer":[B
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 1126
    const-wide/16 v10, 0x0

    .line 1128
    .local v10, "lastLoggedTime":J
    :goto_0
    invoke-virtual {v8, v4}, Ljava/io/InputStream;->read([B)I

    move-result v22

    move/from16 v0, v22

    int-to-long v6, v0

    .local v6, "bytesRead":J
    const-wide/16 v22, -0x1

    cmp-long v22, v6, v22

    if-eqz v22, :cond_1

    .line 1129
    add-long v18, v18, v6

    goto :goto_0

    .line 1132
    .end local v4    # "buffer":[B
    .end local v6    # "bytesRead":J
    .end local v10    # "lastLoggedTime":J
    :cond_0
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Response code not OK "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1141
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1142
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v13, v0

    .line 1143
    .local v13, "speedBps":F
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Download finished, Size : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Time Taken :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " milliseconds , RawSpeed :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " kBps"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1144
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1146
    .end local v9    # "responseCode":I
    .end local v20    # "url":Ljava/net/URL;
    .end local v21    # "urlConnection":Ljava/net/HttpURLConnection;
    :goto_1
    return-void

    .line 1134
    .end local v13    # "speedBps":F
    :catch_0
    move-exception v12

    .line 1135
    .local v12, "se":Ljava/net/SocketTimeoutException;
    :try_start_1
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v23, "Socket timed out Exception "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1141
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1142
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v13, v0

    .line 1143
    .restart local v13    # "speedBps":F
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Download finished, Size : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Time Taken :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " milliseconds , RawSpeed :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " kBps"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1144
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .line 1136
    .end local v12    # "se":Ljava/net/SocketTimeoutException;
    .end local v13    # "speedBps":F
    :catch_1
    move-exception v5

    .line 1137
    .local v5, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v23, "IOException "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1141
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1142
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v13, v0

    .line 1143
    .restart local v13    # "speedBps":F
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Download finished, Size : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Time Taken :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " milliseconds , RawSpeed :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " kBps"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1144
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 1138
    .end local v5    # "e":Ljava/io/IOException;
    .end local v13    # "speedBps":F
    :catch_2
    move-exception v5

    .line 1139
    .local v5, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v23, "Exception "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1141
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    sub-long v22, v22, v14

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1142
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x408f400000000000L    # 1000.0

    div-double v24, v16, v24

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v13, v0

    .line 1143
    .restart local v13    # "speedBps":F
    sget-object v22, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Download finished, Size : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-wide/16 v24, 0x3e8

    div-long v24, v18, v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Time Taken :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " milliseconds , RawSpeed :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " kBps"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1144
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 1141
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v13    # "speedBps":F
    :catchall_0
    move-exception v22

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    sub-long v24, v24, v14

    move-wide/from16 v0, v24

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 1142
    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x447a0000    # 1000.0f

    div-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide v26, 0x408f400000000000L    # 1000.0

    div-double v26, v16, v26

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v13, v0

    .line 1143
    .restart local v13    # "speedBps":F
    sget-object v23, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Download finished, Size : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-wide/16 v26, 0x3e8

    div-long v26, v18, v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", Time Taken :"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " milliseconds , RawSpeed :"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " kBps"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1144
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1145
    throw v22
.end method
