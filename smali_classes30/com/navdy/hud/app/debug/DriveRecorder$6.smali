.class Lcom/navdy/hud/app/debug/DriveRecorder$6;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Lcom/navdy/obd/IPidListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConnectionStateChange(I)V
    .locals 0
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 405
    return-void
.end method

.method public pidsChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method

.method public pidsRead(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "pidsRead":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .local p2, "pidsChanged":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    if-eqz p2, :cond_0

    .line 370
    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z
    invoke-static {v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$000(Lcom/navdy/hud/app/debug/DriveRecorder;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 395
    :cond_0
    return-void

    .line 373
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z
    invoke-static {v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$100(Lcom/navdy/hud/app/debug/DriveRecorder;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 374
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/obd/ObdManager;->setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V

    .line 376
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->bRecordSupportedPids()V
    invoke-static {v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$800(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->pidListener:Lcom/navdy/obd/IPidListener;
    invoke-static {v3}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$900(Lcom/navdy/hud/app/debug/DriveRecorder;)Lcom/navdy/obd/IPidListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/obd/ObdManager;->setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V

    .line 384
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v2, v2, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 385
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 386
    .local v0, "pid":Lcom/navdy/obd/Pid;
    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v3, v3, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 387
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v0, v3, :cond_3

    .line 388
    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v3, v3, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    const-string v4, ","

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 377
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :catch_0
    move-exception v1

    .line 378
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while recording the supported PIDs"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 391
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$6;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v3, v3, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    goto :goto_1
.end method
