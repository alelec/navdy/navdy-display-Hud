.class Lcom/navdy/hud/app/debug/DriveRecorder$9;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->checkAndStartAutoRecording()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 667
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$9;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 670
    sget-object v4, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Auto recording is enabled so starting the auto record"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 671
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "drive_logs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 672
    .local v0, "driveLogsDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 673
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_0

    array-length v4, v1

    const/16 v5, 0x14

    if-le v4, v5, :cond_0

    .line 674
    new-instance v4, Lcom/navdy/hud/app/debug/DriveRecorder$FilesModifiedTimeComparator;

    invoke-direct {v4}, Lcom/navdy/hud/app/debug/DriveRecorder$FilesModifiedTimeComparator;-><init>()V

    invoke-static {v1, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 675
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    add-int/lit8 v4, v4, -0x14

    if-ge v2, v4, :cond_0

    .line 676
    aget-object v3, v1, v2

    .line 677
    .local v3, "lastFile":Ljava/io/File;
    sget-object v4, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleting the drive record "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " as its old "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 678
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 675
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 681
    .end local v2    # "i":I
    .end local v3    # "lastFile":Ljava/io/File;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder$9;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1600(Lcom/navdy/hud/app/debug/DriveRecorder;)Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v4

    new-instance v5, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    const-string v6, "auto"

    invoke-direct {v5, v6}, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendLocalMessage(Lcom/squareup/wire/Message;)V

    .line 683
    return-void
.end method
