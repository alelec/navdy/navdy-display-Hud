.class Lcom/navdy/hud/app/debug/RouteRecorder$8;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;->createDriveRecord(Ljava/lang/String;Z)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$local:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$fileName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$local:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 436
    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$fileName:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 437
    .local v1, "driveLogsDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 438
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 441
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$fileName:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 444
    .local v0, "driveLogFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v7, "utf-8"

    invoke-direct {v5, v6, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    # setter for: Lcom/navdy/hud/app/debug/RouteRecorder;->driveLogWriter:Ljava/io/BufferedWriter;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1202(Lcom/navdy/hud/app/debug/RouteRecorder;Ljava/io/BufferedWriter;)Ljava/io/BufferedWriter;

    .line 450
    sget-object v3, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "writing drive log: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 451
    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$500(Lcom/navdy/hud/app/debug/RouteRecorder;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->startLocationUpdates:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1300(Lcom/navdy/hud/app/debug/RouteRecorder;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 452
    iget-boolean v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->val$local:Z

    if-nez v3, :cond_1

    .line 453
    iget-object v3, p0, Lcom/navdy/hud/app/debug/RouteRecorder$8;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v3}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v4, v5}, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :cond_1
    :goto_0
    return-void

    .line 456
    :catch_0
    move-exception v2

    .line 457
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/hud/app/debug/RouteRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
