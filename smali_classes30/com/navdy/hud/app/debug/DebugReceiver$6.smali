.class Lcom/navdy/hud/app/debug/DebugReceiver$6;
.super Ljava/lang/Object;
.source "DebugReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DebugReceiver;->getGoogleHomePage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DebugReceiver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DebugReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DebugReceiver;

    .prologue
    .line 1181
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$6;->this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1184
    const/4 v1, 0x0

    .line 1185
    .local v1, "connection":Ljava/net/HttpURLConnection;
    const/4 v4, 0x0

    .line 1187
    .local v4, "inputStream":Ljava/io/InputStream;
    :try_start_0
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "getGoogleHomePage"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1188
    new-instance v8, Ljava/net/URL;

    const-string v10, "https://www.google.com"

    invoke-direct {v8, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1189
    .local v8, "url":Ljava/net/URL;
    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 1190
    const-string v10, "GET"

    invoke-virtual {v1, v10}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1191
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 1192
    const-string v10, "UTF-8"

    invoke-static {v4, v10}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1193
    .local v2, "data":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    .line 1194
    .local v3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1195
    .local v5, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1196
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1197
    .local v6, "key":Ljava/lang/String;
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1198
    .local v9, "val":Ljava/lang/String;
    const-string v10, "null"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    if-nez v6, :cond_2

    .line 1199
    :cond_0
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getGoogleHomePage "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1207
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v6    # "key":Ljava/lang/String;
    .end local v8    # "url":Ljava/net/URL;
    .end local v9    # "val":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1208
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "getGoogleHomePage"

    invoke-virtual {v10, v11, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1212
    if-eqz v1, :cond_1

    .line 1214
    :try_start_2
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 1220
    .end local v7    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_1
    return-void

    .line 1201
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v3    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .restart local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v6    # "key":Ljava/lang/String;
    .restart local v8    # "url":Ljava/net/URL;
    .restart local v9    # "val":Ljava/lang/String;
    :cond_2
    :try_start_3
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getGoogleHomePage "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1210
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v6    # "key":Ljava/lang/String;
    .end local v8    # "url":Ljava/net/URL;
    .end local v9    # "val":Ljava/lang/String;
    :catchall_0
    move-exception v10

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1212
    if-eqz v1, :cond_3

    .line 1214
    :try_start_4
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 1217
    :cond_3
    :goto_2
    throw v10

    .line 1205
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v3    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .restart local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v8    # "url":Ljava/net/URL;
    :cond_4
    :try_start_5
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getGoogleHomePage "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1206
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getGoogleHomePage LEN = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1210
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1212
    if-eqz v1, :cond_1

    .line 1214
    :try_start_6
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 1215
    :catch_1
    move-exception v7

    .line 1216
    .restart local v7    # "t":Ljava/lang/Throwable;
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v10, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1215
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v8    # "url":Ljava/net/URL;
    :catch_2
    move-exception v7

    .line 1216
    sget-object v10, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v10, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1215
    .end local v7    # "t":Ljava/lang/Throwable;
    :catch_3
    move-exception v7

    .line 1216
    .restart local v7    # "t":Ljava/lang/Throwable;
    sget-object v11, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v11, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2
.end method
