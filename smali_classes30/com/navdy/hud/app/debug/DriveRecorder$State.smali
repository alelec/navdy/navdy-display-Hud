.class final enum Lcom/navdy/hud/app/debug/DriveRecorder$State;
.super Ljava/lang/Enum;
.source "DriveRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/debug/DriveRecorder$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$State;

.field public static final enum PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

.field public static final enum PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

.field public static final enum STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 112
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/debug/DriveRecorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 113
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/debug/DriveRecorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 110
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/debug/DriveRecorder$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 110
    const-class v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/debug/DriveRecorder$State;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/debug/DriveRecorder$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/debug/DriveRecorder$State;

    return-object v0
.end method
