.class public Lcom/navdy/hud/app/debug/DriveRecorder;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/navdy/hud/app/debug/SerialExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/debug/DriveRecorder$FilesModifiedTimeComparator;,
        Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;,
        Lcom/navdy/hud/app/debug/DriveRecorder$Action;,
        Lcom/navdy/hud/app/debug/DriveRecorder$State;
    }
.end annotation


# static fields
.field private static final AUTO_DRIVERECORD_PROP:Ljava/lang/String; = "persist.sys.driverecord.auto"

.field public static final AUTO_RECORD_LABEL:Ljava/lang/String; = "auto"

.field public static final BUFFER_SIZE:I = 0x1f4

.field public static final BUFFER_SIZE_OBD_LOG:I = 0xfa

.field public static final DEMO_LOG_FILE_NAME:Ljava/lang/String; = "demo_log.log"

.field public static final DRIVE_LOGS_FOLDER:Ljava/lang/String; = "drive_logs"

.field public static final MAXIMUM_FILES_IN_THE_FOLDER:I = 0x14

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;

.field private connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

.field private final context:Landroid/content/Context;

.field private volatile currentObdDataInjectionIndex:I

.field demoObdLogFileExists:Z

.field demoRouteLogFileExists:Z

.field driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

.field private gforceDataBuilder:Ljava/lang/StringBuilder;

.field private final handler:Landroid/os/Handler;

.field private final injectFakeObdData:Ljava/lang/Runnable;

.field private isEngineInitialized:Z

.field private volatile isLooping:Z

.field private volatile isRecording:Z

.field private volatile isSupportedPidsWritten:Z

.field obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

.field private obdDataPlaybackHandler:Landroid/os/Handler;

.field private obdPlaybackThread:Landroid/os/HandlerThread;

.field private final pidListener:Lcom/navdy/obd/IPidListener;

.field private volatile playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

.field private volatile preparedFileLastModifiedTime:J

.field private volatile preparedFileName:Ljava/lang/String;

.field volatile realObdConnected:Z

.field private recordedObdData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/service/ConnectionHandler;)V
    .locals 4
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "connectionHandler"    # Lcom/navdy/hud/app/service/ConnectionHandler;

    .prologue
    const/4 v3, 0x0

    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->gforceDataBuilder:Ljava/lang/StringBuilder;

    .line 85
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 87
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isLooping:Z

    .line 88
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->realObdConnected:Z

    .line 89
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoRouteLogFileExists:Z

    .line 90
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoObdLogFileExists:Z

    .line 344
    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DriveRecorder$5;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    .line 366
    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DriveRecorder$6;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->pidListener:Lcom/navdy/obd/IPidListener;

    .line 414
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->context:Landroid/content/Context;

    .line 415
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->handler:Landroid/os/Handler;

    .line 416
    iput-object p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 417
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    .line 418
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 419
    iput-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z

    .line 420
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->bus:Lcom/squareup/otto/Bus;

    .line 421
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 422
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager;->setDriveRecorder(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    .line 423
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->context:Landroid/content/Context;

    const-class v2, Lcom/navdy/hud/app/debug/RouteRecorderService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 424
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->context:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 425
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/debug/DriveRecorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/debug/DriveRecorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/debug/DriveRecorder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->playDemo(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->pauseDemo()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->resumeDemo()V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/debug/DriveRecorder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->restartDemo(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopDemo()V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->prepareDemo()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/debug/DriveRecorder;)Lcom/navdy/hud/app/service/ConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/debug/DriveRecorder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/debug/DriveRecorder;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    return p1
.end method

.method static synthetic access$204(Lcom/navdy/hud/app/debug/DriveRecorder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/debug/DriveRecorder;Landroid/os/HandlerThread;)Landroid/os/HandlerThread;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Landroid/os/HandlerThread;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/debug/DriveRecorder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/debug/DriveRecorder;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/debug/DriveRecorder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/debug/DriveRecorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isLooping:Z

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->bRecordSupportedPids()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/debug/DriveRecorder;)Lcom/navdy/obd/IPidListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->pidListener:Lcom/navdy/obd/IPidListener;

    return-object v0
.end method

.method private bRecordSupportedPids()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 477
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    .line 478
    .local v0, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v2

    .line 479
    .local v2, "supportedPidSet":Lcom/navdy/obd/PidSet;
    if-eqz v2, :cond_3

    .line 480
    invoke-virtual {v2}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v3

    .line 482
    .local v3, "supportedPidsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 483
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/Pid;

    .line 484
    .local v1, "pid":Lcom/navdy/obd/Pid;
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eq v1, v5, :cond_0

    .line 485
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 487
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 491
    .end local v1    # "pid":Lcom/navdy/obd/Pid;
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    const-string v5, "-1\n"

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 494
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 495
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "47:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 496
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "13:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 497
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "12:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getEngineRpm()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 498
    iget-object v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "256:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getInstantFuelConsumption()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v8}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;Z)V

    .line 499
    iput-boolean v8, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z

    .line 500
    sget-object v4, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Obd data record created successfully and starting to listening to the PID changes"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 502
    .end local v3    # "supportedPidsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_3
    return-void
.end method

.method private checkAndStartAutoRecording()V
    .locals 3

    .prologue
    .line 666
    invoke-static {}, Lcom/navdy/hud/app/debug/DriveRecorder;->isAutoRecordingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DriveRecorder$9;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 686
    :cond_0
    return-void
.end method

.method private createDriveRecord(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 439
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$7;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder$7;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 706
    const/4 v0, 0x0

    .line 707
    .local v0, "driveLogsDir":Ljava/io/File;
    const-string v1, "demo_log.log"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 708
    new-instance v0, Ljava/io/File;

    .end local v0    # "driveLogsDir":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 712
    .restart local v0    # "driveLogsDir":Ljava/io/File;
    :goto_0
    return-object v0

    .line 710
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "driveLogsDir":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "drive_logs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v0    # "driveLogsDir":Ljava/io/File;
    goto :goto_0
.end method

.method private static getLatitude()D
    .locals 4

    .prologue
    .line 580
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 582
    .local v0, "c":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v0, :cond_0

    .line 583
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    .line 586
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private static getLongitude()D
    .locals 4

    .prologue
    .line 590
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 591
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 592
    .local v0, "c":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v0, :cond_0

    .line 593
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    .line 596
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public static isAutoRecordingEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 716
    const-string v1, "persist.sys.driverecord.auto"

    invoke-static {v1, v0}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private isPaused()Z
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPlaying()Z
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStopped()Z
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pauseDemo()V
    .locals 3

    .prologue
    .line 815
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecordingBeforeDemo()V

    .line 817
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->pausePlayback()V

    .line 822
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 823
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->pausePlayback()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :cond_1
    :goto_0
    return-void

    .line 825
    :catch_0
    move-exception v0

    .line 826
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error pausing the route playback"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private playDemo(Z)V
    .locals 4
    .param p1, "loop"    # Z

    .prologue
    .line 768
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecordingBeforeDemo()V

    .line 770
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 771
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 772
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->resumePlayback()V

    .line 779
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 780
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->resumePlayback()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :cond_1
    :goto_1
    return-void

    .line 774
    :cond_2
    const-string v1, "demo_log.log"

    invoke-virtual {p0, v1, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->startPlayback(Ljava/lang/String;Z)V

    goto :goto_0

    .line 782
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    .line 783
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    const-string v2, "demo_log.log"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, p1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->startPlayback(Ljava/lang/String;ZZ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 786
    :catch_0
    move-exception v0

    .line 787
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error playing the route playback"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private prepareDemo()V
    .locals 4

    .prologue
    .line 759
    const-string v1, "demo_log.log"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->prepare(Ljava/lang/String;)V

    .line 761
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    const-string v2, "demo_log.log"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/navdy/hud/app/debug/IRouteRecorder;->prepare(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    :goto_0
    return-void

    .line 762
    :catch_0
    move-exception v0

    .line 763
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error preparing the route recorder for demo"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private restartDemo(Z)V
    .locals 4
    .param p1, "loop"    # Z

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecordingBeforeDemo()V

    .line 861
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->restartPlayback()Z

    move-result v1

    if-nez v1, :cond_0

    .line 862
    const-string v1, "demo_log.log"

    invoke-virtual {p0, v1, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->startPlayback(Ljava/lang/String;Z)V

    .line 865
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->restartPlayback()Z

    move-result v1

    if-nez v1, :cond_1

    .line 866
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    const-string v2, "demo_log.log"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, p1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->startPlayback(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 871
    :cond_1
    :goto_0
    return-void

    .line 868
    :catch_0
    move-exception v0

    .line 869
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error restarting the route playback"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private resumeDemo()V
    .locals 3

    .prologue
    .line 845
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecordingBeforeDemo()V

    .line 847
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 848
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->resumePlayback()V

    .line 851
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 852
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->resumePlayback()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 857
    :cond_1
    :goto_0
    return-void

    .line 854
    :catch_0
    move-exception v0

    .line 855
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error resuming the route playback"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private stopDemo()V
    .locals 3

    .prologue
    .line 832
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 833
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 836
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 837
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->stopPlayback()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 842
    :cond_2
    :goto_0
    return-void

    .line 839
    :catch_0
    move-exception v0

    .line 840
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error stopping the route playback"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private stopRecordingBeforeDemo()V
    .locals 4

    .prologue
    .line 792
    iget-boolean v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-eqz v1, :cond_0

    .line 793
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecording()V

    .line 795
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 801
    :cond_0
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 802
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v1}, Lcom/navdy/hud/app/debug/IRouteRecorder;->stopRecording()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 804
    const-wide/16 v2, 0x3e8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 812
    :cond_1
    :goto_1
    return-void

    .line 796
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Interrupted "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 805
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 806
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    :try_start_3
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Interrupted while stopping the recording"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 809
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v0

    .line 810
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while stopping the recording"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public bPrepare(Ljava/lang/String;)Z
    .locals 28
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 153
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 154
    .local v2, "driveLogsDir":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".obd"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 155
    .local v11, "obdDataLogFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_0

    .line 156
    const/16 v24, 0x0

    .line 219
    :goto_0
    return v24

    .line 158
    :cond_0
    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    .line 159
    .local v6, "lastModifiedTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileLastModifiedTime:J

    move-wide/from16 v24, v0

    cmp-long v24, v24, v6

    if-nez v24, :cond_1

    .line 160
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "File already prepared"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 161
    const/16 v24, 0x1

    goto :goto_0

    .line 163
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileName:Ljava/lang/String;

    .line 164
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileLastModifiedTime:J

    .line 165
    const/16 v16, 0x0

    .line 167
    .local v16, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v17, Ljava/io/BufferedReader;

    new-instance v24, Ljava/io/InputStreamReader;

    new-instance v25, Ljava/io/FileInputStream;

    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v26, "utf-8"

    invoke-direct/range {v24 .. v26}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .local v17, "reader":Ljava/io/BufferedReader;
    :try_start_1
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "startPlayback of Obd data"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;

    .line 171
    new-instance v16, Ljava/io/BufferedReader;

    new-instance v24, Ljava/io/InputStreamReader;

    new-instance v25, Ljava/io/FileInputStream;

    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v26, "utf-8"

    invoke-direct/range {v24 .. v26}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 172
    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .line 173
    .local v8, "line":Ljava/lang/String;
    new-instance v18, Lcom/navdy/obd/PidSet;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/obd/PidSet;-><init>()V

    .line 175
    .local v18, "supportedPids":Lcom/navdy/obd/PidSet;
    if-eqz v8, :cond_2

    .line 176
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Supported pids recorded "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    const-string v25, "-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 178
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "There are no supported pids when trying to playback the data"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 191
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager;->setSupportedPidSet(Lcom/navdy/obd/PidSet;)V

    .line 192
    :cond_3
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 193
    const-string v24, ","

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 194
    .local v9, "lineSplit":[Ljava/lang/String;
    if-eqz v9, :cond_3

    array-length v0, v9

    move/from16 v24, v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_3

    .line 196
    const/16 v24, 0x0

    :try_start_3
    aget-object v24, v9, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 197
    .local v20, "timeStamp":J
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v15, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_2
    array-length v0, v9

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v4, v0, :cond_3

    .line 199
    aget-object v24, v9, v4

    const-string v25, ":"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 200
    .local v13, "pidFields":[Ljava/lang/String;
    const/16 v24, 0x0

    aget-object v24, v13, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 201
    .local v5, "id":I
    const/16 v24, 0x1

    aget-object v24, v13, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    .line 202
    .local v22, "value":D
    new-instance v12, Lcom/navdy/obd/Pid;

    invoke-direct {v12, v5}, Lcom/navdy/obd/Pid;-><init>(I)V

    .line 203
    .local v12, "pid":Lcom/navdy/obd/Pid;
    move-wide/from16 v0, v22

    invoke-virtual {v12, v0, v1}, Lcom/navdy/obd/Pid;->setValue(D)V

    .line 204
    invoke-interface {v15, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;

    move-object/from16 v24, v0

    new-instance v25, Ljava/util/AbstractMap$SimpleEntry;

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v15}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 198
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 180
    .end local v4    # "i":I
    .end local v5    # "id":I
    .end local v9    # "lineSplit":[Ljava/lang/String;
    .end local v12    # "pid":Lcom/navdy/obd/Pid;
    .end local v13    # "pidFields":[Ljava/lang/String;
    .end local v15    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v20    # "timeStamp":J
    .end local v22    # "value":D
    :cond_4
    :try_start_4
    const-string v24, ","

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 181
    .local v14, "pids":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_3
    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_2

    aget-object v12, v14, v24
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 183
    .local v12, "pid":Ljava/lang/String;
    :try_start_5
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 184
    .restart local v5    # "id":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/navdy/obd/PidSet;->add(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 181
    .end local v5    # "id":I
    :goto_4
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    .line 185
    :catch_0
    move-exception v3

    .line 186
    .local v3, "e":Ljava/lang/Exception;
    :try_start_6
    sget-object v26, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v27, "Exception while parsing the supported Pids"

    invoke-virtual/range {v26 .. v27}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 214
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v8    # "line":Ljava/lang/String;
    .end local v12    # "pid":Ljava/lang/String;
    .end local v14    # "pids":[Ljava/lang/String;
    .end local v18    # "supportedPids":Lcom/navdy/obd/PidSet;
    :catch_1
    move-exception v19

    .line 215
    .local v19, "t":Ljava/lang/Throwable;
    :goto_5
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 216
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 217
    const/16 v24, 0x0

    .line 219
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 208
    .end local v19    # "t":Ljava/lang/Throwable;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v9    # "lineSplit":[Ljava/lang/String;
    .restart local v18    # "supportedPids":Lcom/navdy/obd/PidSet;
    :catch_2
    move-exception v10

    .line 209
    .local v10, "ne":Ljava/lang/NumberFormatException;
    :try_start_8
    sget-object v24, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "Error parsing the Obd data from the file "

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 219
    .end local v8    # "line":Ljava/lang/String;
    .end local v9    # "lineSplit":[Ljava/lang/String;
    .end local v10    # "ne":Ljava/lang/NumberFormatException;
    .end local v18    # "supportedPids":Lcom/navdy/obd/PidSet;
    :catchall_0
    move-exception v24

    :goto_6
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v24

    .line 213
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v18    # "supportedPids":Lcom/navdy/obd/PidSet;
    :cond_5
    const/16 v24, 0x1

    .line 219
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v8    # "line":Ljava/lang/String;
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .end local v18    # "supportedPids":Lcom/navdy/obd/PidSet;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v24

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto :goto_6

    .line 214
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catch_3
    move-exception v19

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto :goto_5
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 107
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, p1, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 108
    return-void
.end method

.method public flushRecordings()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DriveRecorder$4;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 342
    return-void
.end method

.method public isDemoAvailable()Z
    .locals 2

    .prologue
    .line 657
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoRouteLogFileExists:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->realObdConnected:Z

    if-eqz v0, :cond_2

    .line 658
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoRouteLogFileExists:Z

    if-nez v0, :cond_1

    const-string v0, "Demo log file does not exist in the map partition. So Demo preference : NA"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 659
    const/4 v0, 0x0

    .line 661
    :goto_1
    return v0

    .line 658
    :cond_1
    const-string v0, "Obd has been connected so skipping the demo"

    goto :goto_0

    .line 661
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isDemoPaused()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 742
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v2}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPaused()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 745
    :cond_1
    :goto_0
    return v1

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isDemoPlaying()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 733
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v2}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 736
    :cond_1
    :goto_0
    return v1

    .line 734
    :catch_0
    move-exception v0

    .line 735
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isDemoStopped()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 751
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isStopped()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    invoke-interface {v2}, Lcom/navdy/hud/app/debug/IRouteRecorder;->isStopped()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 754
    :cond_0
    :goto_0
    return v1

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public load()V
    .locals 4

    .prologue
    .line 643
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Loading the Obd Data recorder"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 646
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "demo_log.log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 647
    .local v0, "demoLogFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoRouteLogFileExists:Z

    .line 648
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Demo file exists : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoRouteLogFileExists:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 651
    new-instance v0, Ljava/io/File;

    .end local v0    # "demoLogFile":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "demo_log.log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".obd"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 652
    .restart local v0    # "demoLogFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoObdLogFileExists:Z

    .line 653
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Demo obd file exists : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->demoObdLogFileExists:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 654
    return-void
.end method

.method public onCalibratedGForceData(Lcom/navdy/hud/app/device/gps/CalibratedGForceData;)V
    .locals 4
    .param p1, "calibratedGForceData"    # Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 543
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->gforceDataBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 545
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->gforceDataBuilder:Ljava/lang/StringBuilder;

    .line 546
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "G,"

    .line 547
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 548
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getXAccel()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 549
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getYAccel()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 550
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->getZAccel()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    .line 551
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 545
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 553
    :cond_0
    return-void
.end method

.method public onDriveScoreUpdatedEvent(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V
    .locals 4
    .param p1, "driveScoreUpdated"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 557
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->gforceDataBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 559
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->gforceDataBuilder:Ljava/lang/StringBuilder;

    .line 560
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "D,"

    .line 561
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DS:"

    .line 562
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getDriveScore()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DE:"

    .line 563
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;->getInterestingEvent()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SD:"

    .line 564
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "RD:"

    .line 565
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionRollingDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SPD:"

    .line 566
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSpeedingDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ESD:"

    .line 567
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getExcessiveSpeedingDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "HA:"

    .line 568
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardAccelerationCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "HB:"

    .line 569
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHardBrakingCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "HG:"

    .line 570
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/analytics/TelemetrySession;->INSTANCE:Lcom/navdy/hud/app/analytics/TelemetrySession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/analytics/TelemetrySession;->getSessionHighGCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LT:"

    .line 571
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/debug/DriveRecorder;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LN:"

    .line 572
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/debug/DriveRecorder;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    .line 573
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 559
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->write(Ljava/lang/String;)V

    .line 576
    :cond_0
    return-void
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 508
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 509
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->STOP:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {p0, v0, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x35

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 511
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESTART:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {p0, v0, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    goto :goto_0

    .line 512
    :cond_2
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x2c

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 513
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PRELOAD:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {p0, v0, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    goto :goto_0
.end method

.method public onMapEngineInitialized(Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;)V
    .locals 3
    .param p1, "mapEngineInitializedEvent"    # Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 601
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMapEnigneInitialzed : message received. Initialized :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;->initialized:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 602
    iget-boolean v0, p1, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;->initialized:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isEngineInitialized:Z

    .line 603
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->checkAndStartAutoRecording()V

    .line 604
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 97
    invoke-static {p2}, Lcom/navdy/hud/app/debug/IRouteRecorder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/hud/app/debug/IRouteRecorder;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    .line 98
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;

    .line 103
    return-void
.end method

.method public onStartPlayback(Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;)V
    .locals 2
    .param p1, "startPlayback"    # Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 531
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Start playback event received"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 532
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->label:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->startPlayback(Ljava/lang/String;Z)V

    .line 533
    return-void
.end method

.method public onStartRecordingEvent(Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V
    .locals 2
    .param p1, "startRecordingEvent"    # Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 519
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Start recording event received"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 520
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;->label:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/debug/DriveRecorder;->startRecording(Ljava/lang/String;)V

    .line 521
    return-void
.end method

.method public onStopPlayback(Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;)V
    .locals 2
    .param p1, "stopDrivePlaybackEvent"    # Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 537
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stop playback event received"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 538
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 539
    return-void
.end method

.method public onStopRecordingEvent(Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;)V
    .locals 2
    .param p1, "stopRecordingEvent"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 525
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stop recording event received"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecording()V

    .line 527
    return-void
.end method

.method public pausePlayback()V
    .locals 2

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Playback is not happening so not pausing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 307
    :goto_0
    return-void

    .line 304
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Pausing the playback"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 306
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PAUSED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    goto :goto_0
.end method

.method public performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V
    .locals 3
    .param p1, "action"    # Lcom/navdy/hud/app/debug/DriveRecorder$Action;
    .param p2, "loop"    # Z

    .prologue
    .line 607
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, ":performDemoPlaybackAction"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 608
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isEngineInitialized:Z

    if-nez v0, :cond_0

    .line 609
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Engine is not initialized so can not start the playback"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 640
    :goto_0
    return-void

    .line 612
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/debug/DriveRecorder$8;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public prepare(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Cannot prepare for playback as the playback is in progress"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 238
    :goto_0
    return-void

    .line 228
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder$2;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-virtual {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->stopPlayback()V

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 293
    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->recordedObdData:Ljava/util/List;

    .line 295
    :cond_0
    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileName:Ljava/lang/String;

    .line 296
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->preparedFileLastModifiedTime:J

    .line 297
    return-void
.end method

.method public restartPlayback()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 322
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 324
    iput v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    .line 325
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    const/4 v0, 0x1

    .line 329
    :cond_1
    return v0
.end method

.method public resumePlayback()V
    .locals 2

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 312
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 314
    :cond_0
    return-void
.end method

.method public setRealObdConnected(Z)V
    .locals 3
    .param p1, "realObdConnected"    # Z

    .prologue
    .line 698
    iput-boolean p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->realObdConnected:Z

    .line 699
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRealObdConnected : state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 700
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->realObdConnected:Z

    if-eqz v0, :cond_0

    .line 701
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->STOP:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    .line 703
    :cond_0
    return-void
.end method

.method public startPlayback(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "loop"    # Z

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-eqz v0, :cond_1

    .line 242
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already busy, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 269
    :goto_0
    return-void

    .line 245
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Starting the playback"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$State;->PLAYING:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    .line 247
    iput-boolean p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isLooping:Z

    .line 248
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder$3;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;Ljava/lang/String;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public startRecording(Ljava/lang/String;)V
    .locals 3
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 429
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Obd data recorder is already busy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    :goto_0
    return-void

    .line 432
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Starting the recording"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    .line 434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z

    .line 435
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->createDriveRecord(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public stopPlayback()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 272
    invoke-direct {p0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "already stopped, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 288
    :goto_0
    return-void

    .line 276
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Stopping the playback"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 277
    iput v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->currentObdDataInjectionIndex:I

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 279
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdDataPlaybackHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->injectFakeObdData:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 281
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    .line 283
    .local v0, "ret":Z
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "obd handler thread quit:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 285
    .end local v0    # "ret":Z
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdPlaybackThread:Landroid/os/HandlerThread;

    .line 286
    iput-boolean v4, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isLooping:Z

    .line 287
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$State;->STOPPED:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    iput-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->playbackState:Lcom/navdy/hud/app/debug/DriveRecorder$State;

    goto :goto_0
.end method

.method public stopRecording()V
    .locals 2

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already stopped, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 150
    :goto_0
    return-void

    .line 138
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stopping the Obd recording"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/debug/DriveRecorder$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/debug/DriveRecorder$1;-><init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
