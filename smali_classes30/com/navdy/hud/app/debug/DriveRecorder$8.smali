.class Lcom/navdy/hud/app/debug/DriveRecorder$8;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

.field final synthetic val$action:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field final synthetic val$loop:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 612
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object p2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->val$action:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    iput-boolean p3, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->val$loop:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 615
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->isDemoAvailable()Z

    move-result v0

    .line 616
    .local v0, "available":Z
    if-eqz v0, :cond_0

    .line 617
    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$10;->$SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action:[I

    iget-object v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->val$action:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 619
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->val$loop:Z

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->playDemo(Z)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1000(Lcom/navdy/hud/app/debug/DriveRecorder;Z)V

    goto :goto_0

    .line 622
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->pauseDemo()V
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1100(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    goto :goto_0

    .line 625
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->resumeDemo()V
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1200(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    goto :goto_0

    .line 628
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-boolean v2, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->val$loop:Z

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->restartDemo(Z)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1300(Lcom/navdy/hud/app/debug/DriveRecorder;Z)V

    goto :goto_0

    .line 631
    :pswitch_4
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->stopDemo()V
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1400(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    goto :goto_0

    .line 634
    :pswitch_5
    iget-object v1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$8;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # invokes: Lcom/navdy/hud/app/debug/DriveRecorder;->prepareDemo()V
    invoke-static {v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$1500(Lcom/navdy/hud/app/debug/DriveRecorder;)V

    goto :goto_0

    .line 617
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
