.class Lcom/navdy/hud/app/debug/DriveRecorder$1;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->stopRecording()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$1;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$1;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$002(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$1;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # setter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isSupportedPidsWritten:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$102(Lcom/navdy/hud/app/debug/DriveRecorder;Z)Z

    .line 145
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager;->setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V

    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$1;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->flushAndClose()V

    .line 147
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$1;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->flushAndClose()V

    .line 148
    return-void
.end method
