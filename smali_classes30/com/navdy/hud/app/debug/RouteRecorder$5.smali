.class Lcom/navdy/hud/app/debug/RouteRecorder$5;
.super Ljava/lang/Object;
.source "RouteRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorder;->requestRecordings()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorder;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorder$5;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 212
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v2, "fileNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/debug/RouteRecorder$5;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->pathManager:Lcom/navdy/hud/app/storage/PathManager;
    invoke-static {v5}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1000(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "drive_logs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 217
    .local v3, "subFiles":[Ljava/io/File;
    if-eqz v3, :cond_1

    .line 218
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v3, v4

    .line 219
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".obd"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 225
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/debug/RouteRecorder$5;->this$0:Lcom/navdy/hud/app/debug/RouteRecorder;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorder;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->access$1100(Lcom/navdy/hud/app/debug/RouteRecorder;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v4

    new-instance v5, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;

    invoke-direct {v5, v2}, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;-><init>(Ljava/util/List;)V

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V

    .line 226
    return-void
.end method
