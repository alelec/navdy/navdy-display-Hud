.class public final enum Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;
.super Ljava/lang/Enum;
.source "DriveRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DemoPreference"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

.field public static final enum DISABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

.field public static final enum ENABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

.field public static final enum NA:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    const-string v1, "NA"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->NA:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    .line 127
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->DISABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    .line 128
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->ENABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->NA:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->DISABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->ENABLED:Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-class v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/debug/DriveRecorder$DemoPreference;

    return-object v0
.end method
