.class public Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector;
.super Ljava/lang/Object;
.source "GestureEngine$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/debug/GestureEngine;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/debug/GestureEngine;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00d8

    const-string v2, "method \'onToggleGesture\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/CompoundButton;

    .end local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$1;-><init>(Lcom/navdy/hud/app/debug/GestureEngine;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 20
    const v1, 0x7f0e00da

    const-string v2, "method \'onToggleDiscreteMode\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/CompoundButton;

    .end local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$2;-><init>(Lcom/navdy/hud/app/debug/GestureEngine;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 30
    const v1, 0x7f0e00d9

    const-string v2, "method \'onTogglePreview\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/CompoundButton;

    .end local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/debug/GestureEngine$$ViewInjector$3;-><init>(Lcom/navdy/hud/app/debug/GestureEngine;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 40
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/debug/GestureEngine;)V
    .locals 0
    .param p0, "target"    # Lcom/navdy/hud/app/debug/GestureEngine;

    .prologue
    .line 43
    return-void
.end method
