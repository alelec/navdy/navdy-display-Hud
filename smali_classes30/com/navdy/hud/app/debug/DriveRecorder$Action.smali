.class public final enum Lcom/navdy/hud/app/debug/DriveRecorder$Action;
.super Ljava/lang/Enum;
.source "DriveRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/debug/DriveRecorder$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum PAUSE:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum PLAY:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum PRELOAD:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum RESTART:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum RESUME:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

.field public static final enum STOP:Lcom/navdy/hud/app/debug/DriveRecorder$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 117
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "PRELOAD"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PRELOAD:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 118
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PLAY:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 119
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PAUSE:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 120
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "RESUME"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESUME:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 121
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "RESTART"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESTART:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 122
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    const-string v1, "STOP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->STOP:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    .line 116
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PRELOAD:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PLAY:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PAUSE:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESUME:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESTART:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->STOP:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/debug/DriveRecorder$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    const-class v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/debug/DriveRecorder$Action;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->$VALUES:[Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/debug/DriveRecorder$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    return-object v0
.end method
