.class Lcom/navdy/hud/app/debug/DriveRecorder$4;
.super Ljava/lang/Object;
.source "DriveRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DriveRecorder;->flushRecordings()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DriveRecorder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DriveRecorder$4;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$4;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    # getter for: Lcom/navdy/hud/app/debug/DriveRecorder;->isRecording:Z
    invoke-static {v0}, Lcom/navdy/hud/app/debug/DriveRecorder;->access$000(Lcom/navdy/hud/app/debug/DriveRecorder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$4;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->obdAsyncBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->flush()V

    .line 338
    iget-object v0, p0, Lcom/navdy/hud/app/debug/DriveRecorder$4;->this$0:Lcom/navdy/hud/app/debug/DriveRecorder;

    iget-object v0, v0, Lcom/navdy/hud/app/debug/DriveRecorder;->driveScoreDataBufferedFileWriter:Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;->flush()V

    .line 340
    :cond_0
    return-void
.end method
