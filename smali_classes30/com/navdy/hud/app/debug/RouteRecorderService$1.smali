.class Lcom/navdy/hud/app/debug/RouteRecorderService$1;
.super Lcom/navdy/hud/app/debug/IRouteRecorder$Stub;
.source "RouteRecorderService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/RouteRecorderService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/RouteRecorderService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/RouteRecorderService;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    invoke-direct {p0}, Lcom/navdy/hud/app/debug/IRouteRecorder$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public isPaused()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPaused()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording()Z

    move-result v0

    return v0
.end method

.method public isStopped()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isStopped()Z

    move-result v0

    return v0
.end method

.method public pausePlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->pausePlayback()V

    .line 47
    return-void
.end method

.method public prepare(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/debug/RouteRecorder;->prepare(Ljava/lang/String;Z)V

    .line 22
    return-void
.end method

.method public restartPlayback()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->restartPlayback()Z

    move-result v0

    return v0
.end method

.method public resumePlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->resumePlayback()V

    .line 52
    return-void
.end method

.method public startPlayback(Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "secondary"    # Z
    .param p3, "loop"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->startPlayback(Ljava/lang/String;ZZZ)V

    .line 42
    return-void
.end method

.method public startRecording(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->startRecording(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public stopPlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopPlayback()V

    .line 57
    return-void
.end method

.method public stopRecording()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/debug/RouteRecorderService$1;->this$0:Lcom/navdy/hud/app/debug/RouteRecorderService;

    # getter for: Lcom/navdy/hud/app/debug/RouteRecorderService;->mRouteRecorder:Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/debug/RouteRecorderService;->access$000(Lcom/navdy/hud/app/debug/RouteRecorderService;)Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/debug/RouteRecorder;->stopRecording(Z)V

    .line 32
    return-void
.end method
