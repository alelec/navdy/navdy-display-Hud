.class Lcom/navdy/hud/app/debug/DebugReceiver$8;
.super Ljava/lang/Object;
.source "DebugReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/debug/DebugReceiver;->dnsLookupTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/debug/DebugReceiver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/debug/DebugReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/debug/DebugReceiver;

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/navdy/hud/app/debug/DebugReceiver$8;->this$0:Lcom/navdy/hud/app/debug/DebugReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1273
    const/16 v5, 0xa

    :try_start_0
    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "version.hybrid.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x1

    const-string v6, "reverse.geocoder.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x2

    const-string v6, "tpeg.traffic.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x3

    const-string v6, "tpeg.hybrid.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x4

    const-string v6, "download.hybrid.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x5

    const-string v6, "v102-62-30-8.route.hybrid.api.here.com"

    aput-object v6, v1, v5

    const/4 v5, 0x6

    const-string v6, "analytics.localytics.com"

    aput-object v6, v1, v5

    const/4 v5, 0x7

    const-string v6, "profile.localytics.com"

    aput-object v6, v1, v5

    const/16 v5, 0x8

    const-string v6, "sdk.hockeyapp.net"

    aput-object v6, v1, v5

    const/16 v5, 0x9

    const-string v6, "navdyhud.atlassian.net"

    aput-object v6, v1, v5

    .line 1286
    .local v1, "hosts":[Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "dns_lookup_test"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1287
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-ge v2, v5, :cond_2

    .line 1289
    :try_start_1
    aget-object v5, v1, v2

    invoke-static {v5}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    .line 1290
    .local v0, "addresses":[Ljava/net/InetAddress;
    if-eqz v0, :cond_0

    .line 1291
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dns_lookup_test["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] addresses = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1292
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v5, v0

    if-ge v3, v5, :cond_1

    .line 1293
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dns_lookup_test["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]  addr["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v3

    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1292
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1296
    .end local v3    # "j":I
    :cond_0
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dns_lookup_test["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] no address"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1287
    .end local v0    # "addresses":[Ljava/net/InetAddress;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1298
    :catch_0
    move-exception v4

    .line 1299
    .local v4, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "dns_lookup_test"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 1302
    .end local v1    # "hosts":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v4    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v4

    .line 1303
    .restart local v4    # "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/debug/DebugReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 1305
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_2
    return-void
.end method
