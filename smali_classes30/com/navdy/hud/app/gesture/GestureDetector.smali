.class public Lcom/navdy/hud/app/gesture/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;
    }
.end annotation


# static fields
.field private static final FOV_RANGE:F = 240.0f

.field private static final FULL_RANGE:F = 640.0f


# instance fields
.field public defaultOffset:F

.field public defaultSensitivity:F

.field protected lastEvent:Lcom/navdy/service/library/events/input/GestureEvent;

.field private listener:Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;

.field protected offset:F

.field protected origin:I

.field protected pinchDuration:I

.field protected startEvent:Lcom/navdy/service/library/events/input/GestureEvent;

.field useRelativeOrigin:Z

.field protected useRelativePosition:Z


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->useRelativePosition:Z

    .line 30
    iput-boolean v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->useRelativeOrigin:Z

    .line 35
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->defaultOffset:F

    .line 43
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->defaultSensitivity:F

    .line 45
    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->pinchDuration:I

    .line 48
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->listener:Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;

    .line 49
    return-void
.end method

.method private calculatePosition(Lcom/navdy/service/library/events/input/GestureEvent;)F
    .locals 8
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 52
    if-nez p1, :cond_0

    .line 53
    const/high16 v2, -0x40800000    # -1.0f

    .line 60
    :goto_0
    return v2

    .line 55
    :cond_0
    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 56
    .local v1, "x":I
    iget-boolean v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->useRelativePosition:Z

    if-eqz v2, :cond_1

    .line 57
    iget v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    iget v3, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->origin:I

    sub-int v3, v1, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->defaultSensitivity:F

    const/high16 v5, 0x42f00000    # 120.0f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {p0, v2, v6, v7}, Lcom/navdy/hud/app/gesture/GestureDetector;->clamp(FFF)F

    move-result v2

    goto :goto_0

    .line 59
    :cond_1
    const/high16 v0, 0x43480000    # 200.0f

    .line 60
    .local v0, "margin":F
    int-to-float v2, v1

    sub-float/2addr v2, v0

    const/high16 v3, 0x43700000    # 240.0f

    div-float/2addr v2, v3

    invoke-direct {p0, v2, v6, v7}, Lcom/navdy/hud/app/gesture/GestureDetector;->clamp(FFF)F

    move-result v2

    goto :goto_0
.end method

.method private clamp(FFF)F
    .locals 1
    .param p1, "value"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    .line 91
    invoke-static {p2, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private hasPosition(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 65
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private trackHand(Lcom/navdy/service/library/events/input/GestureEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/GestureDetector;->hasPosition(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->startEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    if-nez v2, :cond_2

    .line 75
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->startEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    .line 76
    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 77
    .local v1, "xPos":I
    iget v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 80
    iget v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->defaultOffset:F

    iput v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    .line 82
    :cond_1
    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->origin:I

    .line 85
    .end local v1    # "xPos":I
    :cond_2
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/GestureDetector;->calculatePosition(Lcom/navdy/service/library/events/input/GestureEvent;)F

    move-result v0

    .line 87
    .local v0, "normalized":F
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->listener:Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;->onTrackHand(F)V

    goto :goto_0
.end method


# virtual methods
.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v3, 0x1

    .line 95
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 96
    .local v0, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureDetector$1;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 136
    :cond_0
    :goto_0
    :pswitch_0
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->lastEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    .line 137
    return v3

    .line 104
    :pswitch_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->useRelativeOrigin:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->lastEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/gesture/GestureDetector;->hasPosition(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->lastEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/gesture/GestureDetector;->calculatePosition(Lcom/navdy/service/library/events/input/GestureEvent;)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    .line 111
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->startEvent:Lcom/navdy/service/library/events/input/GestureEvent;

    goto :goto_0

    .line 108
    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->offset:F

    goto :goto_1

    .line 115
    :pswitch_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->pinchDuration:I

    goto :goto_0

    .line 119
    :pswitch_3
    iget v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->pinchDuration:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->pinchDuration:I

    .line 120
    iget v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->pinchDuration:I

    if-ne v1, v3, :cond_0

    .line 121
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureDetector;->listener:Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;

    invoke-interface {v1}, Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;->onClick()V

    goto :goto_0

    .line 129
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/GestureDetector;->trackHand(Lcom/navdy/service/library/events/input/GestureEvent;)V

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
