.class Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;
.super Ljava/lang/Object;
.source "GestureServiceConnector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/gesture/GestureServiceConnector;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 183
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$000(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 185
    :try_start_0
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "creating socket"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    new-instance v3, Landroid/net/LocalSocket;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/net/LocalSocket;-><init>(I)V

    # setter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$202(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;

    .line 188
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "bind socket"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$200(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Landroid/net/LocalSocket;

    move-result-object v2

    new-instance v3, Landroid/net/LocalSocketAddress;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->bind(Landroid/net/LocalSocketAddress;)V

    .line 191
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "calling communicateWithSwipeDaemon"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iget-object v3, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;
    invoke-static {v3}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$200(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Landroid/net/LocalSocket;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->communicateWithSwipeDaemon(Landroid/net/LocalSocket;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$300(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Landroid/net/LocalSocket;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    move-result-object v0

    .line 193
    .local v0, "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "called communicateWithSwipeDaemon"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 195
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$000(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # invokes: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeSocket()V
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$400(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V

    .line 199
    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector$7;->$SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->ordinal()I

    move-result v3

    aget v2, v2, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 223
    :cond_0
    :goto_1
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "swipedconnector connect thread exit"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :pswitch_0
    :try_start_1
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "communicateWithSwipeDaemon  cannot connect"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 203
    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 217
    .end local v0    # "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    :catch_0
    move-exception v1

    .line 218
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 219
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$000(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    const/16 v2, 0x1388

    invoke-static {v2}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 223
    :cond_1
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "swipedconnector connect thread exit"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 207
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v0    # "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    :pswitch_1
    :try_start_3
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "communicateWithSwipeDaemon  communication lost, restart"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # invokes: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->reStart()V
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$500(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 223
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "swipedconnector connect thread exit"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    .end local v0    # "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    :cond_2
    :goto_2
    return-void

    .line 212
    .restart local v0    # "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    :pswitch_2
    :try_start_4
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "communicateWithSwipeDaemon  swiped restarted, restart"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 213
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # invokes: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->reStart()V
    invoke-static {v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$500(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 223
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "swipedconnector connect thread exit"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .end local v0    # "error":Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    :catchall_0
    move-exception v2

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "swipedconnector connect thread exit"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    throw v2

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
