.class public Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;
.super Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;
.source "FlickTurnGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;
    }
.end annotation


# static fields
.field private static final FLICKABLE_EVENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLICK_EVENT_COUNT:I = 0x4

.field private static final FLICK_PAIRED_EVENT_TIMEOUT_MS:I = 0x1f4

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private consecutiveTurnEventCount:S

.field private listener:Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

.field private previousKeyEvent:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field private previousKeyEventTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->FLICKABLE_EVENTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    .prologue
    .line 37
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;-><init>(ILcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEvent:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEventTime:J

    .line 33
    const/4 v0, 0x0

    iput-short v0, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->consecutiveTurnEventCount:S

    .line 38
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

    if-nez v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 41
    :cond_1
    check-cast p1, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

    .end local p1    # "listener":Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->listener:Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

    .line 42
    return-void
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 52
    .local v0, "result":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 54
    .local v2, "time":J
    sget-object v1, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->FLICKABLE_EVENTS:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEvent:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-ne v1, p1, :cond_3

    iget-wide v4, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEventTime:J

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 56
    iget-short v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->consecutiveTurnEventCount:S

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->consecutiveTurnEventCount:S

    const/4 v4, 0x4

    if-ne v1, v4, :cond_0

    .line 57
    sget-object v1, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Combined key event recognized from a single "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 58
    const/4 v1, 0x0

    iput-short v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->consecutiveTurnEventCount:S

    .line 59
    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->listener:Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

    invoke-interface {v1}, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;->onFlickLeft()V

    .line 71
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEvent:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 72
    iput-wide v2, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->previousKeyEventTime:J

    .line 73
    return v0

    .line 61
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->listener:Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;

    invoke-interface {v1}, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector$IFlickTurnKeyGesture;->onFlickRight()V

    goto :goto_0

    .line 64
    :cond_2
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Flick for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not handled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 68
    :cond_3
    const/4 v1, 0x1

    iput-short v1, p0, Lcom/navdy/hud/app/gesture/FlickTurnGestureDetector;->consecutiveTurnEventCount:S

    goto :goto_0
.end method
