.class Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;
.super Ljava/lang/Object;
.source "GestureServiceConnector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/gesture/GestureServiceConnector;->onInputPreferenceUpdate(Lcom/navdy/service/library/events/preferences/InputPreferences;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

.field final synthetic val$event:Lcom/navdy/service/library/events/preferences/InputPreferences;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Lcom/navdy/service/library/events/preferences/InputPreferences;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 577
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object p2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->val$event:Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->val$event:Lcom/navdy/service/library/events/preferences/InputPreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;
    invoke-static {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$200(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Landroid/net/LocalSocket;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 582
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "already running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 593
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->start()V

    goto :goto_0

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;
    invoke-static {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$200(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Landroid/net/LocalSocket;

    move-result-object v0

    if-nez v0, :cond_2

    .line 588
    # getter for: Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "not running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 591
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;->this$0:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->stop()V

    goto :goto_0
.end method
