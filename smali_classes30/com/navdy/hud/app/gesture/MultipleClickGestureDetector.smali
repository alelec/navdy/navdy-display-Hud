.class public Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;
.super Ljava/lang/Object;
.source "MultipleClickGestureDetector.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;
    }
.end annotation


# static fields
.field public static final DOUBLE_CLICK:I = 0x2

.field private static final MULTIPLE_CLICK_THRESHOLD:I = 0x190

.field private static final MULTIPLIABLE_EVENT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final TRIPLE_CLICK:I = 0x3


# instance fields
.field private final handler:Landroid/os/Handler;

.field private listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

.field private maxAcceptableClickCount:I

.field private multipleClickCount:I

.field private final multipleClickTrigger:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    sput-object v0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->MULTIPLIABLE_EVENT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    return-void
.end method

.method public constructor <init>(ILcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V
    .locals 2
    .param p1, "maxAcceptableClickCount"    # I
    .param p2, "listener"    # Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->handler:Landroid/os/Handler;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$1;-><init>(Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;)V

    iput-object v0, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickTrigger:Ljava/lang/Runnable;

    .line 42
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    if-gt p1, v0, :cond_1

    .line 43
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 46
    :cond_1
    iput p1, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->maxAcceptableClickCount:I

    .line 47
    iput-object p2, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;
    .param p1, "x1"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->processEvent(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized processEvent(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 8
    .param p1, "receivedEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 71
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickTrigger:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 73
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 119
    :goto_0
    :pswitch_0
    monitor-exit p0

    return v1

    .line 77
    :cond_0
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->MULTIPLIABLE_EVENT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v3, p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 79
    .local v0, "isSelectEvent":Z
    if-eqz v0, :cond_1

    .line 80
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    .line 86
    :cond_1
    :goto_1
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    iget v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->maxAcceptableClickCount:I

    if-lt v3, v4, :cond_2

    .line 88
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    iget v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->maxAcceptableClickCount:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;->onMultipleClick(I)V

    .line 89
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    iget v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->maxAcceptableClickCount:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 71
    .end local v0    # "isSelectEvent":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 92
    .restart local v0    # "isSelectEvent":Z
    :cond_2
    if-eqz v0, :cond_4

    .line 93
    :try_start_2
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    if-lez v3, :cond_3

    .line 95
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickTrigger:Ljava/lang/Runnable;

    const-wide/16 v6, 0x190

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    :goto_2
    move v1, v2

    .line 119
    goto :goto_0

    .line 98
    :cond_4
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    if-le v3, v2, :cond_6

    .line 102
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    iget v4, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;->onMultipleClick(I)V

    .line 106
    :cond_5
    :goto_3
    const/4 v3, 0x0

    iput v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    .line 109
    if-eqz p1, :cond_3

    .line 110
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    invoke-interface {v3, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v1

    .line 111
    .local v1, "res":Z
    sget-object v3, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 103
    .end local v1    # "res":Z
    :cond_6
    iget v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->multipleClickCount:I

    if-ne v3, v2, :cond_5

    .line 104
    iget-object v3, p0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->listener:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;

    sget-object v4, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->MULTIPLIABLE_EVENT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->processEvent(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method
