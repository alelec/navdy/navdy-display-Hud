.class public Lcom/navdy/hud/app/gesture/GestureServiceConnector;
.super Ljava/lang/Object;
.source "GestureServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;,
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;,
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;,
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;,
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$TakeSnapshot;,
        Lcom/navdy/hud/app/gesture/GestureServiceConnector$ConnectedEvent;
    }
.end annotation


# static fields
.field private static final AUTO_EXPOSURE_OFF:Ljava/lang/String; = "autoexpo off"

.field private static final AUTO_EXPOSURE_ON:Ljava/lang/String; = "autoexpo on"

.field private static final CALIBRATE_MASK:Ljava/lang/String; = "calibrate-mask"

.field private static final CALIBRATE_MASK_FORCE:Ljava/lang/String; = "calibrate-mask-force"

.field private static final CALIBRATE_SAVE_SNAPSHOT_ON:Ljava/lang/String; = "calibrate-save-snapshot on"

.field private static final DEV_SOCKET_SWIPED:Ljava/lang/String; = "/dev/socket/swiped"

.field private static final EXPOSURE:Ljava/lang/String; = "exposure"

.field private static final FORMAT:Ljava/lang/String; = "format"

.field private static final GAIN:Ljava/lang/String; = "gain"

.field public static final GESTURE_ENABLED:Ljava/lang/String; = "gesture.enabled"

.field public static final GESTURE_LEFT:Ljava/lang/String; = "gesture left"

.field public static final GESTURE_RIGHT:Ljava/lang/String; = "gesture right"

.field public static final GESTURE_VERSION:Ljava/lang/String; = "persist.sys.gesture.version"

.field public static final LEFT:Ljava/lang/String; = "left"

.field public static final LEFT_FIRST_CHAR:C = 'l'

.field public static final MAX_RECORD_TIME:I = 0x1770

.field private static final MINIMUM_INTERVAL_BETWEEN_SNAPSHOTS:I = 0x1388

.field private static final NOTIFY:Ljava/lang/String; = "notify"

.field private static final PID_CHECK_INTERVAL:I = 0x2710

.field public static final RECORDING_SAVED:Ljava/lang/String; = "recording-saved:"

.field public static final RECORD_SAVE:Ljava/lang/String; = "record save"

.field public static final RIGHT:Ljava/lang/String; = "right"

.field public static final RIGHT_FIRST_CHAR:C = 'r'

.field public static final SNAPSHOT:Ljava/lang/String; = "snapshot "

.field public static final SNAPSHOT_PATH:Ljava/lang/String; = "/data/misc/swiped/camera.png"

.field private static final SOCKET_NAME:Ljava/lang/String; = "swiped"

.field private static final SO_TIMEOUT:I = 0x7d0

.field public static final SWIPED_DISABLE_CALIBRATION:Ljava/lang/String; = "calibration pause"

.field public static final SWIPED_ENABLE_CALIBRATION:Ljava/lang/String; = "calibration resume"

.field private static final SWIPED_PROCESS_NAME:Ljava/lang/String; = "/system/xbin/swiped"

.field public static final SWIPED_RECORD_MODE_ONE_SHOT:Ljava/lang/String; = "record-mode oneshot"

.field public static final SWIPED_RECORD_MODE_ROLLING:Ljava/lang/String; = "record-mode rolling"

.field public static final SWIPED_START_RECORDING:Ljava/lang/String; = "record on"

.field public static final SWIPED_STOP_RECORDING:Ljava/lang/String; = "record off"

.field public static final SWIPE_PROGRESS:Ljava/lang/String; = "swipe-progress:"

.field public static final SWIPE_PROGRESS_COMMAND_DATA_OFFSET:I

.field public static final SWIPE_PROGRESS_LEFT:Ljava/lang/String; = "swipe-progress: left,"

.field public static final SWIPE_PROGRESS_RIGHT:Ljava/lang/String; = "swipe-progress: right,"

.field public static final SWIPE_PROGRESS_UNKNOWN:Ljava/lang/String; = "swipe-progress: unknown,"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"

.field private static final encoder:Ljava/nio/charset/CharsetEncoder;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final temp:[B


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private lastSnapshotTime:J

.field private powerManager:Lcom/navdy/hud/app/device/PowerManager;

.field private recordingPath:Ljava/lang/String;

.field private volatile running:Z

.field private shuttingDown:Z

.field private volatile swipedReader:Ljava/lang/Thread;

.field private volatile swipedSocket:Landroid/net/LocalSocket;

.field private vin:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 98
    const-string v0, "swipe-progress:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->SWIPE_PROGRESS_COMMAND_DATA_OFFSET:I

    .line 409
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->encoder:Ljava/nio/charset/CharsetEncoder;

    .line 410
    const/16 v0, 0x400

    new-array v0, v0, [B

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->temp:[B

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->lastSnapshotTime:J

    .line 519
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->vin:Ljava/lang/String;

    .line 150
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    .line 151
    iput-object p2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 152
    invoke-virtual {p1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    return v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)Landroid/net/LocalSocket;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .param p1, "x1"    # Landroid/net/LocalSocket;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Landroid/net/LocalSocket;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .param p1, "x1"    # Landroid/net/LocalSocket;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->communicateWithSwipeDaemon(Landroid/net/LocalSocket;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeSocket()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->reStart()V

    return-void
.end method

.method private closeSocket()V
    .locals 4

    .prologue
    .line 611
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    if-eqz v2, :cond_0

    .line 612
    const/4 v0, 0x0

    .line 614
    .local v0, "ip":Ljava/io/InputStream;
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 617
    :goto_0
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 619
    const/4 v1, 0x0

    .line 621
    .local v1, "op":Ljava/io/OutputStream;
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 624
    :goto_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 626
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 627
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    .line 628
    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "swipedconnector socket closed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 630
    .end local v0    # "ip":Ljava/io/InputStream;
    .end local v1    # "op":Ljava/io/OutputStream;
    :cond_0
    return-void

    .line 622
    .restart local v0    # "ip":Ljava/io/InputStream;
    .restart local v1    # "op":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    goto :goto_1

    .line 615
    .end local v1    # "op":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method private closeThread()V
    .locals 2

    .prologue
    .line 633
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 634
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "swipedconnector thread alive"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 638
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "swipedconnector thread waiting"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 640
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "swipedconnector thread waited"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 646
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    .line 648
    :cond_1
    return-void

    .line 641
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private communicateWithSwipeDaemon(Landroid/net/LocalSocket;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    .locals 33
    .param p1, "sock"    # Landroid/net/LocalSocket;

    .prologue
    .line 261
    const/4 v7, 0x0

    .line 265
    .local v7, "connected":Z
    :try_start_0
    new-instance v28, Landroid/net/LocalSocketAddress;

    const-string v29, "swiped"

    sget-object v30, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct/range {v28 .. v30}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 267
    const-string v28, "notify"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 268
    const/4 v7, 0x1

    .line 271
    const-string v28, "calibration resume"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 272
    const-string v28, "record off"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 273
    const-string v28, "record-mode rolling"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    move/from16 v28, v0

    if-nez v28, :cond_0

    .line 288
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "swipedconnector is not running"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 289
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->CANNOT_CONNECT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    .line 406
    :goto_1
    return-object v28

    .line 274
    :catch_0
    move-exception v10

    .line 275
    .local v10, "e":Ljava/lang/Throwable;
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "socket connect to reserved namespace failed"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    :try_start_1
    new-instance v28, Landroid/net/LocalSocketAddress;

    const-string v29, "/dev/socket/swiped"

    sget-object v30, Landroid/net/LocalSocketAddress$Namespace;->FILESYSTEM:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct/range {v28 .. v30}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 280
    const-string v28, "notify"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 281
    const/4 v7, 0x1

    goto :goto_0

    .line 282
    :catch_1
    move-exception v11

    .line 283
    .local v11, "e1":Ljava/lang/Throwable;
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "socket connect to filesystem namespace failed"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 292
    .end local v10    # "e":Ljava/lang/Throwable;
    .end local v11    # "e1":Ljava/lang/Throwable;
    :cond_0
    if-nez v7, :cond_1

    .line 293
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "swipedconnector could not connect"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 294
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->CANNOT_CONNECT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    goto :goto_1

    .line 298
    :cond_1
    :try_start_2
    const-string v28, "/system/xbin/swiped"

    invoke-static/range {v28 .. v28}, Lcom/navdy/service/library/util/SystemUtils;->getNativeProcessId(Ljava/lang/String;)I

    move-result v25

    .line 299
    .local v25, "swipedPid":I
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "swipedconnector connected"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/hud/app/gesture/GestureServiceConnector$ConnectedEvent;

    invoke-direct/range {v29 .. v29}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$ConnectedEvent;-><init>()V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 301
    invoke-virtual/range {p1 .. p1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    .line 303
    .local v15, "ins":Ljava/io/InputStream;
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "read starting"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 304
    const/16 v28, 0x3e8

    move/from16 v0, v28

    new-array v6, v0, [B

    .line 305
    .local v6, "buf":[B
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "setting timeout"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    move-object/from16 v28, v0

    const/16 v29, 0x7d0

    invoke-virtual/range {v28 .. v29}, Landroid/net/LocalSocket;->setSoTimeout(I)V

    .line 307
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "set timeout"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 309
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 310
    .local v18, "lastPidCheckTime":J
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "swipedconnector pid  ["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 312
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    move/from16 v28, v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    if-eqz v28, :cond_3

    .line 315
    :try_start_3
    invoke-virtual {v15, v6}, Ljava/io/InputStream;->read([B)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    move-result v5

    .line 339
    .local v5, "br":I
    if-gez v5, :cond_6

    .line 340
    :try_start_4
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "swipedconnector closed the socket"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->COMMUNICATION_LOST:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    goto/16 :goto_1

    .line 316
    .end local v5    # "br":I
    :catch_2
    move-exception v16

    .line 317
    .local v16, "io":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    move/from16 v28, v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    if-nez v28, :cond_4

    .line 406
    .end local v6    # "buf":[B
    .end local v15    # "ins":Ljava/io/InputStream;
    .end local v16    # "io":Ljava/io/IOException;
    .end local v18    # "lastPidCheckTime":J
    .end local v25    # "swipedPid":I
    :cond_3
    :goto_3
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->COMMUNICATION_LOST:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    goto/16 :goto_1

    .line 320
    .restart local v6    # "buf":[B
    .restart local v15    # "ins":Ljava/io/InputStream;
    .restart local v16    # "io":Ljava/io/IOException;
    .restart local v18    # "lastPidCheckTime":J
    .restart local v25    # "swipedPid":I
    :cond_4
    :try_start_5
    const-string v28, "Try again"

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 323
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 324
    .local v8, "clockTime":J
    sub-long v28, v8, v18

    const-wide/16 v30, 0x2710

    cmp-long v28, v28, v30

    if-lez v28, :cond_2

    .line 325
    const-string v28, "/system/xbin/swiped"

    invoke-static/range {v28 .. v28}, Lcom/navdy/service/library/util/SystemUtils;->getNativeProcessId(Ljava/lang/String;)I

    move-result v22

    .line 326
    .local v22, "pid":I
    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_5

    .line 327
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "swipedconnector pid has changed from ["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "] to ["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 328
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->SWIPED_RESTARTED:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    goto/16 :goto_1

    .line 330
    :cond_5
    move-wide/from16 v18, v8

    goto :goto_2

    .line 344
    .end local v8    # "clockTime":J
    .end local v16    # "io":Ljava/io/IOException;
    .end local v22    # "pid":I
    .restart local v5    # "br":I
    :cond_6
    const/16 v28, 0x1

    move/from16 v0, v28

    if-le v5, v0, :cond_2

    .line 345
    new-instance v24, Ljava/lang/String;

    const/16 v28, 0x0

    add-int/lit8 v29, v5, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v6, v1, v2}, Ljava/lang/String;-><init>([BII)V

    .line 346
    .local v24, "s":Ljava/lang/String;
    const/16 v28, -0x1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->hashCode()I

    move-result v29

    sparse-switch v29, :sswitch_data_0

    :cond_7
    :goto_4
    packed-switch v28, :pswitch_data_0

    .line 354
    const-string v28, "swipe-progress:"

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_8

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v28

    sget v29, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->SWIPE_PROGRESS_COMMAND_DATA_OFFSET:I

    add-int/lit8 v29, v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_8

    .line 355
    sget v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->SWIPE_PROGRESS_COMMAND_DATA_OFFSET:I

    add-int/lit8 v28, v28, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    move-result v13

    .line 356
    .local v13, "firstChar":C
    sparse-switch v13, :sswitch_data_1

    goto/16 :goto_2

    .line 359
    :sswitch_0
    :try_start_6
    const-string v28, "swipe-progress: left,"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Float;->floatValue()F

    move-result v23

    .line 360
    .local v23, "progress":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;

    sget-object v30, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->LEFT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;F)V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_2

    .line 361
    .end local v23    # "progress":F
    :catch_3
    move-exception v26

    .line 362
    .local v26, "t":Ljava/lang/Throwable;
    :try_start_7
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Error parsing the progress "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_2

    .line 401
    .end local v5    # "br":I
    .end local v6    # "buf":[B
    .end local v13    # "firstChar":C
    .end local v15    # "ins":Ljava/io/InputStream;
    .end local v18    # "lastPidCheckTime":J
    .end local v24    # "s":Ljava/lang/String;
    .end local v25    # "swipedPid":I
    .end local v26    # "t":Ljava/lang/Throwable;
    :catch_4
    move-exception v10

    .line 402
    .restart local v10    # "e":Ljava/lang/Throwable;
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v29, "swipedconnector"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 346
    .end local v10    # "e":Ljava/lang/Throwable;
    .restart local v5    # "br":I
    .restart local v6    # "buf":[B
    .restart local v15    # "ins":Ljava/io/InputStream;
    .restart local v18    # "lastPidCheckTime":J
    .restart local v24    # "s":Ljava/lang/String;
    .restart local v25    # "swipedPid":I
    :sswitch_1
    :try_start_8
    const-string v29, "gesture left"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    const/16 v28, 0x0

    goto/16 :goto_4

    :sswitch_2
    const-string v29, "gesture right"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    const/16 v28, 0x1

    goto/16 :goto_4

    .line 348
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/service/library/events/input/GestureEvent;

    sget-object v30, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    const/16 v31, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    const/16 v32, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    invoke-direct/range {v29 .. v32}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 351
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/service/library/events/input/GestureEvent;

    sget-object v30, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    const/16 v31, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    const/16 v32, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    invoke-direct/range {v29 .. v32}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_2

    .line 367
    .restart local v13    # "firstChar":C
    :sswitch_3
    :try_start_9
    const-string v28, "swipe-progress: right,"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Float;->floatValue()F

    move-result v23

    .line 368
    .restart local v23    # "progress":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;

    sget-object v30, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->RIGHT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureProgress;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;F)V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_2

    .line 369
    .end local v23    # "progress":F
    :catch_5
    move-exception v26

    .line 370
    .restart local v26    # "t":Ljava/lang/Throwable;
    :try_start_a
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Error parsing the progress "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 376
    .end local v13    # "firstChar":C
    .end local v26    # "t":Ljava/lang/Throwable;
    :cond_8
    const-string v28, "recording-saved:"

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 377
    const-string v28, "recording-saved:"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->recordingPath:Ljava/lang/String;

    .line 378
    sget-object v28, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "recording saved at "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->recordingPath:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 379
    monitor-enter p0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4

    .line 380
    :try_start_b
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    .line 381
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 382
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v28, v0

    new-instance v29, Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->recordingPath:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-direct/range {v29 .. v30}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$RecordingSaved;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v28 .. v29}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_4

    goto/16 :goto_2

    .line 381
    :catchall_0
    move-exception v28

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :try_start_e
    throw v28

    .line 383
    :cond_9
    const-string v28, "swiped event: "

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 384
    const-string v28, " "

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 385
    .local v20, "parts":[Ljava/lang/String;
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x3

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_2

    .line 386
    const/16 v28, 0x2

    aget-object v12, v20, v28

    .line 387
    .local v12, "eventName":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v4, "args":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v14, 0x3

    .local v14, "i":I
    :goto_5
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v14, v0, :cond_a

    .line 389
    aget-object v28, v20, v14

    const-string v29, "="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 390
    .local v21, "parts2":[Ljava/lang/String;
    const/16 v28, 0x0

    aget-object v17, v21, v28

    .local v17, "key":Ljava/lang/String;
    const/16 v28, 0x1

    aget-object v27, v21, v28

    .line 391
    .local v27, "value":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    move-object/from16 v0, v27

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 395
    .end local v17    # "key":Ljava/lang/String;
    .end local v21    # "parts2":[Ljava/lang/String;
    .end local v27    # "value":Ljava/lang/String;
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v28

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v28

    check-cast v28, [Ljava/lang/String;

    .line 394
    move-object/from16 v0, v28

    invoke-static {v12, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordSwipedCalibration(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_4

    goto/16 :goto_2

    .line 346
    :sswitch_data_0
    .sparse-switch
        -0x4ab9a77b -> :sswitch_2
        0x7973187e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 356
    :sswitch_data_1
    .sparse-switch
        0x6c -> :sswitch_0
        0x72 -> :sswitch_3
    .end sparse-switch
.end method

.method private reStart()V
    .locals 3

    .prologue
    .line 250
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$2;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 258
    return-void
.end method


# virtual methods
.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 523
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v1

    if-nez v1, :cond_1

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    iget-boolean v1, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;->connected:Z

    if-eqz v1, :cond_0

    .line 528
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v0

    .line 529
    .local v0, "newVin":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->vin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 530
    iput-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->vin:Ljava/lang/String;

    .line 531
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set-id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->vin:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommandAsync(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized dumpRecording()Ljava/lang/String;
    .locals 2

    .prologue
    .line 498
    monitor-enter p0

    :try_start_0
    const-string v0, "record save"

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 499
    const-wide/16 v0, 0x1770

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->recordingPath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    :goto_0
    monitor-exit p0

    return-object v0

    .line 502
    :catch_0
    move-exception v0

    .line 504
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 501
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public enablePreview(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 517
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 549
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v1

    if-nez v1, :cond_1

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v0

    .line 553
    .local v0, "inputPreferences":Lcom/navdy/service/library/events/preferences/InputPreferences;
    if-eqz v0, :cond_0

    .line 554
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->onInputPreferenceUpdate(Lcom/navdy/service/library/events/preferences/InputPreferences;)V

    goto :goto_0
.end method

.method public onInputPreferenceUpdate(Lcom/navdy/service/library/events/preferences/InputPreferences;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/preferences/InputPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 573
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->shuttingDown:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$5;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Lcom/navdy/service/library/events/preferences/InputPreferences;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 599
    iget-object v0, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMED:Lcom/navdy/hud/app/event/Shutdown$State;

    if-ne v0, v1, :cond_0

    .line 600
    iput-boolean v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->shuttingDown:Z

    .line 601
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$6;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 608
    :cond_0
    return-void
.end method

.method public onTakeSnapshot(Lcom/navdy/hud/app/gesture/GestureServiceConnector$TakeSnapshot;)V
    .locals 3
    .param p1, "takeSnapshot"    # Lcom/navdy/hud/app/gesture/GestureServiceConnector$TakeSnapshot;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 560
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 569
    :goto_0
    return-void

    .line 563
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Request to take snap shot"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 565
    :try_start_0
    const-string v1, "/data/misc/swiped/camera.png"

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->takeSnapShot(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while taking the snapshot "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 539
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$4;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 545
    return-void
.end method

.method public declared-synchronized sendCommand(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 415
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->temp:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 416
    .local v0, "out":Ljava/nio/ByteBuffer;
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->encoder:Ljava/nio/charset/CharsetEncoder;

    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;)Ljava/nio/CharBuffer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    .line 417
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 419
    iget-object v1, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->temp:[B

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    .end local v0    # "out":Ljava/nio/ByteBuffer;
    :cond_0
    monitor-exit p0

    return-void

    .line 413
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public varargs sendCommand([Ljava/lang/Object;)V
    .locals 6
    .param p1, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    if-eqz p1, :cond_3

    array-length v2, p1

    if-lez v2, :cond_3

    .line 438
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 439
    .local v1, "builder":Ljava/lang/StringBuilder;
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v0, p1, v2

    .line 440
    .local v0, "arg":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 441
    array-length v4, p1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p1, v4

    if-ne v0, v4, :cond_1

    .line 442
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 444
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 448
    .end local v0    # "arg":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 451
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_3
    return-void
.end method

.method public sendCommandAsync(Ljava/lang/String;)V
    .locals 3
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 424
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$3;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;Ljava/lang/String;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 434
    return-void
.end method

.method public setCalibrationEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 472
    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "calibration resume"

    :goto_0
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 476
    :goto_1
    return-void

    .line 472
    :cond_0
    const-string v1, "calibration pause"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 473
    :catch_0
    move-exception v0

    .line 474
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while setting calibration state "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public setDiscreteMode(Z)V
    .locals 0
    .param p1, "discreteMode"    # Z

    .prologue
    .line 513
    return-void
.end method

.method public setRecordMode(Z)V
    .locals 4
    .param p1, "oneShot"    # Z

    .prologue
    .line 462
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setRecordMode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 464
    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "record-mode oneshot"

    :goto_0
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 468
    :goto_1
    return-void

    .line 464
    :cond_0
    const-string v1, "record-mode rolling"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception while setting the record mode "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public declared-synchronized start()V
    .locals 4

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 158
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 232
    :goto_0
    monitor-exit p0

    return-void

    .line 162
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    if-eqz v2, :cond_1

    .line 163
    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "swipedconnector already running"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 167
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeSocket()V

    .line 168
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeThread()V

    .line 170
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171
    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Not starting gesture engine due to quiet mode"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    .line 176
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "1"

    .line 177
    .local v0, "defaultVersion":Ljava/lang/String;
    :goto_1
    const-string v2, "persist.sys.gesture.version"

    invoke-static {v2, v0}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "gestureVersion":Ljava/lang/String;
    const-string v2, "gesture.enabled"

    invoke-static {v2, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$1;-><init>(Lcom/navdy/hud/app/gesture/GestureServiceConnector;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    .line 229
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    const-string v3, "SwipedReaderThread"

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 230
    iget-object v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->swipedReader:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 231
    sget-object v2, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "swipedconnector connect thread started"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    .end local v0    # "defaultVersion":Ljava/lang/String;
    .end local v1    # "gestureVersion":Ljava/lang/String;
    :cond_3
    const-string v0, "beta"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public startRecordingVideo()V
    .locals 2

    .prologue
    .line 480
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "startRecordingVideo"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 481
    const-string v0, "record on"

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :goto_0
    return-void

    .line 482
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized stop()V
    .locals 2

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 237
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 241
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    if-eqz v0, :cond_0

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->running:Z

    .line 243
    const-string v0, "gesture.enabled"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeSocket()V

    .line 245
    invoke-direct {p0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->closeThread()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stopRecordingVideo()V
    .locals 2

    .prologue
    .line 488
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopRecordingVideo"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 490
    :try_start_0
    const-string v0, "record off"

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :goto_0
    return-void

    .line 491
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public takeSnapShot(Ljava/lang/String;)V
    .locals 6
    .param p1, "absolutePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 454
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 455
    .local v0, "time":J
    iget-wide v2, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->lastSnapshotTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1388

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 456
    iput-wide v0, p0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->lastSnapshotTime:J

    .line 457
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "snapshot "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->sendCommand(Ljava/lang/String;)V

    .line 459
    :cond_0
    return-void
.end method
