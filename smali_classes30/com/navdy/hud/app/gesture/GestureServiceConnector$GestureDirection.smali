.class public final enum Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;
.super Ljava/lang/Enum;
.source "GestureServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/gesture/GestureServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GestureDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

.field public static final enum LEFT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

.field public static final enum RIGHT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

.field public static final enum UNKNOWN:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->UNKNOWN:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    .line 131
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->LEFT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    .line 132
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->RIGHT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    .line 129
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->UNKNOWN:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->LEFT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->RIGHT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->$VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 129
    const-class v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->$VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$GestureDirection;

    return-object v0
.end method
