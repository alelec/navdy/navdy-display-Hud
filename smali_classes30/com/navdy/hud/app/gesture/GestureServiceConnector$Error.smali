.class final enum Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
.super Ljava/lang/Enum;
.source "GestureServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/gesture/GestureServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

.field public static final enum CANNOT_CONNECT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

.field public static final enum COMMUNICATION_LOST:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

.field public static final enum SWIPED_RESTARTED:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    const-string v1, "CANNOT_CONNECT"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->CANNOT_CONNECT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    .line 145
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    const-string v1, "COMMUNICATION_LOST"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->COMMUNICATION_LOST:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    .line 146
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    const-string v1, "SWIPED_RESTARTED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->SWIPED_RESTARTED:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    .line 143
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->CANNOT_CONNECT:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->COMMUNICATION_LOST:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->SWIPED_RESTARTED:Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->$VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 143
    const-class v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->$VALUES:[Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/gesture/GestureServiceConnector$Error;

    return-object v0
.end method
