.class Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;
.super Ljava/io/OutputStream;
.source "iAP2Link.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/iAP2Link;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProxyStream"
.end annotation


# instance fields
.field private outputStream:Ljava/io/OutputStream;

.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method private constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/hud/device/connection/iAP2Link$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;
    .param p2, "x1"    # Lcom/navdy/hud/device/connection/iAP2Link$1;

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    return-void
.end method


# virtual methods
.method public setOutputStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->outputStream:Ljava/io/OutputStream;

    .line 210
    return-void
.end method

.method public write(I)V
    .locals 2
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Shouldn\'t be calling this method!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write([B)V
    .locals 1
    .param p1, "buffer"    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->outputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 217
    :cond_0
    return-void
.end method
