.class Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;
.super Ljava/lang/Object;
.source "iAP2MusicHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

.field final synthetic val$data:[B

.field final synthetic val$fileTransferIdentifier:I

.field final synthetic val$musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    iput p2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$fileTransferIdentifier:I

    iput-object p3, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$data:[B

    iput-object p4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 145
    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onFileReceived task "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$fileTransferIdentifier:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 147
    const/4 v2, 0x0

    .line 148
    .local v2, "photo":Lokio/ByteString;
    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$data:[B

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$data:[B

    array-length v4, v4

    if-lez v4, :cond_0

    .line 150
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$data:[B

    const/16 v5, 0xc8

    const/16 v6, 0xc8

    sget-object v7, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/service/library/util/ScalingUtilities;->decodeByteArray([BIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 152
    const/16 v4, 0xc8

    const/16 v5, 0xc8

    sget-object v6, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {v0, v4, v5, v6}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmapAndRecycleOriginalIfScaled(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 153
    .local v3, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/navdy/service/library/util/ScalingUtilities;->encodeByteArray(Landroid/graphics/Bitmap;)[B

    move-result-object v4

    invoke-static {v4}, Lokio/ByteString;->of([B)Lokio/ByteString;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 161
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    new-instance v5, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;-><init>()V

    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v6, v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 162
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v6, v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 163
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->val$musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v6, v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 164
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v5

    .line 165
    invoke-virtual {v5, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->photo(Lokio/ByteString;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v5

    .line 166
    invoke-virtual {v5}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    move-result-object v5

    .line 161
    # setter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    invoke-static {v4, v5}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$102(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .line 167
    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sending artworkResponse "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$100(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 168
    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;
    invoke-static {v4}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$200(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)Lcom/navdy/hud/device/connection/iAP2Link;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    invoke-static {v5}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$100(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V

    .line 169
    return-void

    .line 155
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_1
    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Couldn\'t decode byte array to bitmap"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 157
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Error updating the art work received "

    invoke-virtual {v4, v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
