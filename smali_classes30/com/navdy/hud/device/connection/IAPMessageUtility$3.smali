.class final Lcom/navdy/hud/device/connection/IAPMessageUtility$3;
.super Ljava/util/HashMap;
.source "IAPMessageUtility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/IAPMessageUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;",
        "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 46
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->MUSIC_REPEAT_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->One:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->MUSIC_REPEAT_MODE_ONE:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->All:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->MUSIC_REPEAT_MODE_ALL:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method
