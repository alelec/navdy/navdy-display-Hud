.class final Lcom/navdy/hud/device/connection/IAPMessageUtility$1;
.super Ljava/util/HashMap;
.source "IAPMessageUtility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/IAPMessageUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;",
        "Lcom/navdy/service/library/events/audio/MusicPlaybackState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 28
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Stopped:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Playing:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Paused:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekForward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_FAST_FORWARDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekBackward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_REWINDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method
