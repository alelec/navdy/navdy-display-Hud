.class Lcom/navdy/hud/device/connection/iAP2Link$4;
.super Ljava/lang/Object;
.source "iAP2Link.java"

# interfaces
.implements Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$4;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    const/4 v3, 0x0

    .line 172
    sget-object v1, Lcom/navdy/service/library/events/Ext_NavdyEvent;->launchAppEvent:Lcom/squareup/wire/Extension;

    invoke-virtual {p1, v1}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    .line 173
    .local v0, "launchAppRequest":Lcom/navdy/service/library/events/input/LaunchAppEvent;
    iget-object v1, v0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link$4;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;
    invoke-static {v1}, Lcom/navdy/hud/device/connection/iAP2Link;->access$600(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/iAPProcessor;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->launchApp(Ljava/lang/String;Z)V

    .line 178
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link$4;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;
    invoke-static {v1}, Lcom/navdy/hud/device/connection/iAP2Link;->access$600(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/iAPProcessor;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->launchApp(Z)V

    goto :goto_0
.end method
