.class public Lcom/navdy/hud/device/connection/IAPMessageUtility;
.super Ljava/lang/Object;
.source "IAPMessageUtility.java"


# static fields
.field private static final PLAYBACK_STATES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;",
            "Lcom/navdy/service/library/events/audio/MusicPlaybackState;",
            ">;"
        }
    .end annotation
.end field

.field private static final REPEAT_MODES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHUFFLE_MODES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;

    invoke-direct {v0}, Lcom/navdy/hud/device/connection/IAPMessageUtility$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/device/connection/IAPMessageUtility;->PLAYBACK_STATES:Ljava/util/Map;

    .line 37
    new-instance v0, Lcom/navdy/hud/device/connection/IAPMessageUtility$2;

    invoke-direct {v0}, Lcom/navdy/hud/device/connection/IAPMessageUtility$2;-><init>()V

    sput-object v0, Lcom/navdy/hud/device/connection/IAPMessageUtility;->SHUFFLE_MODES:Ljava/util/Map;

    .line 44
    new-instance v0, Lcom/navdy/hud/device/connection/IAPMessageUtility$3;

    invoke-direct {v0}, Lcom/navdy/hud/device/connection/IAPMessageUtility$3;-><init>()V

    sput-object v0, Lcom/navdy/hud/device/connection/IAPMessageUtility;->REPEAT_MODES:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMusicTrackInfoForNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 5
    .param p0, "nowPlayingUpdate"    # Lcom/navdy/hud/mfi/NowPlayingUpdate;

    .prologue
    const/4 v4, 0x1

    .line 150
    if-nez p0, :cond_0

    .line 151
    const/4 v1, 0x0

    .line 172
    :goto_0
    return-object v1

    .line 155
    :cond_0
    new-instance v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v1, Lcom/navdy/hud/device/connection/IAPMessageUtility;->PLAYBACK_STATES:Ljava/util/Map;

    iget-object v3, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 156
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    .line 157
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    .line 158
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    .line 159
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    long-to-int v2, v2

    .line 160
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    long-to-int v2, v2

    .line 161
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v1, Lcom/navdy/hud/device/connection/IAPMessageUtility;->SHUFFLE_MODES:Ljava/util/Map;

    iget-object v3, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 162
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v1, Lcom/navdy/hud/device/connection/IAPMessageUtility;->REPEAT_MODES:Ljava/util/Map;

    iget-object v3, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 163
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->repeatMode(Lcom/navdy/service/library/events/audio/MusicRepeatMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 165
    .local v0, "resultBuilder":Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    if-eqz v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    .line 170
    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    .line 172
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    goto :goto_0
.end method

.method public static getPhoneEventForCallStateUpdate(Lcom/navdy/hud/mfi/CallStateUpdate;Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .locals 4
    .param p0, "callstateUpdate"    # Lcom/navdy/hud/mfi/CallStateUpdate;
    .param p1, "lastCallActionRequested"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    const/4 v1, 0x0

    .line 53
    if-eqz p0, :cond_4

    .line 54
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;-><init>()V

    .line 55
    .local v0, "builder":Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    sget-object v2, Lcom/navdy/hud/device/connection/IAPMessageUtility$4;->$SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status:[I

    iget-object v3, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->status:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->displayName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->remoteID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->remoteID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->label:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 94
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->callUUID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 97
    iget-object v1, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->callUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->callUUID(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    .line 99
    :cond_3
    invoke-virtual {v0}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v1

    .line 101
    .end local v0    # "builder":Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    :cond_4
    :pswitch_0
    return-object v1

    .line 57
    .restart local v0    # "builder":Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    :pswitch_1
    if-eqz p1, :cond_5

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_DIAL:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-ne p1, v1, :cond_5

    .line 59
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DISCONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 61
    :cond_5
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 65
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 68
    :pswitch_3
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 73
    :pswitch_4
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 76
    :pswitch_5
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 79
    :pswitch_6
    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_HELD:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method public static performActionForTelephonyRequest(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;Lcom/navdy/hud/mfi/IAPCommunicationsManager;)Z
    .locals 3
    .param p0, "request"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
    .param p1, "manager"    # Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    .prologue
    const/4 v0, 0x0

    .line 105
    if-eqz p0, :cond_0

    .line 106
    sget-object v1, Lcom/navdy/hud/device/connection/IAPMessageUtility$4;->$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction:[I

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/callcontrol/CallAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 108
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->acceptCall(Ljava/lang/String;)V

    .line 136
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 111
    :cond_2
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->acceptCall()V

    goto :goto_1

    .line 116
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->endCall(Ljava/lang/String;)V

    goto :goto_1

    .line 119
    :cond_3
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->endCall()V

    goto :goto_1

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->initiateDestinationCall(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :pswitch_3
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mute()V

    goto :goto_1

    .line 131
    :pswitch_4
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->unMute()V

    goto :goto_1

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
