.class Lcom/navdy/hud/device/connection/iAP2Link$1;
.super Ljava/lang/Object;
.source "iAP2Link.java"

# interfaces
.implements Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/iAP2Link;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$1;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processEvent([B)[B
    .locals 7
    .param p1, "eventData"    # [B

    .prologue
    .line 115
    invoke-static {p1}, Lcom/navdy/service/library/events/WireUtil;->getEventType([B)Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    move-result-object v4

    .line 117
    .local v4, "type":Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/device/connection/iAP2Link$1;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;
    invoke-static {v5}, Lcom/navdy/hud/device/connection/iAP2Link;->access$000(Lcom/navdy/hud/device/connection/iAP2Link;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 130
    .end local p1    # "eventData":[B
    :cond_0
    :goto_0
    return-object p1

    .line 121
    .restart local p1    # "eventData":[B
    :cond_1
    const/4 v2, 0x0

    .line 122
    .local v2, "handled":Z
    iget-object v5, p0, Lcom/navdy/hud/device/connection/iAP2Link$1;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;
    invoke-static {v5}, Lcom/navdy/hud/device/connection/iAP2Link;->access$000(Lcom/navdy/hud/device/connection/iAP2Link;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;

    .line 124
    .local v3, "processor":Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/device/connection/iAP2Link$1;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->wire:Lcom/squareup/wire/Wire;
    invoke-static {v5}, Lcom/navdy/hud/device/connection/iAP2Link;->access$100(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/squareup/wire/Wire;

    move-result-object v5

    const-class v6, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v5, p1, v6}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 125
    .local v1, "event":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-interface {v3, v1}, Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;->processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 130
    .end local v1    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    :goto_1
    if-eqz v2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Error while parsing request"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
