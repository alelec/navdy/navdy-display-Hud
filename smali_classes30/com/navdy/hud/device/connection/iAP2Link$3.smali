.class Lcom/navdy/hud/device/connection/iAP2Link$3;
.super Ljava/lang/Object;
.source "iAP2Link.java"

# interfaces
.implements Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$3;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 163
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link$3;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->currentPhoneEvent:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    invoke-static {v2}, Lcom/navdy/hud/device/connection/iAP2Link;->access$500(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V

    .line 164
    .local v0, "response":Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link$3;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V

    .line 165
    const/4 v1, 0x1

    return v1
.end method
