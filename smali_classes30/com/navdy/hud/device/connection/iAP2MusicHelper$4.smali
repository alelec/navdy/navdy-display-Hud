.class Lcom/navdy/hud/device/connection/iAP2MusicHelper$4;
.super Ljava/lang/Object;
.source "iAP2MusicHelper.java"

# interfaces
.implements Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2MusicHelper;->addEventProcessors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$4;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 213
    # getter for: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "got PhotoUpdatesRequest"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 214
    sget-object v1, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoUpdateRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {p1, v1}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    .line 215
    .local v0, "request":Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$4;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    # invokes: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onPhotoUpdatesRequest(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V
    invoke-static {v1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$400(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V

    .line 216
    const/4 v1, 0x1

    return v1
.end method
