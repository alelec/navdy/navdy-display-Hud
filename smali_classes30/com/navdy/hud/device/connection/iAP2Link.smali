.class public Lcom/navdy/hud/device/connection/iAP2Link;
.super Ljava/lang/Object;
.source "iAP2Link.java"

# interfaces
.implements Lcom/navdy/service/library/device/link/Link;
.implements Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;
.implements Lcom/navdy/hud/mfi/iAPProcessor$StateListener;
.implements Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;,
        Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;,
        Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;
    }
.end annotation


# static fields
.field public static proxyEASession:Lcom/navdy/hud/mfi/EASession;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bandwidthLevel:I

.field private connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

.field private currentPhoneEvent:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

.field private eventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

.field private eventProcessors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent$MessageType;",
            "Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

.field private iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

.field private lastCallActionRequested:Lcom/navdy/service/library/events/callcontrol/CallAction;

.field private linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

.field private linkListener:Lcom/navdy/service/library/device/link/LinkListener;

.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

.field private proxyStream:Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

.field private readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

.field private wire:Lcom/squareup/wire/Wire;

.field private writerThread:Lcom/navdy/service/library/device/link/WriterThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V
    .locals 4
    .param p1, "listener"    # Lcom/navdy/hud/mfi/IAPListener;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput v2, p0, Lcom/navdy/hud/device/connection/iAP2Link;->bandwidthLevel:I

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    .line 112
    new-instance v0, Lcom/navdy/hud/device/connection/iAP2Link$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/device/connection/iAP2Link$1;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

    .line 225
    new-instance v0, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/hud/device/connection/iAP2Link$1;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->proxyStream:Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

    .line 90
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 92
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/mfi/iAPProcessor;-><init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {p0}, Lcom/navdy/hud/device/connection/iAP2Link;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor;->setAccessoryName(Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    .line 95
    new-instance v0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/hud/mfi/iAPProcessor;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .line 96
    new-instance v0, Lcom/navdy/hud/mfi/LinkLayer;

    invoke-direct {v0}, Lcom/navdy/hud/mfi/LinkLayer;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor;->connect(Lcom/navdy/hud/mfi/LinkLayer;)V

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/mfi/iAPProcessor;->connect(Lcom/navdy/hud/mfi/iAPProcessor$StateListener;)V

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/LinkLayer;->connect(Lcom/navdy/hud/mfi/iAPProcessor;)V

    .line 102
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/mfi/LinkLayer;->connect(Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;)V

    .line 103
    new-instance v0, Lcom/squareup/wire/Wire;

    new-array v1, v2, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->wire:Lcom/squareup/wire/Wire;

    .line 105
    invoke-direct {p0}, Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessors()V

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/device/connection/iAP2Link;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/squareup/wire/Wire;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->wire:Lcom/squareup/wire/Wire;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/LinkLayer;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/CallAction;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;
    .param p1, "x1"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->lastCallActionRequested:Lcom/navdy/service/library/events/callcontrol/CallAction;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/IAPCommunicationsManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->currentPhoneEvent:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/iAPProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/network/SocketAdapter;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;
    .param p1, "x1"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/hud/device/connection/iAP2Link;->getRemoteDevice(Lcom/navdy/service/library/network/SocketAdapter;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/service/library/device/link/LinkListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    return-object v0
.end method

.method private addEventProcessors()V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TelephonyRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2Link$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2Link$2;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2Link$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2Link$3;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->LaunchAppEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2Link$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2Link$4;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CallStateUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2Link$5;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2Link$5;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return-void
.end method

.method private getRemoteDevice(Lcom/navdy/service/library/network/SocketAdapter;)Landroid/bluetooth/BluetoothDevice;
    .locals 3
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 383
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 384
    .local v0, "navdyDevice":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    return-object v1
.end method

.method private startProtobufLink(Lcom/navdy/service/library/network/SocketAdapter;)Z
    .locals 8
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    const/4 v3, 0x0

    .line 279
    const/4 v1, 0x0

    .line 282
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 283
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 284
    .local v2, "outputStream":Ljava/io/OutputStream;
    new-instance v4, Lcom/navdy/service/library/device/link/ReaderThread;

    sget-object v5, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/navdy/service/library/device/link/ReaderThread;-><init>(Lcom/navdy/service/library/device/connection/ConnectionType;Ljava/io/InputStream;Lcom/navdy/service/library/device/link/LinkListener;Z)V

    iput-object v4, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 285
    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2Link;->proxyStream:Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->setOutputStream(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    iget-object v3, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/link/ReaderThread;->start()V

    .line 293
    const/4 v3, 0x1

    .end local v2    # "outputStream":Ljava/io/OutputStream;
    :goto_0
    return v3

    .line 286
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 289
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    goto :goto_0
.end method

.method private stopProtobufLink()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 297
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/ReaderThread;->cancel()V

    .line 299
    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->proxyStream:Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;->setOutputStream(Ljava/io/OutputStream;)V

    .line 302
    return-void
.end method


# virtual methods
.method addEventProcessor(Lcom/navdy/service/library/events/NavdyEvent$MessageType;Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    .param p2, "processor"    # Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    sget-object v0, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Already have a processor for that type!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessors:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-virtual {v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->close()V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    invoke-virtual {v0}, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->cancel()V

    .line 262
    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    if-eqz v0, :cond_2

    .line 266
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/WriterThread;->cancel()V

    .line 267
    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    if-eqz v0, :cond_3

    .line 271
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/ReaderThread;->cancel()V

    .line 272
    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 274
    :cond_3
    return-void
.end method

.method public getBandWidthLevel()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->bandwidthLevel:I

    return v0
.end method

.method public getLocalAddress()[B
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/mfi/Utils;->parseMACAddress(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCallStateUpdate(Lcom/navdy/hud/mfi/CallStateUpdate;)V
    .locals 2
    .param p1, "callStateUpdate"    # Lcom/navdy/hud/mfi/CallStateUpdate;

    .prologue
    .line 192
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->lastCallActionRequested:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-static {p1, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility;->getPhoneEventForCallStateUpdate(Lcom/navdy/hud/mfi/CallStateUpdate;Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v0

    .line 193
    .local v0, "event":Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    if-eqz v0, :cond_0

    .line 194
    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->currentPhoneEvent:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .line 195
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->lastCallActionRequested:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 196
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->currentPhoneEvent:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V

    .line 198
    :cond_0
    return-void
.end method

.method public onCommunicationUpdate(Lcom/navdy/hud/mfi/CommunicationUpdate;)V
    .locals 3
    .param p1, "communicationUpdate"    # Lcom/navdy/hud/mfi/CommunicationUpdate;

    .prologue
    .line 202
    sget-object v0, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CommunicationUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 420
    sget-object v0, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Error in IAP session"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 421
    return-void
.end method

.method public onReady()V
    .locals 3

    .prologue
    .line 427
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    invoke-virtual {v1, p0}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->setUpdatesListener(Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;)V

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->startUpdates()V

    .line 429
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->startCommunicationUpdates()V

    .line 430
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-virtual {v1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onReady()V

    .line 431
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->startHIDSession()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :goto_0
    return-void

    .line 432
    :catch_0
    move-exception v0

    .line 433
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error starting the call state updates "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onSessionStart(Lcom/navdy/hud/mfi/EASession;)V
    .locals 3
    .param p1, "session"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 393
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/EASession;->getProtocol()Ljava/lang/String;

    move-result-object v1

    .line 394
    .local v1, "proto":Ljava/lang/String;
    const-string v2, "com.navdy.hud.api.v1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 396
    new-instance v0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;

    invoke-direct {v0, p1}, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;-><init>(Lcom/navdy/hud/mfi/EASession;)V

    .line 397
    .local v0, "adapter":Lcom/navdy/service/library/network/SocketAdapter;
    invoke-direct {p0, v0}, Lcom/navdy/hud/device/connection/iAP2Link;->startProtobufLink(Lcom/navdy/service/library/network/SocketAdapter;)Z

    .line 404
    .end local v0    # "adapter":Lcom/navdy/service/library/network/SocketAdapter;
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    const-string v2, "com.navdy.hud.proxy.v1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    sput-object p1, Lcom/navdy/hud/device/connection/iAP2Link;->proxyEASession:Lcom/navdy/hud/mfi/EASession;

    .line 400
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    if-eqz v2, :cond_0

    .line 401
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    invoke-interface {v2}, Lcom/navdy/service/library/device/link/LinkListener;->onNetworkLinkReady()V

    goto :goto_0
.end method

.method public onSessionStop(Lcom/navdy/hud/mfi/EASession;)V
    .locals 2
    .param p1, "session"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 408
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/EASession;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "proto":Ljava/lang/String;
    const-string v1, "com.navdy.hud.api.v1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410
    invoke-direct {p0}, Lcom/navdy/hud/device/connection/iAP2Link;->stopProtobufLink()V

    .line 415
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/navdy/hud/mfi/EASession;->close()V

    .line 416
    return-void

    .line 411
    :cond_1
    const-string v1, "com.navdy.hud.proxy.v1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 412
    const/4 v1, 0x0

    sput-object v1, Lcom/navdy/hud/device/connection/iAP2Link;->proxyEASession:Lcom/navdy/hud/mfi/EASession;

    goto :goto_0
.end method

.method public queue(Lcom/navdy/hud/mfi/LinkPacket;)V
    .locals 2
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/LinkPacket;

    .prologue
    .line 445
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    iget-object v1, p1, Lcom/navdy/hud/mfi/LinkPacket;->data:[B

    invoke-virtual {v0, v1}, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->write([B)V

    .line 448
    :cond_0
    return-void
.end method

.method sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V
    .locals 3
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    if-eqz v1, :cond_0

    .line 136
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v0

    .line 137
    .local v0, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/navdy/service/library/device/link/LinkListener;->onNavdyEventReceived([B)V

    .line 139
    .end local v0    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    :cond_0
    return-void
.end method

.method public setBandwidthLevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 244
    iput p1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->bandwidthLevel:I

    .line 245
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->musicHelper:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->setBandwidthLevel(I)V

    .line 246
    return-void
.end method

.method public start(Lcom/navdy/service/library/network/SocketAdapter;Ljava/util/concurrent/LinkedBlockingDeque;Lcom/navdy/service/library/device/link/LinkListener;)Z
    .locals 3
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;
    .param p3, "listener"    # Lcom/navdy/service/library/device/link/LinkListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/network/SocketAdapter;",
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;",
            "Lcom/navdy/service/library/device/link/LinkListener;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 229
    .local p2, "outboundEventQueue":Ljava/util/concurrent/LinkedBlockingDeque;, "Ljava/util/concurrent/LinkedBlockingDeque<Lcom/navdy/service/library/device/link/EventRequest;>;"
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/WriterThread;->isClosing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 230
    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/ReaderThread;->isClosing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 231
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must stop threads before calling startLink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_2
    iput-object p3, p0, Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;

    .line 234
    new-instance v0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;-><init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/network/SocketAdapter;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    .line 235
    new-instance v0, Lcom/navdy/service/library/device/link/WriterThread;

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link;->proxyStream:Lcom/navdy/hud/device/connection/iAP2Link$ProxyStream;

    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link;->eventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

    invoke-direct {v0, p2, v1, v2}, Lcom/navdy/service/library/device/link/WriterThread;-><init>(Ljava/util/concurrent/LinkedBlockingDeque;Ljava/io/OutputStream;Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    .line 237
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->connectedThread:Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;

    invoke-virtual {v0}, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->start()V

    .line 238
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/WriterThread;->start()V

    .line 239
    const/4 v0, 0x1

    return v0
.end method
