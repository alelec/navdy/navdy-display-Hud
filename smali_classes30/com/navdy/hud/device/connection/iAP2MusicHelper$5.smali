.class Lcom/navdy/hud/device/connection/iAP2MusicHelper$5;
.super Ljava/lang/Object;
.source "iAP2MusicHelper.java"

# interfaces
.implements Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2MusicHelper;->addEventProcessors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$5;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 223
    sget-object v1, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicArtworkRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {p1, v1}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 224
    .local v0, "musicArtworkRequest":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper$5;->this$0:Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    # invokes: Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onMusicArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)Z
    invoke-static {v1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->access$500(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)Z

    move-result v1

    return v1
.end method
