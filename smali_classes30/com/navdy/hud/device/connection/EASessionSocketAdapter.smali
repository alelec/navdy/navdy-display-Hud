.class public Lcom/navdy/hud/device/connection/EASessionSocketAdapter;
.super Ljava/lang/Object;
.source "EASessionSocketAdapter.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketAdapter;


# instance fields
.field eaSession:Lcom/navdy/hud/mfi/EASession;

.field private sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/EASession;)V
    .locals 2
    .param p1, "eaSession"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 25
    iput-object p1, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    .line 26
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "closing connection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/EASession;->close()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    .line 44
    :cond_0
    return-void
.end method

.method public connect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t create outbound EASession connections"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Connecting to existing eaSession"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/EASession;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/EASession;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 4

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EA:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    iget-object v2, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/EASession;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/EASession;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/device/connection/EASessionSocketAdapter;->eaSession:Lcom/navdy/hud/mfi/EASession;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
