.class Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;
.super Lcom/navdy/service/library/device/link/IOThread;
.source "iAP2Link.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/iAP2Link;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectedThread"
.end annotation


# instance fields
.field private final mmInStream:Ljava/io/InputStream;

.field private final mmOutStream:Ljava/io/OutputStream;

.field private final mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 5
    .param p2, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Lcom/navdy/service/library/device/link/IOThread;-><init>()V

    .line 311
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "create ConnectedThread"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 312
    iput-object p2, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 313
    const/4 v1, 0x0

    .line 314
    .local v1, "tmpIn":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 318
    .local v2, "tmpOut":Ljava/io/OutputStream;
    :try_start_0
    invoke-interface {p2}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 319
    invoke-interface {p2}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 324
    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    .line 325
    iput-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    .line 326
    return-void

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "temp sockets not created"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 376
    invoke-super {p0}, Lcom/navdy/service/library/device/link/IOThread;->cancel()V

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 378
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 379
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    .line 329
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    iget-object v7, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    # invokes: Lcom/navdy/hud/device/connection/iAP2Link;->getRemoteDevice(Lcom/navdy/service/library/network/SocketAdapter;)Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6, v7}, Lcom/navdy/hud/device/connection/iAP2Link;->access$800(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/network/SocketAdapter;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    .line 331
    .local v4, "device":Landroid/bluetooth/BluetoothDevice;
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BEGIN mConnectedThread - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " name:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 332
    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 334
    .local v2, "cause":Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2Link;->access$900(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/service/library/device/link/LinkListener;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-interface {v6, v7}, Lcom/navdy/service/library/device/link/LinkListener;->linkEstablished(Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 335
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2Link;->access$1000(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/LinkLayer;

    move-result-object v6

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/navdy/hud/mfi/LinkLayer;->connectionStarted(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const/16 v6, 0x400

    new-array v0, v6, [B

    .line 341
    .local v0, "buffer":[B
    :goto_0
    iget-boolean v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->closing:Z

    if-nez v6, :cond_0

    .line 344
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 345
    .local v1, "bytes":I
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 346
    .local v3, "data":[B
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2Link;->access$1000(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/LinkLayer;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/mfi/LinkPacket;

    invoke-direct {v7, v3}, Lcom/navdy/hud/mfi/LinkPacket;-><init>([B)V

    invoke-virtual {v6, v7}, Lcom/navdy/hud/mfi/LinkLayer;->queue(Lcom/navdy/hud/mfi/LinkPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 347
    .end local v1    # "bytes":I
    .end local v3    # "data":[B
    :catch_0
    move-exception v5

    .line 348
    .local v5, "e":Ljava/io/IOException;
    iget-boolean v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->closing:Z

    if-nez v6, :cond_0

    .line 349
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "disconnected: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 350
    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ABORTED:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 355
    .end local v5    # "e":Ljava/io/IOException;
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 358
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2Link;->access$1000(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/LinkLayer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/LinkLayer;->connectionEnded()V

    .line 359
    iget-object v6, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->linkListener:Lcom/navdy/service/library/device/link/LinkListener;
    invoke-static {v6}, Lcom/navdy/hud/device/connection/iAP2Link;->access$900(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/service/library/device/link/LinkListener;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-interface {v6, v7, v2}, Lcom/navdy/service/library/device/link/LinkListener;->linkLost(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 360
    return-void
.end method

.method public write([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 369
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2Link$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :goto_0
    return-void

    .line 370
    :catch_0
    move-exception v0

    .line 371
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/device/connection/iAP2Link;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Exception during write"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
