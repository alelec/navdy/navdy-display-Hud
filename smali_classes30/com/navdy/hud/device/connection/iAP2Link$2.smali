.class Lcom/navdy/hud/device/connection/iAP2Link$2;
.super Ljava/lang/Object;
.source "iAP2Link.java"

# interfaces
.implements Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/device/connection/iAP2Link;


# direct methods
.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/device/connection/iAP2Link;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2Link$2;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 7
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 152
    sget-object v2, Lcom/navdy/service/library/events/Ext_NavdyEvent;->telephonyRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {p1, v2}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    .line 153
    .local v0, "request":Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link$2;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    iget-object v3, v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    # setter for: Lcom/navdy/hud/device/connection/iAP2Link;->lastCallActionRequested:Lcom/navdy/service/library/events/callcontrol/CallAction;
    invoke-static {v2, v3}, Lcom/navdy/hud/device/connection/iAP2Link;->access$302(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 154
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2Link$2;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    # getter for: Lcom/navdy/hud/device/connection/iAP2Link;->iAPCommunicationsManager:Lcom/navdy/hud/mfi/IAPCommunicationsManager;
    invoke-static {v2}, Lcom/navdy/hud/device/connection/iAP2Link;->access$400(Lcom/navdy/hud/device/connection/iAP2Link;)Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/navdy/hud/device/connection/IAPMessageUtility;->performActionForTelephonyRequest(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;Lcom/navdy/hud/mfi/IAPCommunicationsManager;)Z

    move-result v1

    .line 155
    .local v1, "succeeded":Z
    iget-object v3, p0, Lcom/navdy/hud/device/connection/iAP2Link$2;->this$0:Lcom/navdy/hud/device/connection/iAP2Link;

    new-instance v4, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-eqz v1, :cond_0

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    :goto_0
    const/4 v6, 0x0

    invoke-direct {v4, v5, v2, v6}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;-><init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V

    .line 156
    const/4 v2, 0x1

    return v2

    .line 155
    :cond_0
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    goto :goto_0
.end method
