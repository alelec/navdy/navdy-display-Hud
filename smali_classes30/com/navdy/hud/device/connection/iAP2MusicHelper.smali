.class Lcom/navdy/hud/device/connection/iAP2MusicHelper;
.super Ljava/lang/Object;
.source "iAP2MusicHelper.java"

# interfaces
.implements Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;
.implements Lcom/navdy/hud/mfi/IAPFileTransferListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;
    }
.end annotation


# static fields
.field private static final LIMITED_BANDWIDTH_FILE_TRANSFER_LIMIT:I = 0x2710

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private fileTransferCanceled:Z

.field private final helperStateLock:Ljava/lang/Object;

.field private iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

.field private iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

.field private keyDownTime:J

.field private lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

.field private link:Lcom/navdy/hud/device/connection/iAP2Link;

.field private nowPlayingFileTransferIdentifier:I

.field private transferIdTrackInfoMap:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

.field private waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

.field private waitingFileTransferIdentifier:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/device/connection/iAP2Link;Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 2
    .param p1, "link"    # Lcom/navdy/hud/device/connection/iAP2Link;
    .param p2, "iAPProcessor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    .line 52
    iput v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->fileTransferCanceled:Z

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->keyDownTime:J

    .line 59
    new-instance v0, Landroid/util/LruCache;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    .line 76
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    .line 78
    new-instance v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-direct {v0, p2}, Lcom/navdy/hud/mfi/IAPFileTransferManager;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-virtual {p2, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->connect(Lcom/navdy/hud/mfi/IIAPFileTransferManager;)V

    .line 81
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager;

    invoke-direct {v0, p2}, Lcom/navdy/hud/mfi/IAPMusicManager;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;)V

    iput-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/mfi/IAPMusicManager;->setNowPlayingUpdateListener(Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;)V

    .line 84
    invoke-direct {p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->addEventProcessors()V

    .line 85
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)Lcom/navdy/hud/device/connection/iAP2Link;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;
    .param p1, "x1"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onMediaRemoteKeyEvent(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;
    .param p1, "x1"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onPhotoUpdatesRequest(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/device/connection/iAP2MusicHelper;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/device/connection/iAP2MusicHelper;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->onMusicArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)Z

    move-result v0

    return v0
.end method

.method private addEventProcessors()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MediaRemoteKeyEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$2;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessor(Lcom/navdy/service/library/events/NavdyEvent$MessageType;Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;)V

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NowPlayingUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$3;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessor(Lcom/navdy/service/library/events/NavdyEvent$MessageType;Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;)V

    .line 210
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdatesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$4;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessor(Lcom/navdy/service/library/events/NavdyEvent$MessageType;Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;)V

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicArtworkRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    new-instance v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper$5;

    invoke-direct {v2, p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$5;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/device/connection/iAP2Link;->addEventProcessor(Lcom/navdy/service/library/events/NavdyEvent$MessageType;Lcom/navdy/hud/device/connection/iAP2Link$NavdyEventProcessor;)V

    .line 227
    return-void
.end method

.method private getLastMusicTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 3

    .prologue
    .line 88
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    iget v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    monitor-exit v1

    return-object v0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onMediaRemoteKeyEvent(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;)V
    .locals 6
    .param p1, "request"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    .prologue
    .line 271
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(MFi) Media Key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 272
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper$6;->$SwitchMap$com$navdy$service$library$events$input$KeyEvent:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/KeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 287
    :goto_0
    return-void

    .line 274
    :pswitch_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->keyDownTime:J

    .line 275
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

    iget-object v3, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/MediaRemoteKey;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/IAPMusicManager;->onKeyDown(I)V

    goto :goto_0

    .line 278
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 279
    .local v0, "currentTime":J
    iget-wide v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->keyDownTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->cancel()V

    .line 283
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->keyDownTime:J

    .line 284
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

    iget-object v3, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/MediaRemoteKey;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/IAPMusicManager;->onKeyUp(I)V

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized onMusicArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)Z
    .locals 6
    .param p1, "musicArtworkRequest"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .prologue
    const/4 v1, 0x1

    .line 230
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->getLastMusicTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 231
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMusicArtworkRequest: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", musicTrackInfo: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 232
    if-eqz p1, :cond_5

    .line 233
    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    .line 234
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    .line 235
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 236
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 237
    if-eqz v0, :cond_4

    .line 238
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sameTrack(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    iget v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    if-ne v2, v4, :cond_4

    .line 240
    sget-object v4, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onMusicArtworkRequest, waiting file: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    invoke-virtual {v2}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 241
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    if-eqz v2, :cond_2

    .line 242
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onMusicArtworkRequest using waiting file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 243
    iget v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    iget-object v4, v4, Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;->data:[B

    invoke-direct {p0, v2, v4, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 255
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    :goto_2
    monitor-exit p0

    return v1

    .line 240
    :cond_1
    :try_start_2
    const-string v2, "null"

    goto :goto_0

    .line 244
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->fileTransferCanceled:Z

    if-eqz v2, :cond_3

    .line 245
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onMusicArtworkRequest, The file transfer has been canceled, send an empty artwork response"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 246
    iget v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    const/4 v4, 0x0

    invoke-direct {p0, v2, v4, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    goto :goto_1

    .line 255
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 230
    .end local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 248
    .restart local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_3
    :try_start_4
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onMusicArtworkRequest continuing file transfer"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->onFileTransferSetupResponse(IZ)V

    goto :goto_1

    .line 252
    :cond_4
    iput-object p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 253
    sget-object v2, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onMusicArtworkRequest waiting for file transfer, waitingArtworkRequest: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 259
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private onPhotoUpdatesRequest(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    .prologue
    .line 263
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v2, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne v1, v2, :cond_1

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 264
    .local v0, "postPhotoUpdates":Z
    :goto_0
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPhotoUpdatesRequest: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 265
    if-eqz v0, :cond_0

    .line 266
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->lastMusicArtworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V

    .line 268
    :cond_0
    return-void

    .line 263
    .end local v0    # "postPhotoUpdates":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sameTrack(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 2
    .param p1, "artworkRequest"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .param p2, "musicTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 290
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 291
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 292
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 293
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 3
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "data"    # [B
    .param p3, "musicTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 142
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$1;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 171
    return-void
.end method


# virtual methods
.method close()V
    .locals 2

    .prologue
    .line 306
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->keyDownTime:J

    .line 307
    return-void
.end method

.method public declared-synchronized onFileReceived(I[B)V
    .locals 5
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "data"    # [B

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    :try_start_1
    sget-object v3, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFileReceived: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p2, :cond_0

    array-length v1, p2

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 132
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "onFileReceived: no music info for this file (yet?)"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 133
    new-instance v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    invoke-direct {v1, p0, p2}, Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;-><init>(Lcom/navdy/hud/device/connection/iAP2MusicHelper;[B)V

    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    .line 134
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 139
    :goto_1
    monitor-exit p0

    return-void

    .line 130
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 136
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 137
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 138
    :try_start_3
    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 129
    .end local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 137
    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized onFileTransferCancel(I)V
    .locals 5
    .param p1, "fileTransferIdentifier"    # I

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 176
    :try_start_1
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFileTransferCancel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 179
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 180
    invoke-direct {p0, v1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sameTrack(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    if-ne v1, p1, :cond_0

    .line 182
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Sending response with null data"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 183
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sendArtworkResponse(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 190
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    monitor-exit p0

    return-void

    .line 185
    :cond_0
    :try_start_2
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "File transferred canceled, waiting for the meta data to send empty response"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 186
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->fileTransferCanceled:Z

    .line 187
    iput p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    .line 188
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    goto :goto_0

    .line 190
    .end local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 175
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onFileTransferSetup(IJ)V
    .locals 6
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "size"    # J

    .prologue
    .line 109
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v2

    .line 110
    :try_start_0
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFileTransferSetup, fileTransferIdentifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", waitingArtworkRequest: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", nowPlayingFileTransferIdentifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 112
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->fileTransferCanceled:Z

    .line 113
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingArtworkRequest:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 114
    invoke-direct {p0, v1, v0}, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sameTrack(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    if-ne v1, p1, :cond_0

    .line 116
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "onFileTransferSetup continuing file transfer"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    iget v3, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->onFileTransferSetupResponse(IZ)V

    .line 123
    :goto_0
    monitor-exit v2

    .line 124
    return-void

    .line 119
    :cond_0
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFileTransferSetup waiting for artwork request, waitingFileTransferIdentifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 120
    iput p1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFileTransferIdentifier:I

    .line 121
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;

    goto :goto_0

    .line 123
    .end local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public declared-synchronized onNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)V
    .locals 5
    .param p1, "nowPlayingUpdate"    # Lcom/navdy/hud/mfi/NowPlayingUpdate;

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/navdy/hud/device/connection/IAPMessageUtility;->getMusicTrackInfoForNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 96
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v2, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->helperStateLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 97
    :try_start_1
    iget v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    iput v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    .line 98
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNowPlayingUpdate nowPlayingFileTransferIdentifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", musicTrackInfo: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 100
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->transferIdTrackInfoMap:Landroid/util/LruCache;

    iget v3, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->nowPlayingFileTransferIdentifier:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->link:Lcom/navdy/hud/device/connection/iAP2Link;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/device/connection/iAP2Link;->sendMessageAsNavdyEvent(Lcom/squareup/wire/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 105
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 95
    .end local v0    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method onReady()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->setFileTransferListener(Lcom/navdy/hud/mfi/IAPFileTransferListener;)V

    .line 311
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iAPMusicManager:Lcom/navdy/hud/mfi/IAPMusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/IAPMusicManager;->startNowPlayingUpdates()V

    .line 312
    return-void
.end method

.method setBandwidthLevel(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 297
    sget-object v1, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bandwidth level changing : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-gtz p1, :cond_0

    const-string v0, "LOW"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 298
    if-gtz p1, :cond_1

    .line 299
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->setFileTransferLimit(I)V

    .line 303
    :goto_1
    return-void

    .line 297
    :cond_0
    const-string v0, "NORMAL"

    goto :goto_0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/device/connection/iAP2MusicHelper;->iapFileTransferManager:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->setFileTransferLimit(I)V

    goto :goto_1
.end method
