.class final Lcom/navdy/hud/device/connection/IAPMessageUtility$2;
.super Ljava/util/HashMap;
.source "IAPMessageUtility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/device/connection/IAPMessageUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;",
        "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 39
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Songs:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Albums:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/device/connection/IAPMessageUtility$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    return-void
.end method
