.class public final Lnet/hockeyapp/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/hockeyapp/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final hockeyapp_crash_dialog_app_name_fallback:I = 0x7f090134

.field public static final hockeyapp_crash_dialog_message:I = 0x7f090135

.field public static final hockeyapp_crash_dialog_negative_button:I = 0x7f090136

.field public static final hockeyapp_crash_dialog_neutral_button:I = 0x7f090137

.field public static final hockeyapp_crash_dialog_positive_button:I = 0x7f090138

.field public static final hockeyapp_crash_dialog_title:I = 0x7f090139

.field public static final hockeyapp_dialog_error_message:I = 0x7f09013a

.field public static final hockeyapp_dialog_error_title:I = 0x7f09013b

.field public static final hockeyapp_dialog_negative_button:I = 0x7f09013c

.field public static final hockeyapp_dialog_positive_button:I = 0x7f09013d

.field public static final hockeyapp_download_failed_dialog_message:I = 0x7f09013e

.field public static final hockeyapp_download_failed_dialog_negative_button:I = 0x7f09013f

.field public static final hockeyapp_download_failed_dialog_positive_button:I = 0x7f090140

.field public static final hockeyapp_download_failed_dialog_title:I = 0x7f090141

.field public static final hockeyapp_error_no_network_message:I = 0x7f090142

.field public static final hockeyapp_expiry_info_text:I = 0x7f090143

.field public static final hockeyapp_expiry_info_title:I = 0x7f090144

.field public static final hockeyapp_feedback_attach_file:I = 0x7f090145

.field public static final hockeyapp_feedback_attach_picture:I = 0x7f090146

.field public static final hockeyapp_feedback_attachment_button_text:I = 0x7f090147

.field public static final hockeyapp_feedback_attachment_error:I = 0x7f090148

.field public static final hockeyapp_feedback_attachment_loading:I = 0x7f090149

.field public static final hockeyapp_feedback_email_hint:I = 0x7f09014a

.field public static final hockeyapp_feedback_failed_text:I = 0x7f09014b

.field public static final hockeyapp_feedback_failed_title:I = 0x7f09014c

.field public static final hockeyapp_feedback_fetching_feedback_text:I = 0x7f09014d

.field public static final hockeyapp_feedback_generic_error:I = 0x7f09014e

.field public static final hockeyapp_feedback_last_updated_text:I = 0x7f09014f

.field public static final hockeyapp_feedback_max_attachments_allowed:I = 0x7f090150

.field public static final hockeyapp_feedback_message_hint:I = 0x7f090151

.field public static final hockeyapp_feedback_name_hint:I = 0x7f090152

.field public static final hockeyapp_feedback_refresh_button_text:I = 0x7f090153

.field public static final hockeyapp_feedback_response_button_text:I = 0x7f090154

.field public static final hockeyapp_feedback_select_file:I = 0x7f090155

.field public static final hockeyapp_feedback_select_picture:I = 0x7f090156

.field public static final hockeyapp_feedback_send_button_text:I = 0x7f090157

.field public static final hockeyapp_feedback_send_generic_error:I = 0x7f090158

.field public static final hockeyapp_feedback_send_network_error:I = 0x7f090159

.field public static final hockeyapp_feedback_sending_feedback_text:I = 0x7f09015a

.field public static final hockeyapp_feedback_subject_hint:I = 0x7f09015b

.field public static final hockeyapp_feedback_title:I = 0x7f09015c

.field public static final hockeyapp_feedback_validate_email_empty:I = 0x7f09015d

.field public static final hockeyapp_feedback_validate_email_error:I = 0x7f09015e

.field public static final hockeyapp_feedback_validate_name_error:I = 0x7f09015f

.field public static final hockeyapp_feedback_validate_subject_error:I = 0x7f090160

.field public static final hockeyapp_feedback_validate_text_error:I = 0x7f090161

.field public static final hockeyapp_login_email_hint:I = 0x7f090162

.field public static final hockeyapp_login_headline_text:I = 0x7f090163

.field public static final hockeyapp_login_headline_text_email_only:I = 0x7f090164

.field public static final hockeyapp_login_login_button_text:I = 0x7f090165

.field public static final hockeyapp_login_missing_credentials_toast:I = 0x7f090166

.field public static final hockeyapp_login_password_hint:I = 0x7f090167

.field public static final hockeyapp_paint_dialog_message:I = 0x7f090168

.field public static final hockeyapp_paint_dialog_negative_button:I = 0x7f090169

.field public static final hockeyapp_paint_dialog_neutral_button:I = 0x7f09016a

.field public static final hockeyapp_paint_dialog_positive_button:I = 0x7f09016b

.field public static final hockeyapp_paint_indicator_toast:I = 0x7f09016c

.field public static final hockeyapp_paint_menu_clear:I = 0x7f09016d

.field public static final hockeyapp_paint_menu_save:I = 0x7f09016e

.field public static final hockeyapp_paint_menu_undo:I = 0x7f09016f

.field public static final hockeyapp_permission_dialog_negative_button:I = 0x7f090170

.field public static final hockeyapp_permission_dialog_positive_button:I = 0x7f090171

.field public static final hockeyapp_permission_update_message:I = 0x7f090172

.field public static final hockeyapp_permission_update_title:I = 0x7f090173

.field public static final hockeyapp_update_button:I = 0x7f090174

.field public static final hockeyapp_update_dialog_message:I = 0x7f090175

.field public static final hockeyapp_update_dialog_negative_button:I = 0x7f090176

.field public static final hockeyapp_update_dialog_positive_button:I = 0x7f090177

.field public static final hockeyapp_update_dialog_title:I = 0x7f090178

.field public static final hockeyapp_update_mandatory_toast:I = 0x7f090179

.field public static final hockeyapp_update_version_details_label:I = 0x7f09032c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
