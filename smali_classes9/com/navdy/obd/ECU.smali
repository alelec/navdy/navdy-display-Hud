.class public Lcom/navdy/obd/ECU;
.super Ljava/lang/Object;
.source "ECU.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final address:I

.field public final supportedPids:Lcom/navdy/obd/PidSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/obd/ECU$1;

    invoke-direct {v0}, Lcom/navdy/obd/ECU$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/ECU;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/navdy/obd/PidSet;)V
    .locals 0
    .param p1, "address"    # I
    .param p2, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/navdy/obd/ECU;->address:I

    .line 16
    iput-object p2, p0, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/obd/ECU;->address:I

    .line 26
    const-class v0, Lcom/navdy/obd/PidSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/PidSet;

    iput-object v0, p0, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    .line 27
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/obd/ECU;->address:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 32
    iget-object v0, p0, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 33
    return-void
.end method
