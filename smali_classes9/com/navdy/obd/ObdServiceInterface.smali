.class public Lcom/navdy/obd/ObdServiceInterface;
.super Ljava/lang/Object;
.source "ObdServiceInterface.java"


# static fields
.field public static final ACTION_RESCAN:Ljava/lang/String; = "com.navdy.obd.action.RESCAN"

.field public static final ACTION_START_AUTO_CONNECT:Ljava/lang/String; = "com.navdy.obd.action.START_AUTO_CONNECT"

.field public static final ACTION_STOP_AUTO_CONNECT:Ljava/lang/String; = "com.navdy.obd.action.STOP_AUTO_CONNECT"

.field public static final CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE:Ljava/lang/String; = "BAD_STATE"

.field public static final GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE:Ljava/lang/String; = "NO_DATA"

.field public static final MODE_J1939:I = 0x1

.field public static final MODE_OBD2:I = 0x0

.field public static final OBD_CLASS_NAME:Ljava/lang/String; = "com.navdy.obd.ObdService"

.field public static final OBD_PACKAGE_NAME:Ljava/lang/String; = "com.navdy.obd.app.ObdTestApp"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getObdServiceComponent()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.navdy.obd.app.ObdTestApp"

    const-string v2, "com.navdy.obd.ObdService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static startObdService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/navdy/obd/ObdServiceInterface;->getObdServiceComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 33
    const-string v1, "com.navdy.obd.action.START_AUTO_CONNECT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 35
    return-void
.end method

.method public static stopObdService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 39
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/navdy/obd/ObdServiceInterface;->getObdServiceComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 40
    const-string v1, "com.navdy.obd.action.STOP_AUTO_CONNECT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 42
    return-void
.end method
