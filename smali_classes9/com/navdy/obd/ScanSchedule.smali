.class public final Lcom/navdy/obd/ScanSchedule;
.super Ljava/lang/Object;
.source "ScanSchedule.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/ScanSchedule$Scan;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/ScanSchedule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field schedule:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/obd/ScanSchedule$Scan;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/navdy/obd/ScanSchedule$1;

    invoke-direct {v0}, Lcom/navdy/obd/ScanSchedule$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/ScanSchedule;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p0, p1}, Lcom/navdy/obd/ScanSchedule;->readFromParcel(Landroid/os/Parcel;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/navdy/obd/ScanSchedule$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/navdy/obd/ScanSchedule$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/obd/ScanSchedule;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/ScanSchedule;)V
    .locals 2
    .param p1, "source"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    .line 39
    return-void
.end method


# virtual methods
.method public addPid(II)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "scanInterval"    # I

    .prologue
    .line 60
    iget-object v1, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 61
    .local v0, "oldScan":Lcom/navdy/obd/ScanSchedule$Scan;
    if-eqz v0, :cond_1

    .line 62
    iget v1, v0, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    if-ge p2, v1, :cond_0

    .line 63
    iput p2, v0, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/navdy/obd/ScanSchedule$Scan;

    invoke-direct {v3, p1, p2}, Lcom/navdy/obd/ScanSchedule$Scan;-><init>(II)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public addPids(Ljava/util/List;I)V
    .locals 3
    .param p2, "scanInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 72
    .local v0, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v2

    invoke-virtual {p0, v2, p2}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    goto :goto_0

    .line 74
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public getScanList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ScanSchedule$Scan;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public merge(Lcom/navdy/obd/ScanSchedule;)V
    .locals 5
    .param p1, "otherSchedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 49
    iget-object v2, p1, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 50
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 51
    .local v1, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    iget v3, v1, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    iget v4, v1, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    invoke-virtual {p0, v3, v4}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    goto :goto_0

    .line 53
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    .end local v1    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_0
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 98
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 100
    .local v2, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 102
    .local v3, "pid":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 103
    .local v1, "interval":I
    invoke-virtual {p0, v3, v1}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "interval":I
    .end local v3    # "pid":I
    :cond_0
    return-void
.end method

.method public remove(I)Lcom/navdy/obd/ScanSchedule$Scan;
    .locals 2
    .param p1, "pid"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/ScanSchedule$Scan;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "ScanSchedule{ schedule="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    .line 127
    .local v2, "first":Z
    iget-object v4, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 128
    iget-object v4, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 129
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 130
    .local v3, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    if-nez v2, :cond_0

    .line 131
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :goto_1
    const-string v5, "["

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    .line 136
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    .line 137
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    .line 138
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms]"

    .line 139
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 133
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 142
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    .end local v3    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_1
    const-string v4, "null"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_2
    const-string v4, " }"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 89
    iget-object v2, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-object v2, p0, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 91
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ScanSchedule$Scan;

    .line 92
    .local v1, "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    iget v3, v1, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget v3, v1, Lcom/navdy/obd/ScanSchedule$Scan;->scanInterval:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 95
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    .end local v1    # "scan":Lcom/navdy/obd/ScanSchedule$Scan;
    :cond_0
    return-void
.end method
