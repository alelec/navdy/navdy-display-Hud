.class public abstract Lcom/navdy/obd/IObdService$Stub;
.super Landroid/os/Binder;
.source "IObdService.java"

# interfaces
.implements Lcom/navdy/obd/IObdService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/IObdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/IObdService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.navdy.obd.IObdService"

.field static final TRANSACTION_addListener:I = 0xc

.field static final TRANSACTION_connect:I = 0x1

.field static final TRANSACTION_disconnect:I = 0x2

.field static final TRANSACTION_getFirmwareVersion:I = 0x4

.field static final TRANSACTION_getState:I = 0x3

.field static final TRANSACTION_readPid:I = 0x7

.field static final TRANSACTION_removeListener:I = 0xd

.field static final TRANSACTION_reset:I = 0x6

.field static final TRANSACTION_scanPids:I = 0xa

.field static final TRANSACTION_scanSupportedPids:I = 0x8

.field static final TRANSACTION_scanVIN:I = 0x9

.field static final TRANSACTION_startRawScan:I = 0xb

.field static final TRANSACTION_updateDeviceFirmware:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.navdy.obd.IObdService"

    invoke-virtual {p0, p0, v0}, Lcom/navdy/obd/IObdService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.navdy.obd.IObdService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/navdy/obd/IObdService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/navdy/obd/IObdService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/navdy/obd/IObdService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/navdy/obd/IObdService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 159
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/obd/IObdServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdServiceListener;

    move-result-object v2

    .line 52
    .local v2, "_arg1":Lcom/navdy/obd/IObdServiceListener;
    invoke-virtual {p0, v0, v2}, Lcom/navdy/obd/IObdService$Stub;->connect(Ljava/lang/String;Lcom/navdy/obd/IObdServiceListener;)V

    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 58
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/navdy/obd/IObdServiceListener;
    :sswitch_2
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->disconnect()V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 65
    :sswitch_3
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->getState()I

    move-result v3

    .line 67
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 73
    .end local v3    # "_result":I
    :sswitch_4
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v3

    .line 75
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v3    # "_result":Ljava/lang/String;
    :sswitch_5
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 85
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/navdy/obd/IObdService$Stub;->updateDeviceFirmware(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 87
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 88
    if-eqz v3, :cond_0

    move v4, v5

    :goto_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 93
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Z
    :sswitch_6
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->reset()V

    .line 95
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 100
    :sswitch_7
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 103
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/navdy/obd/IObdService$Stub;->readPid(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 105
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 110
    .end local v0    # "_arg0":I
    .end local v3    # "_result":Ljava/lang/String;
    :sswitch_8
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->scanSupportedPids()V

    .line 112
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 117
    :sswitch_9
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->scanVIN()V

    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 124
    :sswitch_a
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    sget-object v4, Lcom/navdy/obd/Pid;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 128
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 129
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/navdy/obd/IObdService$Stub;->scanPids(Ljava/util/List;I)V

    .line 130
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 135
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v2    # "_arg1":I
    :sswitch_b
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/navdy/obd/IObdService$Stub;->startRawScan()V

    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 142
    :sswitch_c
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/obd/IObdServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdServiceListener;

    move-result-object v0

    .line 145
    .local v0, "_arg0":Lcom/navdy/obd/IObdServiceListener;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/IObdService$Stub;->addListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 151
    .end local v0    # "_arg0":Lcom/navdy/obd/IObdServiceListener;
    :sswitch_d
    const-string v4, "com.navdy.obd.IObdService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/obd/IObdServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/IObdServiceListener;

    move-result-object v0

    .line 154
    .restart local v0    # "_arg0":Lcom/navdy/obd/IObdServiceListener;
    invoke-virtual {p0, v0}, Lcom/navdy/obd/IObdService$Stub;->removeListener(Lcom/navdy/obd/IObdServiceListener;)V

    .line 155
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
