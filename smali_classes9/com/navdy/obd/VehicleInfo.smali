.class public Lcom/navdy/obd/VehicleInfo;
.super Ljava/lang/Object;
.source "VehicleInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/VehicleInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final ecus:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation
.end field

.field public final isCheckEngineLightOn:Z

.field private primaryEcu:Lcom/navdy/obd/ECU;

.field public final protocol:Ljava/lang/String;

.field public final troubleCodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final vin:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/navdy/obd/VehicleInfo$1;

    invoke-direct {v0}, Lcom/navdy/obd/VehicleInfo$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/VehicleInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v0, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/VehicleInfo;->protocol:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    .line 43
    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 44
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v1, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1, v1, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 48
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 49
    return-void

    .line 45
    .end local v1    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "vin"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 23
    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/VehicleInfo;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;)V
    .locals 1
    .param p1, "protocol"    # Ljava/lang/String;
    .param p3, "vin"    # Ljava/lang/String;
    .param p4, "isCheckEngineLightOn"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .local p5, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/navdy/obd/VehicleInfo;->protocol:Ljava/lang/String;

    .line 28
    if-eqz p2, :cond_0

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    .line 29
    iput-object p3, p0, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    .line 30
    iput-boolean p4, p0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    .line 31
    iput-object p5, p0, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    .line 32
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getEcus()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    return-object v0
.end method

.method public getPrimaryEcu()Lcom/navdy/obd/ECU;
    .locals 7

    .prologue
    .line 53
    iget-object v4, p0, Lcom/navdy/obd/VehicleInfo;->primaryEcu:Lcom/navdy/obd/ECU;

    if-nez v4, :cond_2

    .line 54
    const/4 v1, 0x0

    .line 55
    .local v1, "bestPidCount":I
    const/4 v0, 0x0

    .line 56
    .local v0, "bestEcu":Lcom/navdy/obd/ECU;
    iget-object v4, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/ECU;

    .line 57
    .local v2, "ecu":Lcom/navdy/obd/ECU;
    iget-object v5, v2, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    invoke-virtual {v5}, Lcom/navdy/obd/PidSet;->size()I

    move-result v3

    .line 58
    .local v3, "pidCount":I
    if-le v3, v1, :cond_0

    iget-object v5, v2, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    move-object v0, v2

    .line 60
    move v1, v3

    goto :goto_0

    .line 63
    .end local v2    # "ecu":Lcom/navdy/obd/ECU;
    .end local v3    # "pidCount":I
    :cond_1
    iput-object v0, p0, Lcom/navdy/obd/VehicleInfo;->primaryEcu:Lcom/navdy/obd/ECU;

    .line 65
    .end local v0    # "bestEcu":Lcom/navdy/obd/ECU;
    .end local v1    # "bestPidCount":I
    :cond_2
    iget-object v4, p0, Lcom/navdy/obd/VehicleInfo;->primaryEcu:Lcom/navdy/obd/ECU;

    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/obd/VehicleInfo;->protocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/navdy/obd/VehicleInfo;->vin:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/navdy/obd/VehicleInfo;->ecus:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78
    iget-boolean v0, p0, Lcom/navdy/obd/VehicleInfo;->isCheckEngineLightOn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 79
    iget-object v0, p0, Lcom/navdy/obd/VehicleInfo;->troubleCodes:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 80
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
