.class public Lcom/navdy/obd/PidSet;
.super Ljava/lang/Object;
.source "PidSet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/PidSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_PID:I = 0x140


# instance fields
.field private pids:Ljava/util/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lcom/navdy/obd/PidSet$1;

    invoke-direct {v0}, Lcom/navdy/obd/PidSet$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/PidSet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/BitSet;

    const/16 v1, 0x140

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    .line 25
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 9
    .param p1, "bitField"    # J
    .param p3, "offset"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/navdy/obd/PidSet;-><init>()V

    .line 36
    const/4 v0, 0x0

    .local v0, "bit":I
    :goto_0
    const/16 v1, 0x20

    if-ge v0, v1, :cond_1

    .line 37
    const/4 v1, 0x1

    rsub-int/lit8 v4, v0, 0x1f

    shl-int/2addr v1, v4

    int-to-long v2, v1

    .line 38
    .local v2, "mask":J
    and-long v4, p1, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    add-int v4, p3, v0

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    .end local v2    # "mask":J
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    .line 123
    .local v1, "len":B
    new-array v0, v1, [J

    .line 124
    .local v0, "bits":[J
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readLongArray([J)V

    .line 125
    invoke-static {v0}, Ljava/util/BitSet;->valueOf([J)Ljava/util/BitSet;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    .line 126
    return-void
.end method

.method public constructor <init>(Lcom/navdy/obd/ScanSchedule;)V
    .locals 4
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/obd/PidSet;-><init>()V

    .line 47
    iget-object v1, p1, Lcom/navdy/obd/ScanSchedule;->schedule:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 48
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    iget-object v3, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/obd/ScanSchedule$Scan;

    iget v1, v1, Lcom/navdy/obd/ScanSchedule$Scan;->pid:I

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 50
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/navdy/obd/ScanSchedule$Scan;>;"
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-direct {p0}, Lcom/navdy/obd/PidSet;-><init>()V

    .line 55
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid;

    .line 56
    .local v0, "pid":Lcom/navdy/obd/Pid;
    iget-object v2, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 58
    .end local v0    # "pid":Lcom/navdy/obd/Pid;
    :cond_0
    return-void
.end method


# virtual methods
.method public add(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    .line 70
    return-void
.end method

.method public add(Lcom/navdy/obd/Pid;)V
    .locals 1
    .param p1, "pid"    # Lcom/navdy/obd/Pid;

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/obd/PidSet;->add(I)V

    .line 62
    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 97
    iget-object v3, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->cardinality()I

    move-result v2

    .line 98
    .local v2, "size":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    const/4 v0, 0x0

    .line 101
    .local v0, "index":I
    :cond_0
    :goto_0
    const/16 v3, 0x140

    if-ge v0, v3, :cond_1

    if-eq v0, v4, :cond_1

    .line 102
    iget-object v3, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v3, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    .line 103
    if-eq v0, v4, :cond_0

    .line 104
    new-instance v3, Lcom/navdy/obd/Pid;

    invoke-direct {v3, v0}, Lcom/navdy/obd/Pid;-><init>(I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    return-object v1
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 88
    return-void
.end method

.method public contains(I)Z
    .locals 1
    .param p1, "pidId"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public contains(Lcom/navdy/obd/Pid;)Z
    .locals 2
    .param p1, "pid"    # Lcom/navdy/obd/Pid;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {p1}, Lcom/navdy/obd/Pid;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 149
    instance-of v0, p1, Lcom/navdy/obd/PidSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    check-cast p1, Lcom/navdy/obd/PidSet;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public merge(Lcom/navdy/obd/PidSet;)V
    .locals 2
    .param p1, "pidSet"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    iget-object v1, p1, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 84
    return-void
.end method

.method public nextPid(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    return v0
.end method

.method public remove(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->clear(I)V

    .line 66
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 130
    iget-object v1, p0, Lcom/navdy/obd/PidSet;->pids:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->toLongArray()[J

    move-result-object v0

    .line 131
    .local v0, "bits":[J
    array-length v1, v0

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 132
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 133
    return-void
.end method
