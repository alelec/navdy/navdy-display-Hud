.class final enum Lcom/navdy/obd/Units$System;
.super Ljava/lang/Enum;
.source "Units.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/Units;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "System"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/obd/Units$System;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/obd/Units$System;

.field public static final enum Metric:Lcom/navdy/obd/Units$System;

.field public static final enum None:Lcom/navdy/obd/Units$System;

.field public static final enum US:Lcom/navdy/obd/Units$System;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/navdy/obd/Units$System;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/Units$System;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Units$System;->None:Lcom/navdy/obd/Units$System;

    new-instance v0, Lcom/navdy/obd/Units$System;

    const-string v1, "Metric"

    invoke-direct {v0, v1, v3}, Lcom/navdy/obd/Units$System;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Units$System;->Metric:Lcom/navdy/obd/Units$System;

    new-instance v0, Lcom/navdy/obd/Units$System;

    const-string v1, "US"

    invoke-direct {v0, v1, v4}, Lcom/navdy/obd/Units$System;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Units$System;->US:Lcom/navdy/obd/Units$System;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/obd/Units$System;

    sget-object v1, Lcom/navdy/obd/Units$System;->None:Lcom/navdy/obd/Units$System;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/obd/Units$System;->Metric:Lcom/navdy/obd/Units$System;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/obd/Units$System;->US:Lcom/navdy/obd/Units$System;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/obd/Units$System;->$VALUES:[Lcom/navdy/obd/Units$System;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/obd/Units$System;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/navdy/obd/Units$System;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Units$System;

    return-object v0
.end method

.method public static values()[Lcom/navdy/obd/Units$System;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/obd/Units$System;->$VALUES:[Lcom/navdy/obd/Units$System;

    invoke-virtual {v0}, [Lcom/navdy/obd/Units$System;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/obd/Units$System;

    return-object v0
.end method
