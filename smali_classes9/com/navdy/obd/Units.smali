.class public final enum Lcom/navdy/obd/Units;
.super Ljava/lang/Enum;
.source "Units.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/Units$System;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/obd/Units;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/obd/Units;

.field public static final enum KILOMETERS_PER_HOUR:Lcom/navdy/obd/Units;

.field public static final enum MILES_PER_HOUR:Lcom/navdy/obd/Units;

.field public static final enum NONE:Lcom/navdy/obd/Units;


# instance fields
.field protected abbreviation:Ljava/lang/String;

.field protected description:Ljava/lang/String;

.field protected system:Lcom/navdy/obd/Units$System;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/navdy/obd/Units;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/obd/Units;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Units;->NONE:Lcom/navdy/obd/Units;

    .line 15
    new-instance v0, Lcom/navdy/obd/Units;

    const-string v1, "MILES_PER_HOUR"

    sget-object v2, Lcom/navdy/obd/Units$System;->US:Lcom/navdy/obd/Units$System;

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/obd/Units;-><init>(Ljava/lang/String;ILcom/navdy/obd/Units$System;)V

    sput-object v0, Lcom/navdy/obd/Units;->MILES_PER_HOUR:Lcom/navdy/obd/Units;

    .line 16
    new-instance v0, Lcom/navdy/obd/Units;

    const-string v1, "KILOMETERS_PER_HOUR"

    sget-object v2, Lcom/navdy/obd/Units$System;->Metric:Lcom/navdy/obd/Units$System;

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/obd/Units;-><init>(Ljava/lang/String;ILcom/navdy/obd/Units$System;)V

    sput-object v0, Lcom/navdy/obd/Units;->KILOMETERS_PER_HOUR:Lcom/navdy/obd/Units;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/obd/Units;

    sget-object v1, Lcom/navdy/obd/Units;->NONE:Lcom/navdy/obd/Units;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/obd/Units;->MILES_PER_HOUR:Lcom/navdy/obd/Units;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/obd/Units;->KILOMETERS_PER_HOUR:Lcom/navdy/obd/Units;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/obd/Units;->$VALUES:[Lcom/navdy/obd/Units;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/obd/Units$System;->None:Lcom/navdy/obd/Units$System;

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/obd/Units;-><init>(Ljava/lang/String;ILcom/navdy/obd/Units$System;)V

    .line 43
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/navdy/obd/Units$System;)V
    .locals 6
    .param p3, "system"    # Lcom/navdy/obd/Units$System;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/obd/Units$System;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    const-string v3, ""

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/obd/Units;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/obd/Units$System;)V

    .line 39
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/obd/Units$System;)V
    .locals 0
    .param p3, "abbreviation"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "system"    # Lcom/navdy/obd/Units$System;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/obd/Units$System;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lcom/navdy/obd/Units;->abbreviation:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Lcom/navdy/obd/Units;->description:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Lcom/navdy/obd/Units;->system:Lcom/navdy/obd/Units$System;

    .line 49
    return-void
.end method

.method static localize(Landroid/content/res/XmlResourceParser;)V
    .locals 7
    .param p0, "xpp"    # Landroid/content/res/XmlResourceParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "currentUnit":Lcom/navdy/obd/Units;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    .line 75
    .local v1, "eventType":I
    :goto_0
    const/4 v5, 0x1

    if-eq v1, v5, :cond_6

    .line 76
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 77
    .local v3, "tagName":Ljava/lang/String;
    const/4 v5, 0x2

    if-ne v1, v5, :cond_1

    .line 78
    const-string v5, "unit"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    const/4 v5, 0x0

    const-string v6, "id"

    invoke-interface {p0, v5, v6}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/obd/Units;->valueOf(Ljava/lang/String;)Lcom/navdy/obd/Units;

    move-result-object v0

    .line 97
    :cond_0
    :goto_1
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 98
    goto :goto_0

    .line 81
    :cond_1
    const/4 v5, 0x3

    if-ne v1, v5, :cond_5

    .line 82
    if-eqz v0, :cond_2

    .line 83
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, "tagValue":Ljava/lang/String;
    const-string v5, "abbreviation"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 85
    invoke-virtual {v0, v4}, Lcom/navdy/obd/Units;->setAbbreviation(Ljava/lang/String;)V

    .line 90
    .end local v4    # "tagValue":Ljava/lang/String;
    :cond_2
    :goto_2
    const-string v5, "unit"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 91
    const/4 v0, 0x0

    .line 93
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .line 86
    .restart local v4    # "tagValue":Ljava/lang/String;
    :cond_4
    const-string v5, "description"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 87
    invoke-virtual {v0, v4}, Lcom/navdy/obd/Units;->setDescription(Ljava/lang/String;)V

    goto :goto_2

    .line 94
    .end local v4    # "tagValue":Ljava/lang/String;
    :cond_5
    const/4 v5, 0x4

    if-ne v1, v5, :cond_0

    .line 95
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 99
    .end local v3    # "tagName":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/obd/Units;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/navdy/obd/Units;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Units;

    return-object v0
.end method

.method public static values()[Lcom/navdy/obd/Units;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/navdy/obd/Units;->$VALUES:[Lcom/navdy/obd/Units;

    invoke-virtual {v0}, [Lcom/navdy/obd/Units;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/obd/Units;

    return-object v0
.end method


# virtual methods
.method public getAbbreviation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/obd/Units;->abbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/obd/Units;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getSystem()Lcom/navdy/obd/Units$System;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/obd/Units;->system:Lcom/navdy/obd/Units$System;

    return-object v0
.end method

.method public setAbbreviation(Ljava/lang/String;)V
    .locals 0
    .param p1, "abbreviation"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/obd/Units;->abbreviation:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/navdy/obd/Units;->description:Ljava/lang/String;

    .line 65
    return-void
.end method
