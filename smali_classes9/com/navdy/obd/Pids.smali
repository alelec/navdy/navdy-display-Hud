.class public Lcom/navdy/obd/Pids;
.super Ljava/lang/Object;
.source "Pids.java"


# static fields
.field public static final CUSTOM_PIDS:I = 0x40

.field public static final CUSTOM_PID_START:I = 0x100

.field public static final DISTANCE_TO_EMPTY:I = 0x101

.field public static final DISTANCE_TRAVELLED:I = 0x31

.field public static final ENGINE_COOLANT_TEMPERATURE:I = 0x5

.field public static final ENGINE_OIL_PRESSURE:I = 0x102

.field public static final ENGINE_RPM:I = 0xc

.field public static final ENGINE_TRIP_FUEL:I = 0x103

.field public static final FUEL_LEVEL:I = 0x2f

.field public static final FUEL_SYSTEM_STATUS:I = 0x3

.field public static final INSTANTANEOUS_FUEL_CONSUMPTION:I = 0x100

.field public static final INTAKE_AIR_TEMPERATURE:I = 0xf

.field public static final MANIFOLD_AIR_PRESSURE:I = 0xb

.field public static final MASS_AIRFLOW:I = 0x10

.field public static final MAX_PID:I = 0x140

.field public static final THROTTLE_POSITION:I = 0x11

.field public static final TOTAL_VEHICLE_DISTANCE:I = 0x104

.field public static final VEHICLE_SPEED:I = 0xd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
