.class public Lcom/navdy/obd/VoltageSettings;
.super Ljava/lang/Object;
.source "VoltageSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/VoltageSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHARGING_VOLTAGE:F = 13.1f

.field public static final DEFAULT_ENGINE_OFF_VOLTAGE:F = 12.9f

.field public static final DEFAULT_LOW_BATTERY_VOLTAGE:F = 12.2f


# instance fields
.field public final chargingVoltage:F

.field public final engineOffVoltage:F

.field public final lowBatteryVoltage:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/navdy/obd/VoltageSettings$1;

    invoke-direct {v0}, Lcom/navdy/obd/VoltageSettings$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/VoltageSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const v0, 0x41433333    # 12.2f

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    .line 29
    const v0, 0x414e6666    # 12.9f

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->engineOffVoltage:F

    .line 30
    const v0, 0x4151999a    # 13.1f

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    .line 31
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "lowerBatteryVoltage"    # F
    .param p2, "engineOffVoltage"    # F
    .param p3, "chargingVoltage"    # F

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    .line 35
    iput p2, p0, Lcom/navdy/obd/VoltageSettings;->engineOffVoltage:F

    .line 36
    iput p3, p0, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->engineOffVoltage:F

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    .line 43
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "VoltageSettings{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string v1, "lowBatteryVoltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 73
    const-string v1, ", engineOffVoltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/VoltageSettings;->engineOffVoltage:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 74
    const-string v1, ", chargingVoltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 75
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/obd/VoltageSettings;->lowBatteryVoltage:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 53
    iget v0, p0, Lcom/navdy/obd/VoltageSettings;->engineOffVoltage:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 54
    iget v0, p0, Lcom/navdy/obd/VoltageSettings;->chargingVoltage:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 55
    return-void
.end method
