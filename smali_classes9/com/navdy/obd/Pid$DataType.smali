.class final enum Lcom/navdy/obd/Pid$DataType;
.super Ljava/lang/Enum;
.source "Pid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/obd/Pid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/obd/Pid$DataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/obd/Pid$DataType;

.field public static final enum BOOL:Lcom/navdy/obd/Pid$DataType;

.field public static final enum FLOAT:Lcom/navdy/obd/Pid$DataType;

.field public static final enum INT:Lcom/navdy/obd/Pid$DataType;

.field public static final enum PERCENTAGE:Lcom/navdy/obd/Pid$DataType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/navdy/obd/Pid$DataType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v2}, Lcom/navdy/obd/Pid$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Pid$DataType;->INT:Lcom/navdy/obd/Pid$DataType;

    new-instance v0, Lcom/navdy/obd/Pid$DataType;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/obd/Pid$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Pid$DataType;->FLOAT:Lcom/navdy/obd/Pid$DataType;

    new-instance v0, Lcom/navdy/obd/Pid$DataType;

    const-string v1, "PERCENTAGE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/obd/Pid$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Pid$DataType;->PERCENTAGE:Lcom/navdy/obd/Pid$DataType;

    new-instance v0, Lcom/navdy/obd/Pid$DataType;

    const-string v1, "BOOL"

    invoke-direct {v0, v1, v5}, Lcom/navdy/obd/Pid$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/obd/Pid$DataType;->BOOL:Lcom/navdy/obd/Pid$DataType;

    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/obd/Pid$DataType;

    sget-object v1, Lcom/navdy/obd/Pid$DataType;->INT:Lcom/navdy/obd/Pid$DataType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/obd/Pid$DataType;->FLOAT:Lcom/navdy/obd/Pid$DataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/obd/Pid$DataType;->PERCENTAGE:Lcom/navdy/obd/Pid$DataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/obd/Pid$DataType;->BOOL:Lcom/navdy/obd/Pid$DataType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/obd/Pid$DataType;->$VALUES:[Lcom/navdy/obd/Pid$DataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/obd/Pid$DataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/navdy/obd/Pid$DataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/obd/Pid$DataType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/obd/Pid$DataType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/obd/Pid$DataType;->$VALUES:[Lcom/navdy/obd/Pid$DataType;

    invoke-virtual {v0}, [Lcom/navdy/obd/Pid$DataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/obd/Pid$DataType;

    return-object v0
.end method
