.class public Lcom/navdy/obd/Pid;
.super Ljava/lang/Object;
.source "Pid.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/obd/Pid$DataType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_DATA:I = -0x80000000


# instance fields
.field protected dataType:Lcom/navdy/obd/Pid$DataType;

.field protected id:I

.field protected name:Ljava/lang/String;

.field protected timeStamp:J

.field protected units:Lcom/navdy/obd/Units;

.field protected value:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/navdy/obd/Pid$1;

    invoke-direct {v0}, Lcom/navdy/obd/Pid$1;-><init>()V

    sput-object v0, Lcom/navdy/obd/Pid;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 8
    .param p1, "id"    # I

    .prologue
    .line 29
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    sget-object v6, Lcom/navdy/obd/Pid$DataType;->INT:Lcom/navdy/obd/Pid$DataType;

    sget-object v7, Lcom/navdy/obd/Units;->NONE:Lcom/navdy/obd/Units;

    move-object v1, p0

    move v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/navdy/obd/Pid;-><init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;)V

    .line 30
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-wide/16 v4, 0x0

    sget-object v6, Lcom/navdy/obd/Pid$DataType;->INT:Lcom/navdy/obd/Pid$DataType;

    sget-object v7, Lcom/navdy/obd/Units;->NONE:Lcom/navdy/obd/Units;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/navdy/obd/Pid;-><init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;)V

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;)V
    .locals 10
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "value"    # D
    .param p5, "dataType"    # Lcom/navdy/obd/Pid$DataType;
    .param p6, "units"    # Lcom/navdy/obd/Units;

    .prologue
    .line 37
    const-wide/16 v4, 0x0

    sget-object v6, Lcom/navdy/obd/Pid$DataType;->INT:Lcom/navdy/obd/Pid$DataType;

    sget-object v7, Lcom/navdy/obd/Units;->NONE:Lcom/navdy/obd/Units;

    const-wide/16 v8, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v9}, Lcom/navdy/obd/Pid;-><init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;J)V

    .line 38
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;J)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "value"    # D
    .param p5, "dataType"    # Lcom/navdy/obd/Pid$DataType;
    .param p6, "units"    # Lcom/navdy/obd/Units;
    .param p7, "timeStamp"    # J

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/navdy/obd/Pid;->id:I

    .line 42
    iput-object p2, p0, Lcom/navdy/obd/Pid;->name:Ljava/lang/String;

    .line 43
    iput-wide p3, p0, Lcom/navdy/obd/Pid;->value:D

    .line 44
    iput-object p5, p0, Lcom/navdy/obd/Pid;->dataType:Lcom/navdy/obd/Pid$DataType;

    .line 45
    iput-object p6, p0, Lcom/navdy/obd/Pid;->units:Lcom/navdy/obd/Units;

    .line 46
    iput-wide p7, p0, Lcom/navdy/obd/Pid;->timeStamp:J

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/obd/Pid$DataType;->valueOf(Ljava/lang/String;)Lcom/navdy/obd/Pid$DataType;

    move-result-object v6

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/obd/Units;->valueOf(Ljava/lang/String;)Lcom/navdy/obd/Units;

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    move-object v1, p0

    .line 71
    invoke-direct/range {v1 .. v9}, Lcom/navdy/obd/Pid;-><init>(ILjava/lang/String;DLcom/navdy/obd/Pid$DataType;Lcom/navdy/obd/Units;J)V

    .line 74
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 101
    instance-of v0, p1, Lcom/navdy/obd/Pid;

    if-eqz v0, :cond_1

    .line 102
    check-cast p1, Lcom/navdy/obd/Pid;

    .end local p1    # "o":Ljava/lang/Object;
    iget v0, p1, Lcom/navdy/obd/Pid;->id:I

    iget v1, p0, Lcom/navdy/obd/Pid;->id:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDataType()Lcom/navdy/obd/Pid$DataType;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/obd/Pid;->dataType:Lcom/navdy/obd/Pid$DataType;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/navdy/obd/Pid;->id:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/obd/Pid;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/navdy/obd/Pid;->timeStamp:J

    return-wide v0
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/navdy/obd/Pid;->value:D

    return-wide v0
.end method

.method public setTimeStamp(J)V
    .locals 1
    .param p1, "timeStamp"    # J

    .prologue
    .line 119
    iput-wide p1, p0, Lcom/navdy/obd/Pid;->timeStamp:J

    .line 120
    return-void
.end method

.method public setValue(D)V
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/navdy/obd/Pid;->value:D

    .line 61
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Pid{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/obd/Pid;->id:I

    .line 110
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    iget v0, p0, Lcom/navdy/obd/Pid;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget-object v0, p0, Lcom/navdy/obd/Pid;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-wide v0, p0, Lcom/navdy/obd/Pid;->value:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 81
    iget-object v0, p0, Lcom/navdy/obd/Pid;->dataType:Lcom/navdy/obd/Pid$DataType;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid$DataType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/obd/Pid;->units:Lcom/navdy/obd/Units;

    invoke-virtual {v0}, Lcom/navdy/obd/Units;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-wide v0, p0, Lcom/navdy/obd/Pid;->timeStamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 84
    return-void
.end method
