.class public Lutil/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static final END_OF_STREAM:I = -0x1

.field private static final INPUT_BUFFER_SIZE:I = 0x4000

.field private static final TAG:Ljava/lang/String; = "ObdService"

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "charSet"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 36
    .local v1, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x4000

    new-array v0, v3, [B

    .line 38
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "n":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 39
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static loadConfigurationMappingList(Landroid/content/Context;Ljava/lang/String;)[Lutil/Configuration;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 45
    const/4 v3, 0x0

    .line 47
    .local v3, "configurationsList":[Lutil/Configuration;
    :try_start_0
    invoke-static {p0, p1}, Lutil/Util;->readObdAssetFile(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "configurationMapping":Ljava/lang/String;
    const-string v9, "\n"

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "configurationMappingEntries":[Ljava/lang/String;
    array-length v9, v2

    new-array v3, v9, [Lutil/Configuration;

    .line 50
    const/4 v5, 0x0

    .line 51
    .local v5, "i":I
    array-length v9, v2

    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v4, v2, v8

    .line 52
    .local v4, "entry":Ljava/lang/String;
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 53
    .local v7, "parts":[Ljava/lang/String;
    new-instance v0, Lutil/Configuration;

    invoke-direct {v0}, Lutil/Configuration;-><init>()V

    .line 54
    .local v0, "configuration":Lutil/Configuration;
    const/4 v10, 0x0

    aget-object v10, v7, v10

    const/4 v11, 0x2

    invoke-static {v10, v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v10

    iput-object v10, v0, Lutil/Configuration;->pattern:Ljava/util/regex/Pattern;

    .line 55
    const/4 v10, 0x1

    aget-object v10, v7, v10

    iput-object v10, v0, Lutil/Configuration;->configurationName:Ljava/lang/String;

    .line 56
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    aput-object v0, v3, v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    add-int/lit8 v8, v8, 0x1

    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_0

    .line 58
    .end local v0    # "configuration":Lutil/Configuration;
    .end local v1    # "configurationMapping":Ljava/lang/String;
    .end local v2    # "configurationMappingEntries":[Ljava/lang/String;
    .end local v4    # "entry":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "parts":[Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 61
    :cond_0
    return-object v3
.end method

.method public static pickConfiguration([Lutil/Configuration;Ljava/lang/String;)Lutil/Configuration;
    .locals 5
    .param p0, "configurationMapping"    # [Lutil/Configuration;
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    .line 72
    const/4 v1, 0x0

    .line 73
    .local v1, "matchingConfiguration":Lutil/Configuration;
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p0, v2

    .line 74
    .local v0, "configuration":Lutil/Configuration;
    iget-object v4, v0, Lutil/Configuration;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 75
    move-object v1, v0

    .line 79
    .end local v0    # "configuration":Lutil/Configuration;
    :cond_0
    return-object v1

    .line 73
    .restart local v0    # "configuration":Lutil/Configuration;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static readObdAssetFile(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "assetFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 24
    .local v2, "is":Ljava/io/InputStream;
    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Lutil/Util;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-object v0

    .line 27
    :catch_0
    move-exception v1

    .line 28
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "ObdService"

    const-string v4, "Error closing the stream after reading the asset file"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
