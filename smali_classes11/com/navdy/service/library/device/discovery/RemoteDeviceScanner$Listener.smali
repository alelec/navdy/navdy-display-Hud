.class public interface abstract Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$Listener;
.super Ljava/lang/Object;
.source "RemoteDeviceScanner.java"

# interfaces
.implements Lcom/navdy/service/library/util/Listenable$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDiscovered(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onLost(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onScanStarted(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;)V
.end method

.method public abstract onScanStopped(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;)V
.end method
