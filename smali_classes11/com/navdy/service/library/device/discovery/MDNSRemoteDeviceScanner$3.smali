.class Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;
.super Ljava/lang/Object;
.source "MDNSRemoteDeviceScanner.java"

# interfaces
.implements Landroid/net/nsd/NsdManager$ResolveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->getNewResolveListener()Landroid/net/nsd/NsdManager$ResolveListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResolveFailed(Landroid/net/nsd/NsdServiceInfo;I)V
    .locals 3
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;
    .param p2, "errorCode"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v0, v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolveListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resolve failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->resolveComplete()V

    .line 199
    return-void
.end method

.method public onServiceResolved(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 5
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 203
    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolveListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 205
    sget-object v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resolve Succeeded. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 210
    :try_start_0
    invoke-static {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->fromServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 216
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3$1;-><init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->resolveComplete()V

    .line 224
    .end local v0    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_0
    return-void

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to process serviceInfo"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
