.class public Lcom/navdy/service/library/device/RemoteDevice;
.super Lcom/navdy/service/library/util/Listenable;
.source "RemoteDevice.java"

# interfaces
.implements Lcom/navdy/service/library/device/connection/Connection$Listener;
.implements Lcom/navdy/service/library/device/link/LinkListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;,
        Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;,
        Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;,
        Lcom/navdy/service/library/device/RemoteDevice$Listener;,
        Lcom/navdy/service/library/device/RemoteDevice$EventDispatcher;,
        Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/util/Listenable",
        "<",
        "Lcom/navdy/service/library/device/RemoteDevice$Listener;",
        ">;",
        "Lcom/navdy/service/library/device/connection/Connection$Listener;",
        "Lcom/navdy/service/library/device/link/LinkListener;"
    }
.end annotation


# static fields
.field public static final MAX_PACKET_SIZE:I = 0x80000

.field private static final lock:Ljava/lang/Object;

.field public static final sLastSeenDeviceInfo:Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected convertToNavdyEvent:Z

.field public lastMessageSentTime:J

.field protected mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

.field protected mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

.field protected final mContext:Landroid/content/Context;

.field protected mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field protected mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

.field protected final mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;"
        }
    .end annotation
.end field

.field protected final mHandler:Landroid/os/Handler;

.field private mLink:Lcom/navdy/service/library/device/link/Link;

.field private mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

.field private volatile mNetworkLinkReady:Z

.field protected mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mWire:Lcom/squareup/wire/Wire;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    .line 48
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;

    invoke-direct {v0}, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLastSeenDeviceInfo:Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p3, "convertToNavdyEvent"    # Z

    .prologue
    const/4 v3, 0x0

    .line 105
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    .line 98
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 102
    iput-boolean v3, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkLinkReady:Z

    .line 103
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mContext:Landroid/content/Context;

    .line 107
    iput-object p2, p0, Lcom/navdy/service/library/device/RemoteDevice;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mHandler:Landroid/os/Handler;

    .line 109
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 110
    iput-boolean p3, p0, Lcom/navdy/service/library/device/RemoteDevice;->convertToNavdyEvent:Z

    .line 111
    if-eqz p3, :cond_0

    .line 112
    new-instance v0, Lcom/squareup/wire/Wire;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mWire:Lcom/squareup/wire/Wire;

    .line 114
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p3, "convertToNavdyEvent"    # Z

    .prologue
    .line 117
    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/navdy/service/library/device/RemoteDevice;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/NavdyDeviceId;Z)V

    .line 119
    iput-object p2, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 120
    return-void
.end method

.method private dispatchLocalEvent(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 458
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v0

    .line 459
    .local v0, "event":Lcom/navdy/service/library/events/NavdyEvent;
    iget-boolean v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->convertToNavdyEvent:Z

    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 464
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchNavdyEvent([B)V

    goto :goto_0
.end method

.method private dispatchStateChangeEvents(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V
    .locals 4
    .param p1, "newState"    # Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    .param p2, "oldState"    # Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "deviceId":Ljava/lang/String;
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 315
    :pswitch_0
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne p2, v1, :cond_1

    .line 316
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    .line 319
    :cond_1
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    .line 322
    iget-boolean v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkLinkReady:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    new-instance v1, Lcom/navdy/service/library/events/connection/NetworkLinkReady;

    invoke-direct {v1}, Lcom/navdy/service/library/events/connection/NetworkLinkReady;-><init>()V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    .line 324
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "dispatchStateChangeEvents: dispatching the Network Link Ready message"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :pswitch_1
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne p2, v1, :cond_2

    .line 329
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    .line 332
    :cond_2
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 336
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne p2, v1, :cond_3

    .line 337
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 340
    :cond_3
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private markDisconnected(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 0
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeActiveConnection()V

    .line 487
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 488
    return-void
.end method

.method private setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .prologue
    .line 295
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-eq v1, p1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 297
    .local v0, "oldState":Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 299
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v1, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchStateChangeEvents(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    .line 301
    .end local v0    # "oldState":Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    :cond_0
    return-void
.end method


# virtual methods
.method public connect()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-eq v3, v4, :cond_0

    .line 200
    :goto_0
    return v1

    .line 172
    :cond_0
    iget-object v3, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    if-nez v3, :cond_1

    .line 173
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "can\'t connect without a connectionInfo"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v3, p0, Lcom/navdy/service/library/device/RemoteDevice;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-static {v3, v4}, Lcom/navdy/service/library/device/connection/Connection;->instantiateFromConnectionInfo(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/connection/Connection;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/navdy/service/library/device/RemoteDevice;->setActiveConnection(Lcom/navdy/service/library/device/connection/Connection;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 179
    sget-object v3, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 184
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/connection/Connection;->connect()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 185
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v4

    sget-object v5, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v4, v5, :cond_2

    move v0, v2

    .line 186
    .local v0, "shouldDispatch":Z
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    if-eqz v0, :cond_3

    .line 189
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchConnectingEvent()V

    .line 190
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Starting connection"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    move v1, v2

    .line 191
    goto :goto_0

    .end local v0    # "shouldDispatch":Z
    :cond_2
    move v0, v1

    .line 185
    goto :goto_1

    .line 186
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 193
    .restart local v0    # "shouldDispatch":Z
    :cond_3
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to connect"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeActiveConnection()V

    goto :goto_0

    .line 199
    .end local v0    # "shouldDispatch":Z
    :cond_4
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unable to find connection of type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized disconnect()Z
    .locals 3

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v1, v2, :cond_0

    .line 205
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "can\'t disconnect; already disconnected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 206
    const/4 v1, 0x1

    .line 217
    :goto_0
    monitor-exit p0

    return v1

    .line 210
    :cond_0
    :try_start_1
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 211
    :try_start_2
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    .line 212
    .local v0, "oldConnection":Lcom/navdy/service/library/device/connection/Connection;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    .line 213
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214
    if-eqz v0, :cond_1

    .line 215
    :try_start_3
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/Connection;->disconnect()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v1

    goto :goto_0

    .line 213
    .end local v0    # "oldConnection":Lcom/navdy/service/library/device/connection/Connection;
    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 204
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 217
    .restart local v0    # "oldConnection":Lcom/navdy/service/library/device/connection/Connection;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected dispatchConnectEvent()V
    .locals 1

    .prologue
    .line 500
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice$2;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 506
    return-void
.end method

.method protected dispatchConnectFailureEvent(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 1
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 509
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/RemoteDevice$3;-><init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 515
    return-void
.end method

.method protected dispatchConnectingEvent()V
    .locals 1

    .prologue
    .line 491
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice$1;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 497
    return-void
.end method

.method protected dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 1
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 518
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/RemoteDevice$4;-><init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 524
    return-void
.end method

.method protected dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 536
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$6;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/RemoteDevice$6;-><init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 542
    return-void
.end method

.method protected dispatchNavdyEvent([B)V
    .locals 1
    .param p1, "eventData"    # [B

    .prologue
    .line 527
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$5;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/RemoteDevice$5;-><init>(Lcom/navdy/service/library/device/RemoteDevice;[B)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 533
    return-void
.end method

.method public getActiveConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 476
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 477
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/Connection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    monitor-exit v1

    .line 481
    :goto_0
    return-object v0

    .line 480
    :cond_0
    monitor-exit v1

    .line 481
    const/4 v0, 0x0

    goto :goto_0

    .line 480
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    return-object v0
.end method

.method public getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;
    .locals 2

    .prologue
    .line 248
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 249
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-nez v0, :cond_0

    .line 250
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    monitor-exit v1

    .line 252
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/Connection;->getStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object v0
.end method

.method public getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    return-object v0
.end method

.method public getLinkBandwidthLevel()I
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    invoke-interface {v0}, Lcom/navdy/service/library/device/link/Link;->getBandWidthLevel()I

    move-result v0

    .line 287
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getLinkStatus()Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    return-object v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnecting()Z
    .locals 2

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized linkEstablished(Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 3
    .param p1, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 364
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 381
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown connection type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 369
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    .line 370
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/navdy/service/library/device/link/Link;->setBandwidthLevel(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 375
    :pswitch_1
    :try_start_2
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    .line 376
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/navdy/service/library/device/link/Link;->setBandwidthLevel(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 364
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized linkLost(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 3
    .param p1, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 411
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v0, v1, :cond_0

    .line 413
    sget-object p2, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkLinkReady:Z

    .line 418
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 439
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown connection type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 422
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    .line 423
    invoke-direct {p0, p2}, Lcom/navdy/service/library/device/RemoteDevice;->markDisconnected(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 429
    :pswitch_1
    :try_start_2
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkStatus()Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne v0, v1, :cond_1

    .line 431
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    goto :goto_0

    .line 435
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkStatus(Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;)V

    .line 436
    invoke-direct {p0, p2}, Lcom/navdy/service/library/device/RemoteDevice;->markDisconnected(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 418
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConnected(Lcom/navdy/service/library/device/connection/Connection;)V
    .locals 3
    .param p1, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 547
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 548
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-ne v0, p1, :cond_0

    .line 549
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Connected"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 550
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchConnectEvent()V

    .line 554
    :goto_0
    monitor-exit v1

    .line 555
    return-void

    .line 552
    :cond_0
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Received connect event for unknown connection"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onConnectionFailed(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 3
    .param p1, "connection"    # Lcom/navdy/service/library/device/connection/Connection;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 559
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 560
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-ne v0, p1, :cond_0

    .line 561
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeActiveConnection()V

    .line 563
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 564
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p0, p2}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchConnectFailureEvent(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    .line 566
    return-void

    .line 563
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onDisconnected(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 4
    .param p1, "connection"    # Lcom/navdy/service/library/device/connection/Connection;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 570
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 571
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-ne v1, p1, :cond_0

    .line 572
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeActiveConnection()V

    .line 574
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/Connection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    .line 576
    .local v0, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Disconnected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v0, :cond_1

    .end local v0    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 577
    invoke-virtual {p0, p2}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 578
    return-void

    .line 574
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 576
    .restart local v0    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public onNavdyEventReceived([B)V
    .locals 7
    .param p1, "eventData"    # [B

    .prologue
    .line 388
    iget-boolean v4, p0, Lcom/navdy/service/library/device/RemoteDevice;->convertToNavdyEvent:Z

    if-eqz v4, :cond_1

    .line 389
    const/4 v2, 0x0

    .line 391
    .local v2, "event":Lcom/navdy/service/library/events/NavdyEvent;
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/device/RemoteDevice;->mWire:Lcom/squareup/wire/Wire;

    const-class v5, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v4, p1, v5}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/navdy/service/library/events/NavdyEvent;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    if-eqz v2, :cond_0

    .line 402
    invoke-virtual {p0, v2}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchNavdyEvent(Lcom/navdy/service/library/events/NavdyEvent;)V

    .line 407
    .end local v2    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    :cond_0
    :goto_1
    return-void

    .line 392
    .restart local v2    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_0
    move-exception v1

    .line 393
    .local v1, "e":Ljava/lang/Throwable;
    const/4 v3, -0x1

    .line 395
    .local v3, "eventTypeIndex":I
    :try_start_1
    invoke-static {p1}, Lcom/navdy/service/library/events/WireUtil;->getEventTypeIndex([B)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    .line 399
    :goto_2
    sget-object v4, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Ignoring invalid navdy event["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 405
    .end local v1    # "e":Ljava/lang/Throwable;
    .end local v2    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v3    # "eventTypeIndex":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchNavdyEvent([B)V

    goto :goto_1

    .line 396
    .restart local v1    # "e":Ljava/lang/Throwable;
    .restart local v2    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    .restart local v3    # "eventTypeIndex":I
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public onNetworkLinkReady()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 446
    iput-boolean v2, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkLinkReady:Z

    .line 447
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkStatus()Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne v0, v1, :cond_1

    .line 448
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    new-instance v0, Lcom/navdy/service/library/events/connection/NetworkLinkReady;

    invoke-direct {v0}, Lcom/navdy/service/library/events/connection/NetworkLinkReady;-><init>()V

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchLocalEvent(Lcom/squareup/wire/Message;)V

    .line 450
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onNetworkLinkReady: dispatching the Network Link Ready message"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Network Link is ready, but state is not connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public postEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 226
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z

    move-result v0

    return v0
.end method

.method public postEvent(Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/NavdyEvent;
    .param p2, "postEventHandler"    # Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

    .prologue
    .line 230
    invoke-virtual {p1}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v0

    .line 231
    .local v0, "eventData":[B
    invoke-virtual {p0, v0, p2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent([BLcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z

    move-result v1

    return v1
.end method

.method public postEvent(Lcom/squareup/wire/Message;)Z
    .locals 2
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 221
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v0

    .line 222
    .local v0, "event":Lcom/navdy/service/library/events/NavdyEvent;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/navdy/service/library/events/NavdyEvent;Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z

    move-result v1

    return v1
.end method

.method public postEvent([B)Z
    .locals 1
    .param p1, "eventData"    # [B

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent([BLcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z

    move-result v0

    return v0
.end method

.method public postEvent([BLcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)Z
    .locals 4
    .param p1, "eventData"    # [B
    .param p2, "postEventHandler"    # Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

    .prologue
    .line 239
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    new-instance v2, Lcom/navdy/service/library/device/link/EventRequest;

    iget-object v3, p0, Lcom/navdy/service/library/device/RemoteDevice;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3, p1, p2}, Lcom/navdy/service/library/device/link/EventRequest;-><init>(Landroid/os/Handler;[BLcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 240
    .local v0, "ret":Z
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/device/RemoteDevice;->lastMessageSentTime:J

    .line 241
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NAVDY-PACKET Event queue size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/RemoteDevice;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v3}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 244
    :cond_0
    return v0
.end method

.method public removeActiveConnection()V
    .locals 2

    .prologue
    .line 143
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/connection/Connection;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    .line 148
    :cond_0
    monitor-exit v1

    .line 149
    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setActiveConnection(Lcom/navdy/service/library/device/connection/Connection;)Z
    .locals 2
    .param p1, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 128
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    .line 130
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/connection/Connection;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 132
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/Connection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->dispatchConnectEvent()V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 161
    if-eqz p1, :cond_0

    .line 162
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLastSeenDeviceInfo:Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;

    # setter for: Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;->access$002(Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;Lcom/navdy/service/library/events/DeviceInfo;)Lcom/navdy/service/library/events/DeviceInfo;

    .line 164
    :cond_0
    return-void
.end method

.method public setLinkBandwidthLevel(I)V
    .locals 2
    .param p1, "bandwidthLevel"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLinkStatus:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne v0, v1, :cond_0

    .line 279
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    invoke-interface {v0, p1}, Lcom/navdy/service/library/device/link/Link;->setBandwidthLevel(I)V

    .line 281
    :cond_0
    return-void
.end method

.method public startLink()Z
    .locals 3

    .prologue
    .line 257
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v1, :cond_0

    .line 258
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Link already started"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 261
    :cond_0
    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Starting link"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/device/link/LinkManager;->build(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/link/Link;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    .line 264
    const/4 v0, 0x0

    .line 265
    .local v0, "adapter":Lcom/navdy/service/library/network/SocketAdapter;
    sget-object v2, Lcom/navdy/service/library/device/RemoteDevice;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mActiveConnection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/Connection;->getSocket()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v0

    .line 269
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    if-eqz v0, :cond_2

    .line 272
    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    iget-object v2, p0, Lcom/navdy/service/library/device/RemoteDevice;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-interface {v1, v0, v2, p0}, Lcom/navdy/service/library/device/link/Link;->start(Lcom/navdy/service/library/network/SocketAdapter;Ljava/util/concurrent/LinkedBlockingDeque;Lcom/navdy/service/library/device/link/LinkListener;)Z

    move-result v1

    .line 274
    :goto_0
    return v1

    .line 269
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 274
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopLink()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stopping link"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    if-eqz v0, :cond_0

    .line 350
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Closing link"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkReadyEventDispatched:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 352
    iput-boolean v2, p0, Lcom/navdy/service/library/device/RemoteDevice;->mNetworkLinkReady:Z

    .line 353
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    invoke-interface {v0}, Lcom/navdy/service/library/device/link/Link;->close()V

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mLink:Lcom/navdy/service/library/device/link/Link;

    .line 355
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 358
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
