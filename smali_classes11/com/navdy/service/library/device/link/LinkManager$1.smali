.class final Lcom/navdy/service/library/device/link/LinkManager$1;
.super Ljava/lang/Object;
.source "LinkManager.java"

# interfaces
.implements Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/link/LinkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/link/Link;
    .locals 3
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 36
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v0

    .line 37
    .local v0, "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    sget-object v1, Lcom/navdy/service/library/device/link/LinkManager$2;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 42
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 40
    :pswitch_0
    new-instance v1, Lcom/navdy/service/library/device/link/ProtobufLink;

    invoke-direct {v1, p1}, Lcom/navdy/service/library/device/link/ProtobufLink;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
