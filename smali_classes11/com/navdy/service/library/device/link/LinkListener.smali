.class public interface abstract Lcom/navdy/service/library/device/link/LinkListener;
.super Ljava/lang/Object;
.source "LinkListener.java"


# virtual methods
.method public abstract linkEstablished(Lcom/navdy/service/library/device/connection/ConnectionType;)V
.end method

.method public abstract linkLost(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
.end method

.method public abstract onNavdyEventReceived([B)V
.end method

.method public abstract onNetworkLinkReady()V
.end method
