.class public Lcom/navdy/service/library/device/connection/SocketConnection;
.super Lcom/navdy/service/library/device/connection/Connection;
.source "SocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;
    }
.end annotation


# instance fields
.field private connector:Lcom/navdy/service/library/network/SocketFactory;

.field protected mBlockReconnect:Z

.field protected mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

.field private socket:Lcom/navdy/service/library/network/SocketAdapter;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 0
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p2, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/Connection;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 25
    invoke-virtual {p0, p2}, Lcom/navdy/service/library/device/connection/SocketConnection;->setExistingSocketConnection(Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;Lcom/navdy/service/library/network/SocketFactory;)V
    .locals 0
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p2, "connector"    # Lcom/navdy/service/library/network/SocketFactory;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/Connection;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 20
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->connector:Lcom/navdy/service/library/network/SocketFactory;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/device/connection/SocketConnection;)Lcom/navdy/service/library/network/SocketFactory;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/SocketConnection;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->connector:Lcom/navdy/service/library/network/SocketFactory;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/SocketConnection;
    .param p1, "x1"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/SocketConnection;->closeQuietly(Lcom/navdy/service/library/network/SocketAdapter;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/SocketConnection;
    .param p1, "x1"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/SocketConnection;->connectionFailed(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    return-void
.end method

.method private closeQuietly(Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 3
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 196
    if-eqz p1, :cond_0

    .line 197
    :try_start_0
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[SOCK] Exception while closing"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private connectionFailed(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 0
    .param p1, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/connection/SocketConnection;->dispatchConnectionFailedEvent(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    .line 120
    return-void
.end method


# virtual methods
.method public declared-synchronized connect()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 38
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mBlockReconnect:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 54
    :goto_0
    monitor-exit p0

    return v0

    .line 42
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connect to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-eq v1, v2, :cond_1

    .line 45
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t connect: state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 49
    :cond_1
    :try_start_2
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 51
    new-instance v0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;-><init>(Lcom/navdy/service/library/device/connection/SocketConnection;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    .line 52
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized connected(Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 2
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v0, v1, :cond_1

    if-nez p1, :cond_2

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "No longer connecting - disconnecting."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/connection/SocketConnection;->closeQuietly(Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 104
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 105
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/SocketConnection;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :goto_0
    monitor-exit p0

    return-void

    .line 109
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 111
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 112
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/SocketConnection;->dispatchConnectEvent()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized disconnect()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 59
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    sget-object v2, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-ne v1, v2, :cond_1

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t disconnect: state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :goto_0
    monitor-exit p0

    return v4

    .line 64
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disconnect - connectThread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " socket:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 66
    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 68
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    if-eqz v1, :cond_2

    .line 69
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->cancel()V

    .line 70
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 71
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_3

    .line 73
    :try_start_3
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-interface {v1}, Lcom/navdy/service/library/network/SocketAdapter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 77
    :goto_1
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception closing socket"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 80
    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/SocketConnection;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public getSocket()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->socket:Lcom/navdy/service/library/network/SocketAdapter;

    return-object v0
.end method

.method protected declared-synchronized setExistingSocketConnection(Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 1
    .param p1, "socketAdapter"    # Lcom/navdy/service/library/network/SocketAdapter;

    .prologue
    .line 31
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mBlockReconnect:Z

    .line 32
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->CONNECTING:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 33
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/connection/SocketConnection;->connected(Lcom/navdy/service/library/network/SocketAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
