.class public final Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;
.super Landroid/os/Handler;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionService;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/ConnectionService;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    .line 125
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 126
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 130
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 162
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 132
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 133
    .local v2, "newState":Lcom/navdy/service/library/device/connection/ConnectionService$State;
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-eq v2, v3, :cond_0

    .line 134
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v4, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/device/connection/ConnectionService;->exitState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 135
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iput-object v2, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 136
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v4, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/device/connection/ConnectionService;->enterState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 140
    .end local v2    # "newState":Lcom/navdy/service/library/device/connection/ConnectionService$State;
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->heartBeat()V

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-static {}, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->values()[Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    aget-object v0, v3, v4

    .line 144
    .local v0, "cause":Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/navdy/service/library/device/RemoteDevice;

    .line 145
    .local v1, "device":Lcom/navdy/service/library/device/RemoteDevice;
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3, v1, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->handleDeviceDisconnect(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    goto :goto_0

    .line 148
    .end local v0    # "cause":Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;
    .end local v1    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :pswitch_3
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    # getter for: Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z
    invoke-static {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$000(Lcom/navdy/service/library/device/connection/ConnectionService;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "stopping/starting listeners"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 150
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopListeners()V

    .line 151
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->startListeners()V

    .line 153
    :cond_1
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    # getter for: Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z
    invoke-static {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$100(Lcom/navdy/service/library/device/connection/ConnectionService;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "restarting broadcasters"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 155
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopBroadcasters()V

    .line 156
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->startBroadcasters()V

    .line 158
    :cond_2
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopProxyService()V

    .line 159
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->startProxyService()V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
