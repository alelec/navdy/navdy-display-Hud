.class public Lcom/navdy/service/library/device/connection/AcceptorListener;
.super Lcom/navdy/service/library/device/connection/ConnectionListener;
.source "AcceptorListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;
    }
.end annotation


# instance fields
.field private acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

.field private connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "acceptor"    # Lcom/navdy/service/library/network/SocketAcceptor;
    .param p3, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Acceptor/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    .line 21
    iput-object p3, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/network/SocketAcceptor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/AcceptorListener;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/device/connection/AcceptorListener;)Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/AcceptorListener;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method


# virtual methods
.method protected getNewAcceptThread()Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;-><init>(Lcom/navdy/service/library/device/connection/AcceptorListener;)V

    return-object v0
.end method

.method protected bridge synthetic getNewAcceptThread()Lcom/navdy/service/library/device/connection/ConnectionListener$AcceptThread;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/AcceptorListener;->getNewAcceptThread()Lcom/navdy/service/library/device/connection/AcceptorListener$AcceptThread;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/AcceptorListener;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
