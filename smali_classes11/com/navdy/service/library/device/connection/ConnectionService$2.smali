.class Lcom/navdy/service/library/device/connection/ConnectionService$2;
.super Ljava/lang/Object;
.source "ConnectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 696
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 699
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v0, v1, :cond_0

    .line 700
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrying to connect to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 702
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$2;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->connect()Z

    .line 705
    :cond_0
    return-void
.end method
