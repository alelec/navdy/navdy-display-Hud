.class public final Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewFileResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/PreviewFileResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/PreviewFileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public filename:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public status_detail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/PreviewFileResponse;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 79
    if-nez p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 81
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status_detail:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->filename:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/PreviewFileResponse;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->checkRequiredFields()V

    .line 109
    new-instance v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/PreviewFileResponse;-><init>(Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;Lcom/navdy/service/library/events/file/PreviewFileResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->build()Lcom/navdy/service/library/events/file/PreviewFileResponse;

    move-result-object v0

    return-object v0
.end method

.method public filename(Ljava/lang/String;)Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->filename:Ljava/lang/String;

    .line 103
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 87
    return-object p0
.end method

.method public status_detail(Ljava/lang/String;)Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
    .locals 0
    .param p1, "status_detail"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status_detail:Ljava/lang/String;

    .line 95
    return-object p0
.end method
