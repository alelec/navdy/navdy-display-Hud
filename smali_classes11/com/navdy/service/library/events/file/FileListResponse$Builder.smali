.class public final Lcom/navdy/service/library/events/file/FileListResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/FileListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/FileListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public file_type:Lcom/navdy/service/library/events/file/FileType;

.field public files:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public status_detail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 89
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileListResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/FileListResponse;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 93
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 95
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status_detail:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->file_type:Lcom/navdy/service/library/events/file/FileType;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/file/FileListResponse;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/file/FileListResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->files:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/FileListResponse;
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->checkRequiredFields()V

    .line 132
    new-instance v0, Lcom/navdy/service/library/events/file/FileListResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/FileListResponse;-><init>(Lcom/navdy/service/library/events/file/FileListResponse$Builder;Lcom/navdy/service/library/events/file/FileListResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileListResponse;

    move-result-object v0

    return-object v0
.end method

.method public file_type(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    .locals 0
    .param p1, "file_type"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->file_type:Lcom/navdy/service/library/events/file/FileType;

    .line 118
    return-object p0
.end method

.method public files(Ljava/util/List;)Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/file/FileListResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->files:Ljava/util/List;

    .line 126
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 102
    return-object p0
.end method

.method public status_detail(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    .locals 0
    .param p1, "status_detail"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status_detail:Ljava/lang/String;

    .line 110
    return-object p0
.end method
