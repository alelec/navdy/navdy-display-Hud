.class public final Lcom/navdy/service/library/events/input/LaunchAppEvent;
.super Lcom/squareup/wire/Message;
.source "LaunchAppEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_APPBUNDLEID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final appBundleID:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;

    .prologue
    .line 29
    iget-object v0, p1, Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;->appBundleID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/input/LaunchAppEvent;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/input/LaunchAppEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;Lcom/navdy/service/library/events/input/LaunchAppEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/input/LaunchAppEvent$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/input/LaunchAppEvent;-><init>(Lcom/navdy/service/library/events/input/LaunchAppEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "appBundleID"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 35
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 37
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 36
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 37
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    check-cast p1, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/input/LaunchAppEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->hashCode:I

    .line 43
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->appBundleID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/input/LaunchAppEvent;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
