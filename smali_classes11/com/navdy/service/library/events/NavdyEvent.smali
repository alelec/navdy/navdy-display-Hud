.class public final Lcom/navdy/service/library/events/NavdyEvent;
.super Lcom/squareup/wire/ExtendableMessage;
.source "NavdyEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/NavdyEvent$MessageType;,
        Lcom/navdy/service/library/events/NavdyEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ExtendableMessage",
        "<",
        "Lcom/navdy/service/library/events/NavdyEvent;",
        ">;"
    }
.end annotation


# static fields
.field public static final DEFAULT_TYPE:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field private static final serialVersionUID:J


# instance fields
.field public final type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Coordinate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent;->DEFAULT_TYPE:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/NavdyEvent$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/NavdyEvent$Builder;

    .prologue
    .line 26
    iget-object v0, p1, Lcom/navdy/service/library/events/NavdyEvent$Builder;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/NavdyEvent;-><init>(Lcom/navdy/service/library/events/NavdyEvent$MessageType;)V

    .line 27
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/NavdyEvent;->setBuilder(Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;)V

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/NavdyEvent$Builder;Lcom/navdy/service/library/events/NavdyEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/NavdyEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/NavdyEvent$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/NavdyEvent;-><init>(Lcom/navdy/service/library/events/NavdyEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/NavdyEvent$MessageType;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/squareup/wire/ExtendableMessage;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 23
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 32
    if-ne p1, p0, :cond_1

    const/4 v1, 0x1

    .line 36
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    instance-of v2, p1, Lcom/navdy/service/library/events/NavdyEvent;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/navdy/service/library/events/NavdyEvent;

    .line 35
    .local v0, "o":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-virtual {p0, v0}, Lcom/navdy/service/library/events/NavdyEvent;->extensionsEqual(Lcom/squareup/wire/ExtendableMessage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    iget-object v2, v0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/service/library/events/NavdyEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/service/library/events/NavdyEvent;->hashCode:I

    .line 42
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/navdy/service/library/events/NavdyEvent;->extensionsHashCode()I

    move-result v0

    .line 44
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 45
    iput v0, p0, Lcom/navdy/service/library/events/NavdyEvent;->hashCode:I

    .line 47
    :cond_0
    return v0

    .line 44
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
