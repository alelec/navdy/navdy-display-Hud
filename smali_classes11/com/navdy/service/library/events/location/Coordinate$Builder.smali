.class public final Lcom/navdy/service/library/events/location/Coordinate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Coordinate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/location/Coordinate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/location/Coordinate;",
        ">;"
    }
.end annotation


# instance fields
.field public accuracy:Ljava/lang/Float;

.field public altitude:Ljava/lang/Double;

.field public bearing:Ljava/lang/Float;

.field public latitude:Ljava/lang/Double;

.field public longitude:Ljava/lang/Double;

.field public provider:Ljava/lang/String;

.field public speed:Ljava/lang/Float;

.field public timestamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 134
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 138
    if-nez p1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude:Ljava/lang/Double;

    .line 140
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude:Ljava/lang/Double;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy:Ljava/lang/Float;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude:Ljava/lang/Double;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing:Ljava/lang/Float;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed:Ljava/lang/Float;

    .line 145
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp:Ljava/lang/Long;

    .line 146
    iget-object v0, p1, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->provider:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "accuracy"    # Ljava/lang/Float;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy:Ljava/lang/Float;

    .line 170
    return-object p0
.end method

.method public altitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "altitude"    # Ljava/lang/Double;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude:Ljava/lang/Double;

    .line 178
    return-object p0
.end method

.method public bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "bearing"    # Ljava/lang/Float;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing:Ljava/lang/Float;

    .line 186
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->checkRequiredFields()V

    .line 216
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Lcom/navdy/service/library/events/location/Coordinate$Builder;Lcom/navdy/service/library/events/location/Coordinate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "latitude"    # Ljava/lang/Double;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude:Ljava/lang/Double;

    .line 154
    return-object p0
.end method

.method public longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "longitude"    # Ljava/lang/Double;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude:Ljava/lang/Double;

    .line 162
    return-object p0
.end method

.method public provider(Ljava/lang/String;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->provider:Ljava/lang/String;

    .line 210
    return-object p0
.end method

.method public speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "speed"    # Ljava/lang/Float;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed:Ljava/lang/Float;

    .line 194
    return-object p0
.end method

.method public timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/Long;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp:Ljava/lang/Long;

    .line 202
    return-object p0
.end method
