.class public final Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/NavigationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/NavigationPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public allowAutoTrains:Ljava/lang/Boolean;

.field public allowFerries:Ljava/lang/Boolean;

.field public allowHOVLanes:Ljava/lang/Boolean;

.field public allowHighways:Ljava/lang/Boolean;

.field public allowTollRoads:Ljava/lang/Boolean;

.field public allowTunnels:Ljava/lang/Boolean;

.field public allowUnpavedRoads:Ljava/lang/Boolean;

.field public phoneticTurnByTurn:Ljava/lang/Boolean;

.field public rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

.field public routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

.field public serial_number:Ljava/lang/Long;

.field public showTrafficInOpenMap:Ljava/lang/Boolean;

.field public showTrafficWhileNavigating:Ljava/lang/Boolean;

.field public spokenCameraWarnings:Ljava/lang/Boolean;

.field public spokenSpeedLimitWarnings:Ljava/lang/Boolean;

.field public spokenTurnByTurn:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 191
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 195
    if-nez p1, :cond_0

    .line 212
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 197
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 198
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 199
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 200
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 201
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficInOpenMap:Ljava/lang/Boolean;

    .line 202
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    .line 203
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHighways:Ljava/lang/Boolean;

    .line 204
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTollRoads:Ljava/lang/Boolean;

    .line 205
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowFerries:Ljava/lang/Boolean;

    .line 206
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTunnels:Ljava/lang/Boolean;

    .line 207
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 208
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowAutoTrains:Ljava/lang/Boolean;

    .line 209
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHOVLanes:Ljava/lang/Boolean;

    .line 210
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenCameraWarnings:Ljava/lang/Boolean;

    .line 211
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->phoneticTurnByTurn:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->phoneticTurnByTurn:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public allowAutoTrains(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowAutoTrains"    # Ljava/lang/Boolean;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowAutoTrains:Ljava/lang/Boolean;

    .line 280
    return-object p0
.end method

.method public allowFerries(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowFerries"    # Ljava/lang/Boolean;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowFerries:Ljava/lang/Boolean;

    .line 265
    return-object p0
.end method

.method public allowHOVLanes(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowHOVLanes"    # Ljava/lang/Boolean;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHOVLanes:Ljava/lang/Boolean;

    .line 285
    return-object p0
.end method

.method public allowHighways(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowHighways"    # Ljava/lang/Boolean;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHighways:Ljava/lang/Boolean;

    .line 255
    return-object p0
.end method

.method public allowTollRoads(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowTollRoads"    # Ljava/lang/Boolean;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTollRoads:Ljava/lang/Boolean;

    .line 260
    return-object p0
.end method

.method public allowTunnels(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowTunnels"    # Ljava/lang/Boolean;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTunnels:Ljava/lang/Boolean;

    .line 270
    return-object p0
.end method

.method public allowUnpavedRoads(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "allowUnpavedRoads"    # Ljava/lang/Boolean;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 275
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .locals 2

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->checkRequiredFields()V

    .line 301
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;Lcom/navdy/service/library/events/preferences/NavigationPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public phoneticTurnByTurn(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "phoneticTurnByTurn"    # Ljava/lang/Boolean;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->phoneticTurnByTurn:Ljava/lang/Boolean;

    .line 295
    return-object p0
.end method

.method public rerouteForTraffic(Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "rerouteForTraffic"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 225
    return-object p0
.end method

.method public routingType(Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "routingType"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 230
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 220
    return-object p0
.end method

.method public showTrafficInOpenMap(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "showTrafficInOpenMap"    # Ljava/lang/Boolean;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficInOpenMap:Ljava/lang/Boolean;

    .line 245
    return-object p0
.end method

.method public showTrafficWhileNavigating(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "showTrafficWhileNavigating"    # Ljava/lang/Boolean;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    .line 250
    return-object p0
.end method

.method public spokenCameraWarnings(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "spokenCameraWarnings"    # Ljava/lang/Boolean;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenCameraWarnings:Ljava/lang/Boolean;

    .line 290
    return-object p0
.end method

.method public spokenSpeedLimitWarnings(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "spokenSpeedLimitWarnings"    # Ljava/lang/Boolean;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 240
    return-object p0
.end method

.method public spokenTurnByTurn(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .locals 0
    .param p1, "spokenTurnByTurn"    # Ljava/lang/Boolean;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 235
    return-object p0
.end method
