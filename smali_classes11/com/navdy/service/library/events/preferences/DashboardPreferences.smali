.class public final Lcom/navdy/service/library/events/preferences/DashboardPreferences;
.super Lcom/squareup/wire/Message;
.source "DashboardPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LEFTGAUGEID:Ljava/lang/String; = ""

.field public static final DEFAULT_MIDDLEGAUGE:Lcom/navdy/service/library/events/preferences/MiddleGauge;

.field public static final DEFAULT_RIGHTGAUGEID:Ljava/lang/String; = ""

.field public static final DEFAULT_SCROLLABLESIDE:Lcom/navdy/service/library/events/preferences/ScrollableSide;

.field private static final serialVersionUID:J


# instance fields
.field public final leftGaugeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final rightGaugeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/service/library/events/preferences/MiddleGauge;->SPEEDOMETER:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->DEFAULT_MIDDLEGAUGE:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    .line 15
    sget-object v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->LEFT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->DEFAULT_SCROLLABLESIDE:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;

    .prologue
    .line 39
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->rightGaugeId:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->leftGaugeId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;-><init>(Lcom/navdy/service/library/events/preferences/MiddleGauge;Lcom/navdy/service/library/events/preferences/ScrollableSide;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;Lcom/navdy/service/library/events/preferences/DashboardPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/DashboardPreferences$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;-><init>(Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/MiddleGauge;Lcom/navdy/service/library/events/preferences/ScrollableSide;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "middleGauge"    # Lcom/navdy/service/library/events/preferences/MiddleGauge;
    .param p2, "scrollableSide"    # Lcom/navdy/service/library/events/preferences/ScrollableSide;
    .param p3, "rightGaugeId"    # Ljava/lang/String;
    .param p4, "leftGaugeId"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    .line 33
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 34
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 47
    check-cast v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    .line 48
    .local v0, "o":Lcom/navdy/service/library/events/preferences/DashboardPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 49
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    .line 50
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    .line 51
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 56
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->hashCode:I

    .line 57
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 58
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/MiddleGauge;->hashCode()I

    move-result v0

    .line 59
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/ScrollableSide;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 60
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 61
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 62
    iput v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->hashCode:I

    .line 64
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 58
    goto :goto_0

    :cond_3
    move v2, v1

    .line 59
    goto :goto_1

    :cond_4
    move v2, v1

    .line 60
    goto :goto_2
.end method
