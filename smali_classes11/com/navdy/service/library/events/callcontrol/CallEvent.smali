.class public final Lcom/navdy/service/library/events/callcontrol/CallEvent;
.super Lcom/squareup/wire/Message;
.source "CallEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ANSWERED:Ljava/lang/Boolean;

.field public static final DEFAULT_CONTACT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_DURATION:Ljava/lang/Long;

.field public static final DEFAULT_INCOMING:Ljava/lang/Boolean;

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_START_TIME:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final answered:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final contact_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final duration:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final incoming:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final start_time:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->DEFAULT_INCOMING:Ljava/lang/Boolean;

    .line 28
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->DEFAULT_ANSWERED:Ljava/lang/Boolean;

    .line 31
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->DEFAULT_START_TIME:Ljava/lang/Long;

    .line 32
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->DEFAULT_DURATION:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;

    .prologue
    .line 91
    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->incoming:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->answered:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->number:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->contact_name:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->start_time:Ljava/lang/Long;

    iget-object v6, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->duration:Ljava/lang/Long;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/callcontrol/CallEvent;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 92
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;Lcom/navdy/service/library/events/callcontrol/CallEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/callcontrol/CallEvent$1;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/callcontrol/CallEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0
    .param p1, "incoming"    # Ljava/lang/Boolean;
    .param p2, "answered"    # Ljava/lang/Boolean;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "contact_name"    # Ljava/lang/String;
    .param p5, "start_time"    # Ljava/lang/Long;
    .param p6, "duration"    # Ljava/lang/Long;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    .line 83
    iput-object p2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    .line 84
    iput-object p3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    .line 86
    iput-object p5, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    .line 87
    iput-object p6, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    .line 88
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    if-ne p1, p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 98
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 99
    check-cast v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    .line 100
    .local v0, "o":Lcom/navdy/service/library/events/callcontrol/CallEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    .line 103
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    .line 104
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    .line 105
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/CallEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 110
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->hashCode:I

    .line 111
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 112
    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 113
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 116
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 117
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 118
    iput v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent;->hashCode:I

    .line 120
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 112
    goto :goto_0

    :cond_3
    move v2, v1

    .line 113
    goto :goto_1

    :cond_4
    move v2, v1

    .line 114
    goto :goto_2

    :cond_5
    move v2, v1

    .line 115
    goto :goto_3

    :cond_6
    move v2, v1

    .line 116
    goto :goto_4
.end method
