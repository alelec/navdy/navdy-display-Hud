.class public final Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoUpdateQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public author:Ljava/lang/String;

.field public checksum:Ljava/lang/String;

.field public identifier:Ljava/lang/String;

.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;

.field public size:Ljava/lang/Long;

.field public track:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 112
    if-nez p1, :cond_0

    .line 120
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->identifier:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->size:Ljava/lang/Long;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->checksum:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->album:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->track:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->author:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public album(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->album:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public author(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->author:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    move-result-object v0

    return-object v0
.end method

.method public checksum(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "checksum"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->checksum:Ljava/lang/String;

    .line 142
    return-object p0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->identifier:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.method public photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 132
    return-object p0
.end method

.method public size(Ljava/lang/Long;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "size"    # Ljava/lang/Long;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->size:Ljava/lang/Long;

    .line 137
    return-object p0
.end method

.method public track(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .locals 0
    .param p1, "track"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->track:Ljava/lang/String;

    .line 152
    return-object p0
.end method
