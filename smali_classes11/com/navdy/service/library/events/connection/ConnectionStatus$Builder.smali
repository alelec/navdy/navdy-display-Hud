.class public final Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConnectionStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public remoteDeviceId:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/connection/ConnectionStatus;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 77
    if-nez p1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 79
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->remoteDeviceId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/connection/ConnectionStatus;
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->checkRequiredFields()V

    .line 99
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;Lcom/navdy/service/library/events/connection/ConnectionStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->build()Lcom/navdy/service/library/events/connection/ConnectionStatus;

    move-result-object v0

    return-object v0
.end method

.method public remoteDeviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;
    .locals 0
    .param p1, "remoteDeviceId"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->remoteDeviceId:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;)Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 84
    return-object p0
.end method
