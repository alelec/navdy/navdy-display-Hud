.class public final Lcom/navdy/service/library/events/glances/KeyValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "KeyValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/glances/KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/glances/KeyValue;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/KeyValue;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/glances/KeyValue;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 65
    if-nez p1, :cond_0

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->key:Ljava/lang/String;

    .line 67
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->value:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/glances/KeyValue;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/navdy/service/library/events/glances/KeyValue;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Lcom/navdy/service/library/events/glances/KeyValue$Builder;Lcom/navdy/service/library/events/glances/KeyValue$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->build()Lcom/navdy/service/library/events/glances/KeyValue;

    move-result-object v0

    return-object v0
.end method

.method public key(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/KeyValue$Builder;
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->key:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/KeyValue$Builder;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->value:Ljava/lang/String;

    .line 77
    return-object p0
.end method
