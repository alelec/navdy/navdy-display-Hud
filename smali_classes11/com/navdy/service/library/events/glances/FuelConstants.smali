.class public final enum Lcom/navdy/service/library/events/glances/FuelConstants;
.super Ljava/lang/Enum;
.source "FuelConstants.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/FuelConstants;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/FuelConstants;

.field public static final enum FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

.field public static final enum GAS_STATION_ADDRESS:Lcom/navdy/service/library/events/glances/FuelConstants;

.field public static final enum GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

.field public static final enum GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

.field public static final enum NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    const-string v1, "FUEL_LEVEL"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/glances/FuelConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    const-string v1, "NO_ROUTE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/glances/FuelConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    const-string v1, "GAS_STATION_NAME"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/glances/FuelConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    const-string v1, "GAS_STATION_ADDRESS"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/glances/FuelConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_ADDRESS:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    const-string v1, "GAS_STATION_DISTANCE"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/glances/FuelConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 7
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/FuelConstants;

    sget-object v1, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_ADDRESS:Lcom/navdy/service/library/events/glances/FuelConstants;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/FuelConstants;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/navdy/service/library/events/glances/FuelConstants;->value:I

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/FuelConstants;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/FuelConstants;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/FuelConstants;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/glances/FuelConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/FuelConstants;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/FuelConstants;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/navdy/service/library/events/glances/FuelConstants;->value:I

    return v0
.end method
