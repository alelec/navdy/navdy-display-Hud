.class public final enum Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
.super Ljava/lang/Enum;
.source "GlanceEvent.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/glances/GlanceEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GlanceActions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

.field public static final enum REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    const-string v1, "REPLY"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    .line 230
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 237
    iput p3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->value:I

    .line 238
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 230
    const-class v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
    .locals 1

    .prologue
    .line 230
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->value:I

    return v0
.end method
