.class public final Lcom/navdy/service/library/events/glances/GlanceEvent;
.super Lcom/squareup/wire/Message;
.source "GlanceEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;,
        Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;,
        Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GLANCEDATA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GLANCETYPE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTTIME:Ljava/lang/Long;

.field public static final DEFAULT_PROVIDER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final actions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;"
        }
    .end annotation
.end field

.field public final glanceData:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/glances/KeyValue;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public final glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final language:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final postTime:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final provider:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_GLANCETYPE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 25
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_POSTTIME:Ljava/lang/Long;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_GLANCEDATA:Ljava/util/List;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_ACTIONS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    .prologue
    .line 88
    iget-object v1, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iget-object v2, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime:Ljava/lang/Long;

    iget-object v5, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData:Ljava/util/List;

    iget-object v6, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions:Ljava/util/List;

    iget-object v7, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->language:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/glances/GlanceEvent;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/glances/GlanceEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 90
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;Lcom/navdy/service/library/events/glances/GlanceEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/glances/GlanceEvent$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/glances/GlanceEvent;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "glanceType"    # Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "postTime"    # Ljava/lang/Long;
    .param p7, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p5, "glanceData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .local p6, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 79
    iput-object p2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    .line 82
    invoke-static {p5}, Lcom/navdy/service/library/events/glances/GlanceEvent;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 83
    invoke-static {p6}, Lcom/navdy/service/library/events/glances/GlanceEvent;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 84
    iput-object p7, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->language:Ljava/lang/String;

    .line 85
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    if-ne p1, p0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 96
    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent;

    .line 97
    .local v0, "o":Lcom/navdy/service/library/events/glances/GlanceEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 98
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    .line 99
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->language:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->language:Ljava/lang/String;

    .line 103
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 108
    iget v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->hashCode:I

    .line 109
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 110
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->hashCode()I

    move-result v0

    .line 111
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 112
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 113
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 114
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 115
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v2, v3

    .line 116
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->language:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->language:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 117
    iput v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->hashCode:I

    .line 119
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 110
    goto :goto_0

    :cond_4
    move v2, v1

    .line 111
    goto :goto_1

    :cond_5
    move v2, v1

    .line 112
    goto :goto_2

    :cond_6
    move v2, v1

    .line 113
    goto :goto_3

    :cond_7
    move v2, v3

    .line 114
    goto :goto_4
.end method
