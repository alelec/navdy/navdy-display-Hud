.class public final Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DriveRecordingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public recordings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 59
    if-nez p1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;->recordings:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;->recordings:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;-><init>(Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;->build()Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public recordings(Ljava/util/List;)Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "recordings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse$Builder;->recordings:Ljava/util/List;

    .line 68
    return-object p0
.end method
