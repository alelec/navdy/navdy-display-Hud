.class public final Lcom/navdy/service/library/events/DeviceInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/DeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/DeviceInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public buildType:Ljava/lang/String;

.field public capabilities:Lcom/navdy/service/library/events/Capabilities;

.field public clientVersion:Ljava/lang/String;

.field public deviceId:Ljava/lang/String;

.field public deviceMake:Ljava/lang/String;

.field public deviceName:Ljava/lang/String;

.field public deviceUuid:Ljava/lang/String;

.field public forceFullUpdate:Ljava/lang/Boolean;

.field public kernelVersion:Ljava/lang/String;

.field public legacyCapabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;"
        }
    .end annotation
.end field

.field public model:Ljava/lang/String;

.field public musicPlayers_OBSOLETE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

.field public protocolVersion:Ljava/lang/String;

.field public systemApiLevel:Ljava/lang/Integer;

.field public systemVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 253
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 256
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 257
    if-nez p1, :cond_0

    .line 274
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId:Ljava/lang/String;

    .line 259
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->clientVersion:Ljava/lang/String;

    .line 260
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->protocolVersion:Ljava/lang/String;

    .line 261
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceName:Ljava/lang/String;

    .line 262
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemVersion:Ljava/lang/String;

    .line 263
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->model:Ljava/lang/String;

    .line 264
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceUuid:Ljava/lang/String;

    .line 265
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemApiLevel:Ljava/lang/Integer;

    .line 266
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->kernelVersion:Ljava/lang/String;

    .line 267
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 268
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->buildType:Ljava/lang/String;

    .line 269
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceMake:Ljava/lang/String;

    .line 270
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->forceFullUpdate:Ljava/lang/Boolean;

    .line 271
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/DeviceInfo;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/DeviceInfo;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->legacyCapabilities:Ljava/util/List;

    .line 272
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/DeviceInfo;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/DeviceInfo;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->musicPlayers_OBSOLETE:Ljava/util/List;

    .line 273
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/DeviceInfo;
    .locals 2

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->checkRequiredFields()V

    .line 417
    new-instance v0, Lcom/navdy/service/library/events/DeviceInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/DeviceInfo;-><init>(Lcom/navdy/service/library/events/DeviceInfo$Builder;Lcom/navdy/service/library/events/DeviceInfo$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->build()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public buildType(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "buildType"    # Ljava/lang/String;

    .prologue
    .line 371
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->buildType:Ljava/lang/String;

    .line 372
    return-object p0
.end method

.method public capabilities(Lcom/navdy/service/library/events/Capabilities;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "capabilities"    # Lcom/navdy/service/library/events/Capabilities;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    .line 411
    return-object p0
.end method

.method public clientVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "clientVersion"    # Ljava/lang/String;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->clientVersion:Ljava/lang/String;

    .line 290
    return-object p0
.end method

.method public deviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId:Ljava/lang/String;

    .line 281
    return-object p0
.end method

.method public deviceMake(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "deviceMake"    # Ljava/lang/String;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceMake:Ljava/lang/String;

    .line 385
    return-object p0
.end method

.method public deviceName(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceName:Ljava/lang/String;

    .line 307
    return-object p0
.end method

.method public deviceUuid(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "deviceUuid"    # Ljava/lang/String;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceUuid:Ljava/lang/String;

    .line 342
    return-object p0
.end method

.method public forceFullUpdate(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "forceFullUpdate"    # Ljava/lang/Boolean;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->forceFullUpdate:Ljava/lang/Boolean;

    .line 390
    return-object p0
.end method

.method public kernelVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "kernelVersion"    # Ljava/lang/String;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->kernelVersion:Ljava/lang/String;

    .line 359
    return-object p0
.end method

.method public legacyCapabilities(Ljava/util/List;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;)",
            "Lcom/navdy/service/library/events/DeviceInfo$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 395
    .local p1, "legacyCapabilities":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/LegacyCapability;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->legacyCapabilities:Ljava/util/List;

    .line 396
    return-object p0
.end method

.method public model(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->model:Ljava/lang/String;

    .line 334
    return-object p0
.end method

.method public musicPlayers_OBSOLETE(Ljava/util/List;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/DeviceInfo$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 405
    .local p1, "musicPlayers_OBSOLETE":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->musicPlayers_OBSOLETE:Ljava/util/List;

    .line 406
    return-object p0
.end method

.method public platform(Lcom/navdy/service/library/events/DeviceInfo$Platform;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "platform"    # Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 367
    return-object p0
.end method

.method public protocolVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/String;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->protocolVersion:Ljava/lang/String;

    .line 299
    return-object p0
.end method

.method public systemApiLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "systemApiLevel"    # Ljava/lang/Integer;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemApiLevel:Ljava/lang/Integer;

    .line 350
    return-object p0
.end method

.method public systemVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .locals 0
    .param p1, "systemVersion"    # Ljava/lang/String;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemVersion:Ljava/lang/String;

    .line 320
    return-object p0
.end method
