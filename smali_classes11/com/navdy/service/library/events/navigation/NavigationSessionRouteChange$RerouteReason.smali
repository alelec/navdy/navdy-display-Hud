.class public final enum Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
.super Ljava/lang/Enum;
.source "NavigationSessionRouteChange.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RerouteReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field public static final enum NAV_SESSION_ARRIVAL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field public static final enum NAV_SESSION_FUEL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field public static final enum NAV_SESSION_OFF_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field public static final enum NAV_SESSION_ROUTE_PICKER:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field public static final enum NAV_SESSION_TRAFFIC_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 121
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const-string v1, "NAV_SESSION_OFF_REROUTE"

    invoke-direct {v0, v1, v7, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_OFF_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 125
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const-string v1, "NAV_SESSION_TRAFFIC_REROUTE"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_TRAFFIC_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 129
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const-string v1, "NAV_SESSION_ARRIVAL_REROUTE"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ARRIVAL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 133
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const-string v1, "NAV_SESSION_FUEL_REROUTE"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_FUEL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 137
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const-string v1, "NAV_SESSION_ROUTE_PICKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ROUTE_PICKER:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 116
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_OFF_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_TRAFFIC_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ARRIVAL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_FUEL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ROUTE_PICKER:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 142
    iput p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->value:I

    .line 143
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->value:I

    return v0
.end method
