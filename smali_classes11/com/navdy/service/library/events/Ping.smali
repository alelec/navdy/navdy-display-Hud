.class public final Lcom/navdy/service/library/events/Ping;
.super Lcom/squareup/wire/Message;
.source "Ping.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/Ping$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 16
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/Ping$Builder;)V
    .locals 0
    .param p1, "builder"    # Lcom/navdy/service/library/events/Ping$Builder;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 19
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/Ping;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/Ping$Builder;Lcom/navdy/service/library/events/Ping$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/Ping$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/Ping$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/Ping;-><init>(Lcom/navdy/service/library/events/Ping$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 24
    instance-of v0, p1, Lcom/navdy/service/library/events/Ping;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method
