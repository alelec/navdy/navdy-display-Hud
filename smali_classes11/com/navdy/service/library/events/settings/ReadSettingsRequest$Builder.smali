.class public final Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReadSettingsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/ReadSettingsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/settings/ReadSettingsRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 57
    if-nez p1, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;->settings:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/settings/ReadSettingsRequest;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;Lcom/navdy/service/library/events/settings/ReadSettingsRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;->build()Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    move-result-object v0

    return-object v0
.end method

.method public settings(Ljava/util/List;)Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "settings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;->settings:Ljava/util/List;

    .line 68
    return-object p0
.end method
