.class public final Lcom/navdy/service/library/events/settings/NetworkStateChange;
.super Lcom/squareup/wire/Message;
.source "NetworkStateChange.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CELLNETWORK:Ljava/lang/Boolean;

.field public static final DEFAULT_NETWORKAVAILABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_REACHABILITY:Ljava/lang/Boolean;

.field public static final DEFAULT_WIFINETWORK:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final cellNetwork:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final networkAvailable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final reachability:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final wifiNetwork:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->DEFAULT_NETWORKAVAILABLE:Ljava/lang/Boolean;

    .line 18
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->DEFAULT_CELLNETWORK:Ljava/lang/Boolean;

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->DEFAULT_WIFINETWORK:Ljava/lang/Boolean;

    .line 20
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->DEFAULT_REACHABILITY:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;

    .prologue
    .line 55
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->networkAvailable:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->cellNetwork:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->wifiNetwork:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->reachability:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/settings/NetworkStateChange;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 56
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/settings/NetworkStateChange;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;Lcom/navdy/service/library/events/settings/NetworkStateChange$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/settings/NetworkStateChange$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/settings/NetworkStateChange;-><init>(Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "networkAvailable"    # Ljava/lang/Boolean;
    .param p2, "cellNetwork"    # Ljava/lang/Boolean;
    .param p3, "wifiNetwork"    # Ljava/lang/Boolean;
    .param p4, "reachability"    # Ljava/lang/Boolean;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    .line 49
    iput-object p2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    .line 50
    iput-object p3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    .line 51
    iput-object p4, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 63
    check-cast v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 64
    .local v0, "o":Lcom/navdy/service/library/events/settings/NetworkStateChange;
    iget-object v3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/NetworkStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/NetworkStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/NetworkStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    .line 67
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/NetworkStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    iget v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->hashCode:I

    .line 73
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 74
    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 75
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 76
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 77
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 78
    iput v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange;->hashCode:I

    .line 80
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 74
    goto :goto_0

    :cond_3
    move v2, v1

    .line 75
    goto :goto_1

    :cond_4
    move v2, v1

    .line 76
    goto :goto_2
.end method
