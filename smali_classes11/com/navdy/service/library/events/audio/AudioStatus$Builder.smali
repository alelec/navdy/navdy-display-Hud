.class public final Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AudioStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/AudioStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/AudioStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public hasSCOConnection:Ljava/lang/Boolean;

.field public isConnected:Ljava/lang/Boolean;

.field public isPlaying:Ljava/lang/Boolean;

.field public profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 99
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/AudioStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/AudioStatus;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 103
    if-nez p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isConnected:Ljava/lang/Boolean;

    .line 105
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 106
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 107
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isPlaying:Ljava/lang/Boolean;

    .line 108
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->hasSCOConnection:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/AudioStatus;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/AudioStatus;-><init>(Lcom/navdy/service/library/events/audio/AudioStatus$Builder;Lcom/navdy/service/library/events/audio/AudioStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->build()Lcom/navdy/service/library/events/audio/AudioStatus;

    move-result-object v0

    return-object v0
.end method

.method public connectionType(Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .locals 0
    .param p1, "connectionType"    # Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 124
    return-object p0
.end method

.method public hasSCOConnection(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .locals 0
    .param p1, "hasSCOConnection"    # Ljava/lang/Boolean;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->hasSCOConnection:Ljava/lang/Boolean;

    .line 148
    return-object p0
.end method

.method public isConnected(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .locals 0
    .param p1, "isConnected"    # Ljava/lang/Boolean;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isConnected:Ljava/lang/Boolean;

    .line 116
    return-object p0
.end method

.method public isPlaying(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .locals 0
    .param p1, "isPlaying"    # Ljava/lang/Boolean;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isPlaying:Ljava/lang/Boolean;

    .line 140
    return-object p0
.end method

.method public profileType(Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .locals 0
    .param p1, "profileType"    # Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 132
    return-object p0
.end method
