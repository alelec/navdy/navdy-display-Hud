.class public final Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCapability.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCapability;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCapability;",
        ">;"
    }
.end annotation


# instance fields
.field public authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;"
        }
    .end annotation
.end field

.field public serial_number:Ljava/lang/Long;

.field public supportedRepeatModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;"
        }
    .end annotation
.end field

.field public supportedShuffleModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCapability;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCapability;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 129
    if-nez p1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 131
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCapability;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionTypes:Ljava/util/List;

    .line 132
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 133
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCapability;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedShuffleModes:Ljava/util/List;

    .line 134
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCapability;->access$200(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedRepeatModes:Ljava/util/List;

    .line 135
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->serial_number:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public authorizationStatus(Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 0
    .param p1, "authorizationStatus"    # Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 159
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/audio/MusicCapability;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapability;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCapability;-><init>(Lcom/navdy/service/library/events/audio/MusicCapability$Builder;Lcom/navdy/service/library/events/audio/MusicCapability$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapability;

    move-result-object v0

    return-object v0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 143
    return-object p0
.end method

.method public collectionTypes(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCapability$Builder;"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionTypes:Ljava/util/List;

    .line 151
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->serial_number:Ljava/lang/Long;

    .line 180
    return-object p0
.end method

.method public supportedRepeatModes(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCapability$Builder;"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "supportedRepeatModes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicRepeatMode;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedRepeatModes:Ljava/util/List;

    .line 172
    return-object p0
.end method

.method public supportedShuffleModes(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCapability$Builder;"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "supportedShuffleModes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicShuffleMode;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedShuffleModes:Ljava/util/List;

    .line 167
    return-object p0
.end method
