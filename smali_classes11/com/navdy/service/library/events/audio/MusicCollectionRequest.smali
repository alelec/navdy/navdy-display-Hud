.class public final Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
.super Lcom/squareup/wire/Message;
.source "MusicCollectionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_COLLECTIONID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_FILTERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GROUPBY:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_INCLUDECHARACTERMAP:Ljava/lang/Boolean;

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_OFFSET:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final collectionId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final filters:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/audio/MusicCollectionFilter;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final includeCharacterMap:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final offset:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_FILTERS:Ljava/util/List;

    .line 23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_OFFSET:Ljava/lang/Integer;

    .line 25
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_INCLUDECHARACTERMAP:Ljava/lang/Boolean;

    .line 26
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->DEFAULT_GROUPBY:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    .prologue
    .line 84
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->filters:Ljava/util/List;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionId:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->offset:Ljava/lang/Integer;

    iget-object v7, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->includeCharacterMap:Ljava/lang/Boolean;

    iget-object v8, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicCollectionType;)V

    .line 85
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicCollectionType;)V
    .locals 1
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p2, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .param p4, "collectionId"    # Ljava/lang/String;
    .param p5, "limit"    # Ljava/lang/Integer;
    .param p6, "offset"    # Ljava/lang/Integer;
    .param p7, "includeCharacterMap"    # Ljava/lang/Boolean;
    .param p8, "groupBy"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionFilter;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 74
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 75
    invoke-static {p3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    .line 76
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    .line 77
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    .line 78
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    .line 79
    iput-object p7, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    .line 80
    iput-object p8, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 81
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    if-ne p1, p0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v1

    .line 91
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 92
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    .line 93
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 94
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    .line 95
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    .line 96
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    .line 97
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    .line 98
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    .line 99
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 105
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->hashCode:I

    .line 106
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 107
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v0

    .line 108
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 109
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 110
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 111
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 112
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 113
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 115
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->hashCode:I

    .line 117
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 107
    goto :goto_0

    :cond_3
    move v2, v1

    .line 108
    goto :goto_1

    .line 109
    :cond_4
    const/4 v2, 0x1

    goto :goto_2

    :cond_5
    move v2, v1

    .line 110
    goto :goto_3

    :cond_6
    move v2, v1

    .line 111
    goto :goto_4

    :cond_7
    move v2, v1

    .line 112
    goto :goto_5

    :cond_8
    move v2, v1

    .line 113
    goto :goto_6
.end method
