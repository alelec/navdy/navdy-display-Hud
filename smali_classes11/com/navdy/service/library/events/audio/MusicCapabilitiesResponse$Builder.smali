.class public final Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCapabilitiesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public capabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCapability;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 54
    if-nez p1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->capabilities:Ljava/util/List;

    # invokes: Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->copyOf(Ljava/util/List;)Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->capabilities:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public capabilities(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCapability;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "capabilities":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCapability;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->capabilities:Ljava/util/List;

    .line 63
    return-object p0
.end method
