.class public interface abstract Lcom/navdy/service/library/file/IFileTransferManager;
.super Ljava/lang/Object;
.source "IFileTransferManager.java"


# virtual methods
.method public abstract getFileType(I)Lcom/navdy/service/library/events/file/FileType;
.end method

.method public abstract getNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public abstract handleFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)Lcom/navdy/service/library/events/file/FileTransferStatus;
.end method

.method public abstract handleFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)Lcom/navdy/service/library/events/file/FileTransferResponse;
.end method

.method public abstract handleFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)Z
.end method

.method public abstract stop()V
.end method
