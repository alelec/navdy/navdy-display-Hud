.class public Lcom/navdy/service/library/network/http/HttpManager;
.super Ljava/lang/Object;
.source "HttpManager.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/IHttpManager;


# instance fields
.field protected mHttpClient:Lokhttp3/OkHttpClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-direct {p0}, Lcom/navdy/service/library/network/http/HttpManager;->initHttpClient()V

    .line 17
    return-void
.end method

.method private initHttpClient()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->retryOnConnectionFailure(Z)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/network/http/HttpManager;->mHttpClient:Lokhttp3/OkHttpClient;

    .line 24
    return-void
.end method


# virtual methods
.method public getClient()Lokhttp3/OkHttpClient;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/service/library/network/http/HttpManager;->mHttpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method public getClientCopy()Lokhttp3/OkHttpClient$Builder;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/service/library/network/http/HttpManager;->mHttpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    return-object v0
.end method
