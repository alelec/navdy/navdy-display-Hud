.class Lcom/navdy/service/library/network/http/services/JiraClient$2;
.super Ljava/lang/Object;
.source "JiraClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/network/http/services/JiraClient;->assignTicketToUser(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

.field final synthetic val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

.field final synthetic val$user:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/network/http/services/JiraClient;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

    iput-object p2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    iput-object p3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$user:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 2
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 150
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFailure "

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v0, p2}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    .line 154
    :cond_0
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 7
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    :try_start_0
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v0

    .line 160
    .local v0, "responseCode":I
    const/16 v3, 0xc8

    if-eq v0, v3, :cond_0

    const/16 v3, 0xcc

    if-ne v0, v3, :cond_2

    .line 161
    :cond_0
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v3, :cond_1

    .line 162
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    iget-object v4, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$user:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onSuccess(Ljava/lang/Object;)V

    .line 177
    .end local v0    # "responseCode":I
    :cond_1
    :goto_0
    return-void

    .line 165
    .restart local v0    # "responseCode":I
    :cond_2
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v3

    invoke-virtual {v3}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "responseData":Ljava/lang/String;
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Response data "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 167
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v3, :cond_1

    .line 168
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Jira request failed with "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 171
    .end local v0    # "responseCode":I
    .end local v1    # "responseData":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 172
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Exception paring response "

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v3, :cond_1

    .line 174
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$2;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v3, v2}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
