.class public Lcom/navdy/service/library/network/http/services/JiraClient;
.super Ljava/lang/Object;
.source "JiraClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;,
        Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    }
.end annotation


# static fields
.field public static final ASSIGNEE:Ljava/lang/String; = "assignee"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final EMAIL_ADDRESS:Ljava/lang/String; = "emailAddress"

.field public static final ENVIRONMENT:Ljava/lang/String; = "environment"

.field public static final FIELDS:Ljava/lang/String; = "fields"

.field public static final ISSUE_TYPE:Ljava/lang/String; = "issuetype"

.field public static final ISSUE_TYPE_BUG:Ljava/lang/String; = "Bug"

.field public static final JIRA_API_SEARCH_ASSIGNABLE_USER:Ljava/lang/String; = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search"

.field private static final JIRA_ATTACHMENTS:Ljava/lang/String; = "/attachments/"

.field private static final JIRA_ISSUE_API_URL:Ljava/lang/String; = "https://navdyhud.atlassian.net/rest/api/2/issue/"

.field private static final JIRA_ISSUE_EDIT_URL:Ljava/lang/String; = "https://navdyhud.atlassian.net/rest/api/2/issue/"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROJECT:Ljava/lang/String; = "project"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private encodedCredentials:Ljava/lang/String;

.field private mClient:Lokhttp3/OkHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/network/http/services/JiraClient;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lokhttp3/OkHttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    .line 68
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public assignTicketForName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 6
    .param p1, "ticketId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "emailAddress"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    .prologue
    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search?issueKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&username="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "url":Ljava/lang/String;
    new-instance v3, Lokhttp3/Request$Builder;

    invoke-direct {v3}, Lokhttp3/Request$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lokhttp3/Request$Builder;->get()Lokhttp3/Request$Builder;

    move-result-object v1

    .line 74
    .local v1, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 75
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Basic "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 77
    :cond_0
    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 78
    .local v0, "request":Lokhttp3/Request;
    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v3, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/network/http/services/JiraClient$1;

    invoke-direct {v4, p0, p4, p3, p1}, Lcom/navdy/service/library/network/http/services/JiraClient$1;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    .line 129
    return-void
.end method

.method public assignTicketToUser(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 12
    .param p1, "ticketId"    # Ljava/lang/String;
    .param p2, "user"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    .prologue
    .line 132
    const/4 v8, 0x0

    .line 134
    .local v8, "writer":Ljava/io/OutputStreamWriter;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 135
    .local v1, "data":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 136
    .local v2, "fields":Lorg/json/JSONObject;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 137
    .local v0, "assignee":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v0, v9, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 138
    const-string v9, "assignee"

    invoke-virtual {v2, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 139
    const-string v9, "fields"

    invoke-virtual {v1, v9, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "jsonData":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/service/library/network/http/HttpUtils;->getJsonRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v5

    .line 142
    .local v5, "requestBody":Lokhttp3/RequestBody;
    new-instance v9, Lokhttp3/Request$Builder;

    invoke-direct {v9}, Lokhttp3/Request$Builder;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "https://navdyhud.atlassian.net/rest/api/2/issue//"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v9

    invoke-virtual {v9, v5}, Lokhttp3/Request$Builder;->put(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v6

    .line 143
    .local v6, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 144
    const-string v9, "Authorization"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Basic "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 146
    :cond_0
    invoke-virtual {v6}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v4

    .line 147
    .local v4, "request":Lokhttp3/Request;
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v9, v4}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v9

    new-instance v10, Lcom/navdy/service/library/network/http/services/JiraClient$2;

    invoke-direct {v10, p0, p3, p2}, Lcom/navdy/service/library/network/http/services/JiraClient$2;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 184
    .end local v0    # "assignee":Lorg/json/JSONObject;
    .end local v1    # "data":Lorg/json/JSONObject;
    .end local v2    # "fields":Lorg/json/JSONObject;
    .end local v3    # "jsonData":Ljava/lang/String;
    .end local v4    # "request":Lokhttp3/Request;
    .end local v5    # "requestBody":Lokhttp3/RequestBody;
    .end local v6    # "requestBuilder":Lokhttp3/Request$Builder;
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v7

    .line 180
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_1
    invoke-interface {p3, v7}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v9

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v9
.end method

.method public attachFilesToTicket(Ljava/lang/String;Ljava/util/List;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 11
    .param p1, "ticketId"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;",
            ">;",
            "Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 258
    .local p2, "attachments":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;>;"
    new-instance v7, Lokhttp3/MultipartBody$Builder;

    invoke-direct {v7}, Lokhttp3/MultipartBody$Builder;-><init>()V

    sget-object v8, Lokhttp3/MultipartBody;->FORM:Lokhttp3/MediaType;

    .line 259
    invoke-virtual {v7, v8}, Lokhttp3/MultipartBody$Builder;->setType(Lokhttp3/MediaType;)Lokhttp3/MultipartBody$Builder;

    move-result-object v1

    .line 260
    .local v1, "builder":Lokhttp3/MultipartBody$Builder;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    .line 261
    .local v0, "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    new-instance v2, Ljava/io/File;

    iget-object v8, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 262
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 263
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Attachment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "File does not exists"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 265
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v8

    if-nez v8, :cond_1

    .line 266
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Attachment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "File cannot be read"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 268
    :cond_1
    iget-object v8, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    invoke-static {v8}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v3

    .line 269
    .local v3, "mediaType":Lokhttp3/MediaType;
    const-string v8, "file"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 270
    invoke-static {v3, v2}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object v10

    .line 269
    invoke-virtual {v1, v8, v9, v10}, Lokhttp3/MultipartBody$Builder;->addFormDataPart(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Builder;

    goto :goto_0

    .line 272
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "mediaType":Lokhttp3/MediaType;
    :cond_2
    invoke-virtual {v1}, Lokhttp3/MultipartBody$Builder;->build()Lokhttp3/MultipartBody;

    move-result-object v5

    .line 273
    .local v5, "requestBody":Lokhttp3/RequestBody;
    new-instance v7, Lokhttp3/Request$Builder;

    invoke-direct {v7}, Lokhttp3/Request$Builder;-><init>()V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "https://navdyhud.atlassian.net/rest/api/2/issue/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/attachments/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    const-string v8, "X-Atlassian-Token"

    const-string v9, "nocheck"

    invoke-virtual {v7, v8, v9}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v6

    .line 275
    .local v6, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v7, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 276
    const-string v7, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Basic "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 278
    :cond_3
    invoke-virtual {v6}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v4

    .line 279
    .local v4, "request":Lokhttp3/Request;
    iget-object v7, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v7, v4}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v7

    new-instance v8, Lcom/navdy/service/library/network/http/services/JiraClient$4;

    invoke-direct {v8, p0, p3, p1}, Lcom/navdy/service/library/network/http/services/JiraClient$4;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;)V

    invoke-interface {v7, v8}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    .line 303
    return-void
.end method

.method public setEncodedCredentials(Ljava/lang/String;)V
    .locals 0
    .param p1, "encodedCredentials"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 7
    .param p1, "projectName"    # Ljava/lang/String;
    .param p2, "issueTypeName"    # Ljava/lang/String;
    .param p3, "summary"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/service/library/network/http/services/JiraClient;->submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    .line 188
    return-void
.end method

.method public submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 14
    .param p1, "projectName"    # Ljava/lang/String;
    .param p2, "issueTypeName"    # Ljava/lang/String;
    .param p3, "summary"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "environment"    # Ljava/lang/String;
    .param p6, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    const/4 v10, 0x0

    .line 193
    .local v10, "writer":Ljava/io/OutputStreamWriter;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 194
    .local v1, "data":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 195
    .local v2, "fields":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 196
    .local v5, "project":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 197
    .local v3, "issueType":Lorg/json/JSONObject;
    const-string v11, "key"

    invoke-virtual {v5, v11, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 198
    const-string v11, "project"

    invoke-virtual {v2, v11, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 199
    const-string v11, "name"

    move-object/from16 v0, p2

    invoke-virtual {v3, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 200
    const-string v11, "issuetype"

    invoke-virtual {v2, v11, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 201
    const-string v11, "summary"

    move-object/from16 v0, p3

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 202
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 203
    const-string v11, "environment"

    move-object/from16 v0, p5

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    :cond_0
    const-string v11, "description"

    move-object/from16 v0, p4

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 207
    const-string v11, "fields"

    invoke-virtual {v1, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 208
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 209
    .local v4, "jsonData":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/service/library/network/http/HttpUtils;->getJsonRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v7

    .line 210
    .local v7, "requestBody":Lokhttp3/RequestBody;
    new-instance v11, Lokhttp3/Request$Builder;

    invoke-direct {v11}, Lokhttp3/Request$Builder;-><init>()V

    const-string v12, "https://navdyhud.atlassian.net/rest/api/2/issue/"

    invoke-virtual {v11, v12}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v11

    invoke-virtual {v11, v7}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v8

    .line 211
    .local v8, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 212
    const-string v11, "Authorization"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Basic "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 214
    :cond_1
    invoke-virtual {v8}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v6

    .line 215
    .local v6, "request":Lokhttp3/Request;
    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v11, v6}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v11

    new-instance v12, Lcom/navdy/service/library/network/http/services/JiraClient$3;

    move-object/from16 v0, p6

    invoke-direct {v12, p0, v0}, Lcom/navdy/service/library/network/http/services/JiraClient$3;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    invoke-interface {v11, v12}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 251
    .end local v1    # "data":Lorg/json/JSONObject;
    .end local v2    # "fields":Lorg/json/JSONObject;
    .end local v3    # "issueType":Lorg/json/JSONObject;
    .end local v4    # "jsonData":Ljava/lang/String;
    .end local v5    # "project":Lorg/json/JSONObject;
    .end local v6    # "request":Lokhttp3/Request;
    .end local v7    # "requestBody":Lokhttp3/RequestBody;
    .end local v8    # "requestBuilder":Lokhttp3/Request$Builder;
    :goto_0
    return-void

    .line 246
    :catch_0
    move-exception v9

    .line 247
    .local v9, "t":Ljava/lang/Throwable;
    :try_start_1
    move-object/from16 v0, p6

    invoke-interface {v0, v9}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v9    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v11

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v11
.end method
