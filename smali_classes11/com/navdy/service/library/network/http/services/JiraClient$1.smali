.class Lcom/navdy/service/library/network/http/services/JiraClient$1;
.super Ljava/lang/Object;
.source "JiraClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/network/http/services/JiraClient;->assignTicketForName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

.field final synthetic val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

.field final synthetic val$emailAddress:Ljava/lang/String;

.field final synthetic val$ticketId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/network/http/services/JiraClient;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

    iput-object p2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    iput-object p3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$emailAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$ticketId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 2
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 81
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFailure "

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v0, p2}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    .line 85
    :cond_0
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 13
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    :try_start_0
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v4

    .line 91
    .local v4, "responseCode":I
    const/16 v9, 0xc8

    if-ne v4, v9, :cond_4

    .line 92
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v9

    invoke-virtual {v9}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "jsonResponse":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 94
    .local v5, "resultsArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 95
    .local v3, "length":I
    if-lez v3, :cond_2

    .line 96
    const-string v7, ""

    .line 97
    .local v7, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_1

    .line 98
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    .line 99
    .local v8, "userObject":Lorg/json/JSONObject;
    if-nez v1, :cond_0

    .line 100
    const-string v9, "name"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 102
    :cond_0
    const-string v9, "emailAddress"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 103
    const-string v9, "emailAddress"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "emailId":Ljava/lang/String;
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Email Id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 105
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$emailAddress:Ljava/lang/String;

    invoke-static {v0, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 106
    const-string v9, "name"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 107
    const-string v9, "name"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 108
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "User name "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 114
    .end local v0    # "emailId":Ljava/lang/String;
    .end local v8    # "userObject":Lorg/json/JSONObject;
    :cond_1
    # getter for: Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/service/library/network/http/services/JiraClient;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Assigning the ticket to User "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 115
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

    iget-object v10, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$ticketId:Ljava/lang/String;

    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-virtual {v9, v10, v7, v11}, Lcom/navdy/service/library/network/http/services/JiraClient;->assignTicketToUser(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    .line 127
    .end local v1    # "i":I
    .end local v2    # "jsonResponse":Ljava/lang/String;
    .end local v3    # "length":I
    .end local v4    # "responseCode":I
    .end local v5    # "resultsArray":Lorg/json/JSONArray;
    .end local v7    # "userName":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 97
    .restart local v1    # "i":I
    .restart local v2    # "jsonResponse":Ljava/lang/String;
    .restart local v3    # "length":I
    .restart local v4    # "responseCode":I
    .restart local v5    # "resultsArray":Lorg/json/JSONArray;
    .restart local v7    # "userName":Ljava/lang/String;
    .restart local v8    # "userObject":Lorg/json/JSONObject;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 118
    .end local v1    # "i":I
    .end local v2    # "jsonResponse":Ljava/lang/String;
    .end local v3    # "length":I
    .end local v5    # "resultsArray":Lorg/json/JSONArray;
    .end local v7    # "userName":Ljava/lang/String;
    .end local v8    # "userObject":Lorg/json/JSONObject;
    :cond_4
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v9, :cond_2

    .line 119
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Jira request failed with "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v9, v10}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 122
    .end local v4    # "responseCode":I
    :catch_0
    move-exception v6

    .line 123
    .local v6, "t":Ljava/lang/Throwable;
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v9, :cond_2

    .line 124
    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v9, v6}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
