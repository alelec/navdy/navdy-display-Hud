.class Lcom/navdy/service/library/network/http/services/JiraClient$4;
.super Ljava/lang/Object;
.source "JiraClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/network/http/services/JiraClient;->attachFilesToTicket(Ljava/lang/String;Ljava/util/List;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

.field final synthetic val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

.field final synthetic val$ticketId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/network/http/services/JiraClient;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

    iput-object p2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    iput-object p3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$ticketId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 1
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 282
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v0, p2}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    .line 285
    :cond_0
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 4
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    :try_start_0
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v0

    .line 291
    .local v0, "responseCode":I
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_0

    const/16 v2, 0xc9

    if-ne v0, v2, :cond_1

    .line 292
    :cond_0
    iget-object v2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v2, :cond_1

    .line 293
    iget-object v2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    iget-object v3, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$ticketId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v0    # "responseCode":I
    :cond_1
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v1

    .line 297
    .local v1, "t":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v2, :cond_1

    .line 298
    iget-object v2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$4;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v2, v1}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
