.class public Lcom/navdy/service/library/util/ScalingUtilities;
.super Ljava/lang/Object;
.source "ScalingUtilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateDstRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;
    .locals 5
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "dstWidth"    # I
    .param p3, "dstHeight"    # I
    .param p4, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    const/4 v4, 0x0

    .line 260
    sget-object v2, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    if-ne p4, v2, :cond_1

    .line 261
    int-to-float v2, p0

    int-to-float v3, p1

    div-float v1, v2, v3

    .line 262
    .local v1, "srcAspect":F
    int-to-float v2, p2

    int-to-float v3, p3

    div-float v0, v2, v3

    .line 264
    .local v0, "dstAspect":F
    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    .line 265
    new-instance v2, Landroid/graphics/Rect;

    int-to-float v3, p2

    div-float/2addr v3, v1

    float-to-int v3, v3

    invoke-direct {v2, v4, v4, p2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 270
    .end local v0    # "dstAspect":F
    .end local v1    # "srcAspect":F
    :goto_0
    return-object v2

    .line 267
    .restart local v0    # "dstAspect":F
    .restart local v1    # "srcAspect":F
    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    int-to-float v3, p3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    invoke-direct {v2, v4, v4, v3, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 270
    .end local v0    # "dstAspect":F
    .end local v1    # "srcAspect":F
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v4, v4, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public static calculateSampleSize(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)I
    .locals 4
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "dstWidth"    # I
    .param p3, "dstHeight"    # I
    .param p4, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    .line 197
    sget-object v2, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    if-ne p4, v2, :cond_1

    .line 198
    int-to-float v2, p0

    int-to-float v3, p1

    div-float v1, v2, v3

    .line 199
    .local v1, "srcAspect":F
    int-to-float v2, p2

    int-to-float v3, p3

    div-float v0, v2, v3

    .line 201
    .local v0, "dstAspect":F
    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    .line 202
    div-int v2, p0, p2

    .line 213
    :goto_0
    return v2

    .line 204
    :cond_0
    div-int v2, p1, p3

    goto :goto_0

    .line 207
    .end local v0    # "dstAspect":F
    .end local v1    # "srcAspect":F
    :cond_1
    int-to-float v2, p0

    int-to-float v3, p1

    div-float v1, v2, v3

    .line 208
    .restart local v1    # "srcAspect":F
    int-to-float v2, p2

    int-to-float v3, p3

    div-float v0, v2, v3

    .line 210
    .restart local v0    # "dstAspect":F
    cmpl-float v2, v1, v0

    if-lez v2, :cond_2

    .line 211
    div-int v2, p1, p3

    goto :goto_0

    .line 213
    :cond_2
    div-int v2, p0, p2

    goto :goto_0
.end method

.method public static calculateSrcRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;
    .locals 9
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "dstWidth"    # I
    .param p3, "dstHeight"    # I
    .param p4, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    const/4 v8, 0x0

    .line 230
    sget-object v6, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->CROP:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    if-ne p4, v6, :cond_1

    .line 231
    int-to-float v6, p0

    int-to-float v7, p1

    div-float v2, v6, v7

    .line 232
    .local v2, "srcAspect":F
    int-to-float v6, p2

    int-to-float v7, p3

    div-float v0, v6, v7

    .line 234
    .local v0, "dstAspect":F
    cmpl-float v6, v2, v0

    if-lez v6, :cond_0

    .line 235
    int-to-float v6, p1

    mul-float/2addr v6, v0

    float-to-int v5, v6

    .line 236
    .local v5, "srcRectWidth":I
    sub-int v6, p0, v5

    div-int/lit8 v4, v6, 0x2

    .line 237
    .local v4, "srcRectLeft":I
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v4, v5

    invoke-direct {v6, v4, v8, v7, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 244
    .end local v0    # "dstAspect":F
    .end local v2    # "srcAspect":F
    .end local v4    # "srcRectLeft":I
    .end local v5    # "srcRectWidth":I
    :goto_0
    return-object v6

    .line 239
    .restart local v0    # "dstAspect":F
    .restart local v2    # "srcAspect":F
    :cond_0
    int-to-float v6, p0

    div-float/2addr v6, v0

    float-to-int v3, v6

    .line 240
    .local v3, "srcRectHeight":I
    sub-int v6, p1, v3

    div-int/lit8 v1, v6, 0x2

    .line 241
    .local v1, "scrRectTop":I
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v1, v3

    invoke-direct {v6, v8, v1, p0, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 244
    .end local v0    # "dstAspect":F
    .end local v1    # "scrRectTop":I
    .end local v2    # "srcAspect":F
    .end local v3    # "srcRectHeight":I
    :cond_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v8, v8, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public static createScaledBitmap(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "unscaledBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "dstWidth"    # I
    .param p2, "dstHeight"    # I
    .param p3, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ne v4, p2, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 99
    .end local p0    # "unscaledBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .line 90
    .restart local p0    # "unscaledBitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, p1, p2, p3}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateSrcRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;

    move-result-object v3

    .line 92
    .local v3, "srcRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, p1, p2, p3}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateDstRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;

    move-result-object v1

    .line 94
    .local v1, "dstRect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 96
    .local v2, "scaledBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 97
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p0, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object p0, v2

    .line 99
    goto :goto_0
.end method

.method public static createScaledBitmapAndRecycleOriginalIfScaled(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "originalBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "dstWidth"    # I
    .param p2, "dstHeight"    # I
    .param p3, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    .line 115
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ne v4, p2, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 129
    .end local p0    # "originalBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .line 119
    .restart local p0    # "originalBitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, p1, p2, p3}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateSrcRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;

    move-result-object v3

    .line 121
    .local v3, "srcRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, p1, p2, p3}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateDstRect(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Rect;

    move-result-object v1

    .line 123
    .local v1, "dstRect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 125
    .local v2, "scaledBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 126
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p0, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 128
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    move-object p0, v2

    .line 129
    goto :goto_0
.end method

.method public static decodeByteArray([BIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "data"    # [B
    .param p1, "dstWidth"    # I
    .param p2, "dstHeight"    # I
    .param p3, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    const/4 v4, 0x0

    .line 142
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 143
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 144
    array-length v2, p0

    invoke-static {p0, v4, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 146
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v2, p1, :cond_0

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v2, p2, :cond_0

    .line 147
    const/4 v0, 0x0

    .line 154
    :goto_0
    array-length v2, p0

    invoke-static {p0, v4, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 155
    .local v1, "unscaledBitmap":Landroid/graphics/Bitmap;
    return-object v1

    .line 149
    .end local v1    # "unscaledBitmap":Landroid/graphics/Bitmap;
    :cond_0
    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 150
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v2, v3, p1, p2, p3}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateSampleSize(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)I

    move-result v2

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_0
.end method

.method public static decodeResource(Landroid/content/res/Resources;IIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "resId"    # I
    .param p2, "dstWidth"    # I
    .param p3, "dstHeight"    # I
    .param p4, "scalingLogic"    # Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .prologue
    .line 62
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 63
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 64
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 65
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 66
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v2, v3, p2, p3, p4}, Lcom/navdy/service/library/util/ScalingUtilities;->calculateSampleSize(IIIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)I

    move-result v2

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 68
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 70
    .local v1, "unscaledBitmap":Landroid/graphics/Bitmap;
    return-object v1
.end method

.method public static encodeByteArray(Landroid/graphics/Bitmap;)[B
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 162
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 163
    .local v0, "stream":Ljava/io/ByteArrayOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 164
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method
