.class public final Lcom/localytics/android/InAppDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "InAppDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/InAppDialogFragment$3;,
        Lcom/localytics/android/InAppDialogFragment$MarketingDialog;,
        Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    }
.end annotation


# static fields
.field private static final AMP_DESCRIPTION:Ljava/lang/String; = "amp_view"

.field private static final CLOSE_BUTTON_DESCRIPTION:Ljava/lang/String; = "close_button"

.field private static final CLOSE_BUTTON_ID:I = 0x1

.field static final DIALOG_TAG:Ljava/lang/String; = "marketing_dialog"

.field private static final WEB_VIEW_ID:I = 0x2

.field private static dismissButtonLocation:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

.field private static sDismissButtonImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAllowableFileDir:Ljava/io/File;

.field private mCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/localytics/android/InAppCallable;",
            ">;"
        }
    .end annotation
.end field

.field private final mEnterAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mExitAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;

.field private mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

.field private mMarketingMessage:Lcom/localytics/android/MarketingMessage;

.field private final mUploadedViewEvent:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/localytics/android/InAppDialogFragment;->sDismissButtonImage:Landroid/graphics/Bitmap;

    .line 110
    sget-object v0, Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;->LEFT:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    sput-object v0, Lcom/localytics/android/InAppDialogFragment;->dismissButtonLocation:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 140
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 141
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mEnterAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 142
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mExitAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 143
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mUploadedViewEvent:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/InAppDialogFragment$MarketingDialog;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/JavaScriptClient;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/localytics/android/InAppDialogFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mEnterAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/localytics/android/InAppDialogFragment;Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;
    .param p1, "x1"    # Ljava/net/URL;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/localytics/android/InAppDialogFragment;->handleFileProtocolRequest(Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/localytics/android/InAppDialogFragment;->sDismissButtonImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/MarketingMessage;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    return-object v0
.end method

.method static synthetic access$300(Lcom/localytics/android/InAppDialogFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mExitAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$400()Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/localytics/android/InAppDialogFragment;->dismissButtonLocation:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    return-object v0
.end method

.method static synthetic access$500(Lcom/localytics/android/InAppDialogFragment;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppDialogFragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    return-object v0
.end method

.method static getInAppDismissButtonLocation()Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/localytics/android/InAppDialogFragment;->dismissButtonLocation:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    return-object v0
.end method

.method private getValueByQueryKey(Ljava/lang/String;Ljava/net/URI;)Ljava/lang/String;
    .locals 11
    .param p1, "queryKey"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/net/URI;

    .prologue
    const/4 v8, 0x0

    .line 609
    invoke-virtual {p2}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v7

    .line 611
    .local v7, "query":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 636
    :cond_0
    :goto_0
    return-object v8

    .line 616
    :cond_1
    invoke-virtual {p2}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v9

    const-string v10, "[&]"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 617
    .local v6, "pairs":[Ljava/lang/String;
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 619
    .local v5, "pair":Ljava/lang/String;
    const-string v9, "[=]"

    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 620
    .local v1, "components":[Ljava/lang/String;
    const/4 v9, 0x0

    aget-object v9, v1, v9

    invoke-virtual {v9, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    .line 622
    const/4 v9, 0x2

    array-length v10, v1

    if-ne v9, v10, :cond_2

    .line 626
    const/4 v9, 0x1

    :try_start_0
    aget-object v9, v1, v9

    const-string v10, "UTF-8"

    invoke-static {v9, v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 628
    :catch_0
    move-exception v2

    .line 630
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_0

    .line 617
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private getValueByQueryKey(Ljava/lang/String;Ljava/net/URL;)Ljava/lang/String;
    .locals 2
    .param p1, "queryKey"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/net/URL;

    .prologue
    .line 643
    :try_start_0
    invoke-virtual {p2}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/localytics/android/InAppDialogFragment;->getValueByQueryKey(Ljava/lang/String;Ljava/net/URI;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 647
    :goto_0
    return-object v1

    .line 645
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/net/URISyntaxException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleCustomProtocolRequest(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 4
    .param p1, "urlString"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 594
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 595
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v2}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 596
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 598
    const-string v2, "[In-app Nav Handler]: An app on this device is registered to handle this protocol scheme. Opening..."

    invoke-static {v2}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 599
    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 600
    invoke-virtual {p2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 601
    sget-object v2, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    .line 604
    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    goto :goto_0
.end method

.method private handleCustomProtocolRequest(Ljava/net/URL;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 1
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 589
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/localytics/android/InAppDialogFragment;->handleCustomProtocolRequest(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v0

    return-object v0
.end method

.method private handleFileProtocolRequest(Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 2
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 514
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    sget-object v0, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    .line 526
    :goto_0
    return-object v0

    .line 518
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "localhost"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/localytics/android/InAppDialogFragment;->isPathWithinCreativeDir(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 521
    sget-object v0, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_INTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    goto :goto_0

    .line 524
    :cond_2
    const-string v0, "[In-app Nav Handler]: Displaying content from your local creatives."

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 526
    sget-object v0, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->DO_NOT_OPEN:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    goto :goto_0
.end method

.method private handleHttpProtocolRequest(Ljava/net/URL;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 8
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v7, 0x0

    .line 558
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    .line 559
    .local v3, "protocol":Ljava/lang/String;
    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "https"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 561
    sget-object v4, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    .line 584
    :goto_0
    return-object v4

    .line 564
    :cond_0
    const-string v4, "[In-app Nav Handler]: Handling a request for an external HTTP address."

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 567
    const-string v4, "ampExternalOpen"

    invoke-direct {p0, v4, p1}, Lcom/localytics/android/InAppDialogFragment;->getValueByQueryKey(Ljava/lang/String;Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    .line 568
    .local v2, "openExternalValue":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "true"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 570
    const-string v4, "[In-app Nav Handler]: Query string hook [%s] set to true. Opening the URL in chrome"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "ampExternalOpen"

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 572
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 573
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v4}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v1, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 574
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 576
    invoke-virtual {p2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 577
    sget-object v4, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    goto :goto_0

    .line 582
    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v4, "[In-app Nav Handler]: Loading HTTP request inside the current in-app view"

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 584
    sget-object v4, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_INTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    goto :goto_0
.end method

.method private isPathWithinCreativeDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "urlString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 531
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 533
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 536
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 543
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 545
    iget-object v2, p0, Lcom/localytics/android/InAppDialogFragment;->mAllowableFileDir:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 547
    const/4 v2, 0x1

    .line 553
    .end local v1    # "file":Ljava/io/File;
    :goto_1
    return v2

    .line 538
    .restart local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    goto :goto_0

    .line 549
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    goto :goto_0

    .line 553
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method static newInstance()Lcom/localytics/android/InAppDialogFragment;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/localytics/android/InAppDialogFragment;

    invoke-direct {v0}, Lcom/localytics/android/InAppDialogFragment;-><init>()V

    .line 152
    .local v0, "fragment":Lcom/localytics/android/InAppDialogFragment;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/localytics/android/InAppDialogFragment;->setRetainInstance(Z)V

    .line 153
    return-object v0
.end method

.method static setDismissButtonImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 158
    sget-object v0, Lcom/localytics/android/InAppDialogFragment;->sDismissButtonImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/localytics/android/InAppDialogFragment;->sDismissButtonImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 162
    :cond_0
    sput-object p0, Lcom/localytics/android/InAppDialogFragment;->sDismissButtonImage:Landroid/graphics/Bitmap;

    .line 163
    return-void
.end method

.method static setInAppDismissButtonLocation(Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;)V
    .locals 0
    .param p0, "buttonLocation"    # Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    .prologue
    .line 172
    sput-object p0, Lcom/localytics/android/InAppDialogFragment;->dismissButtonLocation:Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    .line 173
    return-void
.end method

.method private tagMarketingActionEventWithAction(Ljava/lang/String;)V
    .locals 13
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 274
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment;->mUploadedViewEvent:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 281
    const-string v7, "The in-app action for this message has already been set. Ignoring in-app Action: [%s]"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_0

    .line 286
    :cond_2
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 287
    .local v1, "attributes":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "ampAction"

    invoke-virtual {v1, v7, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-string v7, "type"

    const-string v8, "In-App"

    invoke-virtual {v1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    const-string v7, "ampCampaignId"

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    const-string v9, "campaign_id"

    invoke-virtual {v8, v9}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    const-string v7, "ampCampaign"

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    const-string v9, "rule_name_non_unique"

    invoke-virtual {v8, v9}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    const-string v7, "Schema Version - Client"

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    const-string v8, "schema_version"

    invoke-virtual {v7, v8}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 293
    .local v6, "schemaVersion":Ljava/lang/Object;
    if-eqz v6, :cond_3

    .line 295
    const-string v7, "Schema Version - Server"

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    :cond_3
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    const-string v8, "ab_test"

    invoke-virtual {v7, v8}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299
    .local v0, "ab":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 301
    const-string v7, "ampAB"

    invoke-virtual {v1, v7, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    :cond_4
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    if-eqz v7, :cond_0

    .line 307
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v7, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/InAppCallable;

    .line 308
    .local v3, "callable":Lcom/localytics/android/InAppCallable;
    if-eqz v3, :cond_5

    .line 310
    new-array v7, v12, [Ljava/lang/Object;

    const-string v8, "ampView"

    aput-object v8, v7, v11

    aput-object v1, v7, v10

    invoke-virtual {v3, v7}, Lcom/localytics/android/InAppCallable;->call([Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    :cond_5
    sget-boolean v7, Lcom/localytics/android/Constants;->IS_LOGGING_ENABLED:Z

    if-eqz v7, :cond_0

    .line 315
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    .local v2, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 318
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "Key = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Value = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 320
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    const-string v7, "In-app event tagged successfully.\n   Attributes Dictionary = \n%s"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private tagMarketingActionForURL(Ljava/net/URI;)V
    .locals 5
    .param p1, "url"    # Ljava/net/URI;

    .prologue
    .line 494
    const-string v2, "ampAction"

    invoke-direct {p0, v2, p1}, Lcom/localytics/android/InAppDialogFragment;->getValueByQueryKey(Ljava/lang/String;Ljava/net/URI;)Ljava/lang/String;

    move-result-object v0

    .line 495
    .local v0, "marketingActionValue":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 497
    const-string v2, "Attempting to tag event with custom in-app action. [Action: %s]"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 498
    invoke-direct {p0, v0}, Lcom/localytics/android/InAppDialogFragment;->tagMarketingActionEventWithAction(Ljava/lang/String;)V

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 505
    .local v1, "protocol":Ljava/lang/String;
    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 507
    const-string v2, "click"

    invoke-direct {p0, v2}, Lcom/localytics/android/InAppDialogFragment;->tagMarketingActionEventWithAction(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method dismissCampaign()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "X"

    invoke-direct {p0, v0}, Lcom/localytics/android/InAppDialogFragment;->tagMarketingActionEventWithAction(Ljava/lang/String;)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v0}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismiss()V

    .line 185
    :cond_1
    return-void
.end method

.method handleUrl(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .locals 9
    .param p1, "urlString"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 426
    move-object v4, p1

    .line 428
    .local v4, "url":Ljava/lang/String;
    const-string v5, "[In-app Nav Handler]: Evaluating in-app URL:\n\tURL:%s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 430
    sget-object v2, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    .line 433
    .local v2, "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :try_start_0
    const-string v5, "://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 435
    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 439
    :cond_0
    new-instance v5, Ljava/net/URI;

    invoke-direct {v5, v4}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/localytics/android/InAppDialogFragment;->tagMarketingActionForURL(Ljava/net/URI;)V

    .line 445
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 448
    .local v0, "aURL":Ljava/net/URL;
    invoke-direct {p0, v0}, Lcom/localytics/android/InAppDialogFragment;->handleFileProtocolRequest(Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v2

    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v5, :cond_2

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_1

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    :cond_1
    move-object v3, v2

    .line 488
    .end local v0    # "aURL":Ljava/net/URL;
    .end local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .local v3, "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :goto_0
    return-object v3

    .line 454
    .end local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v0    # "aURL":Ljava/net/URL;
    .restart local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :cond_2
    :try_start_1
    invoke-direct {p0, v0, p2}, Lcom/localytics/android/InAppDialogFragment;->handleHttpProtocolRequest(Ljava/net/URL;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v2

    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v2, v5, :cond_4

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_3

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    :cond_3
    move-object v3, v2

    .end local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    goto :goto_0

    .line 460
    .end local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :cond_4
    :try_start_2
    invoke-direct {p0, v0, p2}, Lcom/localytics/android/InAppDialogFragment;->handleCustomProtocolRequest(Ljava/net/URL;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v2

    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eq v2, v5, :cond_6

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_5

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    :cond_5
    move-object v3, v2

    .end local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    goto :goto_0

    .line 465
    .end local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :cond_6
    :try_start_3
    const-string v5, "[In-app Nav Handler]: Protocol handler scheme not recognized. Attempting to load the URL... [Scheme: %s]"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_7

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    .end local v0    # "aURL":Ljava/net/URL;
    :cond_7
    :goto_1
    move-object v3, v2

    .line 488
    .end local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    goto :goto_0

    .line 467
    .end local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :catch_0
    move-exception v1

    .line 470
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-direct {p0, v4, p2}, Lcom/localytics/android/InAppDialogFragment;->handleCustomProtocolRequest(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v2

    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->PROTOCOL_UNMATCHED:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eq v2, v5, :cond_9

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_8

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    :cond_8
    move-object v3, v2

    .end local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    goto :goto_0

    .line 475
    .end local v3    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v2    # "result":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    :cond_9
    :try_start_5
    const-string v5, "[In-app Nav Handler]: Invalid url %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 476
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 481
    sget-object v5, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v5, :cond_7

    .line 483
    iget-object v5, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    goto :goto_1

    .line 481
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    sget-object v6, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v6, :cond_a

    .line 483
    iget-object v6, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v6}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->dismissWithAnimation()V

    :cond_a
    throw v5
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 236
    const-string v0, "[InAppDialogFragment]: onActivityCreated"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 237
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 238
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 190
    const-string v0, "[InAppDialogFragment]: onAttach"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 191
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 192
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    const-string v0, "[InAppDialogFragment]: onCreate"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 205
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 206
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 211
    const-string v0, "[InAppDialogFragment]: onCreateDialog"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 212
    new-instance v0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {p0}, Lcom/localytics/android/InAppDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x103000b

    invoke-direct {v0, p0, v1, v2}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;-><init>(Lcom/localytics/android/InAppDialogFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 328
    const-string v0, "[InAppDialogFragment]: onCreateView"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 329
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 356
    const-string v1, "[InAppDialogFragment]: onDestroy"

    invoke-static {v1}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 357
    iget-object v1, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/localytics/android/InAppCallable;

    .line 360
    .local v0, "callable":Lcom/localytics/android/InAppCallable;
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 362
    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/localytics/android/InAppCallable;->call([Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    .end local v0    # "callable":Lcom/localytics/android/InAppCallable;
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 366
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 264
    const-string v0, "[InAppDialogFragment]: onDestroyView"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 265
    invoke-virtual {p0}, Lcom/localytics/android/InAppDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/localytics/android/InAppDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/localytics/android/InAppDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 269
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 270
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 197
    const-string v0, "[InAppDialogFragment]: onDetach"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 198
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    .line 199
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 218
    const-string v1, "[InAppDialogFragment]: onDismiss"

    invoke-static {v1}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 221
    :try_start_0
    iget-object v1, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    if-eqz v1, :cond_0

    .line 223
    const-string v1, "X"

    invoke-direct {p0, v1}, Lcom/localytics/android/InAppDialogFragment;->tagMarketingActionEventWithAction(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 231
    return-void

    .line 226
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MarketingDialogFragment onDismiss"

    invoke-static {v1, v0}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 349
    const-string v0, "[InAppDialogFragment]: onPause"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 350
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 351
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 342
    const-string v0, "[InAppDialogFragment]: onResume"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 343
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 344
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 250
    const-string v0, "[InAppDialogFragment]: onSaveInstanceState"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 251
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 252
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 243
    const-string v0, "[InAppDialogFragment]: onStart"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 244
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 245
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 257
    const-string v0, "[InAppDialogFragment]: onStop"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 258
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStop()V

    .line 259
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 335
    const-string v0, "[InAppDialogFragment]: onViewStateRestored"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 336
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 337
    return-void
.end method

.method public setCallbacks(Landroid/util/SparseArray;)Lcom/localytics/android/InAppDialogFragment;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/localytics/android/InAppCallable;",
            ">;)",
            "Lcom/localytics/android/InAppDialogFragment;"
        }
    .end annotation

    .prologue
    .line 653
    .local p1, "callbacks":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/localytics/android/InAppCallable;>;"
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment;->mCallbacks:Landroid/util/SparseArray;

    .line 654
    return-object p0
.end method

.method setData(Lcom/localytics/android/MarketingMessage;)Lcom/localytics/android/InAppDialogFragment;
    .locals 3
    .param p1, "marketingMessage"    # Lcom/localytics/android/MarketingMessage;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;

    .line 371
    new-instance v0, Ljava/io/File;

    const-string v2, "base_path"

    invoke-virtual {p1, v2}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 374
    .local v0, "dirFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/localytics/android/InAppDialogFragment;->mAllowableFileDir:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    :goto_0
    return-object p0

    .line 376
    :catch_0
    move-exception v1

    .line 378
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/localytics/android/InAppDialogFragment;->mAllowableFileDir:Ljava/io/File;

    goto :goto_0
.end method

.method setJavaScriptClient(Lcom/localytics/android/JavaScriptClient;)Lcom/localytics/android/InAppDialogFragment;
    .locals 3
    .param p1, "javaScriptClient"    # Lcom/localytics/android/JavaScriptClient;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment;->mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;

    .line 386
    iget-object v1, p0, Lcom/localytics/android/InAppDialogFragment;->mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;

    invoke-virtual {v1}, Lcom/localytics/android/JavaScriptClient;->getCallbacks()Landroid/util/SparseArray;

    move-result-object v0

    .line 388
    .local v0, "callbacks":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/localytics/android/InAppCallable;>;"
    const/16 v1, 0xf

    new-instance v2, Lcom/localytics/android/InAppDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/localytics/android/InAppDialogFragment$1;-><init>(Lcom/localytics/android/InAppDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 410
    const/4 v1, 0x4

    new-instance v2, Lcom/localytics/android/InAppDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/localytics/android/InAppDialogFragment$2;-><init>(Lcom/localytics/android/InAppDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 421
    return-object p0
.end method
