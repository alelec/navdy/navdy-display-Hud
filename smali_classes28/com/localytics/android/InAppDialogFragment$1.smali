.class Lcom/localytics/android/InAppDialogFragment$1;
.super Lcom/localytics/android/InAppCallable;
.source "InAppDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/InAppDialogFragment;->setJavaScriptClient(Lcom/localytics/android/JavaScriptClient;)Lcom/localytics/android/InAppDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/InAppDialogFragment;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppDialogFragment;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment$1;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-direct {p0}, Lcom/localytics/android/InAppCallable;-><init>()V

    return-void
.end method


# virtual methods
.method call([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 393
    const/4 v2, 0x0

    aget-object v1, p1, v2

    check-cast v1, Ljava/lang/String;

    .line 394
    .local v1, "url":Ljava/lang/String;
    iget-object v2, p0, Lcom/localytics/android/InAppDialogFragment$1;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # getter for: Lcom/localytics/android/InAppDialogFragment;->mMarketingDialog:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;
    invoke-static {v2}, Lcom/localytics/android/InAppDialogFragment;->access$000(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    move-result-object v2

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mWebView:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;
    invoke-static {v2}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$100(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    move-result-object v0

    .line 395
    .local v0, "mWebView":Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;
    iget-object v2, p0, Lcom/localytics/android/InAppDialogFragment$1;->this$0:Lcom/localytics/android/InAppDialogFragment;

    iget-object v3, p0, Lcom/localytics/android/InAppDialogFragment$1;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-virtual {v3}, Lcom/localytics/android/InAppDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/localytics/android/InAppDialogFragment;->handleUrl(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v2

    sget-object v3, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_EXTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-ne v2, v3, :cond_0

    .line 397
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/localytics/android/InAppDialogFragment$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/localytics/android/InAppDialogFragment$1$1;-><init>(Lcom/localytics/android/InAppDialogFragment$1;Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 405
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method
