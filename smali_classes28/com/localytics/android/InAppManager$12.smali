.class Lcom/localytics/android/InAppManager$12;
.super Lcom/localytics/android/InAppCallable;
.source "InAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/InAppManager;->_showInAppTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/InAppManager;

.field final synthetic val$adapter:Lcom/localytics/android/InAppMessagesAdapter;

.field final synthetic val$listView:Lcom/localytics/android/TestModeListView;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;Lcom/localytics/android/TestModeListView;)V
    .locals 0

    .prologue
    .line 2378
    iput-object p1, p0, Lcom/localytics/android/InAppManager$12;->this$0:Lcom/localytics/android/InAppManager;

    iput-object p2, p0, Lcom/localytics/android/InAppManager$12;->val$adapter:Lcom/localytics/android/InAppMessagesAdapter;

    iput-object p3, p0, Lcom/localytics/android/InAppManager$12;->val$listView:Lcom/localytics/android/TestModeListView;

    invoke-direct {p0}, Lcom/localytics/android/InAppCallable;-><init>()V

    return-void
.end method


# virtual methods
.method call([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/localytics/android/InAppManager$12;->val$adapter:Lcom/localytics/android/InAppMessagesAdapter;

    invoke-virtual {v0}, Lcom/localytics/android/InAppMessagesAdapter;->updateDataSet()Z

    .line 2383
    iget-object v0, p0, Lcom/localytics/android/InAppManager$12;->val$listView:Lcom/localytics/android/TestModeListView;

    iget-object v1, p0, Lcom/localytics/android/InAppManager$12;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v1}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "marketing_test_mode_list"

    invoke-virtual {v0, v1, v2}, Lcom/localytics/android/TestModeListView;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2384
    iget-object v0, p0, Lcom/localytics/android/InAppManager$12;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v0}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 2385
    const/4 v0, 0x0

    return-object v0
.end method
