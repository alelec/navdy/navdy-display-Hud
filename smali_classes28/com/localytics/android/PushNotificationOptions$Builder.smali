.class public Lcom/localytics/android/PushNotificationOptions$Builder;
.super Ljava/lang/Object;
.source "PushNotificationOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/PushNotificationOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAccentColor:I

.field private mCategory:Ljava/lang/String;

.field private mLargeIcon:Landroid/graphics/Bitmap;

.field private mLaunchIntent:Landroid/content/Intent;

.field private mLedLightColor:I

.field private mLedLightOffMillis:I

.field private mLedLightOnMillis:I

.field private mOnlyAlertOnce:Z

.field private mPriority:I

.field private mSmallIcon:I

.field private mSoundUri:Landroid/net/Uri;

.field private mTitle:Ljava/lang/CharSequence;

.field private mVibrationPattern:[J

.field private mVisibility:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mAccentColor:I

    .line 51
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mCategory:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSoundUri:Landroid/net/Uri;

    .line 53
    iput v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVisibility:I

    .line 54
    iput v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mPriority:I

    .line 55
    iput v2, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSmallIcon:I

    .line 56
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    .line 57
    iput v2, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightColor:I

    .line 58
    iput v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOnMillis:I

    .line 59
    iput v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOffMillis:I

    .line 60
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVibrationPattern:[J

    .line 61
    iput-boolean v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mOnlyAlertOnce:Z

    .line 62
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLaunchIntent:Landroid/content/Intent;

    .line 63
    iput-object v1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mTitle:Ljava/lang/CharSequence;

    return-void
.end method

.method static synthetic access$000(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mAccentColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/localytics/android/PushNotificationOptions$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/localytics/android/PushNotificationOptions$Builder;)[J
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVibrationPattern:[J

    return-object v0
.end method

.method static synthetic access$1100(Lcom/localytics/android/PushNotificationOptions$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mOnlyAlertOnce:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLaunchIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/localytics/android/PushNotificationOptions$Builder;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$200(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSoundUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVisibility:I

    return v0
.end method

.method static synthetic access$400(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mPriority:I

    return v0
.end method

.method static synthetic access$500(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSmallIcon:I

    return v0
.end method

.method static synthetic access$600(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightColor:I

    return v0
.end method

.method static synthetic access$800(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOnMillis:I

    return v0
.end method

.method static synthetic access$900(Lcom/localytics/android/PushNotificationOptions$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;

    .prologue
    .line 48
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOffMillis:I

    return v0
.end method


# virtual methods
.method public build()Lcom/localytics/android/PushNotificationOptions;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lcom/localytics/android/PushNotificationOptions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/localytics/android/PushNotificationOptions;-><init>(Lcom/localytics/android/PushNotificationOptions$Builder;Lcom/localytics/android/PushNotificationOptions$1;)V

    return-object v0
.end method

.method public setAccentColor(I)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "argbColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 67
    iput p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mAccentColor:I

    .line 68
    return-object p0
.end method

.method public setCategory(Ljava/lang/String;)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "category"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mCategory:Ljava/lang/String;

    .line 74
    return-object p0
.end method

.method public setLargeIcon(Landroid/graphics/Bitmap;)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 1
    .param p1, "largeIcon"    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 103
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 107
    :cond_0
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    .line 108
    return-object p0
.end method

.method public setLaunchIntent(Landroid/content/Intent;)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "launchIntent"    # Landroid/content/Intent;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLaunchIntent:Landroid/content/Intent;

    .line 134
    return-object p0
.end method

.method public setLedLight(III)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "argbColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p2, "onMillis"    # I
    .param p3, "offMilis"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightColor:I

    .line 114
    iput p2, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOnMillis:I

    .line 115
    iput p3, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOffMillis:I

    .line 116
    return-object p0
.end method

.method public setOnlyAlertOnce(Z)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "onlyAlertOnce"    # Z

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mOnlyAlertOnce:Z

    .line 128
    return-object p0
.end method

.method public setPriority(I)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mPriority:I

    .line 92
    return-object p0
.end method

.method public setSmallIcon(I)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "smallIcon"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 97
    iput p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSmallIcon:I

    .line 98
    return-object p0
.end method

.method public setSound(Landroid/net/Uri;)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "sound"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mSoundUri:Landroid/net/Uri;

    .line 80
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 139
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mTitle:Ljava/lang/CharSequence;

    .line 143
    :cond_0
    return-object p0
.end method

.method public setVibrationPattern([J)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "pattern"    # [J

    .prologue
    .line 121
    iput-object p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVibrationPattern:[J

    .line 122
    return-object p0
.end method

.method public setVisibility(I)Lcom/localytics/android/PushNotificationOptions$Builder;
    .locals 0
    .param p1, "visibility"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/localytics/android/PushNotificationOptions$Builder;->mVisibility:I

    .line 86
    return-object p0
.end method
