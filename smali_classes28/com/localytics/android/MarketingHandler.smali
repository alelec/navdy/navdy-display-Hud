.class Lcom/localytics/android/MarketingHandler;
.super Lcom/localytics/android/BaseHandler;
.source "MarketingHandler.java"

# interfaces
.implements Lcom/localytics/android/AnalyticsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/MarketingHandler$MessagingListenersSet;
    }
.end annotation


# static fields
.field private static final MESSAGE_HANDLE_PUSH_RECEIVED:I = 0xcc

.field private static final MESSAGE_IN_APP_MESSAGE_TRIGGER:I = 0xc9

.field private static final MESSAGE_SET_IN_APP_MESSAGE_AS_NOT_DISPLAYED:I = 0xcd

.field private static final MESSAGE_SHOW_IN_APP_MESSAGES_TEST:I = 0xcb

.field private static final MESSAGE_TAG_PUSH_RECEIVED_EVENT:I = 0xce


# instance fields
.field private lastMarketingMessagesHash:I

.field protected mInAppManager:Lcom/localytics/android/InAppManager;

.field protected mPushManager:Lcom/localytics/android/PushManager;


# direct methods
.method constructor <init>(Lcom/localytics/android/LocalyticsDao;Landroid/os/Looper;Landroid/content/Context;)V
    .locals 2
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "appContext"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/localytics/android/BaseHandler;-><init>(Lcom/localytics/android/LocalyticsDao;Landroid/os/Looper;)V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/localytics/android/MarketingHandler;->lastMarketingMessagesHash:I

    .line 66
    const-string v0, "In-app"

    iput-object v0, p0, Lcom/localytics/android/MarketingHandler;->siloName:Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/localytics/android/MarketingHandler$MessagingListenersSet;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/localytics/android/MarketingHandler$MessagingListenersSet;-><init>(Lcom/localytics/android/MarketingHandler;Lcom/localytics/android/MarketingHandler$1;)V

    iput-object v0, p0, Lcom/localytics/android/MarketingHandler;->listeners:Lcom/localytics/android/BaseHandler$ListenersSet;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/localytics/android/MarketingHandler;->doesRetry:Z

    .line 70
    new-instance v0, Lcom/localytics/android/InAppManager;

    new-instance v1, Lcom/localytics/android/FileDownloader;

    invoke-direct {v1}, Lcom/localytics/android/FileDownloader;-><init>()V

    invoke-direct {v0, p1, p0, v1}, Lcom/localytics/android/InAppManager;-><init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;Lcom/localytics/android/FileDownloader;)V

    iput-object v0, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    .line 71
    new-instance v0, Lcom/localytics/android/PushManager;

    invoke-direct {v0, p1}, Lcom/localytics/android/PushManager;-><init>(Lcom/localytics/android/LocalyticsDao;)V

    iput-object v0, p0, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    .line 74
    invoke-direct {p0, p3}, Lcom/localytics/android/MarketingHandler;->_createLocalyticsDirectory(Landroid/content/Context;)Z

    .line 76
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    .line 77
    return-void
.end method

.method private _createLocalyticsDirectory(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string v2, ".localytics"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected _deleteUploadedData(I)V
    .locals 0
    .param p1, "maxRowToDelete"    # I

    .prologue
    .line 260
    return-void
.end method

.method protected _getDataToUpload()Ljava/util/TreeMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 247
    .local v0, "tree":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    return-object v0
.end method

.method protected _getMaxRowToUpload()I
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x1

    return v0
.end method

.method protected _init()V
    .locals 3

    .prologue
    .line 107
    new-instance v0, Lcom/localytics/android/MarketingProvider;

    iget-object v1, p0, Lcom/localytics/android/MarketingHandler;->siloName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-direct {v0, v1, v2}, Lcom/localytics/android/MarketingProvider;-><init>(Ljava/lang/String;Lcom/localytics/android/LocalyticsDao;)V

    iput-object v0, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    .line 108
    invoke-virtual {p0}, Lcom/localytics/android/MarketingHandler;->_setProviderForManagers()V

    .line 109
    return-void
.end method

.method protected _onUploadCompleted(Ljava/lang/String;)V
    .locals 6
    .param p1, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 265
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    .line 266
    .local v3, "responseBodyHash":I
    :goto_0
    iget v4, p0, Lcom/localytics/android/MarketingHandler;->lastMarketingMessagesHash:I

    if-eq v3, v4, :cond_0

    .line 271
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/localytics/android/JsonHelper;->toMap(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v2

    .line 272
    .local v2, "marketingMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "config"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 273
    .local v0, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    invoke-virtual {v4, v2, v0}, Lcom/localytics/android/InAppManager;->_processMarketingObject(Ljava/util/Map;Ljava/util/Map;)V

    .line 275
    iput v3, p0, Lcom/localytics/android/MarketingHandler;->lastMarketingMessagesHash:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    .end local v0    # "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "marketingMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    invoke-virtual {v4}, Lcom/localytics/android/InAppManager;->_handleTestCampaigns()Z

    move-result v4

    if-nez v4, :cond_1

    .line 285
    iget-object v4, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v5, "open"

    invoke-interface {v4, v5}, Lcom/localytics/android/LocalyticsDao;->triggerInAppMessage(Ljava/lang/String;)V

    .line 287
    :cond_1
    return-void

    .line 265
    .end local v3    # "responseBodyHash":I
    :cond_2
    iget v3, p0, Lcom/localytics/android/MarketingHandler;->lastMarketingMessagesHash:I

    goto :goto_0

    .line 277
    .restart local v3    # "responseBodyHash":I
    :catch_0
    move-exception v1

    .line 279
    .local v1, "e":Lorg/json/JSONException;
    const-string v4, "JSONException"

    invoke-static {v4, v1}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method protected _setProviderForManagers()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    iget-object v1, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    invoke-virtual {v0, v1}, Lcom/localytics/android/InAppManager;->setProvider(Lcom/localytics/android/BaseProvider;)V

    .line 114
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    iget-object v1, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    invoke-virtual {v0, v1}, Lcom/localytics/android/PushManager;->setProvider(Lcom/localytics/android/BaseProvider;)V

    .line 115
    return-void
.end method

.method dismissCurrentInAppMessage()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    invoke-virtual {v0}, Lcom/localytics/android/InAppManager;->dismissCurrentInAppMessage()V

    .line 130
    return-void
.end method

.method displayInAppMessage(Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 6
    .param p1, "eventName"    # Ljava/lang/String;
    .param p3, "delay"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v4, 0xc9

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 161
    const-string v2, "open"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v1

    const/4 v3, 0x0

    aput-object v3, v2, v0

    invoke-virtual {p0, v4, v2}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x2710

    if-eqz p3, :cond_0

    :goto_0
    int-to-long v0, v0

    mul-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lcom/localytics/android/MarketingHandler;->queueMessageDelayed(Landroid/os/Message;J)Z

    .line 169
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 163
    goto :goto_0

    .line 167
    :cond_1
    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v1

    aput-object p2, v2, v0

    invoke-virtual {p0, v4, v2}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method getInAppDismissButtonLocation()Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;
    .locals 1

    .prologue
    .line 219
    invoke-static {}, Lcom/localytics/android/InAppDialogFragment;->getInAppDismissButtonLocation()Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    move-result-object v0

    return-object v0
.end method

.method protected getUploadThread(Ljava/util/TreeMap;Ljava/lang/String;)Lcom/localytics/android/BaseUploadThread;
    .locals 2
    .param p2, "customerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/localytics/android/BaseUploadThread;"
        }
    .end annotation

    .prologue
    .line 254
    .local p1, "data":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    new-instance v0, Lcom/localytics/android/MarketingDownloader;

    iget-object v1, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/localytics/android/MarketingDownloader;-><init>(Lcom/localytics/android/BaseHandler;Ljava/util/TreeMap;Ljava/lang/String;Lcom/localytics/android/LocalyticsDao;)V

    return-object v0
.end method

.method protected handleMessageExtended(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 292
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 387
    :pswitch_0
    invoke-super {p0, p1}, Lcom/localytics/android/BaseHandler;->handleMessageExtended(Landroid/os/Message;)V

    .line 391
    :goto_0
    return-void

    .line 296
    :pswitch_1
    const-string v5, "In-app handler received MESSAGE_INAPP_TRIGGER"

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 298
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    move-object v4, v5

    check-cast v4, [Ljava/lang/Object;

    .line 300
    .local v4, "params":[Ljava/lang/Object;
    const/4 v5, 0x0

    aget-object v2, v4, v5

    check-cast v2, Ljava/lang/String;

    .line 302
    .local v2, "event":Ljava/lang/String;
    aget-object v0, v4, v6

    check-cast v0, Ljava/util/Map;

    .line 304
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    new-instance v6, Lcom/localytics/android/MarketingHandler$2;

    invoke-direct {v6, p0, v2, v0}, Lcom/localytics/android/MarketingHandler$2;-><init>(Lcom/localytics/android/MarketingHandler;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v5, v6}, Lcom/localytics/android/BaseProvider;->runBatchTransaction(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 316
    .end local v0    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "event":Ljava/lang/String;
    .end local v4    # "params":[Ljava/lang/Object;
    :pswitch_2
    const-string v5, "In-app handler received MESSAGE_SHOW_INAPP_TEST"

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 322
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p0, v6, v5}, Lcom/localytics/android/MarketingHandler;->_upload(ZLjava/lang/String;)V

    .line 323
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v6, Lcom/localytics/android/MarketingHandler$3;

    invoke-direct {v6, p0}, Lcom/localytics/android/MarketingHandler$3;-><init>(Lcom/localytics/android/MarketingHandler;)V

    const-wide/16 v8, 0x3e8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 335
    :pswitch_3
    const-string v5, "In-app handler received MESSAGE_HANDLE_PUSH_RECEIVED"

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 337
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/content/Intent;

    .line 339
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    new-instance v6, Lcom/localytics/android/MarketingHandler$4;

    invoke-direct {v6, p0, v3}, Lcom/localytics/android/MarketingHandler$4;-><init>(Lcom/localytics/android/MarketingHandler;Landroid/content/Intent;)V

    invoke-virtual {v5, v6}, Lcom/localytics/android/BaseProvider;->runBatchTransaction(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 355
    .end local v3    # "intent":Landroid/content/Intent;
    :pswitch_4
    const-string v5, "Marketing handler received MESSAGE_SET_IN_APP_MESSAGE_AS_NOT_DISPLAYED"

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 357
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 359
    .local v1, "campaignId":I
    iget-object v5, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    new-instance v6, Lcom/localytics/android/MarketingHandler$5;

    invoke-direct {v6, p0, v1}, Lcom/localytics/android/MarketingHandler$5;-><init>(Lcom/localytics/android/MarketingHandler;I)V

    invoke-virtual {v5, v6}, Lcom/localytics/android/BaseProvider;->runBatchTransaction(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 371
    .end local v1    # "campaignId":I
    :pswitch_5
    const-string v5, "Marketing handler received MESSAGE_TAG_PUSH_RECEIVED_EVENT"

    invoke-static {v5}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 373
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/content/Intent;

    .line 375
    .restart local v3    # "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/localytics/android/MarketingHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    new-instance v6, Lcom/localytics/android/MarketingHandler$6;

    invoke-direct {v6, p0, v3}, Lcom/localytics/android/MarketingHandler$6;-><init>(Lcom/localytics/android/MarketingHandler;Landroid/content/Intent;)V

    invoke-virtual {v5, v6}, Lcom/localytics/android/BaseProvider;->runBatchTransaction(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method handleNotificationReceived(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 173
    const/16 v0, 0xcc

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    .line 174
    return-void
.end method

.method handlePushNotificationOpened(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    invoke-virtual {v0, p1}, Lcom/localytics/android/PushManager;->handlePushNotificationOpened(Landroid/content/Intent;)V

    .line 135
    return-void
.end method

.method handleTestModeIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 183
    if-nez p1, :cond_0

    .line 185
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "intent cannot be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 188
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 190
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "amp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getApiKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 215
    :cond_1
    :goto_0
    return-void

    .line 195
    :cond_2
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, "path":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "host":Ljava/lang/String;
    const-string v4, "[/]"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "components":[Ljava/lang/String;
    array-length v4, v0

    if-eqz v4, :cond_1

    .line 204
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "testMode"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 206
    aget-object v4, v0, v8

    const-string v5, "enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 208
    iget-object v4, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4, v7}, Lcom/localytics/android/LocalyticsDao;->setTestModeEnabled(Z)V

    goto :goto_0

    .line 210
    :cond_3
    aget-object v4, v0, v8

    const-string v5, "launch"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v0, v7

    const-string v5, "push"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 212
    iget-object v4, p0, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    invoke-virtual {v4, v0}, Lcom/localytics/android/PushManager;->handlePushTestMode([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public localyticsDidTagEvent(Ljava/lang/String;Ljava/util/Map;J)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p3, "clv"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 434
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0, p1, p2}, Lcom/localytics/android/LocalyticsDao;->triggerInAppMessage(Ljava/lang/String;Ljava/util/Map;)V

    .line 435
    return-void
.end method

.method public localyticsSessionDidOpen(ZZZ)V
    .locals 4
    .param p1, "isFirstEverSession"    # Z
    .param p2, "isFirstSessionSinceUpgrade"    # Z
    .param p3, "didResumeOldSession"    # Z

    .prologue
    .line 402
    if-nez p3, :cond_0

    .line 404
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdInMemory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->upload(Ljava/lang/String;)V

    .line 407
    :cond_0
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 409
    if-eqz p1, :cond_1

    .line 411
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v1, "AMP First Run"

    invoke-interface {v0, v1}, Lcom/localytics/android/LocalyticsDao;->triggerInAppMessage(Ljava/lang/String;)V

    .line 413
    :cond_1
    if-eqz p2, :cond_2

    .line 415
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v1, "AMP upgrade"

    invoke-interface {v0, v1}, Lcom/localytics/android/LocalyticsDao;->triggerInAppMessage(Ljava/lang/String;)V

    .line 417
    :cond_2
    if-nez p3, :cond_3

    .line 419
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/localytics/android/InAppManager;->setSessionStartInAppMessageShown(Z)V

    .line 420
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v1, "open"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/localytics/android/LocalyticsDao;->triggerInAppMessage(Ljava/lang/String;Ljava/util/Map;Z)V

    .line 423
    :cond_3
    return-void
.end method

.method public localyticsSessionWillClose()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public localyticsSessionWillOpen(ZZZ)V
    .locals 0
    .param p1, "isFirstEverSession"    # Z
    .param p2, "isFirstSessionSinceUpgrade"    # Z
    .param p3, "willResumeOldSession"    # Z

    .prologue
    .line 397
    return-void
.end method

.method setDismissButtonImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 234
    invoke-static {p1}, Lcom/localytics/android/InAppDialogFragment;->setDismissButtonImage(Landroid/graphics/Bitmap;)V

    .line 235
    return-void
.end method

.method setFragmentManager(Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mInAppManager:Lcom/localytics/android/InAppManager;

    invoke-virtual {v0, p1}, Lcom/localytics/android/InAppManager;->setFragmentManager(Landroid/support/v4/app/FragmentManager;)V

    .line 125
    return-void
.end method

.method setInAppAsDisplayed(I)Z
    .locals 1
    .param p1, "campaignId"    # I

    .prologue
    .line 144
    new-instance v0, Lcom/localytics/android/MarketingHandler$1;

    invoke-direct {v0, p0, p1}, Lcom/localytics/android/MarketingHandler$1;-><init>(Lcom/localytics/android/MarketingHandler;I)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->getBool(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method setInAppDismissButtonLocation(Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;)V
    .locals 0
    .param p1, "buttonLocation"    # Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;

    .prologue
    .line 224
    invoke-static {p1}, Lcom/localytics/android/InAppDialogFragment;->setInAppDismissButtonLocation(Lcom/localytics/android/Localytics$InAppMessageDismissButtonLocation;)V

    .line 225
    return-void
.end method

.method setInAppMessageAsNotDisplayed(I)V
    .locals 2
    .param p1, "campaignId"    # I

    .prologue
    .line 156
    const/16 v0, 0xcd

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    .line 157
    return-void
.end method

.method setPushNotificationOptions(Lcom/localytics/android/PushNotificationOptions;)V
    .locals 1
    .param p1, "options"    # Lcom/localytics/android/PushNotificationOptions;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 139
    iget-object v0, p0, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    invoke-virtual {v0, p1}, Lcom/localytics/android/PushManager;->setPushNotificationOptions(Lcom/localytics/android/PushNotificationOptions;)V

    .line 140
    return-void
.end method

.method showMarketingTest()V
    .locals 2

    .prologue
    .line 229
    const/16 v0, 0xcb

    iget-object v1, p0, Lcom/localytics/android/MarketingHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdInMemory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    .line 230
    return-void
.end method

.method tagPushReceivedEvent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 178
    const/16 v0, 0xce

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/MarketingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/MarketingHandler;->queueMessage(Landroid/os/Message;)Z

    .line 179
    return-void
.end method
