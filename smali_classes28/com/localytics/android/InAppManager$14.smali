.class Lcom/localytics/android/InAppManager$14;
.super Lcom/localytics/android/InAppCallable;
.source "InAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/InAppManager;->_showInAppTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/InAppManager;

.field final synthetic val$adapter:Lcom/localytics/android/InAppMessagesAdapter;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;)V
    .locals 0

    .prologue
    .line 2417
    iput-object p1, p0, Lcom/localytics/android/InAppManager$14;->this$0:Lcom/localytics/android/InAppManager;

    iput-object p2, p0, Lcom/localytics/android/InAppManager$14;->val$adapter:Lcom/localytics/android/InAppMessagesAdapter;

    invoke-direct {p0}, Lcom/localytics/android/InAppCallable;-><init>()V

    return-void
.end method


# virtual methods
.method call([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 2421
    iget-object v0, p0, Lcom/localytics/android/InAppManager$14;->this$0:Lcom/localytics/android/InAppManager;

    iget-object v0, v0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v1, "Test Mode Update Data"

    invoke-interface {v0, v1}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;)V

    .line 2422
    iget-object v0, p0, Lcom/localytics/android/InAppManager$14;->this$0:Lcom/localytics/android/InAppManager;

    iget-object v1, p0, Lcom/localytics/android/InAppManager$14;->val$adapter:Lcom/localytics/android/InAppMessagesAdapter;

    # setter for: Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;
    invoke-static {v0, v1}, Lcom/localytics/android/InAppManager;->access$402(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;)Lcom/localytics/android/InAppMessagesAdapter;

    .line 2423
    iget-object v0, p0, Lcom/localytics/android/InAppManager$14;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;
    invoke-static {v0}, Lcom/localytics/android/InAppManager;->access$200(Lcom/localytics/android/InAppManager;)Lcom/localytics/android/MarketingHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/localytics/android/InAppManager$14;->this$0:Lcom/localytics/android/InAppManager;

    iget-object v1, v1, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdInMemory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/localytics/android/MarketingHandler;->upload(Ljava/lang/String;)V

    .line 2424
    const/4 v0, 0x0

    return-object v0
.end method
