.class Lcom/localytics/android/PushManager;
.super Lcom/localytics/android/BaseMarketingManager;
.source "PushManager.java"


# static fields
.field private static final ACTION_ATTRIBUTE:Ljava/lang/String; = "Action"

.field private static final APP_CONTEXT_ATTRIBUTE:Ljava/lang/String; = "App Context"

.field private static final CAMPAIGN_ID_ATTRIBUTE:Ljava/lang/String; = "Campaign ID"

.field private static final CREATIVE_DISPLAYED_ATTRIBUTE:Ljava/lang/String; = "Creative Displayed"

.field private static final CREATIVE_ID_ATTRIBUTE:Ljava/lang/String; = "Creative ID"

.field private static final CREATIVE_TYPE_ATTRIBUTE:Ljava/lang/String; = "Creative Type"

.field private static final PUSH_API_URL_TEMPLATE:Ljava/lang/String; = "http://pushapi.localytics.com/push_test?platform=android&type=prod&campaign=%s&creative=%s&token=%s&install_id=%s&client_ts=%s"

.field private static final PUSH_NOTIFICATIONS_ENABLED_ATTRIBUTE:Ljava/lang/String; = "Push Notifications Enabled"

.field private static final PUSH_OPENED_EVENT:Ljava/lang/String; = "Localytics Push Opened"

.field private static final PUSH_RECEIVED_EVENT:Ljava/lang/String; = "Localytics Push Received"


# instance fields
.field private mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;


# direct methods
.method constructor <init>(Lcom/localytics/android/LocalyticsDao;)V
    .locals 1
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/localytics/android/BaseMarketingManager;-><init>(Lcom/localytics/android/LocalyticsDao;)V

    .line 45
    new-instance v0, Lcom/localytics/android/PushNotificationOptions$Builder;

    invoke-direct {v0}, Lcom/localytics/android/PushNotificationOptions$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/localytics/android/PushNotificationOptions$Builder;->build()Lcom/localytics/android/PushNotificationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    .line 50
    return-void
.end method


# virtual methods
.method _showPushNotification(Landroid/content/Intent;)V
    .locals 25
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 169
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "message"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 173
    .local v15, "message":Ljava/lang/String;
    :try_start_0
    new-instance v14, Lorg/json/JSONObject;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "ll"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    .local v14, "llObject":Lorg/json/JSONObject;
    const-string v22, "ca"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 183
    .local v7, "campaignId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 184
    .local v2, "appContext":Landroid/content/Context;
    const-string v4, ""

    .line 185
    .local v4, "appName":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getSmallIcon()I

    move-result v3

    .line 186
    .local v3, "appIcon":I
    invoke-static {v2, v3}, Lcom/localytics/android/DatapointHelper;->isValidResourceId(Landroid/content/Context;I)Z

    move-result v22

    if-nez v22, :cond_0

    .line 188
    invoke-static {v2}, Lcom/localytics/android/DatapointHelper;->getLocalyticsNotificationIcon(Landroid/content/Context;)I

    move-result v3

    .line 192
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v22

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 193
    .local v5, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 201
    .end local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    new-instance v20, Landroid/content/Intent;

    const-class v22, Lcom/localytics/android/PushTrackingActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 202
    .local v20, "trackingIntent":Landroid/content/Intent;
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v10

    .line 206
    .local v10, "customLaunchIntent":Landroid/content/Intent;
    if-eqz v10, :cond_1

    .line 208
    const-string v22, "ll_launch_intent"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 211
    :cond_1
    const/high16 v22, 0x8000000

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v2, v7, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 213
    .local v8, "contentIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getTitle()Ljava/lang/CharSequence;

    move-result-object v17

    .line 214
    .local v17, "optionsTitle":Ljava/lang/CharSequence;
    if-eqz v17, :cond_3

    move-object/from16 v9, v17

    .line 215
    .local v9, "contentTitle":Ljava/lang/CharSequence;
    :goto_1
    new-instance v22, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getLargeIcon()Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getOnlyAlertOnce()Z

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getPriority()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getAccentColor()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getCategory()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getVisibility()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 227
    .local v6, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    const/4 v11, 0x0

    .line 228
    .local v11, "defaults":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getLedLightColor()I

    move-result v13

    .line 229
    .local v13, "ledColor":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v13, v0, :cond_4

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getLedLightOnMillis()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/localytics/android/PushNotificationOptions;->getLedLightOffMillis()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v13, v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setLights(III)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 237
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getSoundUri()Landroid/net/Uri;

    move-result-object v19

    .line 238
    .local v19, "soundUri":Landroid/net/Uri;
    if-eqz v19, :cond_5

    .line 240
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 246
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/localytics/android/PushNotificationOptions;->getVibrationPattern()[J

    move-result-object v21

    .line 247
    .local v21, "vibrationPattern":[J
    if-eqz v21, :cond_6

    .line 249
    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setVibrate([J)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 255
    :goto_4
    invoke-virtual {v6, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 258
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "ll_public_message"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 259
    .local v18, "publicMessage":Ljava/lang/String;
    if-eqz v18, :cond_2

    .line 261
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    new-instance v23, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct/range {v23 .. v23}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 266
    :cond_2
    invoke-virtual {v6, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v22

    new-instance v23, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct/range {v23 .. v23}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 271
    const-string v22, "notification"

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/NotificationManager;

    .line 272
    .local v16, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 273
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v3    # "appIcon":I
    .end local v4    # "appName":Ljava/lang/CharSequence;
    .end local v6    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v7    # "campaignId":I
    .end local v8    # "contentIntent":Landroid/app/PendingIntent;
    .end local v9    # "contentTitle":Ljava/lang/CharSequence;
    .end local v10    # "customLaunchIntent":Landroid/content/Intent;
    .end local v11    # "defaults":I
    .end local v13    # "ledColor":I
    .end local v14    # "llObject":Lorg/json/JSONObject;
    .end local v16    # "notificationManager":Landroid/app/NotificationManager;
    .end local v17    # "optionsTitle":Ljava/lang/CharSequence;
    .end local v18    # "publicMessage":Ljava/lang/String;
    .end local v19    # "soundUri":Landroid/net/Uri;
    .end local v20    # "trackingIntent":Landroid/content/Intent;
    .end local v21    # "vibrationPattern":[J
    :goto_5
    return-void

    .line 176
    :catch_0
    move-exception v12

    .line 178
    .local v12, "e":Lorg/json/JSONException;
    const-string v22, "Failed to get campaign id from payload, ignoring message"

    invoke-static/range {v22 .. v22}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_5

    .line 195
    .end local v12    # "e":Lorg/json/JSONException;
    .restart local v2    # "appContext":Landroid/content/Context;
    .restart local v3    # "appIcon":I
    .restart local v4    # "appName":Ljava/lang/CharSequence;
    .restart local v7    # "campaignId":I
    .restart local v14    # "llObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v12

    .line 197
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v22, "Failed to get application name, icon, or launch intent"

    invoke-static/range {v22 .. v22}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v12    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v8    # "contentIntent":Landroid/app/PendingIntent;
    .restart local v10    # "customLaunchIntent":Landroid/content/Intent;
    .restart local v17    # "optionsTitle":Ljava/lang/CharSequence;
    .restart local v20    # "trackingIntent":Landroid/content/Intent;
    :cond_3
    move-object v9, v4

    .line 214
    goto/16 :goto_1

    .line 235
    .restart local v6    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .restart local v9    # "contentTitle":Ljava/lang/CharSequence;
    .restart local v11    # "defaults":I
    .restart local v13    # "ledColor":I
    :cond_4
    or-int/lit8 v11, v11, 0x4

    goto/16 :goto_2

    .line 244
    .restart local v19    # "soundUri":Landroid/net/Uri;
    :cond_5
    or-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 253
    .restart local v21    # "vibrationPattern":[J
    :cond_6
    or-int/lit8 v11, v11, 0x2

    goto :goto_4
.end method

.method _tagPushReceivedEvent(Landroid/content/Intent;)Z
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 63
    const-string v15, "ll"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 64
    .local v10, "llString":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 66
    const-string v15, "Ignoring message that isn\'t from Localytics."

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 67
    const/4 v15, 0x0

    .line 164
    :goto_0
    return v15

    .line 72
    :cond_0
    const/4 v3, 0x0

    .line 75
    .local v3, "campaignId":I
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 76
    .local v9, "llObject":Lorg/json/JSONObject;
    const-string v15, "ca"

    invoke-virtual {v9, v15}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 84
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v15

    const-string v16, "message"

    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 85
    .local v11, "message":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v15}, Lcom/localytics/android/LocalyticsDao;->isPushDisabled()Z

    move-result v12

    .line 86
    .local v12, "pushDisabled":Z
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_6

    const/4 v8, 0x1

    .line 87
    .local v8, "hasMessage":Z
    :goto_1
    if-nez v12, :cond_7

    if-eqz v8, :cond_7

    const/4 v14, 0x1

    .line 90
    .local v14, "shouldDisplay":Z
    :goto_2
    :try_start_1
    const-string v15, "cr"

    invoke-virtual {v9, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 91
    .local v5, "creativeId":Ljava/lang/String;
    const-string v15, "v"

    const-string v16, "1"

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 92
    .local v13, "serverSchemaVersion":Ljava/lang/String;
    const-string v15, "t"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "creativeType":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 95
    if-eqz v8, :cond_8

    .line 97
    const-string v6, "Alert"

    .line 105
    :cond_1
    :goto_3
    const-string v1, "Not Available"

    .line 106
    .local v1, "appContext":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v15}, Lcom/localytics/android/LocalyticsDao;->isAutoIntegrate()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 108
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v15}, Lcom/localytics/android/LocalyticsDao;->isAppInForeground()Z

    move-result v15

    if-eqz v15, :cond_9

    .line 110
    const-string v1, "Foreground"

    .line 119
    :cond_2
    :goto_4
    if-eqz v8, :cond_b

    .line 121
    if-eqz v12, :cond_a

    const-string v4, "No"

    .line 128
    .local v4, "creativeDisplayed":Ljava/lang/String;
    :goto_5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 129
    .local v2, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v15, "Campaign ID"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v15, "Creative ID"

    invoke-virtual {v2, v15, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string v15, "Creative Type"

    invoke-virtual {v2, v15, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string v15, "Creative Displayed"

    invoke-virtual {v2, v15, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string v16, "Push Notifications Enabled"

    if-eqz v12, :cond_c

    const-string v15, "No"

    :goto_6
    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const-string v15, "App Context"

    invoke-virtual {v2, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const-string v15, "Schema Version - Client"

    const/16 v16, 0x3

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v15, "Schema Version - Server"

    invoke-virtual {v2, v15, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v15, "x"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    if-nez v15, :cond_3

    .line 141
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v16, "Localytics Push Received"

    move-object/from16 v0, v16

    invoke-interface {v15, v0, v2}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v15}, Lcom/localytics/android/LocalyticsDao;->upload()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 150
    .end local v1    # "appContext":Ljava/lang/String;
    .end local v2    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "creativeDisplayed":Ljava/lang/String;
    .end local v5    # "creativeId":Ljava/lang/String;
    .end local v6    # "creativeType":Ljava/lang/String;
    .end local v13    # "serverSchemaVersion":Ljava/lang/String;
    :cond_3
    :goto_7
    if-nez v14, :cond_d

    .line 152
    if-eqz v12, :cond_4

    .line 154
    const-string v15, "Got push notification while push is disabled."

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 157
    :cond_4
    if-nez v8, :cond_5

    .line 159
    const-string v15, "Got push notification without a message."

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 161
    :cond_5
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 78
    .end local v8    # "hasMessage":Z
    .end local v9    # "llObject":Lorg/json/JSONObject;
    .end local v11    # "message":Ljava/lang/String;
    .end local v12    # "pushDisabled":Z
    .end local v14    # "shouldDisplay":Z
    :catch_0
    move-exception v7

    .line 80
    .local v7, "e":Lorg/json/JSONException;
    const-string v15, "Failed to get campaign id from payload, ignoring message"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 81
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 86
    .end local v7    # "e":Lorg/json/JSONException;
    .restart local v9    # "llObject":Lorg/json/JSONObject;
    .restart local v11    # "message":Ljava/lang/String;
    .restart local v12    # "pushDisabled":Z
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 87
    .restart local v8    # "hasMessage":Z
    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 101
    .restart local v5    # "creativeId":Ljava/lang/String;
    .restart local v6    # "creativeType":Ljava/lang/String;
    .restart local v13    # "serverSchemaVersion":Ljava/lang/String;
    .restart local v14    # "shouldDisplay":Z
    :cond_8
    :try_start_2
    const-string v6, "Silent"

    goto/16 :goto_3

    .line 114
    .restart local v1    # "appContext":Ljava/lang/String;
    :cond_9
    const-string v1, "Background"

    goto/16 :goto_4

    .line 121
    :cond_a
    const-string v4, "Yes"

    goto/16 :goto_5

    .line 125
    :cond_b
    const-string v4, "Not Applicable"

    .restart local v4    # "creativeDisplayed":Ljava/lang/String;
    goto/16 :goto_5

    .line 133
    .restart local v2    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_c
    const-string v15, "Yes"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6

    .line 145
    .end local v1    # "appContext":Ljava/lang/String;
    .end local v2    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "creativeDisplayed":Ljava/lang/String;
    .end local v5    # "creativeId":Ljava/lang/String;
    .end local v6    # "creativeType":Ljava/lang/String;
    .end local v13    # "serverSchemaVersion":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 147
    .restart local v7    # "e":Lorg/json/JSONException;
    const-string v15, "Failed to get campaign id or creative id from payload"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_7

    .line 164
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_d
    const/4 v15, 0x1

    goto/16 :goto_0
.end method

.method handlePushNotificationOpened(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 277
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    if-nez v8, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "ll"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 284
    .local v6, "llString":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 288
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 289
    .local v5, "llObject":Lorg/json/JSONObject;
    const-string v8, "ca"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 290
    .local v1, "campaignId":Ljava/lang/String;
    const-string v8, "cr"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 291
    .local v2, "creativeId":Ljava/lang/String;
    const-string v8, "v"

    const-string v9, "1"

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 293
    .local v7, "serverSchemaVersion":Ljava/lang/String;
    const-string v8, "t"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 294
    .local v3, "creativeType":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 296
    const-string v3, "Alert"

    .line 299
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 301
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 302
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v8, "Campaign ID"

    invoke-virtual {v0, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    const-string v8, "Creative ID"

    invoke-virtual {v0, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    const-string v8, "Creative Type"

    invoke-virtual {v0, v8, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    const-string v8, "Action"

    const-string v9, "Click"

    invoke-virtual {v0, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    const-string v8, "Schema Version - Client"

    const/4 v9, 0x3

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    const-string v8, "Schema Version - Server"

    invoke-virtual {v0, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v8, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v9, "Localytics Push Opened"

    invoke-interface {v8, v9, v0}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 312
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    const-string v8, "ll"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 314
    .end local v1    # "campaignId":Ljava/lang/String;
    .end local v2    # "creativeId":Ljava/lang/String;
    .end local v3    # "creativeType":Ljava/lang/String;
    .end local v5    # "llObject":Lorg/json/JSONObject;
    .end local v7    # "serverSchemaVersion":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 316
    .local v4, "e":Lorg/json/JSONException;
    const-string v8, "Failed to get campaign id or creative id from payload"

    invoke-static {v8}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_0
.end method

.method handlePushTestMode([Ljava/lang/String;)V
    .locals 6
    .param p1, "components"    # [Ljava/lang/String;

    .prologue
    .line 323
    const/4 v0, 0x2

    aget-object v3, p1, v0

    .line 324
    .local v3, "campaign":Ljava/lang/String;
    const/4 v0, 0x3

    aget-object v4, p1, v0

    .line 325
    .local v4, "creative":Ljava/lang/String;
    iget-object v0, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getCustomerIdInMemory()Ljava/lang/String;

    move-result-object v5

    .line 326
    .local v5, "customerID":Ljava/lang/String;
    iget-object v0, p0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 328
    .local v2, "appContext":Landroid/content/Context;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    new-instance v0, Lcom/localytics/android/PushManager$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/PushManager$1;-><init>(Lcom/localytics/android/PushManager;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/localytics/android/PushManager$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 414
    :cond_0
    return-void
.end method

.method setPushNotificationOptions(Lcom/localytics/android/PushNotificationOptions;)V
    .locals 0
    .param p1, "options"    # Lcom/localytics/android/PushNotificationOptions;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    if-eqz p1, :cond_0

    .line 56
    iput-object p1, p0, Lcom/localytics/android/PushManager;->mPushNotificationOptions:Lcom/localytics/android/PushNotificationOptions;

    .line 58
    :cond_0
    return-void
.end method
