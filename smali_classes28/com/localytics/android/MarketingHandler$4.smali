.class Lcom/localytics/android/MarketingHandler$4;
.super Ljava/lang/Object;
.source "MarketingHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/MarketingHandler;->handleMessageExtended(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/MarketingHandler;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/localytics/android/MarketingHandler;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/localytics/android/MarketingHandler$4;->this$0:Lcom/localytics/android/MarketingHandler;

    iput-object p2, p0, Lcom/localytics/android/MarketingHandler$4;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 344
    iget-object v1, p0, Lcom/localytics/android/MarketingHandler$4;->this$0:Lcom/localytics/android/MarketingHandler;

    iget-object v1, v1, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    iget-object v2, p0, Lcom/localytics/android/MarketingHandler$4;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/localytics/android/PushManager;->_tagPushReceivedEvent(Landroid/content/Intent;)Z

    move-result v0

    .line 345
    .local v0, "shouldDisplay":Z
    if-eqz v0, :cond_0

    .line 347
    iget-object v1, p0, Lcom/localytics/android/MarketingHandler$4;->this$0:Lcom/localytics/android/MarketingHandler;

    iget-object v1, v1, Lcom/localytics/android/MarketingHandler;->mPushManager:Lcom/localytics/android/PushManager;

    iget-object v2, p0, Lcom/localytics/android/MarketingHandler$4;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/localytics/android/PushManager;->_showPushNotification(Landroid/content/Intent;)V

    .line 349
    :cond_0
    return-void
.end method
