.class Lcom/localytics/android/InAppManager$2;
.super Ljava/lang/Object;
.source "InAppManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/InAppManager;->_tryDisplayingInAppCampaign(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/InAppManager;

.field final synthetic val$inAppMessage:Lcom/localytics/android/MarketingMessage;

.field final synthetic val$jsCallbacks:Landroid/util/SparseArray;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/MarketingMessage;Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 1426
    iput-object p1, p0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    iput-object p2, p0, Lcom/localytics/android/InAppManager$2;->val$inAppMessage:Lcom/localytics/android/MarketingMessage;

    iput-object p3, p0, Lcom/localytics/android/InAppManager$2;->val$jsCallbacks:Landroid/util/SparseArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1430
    iget-object v2, p0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v2}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1494
    :cond_0
    :goto_0
    return-void

    .line 1437
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v2}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "marketing_dialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1442
    iget-object v2, p0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mCampaignDisplaying:Z
    invoke-static {v2}, Lcom/localytics/android/InAppManager;->access$100(Lcom/localytics/android/InAppManager;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1444
    iget-object v2, p0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    const/4 v3, 0x1

    # setter for: Lcom/localytics/android/InAppManager;->mCampaignDisplaying:Z
    invoke-static {v2, v3}, Lcom/localytics/android/InAppManager;->access$102(Lcom/localytics/android/InAppManager;Z)Z

    .line 1445
    iget-object v2, p0, Lcom/localytics/android/InAppManager$2;->val$inAppMessage:Lcom/localytics/android/MarketingMessage;

    const-string v3, "campaign_id"

    invoke-virtual {v2, v3}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1446
    .local v0, "campaignId":I
    new-instance v2, Lcom/localytics/android/InAppManager$2$1;

    invoke-direct {v2, p0, v0}, Lcom/localytics/android/InAppManager$2$1;-><init>(Lcom/localytics/android/InAppManager$2;I)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/localytics/android/InAppManager$2$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1490
    .end local v0    # "campaignId":I
    :catch_0
    move-exception v1

    .line 1492
    .local v1, "e":Ljava/lang/Exception;
    const-class v2, Ljava/lang/RuntimeException;

    const-string v3, "Localytics library threw an uncaught exception"

    invoke-static {v2, v3, v1}, Lcom/localytics/android/LocalyticsManager;->throwOrLogError(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Exception;)Ljava/lang/Object;

    goto :goto_0
.end method
