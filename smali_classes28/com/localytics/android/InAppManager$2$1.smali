.class Lcom/localytics/android/InAppManager$2$1;
.super Landroid/os/AsyncTask;
.source "InAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/InAppManager$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/localytics/android/InAppManager$2;

.field final synthetic val$campaignId:I


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppManager$2;I)V
    .locals 0

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iput p2, p0, Lcom/localytics/android/InAppManager$2$1;->val$campaignId:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v0, v0, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;
    invoke-static {v0}, Lcom/localytics/android/InAppManager;->access$200(Lcom/localytics/android/InAppManager;)Lcom/localytics/android/MarketingHandler;

    move-result-object v0

    iget v1, p0, Lcom/localytics/android/InAppManager$2$1;->val$campaignId:I

    invoke-virtual {v0, v1}, Lcom/localytics/android/MarketingHandler;->setInAppAsDisplayed(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1447
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/InAppManager$2$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "readyToDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 1457
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1459
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v1}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1461
    invoke-static {}, Lcom/localytics/android/InAppDialogFragment;->newInstance()Lcom/localytics/android/InAppDialogFragment;

    move-result-object v0

    .line 1462
    .local v0, "fragment":Lcom/localytics/android/InAppDialogFragment;
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->val$inAppMessage:Lcom/localytics/android/MarketingMessage;

    invoke-virtual {v0, v1}, Lcom/localytics/android/InAppDialogFragment;->setData(Lcom/localytics/android/MarketingMessage;)Lcom/localytics/android/InAppDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v2, v2, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    invoke-virtual {v2}, Lcom/localytics/android/InAppManager;->getDialogCallbacks()Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/localytics/android/InAppDialogFragment;->setCallbacks(Landroid/util/SparseArray;)Lcom/localytics/android/InAppDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/localytics/android/JavaScriptClient;

    iget-object v3, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v3, v3, Lcom/localytics/android/InAppManager$2;->val$jsCallbacks:Landroid/util/SparseArray;

    invoke-direct {v2, v3}, Lcom/localytics/android/JavaScriptClient;-><init>(Landroid/util/SparseArray;)V

    invoke-virtual {v1, v2}, Lcom/localytics/android/InAppDialogFragment;->setJavaScriptClient(Lcom/localytics/android/JavaScriptClient;)Lcom/localytics/android/InAppDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v2, v2, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v2}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "marketing_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/localytics/android/InAppDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1467
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1469
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;
    invoke-static {v1}, Lcom/localytics/android/InAppManager;->access$200(Lcom/localytics/android/InAppManager;)Lcom/localytics/android/MarketingHandler;

    move-result-object v1

    iget-object v1, v1, Lcom/localytics/android/MarketingHandler;->listeners:Lcom/localytics/android/BaseHandler$ListenersSet;

    check-cast v1, Lcom/localytics/android/MessagingListener;

    invoke-interface {v1}, Lcom/localytics/android/MessagingListener;->localyticsWillDisplayInAppMessage()V

    .line 1476
    :cond_0
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v1}, Lcom/localytics/android/InAppManager;->access$000(Lcom/localytics/android/InAppManager;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 1485
    .end local v0    # "fragment":Lcom/localytics/android/InAppDialogFragment;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    const/4 v2, 0x0

    # setter for: Lcom/localytics/android/InAppManager;->mCampaignDisplaying:Z
    invoke-static {v1, v2}, Lcom/localytics/android/InAppManager;->access$102(Lcom/localytics/android/InAppManager;Z)Z

    .line 1486
    return-void

    .line 1482
    :cond_2
    iget-object v1, p0, Lcom/localytics/android/InAppManager$2$1;->this$1:Lcom/localytics/android/InAppManager$2;

    iget-object v1, v1, Lcom/localytics/android/InAppManager$2;->this$0:Lcom/localytics/android/InAppManager;

    # getter for: Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;
    invoke-static {v1}, Lcom/localytics/android/InAppManager;->access$200(Lcom/localytics/android/InAppManager;)Lcom/localytics/android/MarketingHandler;

    move-result-object v1

    iget v2, p0, Lcom/localytics/android/InAppManager$2$1;->val$campaignId:I

    invoke-virtual {v1, v2}, Lcom/localytics/android/MarketingHandler;->setInAppMessageAsNotDisplayed(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1447
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/InAppManager$2$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
