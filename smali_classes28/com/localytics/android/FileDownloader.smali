.class Lcom/localytics/android/FileDownloader;
.super Ljava/lang/Object;
.source "FileDownloader.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method downloadFile(Ljava/lang/String;Ljava/lang/String;ZLjava/net/Proxy;)Ljava/lang/String;
    .locals 18
    .param p1, "remoteFilePath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "localFilePath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "isOverwrite"    # Z
    .param p4, "proxy"    # Ljava/net/Proxy;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 32
    move-object/from16 v10, p2

    .line 34
    .local v10, "result":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    if-nez p3, :cond_0

    .line 37
    const-string v14, "The file %s does exist and overwrite is turned off."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 87
    .end local p2    # "localFilePath":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 41
    .restart local p2    # "localFilePath":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 42
    .local v4, "dir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-nez v14, :cond_1

    .line 44
    const-string v14, "Could not create the directory %s for saving file."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 45
    const/16 p2, 0x0

    goto :goto_0

    .line 51
    :cond_1
    :try_start_0
    new-instance v11, Ljava/io/File;

    const-string v14, "%s_temp"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p2, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v11, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .local v11, "tempFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    .line 54
    new-instance v13, Ljava/net/URL;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 55
    .local v13, "url":Ljava/net/URL;
    move-object/from16 v0, p4

    invoke-static {v13, v0}, Lcom/localytics/android/BaseUploadThread;->createURLConnection(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v12

    .line 60
    .local v12, "ucon":Ljava/net/URLConnection;
    const/16 v1, 0x2000

    .line 61
    .local v1, "BUF_SIZE":I
    invoke-virtual {v12}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 62
    .local v8, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v14, 0x4000

    invoke-direct {v2, v8, v14}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 63
    .local v2, "bis":Ljava/io/BufferedInputStream;
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 66
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/16 v14, 0x2000

    new-array v3, v14, [B

    .line 68
    .local v3, "buffer":[B
    :goto_1
    invoke-virtual {v2, v3}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v9

    .local v9, "read":I
    const/4 v14, -0x1

    if-eq v9, v14, :cond_3

    .line 70
    const/4 v14, 0x0

    invoke-virtual {v7, v3, v14, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 81
    .end local v1    # "BUF_SIZE":I
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "buffer":[B
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "read":I
    .end local v11    # "tempFile":Ljava/io/File;
    .end local v12    # "ucon":Ljava/net/URLConnection;
    .end local v13    # "url":Ljava/net/URL;
    :catch_0
    move-exception v5

    .line 83
    .local v5, "e":Ljava/io/IOException;
    const-string v14, "Failed to download In-app campaign"

    invoke-static {v14, v5}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    const/4 v10, 0x0

    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    move-object/from16 p2, v10

    .line 87
    goto :goto_0

    .line 72
    .restart local v1    # "BUF_SIZE":I
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "buffer":[B
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "is":Ljava/io/InputStream;
    .restart local v9    # "read":I
    .restart local v11    # "tempFile":Ljava/io/File;
    .restart local v12    # "ucon":Ljava/net/URLConnection;
    .restart local v13    # "url":Ljava/net/URL;
    :cond_3
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 74
    invoke-virtual {v11, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 76
    const-string v14, "Failed to create permanent file for In-app campaign"

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 77
    invoke-virtual {v11}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 78
    const/16 p2, 0x0

    goto/16 :goto_0
.end method
