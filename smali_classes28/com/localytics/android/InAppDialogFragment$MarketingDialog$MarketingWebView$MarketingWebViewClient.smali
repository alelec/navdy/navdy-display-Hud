.class final Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "InAppDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MarketingWebViewClient"
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field final synthetic this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;Landroid/app/Activity;)V
    .locals 0
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1127
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 1128
    iput-object p2, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->mActivity:Landroid/app/Activity;

    .line 1129
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 11
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    .line 1141
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # getter for: Lcom/localytics/android/InAppDialogFragment;->mMarketingMessage:Lcom/localytics/android/MarketingMessage;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment;->access$200(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/MarketingMessage;

    move-result-object v7

    const-string v8, "location"

    invoke-virtual {v7, v8}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1142
    .local v0, "location":Ljava/lang/String;
    const-string v7, "center"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/high16 v7, 0x41200000    # 10.0f

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v7, v7

    shl-int/lit8 v1, v7, 0x1

    .line 1143
    .local v1, "margin":I
    :goto_0
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    sub-int v3, v7, v1

    .line 1144
    .local v3, "maxWidth":I
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    sub-int v2, v7, v1

    .line 1145
    .local v2, "maxHeight":I
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mWidth:F
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1300(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)F

    move-result v7

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    div-float v5, v7, v8

    .line 1146
    .local v5, "viewportWidth":F
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mHeight:F
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1400(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)F

    move-result v7

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v7, v7

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$1200(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    div-float v4, v7, v8

    .line 1148
    .local v4, "viewportHeight":F
    const-string v7, "javascript:(function() {  var viewportNode = document.createElement(\'meta\');  viewportNode.name    = \'viewport\';  viewportNode.content = \'width=%f, height=%f, user-scalable=no, minimum-scale=.25, maximum-scale=1\';  viewportNode.id      = \'metatag\';  document.getElementsByTagName(\'head\')[0].appendChild(viewportNode);})()"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v6

    const/4 v9, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 1157
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # getter for: Lcom/localytics/android/InAppDialogFragment;->mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment;->access$1100(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/JavaScriptClient;

    move-result-object v7

    invoke-virtual {v7}, Lcom/localytics/android/JavaScriptClient;->getJsGlueCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 1160
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    # getter for: Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->mRootLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->access$700(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1161
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    iget-object v7, v7, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # getter for: Lcom/localytics/android/InAppDialogFragment;->mEnterAnimatable:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v7}, Lcom/localytics/android/InAppDialogFragment;->access$1500(Lcom/localytics/android/InAppDialogFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1163
    iget-object v6, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v6, v6, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    invoke-virtual {v6}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->enterWithAnimation()V

    .line 1165
    :cond_0
    return-void

    .end local v1    # "margin":I
    .end local v2    # "maxHeight":I
    .end local v3    # "maxWidth":I
    .end local v4    # "viewportHeight":F
    .end local v5    # "viewportWidth":F
    :cond_1
    move v1, v6

    .line 1142
    goto/16 :goto_0
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 8
    .param p1, "view"    # Landroid/webkit/WebView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "urlString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1173
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1174
    .local v3, "url":Ljava/net/URL;
    iget-object v4, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v4, v4, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    iget-object v4, v4, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # invokes: Lcom/localytics/android/InAppDialogFragment;->handleFileProtocolRequest(Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    invoke-static {v4, v3}, Lcom/localytics/android/InAppDialogFragment;->access$1600(Lcom/localytics/android/InAppDialogFragment;Ljava/net/URL;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v0

    .line 1175
    .local v0, "actionForRequest":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    sget-object v4, Lcom/localytics/android/InAppDialogFragment$3;->$SwitchMap$com$localytics$android$InAppDialogFragment$ProtocolHandleAction:[I

    invoke-virtual {v0}, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->ordinal()I

    move-result v5

    aget v4, v4, v5
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v4, :pswitch_data_0

    .line 1181
    const/4 v2, 0x1

    .line 1189
    .end local v0    # "actionForRequest":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .end local v3    # "url":Ljava/net/URL;
    .local v2, "intercept":Z
    :goto_0
    if-eqz v2, :cond_0

    new-instance v4, Landroid/webkit/WebResourceResponse;

    const-string v5, "text/plain"

    const-string v6, "UTF-8"

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    :goto_1
    return-object v4

    .line 1178
    .end local v2    # "intercept":Z
    .restart local v0    # "actionForRequest":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .restart local v3    # "url":Ljava/net/URL;
    :pswitch_0
    const/4 v2, 0x0

    .line 1179
    .restart local v2    # "intercept":Z
    goto :goto_0

    .line 1184
    .end local v0    # "actionForRequest":Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;
    .end local v2    # "intercept":Z
    .end local v3    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 1186
    .local v1, "e":Ljava/net/MalformedURLException;
    const/4 v2, 0x1

    .restart local v2    # "intercept":Z
    goto :goto_0

    .line 1189
    .end local v1    # "e":Ljava/net/MalformedURLException;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v4

    goto :goto_1

    .line 1175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->this$2:Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;

    iget-object v0, v0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    iget-object v0, v0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    iget-object v1, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p2, v1}, Lcom/localytics/android/InAppDialogFragment;->handleUrl(Ljava/lang/String;Landroid/app/Activity;)Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    move-result-object v0

    sget-object v1, Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;->OPENING_INTERNAL:Lcom/localytics/android/InAppDialogFragment$ProtocolHandleAction;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
