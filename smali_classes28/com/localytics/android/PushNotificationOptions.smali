.class public Lcom/localytics/android/PushNotificationOptions;
.super Ljava/lang/Object;
.source "PushNotificationOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/PushNotificationOptions$1;,
        Lcom/localytics/android/PushNotificationOptions$Builder;
    }
.end annotation


# instance fields
.field private final mAccentColor:I

.field private final mCategory:Ljava/lang/String;

.field private final mLargeIcon:Landroid/graphics/Bitmap;

.field private final mLaunchIntent:Landroid/content/Intent;

.field private final mLedLightColor:I

.field private final mLedLightOffMillis:I

.field private final mLedLightOnMillis:I

.field private final mOnlyAlertOnce:Z

.field private final mPriority:I

.field private final mSmallIcon:I

.field private final mSoundUri:Landroid/net/Uri;

.field private final mTitle:Ljava/lang/CharSequence;

.field private final mVibrationPattern:[J

.field private final mVisibility:I


# direct methods
.method private constructor <init>(Lcom/localytics/android/PushNotificationOptions$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/localytics/android/PushNotificationOptions$Builder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mAccentColor:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$000(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mAccentColor:I

    .line 33
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mCategory:Ljava/lang/String;
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$100(Lcom/localytics/android/PushNotificationOptions$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mCategory:Ljava/lang/String;

    .line 34
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mSoundUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$200(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mSoundUri:Landroid/net/Uri;

    .line 35
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mVisibility:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$300(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mVisibility:I

    .line 36
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mPriority:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$400(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mPriority:I

    .line 37
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mSmallIcon:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$500(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mSmallIcon:I

    .line 38
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mLargeIcon:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$600(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLargeIcon:Landroid/graphics/Bitmap;

    .line 39
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightColor:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$700(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightColor:I

    .line 40
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOnMillis:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$800(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightOnMillis:I

    .line 41
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mLedLightOffMillis:I
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$900(Lcom/localytics/android/PushNotificationOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightOffMillis:I

    .line 42
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mVibrationPattern:[J
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$1000(Lcom/localytics/android/PushNotificationOptions$Builder;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mVibrationPattern:[J

    .line 43
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mOnlyAlertOnce:Z
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$1100(Lcom/localytics/android/PushNotificationOptions$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/localytics/android/PushNotificationOptions;->mOnlyAlertOnce:Z

    .line 44
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mLaunchIntent:Landroid/content/Intent;
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$1200(Lcom/localytics/android/PushNotificationOptions$Builder;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLaunchIntent:Landroid/content/Intent;

    .line 45
    # getter for: Lcom/localytics/android/PushNotificationOptions$Builder;->mTitle:Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/localytics/android/PushNotificationOptions$Builder;->access$1300(Lcom/localytics/android/PushNotificationOptions$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mTitle:Ljava/lang/CharSequence;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Lcom/localytics/android/PushNotificationOptions$Builder;Lcom/localytics/android/PushNotificationOptions$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/localytics/android/PushNotificationOptions$Builder;
    .param p2, "x1"    # Lcom/localytics/android/PushNotificationOptions$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/localytics/android/PushNotificationOptions;-><init>(Lcom/localytics/android/PushNotificationOptions$Builder;)V

    return-void
.end method


# virtual methods
.method public getAccentColor()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mAccentColor:I

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getLargeIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLargeIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLaunchIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLaunchIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getLedLightColor()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightColor:I

    return v0
.end method

.method public getLedLightOffMillis()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightOffMillis:I

    return v0
.end method

.method public getLedLightOnMillis()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mLedLightOnMillis:I

    return v0
.end method

.method public getOnlyAlertOnce()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/localytics/android/PushNotificationOptions;->mOnlyAlertOnce:Z

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mPriority:I

    return v0
.end method

.method public getSmallIcon()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mSmallIcon:I

    return v0
.end method

.method public getSoundUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mSoundUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getVibrationPattern()[J
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/localytics/android/PushNotificationOptions;->mVibrationPattern:[J

    return-object v0
.end method

.method public getVisibility()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/localytics/android/PushNotificationOptions;->mVisibility:I

    return v0
.end method
