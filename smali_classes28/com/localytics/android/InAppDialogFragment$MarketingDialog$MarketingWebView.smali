.class final Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;
.super Landroid/webkit/WebView;
.source "InAppDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/InAppDialogFragment$MarketingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MarketingWebView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppDialogFragment$MarketingDialog;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1096
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->this$1:Lcom/localytics/android/InAppDialogFragment$MarketingDialog;

    .line 1097
    invoke-direct {p0, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1100
    invoke-static {}, Lcom/localytics/android/DatapointHelper;->getApiLevel()I

    move-result v2

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    .line 1102
    const/4 v2, 0x0

    invoke-virtual {p0, v4, v2}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1105
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1106
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1107
    invoke-virtual {p0, v0}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1109
    invoke-virtual {p0, v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setBackgroundColor(I)V

    .line 1110
    invoke-virtual {p0, v4}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setInitialScale(I)V

    .line 1111
    invoke-virtual {p0, v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 1112
    invoke-virtual {p0, v5}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setVerticalScrollBarEnabled(Z)V

    .line 1114
    new-instance v2, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;

    iget-object v3, p1, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-virtual {v3}, Lcom/localytics/android/InAppDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView$MarketingWebViewClient;-><init>(Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;Landroid/app/Activity;)V

    invoke-virtual {p0, v2}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1116
    invoke-virtual {p0}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 1117
    .local v1, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 1118
    iget-object v2, p1, Lcom/localytics/android/InAppDialogFragment$MarketingDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    # getter for: Lcom/localytics/android/InAppDialogFragment;->mJavaScriptClient:Lcom/localytics/android/JavaScriptClient;
    invoke-static {v2}, Lcom/localytics/android/InAppDialogFragment;->access$1100(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/JavaScriptClient;

    move-result-object v2

    const-string v3, "localytics"

    invoke-virtual {p0, v2, v3}, Lcom/localytics/android/InAppDialogFragment$MarketingDialog$MarketingWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1119
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 1120
    return-void
.end method
