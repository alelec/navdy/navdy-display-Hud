.class Lflow/Flow$3;
.super Lflow/Flow$Transition;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->replaceTo(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;

.field final synthetic val$screen:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lflow/Flow;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 116
    iput-object p1, p0, Lflow/Flow$3;->this$0:Lflow/Flow;

    iput-object p2, p0, Lflow/Flow$3;->val$screen:Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflow/Flow$Transition;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 3

    .prologue
    .line 118
    iget-object v1, p0, Lflow/Flow$3;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v1}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v1

    iget-object v2, p0, Lflow/Flow$3;->val$screen:Ljava/lang/Object;

    invoke-static {v2}, Lflow/Backstack;->fromUpChain(Ljava/lang/Object;)Lflow/Backstack;

    move-result-object v2

    # invokes: Lflow/Flow;->preserveEquivalentPrefix(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;
    invoke-static {v1, v2}, Lflow/Flow;->access$200(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;

    move-result-object v0

    .line 119
    .local v0, "newBackstack":Lflow/Backstack;
    sget-object v1, Lflow/Flow$Direction;->REPLACE:Lflow/Flow$Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow$3;->go(Lflow/Backstack;Lflow/Flow$Direction;)V

    .line 120
    return-void
.end method
