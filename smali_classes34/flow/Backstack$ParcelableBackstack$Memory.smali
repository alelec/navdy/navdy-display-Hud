.class public Lflow/Backstack$ParcelableBackstack$Memory;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Lflow/Backstack$ParcelableBackstack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack$ParcelableBackstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Memory"
.end annotation


# instance fields
.field private final backstack:Lflow/Backstack;

.field private final parcer:Lflow/Parcer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Backstack;Lflow/Parcer;)V
    .locals 0
    .param p1, "backstack"    # Lflow/Backstack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Backstack;",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p2, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p1, p0, Lflow/Backstack$ParcelableBackstack$Memory;->backstack:Lflow/Backstack;

    .line 215
    iput-object p2, p0, Lflow/Backstack$ParcelableBackstack$Memory;->parcer:Lflow/Parcer;

    .line 216
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public getBackstack(Lflow/Parcer;)Lflow/Backstack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack;"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    iget-object v0, p0, Lflow/Backstack$ParcelableBackstack$Memory;->backstack:Lflow/Backstack;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 8
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lflow/Backstack$ParcelableEntry;>;"
    iget-object v3, p0, Lflow/Backstack$ParcelableBackstack$Memory;->backstack:Lflow/Backstack;

    invoke-virtual {v3}, Lflow/Backstack;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Backstack$Entry;

    .line 229
    .local v1, "entry":Lflow/Backstack$Entry;
    new-instance v3, Lflow/Backstack$ParcelableEntry;

    # getter for: Lflow/Backstack$Entry;->id:J
    invoke-static {v1}, Lflow/Backstack$Entry;->access$300(Lflow/Backstack$Entry;)J

    move-result-wide v4

    iget-object v6, p0, Lflow/Backstack$ParcelableBackstack$Memory;->parcer:Lflow/Parcer;

    invoke-virtual {v1}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Lflow/Parcer;->wrap(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lflow/Backstack$ParcelableEntry;-><init>(JLandroid/os/Parcelable;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    .end local v1    # "entry":Lflow/Backstack$Entry;
    :cond_0
    iget-object v3, p0, Lflow/Backstack$ParcelableBackstack$Memory;->backstack:Lflow/Backstack;

    # getter for: Lflow/Backstack;->highestId:J
    invoke-static {v3}, Lflow/Backstack;->access$400(Lflow/Backstack;)J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 233
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 234
    return-void
.end method
