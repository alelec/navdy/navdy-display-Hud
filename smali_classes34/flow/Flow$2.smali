.class Lflow/Flow$2;
.super Lflow/Flow$Transition;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->resetTo(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;

.field final synthetic val$screen:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lflow/Flow;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    iput-object p2, p0, Lflow/Flow$2;->val$screen:Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflow/Flow$Transition;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 9

    .prologue
    .line 79
    iget-object v7, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v7}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v7

    invoke-virtual {v7}, Lflow/Backstack;->buildUpon()Lflow/Backstack$Builder;

    move-result-object v0

    .line 80
    .local v0, "builder":Lflow/Backstack$Builder;
    const/4 v1, 0x0

    .line 84
    .local v1, "count":I
    const/4 v5, 0x0

    .line 85
    .local v5, "lastPopped":Ljava/lang/Object;
    iget-object v7, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v7}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v7

    invoke-virtual {v7}, Lflow/Backstack;->reverseIterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lflow/Backstack$Entry;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 86
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/Backstack$Entry;

    .line 88
    .local v2, "entry":Lflow/Backstack$Entry;
    invoke-virtual {v2}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, Lflow/Flow$2;->val$screen:Ljava/lang/Object;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 90
    const/4 v3, 0x0

    .end local v5    # "lastPopped":Ljava/lang/Object;
    .local v3, "i":I
    :goto_1
    iget-object v7, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v7}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v7

    invoke-virtual {v7}, Lflow/Backstack;->size()I

    move-result v7

    sub-int/2addr v7, v1

    if-ge v3, v7, :cond_1

    .line 91
    invoke-virtual {v0}, Lflow/Backstack$Builder;->pop()Lflow/Backstack$Entry;

    move-result-object v7

    invoke-virtual {v7}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    .line 90
    .restart local v5    # "lastPopped":Ljava/lang/Object;
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 95
    .end local v3    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 97
    goto :goto_0

    .line 100
    .end local v2    # "entry":Lflow/Backstack$Entry;
    .end local v5    # "lastPopped":Ljava/lang/Object;
    :cond_1
    if-eqz v5, :cond_2

    .line 101
    invoke-virtual {v0, v5}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    .line 102
    invoke-virtual {v0}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v6

    .line 103
    .local v6, "newBackstack":Lflow/Backstack;
    sget-object v7, Lflow/Flow$Direction;->BACKWARD:Lflow/Flow$Direction;

    invoke-virtual {p0, v6, v7}, Lflow/Flow$2;->go(Lflow/Backstack;Lflow/Flow$Direction;)V

    .line 109
    :goto_2
    return-void

    .line 105
    .end local v6    # "newBackstack":Lflow/Backstack;
    :cond_2
    iget-object v7, p0, Lflow/Flow$2;->val$screen:Ljava/lang/Object;

    invoke-virtual {v0, v7}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    .line 106
    invoke-virtual {v0}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v6

    .line 107
    .restart local v6    # "newBackstack":Lflow/Backstack;
    sget-object v7, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    invoke-virtual {p0, v6, v7}, Lflow/Flow$2;->go(Lflow/Backstack;Lflow/Flow$Direction;)V

    goto :goto_2
.end method
