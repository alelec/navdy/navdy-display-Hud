.class final Lflow/Backstack$ParcelableBackstack$1;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack$ParcelableBackstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lflow/Backstack$ParcelableBackstack;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lflow/Backstack$ParcelableBackstack;
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lflow/Backstack$ParcelableEntry;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 200
    .local v2, "highestId":J
    sget-object v1, Lflow/Backstack$ParcelableEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 201
    new-instance v1, Lflow/Backstack$ParcelableBackstack$Parcelled;

    invoke-direct {v1, v2, v3, v0}, Lflow/Backstack$ParcelableBackstack$Parcelled;-><init>(JLjava/util/List;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lflow/Backstack$ParcelableBackstack$1;->createFromParcel(Landroid/os/Parcel;)Lflow/Backstack$ParcelableBackstack;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lflow/Backstack$ParcelableBackstack;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 205
    new-array v0, p1, [Lflow/Backstack$ParcelableBackstack$Parcelled;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lflow/Backstack$ParcelableBackstack$1;->newArray(I)[Lflow/Backstack$ParcelableBackstack;

    move-result-object v0

    return-object v0
.end method
