.class Lflow/Flow$5;
.super Lflow/Flow$Transition;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->goBack()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 1

    .prologue
    .line 155
    iput-object p1, p0, Lflow/Flow$5;->this$0:Lflow/Flow;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflow/Flow$Transition;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 4

    .prologue
    .line 157
    iget-object v2, p0, Lflow/Flow$5;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v2}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v2

    invoke-virtual {v2}, Lflow/Backstack;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 159
    invoke-virtual {p0}, Lflow/Flow$5;->onComplete()V

    .line 166
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v2, p0, Lflow/Flow$5;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v2}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v2

    invoke-virtual {v2}, Lflow/Backstack;->buildUpon()Lflow/Backstack$Builder;

    move-result-object v0

    .line 162
    .local v0, "builder":Lflow/Backstack$Builder;
    invoke-virtual {v0}, Lflow/Backstack$Builder;->pop()Lflow/Backstack$Entry;

    .line 163
    invoke-virtual {v0}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v1

    .line 164
    .local v1, "newBackstack":Lflow/Backstack;
    sget-object v2, Lflow/Flow$Direction;->BACKWARD:Lflow/Flow$Direction;

    invoke-virtual {p0, v1, v2}, Lflow/Flow$5;->go(Lflow/Backstack;Lflow/Flow$Direction;)V

    goto :goto_0
.end method
