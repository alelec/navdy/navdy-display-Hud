.class public final Lflow/Backstack;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/Backstack$1;,
        Lflow/Backstack$ParcelableEntry;,
        Lflow/Backstack$ParcelableBackstack;,
        Lflow/Backstack$ReadIterator;,
        Lflow/Backstack$Builder;,
        Lflow/Backstack$Entry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lflow/Backstack$Entry;",
        ">;"
    }
.end annotation


# instance fields
.field private final backstack:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lflow/Backstack$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final highestId:J


# direct methods
.method private constructor <init>(JLjava/util/Deque;)V
    .locals 1
    .param p1, "highestId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Deque",
            "<",
            "Lflow/Backstack$Entry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p3, "backstack":Ljava/util/Deque;, "Ljava/util/Deque<Lflow/Backstack$Entry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-wide p1, p0, Lflow/Backstack;->highestId:J

    .line 55
    iput-object p3, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    .line 56
    return-void
.end method

.method synthetic constructor <init>(JLjava/util/Deque;Lflow/Backstack$1;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # Ljava/util/Deque;
    .param p4, "x2"    # Lflow/Backstack$1;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lflow/Backstack;-><init>(JLjava/util/Deque;)V

    return-void
.end method

.method static synthetic access$400(Lflow/Backstack;)J
    .locals 2
    .param p0, "x0"    # Lflow/Backstack;

    .prologue
    .line 34
    iget-wide v0, p0, Lflow/Backstack;->highestId:J

    return-wide v0
.end method

.method public static emptyBuilder()Lflow/Backstack$Builder;
    .locals 5

    .prologue
    .line 45
    new-instance v0, Lflow/Backstack$Builder;

    const-wide/16 v2, -0x1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v1, v4}, Lflow/Backstack$Builder;-><init>(JLjava/util/Collection;Lflow/Backstack$1;)V

    return-object v0
.end method

.method public static from(Landroid/os/Parcelable;Lflow/Parcer;)Lflow/Backstack;
    .locals 2
    .param p0, "parcelable"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack;"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    move-object v0, p0

    check-cast v0, Lflow/Backstack$ParcelableBackstack;

    .line 41
    .local v0, "backstack":Lflow/Backstack$ParcelableBackstack;
    invoke-interface {v0, p1}, Lflow/Backstack$ParcelableBackstack;->getBackstack(Lflow/Parcer;)Lflow/Backstack;

    move-result-object v1

    return-object v1
.end method

.method public static fromUpChain(Ljava/lang/Object;)Lflow/Backstack;
    .locals 4
    .param p0, "screen"    # Ljava/lang/Object;

    .prologue
    .line 89
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 91
    .local v2, "newBackstack":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Object;>;"
    move-object v1, p0

    .line 92
    .local v1, "current":Ljava/lang/Object;
    :goto_0
    instance-of v3, v1, Lflow/HasParent;

    if-eqz v3, :cond_0

    .line 93
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 94
    check-cast v1, Lflow/HasParent;

    .end local v1    # "current":Ljava/lang/Object;
    invoke-interface {v1}, Lflow/HasParent;->getParent()Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "current":Ljava/lang/Object;
    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 98
    invoke-static {}, Lflow/Backstack;->emptyBuilder()Lflow/Backstack$Builder;

    move-result-object v0

    .line 99
    .local v0, "builder":Lflow/Backstack$Builder;
    invoke-virtual {v0, v2}, Lflow/Backstack$Builder;->addAll(Ljava/util/Collection;)Lflow/Backstack$Builder;

    .line 100
    invoke-virtual {v0}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v3

    return-object v3
.end method

.method public static single(Ljava/lang/Object;)Lflow/Backstack;
    .locals 1
    .param p0, "screen"    # Ljava/lang/Object;

    .prologue
    .line 50
    invoke-static {}, Lflow/Backstack;->emptyBuilder()Lflow/Backstack$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lflow/Backstack$Builder;
    .locals 5

    .prologue
    .line 81
    new-instance v0, Lflow/Backstack$Builder;

    iget-wide v2, p0, Lflow/Backstack;->highestId:J

    iget-object v1, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v1, v4}, Lflow/Backstack$Builder;-><init>(JLjava/util/Collection;Lflow/Backstack$1;)V

    return-object v0
.end method

.method public current()Lflow/Backstack$Entry;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Backstack$Entry;

    return-object v0
.end method

.method public getParcelable(Lflow/Parcer;)Landroid/os/Parcelable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/os/Parcelable;"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    new-instance v0, Lflow/Backstack$ParcelableBackstack$Memory;

    invoke-direct {v0, p0, p1}, Lflow/Backstack$ParcelableBackstack$Memory;-><init>(Lflow/Backstack;Lflow/Parcer;)V

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lflow/Backstack$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lflow/Backstack$ReadIterator;

    iget-object v1, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lflow/Backstack$ReadIterator;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method public reverseIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lflow/Backstack$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lflow/Backstack$ReadIterator;

    iget-object v1, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lflow/Backstack$ReadIterator;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflow/Backstack;->backstack:Ljava/util/Deque;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
