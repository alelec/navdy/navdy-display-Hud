.class interface abstract Lflow/Backstack$ParcelableBackstack;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ParcelableBackstack"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/Backstack$ParcelableBackstack$Parcelled;,
        Lflow/Backstack$ParcelableBackstack$Memory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lflow/Backstack$ParcelableBackstack;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lflow/Backstack$ParcelableBackstack$1;

    invoke-direct {v0}, Lflow/Backstack$ParcelableBackstack$1;-><init>()V

    sput-object v0, Lflow/Backstack$ParcelableBackstack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public abstract getBackstack(Lflow/Parcer;)Lflow/Backstack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack;"
        }
    .end annotation
.end method
