.class final Lflow/Backstack$ParcelableEntry$1;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack$ParcelableEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lflow/Backstack$ParcelableEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lflow/Backstack$ParcelableEntry;
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 291
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 292
    .local v0, "id":J
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    .line 293
    .local v2, "parcelable":Landroid/os/Parcelable;
    new-instance v3, Lflow/Backstack$ParcelableEntry;

    invoke-direct {v3, v0, v1, v2}, Lflow/Backstack$ParcelableEntry;-><init>(JLandroid/os/Parcelable;)V

    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lflow/Backstack$ParcelableEntry$1;->createFromParcel(Landroid/os/Parcel;)Lflow/Backstack$ParcelableEntry;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lflow/Backstack$ParcelableEntry;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 297
    new-array v0, p1, [Lflow/Backstack$ParcelableEntry;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lflow/Backstack$ParcelableEntry$1;->newArray(I)[Lflow/Backstack$ParcelableEntry;

    move-result-object v0

    return-object v0
.end method
