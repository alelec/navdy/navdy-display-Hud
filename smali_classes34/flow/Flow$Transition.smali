.class abstract Lflow/Flow$Transition;
.super Ljava/lang/Object;
.source "Flow.java"

# interfaces
.implements Lflow/Flow$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Transition"
.end annotation


# instance fields
.field finished:Z

.field next:Lflow/Flow$Transition;

.field nextBackstack:Lflow/Backstack;

.field final synthetic this$0:Lflow/Flow;


# direct methods
.method private constructor <init>(Lflow/Flow;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lflow/Flow$Transition;->this$0:Lflow/Flow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflow/Flow;Lflow/Flow$1;)V
    .locals 0
    .param p1, "x0"    # Lflow/Flow;
    .param p2, "x1"    # Lflow/Flow$1;

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lflow/Flow$Transition;-><init>(Lflow/Flow;)V

    return-void
.end method


# virtual methods
.method enqueue(Lflow/Flow$Transition;)V
    .locals 1
    .param p1, "transition"    # Lflow/Flow$Transition;

    .prologue
    .line 232
    iget-object v0, p0, Lflow/Flow$Transition;->next:Lflow/Flow$Transition;

    if-nez v0, :cond_0

    .line 233
    iput-object p1, p0, Lflow/Flow$Transition;->next:Lflow/Flow$Transition;

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lflow/Flow$Transition;->next:Lflow/Flow$Transition;

    invoke-virtual {v0, p1}, Lflow/Flow$Transition;->enqueue(Lflow/Flow$Transition;)V

    goto :goto_0
.end method

.method protected abstract execute()V
.end method

.method protected go(Lflow/Backstack;Lflow/Flow$Direction;)V
    .locals 1
    .param p1, "nextBackstack"    # Lflow/Backstack;
    .param p2, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 254
    iput-object p1, p0, Lflow/Flow$Transition;->nextBackstack:Lflow/Backstack;

    .line 255
    iget-object v0, p0, Lflow/Flow$Transition;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->listener:Lflow/Flow$Listener;
    invoke-static {v0}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Flow$Listener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p0}, Lflow/Flow$Listener;->go(Lflow/Backstack;Lflow/Flow$Direction;Lflow/Flow$Callback;)V

    .line 256
    return-void
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 240
    iget-boolean v0, p0, Lflow/Flow$Transition;->finished:Z

    if-eqz v0, :cond_0

    .line 241
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onComplete already called for this transition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_0
    iget-object v0, p0, Lflow/Flow$Transition;->nextBackstack:Lflow/Backstack;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lflow/Flow$Transition;->this$0:Lflow/Flow;

    iget-object v1, p0, Lflow/Flow$Transition;->nextBackstack:Lflow/Backstack;

    # setter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v0, v1}, Lflow/Flow;->access$102(Lflow/Flow;Lflow/Backstack;)Lflow/Backstack;

    .line 246
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflow/Flow$Transition;->finished:Z

    .line 247
    iget-object v0, p0, Lflow/Flow$Transition;->next:Lflow/Flow$Transition;

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lflow/Flow$Transition;->this$0:Lflow/Flow;

    iget-object v1, p0, Lflow/Flow$Transition;->next:Lflow/Flow$Transition;

    # setter for: Lflow/Flow;->transition:Lflow/Flow$Transition;
    invoke-static {v0, v1}, Lflow/Flow;->access$302(Lflow/Flow;Lflow/Flow$Transition;)Lflow/Flow$Transition;

    .line 249
    iget-object v0, p0, Lflow/Flow$Transition;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->transition:Lflow/Flow$Transition;
    invoke-static {v0}, Lflow/Flow;->access$300(Lflow/Flow;)Lflow/Flow$Transition;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow$Transition;->execute()V

    .line 251
    :cond_2
    return-void
.end method
