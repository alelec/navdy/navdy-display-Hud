.class public final Lflow/Backstack$Entry;
.super Ljava/lang/Object;
.source "Backstack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Entry"
.end annotation


# instance fields
.field private final id:J

.field private final screen:Ljava/lang/Object;


# direct methods
.method private constructor <init>(JLjava/lang/Object;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "screen"    # Ljava/lang/Object;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-wide p1, p0, Lflow/Backstack$Entry;->id:J

    .line 109
    iput-object p3, p0, Lflow/Backstack$Entry;->screen:Ljava/lang/Object;

    .line 110
    return-void
.end method

.method synthetic constructor <init>(JLjava/lang/Object;Lflow/Backstack$1;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # Ljava/lang/Object;
    .param p4, "x2"    # Lflow/Backstack$1;

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lflow/Backstack$Entry;-><init>(JLjava/lang/Object;)V

    return-void
.end method

.method static synthetic access$300(Lflow/Backstack$Entry;)J
    .locals 2
    .param p0, "x0"    # Lflow/Backstack$Entry;

    .prologue
    .line 103
    iget-wide v0, p0, Lflow/Backstack$Entry;->id:J

    return-wide v0
.end method


# virtual methods
.method public getId()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lflow/Backstack$Entry;->id:J

    return-wide v0
.end method

.method public getScreen()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lflow/Backstack$Entry;->screen:Ljava/lang/Object;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lflow/Backstack$Entry;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflow/Backstack$Entry;->screen:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
