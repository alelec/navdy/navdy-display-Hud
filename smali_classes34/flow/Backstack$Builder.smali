.class public final Lflow/Backstack$Builder;
.super Ljava/lang/Object;
.source "Backstack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final backstack:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lflow/Backstack$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private highestId:J


# direct methods
.method private constructor <init>(JLjava/util/Collection;)V
    .locals 1
    .param p1, "highestId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Lflow/Backstack$Entry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p3, "backstack":Ljava/util/Collection;, "Ljava/util/Collection<Lflow/Backstack$Entry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-wide p1, p0, Lflow/Backstack$Builder;->highestId:J

    .line 131
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, p3}, Ljava/util/ArrayDeque;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    .line 132
    return-void
.end method

.method synthetic constructor <init>(JLjava/util/Collection;Lflow/Backstack$1;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # Ljava/util/Collection;
    .param p4, "x2"    # Lflow/Backstack$1;

    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p3}, Lflow/Backstack$Builder;-><init>(JLjava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)Lflow/Backstack$Builder;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack$Builder;"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 142
    .local v1, "screen":Ljava/lang/Object;
    iget-object v2, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    new-instance v3, Lflow/Backstack$Entry;

    iget-wide v4, p0, Lflow/Backstack$Builder;->highestId:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lflow/Backstack$Builder;->highestId:J

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v1, v6}, Lflow/Backstack$Entry;-><init>(JLjava/lang/Object;Lflow/Backstack$1;)V

    invoke-interface {v2, v3}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    .end local v1    # "screen":Ljava/lang/Object;
    :cond_0
    return-object p0
.end method

.method public build()Lflow/Backstack;
    .locals 5

    .prologue
    .line 163
    iget-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Backstack may not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    new-instance v0, Lflow/Backstack;

    iget-wide v2, p0, Lflow/Backstack$Builder;->highestId:J

    iget-object v1, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v1, v4}, Lflow/Backstack;-><init>(JLjava/util/Deque;Lflow/Backstack$1;)V

    return-object v0
.end method

.method public clear()Lflow/Backstack$Builder;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 159
    return-object p0
.end method

.method public peek()Lflow/Backstack$Entry;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Backstack$Entry;

    return-object v0
.end method

.method public pop()Lflow/Backstack$Entry;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Backstack$Entry;

    return-object v0
.end method

.method public push(Ljava/lang/Object;)Lflow/Backstack$Builder;
    .locals 6
    .param p1, "screen"    # Ljava/lang/Object;

    .prologue
    .line 135
    iget-object v0, p0, Lflow/Backstack$Builder;->backstack:Ljava/util/Deque;

    new-instance v1, Lflow/Backstack$Entry;

    iget-wide v2, p0, Lflow/Backstack$Builder;->highestId:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lflow/Backstack$Builder;->highestId:J

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p1, v4}, Lflow/Backstack$Entry;-><init>(JLjava/lang/Object;Lflow/Backstack$1;)V

    invoke-interface {v0, v1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 137
    return-object p0
.end method
