.class Lflow/Backstack$ParcelableEntry;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParcelableEntry"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lflow/Backstack$ParcelableEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:J

.field private final parcelable:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lflow/Backstack$ParcelableEntry$1;

    invoke-direct {v0}, Lflow/Backstack$ParcelableEntry$1;-><init>()V

    sput-object v0, Lflow/Backstack$ParcelableEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(JLandroid/os/Parcelable;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "parcelable"    # Landroid/os/Parcelable;

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput-wide p1, p0, Lflow/Backstack$ParcelableEntry;->id:J

    .line 272
    iput-object p3, p0, Lflow/Backstack$ParcelableEntry;->parcelable:Landroid/os/Parcelable;

    .line 273
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public toRealEntry(Lflow/Parcer;)Lflow/Backstack$Entry;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack$Entry;"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    new-instance v0, Lflow/Backstack$Entry;

    iget-wide v2, p0, Lflow/Backstack$ParcelableEntry;->id:J

    iget-object v1, p0, Lflow/Backstack$ParcelableEntry;->parcelable:Landroid/os/Parcelable;

    invoke-interface {p1, v1}, Lflow/Parcer;->unwrap(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v1, v4}, Lflow/Backstack$Entry;-><init>(JLjava/lang/Object;Lflow/Backstack$1;)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 284
    iget-wide v0, p0, Lflow/Backstack$ParcelableEntry;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 285
    iget-object v0, p0, Lflow/Backstack$ParcelableEntry;->parcelable:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 286
    return-void
.end method
