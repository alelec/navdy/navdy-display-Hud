.class Lflow/Flow$4;
.super Lflow/Flow$Transition;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->goUp()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 1

    .prologue
    .line 133
    iput-object p1, p0, Lflow/Flow$4;->this$0:Lflow/Flow;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflow/Flow$Transition;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 5

    .prologue
    .line 135
    iget-object v3, p0, Lflow/Flow$4;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v3}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v3

    invoke-virtual {v3}, Lflow/Backstack;->current()Lflow/Backstack$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v0

    .line 136
    .local v0, "current":Ljava/lang/Object;
    instance-of v3, v0, Lflow/HasParent;

    if-eqz v3, :cond_0

    .line 137
    check-cast v0, Lflow/HasParent;

    .end local v0    # "current":Ljava/lang/Object;
    invoke-interface {v0}, Lflow/HasParent;->getParent()Ljava/lang/Object;

    move-result-object v2

    .line 138
    .local v2, "parent":Ljava/lang/Object;
    iget-object v3, p0, Lflow/Flow$4;->this$0:Lflow/Flow;

    # getter for: Lflow/Flow;->backstack:Lflow/Backstack;
    invoke-static {v3}, Lflow/Flow;->access$100(Lflow/Flow;)Lflow/Backstack;

    move-result-object v3

    invoke-static {v2}, Lflow/Backstack;->fromUpChain(Ljava/lang/Object;)Lflow/Backstack;

    move-result-object v4

    # invokes: Lflow/Flow;->preserveEquivalentPrefix(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;
    invoke-static {v3, v4}, Lflow/Flow;->access$200(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;

    move-result-object v1

    .line 139
    .local v1, "newBackstack":Lflow/Backstack;
    sget-object v3, Lflow/Flow$Direction;->BACKWARD:Lflow/Flow$Direction;

    invoke-virtual {p0, v1, v3}, Lflow/Flow$4;->go(Lflow/Backstack;Lflow/Flow$Direction;)V

    .line 144
    .end local v1    # "newBackstack":Lflow/Backstack;
    .end local v2    # "parent":Ljava/lang/Object;
    :goto_0
    return-void

    .line 142
    .restart local v0    # "current":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Lflow/Flow$4;->onComplete()V

    goto :goto_0
.end method
