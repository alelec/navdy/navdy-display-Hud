.class public Lflow/Backstack$ParcelableBackstack$Parcelled;
.super Ljava/lang/Object;
.source "Backstack.java"

# interfaces
.implements Lflow/Backstack$ParcelableBackstack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Backstack$ParcelableBackstack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Parcelled"
.end annotation


# instance fields
.field private final entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflow/Backstack$ParcelableEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final highestId:J


# direct methods
.method constructor <init>(JLjava/util/List;)V
    .locals 1
    .param p1, "highestId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lflow/Backstack$ParcelableEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p3, "entries":Ljava/util/List;, "Ljava/util/List<Lflow/Backstack$ParcelableEntry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    iput-wide p1, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->highestId:J

    .line 243
    iput-object p3, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->entries:Ljava/util/List;

    .line 244
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public getBackstack(Lflow/Parcer;)Lflow/Backstack;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Parcer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lflow/Backstack;"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "parcer":Lflow/Parcer;, "Lflow/Parcer<Ljava/lang/Object;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v0, "backstack":Ljava/util/List;, "Ljava/util/List<Lflow/Backstack$Entry;>;"
    iget-object v3, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->entries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Backstack$ParcelableEntry;

    .line 249
    .local v1, "entry":Lflow/Backstack$ParcelableEntry;
    invoke-virtual {v1, p1}, Lflow/Backstack$ParcelableEntry;->toRealEntry(Lflow/Parcer;)Lflow/Backstack$Entry;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    .end local v1    # "entry":Lflow/Backstack$ParcelableEntry;
    :cond_0
    new-instance v3, Lflow/Backstack$Builder;

    iget-wide v4, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->highestId:J

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v0, v6}, Lflow/Backstack$Builder;-><init>(JLjava/util/Collection;Lflow/Backstack$1;)V

    invoke-virtual {v3}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v3

    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 260
    iget-wide v0, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->highestId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 261
    iget-object v0, p0, Lflow/Backstack$ParcelableBackstack$Parcelled;->entries:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 262
    return-void
.end method
