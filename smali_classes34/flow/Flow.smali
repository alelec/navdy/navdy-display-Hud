.class public final Lflow/Flow;
.super Ljava/lang/Object;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/Flow$Transition;,
        Lflow/Flow$Listener;,
        Lflow/Flow$Callback;,
        Lflow/Flow$Direction;
    }
.end annotation


# instance fields
.field private backstack:Lflow/Backstack;

.field private final listener:Lflow/Flow$Listener;

.field private transition:Lflow/Flow$Transition;


# direct methods
.method public constructor <init>(Lflow/Backstack;Lflow/Flow$Listener;)V
    .locals 0
    .param p1, "backstack"    # Lflow/Backstack;
    .param p2, "listener"    # Lflow/Flow$Listener;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lflow/Flow;->listener:Lflow/Flow$Listener;

    .line 55
    iput-object p1, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    .line 56
    return-void
.end method

.method static synthetic access$100(Lflow/Flow;)Lflow/Backstack;
    .locals 1
    .param p0, "x0"    # Lflow/Flow;

    .prologue
    .line 22
    iget-object v0, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    return-object v0
.end method

.method static synthetic access$102(Lflow/Flow;Lflow/Backstack;)Lflow/Backstack;
    .locals 0
    .param p0, "x0"    # Lflow/Flow;
    .param p1, "x1"    # Lflow/Backstack;

    .prologue
    .line 22
    iput-object p1, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    return-object p1
.end method

.method static synthetic access$200(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;
    .locals 1
    .param p0, "x0"    # Lflow/Backstack;
    .param p1, "x1"    # Lflow/Backstack;

    .prologue
    .line 22
    invoke-static {p0, p1}, Lflow/Flow;->preserveEquivalentPrefix(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lflow/Flow;)Lflow/Flow$Transition;
    .locals 1
    .param p0, "x0"    # Lflow/Flow;

    .prologue
    .line 22
    iget-object v0, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    return-object v0
.end method

.method static synthetic access$302(Lflow/Flow;Lflow/Flow$Transition;)Lflow/Flow$Transition;
    .locals 0
    .param p0, "x0"    # Lflow/Flow;
    .param p1, "x1"    # Lflow/Flow$Transition;

    .prologue
    .line 22
    iput-object p1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    return-object p1
.end method

.method static synthetic access$400(Lflow/Flow;)Lflow/Flow$Listener;
    .locals 1
    .param p0, "x0"    # Lflow/Flow;

    .prologue
    .line 22
    iget-object v0, p0, Lflow/Flow;->listener:Lflow/Flow$Listener;

    return-object v0
.end method

.method private move(Lflow/Flow$Transition;)V
    .locals 1
    .param p1, "transition"    # Lflow/Flow$Transition;

    .prologue
    .line 191
    iget-object v0, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    iget-boolean v0, v0, Lflow/Flow$Transition;->finished:Z

    if-eqz v0, :cond_1

    .line 192
    :cond_0
    iput-object p1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    .line 193
    invoke-virtual {p1}, Lflow/Flow$Transition;->execute()V

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    invoke-virtual {v0, p1}, Lflow/Flow$Transition;->enqueue(Lflow/Flow$Transition;)V

    goto :goto_0
.end method

.method private static preserveEquivalentPrefix(Lflow/Backstack;Lflow/Backstack;)Lflow/Backstack;
    .locals 7
    .param p0, "current"    # Lflow/Backstack;
    .param p1, "proposed"    # Lflow/Backstack;

    .prologue
    .line 200
    invoke-virtual {p0}, Lflow/Backstack;->reverseIterator()Ljava/util/Iterator;

    move-result-object v3

    .line 201
    .local v3, "oldIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lflow/Backstack$Entry;>;"
    invoke-virtual {p1}, Lflow/Backstack;->reverseIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 203
    .local v1, "newIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lflow/Backstack$Entry;>;"
    invoke-static {}, Lflow/Backstack;->emptyBuilder()Lflow/Backstack$Builder;

    move-result-object v4

    .line 205
    .local v4, "preserving":Lflow/Backstack$Builder;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 206
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Backstack$Entry;

    .line 207
    .local v0, "newEntry":Lflow/Backstack$Entry;
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 208
    invoke-virtual {v0}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    .line 220
    .end local v0    # "newEntry":Lflow/Backstack$Entry;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 221
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lflow/Backstack$Entry;

    invoke-virtual {v5}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    goto :goto_1

    .line 211
    .restart local v0    # "newEntry":Lflow/Backstack$Entry;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/Backstack$Entry;

    .line 212
    .local v2, "oldEntry":Lflow/Backstack$Entry;
    invoke-virtual {v2}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 213
    invoke-virtual {v2}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {v0}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflow/Backstack$Builder;->push(Ljava/lang/Object;)Lflow/Backstack$Builder;

    goto :goto_1

    .line 223
    .end local v0    # "newEntry":Lflow/Backstack$Entry;
    .end local v2    # "oldEntry":Lflow/Backstack$Entry;
    :cond_3
    invoke-virtual {v4}, Lflow/Backstack$Builder;->build()Lflow/Backstack;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public backward(Lflow/Backstack;)V
    .locals 1
    .param p1, "newBackstack"    # Lflow/Backstack;

    .prologue
    .line 183
    new-instance v0, Lflow/Flow$7;

    invoke-direct {v0, p0, p1}, Lflow/Flow$7;-><init>(Lflow/Flow;Lflow/Backstack;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 188
    return-void
.end method

.method public forward(Lflow/Backstack;)V
    .locals 1
    .param p1, "newBackstack"    # Lflow/Backstack;

    .prologue
    .line 174
    new-instance v0, Lflow/Flow$6;

    invoke-direct {v0, p0, p1}, Lflow/Flow$6;-><init>(Lflow/Flow;Lflow/Backstack;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 179
    return-void
.end method

.method public getBackstack()Lflow/Backstack;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    return-object v0
.end method

.method public goBack()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 154
    iget-object v1, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    invoke-virtual {v1}, Lflow/Backstack;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    iget-boolean v1, v1, Lflow/Flow$Transition;->finished:Z

    if-nez v1, :cond_1

    .line 155
    .local v0, "canGoBack":Z
    :cond_0
    :goto_0
    new-instance v1, Lflow/Flow$5;

    invoke-direct {v1, p0}, Lflow/Flow$5;-><init>(Lflow/Flow;)V

    invoke-direct {p0, v1}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 169
    return v0

    .line 154
    .end local v0    # "canGoBack":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public goTo(Ljava/lang/Object;)V
    .locals 1
    .param p1, "screen"    # Ljava/lang/Object;

    .prologue
    .line 64
    new-instance v0, Lflow/Flow$1;

    invoke-direct {v0, p0, p1}, Lflow/Flow$1;-><init>(Lflow/Flow;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 70
    return-void
.end method

.method public goUp()Z
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    .line 130
    .local v0, "canGoUp":Z
    iget-object v1, p0, Lflow/Flow;->backstack:Lflow/Backstack;

    invoke-virtual {v1}, Lflow/Backstack;->current()Lflow/Backstack$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lflow/HasParent;

    if-nez v1, :cond_0

    iget-object v1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflow/Flow;->transition:Lflow/Flow$Transition;

    iget-boolean v1, v1, Lflow/Flow$Transition;->finished:Z

    if-nez v1, :cond_1

    .line 131
    :cond_0
    const/4 v0, 0x1

    .line 133
    :cond_1
    new-instance v1, Lflow/Flow$4;

    invoke-direct {v1, p0}, Lflow/Flow$4;-><init>(Lflow/Flow;)V

    invoke-direct {p0, v1}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 146
    return v0
.end method

.method public replaceTo(Ljava/lang/Object;)V
    .locals 1
    .param p1, "screen"    # Ljava/lang/Object;

    .prologue
    .line 116
    new-instance v0, Lflow/Flow$3;

    invoke-direct {v0, p0, p1}, Lflow/Flow$3;-><init>(Lflow/Flow;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 122
    return-void
.end method

.method public resetTo(Ljava/lang/Object;)V
    .locals 1
    .param p1, "screen"    # Ljava/lang/Object;

    .prologue
    .line 77
    new-instance v0, Lflow/Flow$2;

    invoke-direct {v0, p0, p1}, Lflow/Flow$2;-><init>(Lflow/Flow;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$Transition;)V

    .line 112
    return-void
.end method
