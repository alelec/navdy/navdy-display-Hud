.class public Lcom/nokia/maps/d;
.super Ljava/lang/Object;
.source "ARControllerImpl.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/d$a;
    }
.end annotation


# static fields
.field private static U:Lcom/nokia/maps/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nokia/maps/ar",
            "<",
            "Lcom/here/android/mpa/ar/ARController;",
            "Lcom/nokia/maps/d;",
            ">;"
        }
    .end annotation
.end field

.field private static V:Lcom/nokia/maps/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/ar/ARController;",
            "Lcom/nokia/maps/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPanListener;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnTapListener;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;",
            ">;"
        }
    .end annotation
.end field

.field private final G:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final H:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final I:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPoseListener;",
            ">;"
        }
    .end annotation
.end field

.field private final J:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final K:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private final L:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private M:Lcom/here/android/mpa/ar/ARController$OnPitchFunction;

.field private N:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/here/android/mpa/ar/ARObject;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lcom/nokia/maps/f;

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private W:Lcom/nokia/maps/aw$d;

.field private X:Lcom/nokia/maps/aw$d;

.field private Y:Lcom/nokia/maps/aw$d;

.field private Z:Lcom/nokia/maps/aw$d;

.field final a:Lcom/nokia/maps/aw;

.field private aa:Lcom/nokia/maps/aw$d;

.field private ab:Lcom/nokia/maps/aw$d;

.field private ac:Lcom/nokia/maps/aw$d;

.field private ad:Lcom/nokia/maps/aw$a;

.field private ae:Lcom/nokia/maps/aw$a;

.field private af:Lcom/nokia/maps/aw$a;

.field private ag:Lcom/nokia/maps/aw$a;

.field private ah:Lcom/nokia/maps/aw$a;

.field private ai:Lcom/nokia/maps/aw$d;

.field private aj:Lcom/nokia/maps/aw$a;

.field private ak:Lcom/nokia/maps/aw$a;

.field private al:Lcom/nokia/maps/aw$a;

.field private am:Lcom/nokia/maps/aw$a;

.field private an:Lcom/nokia/maps/aw$a;

.field private ao:Lcom/nokia/maps/aw$e;

.field private ap:Lcom/nokia/maps/aw$d;

.field private aq:Lcom/nokia/maps/aw$a;

.field private ar:Lcom/nokia/maps/aw$a;

.field private as:Lcom/nokia/maps/aw$a;

.field final b:Ljava/lang/Object;

.field final c:Ljava/lang/Object;

.field final d:Lcom/nokia/maps/aw$a;

.field e:Lcom/here/android/mpa/ar/ARController$c;

.field f:Lcom/here/android/mpa/ar/ARController$ViewType;

.field g:I

.field final h:Ljava/lang/Object;

.field public i:Lcom/nokia/maps/aw$a;

.field public j:Lcom/nokia/maps/aw$a;

.field public k:Lcom/nokia/maps/aw$a;

.field private l:Lcom/nokia/maps/ARLayoutControl;

.field private m:Lcom/nokia/maps/MapImpl;

.field private n:Lcom/nokia/maps/y;

.field private o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private p:Z

.field private q:Lcom/nokia/maps/ee;

.field private final r:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$a;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$b;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 218
    sput-object v0, Lcom/nokia/maps/d;->U:Lcom/nokia/maps/ar;

    .line 220
    sput-object v0, Lcom/nokia/maps/d;->V:Lcom/nokia/maps/m;

    .line 223
    const-class v0, Lcom/here/android/mpa/ar/ARController;

    invoke-static {v0}, Lcom/nokia/maps/ck;->a(Ljava/lang/Class;)V

    .line 224
    return-void
.end method

.method public constructor <init>(Lcom/nokia/maps/y;Lcom/nokia/maps/ee;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v3, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    .line 83
    iput-object v3, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    .line 85
    iput-object v3, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    .line 87
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 89
    iput-boolean v1, p0, Lcom/nokia/maps/d;->p:Z

    .line 92
    iput-object v3, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    .line 94
    new-instance v2, Lcom/nokia/maps/aw;

    invoke-direct {v2}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->a:Lcom/nokia/maps/aw;

    .line 96
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 98
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 100
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 102
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 104
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 106
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 118
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->x:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 120
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 122
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 124
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 126
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 128
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 130
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 132
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 134
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 136
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 138
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 140
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 142
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 144
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 146
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 148
    iput-object v3, p0, Lcom/nokia/maps/d;->M:Lcom/here/android/mpa/ar/ARController$OnPitchFunction;

    .line 155
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    .line 157
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->b:Ljava/lang/Object;

    .line 159
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/nokia/maps/d;->c:Ljava/lang/Object;

    .line 161
    iput-object v3, p0, Lcom/nokia/maps/d;->O:Lcom/nokia/maps/f;

    .line 163
    iput-boolean v0, p0, Lcom/nokia/maps/d;->P:Z

    .line 165
    iput-boolean v0, p0, Lcom/nokia/maps/d;->Q:Z

    .line 167
    iput-boolean v0, p0, Lcom/nokia/maps/d;->R:Z

    .line 169
    iput-boolean v0, p0, Lcom/nokia/maps/d;->S:Z

    .line 171
    iput-boolean v0, p0, Lcom/nokia/maps/d;->T:Z

    .line 384
    new-instance v2, Lcom/nokia/maps/d$1;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$1;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->d:Lcom/nokia/maps/aw$a;

    .line 1104
    sget-object v2, Lcom/here/android/mpa/ar/ARController$c;->a:Lcom/here/android/mpa/ar/ARController$c;

    iput-object v2, p0, Lcom/nokia/maps/d;->e:Lcom/here/android/mpa/ar/ARController$c;

    .line 1134
    sget-object v2, Lcom/here/android/mpa/ar/ARController$ViewType;->AUTO:Lcom/here/android/mpa/ar/ARController$ViewType;

    iput-object v2, p0, Lcom/nokia/maps/d;->f:Lcom/here/android/mpa/ar/ARController$ViewType;

    .line 1830
    const/4 v2, 0x3

    iput v2, p0, Lcom/nokia/maps/d;->g:I

    .line 2495
    new-instance v2, Lcom/nokia/maps/d$12;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$12;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->h:Ljava/lang/Object;

    .line 2554
    new-instance v2, Lcom/nokia/maps/d$22;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$22;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->i:Lcom/nokia/maps/aw$a;

    .line 2566
    new-instance v2, Lcom/nokia/maps/d$23;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$23;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->j:Lcom/nokia/maps/aw$a;

    .line 2578
    new-instance v2, Lcom/nokia/maps/d$24;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$24;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->k:Lcom/nokia/maps/aw$a;

    .line 3139
    new-instance v2, Lcom/nokia/maps/d$25;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$25;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->W:Lcom/nokia/maps/aw$d;

    .line 3146
    new-instance v2, Lcom/nokia/maps/d$26;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$26;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->X:Lcom/nokia/maps/aw$d;

    .line 3159
    new-instance v2, Lcom/nokia/maps/d$27;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$27;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->Y:Lcom/nokia/maps/aw$d;

    .line 3172
    new-instance v2, Lcom/nokia/maps/d$28;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$28;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->Z:Lcom/nokia/maps/aw$d;

    .line 3185
    new-instance v2, Lcom/nokia/maps/d$2;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$2;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->aa:Lcom/nokia/maps/aw$d;

    .line 3198
    new-instance v2, Lcom/nokia/maps/d$3;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$3;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ab:Lcom/nokia/maps/aw$d;

    .line 3211
    new-instance v2, Lcom/nokia/maps/d$4;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$4;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ac:Lcom/nokia/maps/aw$d;

    .line 3251
    new-instance v2, Lcom/nokia/maps/d$5;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$5;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ad:Lcom/nokia/maps/aw$a;

    .line 3274
    new-instance v2, Lcom/nokia/maps/d$6;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$6;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ae:Lcom/nokia/maps/aw$a;

    .line 3290
    new-instance v2, Lcom/nokia/maps/d$7;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$7;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->af:Lcom/nokia/maps/aw$a;

    .line 3316
    new-instance v2, Lcom/nokia/maps/d$8;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$8;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ag:Lcom/nokia/maps/aw$a;

    .line 3332
    new-instance v2, Lcom/nokia/maps/d$9;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$9;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ah:Lcom/nokia/maps/aw$a;

    .line 3348
    new-instance v2, Lcom/nokia/maps/d$10;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$10;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ai:Lcom/nokia/maps/aw$d;

    .line 3361
    new-instance v2, Lcom/nokia/maps/d$11;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$11;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->aj:Lcom/nokia/maps/aw$a;

    .line 3374
    new-instance v2, Lcom/nokia/maps/d$13;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$13;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ak:Lcom/nokia/maps/aw$a;

    .line 3387
    new-instance v2, Lcom/nokia/maps/d$14;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$14;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->al:Lcom/nokia/maps/aw$a;

    .line 3407
    new-instance v2, Lcom/nokia/maps/d$15;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$15;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->am:Lcom/nokia/maps/aw$a;

    .line 3420
    new-instance v2, Lcom/nokia/maps/d$16;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$16;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->an:Lcom/nokia/maps/aw$a;

    .line 3433
    new-instance v2, Lcom/nokia/maps/d$17;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$17;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ao:Lcom/nokia/maps/aw$e;

    .line 3447
    new-instance v2, Lcom/nokia/maps/d$18;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$18;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    .line 3461
    new-instance v2, Lcom/nokia/maps/d$19;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$19;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->aq:Lcom/nokia/maps/aw$a;

    .line 3472
    new-instance v2, Lcom/nokia/maps/d$20;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$20;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->ar:Lcom/nokia/maps/aw$a;

    .line 3490
    new-instance v2, Lcom/nokia/maps/d$21;

    invoke-direct {v2, p0}, Lcom/nokia/maps/d$21;-><init>(Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->as:Lcom/nokia/maps/aw$a;

    .line 249
    iput-object p1, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    .line 250
    iput-object p2, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    .line 251
    iget-object v2, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v2, v2, Lcom/nokia/maps/ee;->a:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->i:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 252
    iget-object v2, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v2, v2, Lcom/nokia/maps/ee;->b:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->j:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 253
    iget-object v2, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v2, v2, Lcom/nokia/maps/ee;->c:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->k:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 260
    new-instance v2, Lcom/nokia/maps/ARLayoutControl;

    iget-object v3, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-direct {v2, v3, p0}, Lcom/nokia/maps/ARLayoutControl;-><init>(Lcom/nokia/maps/y;Lcom/nokia/maps/d;)V

    iput-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    .line 262
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v3, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/ARLayoutControl;->a(Lcom/nokia/maps/ee;)V

    .line 264
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->a:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->W:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 265
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->b:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->X:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 266
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->c:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->Y:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 267
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->d:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->Z:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 268
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->e:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->aa:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 270
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->f:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ab:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 271
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->g:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ac:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 277
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->o:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ad:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 279
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->k:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ae:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 280
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->j:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->af:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 281
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->l:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ag:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 282
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->m:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ah:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 283
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->r:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->aj:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 284
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->n:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ao:Lcom/nokia/maps/aw$e;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 285
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->p:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ai:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 286
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->s:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ak:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 287
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->t:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->al:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 288
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->u:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->am:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 289
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->v:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->an:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 290
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->w:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 291
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->y:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->aq:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 292
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->z:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->ar:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 293
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->x:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->d:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 294
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v2, v2, Lcom/nokia/maps/ARLayoutControl;->A:Lcom/nokia/maps/aw;

    iget-object v3, p0, Lcom/nokia/maps/d;->as:Lcom/nokia/maps/aw$a;

    invoke-virtual {v2, v3}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 297
    invoke-virtual {p0}, Lcom/nokia/maps/d;->n()Z

    .line 298
    invoke-virtual {p0}, Lcom/nokia/maps/d;->o()Z

    .line 299
    invoke-virtual {p0}, Lcom/nokia/maps/d;->m()Z

    .line 300
    invoke-virtual {p0}, Lcom/nokia/maps/d;->p()Z

    .line 301
    invoke-virtual {p0}, Lcom/nokia/maps/d;->q()Z

    .line 306
    iget-object v2, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    instance-of v2, v2, Lcom/nokia/maps/an;

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    .line 308
    new-instance v0, Lcom/nokia/maps/f;

    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-direct {v0, v1}, Lcom/nokia/maps/f;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/d;->O:Lcom/nokia/maps/f;

    .line 309
    return-void

    :cond_0
    move v0, v1

    .line 306
    goto :goto_0
.end method

.method static synthetic A(Lcom/nokia/maps/d;)Lcom/here/android/mpa/ar/ARController$OnPitchFunction;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->M:Lcom/here/android/mpa/ar/ARController$OnPitchFunction;

    return-object v0
.end method

.method static synthetic B(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static a(Lcom/nokia/maps/d;)Lcom/here/android/mpa/ar/ARController;
    .locals 1

    .prologue
    .line 227
    if-eqz p0, :cond_0

    sget-object v0, Lcom/nokia/maps/d;->U:Lcom/nokia/maps/ar;

    invoke-interface {v0, p0}, Lcom/nokia/maps/ar;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/ar/ARController;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/here/android/mpa/ar/ARController;)Lcom/nokia/maps/d;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/nokia/maps/d;->V:Lcom/nokia/maps/m;

    invoke-interface {v0, p0}, Lcom/nokia/maps/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/d;

    return-object v0
.end method

.method public static a(Lcom/nokia/maps/m;Lcom/nokia/maps/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/ar/ARController;",
            "Lcom/nokia/maps/d;",
            ">;",
            "Lcom/nokia/maps/ar",
            "<",
            "Lcom/here/android/mpa/ar/ARController;",
            "Lcom/nokia/maps/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 232
    sput-object p0, Lcom/nokia/maps/d;->V:Lcom/nokia/maps/m;

    .line 233
    sput-object p1, Lcom/nokia/maps/d;->U:Lcom/nokia/maps/ar;

    .line 234
    return-void
.end method

.method private ab()V
    .locals 1

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->k()V

    .line 413
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->n()V

    .line 415
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/nokia/maps/d;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/nokia/maps/d;->ab()V

    return-void
.end method

.method static synthetic c(Lcom/nokia/maps/d;)Lcom/nokia/maps/ARLayoutControl;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    return-object v0
.end method

.method static synthetic d(Lcom/nokia/maps/d;)Lcom/nokia/maps/aw$d;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    return-object v0
.end method

.method static synthetic e(Lcom/nokia/maps/d;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic f(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic j(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic m(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic n(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic o(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic p(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic q(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic r(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic s(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic t(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic u(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic v(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic w(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic x(Lcom/nokia/maps/d;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic y(Lcom/nokia/maps/d;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    return v0
.end method

.method static synthetic z(Lcom/nokia/maps/d;)Lcom/nokia/maps/y;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1748
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1749
    const/4 v0, 0x0

    .line 1752
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getUseDownIconOpacity()Z

    move-result v0

    goto :goto_0
.end method

.method public B()F
    .locals 2

    .prologue
    .line 1776
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1777
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1778
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1781
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getDownIconOpacity()F

    move-result v0

    goto :goto_0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 1848
    iget v0, p0, Lcom/nokia/maps/d;->g:I

    return v0
.end method

.method public D()J
    .locals 2

    .prologue
    .line 1964
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1965
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1966
    const-wide/16 v0, -0x1

    .line 1968
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getIntroAnimationTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public E()J
    .locals 2

    .prologue
    .line 1991
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1992
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1993
    const-wide/16 v0, -0x1

    .line 1995
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getTiltUpMaxTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public F()J
    .locals 2

    .prologue
    .line 2018
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2019
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2020
    const-wide/16 v0, -0x1

    .line 2022
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getTiltUpMinTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public G()F
    .locals 1

    .prologue
    .line 2134
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getInfoAnimationMinWidthFactor()F

    move-result v0

    return v0
.end method

.method public H()F
    .locals 2

    .prologue
    .line 2184
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2185
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2186
    const/high16 v0, -0x40800000    # -1.0f

    .line 2188
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getDownViewMinOpacity()F

    move-result v0

    goto :goto_0
.end method

.method public I()F
    .locals 2

    .prologue
    .line 2211
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2212
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2213
    const/high16 v0, -0x40800000    # -1.0f

    .line 2215
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getDownViewMaxOpacity()F

    move-result v0

    goto :goto_0
.end method

.method public J()Lcom/here/android/mpa/ar/ARController$ProjectionType;
    .locals 4

    .prologue
    .line 2238
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2239
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2240
    sget-object v0, Lcom/here/android/mpa/ar/ARController$ProjectionType;->NEAR_FAR:Lcom/here/android/mpa/ar/ARController$ProjectionType;

    .line 2242
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/here/android/mpa/ar/ARController$ProjectionType;->values()[Lcom/here/android/mpa/ar/ARController$ProjectionType;

    move-result-object v0

    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->getProjectionType(J)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public K()F
    .locals 2

    .prologue
    .line 2265
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2266
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2267
    const/high16 v0, -0x40800000    # -1.0f

    .line 2269
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getSelectedItemOpacity()F

    move-result v0

    goto :goto_0
.end method

.method public L()F
    .locals 2

    .prologue
    .line 2292
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2293
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2294
    const/high16 v0, -0x40800000    # -1.0f

    .line 2296
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getNonSelectedItemsOpacity()F

    move-result v0

    goto :goto_0
.end method

.method public M()F
    .locals 2

    .prologue
    .line 2325
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2326
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2327
    const/high16 v0, -0x40800000    # -1.0f

    .line 2329
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMaxZoomScale()F

    move-result v0

    goto :goto_0
.end method

.method public N()F
    .locals 2

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2353
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2354
    const/high16 v0, -0x40800000    # -1.0f

    .line 2356
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMinPitchDownView()F

    move-result v0

    goto :goto_0
.end method

.method public O()J
    .locals 2

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2383
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2384
    const-wide/16 v0, -0x1

    .line 2386
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getSensorsWaitTimeout()J

    move-result-wide v0

    goto :goto_0
.end method

.method public P()Landroid/graphics/PointF;
    .locals 2

    .prologue
    .line 2415
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2416
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2417
    const/4 v0, 0x0

    .line 2419
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getScreenViewPoint()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public Q()F
    .locals 2

    .prologue
    .line 2442
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2443
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2444
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2446
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getOcclusionOpacity()F

    move-result v0

    goto :goto_0
.end method

.method R()Lcom/nokia/maps/ARLayoutControl;
    .locals 1

    .prologue
    .line 2487
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    return-object v0
.end method

.method public S()V
    .locals 2

    .prologue
    .line 3033
    iget-object v1, p0, Lcom/nokia/maps/d;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 3034
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/nokia/maps/d;->M:Lcom/here/android/mpa/ar/ARController$OnPitchFunction;

    .line 3035
    monitor-exit v1

    .line 3036
    return-void

    .line 3035
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method T()V
    .locals 2

    .prologue
    .line 3062
    sget-object v0, Lcom/nokia/maps/j;->a:Ljava/lang/String;

    const-string v1, "+++ APP paused!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3064
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-eqz v0, :cond_0

    .line 3065
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->pause()V

    .line 3068
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    if-eqz v0, :cond_1

    .line 3069
    iget-object v0, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapImpl;->a(Z)V

    .line 3071
    :cond_1
    return-void
.end method

.method U()V
    .locals 2

    .prologue
    .line 3077
    sget-object v0, Lcom/nokia/maps/j;->a:Ljava/lang/String;

    const-string v1, "+++ APP resumed!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3079
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-eqz v0, :cond_0

    .line 3080
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->resume()V

    .line 3083
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    if-eqz v0, :cond_1

    .line 3084
    iget-object v0, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapImpl;->a(Z)V

    .line 3086
    :cond_1
    return-void
.end method

.method declared-synchronized V()V
    .locals 6

    .prologue
    .line 3093
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/nokia/maps/j;->a:Ljava/lang/String;

    const-string v1, "+++ APP destroy - IN!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3094
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3095
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3137
    :goto_0
    monitor-exit p0

    return-void

    .line 3099
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/d;->a:Lcom/nokia/maps/aw;

    invoke-virtual {v0}, Lcom/nokia/maps/aw;->a()V

    .line 3101
    invoke-static {}, Lcom/nokia/maps/e;->c()V

    .line 3103
    iget-object v1, p0, Lcom/nokia/maps/d;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3104
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3105
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3106
    iget-object v0, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3108
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3093
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3108
    :cond_1
    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3111
    :try_start_5
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->destroy()V

    .line 3112
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->a(Lcom/nokia/maps/MapImpl;)V

    .line 3113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    .line 3116
    iget-object v0, p0, Lcom/nokia/maps/d;->O:Lcom/nokia/maps/f;

    invoke-virtual {v0}, Lcom/nokia/maps/f;->j()V

    .line 3117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/d;->O:Lcom/nokia/maps/f;

    .line 3121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    .line 3123
    iget-object v0, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v0, v0, Lcom/nokia/maps/ee;->c:Lcom/nokia/maps/aw;

    invoke-virtual {v0}, Lcom/nokia/maps/aw;->a()V

    .line 3124
    iget-object v0, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v0, v0, Lcom/nokia/maps/ee;->a:Lcom/nokia/maps/aw;

    invoke-virtual {v0}, Lcom/nokia/maps/aw;->a()V

    .line 3125
    iget-object v0, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    iget-object v0, v0, Lcom/nokia/maps/ee;->b:Lcom/nokia/maps/aw;

    invoke-virtual {v0}, Lcom/nokia/maps/aw;->a()V

    .line 3127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/d;->q:Lcom/nokia/maps/ee;

    .line 3136
    sget-object v0, Lcom/nokia/maps/j;->a:Ljava/lang/String;

    const-string v1, "+++ APP destroy - OUT!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method W()Z
    .locals 1

    .prologue
    .line 3592
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public X()F
    .locals 2

    .prologue
    .line 3640
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3641
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3642
    const/4 v0, 0x0

    .line 3645
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getFixedAltitude()F

    move-result v0

    goto :goto_0
.end method

.method public Y()F
    .locals 1

    .prologue
    .line 3667
    invoke-static {}, Lcom/nokia/maps/ARSensors;->c()F

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 2

    .prologue
    .line 3794
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3795
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3796
    const/4 v0, 0x0

    .line 3799
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->isEdgeDetectionEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/nokia/maps/d$a;)J
    .locals 2

    .prologue
    .line 2047
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2048
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2049
    const-wide/16 v0, -0x1

    .line 2051
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->getAnimationDelay(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public declared-synchronized a()Lcom/here/android/mpa/ar/ARController$Error;
    .locals 2

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    :goto_0
    monitor-exit p0

    return-object v0

    .line 320
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 322
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    goto :goto_0

    .line 325
    :cond_1
    iget-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    if-eqz v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->m()V

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->startLivesight()Z

    move-result v0

    if-nez v0, :cond_4

    .line 332
    iget-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    if-eqz v0, :cond_3

    .line 333
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->n()V

    .line 335
    :cond_3
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->OPERATION_NOT_ALLOWED:Lcom/here/android/mpa/ar/ARController$Error;

    goto :goto_0

    .line 338
    :cond_4
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 340
    iget-boolean v0, p0, Lcom/nokia/maps/d;->p:Z

    if-eqz v0, :cond_5

    .line 341
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->n()V

    .line 343
    :cond_5
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->stopLivesight(Z)V

    .line 344
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->SENSORS_UNAVAILABLE:Lcom/here/android/mpa/ar/ARController$Error;

    goto :goto_0

    .line 347
    :cond_6
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 348
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->NONE:Lcom/here/android/mpa/ar/ARController$Error;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)Lcom/here/android/mpa/ar/ARController$Error;
    .locals 2

    .prologue
    .line 366
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    :goto_0
    monitor-exit p0

    return-object v0

    .line 370
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 372
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    goto :goto_0

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->stopLivesight(Z)V

    .line 377
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->NONE:Lcom/here/android/mpa/ar/ARController$Error;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/graphics/PointF;)Lcom/here/android/mpa/ar/ARObject;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 701
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 702
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 711
    :cond_0
    :goto_0
    return-object v0

    .line 705
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v2, Landroid/graphics/Point;

    iget v3, p1, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iget v4, p1, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/nokia/maps/ARLayoutControl;->getObjects(Landroid/graphics/Point;)[J

    move-result-object v1

    .line 707
    array-length v2, v1

    if-lez v2, :cond_0

    .line 708
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    aget-wide v2, v1, v5

    invoke-virtual {v0, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->press(J)V

    .line 709
    aget-wide v0, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/nokia/maps/d;->b(J)Lcom/here/android/mpa/ar/ARObject;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/nokia/maps/GeoCoordinateImpl;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1419
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1420
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1429
    :cond_0
    :goto_0
    return-object v0

    .line 1424
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1425
    if-eqz v1, :cond_0

    .line 1426
    invoke-virtual {v1, p1}, Lcom/nokia/maps/ARSensors;->a(Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/ViewRect;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/ar/ARObject;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 983
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 984
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1004
    :cond_0
    return-object v0

    .line 988
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getX()I

    move-result v2

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getY()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 989
    new-instance v2, Landroid/graphics/Point;

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getX()I

    move-result v3

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getY()I

    move-result v4

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 991
    iget-object v3, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v3, v1, v2}, Lcom/nokia/maps/ARLayoutControl;->getObjectsRect(Landroid/graphics/Point;Landroid/graphics/Point;)[J

    move-result-object v2

    .line 994
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 995
    invoke-virtual {p0, v4, v5}, Lcom/nokia/maps/d;->b(J)Lcom/here/android/mpa/ar/ARObject;

    move-result-object v4

    .line 996
    if-nez v4, :cond_2

    .line 994
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 999
    :cond_2
    if-nez v0, :cond_3

    .line 1000
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 1002
    :cond_3
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 690
    :goto_0
    return-void

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setBack2FrontIconSizeRatio(F)V

    goto :goto_0
.end method

.method public a(FFFF)V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 675
    :goto_0
    return-void

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/nokia/maps/ARLayoutControl;->setPlanesParam(FFFF)V

    goto :goto_0
.end method

.method public a(FZ)V
    .locals 2

    .prologue
    .line 3620
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3621
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3631
    :goto_0
    return-void

    .line 3625
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 3626
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_PARAMETERS:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 3630
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setFixedAltitude(FZ)V

    goto :goto_0
.end method

.method public a(FZZ)V
    .locals 2

    .prologue
    .line 2310
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2311
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2315
    :goto_0
    return-void

    .line 2314
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/ARLayoutControl;->setMaxZoomScale(FZZ)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1833
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1834
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1839
    :goto_0
    return-void

    .line 1837
    :cond_0
    iput p1, p0, Lcom/nokia/maps/d;->g:I

    .line 1838
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->setUpdateDistanceThreshold(F)V

    goto :goto_0
.end method

.method public a(IF)V
    .locals 2

    .prologue
    .line 1885
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1886
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1890
    :goto_0
    return-void

    .line 1889
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setFilterCoeff(IF)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 1642
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1643
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1647
    :goto_0
    return-void

    .line 1646
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v1, Lcom/here/android/mpa/common/Size;

    invoke-direct {v1, p1, p2}, Lcom/here/android/mpa/common/Size;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->setFrontIconSize(Lcom/here/android/mpa/common/Size;)V

    goto :goto_0
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/nokia/maps/d;->O:Lcom/nokia/maps/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/f;->a(III)V

    .line 446
    return-void
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 596
    iget-object v1, p0, Lcom/nokia/maps/d;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 597
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    monitor-exit v1

    .line 599
    return-void

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 5

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1175
    :goto_0
    return-void

    .line 1173
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v1, Landroid/graphics/Point;

    iget v2, p1, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget v3, p1, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    new-instance v2, Landroid/graphics/Point;

    iget v3, p2, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iget v4, p2, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ARLayoutControl;->pan(Landroid/graphics/Point;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/PointF;Z)V
    .locals 2

    .prologue
    .line 2401
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2402
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2406
    :goto_0
    return-void

    .line 2405
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setScreenViewPoint(Landroid/graphics/PointF;Z)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    .line 820
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 826
    :goto_0
    return-void

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p1, Landroid/graphics/RectF;->left:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v2, Landroid/graphics/PointF;

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ARLayoutControl;->setSelectedBoundingBox(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;)V
    .locals 2

    .prologue
    .line 3903
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3904
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3909
    :goto_0
    return-void

    .line 3908
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->setIntroAnimationMode(I)V

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V
    .locals 2

    .prologue
    .line 2588
    if-nez p1, :cond_0

    .line 2595
    :goto_0
    return-void

    .line 2592
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2593
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2594
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V
    .locals 2

    .prologue
    .line 2608
    if-nez p1, :cond_0

    .line 2615
    :goto_0
    return-void

    .line 2612
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2613
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2614
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V
    .locals 2

    .prologue
    .line 2802
    if-nez p1, :cond_0

    .line 2809
    :goto_0
    return-void

    .line 2806
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->x:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2807
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->x:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2808
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V
    .locals 2

    .prologue
    .line 3003
    if-nez p1, :cond_0

    .line 3010
    :goto_0
    return-void

    .line 3007
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3008
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 3009
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V
    .locals 2

    .prologue
    .line 2628
    if-nez p1, :cond_0

    .line 2635
    :goto_0
    return-void

    .line 2632
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2633
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2634
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V
    .locals 2

    .prologue
    .line 2648
    if-nez p1, :cond_0

    .line 2655
    :goto_0
    return-void

    .line 2652
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2653
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2654
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V
    .locals 2

    .prologue
    .line 2845
    if-nez p1, :cond_0

    .line 2852
    :goto_0
    return-void

    .line 2849
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2850
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2851
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V
    .locals 2

    .prologue
    .line 2721
    if-nez p1, :cond_0

    .line 2728
    :goto_0
    return-void

    .line 2725
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2726
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2727
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPitchFunction;)V
    .locals 2

    .prologue
    .line 3023
    if-nez p1, :cond_0

    .line 3030
    :goto_0
    return-void

    .line 3027
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 3028
    :try_start_0
    iput-object p1, p0, Lcom/nokia/maps/d;->M:Lcom/here/android/mpa/ar/ARController$OnPitchFunction;

    .line 3029
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V
    .locals 2

    .prologue
    .line 2903
    if-nez p1, :cond_0

    .line 2910
    :goto_0
    return-void

    .line 2907
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2908
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2909
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V
    .locals 2

    .prologue
    .line 2983
    if-nez p1, :cond_0

    .line 2990
    :goto_0
    return-void

    .line 2987
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2988
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2989
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V
    .locals 2

    .prologue
    .line 2923
    if-nez p1, :cond_0

    .line 2930
    :goto_0
    return-void

    .line 2927
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2928
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2929
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V
    .locals 2

    .prologue
    .line 2943
    if-nez p1, :cond_0

    .line 2950
    :goto_0
    return-void

    .line 2947
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2948
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2949
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V
    .locals 2

    .prologue
    .line 2963
    if-nez p1, :cond_0

    .line 2970
    :goto_0
    return-void

    .line 2967
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2968
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2969
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V
    .locals 2

    .prologue
    .line 3039
    if-nez p1, :cond_0

    .line 3046
    :goto_0
    return-void

    .line 3043
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3044
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 3045
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V
    .locals 3

    .prologue
    .line 2865
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 2866
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2881
    :cond_0
    :goto_0
    return-void

    .line 2870
    :cond_1
    if-eqz p1, :cond_0

    .line 2874
    iget-object v1, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2875
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2877
    iget-object v0, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2878
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/nokia/maps/ARLayoutControl;->enableRadar(Z)V

    .line 2880
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V
    .locals 2

    .prologue
    .line 2824
    if-nez p1, :cond_0

    .line 2831
    :goto_0
    return-void

    .line 2828
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2829
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2830
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V
    .locals 2

    .prologue
    .line 2741
    if-nez p1, :cond_0

    .line 2748
    :goto_0
    return-void

    .line 2745
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2746
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2747
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V
    .locals 2

    .prologue
    .line 2761
    if-nez p1, :cond_0

    .line 2768
    :goto_0
    return-void

    .line 2765
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2766
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2767
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V
    .locals 2

    .prologue
    .line 2781
    if-nez p1, :cond_0

    .line 2788
    :goto_0
    return-void

    .line 2785
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2786
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2787
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$ProjectionType;)V
    .locals 4

    .prologue
    .line 2225
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2226
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2230
    :goto_0
    return-void

    .line 2229
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const-wide/16 v2, -0x1

    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARController$ProjectionType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/nokia/maps/ARLayoutControl;->setProjectionType(JI)V

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$SensorType;DDDJ)V
    .locals 10

    .prologue
    .line 3726
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3727
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3738
    :goto_0
    return-void

    .line 3731
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v0

    .line 3732
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3733
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    .line 3737
    invoke-virtual/range {v0 .. v9}, Lcom/nokia/maps/ARSensors;->a(Lcom/here/android/mpa/ar/ARController$SensorType;DDDJ)V

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$SensorType;Z)V
    .locals 2

    .prologue
    .line 3686
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 3687
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3700
    :cond_0
    :goto_0
    return-void

    .line 3691
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v0

    .line 3692
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 3693
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 3697
    :cond_3
    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARSensors;->a(Lcom/here/android/mpa/ar/ARController$SensorType;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3698
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARController$ViewType;)V
    .locals 2

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1143
    :goto_0
    return-void

    .line 1141
    :cond_0
    iput-object p1, p0, Lcom/nokia/maps/d;->f:Lcom/here/android/mpa/ar/ARController$ViewType;

    .line 1142
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARController$ViewType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->showScene(I)V

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/ar/ARModelObject;)V
    .locals 2

    .prologue
    .line 531
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    :goto_0
    monitor-exit p0

    return-void

    .line 536
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/nokia/maps/ARModelObjectImpl;->a(Lcom/here/android/mpa/ar/ARModelObject;)Lcom/nokia/maps/ARModelObjectImpl;

    move-result-object v0

    .line 537
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1, v0}, Lcom/nokia/maps/ARLayoutControl;->addARViewObject(Lcom/nokia/maps/ARModelObjectImpl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 4

    .prologue
    .line 455
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    :goto_0
    monitor-exit p0

    return-void

    .line 460
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/nokia/maps/d;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461
    :try_start_2
    iget-object v0, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARObject;->getUid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 464
    :try_start_3
    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v0

    .line 466
    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARObjectImpl;->a(Lcom/here/android/mpa/ar/ARObject;)V

    .line 468
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1, v0}, Lcom/nokia/maps/ARLayoutControl;->addARObject(Lcom/nokia/maps/ARObjectImpl;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 462
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARObject;ZF)V
    .locals 4

    .prologue
    .line 790
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 795
    :goto_0
    return-void

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nokia/maps/ARObjectImpl;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2, p3}, Lcom/nokia/maps/ARLayoutControl;->selectWithScale(JZF)V

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/ar/ARPolylineObject;)V
    .locals 2

    .prologue
    .line 499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    :goto_0
    monitor-exit p0

    return-void

    .line 503
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/nokia/maps/ARPolylineObjectImpl;->a(Lcom/here/android/mpa/ar/ARPolylineObject;)Lcom/nokia/maps/ARPolylineObjectImpl;

    move-result-object v0

    .line 504
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1, v0}, Lcom/nokia/maps/ARLayoutControl;->addARObject(Lcom/nokia/maps/ARPolylineObjectImpl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 2

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 1182
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1188
    :cond_0
    :goto_0
    return-void

    .line 1185
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->get(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->panTo(Lcom/nokia/maps/GeoCoordinateImpl;)V

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/mapping/Map;)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 432
    :goto_0
    return-void

    .line 430
    :cond_0
    invoke-static {p1}, Lcom/nokia/maps/MapImpl;->get(Lcom/here/android/mpa/mapping/Map;)Lcom/nokia/maps/MapImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    .line 431
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    iget-object v1, p0, Lcom/nokia/maps/d;->m:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->a(Lcom/nokia/maps/MapImpl;)V

    goto :goto_0
.end method

.method public a(Lcom/nokia/maps/d$a;J)V
    .locals 2

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2035
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2039
    :goto_0
    return-void

    .line 2038
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/nokia/maps/ARLayoutControl;->setAnimationDelay(IJ)V

    goto :goto_0
.end method

.method public a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V
    .locals 3

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2094
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2098
    :goto_0
    return-void

    .line 2097
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/here/android/mpa/ar/AnimationInterpolator;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ARLayoutControl;->setAnimationInterpolator(II)V

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1244
    :goto_0
    return-void

    .line 1242
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/d;->P:Z

    .line 1243
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoZoom(ZZ)V

    goto :goto_0
.end method

.method public a(FLandroid/graphics/PointF;Lcom/here/android/mpa/common/Vector3f;)Z
    .locals 2

    .prologue
    .line 3817
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3818
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3819
    const/4 v0, 0x0

    .line 3822
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/ARLayoutControl;->pixelTo3dPosition(FLandroid/graphics/PointF;Lcom/here/android/mpa/common/Vector3f;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/ar/ARObject;II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1714
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1715
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1723
    :cond_0
    :goto_0
    return v0

    .line 1719
    :cond_1
    if-eqz p1, :cond_0

    .line 1722
    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/nokia/maps/ARObjectImpl;->setStartStopSizeOnMap(II)V

    .line 1723
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Vector3f;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3838
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_0

    .line 3839
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3848
    :goto_0
    return v0

    .line 3843
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3844
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_PARAMETERS:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 3848
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->get(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/nokia/maps/ARLayoutControl;->geoTo3dPosition(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/common/Vector3f;)Z

    move-result v0

    goto :goto_0
.end method

.method public aa()Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;
    .locals 2

    .prologue
    .line 3917
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3918
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3919
    invoke-static {}, Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;->values()[Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 3922
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;->values()[Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;

    move-result-object v0

    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->getIntroAnimationMode()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1872
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1873
    const/4 v0, -0x1

    .line 1875
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->getFilterSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public b(Lcom/nokia/maps/d$a;)J
    .locals 2

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2077
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2078
    const-wide/16 v0, -0x1

    .line 2080
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->getAnimationDuration(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b(J)Lcom/here/android/mpa/ar/ARObject;
    .locals 3

    .prologue
    .line 606
    iget-object v1, p0, Lcom/nokia/maps/d;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 607
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->N:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/ar/ARObject;

    monitor-exit v1

    return-object v0

    .line 608
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/ar/ARObject;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 953
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 954
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 975
    :cond_0
    return-object v0

    .line 958
    :cond_1
    if-eqz p1, :cond_0

    .line 962
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v2, Landroid/graphics/Point;

    iget v3, p1, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iget v4, p1, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/nokia/maps/ARLayoutControl;->getObjects(Landroid/graphics/Point;)[J

    move-result-object v2

    .line 965
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 966
    invoke-virtual {p0, v4, v5}, Lcom/nokia/maps/d;->b(J)Lcom/here/android/mpa/ar/ARObject;

    move-result-object v4

    .line 967
    if-nez v4, :cond_2

    .line 965
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 970
    :cond_2
    if-nez v0, :cond_3

    .line 971
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 973
    :cond_3
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 648
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 653
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->defocus()V

    goto :goto_0
.end method

.method public b(F)V
    .locals 2

    .prologue
    .line 835
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 840
    :goto_0
    return-void

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setSelectedItemMaxViewAngle(F)V

    goto :goto_0
.end method

.method public b(IF)V
    .locals 2

    .prologue
    .line 2144
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2145
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2149
    :goto_0
    return-void

    .line 2148
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setFlyRotateDeg(IF)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 2

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1660
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1664
    :goto_0
    return-void

    .line 1663
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v1, Lcom/here/android/mpa/common/Size;

    invoke-direct {v1, p1, p2}, Lcom/here/android/mpa/common/Size;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->setBackIconSize(Lcom/here/android/mpa/common/Size;)V

    goto :goto_0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V
    .locals 2

    .prologue
    .line 2598
    if-nez p1, :cond_0

    .line 2605
    :goto_0
    return-void

    .line 2602
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2603
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2604
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V
    .locals 2

    .prologue
    .line 2618
    if-nez p1, :cond_0

    .line 2625
    :goto_0
    return-void

    .line 2622
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2623
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->s:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2624
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V
    .locals 2

    .prologue
    .line 2813
    if-nez p1, :cond_0

    .line 2820
    :goto_0
    return-void

    .line 2817
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->x:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2818
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->x:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2819
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V
    .locals 2

    .prologue
    .line 3013
    if-nez p1, :cond_0

    .line 3020
    :goto_0
    return-void

    .line 3017
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3018
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->K:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 3019
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V
    .locals 2

    .prologue
    .line 2638
    if-nez p1, :cond_0

    .line 2645
    :goto_0
    return-void

    .line 2642
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2643
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2644
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V
    .locals 2

    .prologue
    .line 2658
    if-nez p1, :cond_0

    .line 2665
    :goto_0
    return-void

    .line 2662
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2663
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->u:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2664
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V
    .locals 2

    .prologue
    .line 2855
    if-nez p1, :cond_0

    .line 2862
    :goto_0
    return-void

    .line 2859
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2860
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->G:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2861
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V
    .locals 2

    .prologue
    .line 2731
    if-nez p1, :cond_0

    .line 2738
    :goto_0
    return-void

    .line 2735
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2736
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->C:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2737
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V
    .locals 2

    .prologue
    .line 2913
    if-nez p1, :cond_0

    .line 2920
    :goto_0
    return-void

    .line 2917
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2918
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->I:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2919
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V
    .locals 2

    .prologue
    .line 2993
    if-nez p1, :cond_0

    .line 3000
    :goto_0
    return-void

    .line 2997
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2998
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2999
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V
    .locals 2

    .prologue
    .line 2933
    if-nez p1, :cond_0

    .line 2940
    :goto_0
    return-void

    .line 2937
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2938
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2939
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V
    .locals 2

    .prologue
    .line 2953
    if-nez p1, :cond_0

    .line 2960
    :goto_0
    return-void

    .line 2957
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2958
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->z:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2959
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V
    .locals 2

    .prologue
    .line 2973
    if-nez p1, :cond_0

    .line 2980
    :goto_0
    return-void

    .line 2977
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2978
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->A:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2979
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V
    .locals 2

    .prologue
    .line 3049
    if-nez p1, :cond_0

    .line 3056
    :goto_0
    return-void

    .line 3053
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3054
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->L:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 3055
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V
    .locals 3

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 2885
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2900
    :cond_0
    :goto_0
    return-void

    .line 2889
    :cond_1
    if-eqz p1, :cond_0

    .line 2893
    iget-object v1, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2894
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2896
    iget-object v0, p0, Lcom/nokia/maps/d;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2897
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/nokia/maps/ARLayoutControl;->enableRadar(Z)V

    .line 2899
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V
    .locals 2

    .prologue
    .line 2835
    if-nez p1, :cond_0

    .line 2842
    :goto_0
    return-void

    .line 2839
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2840
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->J:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2841
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V
    .locals 2

    .prologue
    .line 2751
    if-nez p1, :cond_0

    .line 2758
    :goto_0
    return-void

    .line 2755
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2756
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2757
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V
    .locals 2

    .prologue
    .line 2771
    if-nez p1, :cond_0

    .line 2778
    :goto_0
    return-void

    .line 2775
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2776
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->E:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2777
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V
    .locals 2

    .prologue
    .line 2791
    if-nez p1, :cond_0

    .line 2798
    :goto_0
    return-void

    .line 2795
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 2796
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/d;->F:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2797
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 2

    .prologue
    .line 1583
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_1

    .line 1584
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1591
    :cond_0
    :goto_0
    return-void

    .line 1587
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_0

    .line 1589
    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARSensors;->a(Lcom/here/android/mpa/common/GeoCoordinate;)V

    goto :goto_0
.end method

.method public b(Lcom/nokia/maps/d$a;J)V
    .locals 2

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2064
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2068
    :goto_0
    return-void

    .line 2067
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/nokia/maps/ARLayoutControl;->setAnimationDuration(IJ)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 622
    :goto_0
    return-void

    .line 621
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->enableDownIcons(Z)V

    goto :goto_0
.end method

.method public b(ZZ)V
    .locals 2

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1272
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1277
    :goto_0
    return-void

    .line 1275
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/d;->Q:Z

    .line 1276
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->a(ZZ)V

    goto :goto_0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/ar/ARModelObject;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 547
    monitor-enter p0

    if-nez p1, :cond_0

    .line 559
    :goto_0
    monitor-exit p0

    return v0

    .line 551
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 552
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 553
    const/4 v0, 0x0

    goto :goto_0

    .line 556
    :cond_1
    invoke-static {p1}, Lcom/nokia/maps/ARModelObjectImpl;->a(Lcom/here/android/mpa/ar/ARModelObject;)Lcom/nokia/maps/ARModelObjectImpl;

    move-result-object v1

    .line 557
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v2, v1}, Lcom/nokia/maps/ARLayoutControl;->removeARViewObject(Lcom/nokia/maps/ARModelObjectImpl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 478
    monitor-enter p0

    if-nez p1, :cond_0

    .line 495
    :goto_0
    monitor-exit p0

    return v0

    .line 482
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 483
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 484
    const/4 v0, 0x0

    goto :goto_0

    .line 487
    :cond_1
    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v1

    .line 491
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v2, v1}, Lcom/nokia/maps/ARLayoutControl;->removeARObject(Lcom/nokia/maps/ARObjectImpl;)V

    .line 493
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/nokia/maps/ARObjectImpl;->a(Lcom/here/android/mpa/ar/ARObject;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 478
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/ar/ARPolylineObject;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 509
    monitor-enter p0

    if-nez p1, :cond_0

    .line 521
    :goto_0
    monitor-exit p0

    return v0

    .line 513
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 514
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 515
    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :cond_1
    invoke-static {p1}, Lcom/nokia/maps/ARPolylineObjectImpl;->a(Lcom/here/android/mpa/ar/ARPolylineObject;)Lcom/nokia/maps/ARPolylineObjectImpl;

    move-result-object v1

    .line 519
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v2, v1}, Lcom/nokia/maps/ARLayoutControl;->removeARObject(Lcom/nokia/maps/ARPolylineObjectImpl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)F
    .locals 2

    .prologue
    .line 1898
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1899
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1900
    const/high16 v0, -0x40800000    # -1.0f

    .line 1902
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->getFilterCoeff(I)F

    move-result v0

    goto :goto_0
.end method

.method public c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 3

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2107
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2108
    sget-object v0, Lcom/here/android/mpa/ar/AnimationInterpolator;->LINEAR:Lcom/here/android/mpa/ar/AnimationInterpolator;

    .line 2111
    :goto_0
    return-object v0

    .line 2110
    :cond_0
    invoke-static {}, Lcom/here/android/mpa/ar/AnimationInterpolator;->values()[Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    .line 2111
    invoke-virtual {p1}, Lcom/nokia/maps/d$a;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/nokia/maps/ARLayoutControl;->getAnimationInterpolator(I)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 805
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 806
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 810
    :goto_0
    return-void

    .line 809
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->unselect()V

    goto :goto_0
.end method

.method public c(F)V
    .locals 2

    .prologue
    .line 861
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 866
    :goto_0
    return-void

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setUpViewPitchThreshold(F)V

    goto :goto_0
.end method

.method public c(II)V
    .locals 2

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1680
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1684
    :goto_0
    return-void

    .line 1683
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    new-instance v1, Lcom/here/android/mpa/common/Size;

    invoke-direct {v1, p1, p2}, Lcom/here/android/mpa/common/Size;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARLayoutControl;->setSelectedIconSize(Lcom/here/android/mpa/common/Size;)V

    goto :goto_0
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1952
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1956
    :goto_0
    return-void

    .line 1955
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setIntroAnimationTime(J)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 908
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 913
    :goto_0
    return-void

    .line 912
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setShowGridEnabled(Z)V

    goto :goto_0
.end method

.method public c(ZZ)V
    .locals 2

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1305
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1310
    :goto_0
    return-void

    .line 1308
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/d;->R:Z

    .line 1309
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoHeading(ZZ)V

    goto :goto_0
.end method

.method public c(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 568
    const/4 v0, 0x0

    .line 573
    :goto_0
    return v0

    .line 571
    :cond_0
    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v0

    .line 573
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1, v0}, Lcom/nokia/maps/ARLayoutControl;->isVisible(Lcom/nokia/maps/ARObjectImpl;)Z

    move-result v0

    goto :goto_0
.end method

.method public d()F
    .locals 2

    .prologue
    .line 850
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 852
    const/4 v0, 0x0

    .line 854
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getSelectedItemMaxViewAngle()F

    move-result v0

    goto :goto_0
.end method

.method public d(I)F
    .locals 2

    .prologue
    .line 2157
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2158
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2159
    const/high16 v0, -0x40800000    # -1.0f

    .line 2161
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->getFlyRotateDeg(I)F

    move-result v0

    goto :goto_0
.end method

.method public d(F)V
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 877
    :goto_0
    return-void

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setDownViewPitchThreshold(F)V

    goto :goto_0
.end method

.method public d(II)V
    .locals 1

    .prologue
    .line 1818
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/y;->a(II)V

    .line 1819
    return-void
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 1978
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1983
    :goto_0
    return-void

    .line 1982
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setTiltUpMaxTime(J)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 941
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 946
    :goto_0
    return-void

    .line 945
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->showFrontItemsOnly(Z)V

    goto :goto_0
.end method

.method public d(ZZ)V
    .locals 2

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1338
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1343
    :goto_0
    return-void

    .line 1341
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/d;->S:Z

    .line 1342
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoTfc(ZZ)V

    goto :goto_0
.end method

.method public d(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 582
    const/4 v0, 0x0

    .line 587
    :goto_0
    return v0

    .line 585
    :cond_0
    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v0

    .line 587
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1, v0}, Lcom/nokia/maps/ARLayoutControl;->isOccluded(Lcom/nokia/maps/ARObjectImpl;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()F
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 885
    const/4 v0, 0x0

    .line 887
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getUpViewPitchThreshold()F

    move-result v0

    goto :goto_0
.end method

.method public e(F)V
    .locals 2

    .prologue
    .line 1559
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1560
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1564
    :goto_0
    return-void

    .line 1563
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setCameraMaxZoomScaleUpView(F)V

    goto :goto_0
.end method

.method public e(II)V
    .locals 2

    .prologue
    .line 1858
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1859
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1863
    :goto_0
    return-void

    .line 1862
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setFilterSize(II)V

    goto :goto_0
.end method

.method public e(J)V
    .locals 3

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2006
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2010
    :goto_0
    return-void

    .line 2009
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setTiltUpMinTime(J)V

    goto :goto_0
.end method

.method public e(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 4

    .prologue
    .line 633
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 638
    :goto_0
    return-void

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARObject;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->focus(J)V

    goto :goto_0
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1222
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1226
    :goto_0
    return-void

    .line 1225
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoControlOnEntryExit(Z)V

    goto :goto_0
.end method

.method public e(ZZ)V
    .locals 2

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1371
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1376
    :goto_0
    return-void

    .line 1374
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/d;->T:Z

    .line 1375
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoGeoPosition(ZZ)V

    goto :goto_0
.end method

.method public f()F
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 895
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 896
    const/4 v0, 0x0

    .line 898
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getDownViewPitchThreshold()F

    move-result v0

    goto :goto_0
.end method

.method public f(F)V
    .locals 2

    .prologue
    .line 1762
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1763
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1768
    :goto_0
    return-void

    .line 1767
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setDownIconOpacity(F)V

    goto :goto_0
.end method

.method public f(J)V
    .locals 3

    .prologue
    .line 2369
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2370
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2374
    :goto_0
    return-void

    .line 2373
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setSensorsWaitTimeout(J)V

    goto :goto_0
.end method

.method public f(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 4

    .prologue
    .line 722
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 727
    :goto_0
    return-void

    .line 726
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nokia/maps/ARObjectImpl;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->press(J)V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1437
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1441
    :goto_0
    return-void

    .line 1440
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setPitchLockedUpView(Z)V

    goto :goto_0
.end method

.method public g(F)V
    .locals 2

    .prologue
    .line 2121
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2122
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2126
    :goto_0
    return-void

    .line 2125
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setInfoAnimationMinWidthFactor(F)V

    goto :goto_0
.end method

.method public g(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 4

    .prologue
    .line 734
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 739
    :goto_0
    return-void

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nokia/maps/ARObjectImpl;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->depress(J)V

    goto :goto_0
.end method

.method public g(Z)V
    .locals 2

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1463
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1467
    :goto_0
    return-void

    .line 1466
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setOcclusionEnabled(Z)V

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 921
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 922
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 923
    const/4 v0, 0x0

    .line 925
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->isShowGridEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public h()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1017
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1018
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1025
    :cond_0
    :goto_0
    return v0

    .line 1021
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1022
    if-eqz v1, :cond_0

    .line 1023
    invoke-virtual {v1}, Lcom/nokia/maps/ARSensors;->n()I

    move-result v0

    goto :goto_0
.end method

.method public h(F)V
    .locals 2

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2172
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2176
    :goto_0
    return-void

    .line 2175
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setDownViewMinOpacity(F)V

    goto :goto_0
.end method

.method public h(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 4

    .prologue
    .line 753
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 758
    :goto_0
    return-void

    .line 757
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-static {p1}, Lcom/nokia/maps/ARObjectImpl;->b(Lcom/here/android/mpa/ar/ARObject;)Lcom/nokia/maps/ARObjectImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nokia/maps/ARObjectImpl;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/nokia/maps/ARLayoutControl;->select(J)V

    goto :goto_0
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 1489
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    if-eqz v0, :cond_0

    .line 1490
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/y;->setPanEnabled(Z)V

    .line 1492
    :cond_0
    return-void
.end method

.method public i()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1038
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1039
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1046
    :cond_0
    :goto_0
    return v0

    .line 1042
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1043
    if-eqz v1, :cond_0

    .line 1044
    invoke-virtual {v1}, Lcom/nokia/maps/ARSensors;->m()I

    move-result v0

    goto :goto_0
.end method

.method public i(Lcom/here/android/mpa/ar/ARObject;)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 1793
    iget-object v2, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v2, :cond_1

    .line 1794
    iget-object v2, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v3, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v2, p0, v3}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1800
    :cond_0
    :goto_0
    return-wide v0

    .line 1797
    :cond_1
    if-eqz p1, :cond_0

    .line 1800
    invoke-virtual {p1}, Lcom/here/android/mpa/ar/ARObject;->getUid()J

    move-result-wide v0

    goto :goto_0
.end method

.method public i(F)V
    .locals 2

    .prologue
    .line 2198
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2199
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2203
    :goto_0
    return-void

    .line 2202
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setDownViewMaxOpacity(F)V

    goto :goto_0
.end method

.method public i(Z)V
    .locals 1

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    if-eqz v0, :cond_0

    .line 1511
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/y;->setPinchEnabled(Z)V

    .line 1513
    :cond_0
    return-void
.end method

.method public j()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1059
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1060
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1067
    :cond_0
    :goto_0
    return v0

    .line 1063
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1064
    if-eqz v1, :cond_0

    .line 1065
    invoke-virtual {v1}, Lcom/nokia/maps/ARSensors;->o()I

    move-result v0

    goto :goto_0
.end method

.method public j(F)V
    .locals 2

    .prologue
    .line 2252
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2253
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2257
    :goto_0
    return-void

    .line 2256
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setSelectedItemOpacity(F)V

    goto :goto_0
.end method

.method public j(Z)V
    .locals 2

    .prologue
    .line 1531
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1536
    :goto_0
    return-void

    .line 1535
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setCameraZoomEnabledUpView(Z)V

    goto :goto_0
.end method

.method public k()Lcom/here/android/mpa/ar/ARController$ViewType;
    .locals 1

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/nokia/maps/d;->f:Lcom/here/android/mpa/ar/ARController$ViewType;

    return-object v0
.end method

.method public k(F)V
    .locals 2

    .prologue
    .line 2279
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2280
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2284
    :goto_0
    return-void

    .line 2283
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setNonSelectedItemsOpacity(F)V

    goto :goto_0
.end method

.method public k(Z)V
    .locals 2

    .prologue
    .line 1733
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1734
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1739
    :goto_0
    return-void

    .line 1738
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setUseDownIconOpacity(Z)V

    goto :goto_0
.end method

.method public l()Lcom/here/android/mpa/ar/ARPoseReading;
    .locals 2

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1160
    const/4 v0, 0x0

    .line 1162
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->a()Lcom/here/android/mpa/ar/ARPoseReading;

    move-result-object v0

    goto :goto_0
.end method

.method public l(F)V
    .locals 2

    .prologue
    .line 2339
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2340
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2344
    :goto_0
    return-void

    .line 2343
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setMinPitchDownView(F)V

    goto :goto_0
.end method

.method public l(Z)V
    .locals 2

    .prologue
    .line 2455
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2456
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2460
    :goto_0
    return-void

    .line 2459
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setInfoAnimationInUpViewOnly(Z)V

    goto :goto_0
.end method

.method public m(F)V
    .locals 2

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2430
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2434
    :goto_0
    return-void

    .line 2433
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setOcclusionOpacity(F)V

    goto :goto_0
.end method

.method public m(Z)V
    .locals 2

    .prologue
    .line 2470
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 2471
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2481
    :goto_0
    return-void

    .line 2475
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->OPERATION_NOT_ALLOWED:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2480
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->a(Z)V

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 1252
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1253
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1254
    const/4 v0, 0x0

    .line 1258
    :goto_0
    return v0

    .line 1257
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMapAutoZoom()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/d;->P:Z

    .line 1258
    iget-boolean v0, p0, Lcom/nokia/maps/d;->P:Z

    goto :goto_0
.end method

.method public n(F)V
    .locals 0

    .prologue
    .line 3656
    invoke-static {p1}, Lcom/nokia/maps/ARSensors;->a(F)V

    .line 3657
    return-void
.end method

.method public n(Z)V
    .locals 2

    .prologue
    .line 3778
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 3779
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 3784
    :goto_0
    return-void

    .line 3783
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARLayoutControl;->setEdgeDetectionEnabled(Z)V

    goto :goto_0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1287
    const/4 v0, 0x0

    .line 1291
    :goto_0
    return v0

    .line 1290
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMapAutoPitch()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/d;->Q:Z

    .line 1291
    iget-boolean v0, p0, Lcom/nokia/maps/d;->Q:Z

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1320
    const/4 v0, 0x0

    .line 1324
    :goto_0
    return v0

    .line 1323
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMapAutoHeading()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/d;->R:Z

    .line 1324
    iget-boolean v0, p0, Lcom/nokia/maps/d;->R:Z

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1352
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1353
    const/4 v0, 0x0

    .line 1357
    :goto_0
    return v0

    .line 1356
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMapAutoTfc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/d;->S:Z

    .line 1357
    iget-boolean v0, p0, Lcom/nokia/maps/d;->S:Z

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1385
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1386
    const/4 v0, 0x0

    .line 1390
    :goto_0
    return v0

    .line 1389
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getMapAutoGeoCenter()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/d;->T:Z

    .line 1390
    iget-boolean v0, p0, Lcom/nokia/maps/d;->T:Z

    goto :goto_0
.end method

.method public r()Lcom/nokia/maps/GeoCoordinateImpl;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1400
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1401
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1408
    :cond_0
    :goto_0
    return-object v0

    .line 1404
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1405
    if-eqz v1, :cond_0

    .line 1406
    invoke-virtual {v1}, Lcom/nokia/maps/ARSensors;->k()Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1448
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1449
    const/4 v0, 0x0

    .line 1451
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->isPitchLockedUpView()Z

    move-result v0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1476
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1477
    const/4 v0, 0x0

    .line 1479
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->isOcclusionEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/d;->n:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 1544
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1545
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1546
    const/4 v0, 0x0

    .line 1548
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->isCameraZoomEnabledUpView()Z

    move-result v0

    goto :goto_0
.end method

.method public x()F
    .locals 2

    .prologue
    .line 1572
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1573
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1574
    const/high16 v0, -0x40800000    # -1.0f

    .line 1576
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getCameraMaxZoomScaleUpView()F

    move-result v0

    goto :goto_0
.end method

.method public y()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1597
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v1, :cond_1

    .line 1598
    iget-object v1, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v2, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v1, p0, v2}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1605
    :cond_0
    :goto_0
    return v0

    .line 1601
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v1}, Lcom/nokia/maps/ARLayoutControl;->f()Lcom/nokia/maps/ARSensors;

    move-result-object v1

    .line 1602
    if-eqz v1, :cond_0

    .line 1603
    invoke-virtual {v1}, Lcom/nokia/maps/ARSensors;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public z()Lcom/here/android/mpa/common/Size;
    .locals 2

    .prologue
    .line 1694
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    if-nez v0, :cond_0

    .line 1695
    iget-object v0, p0, Lcom/nokia/maps/d;->ap:Lcom/nokia/maps/aw$d;

    sget-object v1, Lcom/here/android/mpa/ar/ARController$Error;->INVALID_OPERATION:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-interface {v0, p0, v1}, Lcom/nokia/maps/aw$d;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1696
    const/4 v0, 0x0

    .line 1698
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/d;->l:Lcom/nokia/maps/ARLayoutControl;

    invoke-virtual {v0}, Lcom/nokia/maps/ARLayoutControl;->getSelectedIconSize()Lcom/here/android/mpa/common/Size;

    move-result-object v0

    goto :goto_0
.end method
