.class public Lcom/nokia/maps/LocationImpl;
.super Lcom/nokia/maps/BaseNativeObject;
.source "LocationImpl.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation


# static fields
.field private static a:Lcom/nokia/maps/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nokia/maps/ar",
            "<",
            "Lcom/here/android/mpa/mapping/Location;",
            "Lcom/nokia/maps/LocationImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/LocationImpl;->a:Lcom/nokia/maps/ar;

    .line 40
    const-class v0, Lcom/here/android/mpa/mapping/Location;

    invoke-static {v0}, Lcom/nokia/maps/ck;->a(Ljava/lang/Class;)V

    .line 41
    return-void
.end method

.method protected constructor <init>()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nokia/maps/BaseNativeObject;-><init>(Z)V

    .line 57
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nokia/maps/BaseNativeObject;-><init>(Z)V

    .line 51
    iput p1, p0, Lcom/nokia/maps/LocationImpl;->nativeptr:I

    .line 52
    return-void
.end method

.method static a(Lcom/nokia/maps/LocationImpl;)Lcom/here/android/mpa/mapping/Location;
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    if-eqz p0, :cond_0

    .line 30
    sget-object v0, Lcom/nokia/maps/LocationImpl;->a:Lcom/nokia/maps/ar;

    invoke-interface {v0, p0}, Lcom/nokia/maps/ar;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/Location;

    .line 32
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/nokia/maps/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nokia/maps/ar",
            "<",
            "Lcom/here/android/mpa/mapping/Location;",
            "Lcom/nokia/maps/LocationImpl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    sput-object p0, Lcom/nokia/maps/LocationImpl;->a:Lcom/nokia/maps/ar;

    .line 37
    return-void
.end method

.method private final native destroyLocationNative()V
.end method

.method private final native getBoundingBoxNative()Lcom/nokia/maps/GeoBoundingBoxImpl;
.end method

.method private final native getCoordinateNative()Lcom/nokia/maps/GeoCoordinateImpl;
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/nokia/maps/LocationImpl;->destroyLocationNative()V

    .line 66
    return-void
.end method

.method public final b()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/nokia/maps/LocationImpl;->getCoordinateNative()Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->create(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/nokia/maps/LocationImpl;->getBoundingBoxNative()Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoBoundingBoxImpl;->create(Lcom/nokia/maps/GeoBoundingBoxImpl;)Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/here/android/mpa/mapping/LocationInfo;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/nokia/maps/LocationImpl;->getInfoNative()Lcom/nokia/maps/LocationInfoImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/LocationInfoImpl;->a(Lcom/nokia/maps/LocationInfoImpl;)Lcom/here/android/mpa/mapping/LocationInfo;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/nokia/maps/LocationImpl;->a()V

    .line 62
    return-void
.end method

.method public final native getInfoNative()Lcom/nokia/maps/LocationInfoImpl;
.end method

.method public final native isValid()Z
.end method
