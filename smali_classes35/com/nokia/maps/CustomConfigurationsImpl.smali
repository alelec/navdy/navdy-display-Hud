.class public Lcom/nokia/maps/CustomConfigurationsImpl;
.super Ljava/lang/Object;
.source "CustomConfigurationsImpl.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation


# static fields
.field private static a:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/here/android/mpa/common/CustomConfigurations$Config;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/nokia/maps/CustomConfigurationsImpl;->a:Ljava/util/Hashtable;

    .line 64
    new-instance v0, Lcom/nokia/maps/CustomConfigurationsImpl$1;

    invoke-direct {v0}, Lcom/nokia/maps/CustomConfigurationsImpl$1;-><init>()V

    invoke-static {v0}, Lcom/nokia/maps/MapsEngine;->a(Lcom/nokia/maps/MapsEngine$l;)V

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/nokia/maps/CustomConfigurationsImpl;->a:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-static {p0}, Lcom/nokia/maps/CustomConfigurationsImpl;->setMapConfigFile(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/util/Hashtable;)V
    .locals 6
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/here/android/mpa/common/CustomConfigurations$Config;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Map engine already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/Hashtable;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Config files shouldn\'t be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_2
    sget-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    invoke-virtual {p0, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    sget-object v1, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP_RESOURCE:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    .line 40
    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Config files of type MAP and MAP_RESOURCE must be set together"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_3
    invoke-virtual {p0}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 47
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 48
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Config file name is empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_5
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    .line 54
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "Can\'t find configuration file \'%s\' for %s configuration type"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 56
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v4

    .line 55
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 60
    :cond_6
    sget-object v0, Lcom/nokia/maps/CustomConfigurationsImpl;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    .line 61
    return-void
.end method

.method static synthetic b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-static {p0}, Lcom/nokia/maps/CustomConfigurationsImpl;->setResourceConfigFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-static {p0}, Lcom/nokia/maps/CustomConfigurationsImpl;->setTrafficConfigFile(Ljava/lang/String;)V

    return-void
.end method

.method private static native setMapConfigFile(Ljava/lang/String;)V
.end method

.method private static native setResourceConfigFile(Ljava/lang/String;)V
.end method

.method private static native setTrafficConfigFile(Ljava/lang/String;)V
.end method
