.class public Lcom/nokia/maps/PlatformDataRequestImpl;
.super Lcom/nokia/maps/BaseNativeObject;
.source "PlatformDataRequestImpl.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/PlatformDataRequestImpl$a;
    }
.end annotation


# static fields
.field private static a:Lcom/nokia/maps/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/pde/PlatformDataRequest;",
            "Lcom/nokia/maps/PlatformDataRequestImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/nokia/maps/PlatformDataRequestImpl$a;

.field private static final e:Lcom/nokia/maps/PlatformDataRequestImpl$a;


# instance fields
.field private final b:Lcom/nokia/maps/PlatformDataRequestImpl$a;

.field private c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/PlatformDataRequestImpl;->a:Lcom/nokia/maps/m;

    .line 46
    const-class v0, Lcom/here/android/mpa/pde/PlatformDataRequest;

    invoke-static {v0}, Lcom/nokia/maps/ck;->a(Ljava/lang/Class;)V

    .line 125
    new-instance v0, Lcom/nokia/maps/PlatformDataRequestImpl$2;

    invoke-direct {v0}, Lcom/nokia/maps/PlatformDataRequestImpl$2;-><init>()V

    sput-object v0, Lcom/nokia/maps/PlatformDataRequestImpl;->d:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    .line 133
    new-instance v0, Lcom/nokia/maps/PlatformDataRequestImpl$3;

    invoke-direct {v0}, Lcom/nokia/maps/PlatformDataRequestImpl$3;-><init>()V

    sput-object v0, Lcom/nokia/maps/PlatformDataRequestImpl;->e:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Lcom/here/android/mpa/common/GeoBoundingBox;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/here/android/mpa/common/GeoBoundingBox;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/nokia/maps/BaseNativeObject;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->c:Landroid/os/Handler;

    .line 68
    invoke-static {}, Lcom/nokia/maps/BaseNativeObject;->u()V

    .line 69
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid arguments passed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 73
    invoke-static {p2}, Lcom/nokia/maps/GeoBoundingBoxImpl;->get(Lcom/here/android/mpa/common/GeoBoundingBox;)Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v1

    .line 72
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/PlatformDataRequestImpl;->createBoundingBoxRequestNative([Ljava/lang/String;Lcom/nokia/maps/GeoBoundingBoxImpl;)V

    .line 74
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->a()V

    .line 75
    sget-object v0, Lcom/nokia/maps/PlatformDataRequestImpl;->e:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    iput-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->b:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/nokia/maps/BaseNativeObject;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->c:Landroid/os/Handler;

    .line 51
    invoke-static {}, Lcom/nokia/maps/BaseNativeObject;->u()V

    .line 52
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid arguments passed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 56
    const/4 v0, 0x0

    .line 57
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 58
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 59
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 60
    goto :goto_0

    .line 61
    :cond_2
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/nokia/maps/PlatformDataRequestImpl;->createLinkIdsRequestNative([Ljava/lang/String;[J)V

    .line 62
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->a()V

    .line 63
    sget-object v0, Lcom/nokia/maps/PlatformDataRequestImpl;->d:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    iput-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->b:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/PlatformDataRequestImpl;)Lcom/nokia/maps/PlatformDataResultImpl;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->executeNative()Lcom/nokia/maps/PlatformDataResultImpl;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->hasPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/security/AccessControlException;

    const-string v1, "Access to this operation is denied. Contact your HERE representative for more information."

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid arguments passed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    return-void
.end method

.method public static a(Lcom/nokia/maps/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/pde/PlatformDataRequest;",
            "Lcom/nokia/maps/PlatformDataRequestImpl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    sput-object p0, Lcom/nokia/maps/PlatformDataRequestImpl;->a:Lcom/nokia/maps/m;

    .line 39
    return-void
.end method

.method static synthetic b(Lcom/nokia/maps/PlatformDataRequestImpl;)Lcom/nokia/maps/PlatformDataRequestImpl$a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->b:Lcom/nokia/maps/PlatformDataRequestImpl$a;

    return-object v0
.end method

.method static synthetic c(Lcom/nokia/maps/PlatformDataRequestImpl;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->c:Landroid/os/Handler;

    return-object v0
.end method

.method private native createBoundingBoxRequestNative([Ljava/lang/String;Lcom/nokia/maps/GeoBoundingBoxImpl;)V
.end method

.method private native createLinkIdsRequestNative([Ljava/lang/String;[J)V
.end method

.method private native destroyPlatformDataRequestNative()V
.end method

.method private native executeNative()Lcom/nokia/maps/PlatformDataResultImpl;
.end method

.method private native hasPermission()Z
.end method

.method private native isValid()Z
.end method


# virtual methods
.method public a(Lcom/here/android/mpa/pde/PlatformDataRequest$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/pde/PlatformDataRequest$Listener",
            "<",
            "Lcom/here/android/mpa/pde/PlatformDataResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lcom/nokia/maps/PlatformDataRequestImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/nokia/maps/PlatformDataRequestImpl$1;-><init>(Lcom/nokia/maps/PlatformDataRequestImpl;Lcom/here/android/mpa/pde/PlatformDataRequest$Listener;)V

    .line 107
    invoke-virtual {v0}, Lcom/nokia/maps/PlatformDataRequestImpl$1;->start()V

    .line 108
    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/nokia/maps/PlatformDataRequestImpl;->nativeptr:I

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/nokia/maps/PlatformDataRequestImpl;->destroyPlatformDataRequestNative()V

    .line 117
    :cond_0
    return-void
.end method
