.class Lcom/nokia/maps/s$3;
.super Lcom/nokia/maps/MapsEngine$k$a;
.source "AnalyticsTrackerExternal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nokia/maps/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nokia/maps/s;


# direct methods
.method constructor <init>(Lcom/nokia/maps/s;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine$k$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 685
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/nokia/maps/s$3;->a(Ljava/lang/String;Z)V

    .line 686
    return-void

    .line 685
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 660
    if-nez p2, :cond_0

    .line 681
    :goto_0
    return-void

    .line 663
    :cond_0
    iget-object v1, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    monitor-enter v1

    .line 664
    :try_start_0
    invoke-static {p1}, Lcom/nokia/maps/s;->h(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    .line 668
    invoke-static {v0}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 671
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->b(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->b(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    .line 673
    invoke-static {v0}, Lcom/nokia/maps/s;->b(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 674
    :cond_3
    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->b(Lcom/nokia/maps/s;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "mapVersion"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 676
    :cond_4
    iget-object v0, p0, Lcom/nokia/maps/s$3;->a:Lcom/nokia/maps/s;

    invoke-static {v0}, Lcom/nokia/maps/s;->c(Lcom/nokia/maps/s;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 680
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 677
    :catch_0
    move-exception v0

    .line 678
    :try_start_3
    invoke-static {}, Lcom/nokia/maps/s;->l()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Unable to track map version"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
