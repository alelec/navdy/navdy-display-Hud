.class public abstract Lcom/nokia/maps/MapServiceClient;
.super Lcom/nokia/maps/bf$a;
.source "MapServiceClient.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/MapServiceClient$a;
    }
.end annotation


# static fields
.field static a:Ljava/lang/String;

.field static b:Z

.field static c:I


# instance fields
.field d:J

.field e:Z

.field f:Z

.field protected g:I

.field private h:Lcom/nokia/maps/bd;

.field private i:Landroid/content/ServiceConnection;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "com.here.android.mpa.service.MapService"

    sput-object v0, Lcom/nokia/maps/MapServiceClient;->a:Ljava/lang/String;

    .line 36
    const/4 v0, 0x1

    sput-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    .line 37
    const/4 v0, -0x1

    sput v0, Lcom/nokia/maps/MapServiceClient;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/nokia/maps/bf$a;-><init>()V

    .line 35
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    .line 38
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/nokia/maps/MapServiceClient;->d:J

    .line 39
    iput-boolean v0, p0, Lcom/nokia/maps/MapServiceClient;->e:Z

    .line 40
    iput-boolean v0, p0, Lcom/nokia/maps/MapServiceClient;->f:Z

    .line 116
    iput v0, p0, Lcom/nokia/maps/MapServiceClient;->g:I

    .line 117
    new-instance v1, Lcom/nokia/maps/MapServiceClient$1;

    invoke-direct {v1, p0}, Lcom/nokia/maps/MapServiceClient$1;-><init>(Lcom/nokia/maps/MapServiceClient;)V

    iput-object v1, p0, Lcom/nokia/maps/MapServiceClient;->i:Landroid/content/ServiceConnection;

    .line 186
    iput-boolean v0, p0, Lcom/nokia/maps/MapServiceClient;->j:Z

    .line 43
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/nokia/maps/MapServiceClient;->f:Z

    .line 44
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 249
    invoke-static {p0}, Lcom/nokia/maps/eg;->b(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v8

    .line 250
    if-eqz p10, :cond_0

    if-eqz p11, :cond_0

    .line 252
    const/4 v0, 0x0

    aput-object p10, v8, v0

    .line 253
    const/4 v0, 0x1

    aput-object p11, v8, v0

    .line 255
    :cond_0
    const-class v0, Lcom/nokia/maps/MapServiceClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service using: appId=%s, appToken=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-object v4, v8, v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v8, v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-object/from16 v9, p9

    .line 258
    invoke-static/range {v0 .. v9}, Lcom/nokia/maps/MapServiceClient;->startServer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISZ[Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    return-void
.end method

.method static b()Z
    .locals 2

    .prologue
    .line 263
    const-string v0, "com.here.android.mpa.service.MapService"

    sget-object v1, Lcom/nokia/maps/MapServiceClient;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static native setMapServiceOnline(Z)Z
.end method

.method public static native setMapServiceProxy(Ljava/lang/String;)V
.end method

.method public static native setUniqueDeviceId(Ljava/lang/String;)V
.end method

.method private static native startServer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISZ[Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native stopServer()V
.end method


# virtual methods
.method protected a(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    .line 147
    return-void
.end method

.method protected a(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8

    .prologue
    .line 131
    invoke-static {p2}, Lcom/nokia/maps/bd$a;->a(Landroid/os/IBinder;)Lcom/nokia/maps/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    .line 133
    iget-object v0, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    if-eqz v0, :cond_0

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    invoke-interface {v0}, Lcom/nokia/maps/bd;->c()I

    move-result v0

    iput v0, p0, Lcom/nokia/maps/MapServiceClient;->g:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    const-class v0, Lcom/nokia/maps/MapServiceClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ComponentName=%s - %dms after start"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/nokia/maps/MapServiceClient;->d:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 140
    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    return-void

    .line 136
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/nokia/maps/MapServiceClient;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    const-string v1, "nukeservice"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 102
    if-nez v1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 108
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 112
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;S)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 50
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/nokia/maps/MapServiceClient;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    const-string v1, "mapdataserverurl"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v1, "mapsatelliteserverurl"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, "terrainserverurl"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v1, "diskcachepath"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v1, "sliserverurl"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v1, "mapvariant"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;S)Landroid/content/Intent;

    .line 57
    const-string v1, "USESSL"

    sget-boolean v2, Lcom/nokia/maps/MapServiceClient;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    invoke-static {}, Lcom/nokia/maps/MapSettings;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const-string v1, "isolated_diskcache_enabled"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 60
    const-string v1, "client_app_id"

    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nokia/maps/ApplicationContext;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "client_app_token"

    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nokia/maps/ApplicationContext;->getAppToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :cond_0
    sget v1, Lcom/nokia/maps/MapServiceClient;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 65
    const-string v1, "shutdownmode"

    sget v2, Lcom/nokia/maps/MapServiceClient;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 69
    if-nez v1, :cond_2

    .line 70
    new-instance v0, Lcom/nokia/maps/MapServiceClient$a;

    invoke-direct {v0}, Lcom/nokia/maps/MapServiceClient$a;-><init>()V

    throw v0

    .line 74
    :cond_2
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 75
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, v1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    iget-object v2, p0, Lcom/nokia/maps/MapServiceClient;->i:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0, v2, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 81
    if-nez v0, :cond_3

    .line 82
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unable to start map service"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_4

    .line 86
    iget v0, v1, Landroid/content/pm/ServiceInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 87
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Service must be set to stopWithTask=false"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/nokia/maps/MapServiceClient;->d:J

    .line 96
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/nokia/maps/MapServiceClient;->j:Z

    return v0
.end method

.method public a(Lcom/nokia/maps/be;)Z
    .locals 2

    .prologue
    .line 161
    const/4 v0, 0x1

    .line 162
    iget-object v1, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    if-eqz v1, :cond_0

    .line 164
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    invoke-interface {v1, p1}, Lcom/nokia/maps/bd;->a(Lcom/nokia/maps/be;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :cond_0
    :goto_0
    return v0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 167
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Z)Z
    .locals 2

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 224
    iget-object v1, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    if-eqz v1, :cond_0

    .line 226
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/MapServiceClient;->h:Lcom/nokia/maps/bd;

    invoke-interface {v1, p1}, Lcom/nokia/maps/bd;->a(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 227
    :catch_0
    move-exception v1

    goto :goto_0
.end method
