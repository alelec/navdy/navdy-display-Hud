.class public Lcom/nokia/maps/s;
.super Ljava/lang/Object;
.source "AnalyticsTrackerExternal.java"

# interfaces
.implements Lcom/nokia/maps/r;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/s$a;,
        Lcom/nokia/maps/s$b;
    }
.end annotation


# static fields
.field private static F:Ljava/lang/String;

.field static final synthetic a:Z

.field private static final l:Ljava/lang/String;

.field private static final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Lcom/nokia/maps/ApplicationContext$c;

.field private final B:Lcom/nokia/maps/ApplicationContext$c;

.field private final C:Lcom/nokia/maps/ApplicationContext$c;

.field private final D:Lcom/nokia/maps/ApplicationContext$c;

.field private final E:Lcom/nokia/maps/ApplicationContext$c;

.field private G:Lcom/nokia/maps/MapsEngine$k;

.field private final H:Ljava/lang/Object;

.field private final I:Ljava/lang/Runnable;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private m:Lorg/json/JSONObject;

.field private n:Lorg/json/JSONObject;

.field private o:Lorg/json/JSONObject;

.field private p:Landroid/content/Context;

.field private q:I

.field private r:Ljava/util/Date;

.field private s:Ljava/util/Date;

.field private t:Z

.field private final v:Lcom/nokia/maps/ApplicationContext$c;

.field private final w:Lcom/nokia/maps/ApplicationContext$c;

.field private final x:Lcom/nokia/maps/ApplicationContext$c;

.field private final y:Lcom/nokia/maps/ApplicationContext$c;

.field private final z:Lcom/nokia/maps/ApplicationContext$c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 71
    const-class v0, Lcom/nokia/maps/s;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/nokia/maps/s;->a:Z

    .line 303
    const-class v0, Lcom/nokia/maps/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    .line 319
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    .line 321
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-online-sli"

    const-string v2, "sliUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-hybrid-sli"

    const-string v2, "sliUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-offline-sli"

    const-string v2, "sliUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-online-ar"

    const-string v2, "arUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-hybrid-ar"

    const-string v2, "arUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "display-offline-ar"

    const-string v2, "arUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-online-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-hybrid-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-offline-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-online-car-traffic"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-hybrid-car-traffic"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-online-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-hybrid-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-offline-car"

    const-string v2, "basicNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "advanced-guidance-online-car"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "advanced-guidance-hybrid-car"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "advanced-guidance-offline-car"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "advanced-guidance-online-car-traffic"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "advanced-guidance-hybrid-car-traffic"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "traffic-update-route"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "traffic-update-route-elements"

    const-string v2, "advNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-online-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-hybrid-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "guidance-offline-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-online-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-hybrid-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "nav_tracking-offline-pedestrian"

    const-string v2, "walkNavUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "fleetMapCount"

    const-string v2, "mamUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "truckAttributesCount"

    const-string v2, "mamUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "congestionZonesCount"

    const-string v2, "mamUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "historicalSpeedPatternCount"

    const-string v2, "mamUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    const-string v1, "routing-online-truck"

    const-string v2, "mamUsageCount"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    const-string v0, ""

    sput-object v0, Lcom/nokia/maps/s;->F:Ljava/lang/String;

    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    iput-boolean v0, p0, Lcom/nokia/maps/s;->b:Z

    .line 287
    iput-boolean v0, p0, Lcom/nokia/maps/s;->c:Z

    .line 289
    iput-boolean v0, p0, Lcom/nokia/maps/s;->d:Z

    .line 290
    iput-boolean v0, p0, Lcom/nokia/maps/s;->e:Z

    .line 291
    iput-boolean v0, p0, Lcom/nokia/maps/s;->f:Z

    .line 293
    iput-boolean v0, p0, Lcom/nokia/maps/s;->g:Z

    .line 294
    iput-boolean v0, p0, Lcom/nokia/maps/s;->h:Z

    .line 295
    iput-boolean v0, p0, Lcom/nokia/maps/s;->i:Z

    .line 296
    iput-boolean v0, p0, Lcom/nokia/maps/s;->j:Z

    .line 298
    iput-boolean v0, p0, Lcom/nokia/maps/s;->k:Z

    .line 310
    iput v0, p0, Lcom/nokia/maps/s;->q:I

    .line 314
    iput-boolean v0, p0, Lcom/nokia/maps/s;->t:Z

    .line 459
    new-instance v0, Lcom/nokia/maps/s$1;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$1;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->v:Lcom/nokia/maps/ApplicationContext$c;

    .line 471
    new-instance v0, Lcom/nokia/maps/s$6;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$6;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->w:Lcom/nokia/maps/ApplicationContext$c;

    .line 483
    new-instance v0, Lcom/nokia/maps/s$7;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$7;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->x:Lcom/nokia/maps/ApplicationContext$c;

    .line 495
    new-instance v0, Lcom/nokia/maps/s$8;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$8;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->y:Lcom/nokia/maps/ApplicationContext$c;

    .line 507
    new-instance v0, Lcom/nokia/maps/s$9;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$9;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->z:Lcom/nokia/maps/ApplicationContext$c;

    .line 519
    new-instance v0, Lcom/nokia/maps/s$10;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$10;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->A:Lcom/nokia/maps/ApplicationContext$c;

    .line 531
    new-instance v0, Lcom/nokia/maps/s$11;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$11;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->B:Lcom/nokia/maps/ApplicationContext$c;

    .line 543
    new-instance v0, Lcom/nokia/maps/s$12;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$12;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->C:Lcom/nokia/maps/ApplicationContext$c;

    .line 555
    new-instance v0, Lcom/nokia/maps/s$13;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$13;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->D:Lcom/nokia/maps/ApplicationContext$c;

    .line 567
    new-instance v0, Lcom/nokia/maps/s$2;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$2;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->E:Lcom/nokia/maps/ApplicationContext$c;

    .line 656
    new-instance v0, Lcom/nokia/maps/s$3;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$3;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->G:Lcom/nokia/maps/MapsEngine$k;

    .line 2419
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->H:Ljava/lang/Object;

    .line 2420
    new-instance v0, Lcom/nokia/maps/s$4;

    invoke-direct {v0, p0}, Lcom/nokia/maps/s$4;-><init>(Lcom/nokia/maps/s;)V

    iput-object v0, p0, Lcom/nokia/maps/s;->I:Ljava/lang/Runnable;

    .line 394
    return-void
.end method

.method private a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 790
    sget-object v0, Lcom/nokia/maps/s$5;->a:[I

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 802
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 792
    :pswitch_0
    const-string v0, "car"

    goto :goto_0

    .line 794
    :pswitch_1
    const-string v0, "truck"

    goto :goto_0

    .line 796
    :pswitch_2
    const-string v0, "pedestrian"

    goto :goto_0

    .line 798
    :pswitch_3
    const-string v0, "transit"

    goto :goto_0

    .line 800
    :pswitch_4
    const-string v0, "bike"

    goto :goto_0

    .line 790
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lcom/nokia/maps/PlacesConstants$b;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1172
    const/4 v0, 0x0

    .line 1174
    sget-object v1, Lcom/nokia/maps/s$5;->c:[I

    invoke-virtual {p1}, Lcom/nokia/maps/PlacesConstants$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1205
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1206
    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v0

    .line 1209
    :cond_1
    return-object v0

    .line 1176
    :pswitch_0
    iget-boolean v1, p0, Lcom/nokia/maps/s;->g:Z

    if-nez v1, :cond_0

    .line 1177
    const-string v0, "online"

    goto :goto_0

    .line 1181
    :pswitch_1
    iget-boolean v1, p0, Lcom/nokia/maps/s;->h:Z

    if-nez v1, :cond_0

    .line 1182
    const-string v0, "online"

    goto :goto_0

    .line 1188
    :pswitch_2
    iget-boolean v1, p0, Lcom/nokia/maps/s;->i:Z

    if-nez v1, :cond_0

    .line 1189
    const-string v0, "online"

    goto :goto_0

    .line 1193
    :pswitch_3
    iget-boolean v1, p0, Lcom/nokia/maps/s;->j:Z

    if-nez v1, :cond_0

    .line 1194
    const-string v0, "online"

    goto :goto_0

    .line 1199
    :pswitch_4
    const-string v0, "online"

    goto :goto_0

    .line 1174
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lorg/json/JSONArray;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2321
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2322
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2323
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2324
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2326
    :cond_0
    return-object v1
.end method

.method static synthetic a(Lcom/nokia/maps/s;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    return-object v0
.end method

.method private a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Ljava/lang/String;Ljava/lang/String;DZ)V
    .locals 4

    .prologue
    .line 1402
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1404
    :cond_0
    const/4 v0, 0x0

    .line 1406
    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "routing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    .line 1407
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    if-nez p2, :cond_2

    const-string v1, ""

    .line 1408
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    if-nez p2, :cond_1

    const-string p2, ""

    .line 1409
    :cond_1
    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1411
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1412
    if-eqz p6, :cond_3

    .line 1413
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1418
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1423
    :goto_2
    return-void

    .line 1407
    :cond_2
    const-string v1, "-"

    goto :goto_0

    .line 1415
    :cond_3
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1416
    const-string v2, "distance"

    invoke-direct {p0, v1, v2, p4, p5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1420
    :catch_0
    move-exception v1

    .line 1421
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 808
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 810
    :cond_0
    const/4 v0, 0x0

    .line 812
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/s;->g(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)Ljava/lang/String;

    move-result-object v0

    .line 813
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 814
    invoke-direct {p0, v1, p3}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 815
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820
    :goto_0
    return-void

    .line 817
    :catch_0
    move-exception v1

    .line 818
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/nokia/maps/PlacesConstants$b;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 1214
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1216
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1218
    sget-object v1, Lcom/nokia/maps/s$5;->c:[I

    invoke-virtual {p1}, Lcom/nokia/maps/PlacesConstants$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1269
    :goto_0
    return-void

    .line 1221
    :pswitch_0
    const-string v1, "reverse-geocoding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1261
    :goto_1
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/PlacesConstants$b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1263
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1264
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1265
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1268
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4}, Lcom/nokia/maps/s;->a(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 1224
    :pswitch_1
    const-string v1, "geocoding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1227
    :pswitch_2
    const-string v1, "place-details"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1231
    :pswitch_3
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1232
    const-string p2, "search"

    goto :goto_1

    .line 1236
    :pswitch_4
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1237
    const-string p2, "suggestion"

    goto :goto_1

    .line 1240
    :pswitch_5
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1241
    const-string p2, "explore"

    goto :goto_1

    .line 1244
    :pswitch_6
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1245
    const-string p2, "around"

    goto :goto_1

    .line 1248
    :pswitch_7
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1249
    const-string p2, "here"

    goto :goto_1

    .line 1252
    :pswitch_8
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1253
    const-string p2, "discovery"

    goto :goto_1

    .line 1218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    .locals 4

    .prologue
    .line 1459
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1462
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "odml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1464
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1466
    sget-object v2, Lcom/nokia/maps/s$5;->e:[I

    invoke-virtual {p2}, Lcom/here/android/mpa/odml/MapLoader$ResultCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1476
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1480
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1485
    :goto_1
    return-void

    .line 1469
    :pswitch_0
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1482
    :catch_0
    move-exception v0

    .line 1483
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t track odml-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1472
    :pswitch_1
    :try_start_1
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1473
    const-string v2, "noResultsCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1466
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1915
    invoke-direct {p0}, Lcom/nokia/maps/s;->o()V

    .line 1918
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1921
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    .line 1922
    iget v0, p0, Lcom/nokia/maps/s;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/nokia/maps/s;->q:I

    .line 1923
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nokia/maps/s;->t:Z

    .line 1925
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->i(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1926
    invoke-direct {p0, p2, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 1929
    const-string v1, "last"

    invoke-static {}, Lcom/nokia/maps/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1931
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v1, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1936
    invoke-direct {p0}, Lcom/nokia/maps/s;->r()V

    .line 1938
    invoke-direct {p0}, Lcom/nokia/maps/s;->t()V

    .line 1939
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1534
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1536
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "pde"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "online"

    .line 1537
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1540
    :try_start_2
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1542
    if-eqz p2, :cond_1

    .line 1543
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1547
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1551
    :goto_1
    monitor-exit p0

    return-void

    .line 1545
    :cond_1
    :try_start_3
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1548
    :catch_0
    move-exception v1

    .line 1549
    :try_start_4
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 1273
    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1274
    if-eqz p2, :cond_0

    .line 1275
    const-string v1, "errors"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1283
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1288
    :goto_1
    return-void

    .line 1276
    :cond_0
    if-eqz p3, :cond_1

    .line 1277
    const-string v1, "count"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1285
    :catch_0
    move-exception v0

    .line 1286
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t track "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1279
    :cond_1
    :try_start_1
    const-string v1, "count"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1280
    const-string v1, "noResultsCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2430
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 2431
    return-void
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;D)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2452
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2453
    invoke-virtual {p1, p2, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2457
    :goto_0
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->o(Ljava/lang/String;)V

    .line 2458
    return-void

    .line 2455
    :cond_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    add-double/2addr v0, p3

    invoke-virtual {p1, p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2434
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2435
    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2439
    :goto_0
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->o(Ljava/lang/String;)V

    .line 2440
    return-void

    .line 2437
    :cond_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, p3

    invoke-virtual {p1, p2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2443
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2444
    invoke-virtual {p1, p2, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2448
    :goto_0
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->o(Ljava/lang/String;)V

    .line 2449
    return-void

    .line 2446
    :cond_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    add-long/2addr v0, p3

    invoke-virtual {p1, p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 707
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 708
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 709
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 710
    const-string v1, "staged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 712
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 714
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 715
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 717
    instance-of v4, v1, Lorg/json/JSONObject;

    if-eqz v4, :cond_1

    move-object v0, v1

    .line 718
    check-cast v0, Lorg/json/JSONObject;

    move-object v1, v2

    check-cast v1, Lorg/json/JSONObject;

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 720
    :cond_1
    instance-of v4, v1, Lorg/json/JSONArray;

    if-nez v4, :cond_0

    .line 723
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 725
    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_0

    .line 727
    :cond_2
    instance-of v4, v1, Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 729
    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {p2, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    goto :goto_0

    .line 731
    :cond_3
    instance-of v4, v1, Ljava/lang/Double;

    if-eqz v4, :cond_4

    .line 733
    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-virtual {p2, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    goto :goto_0

    .line 735
    :cond_4
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 736
    const-string v2, "startTime"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 738
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_0

    .line 742
    :cond_5
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_0

    .line 746
    :cond_6
    return-void
.end method

.method private a(ZZII)V
    .locals 4

    .prologue
    .line 1585
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1587
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "traffic-update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string v0, "route"

    .line 1588
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1591
    :try_start_0
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1595
    if-eqz p2, :cond_2

    .line 1596
    const-string v2, "flowCount"

    invoke-direct {p0, v1, v2, p3}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 1597
    const-string v2, "incidentCount"

    invoke-direct {p0, v1, v2, p4}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 1602
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1607
    :goto_2
    return-void

    .line 1587
    :cond_1
    const-string v0, "route-elements"

    goto :goto_0

    .line 1599
    :cond_2
    :try_start_1
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1604
    :catch_0
    move-exception v1

    .line 1605
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->b:Z

    return p1
.end method

.method private a(Lorg/json/JSONObject;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2294
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2295
    new-instance v0, Lcom/nokia/maps/s$a;

    invoke-direct {v0}, Lcom/nokia/maps/s$a;-><init>()V

    .line 2296
    invoke-virtual {v0}, Lcom/nokia/maps/s$a;->names()Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2297
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_4

    .line 2298
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2299
    const-string v1, "staged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sdk-usage"

    .line 2300
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 2301
    :cond_0
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2297
    :cond_1
    :goto_1
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    .line 2303
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_2
    if-ltz v3, :cond_1

    .line 2304
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2305
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 2306
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2307
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2314
    :catch_0
    move-exception v0

    .line 2315
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t determine whether SDK is used "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 2317
    :goto_3
    return v0

    .line 2303
    :cond_3
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_2

    .line 2313
    :cond_4
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method private b(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1344
    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/nokia/maps/s;->d:Z

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->PEDESTRIAN:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/nokia/maps/s;->e:Z

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->PUBLIC_TRANSPORT:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/nokia/maps/s;->f:Z

    if-nez v0, :cond_3

    .line 1346
    :cond_2
    const-string v0, "online"

    .line 1351
    :goto_0
    return-object v0

    .line 1348
    :cond_3
    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/nokia/maps/s;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1833
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1835
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1836
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1837
    invoke-direct {p0, v1, p2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1838
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1843
    :goto_0
    return-void

    .line 1840
    :catch_0
    move-exception v0

    .line 1841
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t track "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1963
    const-string v0, "last"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 1964
    iget-object v1, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969
    const-string v0, "usageCount"

    invoke-direct {p0, p2, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1978
    :cond_0
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1979
    if-eqz v0, :cond_3

    .line 1980
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v2, "sdk-usage"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1981
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Last"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1983
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1984
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 1985
    invoke-static {v3}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1989
    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1991
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->c(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1993
    :cond_2
    invoke-static {}, Lcom/nokia/maps/o;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1995
    :cond_3
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 1815
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1817
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1818
    if-nez p3, :cond_2

    .line 1819
    const-string v1, "count"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1820
    if-eqz p2, :cond_1

    .line 1821
    const-string v1, "isRealTime"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1826
    :cond_1
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1830
    :goto_1
    monitor-exit p0

    return-void

    .line 1824
    :cond_2
    :try_start_2
    const-string v1, "errors"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1827
    :catch_0
    move-exception v0

    .line 1828
    :try_start_3
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t track "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private b(ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1867
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1869
    :cond_0
    :try_start_0
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1870
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1872
    const-string v2, "subSessions"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1873
    if-eqz p1, :cond_3

    .line 1874
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1875
    const-string v2, "sli"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "ar"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 1876
    const-string v2, "mapSchemeCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1881
    :cond_1
    const-string v2, "sli"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "satellite"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    .line 1882
    const-string v2, "extrudedBuildingCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1884
    iget-boolean v2, p0, Lcom/nokia/maps/s;->k:Z

    if-eqz v2, :cond_2

    .line 1885
    const-string v2, "3DLandmarkCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1889
    :cond_2
    iget-object v2, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "lastDisplaySessionName"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1892
    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1897
    :goto_0
    return-void

    .line 1894
    :catch_0
    move-exception v0

    .line 1895
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t track display-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->c:Z

    return p1
.end method

.method static synthetic c(Lcom/nokia/maps/s;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/nokia/maps/s;->t()V

    return-void
.end method

.method private c(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2029
    const-string v0, "basicNavUsageCount"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "advNavUsageCount"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "walkNavUsageCount"

    .line 2030
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2032
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "basicNavUsageCount"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Last"

    .line 2033
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2032
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2034
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "advNavUsageCount"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Last"

    .line 2035
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2034
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2036
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "walkNavUsageCount"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Last"

    .line 2037
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2036
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2039
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2040
    invoke-static {v0}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 2039
    invoke-virtual {v3, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2041
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2042
    invoke-static {v1}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2043
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2044
    invoke-static {v2}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 2043
    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2046
    :cond_3
    const-string v0, "navUsageCount"

    invoke-direct {p0, p2, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 2049
    :cond_4
    return-void
.end method

.method static synthetic c(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->d:Z

    return p1
.end method

.method static synthetic d(Lcom/nokia/maps/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/nokia/maps/s;->H:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->e:Z

    return p1
.end method

.method static synthetic e(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->f:Z

    return p1
.end method

.method static synthetic f(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->g:Z

    return p1
.end method

.method private g(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 861
    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v3

    .line 864
    if-eqz p2, :cond_2

    const-string v0, "offline"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->TRUCK:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_2

    :cond_0
    move v0, v1

    .line 866
    :goto_0
    iget-boolean v4, p0, Lcom/nokia/maps/s;->c:Z

    if-eqz v4, :cond_3

    sget-object v4, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-eq p1, v4, :cond_1

    sget-object v4, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->TRUCK:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v4, :cond_3

    .line 868
    :cond_1
    :goto_1
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz v1, :cond_4

    const-string v2, "advanced"

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    if-eqz v1, :cond_5

    const-string v1, "-"

    .line 869
    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "guidance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 870
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    if-eqz v0, :cond_6

    const-string v1, "-"

    .line 871
    :goto_4
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    if-eqz v0, :cond_7

    const-string v0, "traffic"

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 864
    goto :goto_0

    :cond_3
    move v1, v2

    .line 866
    goto :goto_1

    .line 868
    :cond_4
    const-string v2, ""

    goto :goto_2

    :cond_5
    const-string v1, ""

    goto :goto_3

    .line 870
    :cond_6
    const-string v1, ""

    goto :goto_4

    .line 871
    :cond_7
    const-string v0, ""

    goto :goto_5
.end method

.method static synthetic g(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->h:Z

    return p1
.end method

.method static synthetic h(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    sput-object p0, Lcom/nokia/maps/s;->F:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic h(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->i:Z

    return p1
.end method

.method private i(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 901
    .line 902
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 903
    new-instance v0, Lcom/nokia/maps/s$b;

    invoke-direct {v0}, Lcom/nokia/maps/s$b;-><init>()V

    .line 907
    :goto_0
    return-object v0

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic i(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->j:Z

    return p1
.end method

.method private j(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 914
    new-instance v0, Lcom/nokia/maps/s$b;

    invoke-direct {v0}, Lcom/nokia/maps/s$b;-><init>()V

    .line 915
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 916
    const-string v1, "last"

    iget-object v2, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "last"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 918
    :cond_0
    return-object v0
.end method

.method static synthetic j(Lcom/nokia/maps/s;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nokia/maps/s;->k:Z

    return p1
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/nokia/maps/s;->F:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized k(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1508
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1510
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "fleet-connectivity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "service"

    .line 1511
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1514
    :try_start_2
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1516
    invoke-direct {p0, v1, p1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1517
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1521
    :goto_0
    monitor-exit p0

    return-void

    .line 1518
    :catch_0
    move-exception v1

    .line 1519
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method static synthetic l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    return-object v0
.end method

.method private l(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1610
    const-string v0, "satellite"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hybrid"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1858
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "display"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    .line 1859
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized m()V
    .locals 3

    .prologue
    .line 648
    monitor-enter p0

    :try_start_0
    const-string v0, ""

    sput-object v0, Lcom/nokia/maps/s;->F:Ljava/lang/String;

    .line 649
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->d()Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/nokia/maps/s;->G:Lcom/nokia/maps/MapsEngine$k;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapsEngine;->a(Lcom/nokia/maps/MapsEngine$k;)V

    .line 650
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->d()Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->G()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 654
    :goto_0
    monitor-exit p0

    return-void

    .line 651
    :catch_0
    move-exception v0

    .line 652
    :try_start_1
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "Couldn\'t get mapversion for sdk-usage"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 694
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 695
    new-instance v0, Lcom/nokia/maps/s$a;

    invoke-direct {v0}, Lcom/nokia/maps/s$a;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    .line 696
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v1, "staged"

    iget-object v2, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 698
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "data"

    iget-object v2, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 699
    return-void
.end method

.method private n(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1949
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "signature"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/nokia/maps/s;->q:I

    if-lez v0, :cond_1

    .line 1950
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "signature"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1951
    iget-object v1, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/nokia/maps/p;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    .line 1952
    if-nez v0, :cond_1

    .line 1953
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "SDK-usage data may have been tampered with"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1956
    :cond_1
    return-void
.end method

.method private o()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x3e8

    .line 760
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v1, "sdk-usage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v1, "sdk-usage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 766
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 767
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    div-long/2addr v2, v8

    .line 768
    const-wide/32 v4, 0x15180

    div-long v4, v2, v4

    long-to-int v4, v4

    .line 769
    mul-int/lit8 v5, v4, 0x18

    mul-int/lit8 v5, v5, 0x3c

    mul-int/lit8 v5, v5, 0x3c

    .line 771
    if-lez v4, :cond_0

    .line 772
    const-string v6, "count"

    invoke-direct {p0, v0, v6, v4}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 774
    const-string v6, "totalTime"

    invoke-direct {p0, v0, v6, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 776
    const-string v6, "emptyCount"

    invoke-direct {p0, v0, v6, v4}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 778
    invoke-direct {p0}, Lcom/nokia/maps/s;->n()V

    .line 781
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v4, "sdk-usage"

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 783
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    int-to-long v4, v5

    sub-long/2addr v2, v4

    mul-long/2addr v2, v8

    sub-long/2addr v0, v2

    .line 784
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 787
    :cond_0
    return-void
.end method

.method private o(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2005
    sget-object v0, Lcom/nokia/maps/s;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2006
    if-eqz v0, :cond_2

    .line 2007
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v2, "sdk-usage"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2008
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Last"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2010
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2011
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2012
    invoke-static {v3}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2016
    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 2018
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->c(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2020
    :cond_1
    invoke-static {}, Lcom/nokia/maps/o;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2022
    :cond_2
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1846
    const-string v0, "hybrid"

    .line 1848
    iget-boolean v1, p0, Lcom/nokia/maps/s;->b:Z

    if-nez v1, :cond_1

    .line 1849
    const-string v0, "online"

    .line 1854
    :cond_0
    :goto_0
    return-object v0

    .line 1850
    :cond_1
    iget-object v1, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    invoke-static {v1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nokia/maps/MapsEngine;->D()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1851
    const-string v0, "offline"

    goto :goto_0
.end method

.method private q()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1942
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "data"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1943
    iget-object v1, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/nokia/maps/p;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1944
    iget-object v2, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "data"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1945
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "signature"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1946
    return-void
.end method

.method private r()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2073
    iget-object v0, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    invoke-static {v0}, Lcom/nokia/maps/o;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2128
    :cond_0
    :goto_0
    return-void

    .line 2080
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2081
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v3, "startTime"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 2082
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 2084
    invoke-virtual {v5, v6}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    .line 2085
    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v0, v3

    if-gt v0, v1, :cond_3

    move v0, v1

    .line 2086
    :goto_1
    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-gt v3, v6, :cond_4

    move v3, v1

    .line 2088
    :goto_2
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v0, v1

    move v1, v2

    .line 2122
    :goto_3
    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    .line 2123
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->c()Lcom/nokia/maps/MapsEngine$e;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapsEngine$e;->c:Lcom/nokia/maps/MapsEngine$e;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    .line 2124
    invoke-static {v0}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->D()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    .line 2125
    invoke-static {v0}, Lcom/nokia/maps/o;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2126
    :cond_2
    invoke-virtual {p0, v2}, Lcom/nokia/maps/s;->k(Z)Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2085
    goto :goto_1

    :cond_4
    move v3, v2

    .line 2086
    goto :goto_2

    .line 2092
    :cond_5
    if-eqz v0, :cond_6

    .line 2093
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    sget-object v7, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v6, v7}, Lcom/nokia/maps/o;->a(Ljava/util/Date;Ljava/util/Date;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-ltz v0, :cond_6

    move v0, v1

    .line 2097
    goto :goto_3

    .line 2099
    :cond_6
    invoke-virtual {v4, v10}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-eq v0, v6, :cond_7

    .line 2102
    if-eqz v3, :cond_b

    move v0, v1

    .line 2103
    goto :goto_3

    .line 2106
    :cond_7
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v3, v4}, Lcom/nokia/maps/o;->a(Ljava/util/Date;Ljava/util/Date;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    const-wide/16 v6, 0x7

    cmp-long v0, v4, v6

    if-ltz v0, :cond_8

    move v0, v1

    move v1, v2

    .line 2109
    goto :goto_3

    .line 2111
    :cond_8
    iget v0, p0, Lcom/nokia/maps/s;->q:I

    const/16 v3, 0xc8

    if-lt v0, v3, :cond_9

    move v0, v1

    move v1, v2

    .line 2113
    goto :goto_3

    .line 2114
    :cond_9
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->isEval()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v3, "sdk-usage"

    .line 2115
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v3, "sdk-usage"

    .line 2116
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_a

    move v0, v1

    move v1, v2

    .line 2119
    goto/16 :goto_3

    :cond_a
    move v1, v2

    move v0, v2

    goto/16 :goto_3

    :cond_b
    move v0, v1

    move v1, v2

    goto/16 :goto_3
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2137
    iget-object v0, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    const-string v1, "cd63818e-a03d-11e4-89d3-123b93f75cba"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 2139
    iput v3, p0, Lcom/nokia/maps/s;->q:I

    .line 2140
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    .line 2143
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 2144
    new-instance v0, Lcom/nokia/maps/s$a;

    invoke-direct {v0}, Lcom/nokia/maps/s$a;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    .line 2146
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    if-nez v0, :cond_1

    .line 2147
    new-instance v0, Lcom/nokia/maps/s$a;

    invoke-direct {v0}, Lcom/nokia/maps/s$a;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    .line 2149
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v1, "staged"

    iget-object v2, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2151
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "index"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "trackingId"

    .line 2152
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "data"

    iget-object v2, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "count"

    const/4 v2, 0x1

    .line 2153
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2159
    :goto_0
    return-void

    .line 2156
    :catch_0
    move-exception v0

    .line 2157
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "Failed to create a tracking session"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2166
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "save()"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2168
    const/4 v0, 0x0

    .line 2170
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "lastAccess"

    iget-object v3, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    invoke-static {v3}, Lcom/nokia/maps/o;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2171
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "sdkStarted"

    iget-object v3, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    invoke-static {v3}, Lcom/nokia/maps/o;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2172
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "sdkUsed"

    iget-boolean v3, p0, Lcom/nokia/maps/s;->t:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2173
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "eventCount"

    iget v3, p0, Lcom/nokia/maps/s;->q:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2175
    invoke-direct {p0}, Lcom/nokia/maps/s;->q()V

    .line 2177
    iget-object v1, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    const-string v2, "cd63818e-a03d-11e4-89d3-123b93f75cba"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2178
    :try_start_1
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 2180
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v2, "data"

    iget-object v3, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2187
    if-eqz v0, :cond_0

    .line 2189
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 2196
    :cond_0
    :goto_0
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saved: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2197
    return-void

    .line 2182
    :catch_0
    move-exception v1

    .line 2183
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v2, "Could not create file to save tracking session"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2187
    if-eqz v0, :cond_0

    .line 2189
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 2190
    :catch_1
    move-exception v0

    goto :goto_0

    .line 2184
    :catch_2
    move-exception v1

    .line 2185
    :try_start_5
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v2, "Could not create file to save tracking session"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2187
    if-eqz v0, :cond_0

    .line 2189
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 2190
    :catch_3
    move-exception v0

    goto :goto_0

    .line 2187
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v1, :cond_1

    .line 2189
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 2192
    :cond_1
    :goto_2
    throw v0

    .line 2190
    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_2

    .line 2187
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method private declared-synchronized u()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2204
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v3, "restore()"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2206
    const/4 v2, 0x1

    .line 2210
    :try_start_1
    iget-object v3, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    const-string v4, "cd63818e-a03d-11e4-89d3-123b93f75cba"

    invoke-virtual {v3, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2217
    :goto_0
    if-eqz v2, :cond_9

    .line 2218
    :try_start_2
    new-instance v2, Ljava/io/InputStreamReader;

    const-string v3, "UTF-8"

    .line 2219
    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2222
    :try_start_3
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2223
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2225
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2226
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 2245
    :catch_0
    move-exception v1

    .line 2246
    :goto_2
    :try_start_5
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v3, "Cannot restore tracking session"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2247
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2248
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/nokia/maps/s;->t:Z

    .line 2249
    invoke-direct {p0}, Lcom/nokia/maps/s;->s()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2256
    if-eqz v0, :cond_0

    .line 2258
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2263
    :cond_0
    :goto_3
    if-eqz v2, :cond_1

    .line 2265
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2278
    :cond_1
    :goto_4
    :try_start_8
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-eqz v0, :cond_a

    .line 2279
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restored: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2283
    :goto_5
    monitor-exit p0

    return-void

    .line 2212
    :catch_1
    move-exception v2

    move v2, v0

    move-object v0, v1

    .line 2214
    goto :goto_0

    .line 2228
    :cond_2
    if-eqz v1, :cond_3

    .line 2229
    :try_start_9
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    .line 2230
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "data"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2231
    invoke-direct {p0, v1}, Lcom/nokia/maps/s;->n(Ljava/lang/String;)V

    .line 2232
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    .line 2233
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v3, "staged"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    .line 2234
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "eventCount"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/nokia/maps/s;->q:I

    .line 2235
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "lastAccess"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    .line 2236
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "sdkStarted"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/nokia/maps/o;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2237
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "sdkUsed"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2238
    iget-object v1, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v3, "sdkUsed"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/nokia/maps/s;->t:Z
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 2256
    :cond_3
    :goto_6
    if-eqz v0, :cond_4

    .line 2258
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 2263
    :cond_4
    :goto_7
    if-eqz v2, :cond_1

    .line 2265
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_4

    .line 2266
    :catch_2
    move-exception v0

    goto/16 :goto_4

    .line 2242
    :cond_5
    :try_start_c
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/nokia/maps/s;->t:Z
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_6

    .line 2250
    :catch_3
    move-exception v1

    .line 2251
    :goto_8
    :try_start_d
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v3, "Cannot restore tracking session"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2252
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2253
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/nokia/maps/s;->t:Z

    .line 2254
    invoke-direct {p0}, Lcom/nokia/maps/s;->s()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 2256
    if-eqz v0, :cond_6

    .line 2258
    :try_start_e
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 2263
    :cond_6
    :goto_9
    if-eqz v2, :cond_1

    .line 2265
    :try_start_f
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_4

    .line 2266
    :catch_4
    move-exception v0

    goto/16 :goto_4

    .line 2256
    :catchall_0
    move-exception v0

    :goto_a
    if-eqz v1, :cond_7

    .line 2258
    :try_start_10
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 2263
    :cond_7
    :goto_b
    if-eqz v2, :cond_8

    .line 2265
    :try_start_11
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 2268
    :cond_8
    :goto_c
    :try_start_12
    throw v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 2204
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2273
    :cond_9
    :try_start_13
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 2274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/s;->t:Z

    .line 2275
    invoke-direct {p0}, Lcom/nokia/maps/s;->s()V

    goto/16 :goto_4

    .line 2281
    :cond_a
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "Error restoring tracking data"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_5

    .line 2259
    :catch_5
    move-exception v0

    goto :goto_7

    :catch_6
    move-exception v0

    goto/16 :goto_3

    .line 2266
    :catch_7
    move-exception v0

    goto/16 :goto_4

    .line 2259
    :catch_8
    move-exception v0

    goto :goto_9

    :catch_9
    move-exception v1

    goto :goto_b

    .line 2266
    :catch_a
    move-exception v1

    goto :goto_c

    .line 2256
    :catchall_2
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_a

    .line 2250
    :catch_b
    move-exception v0

    move-object v0, v1

    goto :goto_8

    .line 2245
    :catch_c
    move-exception v0

    move-object v0, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 6

    .prologue
    .line 586
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 589
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 591
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v2, "sdk-usage"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 598
    invoke-direct {p0}, Lcom/nokia/maps/s;->o()V

    .line 603
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 605
    iget-object v2, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v3, "sdk-usage"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 606
    const-string v3, "count"

    invoke-direct {p0, v2, v3}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 609
    const-string v3, "loadCount"

    invoke-direct {p0, v2, v3}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 611
    const-string v3, "totalTime"

    invoke-direct {p0, v2, v3, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 612
    iget-boolean v1, p0, Lcom/nokia/maps/s;->t:Z

    if-nez v1, :cond_1

    .line 613
    const-string v1, "emptyCount"

    invoke-direct {p0, v2, v1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 618
    :cond_1
    invoke-direct {p0}, Lcom/nokia/maps/s;->n()V

    .line 624
    :cond_2
    iget-object v1, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    const-string v2, "sdk-usage"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 625
    iput-object v0, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 626
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/s;->t:Z

    .line 629
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    .line 630
    iget v0, p0, Lcom/nokia/maps/s;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/nokia/maps/s;->q:I

    .line 635
    invoke-direct {p0}, Lcom/nokia/maps/s;->r()V

    .line 637
    invoke-direct {p0}, Lcom/nokia/maps/s;->t()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    :goto_0
    monitor-exit p0

    return-void

    .line 639
    :catch_0
    move-exception v0

    .line 640
    :try_start_2
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "Couldn\'t track sdk-usage"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(I)V
    .locals 7

    .prologue
    .line 1310
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->PUBLIC_TRANSPORT:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    const-string v2, "timetable"

    const-string v3, "online"

    int-to-double v4, p1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Ljava/lang/String;Ljava/lang/String;DZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1311
    monitor-exit p0

    return-void

    .line 1310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IIZI)V
    .locals 4

    .prologue
    .line 958
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 960
    :cond_0
    const/4 v0, 0x0

    .line 963
    :try_start_1
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "venue3D"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 964
    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 966
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 967
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 968
    const-string v2, "selections"

    invoke-direct {p0, v1, v2, p2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 969
    const-string v2, "floorChanges"

    invoke-direct {p0, v1, v2, p1}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 970
    const-string v2, "totalTime"

    invoke-direct {p0, v1, v2, p4}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 972
    if-eqz p3, :cond_1

    .line 973
    const-string v2, "privateCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 976
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 981
    :goto_0
    monitor-exit p0

    return-void

    .line 978
    :catch_0
    move-exception v1

    .line 979
    :try_start_2
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(J)V
    .locals 5

    .prologue
    .line 877
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 879
    :cond_0
    const/4 v0, 0x0

    .line 884
    :try_start_1
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "nav_tracking"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    .line 885
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "car"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 887
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 888
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 889
    const-string v2, "distance"

    invoke-direct {p0, v1, v2, p1, p2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 890
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 895
    :goto_0
    monitor-exit p0

    return-void

    .line 892
    :catch_0
    move-exception v1

    .line 893
    :try_start_2
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 402
    invoke-direct {p0}, Lcom/nokia/maps/s;->m()V

    .line 404
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 406
    :cond_0
    iput-object p1, p0, Lcom/nokia/maps/s;->p:Landroid/content/Context;

    .line 411
    if-eqz p2, :cond_3

    .line 412
    :try_start_0
    const-string v0, "ZjVyanlrcWFxOA==\n"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 413
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :goto_0
    new-instance v1, Lcom/here/sdk/hacwrapper/HacSettings;

    invoke-direct {v1, p1, v0}, Lcom/here/sdk/hacwrapper/HacSettings;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v1, v4}, Lcom/here/sdk/hacwrapper/HacSettings;->setOffline(Z)V

    .line 426
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/here/sdk/hacwrapper/HacSettings;->setFlushEnabledWhileRoaming(Ljava/lang/Boolean;)V

    .line 427
    invoke-virtual {v1, v4}, Lcom/here/sdk/hacwrapper/HacSettings;->setFlushSize(I)V

    .line 428
    const-wide v2, 0x9184e729fffL

    invoke-virtual {v1, v2, v3}, Lcom/here/sdk/hacwrapper/HacSettings;->setFlushTime(J)V

    .line 429
    sget-object v0, Lcom/here/sdk/hacwrapper/LoggingLevels;->NONE:Lcom/here/sdk/hacwrapper/LoggingLevels;

    invoke-virtual {v1, v0}, Lcom/here/sdk/hacwrapper/HacSettings;->setLogLevel(Lcom/here/sdk/hacwrapper/LoggingLevels;)V

    .line 431
    invoke-static {v1}, Lcom/here/sdk/hacwrapper/HacAnalytics;->initialize(Lcom/here/sdk/hacwrapper/HacSettings;)V

    .line 432
    invoke-static {v4}, Lcom/here/sdk/hacwrapper/HacAnalytics;->setOfflineMode(Z)V

    .line 434
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/nokia/maps/s;->v:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 435
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/nokia/maps/s;->w:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 437
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/nokia/maps/s;->x:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 438
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/nokia/maps/s;->y:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 439
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/nokia/maps/s;->z:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 441
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/nokia/maps/s;->A:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 442
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/nokia/maps/s;->B:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 443
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/nokia/maps/s;->C:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 444
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/nokia/maps/s;->D:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 446
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/nokia/maps/s;->E:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 448
    invoke-direct {p0}, Lcom/nokia/maps/s;->u()V

    .line 451
    iget-object v0, p0, Lcom/nokia/maps/s;->n:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    if-nez v0, :cond_2

    .line 453
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/s;->r:Ljava/util/Date;

    .line 454
    iput-boolean v5, p0, Lcom/nokia/maps/s;->t:Z

    .line 455
    invoke-direct {p0}, Lcom/nokia/maps/s;->s()V

    .line 457
    :cond_2
    return-void

    .line 415
    :cond_3
    :try_start_1
    const-string v0, "0QLS/6jl2s6fmA==\n"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 416
    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/nokia/maps/u;->a([BI)[B

    move-result-object v1

    .line 417
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 419
    :catch_0
    move-exception v0

    .line 421
    new-instance v0, Ljava/lang/Error;

    const-string v1, "Cannot initialize segment.io"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/customlocation2/CLE2Request$CLE2ConnectivityMode;ZZ)V
    .locals 2

    .prologue
    .line 1293
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1295
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1296
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1297
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1298
    sget-object v1, Lcom/here/android/mpa/customlocation2/CLE2Request$CLE2ConnectivityMode;->AUTO:Lcom/here/android/mpa/customlocation2/CLE2Request$CLE2ConnectivityMode;

    if-ne p1, v1, :cond_1

    .line 1299
    const-string v1, "custom-location-hybrid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1305
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/nokia/maps/s;->a(Ljava/lang/String;ZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1306
    monitor-exit p0

    return-void

    .line 1300
    :cond_1
    :try_start_2
    sget-object v1, Lcom/here/android/mpa/customlocation2/CLE2Request$CLE2ConnectivityMode;->OFFLINE:Lcom/here/android/mpa/customlocation2/CLE2Request$CLE2ConnectivityMode;

    if-ne p1, v1, :cond_2

    .line 1301
    const-string v1, "custom-location-offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1303
    :cond_2
    const-string v1, "custom-location"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    .locals 1

    .prologue
    .line 1430
    monitor-enter p0

    :try_start_0
    const-string v0, "list"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1431
    monitor-exit p0

    return-void

    .line 1430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Lcom/here/android/mpa/routing/RouteOptions$Type;ZI)V
    .locals 4

    .prologue
    .line 987
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 989
    :cond_0
    const/4 v0, 0x0

    .line 991
    :try_start_1
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "routing"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "venue3D"

    .line 992
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 993
    invoke-direct {p0}, Lcom/nokia/maps/s;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 994
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 996
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 997
    if-eqz p3, :cond_1

    .line 998
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1003
    :goto_0
    packed-switch p4, :pswitch_data_0

    .line 1020
    :goto_1
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$Type;->SHORTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    if-ne p2, v2, :cond_2

    .line 1021
    const-string v2, "shortestCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1026
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1031
    :goto_3
    monitor-exit p0

    return-void

    .line 1000
    :cond_1
    :try_start_2
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1028
    :catch_0
    move-exception v1

    .line 1029
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1005
    :pswitch_0
    :try_start_4
    const-string v2, "insideCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_1

    .line 1008
    :pswitch_1
    const-string v2, "outsideToInsideCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_1

    .line 1011
    :pswitch_2
    const-string v2, "insideToOutsideCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_1

    .line 1014
    :pswitch_3
    const-string v2, "venueToVenueCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_1

    .line 1023
    :cond_2
    const-string v2, "fastestCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 1003
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Lcom/nokia/maps/RouteImpl;Lcom/here/android/mpa/routing/CoreRouter$Connectivity;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1361
    monitor-enter p0

    .line 1362
    const/4 v6, 0x0

    .line 1363
    const-wide/16 v4, 0x0

    .line 1365
    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->b(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Ljava/lang/String;

    move-result-object v3

    .line 1367
    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->PUBLIC_TRANSPORT:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_1

    .line 1368
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/nokia/maps/RouteImpl;->a()Lcom/nokia/maps/RouteImpl$a;

    move-result-object v0

    sget-object v2, Lcom/nokia/maps/RouteImpl$a;->b:Lcom/nokia/maps/RouteImpl$a;

    if-ne v0, v2, :cond_0

    const-string v0, "timetable"

    :goto_0
    move-object v2, v0

    .line 1376
    :goto_1
    if-nez p2, :cond_3

    .line 1378
    const/4 v6, 0x1

    .line 1384
    :goto_2
    sget-object v0, Lcom/nokia/maps/s$5;->d:[I

    invoke-virtual {p3}, Lcom/here/android/mpa/routing/CoreRouter$Connectivity;->ordinal()I

    move-result v7

    aget v0, v0, v7

    packed-switch v0, :pswitch_data_0

    move-object v3, v1

    :goto_3
    :pswitch_0
    move-object v0, p0

    move-object v1, p1

    .line 1396
    invoke-direct/range {v0 .. v6}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Ljava/lang/String;Ljava/lang/String;DZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1397
    monitor-exit p0

    return-void

    .line 1368
    :cond_0
    :try_start_1
    const-string v0, "estimated"

    goto :goto_0

    .line 1370
    :cond_1
    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->TRUCK:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    if-ne p1, v0, :cond_4

    :cond_2
    if-eqz p2, :cond_4

    .line 1371
    invoke-virtual {p2}, Lcom/nokia/maps/RouteImpl;->l()Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    move-result-object v0

    sget-object v2, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    if-eq v0, v2, :cond_4

    const-string v0, "offline"

    .line 1372
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1373
    const-string v2, "traffic"

    goto :goto_1

    .line 1381
    :cond_3
    invoke-virtual {p2}, Lcom/nokia/maps/RouteImpl;->getLength()I

    move-result v0

    int-to-double v4, v0

    goto :goto_2

    .line 1386
    :pswitch_1
    const-string v3, "online"

    goto :goto_3

    .line 1389
    :pswitch_2
    const-string v3, "offline"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move-object v2, v1

    goto :goto_1

    .line 1384
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 824
    monitor-enter p0

    :try_start_0
    const-string v0, "laneAssistanceCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    monitor-exit p0

    return-void

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZJZZZ)V
    .locals 5

    .prologue
    .line 928
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 930
    :cond_0
    const/4 v0, 0x0

    .line 932
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/s;->g(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)Ljava/lang/String;

    move-result-object v0

    .line 933
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 935
    if-eqz p7, :cond_2

    .line 936
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 947
    :cond_1
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 952
    :goto_1
    monitor-exit p0

    return-void

    .line 937
    :cond_2
    if-eqz p5, :cond_3

    .line 938
    :try_start_2
    const-string v2, "reroutes"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 949
    :catch_0
    move-exception v1

    .line 950
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 940
    :cond_3
    :try_start_4
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 941
    const-string v2, "distance"

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 942
    if-eqz p6, :cond_1

    .line 943
    const-string v2, "completedCount"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLcom/here/android/mpa/guidance/VoiceSkin;)V
    .locals 4

    .prologue
    .line 855
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "voicePackage"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_"

    .line 856
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/here/android/mpa/guidance/VoiceSkin;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    .line 857
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/here/android/mpa/guidance/VoiceSkin;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 855
    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 858
    monitor-exit p0

    return-void

    .line 855
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/posclient/analytics/PositioningCounters;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1059
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "positioning"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1060
    iget v1, p1, Lcom/here/posclient/analytics/PositioningCounters;->event:I

    sparse-switch v1, :sswitch_data_0

    .line 1077
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "unknown event type: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->event:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1121
    :goto_0
    monitor-exit p0

    return-void

    .line 1062
    :sswitch_0
    :try_start_1
    const-string v1, "online"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "outdoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1082
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1083
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1084
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->updates:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_0

    .line 1085
    const-string v2, "updates"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->updates:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1087
    :cond_0
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->updateErrors:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 1088
    const-string v2, "errorCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->updateErrors:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1090
    :cond_1
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->fallbacks:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    .line 1091
    const-string v2, "fallbacks"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->fallbacks:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1093
    :cond_2
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->estimates:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    .line 1094
    const-string v2, "estimates"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->estimates:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1096
    :cond_3
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->externals:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_4

    .line 1097
    const-string v2, "externals"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->externals:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1099
    :cond_4
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->withBuilding:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_5

    .line 1100
    const-string v2, "buildingAwareCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->withBuilding:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1102
    :cond_5
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->withFloor:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_6

    .line 1103
    const-string v2, "floorAwareCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->withFloor:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1105
    :cond_6
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->byCell:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    .line 1106
    const-string v2, "cellBasedCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->byCell:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1108
    :cond_7
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->byWlan:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_8

    .line 1109
    const-string v2, "wlanBasedCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->byWlan:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1111
    :cond_8
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->byBle:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_9

    .line 1112
    const-string v2, "bleBasedCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->byBle:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1114
    :cond_9
    iget-wide v2, p1, Lcom/here/posclient/analytics/PositioningCounters;->onlines:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_a

    .line 1115
    const-string v2, "onlineCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/PositioningCounters;->onlines:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1117
    :cond_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1118
    :catch_0
    move-exception v1

    .line 1119
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v2, "Couldn\'t track: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1059
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1065
    :sswitch_1
    :try_start_4
    const-string v1, "hybrid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "outdoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1068
    :sswitch_2
    const-string v1, "offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "outdoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1071
    :sswitch_3
    const-string v1, "offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "common_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1074
    :sswitch_4
    const-string v1, "offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "private_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1060
    nop

    :sswitch_data_0
    .sparse-switch
        0x6f -> :sswitch_0
        0x79 -> :sswitch_1
        0x83 -> :sswitch_2
        0x84 -> :sswitch_3
        0x85 -> :sswitch_4
    .end sparse-switch
.end method

.method public declared-synchronized a(Lcom/here/posclient/analytics/RadiomapCounters;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1125
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1126
    iget v1, p1, Lcom/here/posclient/analytics/RadiomapCounters;->event:I

    packed-switch v1, :pswitch_data_0

    .line 1146
    :pswitch_0
    sget-object v0, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v1, "unknown event type: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p1, Lcom/here/posclient/analytics/RadiomapCounters;->event:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1166
    :goto_0
    monitor-exit p0

    return-void

    .line 1128
    :pswitch_1
    :try_start_1
    const-string v1, "odnp_rm_demand"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "outdoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1151
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1152
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1153
    iget-wide v2, p1, Lcom/here/posclient/analytics/RadiomapCounters;->errors:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_0

    .line 1154
    const-string v2, "errors"

    iget-wide v4, p1, Lcom/here/posclient/analytics/RadiomapCounters;->errors:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1156
    :cond_0
    iget-wide v2, p1, Lcom/here/posclient/analytics/RadiomapCounters;->downloadCount:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 1157
    const-string v2, "downloadCount"

    iget-wide v4, p1, Lcom/here/posclient/analytics/RadiomapCounters;->downloadCount:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1159
    :cond_1
    iget-wide v2, p1, Lcom/here/posclient/analytics/RadiomapCounters;->downloadFileSize:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    .line 1160
    const-string v2, "downloadFileSize"

    iget-wide v4, p1, Lcom/here/posclient/analytics/RadiomapCounters;->downloadFileSize:J

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 1162
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1163
    :catch_0
    move-exception v1

    .line 1164
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    const-string v2, "Couldn\'t track: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1131
    :pswitch_2
    :try_start_4
    const-string v1, "odnp_rm_demand"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "common_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1134
    :pswitch_3
    const-string v1, "odnp_rm_demand"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "private_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1137
    :pswitch_4
    const-string v1, "odnp_rm_manual"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "outdoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1140
    :pswitch_5
    const-string v1, "odnp_rm_manual"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "common_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1143
    :pswitch_6
    const-string v1, "odnp_rm_manual"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "private_indoor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1126
    :pswitch_data_0
    .packed-switch 0xd3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/nokia/maps/PlacesConstants$b;ZZ)V
    .locals 1

    .prologue
    .line 1054
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/PlacesConstants$b;Ljava/lang/String;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055
    monitor-exit p0

    return-void

    .line 1054
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/dp$a;ZZ)V
    .locals 3

    .prologue
    .line 1036
    monitor-enter p0

    const/4 v0, 0x0

    .line 1037
    :try_start_0
    sget-object v1, Lcom/nokia/maps/s$5;->b:[I

    invoke-virtual {p1}, Lcom/nokia/maps/dp$a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1048
    :goto_0
    sget-object v1, Lcom/nokia/maps/PlacesConstants$b;->j:Lcom/nokia/maps/PlacesConstants$b;

    invoke-direct {p0, v1, v0, p2, p3}, Lcom/nokia/maps/s;->a(Lcom/nokia/maps/PlacesConstants$b;Ljava/lang/String;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1049
    monitor-exit p0

    return-void

    .line 1039
    :pswitch_0
    :try_start_1
    const-string v0, "address"

    goto :goto_0

    .line 1042
    :pswitch_1
    const-string v0, "freeform"

    goto :goto_0

    .line 1045
    :pswitch_2
    const-string v0, "unknown"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1036
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1037
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1664
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "trafficOnMapCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1665
    monitor-exit p0

    return-void

    .line 1664
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1670
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    .line 1671
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v1

    .line 1673
    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 1676
    const/4 v0, 0x1

    const-string v1, "satellite"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1686
    :goto_0
    monitor-exit p0

    return-void

    .line 1678
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 1681
    const/4 v0, 0x1

    :try_start_1
    const-string v1, "map"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(ZLjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1670
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1684
    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    const-string v0, "satellite"

    :goto_1
    const-string v1, "mapSchemeCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "map"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 1525
    const-string v0, "bbox"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Z)V

    .line 1526
    return-void
.end method

.method public declared-synchronized a(ZII)V
    .locals 1

    .prologue
    .line 1575
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/nokia/maps/s;->a(ZZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1576
    monitor-exit p0

    return-void

    .line 1575
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1621
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/s;->b(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1622
    monitor-exit p0

    return-void

    .line 1621
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ZZ)V
    .locals 1

    .prologue
    .line 1766
    const-string v0, "um-next_departures-stationBoard"

    .line 1767
    invoke-direct {p0, v0, p1, p2}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1768
    return-void
.end method

.method public declared-synchronized a(ZZIZ)V
    .locals 4

    .prologue
    .line 1316
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1317
    :cond_0
    :try_start_1
    const-string v0, "routing-online-um"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1319
    :try_start_2
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1320
    if-nez p4, :cond_4

    .line 1321
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1322
    if-lez p3, :cond_1

    .line 1323
    const-string v2, "distance"

    invoke-direct {p0, v1, v2, p3}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    .line 1325
    :cond_1
    if-eqz p2, :cond_2

    .line 1326
    const-string v2, "isRealTime"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1328
    :cond_2
    if-eqz p1, :cond_3

    .line 1329
    const-string v2, "hasFareInfo"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1334
    :cond_3
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1338
    :goto_1
    monitor-exit p0

    return-void

    .line 1332
    :cond_4
    :try_start_3
    const-string v2, "errors"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1335
    :catch_0
    move-exception v1

    .line 1336
    :try_start_4
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1489
    const-string v0, "count"

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->k(Ljava/lang/String;)V

    .line 1490
    return-void
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    .locals 1

    .prologue
    .line 1438
    monitor-enter p0

    :try_start_0
    const-string v0, "install"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1439
    monitor-exit p0

    return-void

    .line 1438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 829
    monitor-enter p0

    :try_start_0
    const-string v0, "safetySpotCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 830
    monitor-exit p0

    return-void

    .line 829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1690
    monitor-enter p0

    :try_start_0
    const-string v0, "changescheme"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1691
    const-string v0, "map"

    const-string v1, "customizedSchemeChangeCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1695
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1692
    :cond_1
    :try_start_1
    const-string v0, "createscheme"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1693
    const-string v0, "map"

    const-string v1, "customizedSchemeCreateCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1690
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 1530
    const-string v0, "linkids"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Z)V

    .line 1531
    return-void
.end method

.method public declared-synchronized b(ZII)V
    .locals 1

    .prologue
    .line 1581
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/nokia/maps/s;->a(ZZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1582
    monitor-exit p0

    return-void

    .line 1581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(ZZ)V
    .locals 1

    .prologue
    .line 1772
    const-string v0, "um-next_departures-multiBoard"

    .line 1773
    invoke-direct {p0, v0, p1, p2}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1774
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1494
    const-string v0, "incomingMessagesCount"

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->k(Ljava/lang/String;)V

    .line 1495
    return-void
.end method

.method public declared-synchronized c(Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    .locals 1

    .prologue
    .line 1446
    monitor-enter p0

    :try_start_0
    const-string v0, "uninstall"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1447
    monitor-exit p0

    return-void

    .line 1446
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 834
    monitor-enter p0

    :try_start_0
    const-string v0, "signPostCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    monitor-exit p0

    return-void

    .line 834
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1709
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "fleetMapCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1710
    monitor-exit p0

    return-void

    .line 1709
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Z)V
    .locals 1

    .prologue
    .line 1643
    monitor-enter p0

    :try_start_0
    const-string v0, "sli"

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/s;->b(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1644
    monitor-exit p0

    return-void

    .line 1643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1499
    const-string v0, "outgoingMessagesCount"

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->k(Ljava/lang/String;)V

    .line 1500
    return-void
.end method

.method public declared-synchronized d(Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    .locals 1

    .prologue
    .line 1454
    monitor-enter p0

    :try_start_0
    const-string v0, "update"

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1455
    monitor-exit p0

    return-void

    .line 1454
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 839
    monitor-enter p0

    :try_start_0
    const-string v0, "junctionViewCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    monitor-exit p0

    return-void

    .line 839
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1714
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "truckAttributesCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1715
    monitor-exit p0

    return-void

    .line 1714
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Z)V
    .locals 1

    .prologue
    .line 1632
    monitor-enter p0

    :try_start_0
    const-string v0, "ar"

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/s;->b(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1633
    monitor-exit p0

    return-void

    .line 1632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1504
    const-string v0, "errors"

    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->k(Ljava/lang/String;)V

    .line 1505
    return-void
.end method

.method public declared-synchronized e(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 844
    monitor-enter p0

    :try_start_0
    const-string v0, "mapMatcherMobileCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 845
    monitor-exit p0

    return-void

    .line 844
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1719
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "congestionZonesCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1720
    monitor-exit p0

    return-void

    .line 1719
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 1778
    const-string v0, "um-transit_coverage-cityInfoByGeoCoordinate"

    .line 1779
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1780
    return-void
.end method

.method public declared-synchronized f()V
    .locals 4

    .prologue
    .line 1556
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/nokia/maps/s;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Analytics tracking called before engine initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1558
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "traffic-update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "geocoord"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 1559
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1562
    :try_start_2
    invoke-direct {p0, v0}, Lcom/nokia/maps/s;->j(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1564
    const-string v2, "count"

    invoke-direct {p0, v1, v2}, Lcom/nokia/maps/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1566
    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1571
    :goto_0
    monitor-exit p0

    return-void

    .line 1568
    :catch_0
    move-exception v1

    .line 1569
    :try_start_3
    sget-object v1, Lcom/nokia/maps/s;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized f(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;Z)V
    .locals 1

    .prologue
    .line 849
    monitor-enter p0

    :try_start_0
    const-string v0, "mapMatcherAutomotiveCount"

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/s;->a(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 850
    monitor-exit p0

    return-void

    .line 849
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1724
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "historicalSpeedPatternCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1725
    monitor-exit p0

    return-void

    .line 1724
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 1784
    const-string v0, "um-transit_coverage-cityInfoByName"

    .line 1785
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1786
    return-void
.end method

.method public declared-synchronized g()V
    .locals 2

    .prologue
    .line 1659
    monitor-enter p0

    :try_start_0
    const-string v0, "map"

    const-string v1, "buildingInteractions"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1660
    monitor-exit p0

    return-void

    .line 1659
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1729
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "satellite"

    :goto_0
    const-string v1, "customRasterTileSourceCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1730
    monitor-exit p0

    return-void

    .line 1729
    :cond_0
    :try_start_1
    const-string v0, "map"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g(Z)V
    .locals 2

    .prologue
    .line 1790
    const-string v0, "um-transit_coverage-coverageInfoByGeoCoordinate"

    .line 1791
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1792
    return-void
.end method

.method public declared-synchronized h()V
    .locals 2

    .prologue
    .line 1699
    monitor-enter p0

    :try_start_0
    const-string v0, "map"

    const-string v1, "3DLandmarkCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700
    monitor-exit p0

    return-void

    .line 1699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h(Z)V
    .locals 2

    .prologue
    .line 1796
    const-string v0, "um-station_search-name"

    .line 1797
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1798
    return-void
.end method

.method public declared-synchronized i()V
    .locals 2

    .prologue
    .line 1704
    monitor-enter p0

    :try_start_0
    const-string v0, "map"

    const-string v1, "extrudedBuildingCount"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1705
    monitor-exit p0

    return-void

    .line 1704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 1802
    const-string v0, "um-station_search-stationId"

    .line 1804
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1805
    return-void
.end method

.method public declared-synchronized j()V
    .locals 2

    .prologue
    .line 1745
    monitor-enter p0

    :try_start_0
    const-string v0, "sli"

    const-string v1, "buildingInteractions"

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1746
    monitor-exit p0

    return-void

    .line 1745
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j(Z)V
    .locals 2

    .prologue
    .line 1809
    const-string v0, "um-station_search-geoCoordinate"

    .line 1811
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/nokia/maps/s;->b(Ljava/lang/String;ZZ)V

    .line 1812
    return-void
.end method

.method k(Z)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 2341
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "lastAccess"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2342
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "eventCount"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2343
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "sdkStarted"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2344
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "sdkUsed"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2345
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "lastDisplaySessionName"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2348
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 2349
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2350
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2351
    iget-object v2, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2352
    if-eqz v0, :cond_0

    .line 2353
    const-string v2, "last"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2354
    const-string v2, "last"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2356
    :cond_1
    const-string v2, "lastDisplayTime"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2357
    const-string v2, "lastDisplayTime"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    .line 2365
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v1, "sdk-usage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2367
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v1, "sdk-usage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2368
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 2371
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v1, "sdk-usage"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2385
    :cond_3
    iget-object v0, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    const-string v1, "endTime"

    iget-object v2, p0, Lcom/nokia/maps/s;->s:Ljava/util/Date;

    invoke-static {v2}, Lcom/nokia/maps/o;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2389
    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const-string v1, "data"

    iget-object v2, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2391
    invoke-direct {p0}, Lcom/nokia/maps/s;->q()V

    .line 2393
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2396
    :goto_1
    iget-object v1, p0, Lcom/nokia/maps/s;->I:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/nokia/maps/fh;->b(Ljava/lang/Runnable;)V

    .line 2397
    iget-object v1, p0, Lcom/nokia/maps/s;->H:Ljava/lang/Object;

    monitor-enter v1

    .line 2398
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/here/sdk/hacwrapper/HacAnalytics;->setOfflineMode(Z)V

    .line 2399
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2401
    new-instance v1, Lcom/here/sdk/hacwrapper/HacProperties;

    invoke-direct {v1}, Lcom/here/sdk/hacwrapper/HacProperties;-><init>()V

    .line 2402
    const-string v2, "SDK-DATA"

    new-instance v3, Lcom/here/sdk/hacwrapper/HacProperties;

    iget-object v4, p0, Lcom/nokia/maps/s;->m:Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lcom/here/sdk/hacwrapper/HacProperties;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v1, v2, v3}, Lcom/here/sdk/hacwrapper/HacProperties;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2403
    const-string v2, "hereKind"

    const-string v3, "SDKUsage"

    invoke-virtual {v1, v2, v3}, Lcom/here/sdk/hacwrapper/HacProperties;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2404
    const-string v2, "sdk-data-01"

    invoke-static {v2, v1}, Lcom/here/sdk/hacwrapper/HacAnalytics;->trackWithoutAmplitude(Ljava/lang/String;Lcom/here/sdk/hacwrapper/HacProperties;)V

    .line 2405
    invoke-static {}, Lcom/here/sdk/hacwrapper/HacAnalytics;->flush()V

    .line 2408
    iget-object v1, p0, Lcom/nokia/maps/s;->I:Ljava/lang/Runnable;

    const-wide/32 v2, 0x249f0

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;J)V

    .line 2411
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nokia/maps/s;->o:Lorg/json/JSONObject;

    .line 2412
    invoke-direct {p0}, Lcom/nokia/maps/s;->s()V

    .line 2414
    invoke-direct {p0}, Lcom/nokia/maps/s;->t()V

    .line 2416
    return-object v0

    .line 2376
    :cond_4
    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 2377
    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2378
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2379
    const-string v2, "Last"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2380
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 2393
    :cond_6
    const-string v0, "Analytics flush called in non-debug mode"

    goto :goto_1

    .line 2399
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
