.class public Lcom/nokia/maps/AudioPlayer;
.super Ljava/lang/Object;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/AudioPlayer$b;,
        Lcom/nokia/maps/AudioPlayer$a;,
        Lcom/nokia/maps/AudioPlayer$d;,
        Lcom/nokia/maps/AudioPlayer$c;,
        Lcom/nokia/maps/AudioPlayer$e;
    }
.end annotation


# static fields
.field private static g:Z

.field private static h:I

.field private static i:F


# instance fields
.field private a:Landroid/speech/tts/TextToSpeech;

.field private b:Landroid/content/Context;

.field private c:Ljava/util/Locale;

.field private final d:Ljava/util/concurrent/Semaphore;

.field private final e:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/nokia/maps/AudioPlayer$c;

.field private j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

.field private volatile k:Lcom/nokia/maps/AudioPlayer$e;

.field private final l:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private m:Lcom/nokia/maps/AudioPlayer$d;

.field private final n:Ljava/lang/Object;

.field private o:Ljava/util/Timer;

.field private volatile p:Z

.field private final q:Lcom/nokia/maps/AudioPlayer$b$a;

.field private final r:Lcom/nokia/maps/AudioPlayer$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nokia/maps/AudioPlayer;->g:Z

    .line 64
    const/4 v0, 0x3

    sput v0, Lcom/nokia/maps/AudioPlayer;->h:I

    .line 66
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/nokia/maps/AudioPlayer;->i:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v2, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    .line 54
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->c:Ljava/util/Locale;

    .line 55
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v1}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->d:Ljava/util/concurrent/Semaphore;

    .line 56
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->e:Ljava/util/LinkedList;

    .line 69
    iput-object v2, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    .line 80
    sget-object v0, Lcom/nokia/maps/AudioPlayer$e;->a:Lcom/nokia/maps/AudioPlayer$e;

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    .line 334
    new-instance v0, Lcom/nokia/maps/AudioPlayer$1;

    invoke-direct {v0, p0}, Lcom/nokia/maps/AudioPlayer$1;-><init>(Lcom/nokia/maps/AudioPlayer;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->l:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 523
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    .line 550
    iput-boolean v3, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    .line 584
    new-instance v0, Lcom/nokia/maps/AudioPlayer$2;

    invoke-direct {v0, p0}, Lcom/nokia/maps/AudioPlayer$2;-><init>(Lcom/nokia/maps/AudioPlayer;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->q:Lcom/nokia/maps/AudioPlayer$b$a;

    .line 597
    new-instance v0, Lcom/nokia/maps/AudioPlayer$b;

    invoke-direct {v0, v2}, Lcom/nokia/maps/AudioPlayer$b;-><init>(Lcom/nokia/maps/AudioPlayer$1;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->r:Lcom/nokia/maps/AudioPlayer$b;

    .line 87
    iput-object p1, p0, Lcom/nokia/maps/AudioPlayer;->b:Landroid/content/Context;

    .line 89
    new-instance v0, Lcom/nokia/maps/AudioPlayer$c;

    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->d:Ljava/util/concurrent/Semaphore;

    iget-object v2, p0, Lcom/nokia/maps/AudioPlayer;->e:Ljava/util/LinkedList;

    invoke-direct {v0, v1, v2}, Lcom/nokia/maps/AudioPlayer$c;-><init>(Ljava/util/concurrent/Semaphore;Ljava/util/LinkedList;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    .line 90
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    invoke-virtual {v0}, Lcom/nokia/maps/AudioPlayer$c;->start()V

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/AudioPlayer;Lcom/nokia/maps/AudioPlayer$e;)Lcom/nokia/maps/AudioPlayer$e;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    return-object p1
.end method

.method static synthetic a(Lcom/nokia/maps/AudioPlayer;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 460
    iget-object v6, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v6

    .line 461
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TTS engine is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 465
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v1, Lcom/nokia/maps/AudioPlayer$e;->b:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v1, :cond_1

    .line 466
    const-string v0, "AudioPlayer"

    const-string v1, "waiting for TTS engine to finish initialization"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    monitor-exit v6

    .line 505
    :goto_0
    return-void

    .line 470
    :cond_1
    const-string v0, "AudioPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TTS is playing ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v1, Lcom/nokia/maps/AudioPlayer$e;->c:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v1, :cond_5

    .line 475
    iget-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    if-nez v0, :cond_3

    .line 476
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    .line 477
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->j()V

    .line 480
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 481
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 482
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;

    if-eqz v0, :cond_2

    .line 483
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/nokia/maps/AudioPlayer$d;->a:Z

    .line 486
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    .line 487
    new-instance v0, Lcom/nokia/maps/AudioPlayer$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/nokia/maps/AudioPlayer$d;-><init>(Lcom/nokia/maps/AudioPlayer;Lcom/nokia/maps/AudioPlayer$1;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;

    const-wide/16 v2, 0xc8

    const-wide/16 v4, 0xc8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497
    :goto_1
    :try_start_3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 498
    const-string v1, "streamType"

    sget v2, Lcom/nokia/maps/AudioPlayer;->h:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    sget v1, Lcom/nokia/maps/AudioPlayer;->i:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    .line 500
    const-string v1, "volume"

    sget v2, Lcom/nokia/maps/AudioPlayer;->i:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    :cond_4
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 504
    :cond_5
    monitor-exit v6

    goto/16 :goto_0

    .line 491
    :catch_0
    move-exception v0

    .line 493
    const-string v1, "AudioPlayer"

    const-string v2, "exception %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private a([Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 534
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ">> playFiles - # of files = %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 537
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->e:Ljava/util/LinkedList;

    monitor-enter v1

    .line 538
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 539
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "No audio files"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 545
    return-void

    .line 542
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/nokia/maps/AudioPlayer;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/nokia/maps/AudioPlayer;)Ljava/util/Locale;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->c:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic c(Lcom/nokia/maps/AudioPlayer;)Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/nokia/maps/AudioPlayer;->h:I

    return v0
.end method

.method static synthetic d(Lcom/nokia/maps/AudioPlayer;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->k()V

    return-void
.end method

.method static synthetic e()F
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/nokia/maps/AudioPlayer;->i:F

    return v0
.end method

.method static synthetic e(Lcom/nokia/maps/AudioPlayer;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/nokia/maps/AudioPlayer;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/nokia/maps/AudioPlayer;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    return v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    invoke-virtual {v0}, Lcom/nokia/maps/AudioPlayer$c;->a()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 101
    const-string v0, "AudioPlayer"

    const-string v1, "TTS is shut down"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :cond_1
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 327
    const-string v0, "AudioPlayer"

    const-string v1, "initializing TTS engine"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 329
    :try_start_0
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/nokia/maps/AudioPlayer;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/nokia/maps/AudioPlayer;->l:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-direct {v0, v2, v3}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    .line 330
    sget-object v0, Lcom/nokia/maps/AudioPlayer$e;->b:Lcom/nokia/maps/AudioPlayer$e;

    iput-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    .line 331
    monitor-exit v1

    .line 332
    return-void

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 5

    .prologue
    .line 404
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 405
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v2, Lcom/nokia/maps/AudioPlayer$e;->c:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v2, :cond_0

    .line 409
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 410
    const-string v2, "streamType"

    sget v3, Lcom/nokia/maps/AudioPlayer;->h:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    const-string v2, "volume"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v2, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    const-string v3, "A"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 414
    :cond_0
    monitor-exit v1

    .line 415
    return-void

    .line 414
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->r:Lcom/nokia/maps/AudioPlayer$b;

    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->q:Lcom/nokia/maps/AudioPlayer$b$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/AudioPlayer$b;->a(Lcom/nokia/maps/AudioPlayer$b$a;)V

    .line 578
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->r:Lcom/nokia/maps/AudioPlayer$b;

    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->q:Lcom/nokia/maps/AudioPlayer$b$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/AudioPlayer$b;->b(Lcom/nokia/maps/AudioPlayer$b$a;)V

    .line 582
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 646
    sget v0, Lcom/nokia/maps/AudioPlayer;->h:I

    return v0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 650
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Audio Volume has to be between [0.0f,1.0f] or DEFAULT_AUDIO_VOLUME"

    invoke-static {v0, v1}, Lcom/nokia/maps/ef;->a(ZLjava/lang/String;)V

    .line 655
    sput p1, Lcom/nokia/maps/AudioPlayer;->i:F

    .line 656
    return-void

    .line 650
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 642
    sput p1, Lcom/nokia/maps/AudioPlayer;->h:I

    .line 643
    return-void
.end method

.method public a(Lcom/here/android/mpa/guidance/AudioPlayerDelegate;)V
    .locals 0

    .prologue
    .line 710
    invoke-virtual {p0}, Lcom/nokia/maps/AudioPlayer;->c()V

    .line 711
    iput-object p1, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    .line 712
    return-void
.end method

.method public a(Lcom/nokia/maps/AudioPlayer$a;)V
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->r:Lcom/nokia/maps/AudioPlayer$b;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/AudioPlayer$b;->a(Ljava/lang/ref/WeakReference;)V

    .line 568
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/AudioPlayer$c;->a(Lcom/nokia/maps/AudioPlayer$a;)V

    .line 569
    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 381
    if-eqz p1, :cond_2

    .line 382
    iput-object p1, p0, Lcom/nokia/maps/AudioPlayer;->c:Ljava/util/Locale;

    .line 383
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v2, Lcom/nokia/maps/AudioPlayer$e;->a:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v2, :cond_3

    .line 387
    :cond_0
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->h()V

    .line 399
    :cond_1
    :goto_0
    monitor-exit v1

    .line 401
    :cond_2
    return-void

    .line 389
    :cond_3
    const-string v0, "AudioPlayer"

    const-string v2, "TTS trying to set locale %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v2, Lcom/nokia/maps/AudioPlayer$e;->c:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v2, :cond_1

    .line 391
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/nokia/maps/AudioPlayer;->c:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v0

    .line 392
    if-eq v0, v5, :cond_4

    if-eqz v0, :cond_4

    if-ne v0, v6, :cond_5

    .line 394
    :cond_4
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->i()V

    .line 396
    :cond_5
    const-string v2, "AudioPlayer"

    const-string v3, "setting TTS language to %s, with result %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 399
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 659
    sget v0, Lcom/nokia/maps/AudioPlayer;->i:F

    return v0
.end method

.method public b(Ljava/util/Locale;)I
    .locals 3

    .prologue
    .line 418
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 419
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->k:Lcom/nokia/maps/AudioPlayer$e;

    sget-object v2, Lcom/nokia/maps/AudioPlayer$e;->c:Lcom/nokia/maps/AudioPlayer$e;

    if-ne v0, v2, :cond_0

    .line 420
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    monitor-exit v1

    .line 422
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    monitor-exit v1

    goto :goto_0

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 7

    .prologue
    .line 664
    iget-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    if-eqz v0, :cond_1

    .line 665
    iget-object v1, p0, Lcom/nokia/maps/AudioPlayer;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 666
    :try_start_0
    iget-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    if-eqz v0, :cond_0

    .line 667
    const-string v0, "AudioPlayer"

    const-string v2, "stopping TTS"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 668
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->a:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 670
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 671
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;

    invoke-virtual {v0}, Lcom/nokia/maps/AudioPlayer$d;->cancel()Z

    .line 672
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->m:Lcom/nokia/maps/AudioPlayer$d;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/nokia/maps/AudioPlayer$d;->a:Z

    .line 674
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/AudioPlayer;->p:Z

    .line 675
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->k()V

    .line 677
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    iget-boolean v0, v0, Lcom/nokia/maps/AudioPlayer$c;->b:Z

    if-eqz v0, :cond_4

    .line 681
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    iget-object v1, v0, Lcom/nokia/maps/AudioPlayer$c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 682
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    iget-boolean v0, v0, Lcom/nokia/maps/AudioPlayer$c;->b:Z

    if-eqz v0, :cond_3

    .line 684
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    iget-object v0, v0, Lcom/nokia/maps/AudioPlayer$c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 686
    :try_start_2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 693
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 694
    :catch_0
    move-exception v0

    .line 696
    :try_start_4
    const-string v3, "AudioPlayer"

    const-string v4, "Exception occurred mp.release(): %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 697
    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    .line 696
    invoke-static {v3, v4, v5}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 704
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 677
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 701
    :cond_2
    :try_start_6
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/nokia/maps/AudioPlayer$c;->b:Z

    .line 702
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->f:Lcom/nokia/maps/AudioPlayer$c;

    invoke-static {v0}, Lcom/nokia/maps/AudioPlayer$c;->a(Lcom/nokia/maps/AudioPlayer$c;)V

    .line 704
    :cond_3
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 706
    :cond_4
    return-void

    .line 687
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/nokia/maps/AudioPlayer;->g()V

    .line 320
    const-string v0, "AudioPlayer"

    const-string v1, "Audio Player finalized"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    return-void
.end method

.method public playFiles([Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 528
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    invoke-interface {v0, p1}, Lcom/here/android/mpa/guidance/AudioPlayerDelegate;->playFiles([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 529
    :cond_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/AudioPlayer;->a([Ljava/lang/String;)V

    .line 531
    :cond_1
    return-void
.end method

.method public playText(Ljava/lang/String;)V
    .locals 5
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x22

    .line 433
    const-string v0, "audio="

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 435
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 436
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 437
    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 436
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 438
    new-array v1, v4, [Ljava/lang/String;

    aput-object v0, v1, v3

    invoke-virtual {p0, v1}, Lcom/nokia/maps/AudioPlayer;->playFiles([Ljava/lang/String;)V

    .line 440
    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 441
    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 442
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v4, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    const-string v0, "\\"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 451
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Missed some keywords"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 454
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer;->j:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    invoke-interface {v0, p1}, Lcom/here/android/mpa/guidance/AudioPlayerDelegate;->playText(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    :cond_3
    invoke-direct {p0, p1}, Lcom/nokia/maps/AudioPlayer;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
