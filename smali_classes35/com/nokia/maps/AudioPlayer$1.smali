.class Lcom/nokia/maps/AudioPlayer$1;
.super Ljava/lang/Object;
.source "AudioPlayer.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nokia/maps/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nokia/maps/AudioPlayer;


# direct methods
.method constructor <init>(Lcom/nokia/maps/AudioPlayer;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 339
    const-string v0, "AudioPlayer"

    const-string v1, "TTS initialization status = %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    invoke-static {v0}, Lcom/nokia/maps/AudioPlayer;->a(Lcom/nokia/maps/AudioPlayer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 356
    if-nez p1, :cond_1

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    sget-object v2, Lcom/nokia/maps/AudioPlayer$e;->c:Lcom/nokia/maps/AudioPlayer$e;

    invoke-static {v0, v2}, Lcom/nokia/maps/AudioPlayer;->a(Lcom/nokia/maps/AudioPlayer;Lcom/nokia/maps/AudioPlayer$e;)Lcom/nokia/maps/AudioPlayer$e;

    .line 359
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    iget-object v2, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    invoke-static {v2}, Lcom/nokia/maps/AudioPlayer;->b(Lcom/nokia/maps/AudioPlayer;)Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/nokia/maps/AudioPlayer;->a(Ljava/util/Locale;)V

    .line 365
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    invoke-static {v0}, Lcom/nokia/maps/AudioPlayer;->c(Lcom/nokia/maps/AudioPlayer;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.tts"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 366
    const-string v0, "AudioPlayer"

    const-string v2, "TTS speach rate adjusted to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x3f666666    # 0.9f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    iget-object v0, p0, Lcom/nokia/maps/AudioPlayer$1;->a:Lcom/nokia/maps/AudioPlayer;

    invoke-static {v0}, Lcom/nokia/maps/AudioPlayer;->c(Lcom/nokia/maps/AudioPlayer;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    const v2, 0x3f666666    # 0.9f

    invoke-virtual {v0, v2}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    return-void

    .line 371
    :cond_1
    :try_start_2
    const-string v0, "AudioPlayer"

    const-string v2, "Could not initialize TextToSpeech."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 373
    :catch_0
    move-exception v0

    .line 374
    :try_start_3
    const-string v2, "AudioPlayer"

    const-string v3, "TTS engine initialization failed!"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 376
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
