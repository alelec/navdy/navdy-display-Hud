.class Lcom/nokia/maps/bw$k;
.super Lcom/nokia/maps/MapsEngine$k$a;
.source "MapLoaderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nokia/maps/bw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "k"
.end annotation


# instance fields
.field public a:Z

.field b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/nokia/maps/bw$h;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/nokia/maps/bw$i;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic d:Lcom/nokia/maps/bw;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/Runnable;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/nokia/maps/bw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 861
    iput-object p1, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine$k$a;-><init>()V

    .line 862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nokia/maps/bw$k;->a:Z

    .line 864
    iput-boolean v1, p0, Lcom/nokia/maps/bw$k;->f:Z

    .line 865
    iput-boolean v1, p0, Lcom/nokia/maps/bw$k;->g:Z

    .line 866
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->h:Ljava/lang/Runnable;

    .line 867
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->i:Ljava/util/List;

    .line 868
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->b:Landroid/util/SparseArray;

    .line 869
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->c:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/nokia/maps/bw;Lcom/nokia/maps/bw$1;)V
    .locals 0

    .prologue
    .line 861
    invoke-direct {p0, p1}, Lcom/nokia/maps/bw$k;-><init>(Lcom/nokia/maps/bw;)V

    return-void
.end method

.method private a(Lcom/nokia/maps/MapPackageSelection;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 966
    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/nokia/maps/MapPackageSelection;->getPackageCount()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 967
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->d(I)Z

    move-result v2

    .line 968
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->f(I)Z

    move-result v4

    .line 969
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->g(I)Z

    move-result v5

    .line 970
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->getPackageIdFromIndex(I)I

    move-result v6

    .line 972
    if-ne v2, v3, :cond_0

    if-nez v4, :cond_0

    .line 973
    iget-object v2, p0, Lcom/nokia/maps/bw$k;->i:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 976
    :cond_0
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->getPackageChildrenCount(I)I

    move-result v2

    if-lez v2, :cond_6

    .line 977
    new-instance v7, Lcom/nokia/maps/bw$h;

    invoke-direct {v7}, Lcom/nokia/maps/bw$h;-><init>()V

    .line 978
    new-instance v8, Lcom/nokia/maps/bw$i;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    if-nez v4, :cond_1

    if-eqz v5, :cond_4

    :cond_1
    move v2, v3

    .line 979
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v8, v9, v2}, Lcom/nokia/maps/bw$i;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 981
    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->getPackageChildrenIndices(I)[I

    move-result-object v4

    .line 982
    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_5

    aget v9, v4, v2

    .line 983
    invoke-virtual {p1, v9}, Lcom/nokia/maps/MapPackageSelection;->f(I)Z

    move-result v10

    .line 984
    invoke-virtual {p1, v9}, Lcom/nokia/maps/MapPackageSelection;->hasChildPackageInstalled(I)Z

    move-result v11

    .line 985
    invoke-virtual {p1, v9}, Lcom/nokia/maps/MapPackageSelection;->getPackageIdFromIndex(I)I

    move-result v9

    .line 987
    if-eqz v10, :cond_2

    .line 988
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/nokia/maps/bw$h;->a(Ljava/lang/Integer;)V

    .line 990
    :cond_2
    if-eqz v11, :cond_3

    .line 991
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/nokia/maps/bw$h;->b(Ljava/lang/Integer;)V

    .line 993
    :cond_3
    iget-object v10, p0, Lcom/nokia/maps/bw$k;->c:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v2, v1

    .line 978
    goto :goto_1

    .line 995
    :cond_5
    iget-object v2, p0, Lcom/nokia/maps/bw$k;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 966
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 998
    :cond_7
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 943
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/bw$k;->f:Z

    .line 944
    const-string v0, ""

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->e:Ljava/lang/String;

    .line 945
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->endODMLInstallation()V

    .line 946
    return-void
.end method

.method public a(Lcom/nokia/maps/MapPackageSelection;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 898
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 899
    iput-boolean p4, p0, Lcom/nokia/maps/bw$k;->g:Z

    .line 900
    if-ne p3, v1, :cond_3

    .line 901
    iput-object p2, p0, Lcom/nokia/maps/bw$k;->e:Ljava/lang/String;

    .line 902
    iput-boolean v1, p0, Lcom/nokia/maps/bw$k;->f:Z

    .line 903
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->m(Lcom/nokia/maps/bw;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 904
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 905
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 906
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 907
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->n(Lcom/nokia/maps/bw;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 909
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->g:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 911
    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/bw$k;->a(Lcom/nokia/maps/MapPackageSelection;)V

    .line 912
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    iget-object v1, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v1}, Lcom/nokia/maps/bw;->m(Lcom/nokia/maps/bw;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;Lcom/nokia/maps/MapPackageSelection;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 925
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->l(Lcom/nokia/maps/bw;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 926
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->cancelMapInstallation()Z

    .line 927
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0, v2}, Lcom/nokia/maps/bw;->e(Lcom/nokia/maps/bw;Z)Z

    .line 935
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->h:Ljava/lang/Runnable;

    .line 936
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nokia/maps/bw$k;->h:Ljava/lang/Runnable;

    .line 937
    if-eqz v0, :cond_2

    .line 938
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 940
    :cond_2
    return-void

    .line 913
    :catch_0
    move-exception v0

    .line 914
    iput-boolean v2, p0, Lcom/nokia/maps/bw$k;->f:Z

    .line 915
    const-string v0, ""

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->e:Ljava/lang/String;

    .line 916
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->m(Lcom/nokia/maps/bw;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 917
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 918
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 919
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 920
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->n(Lcom/nokia/maps/bw;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 930
    :cond_3
    const-string v0, ""

    iput-object v0, p0, Lcom/nokia/maps/bw$k;->e:Ljava/lang/String;

    .line 931
    iput-boolean v2, p0, Lcom/nokia/maps/bw$k;->f:Z

    goto :goto_1
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 872
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->f:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 875
    if-eqz p1, :cond_0

    .line 876
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->l(Lcom/nokia/maps/bw;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 879
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->cancelMapInstallation()Z

    .line 880
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->e(Lcom/nokia/maps/bw;Z)Z

    .line 893
    :cond_1
    :goto_0
    return-void

    .line 885
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->a(Lcom/nokia/maps/MapsEngine$k;)V

    .line 886
    iput-object p1, p0, Lcom/nokia/maps/bw$k;->h:Ljava/lang/Runnable;

    .line 887
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->d:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine;->beginODMLInstallation()Z

    move-result v0

    iput-boolean v0, p0, Lcom/nokia/maps/bw$k;->a:Z

    .line 888
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->a:Z

    if-nez v0, :cond_1

    .line 890
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 953
    iget-object v0, p0, Lcom/nokia/maps/bw$k;->i:Ljava/util/List;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 957
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->f:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 961
    iget-boolean v0, p0, Lcom/nokia/maps/bw$k;->g:Z

    return v0
.end method
