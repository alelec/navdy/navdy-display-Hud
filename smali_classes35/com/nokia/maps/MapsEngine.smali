.class public Lcom/nokia/maps/MapsEngine;
.super Lcom/nokia/maps/MapServiceClient;
.source "MapsEngine.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/MapsEngine$m;,
        Lcom/nokia/maps/MapsEngine$h;,
        Lcom/nokia/maps/MapsEngine$k;,
        Lcom/nokia/maps/MapsEngine$f;,
        Lcom/nokia/maps/MapsEngine$b;,
        Lcom/nokia/maps/MapsEngine$a;,
        Lcom/nokia/maps/MapsEngine$i;,
        Lcom/nokia/maps/MapsEngine$l;,
        Lcom/nokia/maps/MapsEngine$j;,
        Lcom/nokia/maps/MapsEngine$n;,
        Lcom/nokia/maps/MapsEngine$c;,
        Lcom/nokia/maps/MapsEngine$d;,
        Lcom/nokia/maps/MapsEngine$e;,
        Lcom/nokia/maps/MapsEngine$g;
    }
.end annotation


# static fields
.field private static final E:Ljava/lang/Object;

.field private static volatile F:Lcom/nokia/maps/MapsEngine;

.field private static G:Landroid/content/Context;

.field private static H:Z

.field private static final I:Ljava/lang/String;

.field private static J:Ljava/lang/String;

.field private static K:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static L:Ljava/lang/String;

.field private static M:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static O:Z

.field private static P:Lcom/here/android/mpa/common/OnEngineInitListener;

.field private static Q:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static R:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static Z:Z

.field private static aa:Z

.field static h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nokia/maps/MapsEngine$g;",
            ">;"
        }
    .end annotation
.end field

.field static i:Ljava/lang/String;

.field static j:Ljava/lang/String;

.field static k:Ljava/lang/String;

.field static l:Ljava/lang/String;

.field static m:Z

.field static n:I

.field private static o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static t:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapsEngine$l;",
            ">;"
        }
    .end annotation
.end field

.field private static u:Lcom/nokia/maps/dl;

.field private static v:Z

.field private static w:Lcom/nokia/maps/MapsEngine$e;

.field private static final x:Lcom/nokia/maps/MapsEngine$d;

.field private static y:Lcom/nokia/maps/MapsEngine$c;

.field private static z:Ljava/lang/String;


# instance fields
.field private A:Lcom/nokia/maps/ci;

.field private B:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field private D:Z

.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/nokia/maps/MapsEngine$h;",
            ">;>;"
        }
    .end annotation
.end field

.field private S:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapsEngine$b;",
            ">;"
        }
    .end annotation
.end field

.field private final T:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapsEngine$f;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapsEngine$k;",
            ">;"
        }
    .end annotation
.end field

.field private V:Lcom/nokia/maps/MapsEngine$m;

.field private W:Lcom/nokia/maps/be;

.field private X:Z

.field private Y:Lcom/nokia/maps/ci$a;

.field private nativeptr:I
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation
.end field

.field private p:[Ljava/lang/String;

.field private q:[Ljava/lang/String;

.field private r:Lcom/nokia/maps/MapsEngine$a;

.field private s:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->o:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 91
    sput-object v3, Lcom/nokia/maps/MapsEngine;->u:Lcom/nokia/maps/dl;

    .line 93
    sput-boolean v5, Lcom/nokia/maps/MapsEngine;->v:Z

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    .line 119
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "crypto_here"

    invoke-direct {v1, v2, v5}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "ssl_here"

    invoke-direct {v1, v2, v5}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "NuanceVocalizer"

    invoke-direct {v1, v2, v4}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "os_adaptation.context"

    invoke-direct {v1, v2, v4}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "os_adaptation.network"

    invoke-direct {v1, v2, v4}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v0, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    new-instance v1, Lcom/nokia/maps/MapsEngine$g;

    const-string v2, "MAPSJNI"

    invoke-direct {v1, v2, v5}, Lcom/nokia/maps/MapsEngine$g;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/nokia/maps/MapsEngine$e;->a:Lcom/nokia/maps/MapsEngine$e;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    .line 162
    sget-object v0, Lcom/nokia/maps/MapsEngine$d;->a:Lcom/nokia/maps/MapsEngine$d;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->x:Lcom/nokia/maps/MapsEngine$d;

    .line 187
    sget-object v0, Lcom/nokia/maps/MapsEngine$c;->a:Lcom/nokia/maps/MapsEngine$c;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    .line 191
    const-string v0, ""

    sput-object v0, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    .line 211
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    .line 212
    sput-object v3, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 213
    sput-object v3, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    .line 214
    sput-boolean v4, Lcom/nokia/maps/MapsEngine;->H:Z

    .line 216
    const-class v0, Lcom/nokia/maps/MapsEngine;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    .line 219
    sput-object v3, Lcom/nokia/maps/MapsEngine;->i:Ljava/lang/String;

    .line 220
    sput-object v3, Lcom/nokia/maps/MapsEngine;->j:Ljava/lang/String;

    .line 221
    sput-object v3, Lcom/nokia/maps/MapsEngine;->k:Ljava/lang/String;

    .line 223
    sput-object v3, Lcom/nokia/maps/MapsEngine;->J:Ljava/lang/String;

    .line 224
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 225
    sput-object v3, Lcom/nokia/maps/MapsEngine;->l:Ljava/lang/String;

    .line 305
    sput-object v3, Lcom/nokia/maps/MapsEngine;->L:Ljava/lang/String;

    .line 307
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 312
    sput-boolean v5, Lcom/nokia/maps/MapsEngine;->O:Z

    .line 349
    sput-object v3, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 1070
    sput-boolean v5, Lcom/nokia/maps/MapsEngine;->m:Z

    .line 1072
    const/4 v0, -0x1

    sput v0, Lcom/nokia/maps/MapsEngine;->n:I

    .line 1384
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1385
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->R:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2544
    sput-boolean v4, Lcom/nokia/maps/MapsEngine;->Z:Z

    .line 2586
    sput-boolean v4, Lcom/nokia/maps/MapsEngine;->aa:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 615
    invoke-direct {p0}, Lcom/nokia/maps/MapServiceClient;-><init>()V

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mwconfig_client"

    aput-object v1, v0, v2

    const-string v1, "resource.db"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->p:[Ljava/lang/String;

    .line 82
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "places"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->q:[Ljava/lang/String;

    .line 86
    iput-object v3, p0, Lcom/nokia/maps/MapsEngine;->s:Ljava/util/Locale;

    .line 207
    iput-object v3, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 208
    iput-boolean v2, p0, Lcom/nokia/maps/MapsEngine;->C:Z

    .line 209
    iput-boolean v2, p0, Lcom/nokia/maps/MapsEngine;->D:Z

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->N:Ljava/util/List;

    .line 1656
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->S:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1680
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->T:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2173
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2174
    iput-object v3, p0, Lcom/nokia/maps/MapsEngine;->V:Lcom/nokia/maps/MapsEngine$m;

    .line 2230
    new-instance v0, Lcom/nokia/maps/MapsEngine$9;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapsEngine$9;-><init>(Lcom/nokia/maps/MapsEngine;)V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->W:Lcom/nokia/maps/be;

    .line 2270
    iput-boolean v2, p0, Lcom/nokia/maps/MapsEngine;->X:Z

    .line 2480
    new-instance v0, Lcom/nokia/maps/MapsEngine$2;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapsEngine$2;-><init>(Lcom/nokia/maps/MapsEngine;)V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->Y:Lcom/nokia/maps/ci$a;

    .line 617
    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v1, Lcom/nokia/maps/MapsEngine$e;->a:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v1, :cond_0

    .line 618
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot initialize the engine twice"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    .line 623
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-static {v0}, Lcom/nokia/maps/ApplicationContext;->a(Landroid/content/Context;)Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 624
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Map Engine Creation Failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 627
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_2

    .line 628
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-static {v0}, Lcom/here/network/NetworkProtocol;->setAppContext(Landroid/content/Context;)V

    .line 631
    :cond_2
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->af()V

    .line 632
    return-void
.end method

.method public static A()V
    .locals 0

    .prologue
    .line 1444
    invoke-static {}, Lcom/nokia/maps/ci;->a()V

    .line 1445
    return-void
.end method

.method public static B()V
    .locals 0

    .prologue
    .line 1455
    invoke-static {}, Lcom/nokia/maps/ci;->b()V

    .line 1456
    return-void
.end method

.method public static E()Z
    .locals 4

    .prologue
    .line 1775
    const/4 v0, 0x0

    .line 1777
    :try_start_0
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->d()Lcom/nokia/maps/MapsEngine;

    move-result-object v1

    .line 1778
    if-eqz v1, :cond_0

    sget-object v2, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v3, Lcom/nokia/maps/MapsEngine$e;->c:Lcom/nokia/maps/MapsEngine$e;

    if-ne v2, v3, :cond_0

    .line 1779
    invoke-virtual {v1}, Lcom/nokia/maps/MapsEngine;->isOnline()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1784
    :cond_0
    :goto_0
    return v0

    .line 1781
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static J()Z
    .locals 1

    .prologue
    .line 2037
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->H:Z

    return v0
.end method

.method public static L()Z
    .locals 1

    .prologue
    .line 2530
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->v:Z

    return v0
.end method

.method static synthetic N()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic P()Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic R()Lcom/nokia/maps/MapsEngine$c;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    return-object v0
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic T()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->O:Z

    return v0
.end method

.method static synthetic U()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->aa:Z

    return v0
.end method

.method static synthetic V()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic W()Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/nokia/maps/MapsEngine;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private static X()V
    .locals 3

    .prologue
    .line 342
    sget-object v1, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 343
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$l;

    .line 344
    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$l;->a()V

    goto :goto_0

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    return-void
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 1225
    const-string v0, "CHN"

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->e(Ljava/lang/String;)V

    .line 1226
    return-void
.end method

.method private Z()V
    .locals 1

    .prologue
    .line 1229
    sget-object v0, Lcom/nokia/maps/MapsEngine$c;->f:Lcom/nokia/maps/MapsEngine$c;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    .line 1230
    const-string v0, "IND"

    sput-object v0, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    .line 1231
    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/TelephonyManager;Ljava/lang/String;SLjava/lang/String;IZLjava/lang/String;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct/range {p0 .. p15}, Lcom/nokia/maps/MapsEngine;->initEngine(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/TelephonyManager;Ljava/lang/String;SLjava/lang/String;IZLjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/here/android/mpa/common/OnEngineInitListener;)Lcom/here/android/mpa/common/OnEngineInitListener;
    .locals 0

    .prologue
    .line 68
    sput-object p0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    return-object p0
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine$e;)Lcom/nokia/maps/MapsEngine$e;
    .locals 0

    .prologue
    .line 68
    sput-object p0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)Lcom/nokia/maps/MapsEngine;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 353
    sget-object v1, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    monitor-enter v1

    .line 354
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->c:Lcom/nokia/maps/MapsEngine$e;

    if-ne v0, v2, :cond_0

    .line 355
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 356
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    monitor-exit v1

    .line 358
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;)Lcom/nokia/maps/ci;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    return-object v0
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;Lcom/nokia/maps/ci;)Lcom/nokia/maps/ci;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    return-object p1
.end method

.method static synthetic a(Lcom/nokia/maps/dl;)Lcom/nokia/maps/dl;
    .locals 0

    .prologue
    .line 68
    sput-object p0, Lcom/nokia/maps/MapsEngine;->u:Lcom/nokia/maps/dl;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V
    .locals 0

    .prologue
    .line 68
    invoke-static {p0, p1, p2}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    return-void
.end method

.method static a(Lcom/nokia/maps/MapsEngine$l;)V
    .locals 2

    .prologue
    .line 869
    if-eqz p0, :cond_0

    .line 870
    sget-object v1, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 871
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 872
    monitor-exit v1

    .line 874
    :cond_0
    return-void

    .line 872
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->createMapsEngineNative(Z)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;ZZ)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/MapsEngine;->setOnlineNative(ZZ)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1075
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->b(Ljava/util/List;)Ljava/util/List;

    .line 1077
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    const-string v1, "phone"

    .line 1079
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1080
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 1082
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 1083
    if-eqz v0, :cond_0

    .line 1084
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087
    :cond_0
    sget v0, Lcom/nokia/maps/MapsEngine;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1089
    const/4 v0, 0x0

    sget v1, Lcom/nokia/maps/MapsEngine;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1091
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapsEngine;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->f(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private aa()V
    .locals 1

    .prologue
    .line 1234
    sget-object v0, Lcom/nokia/maps/MapsEngine$c;->e:Lcom/nokia/maps/MapsEngine$c;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    .line 1235
    const-string v0, "KOR"

    sput-object v0, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    .line 1236
    return-void
.end method

.method private ab()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 2366
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->s:Ljava/util/Locale;

    return-object v0
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 2462
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;

    .line 2463
    invoke-interface {v0}, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;->onMapDataDownloadStart()V

    goto :goto_0

    .line 2465
    :cond_0
    return-void
.end method

.method private ad()V
    .locals 2

    .prologue
    .line 2468
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;

    .line 2469
    invoke-interface {v0}, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;->onMapDataDownloadInProgress()V

    goto :goto_0

    .line 2471
    :cond_0
    return-void
.end method

.method public static native addIMEICryptoString(Ljava/lang/String;)V
.end method

.method private ae()V
    .locals 2

    .prologue
    .line 2474
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;

    .line 2475
    invoke-interface {v0}, Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;->onMapDataDownloadEnd()V

    goto :goto_0

    .line 2477
    :cond_0
    return-void
.end method

.method private af()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2569
    const-string v0, "java.vm.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2572
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 2573
    if-lt v1, v2, :cond_0

    .line 2575
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2576
    if-ltz v0, :cond_0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 2578
    const/4 v0, 0x1

    sput-boolean v0, Lcom/nokia/maps/MapsEngine;->Z:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2584
    :cond_0
    :goto_0
    return-void

    .line 2580
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 932
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "IN"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 934
    sget-object v1, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    monitor-enter v1

    .line 935
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "->> synchronized() ..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 937
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-nez v0, :cond_0

    .line 938
    invoke-static {p0}, Lcom/nokia/maps/MapsEngine;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    new-instance v0, Lcom/nokia/maps/MapsEngine;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapsEngine;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 943
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "<<- synchronized() ..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 944
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 946
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "OUT - s_instance %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-nez v0, :cond_1

    const-string v0, "NULL"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    return-object v0

    .line 944
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 946
    :cond_1
    const-string v0, "OK"

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)Lcom/nokia/maps/MapsEngine;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 368
    sget-object v2, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "IN - s_initState=%s listener=0x%08x"

    new-array v4, v12, [Ljava/lang/Object;

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$e;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    if-nez p1, :cond_1

    move v0, v8

    .line 369
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    .line 368
    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 370
    const-string v0, "Cannot initialize with a null Context"

    invoke-static {p0, v0}, Lcom/nokia/maps/ef;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Landroid/content/ContextWrapper;

    invoke-direct {v2, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "lib/libMAPSJNI.so"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 377
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 378
    if-eqz p1, :cond_0

    .line 379
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "MapsEngine#serviceInit NOT FOUND:"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_LIBRARIES:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Library "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not found."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-static {v1, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 380
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 385
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 581
    :goto_1
    return-object v0

    .line 369
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 389
    :cond_2
    const-string v0, "com.here.network.NetworkProtocol"

    .line 392
    :try_start_0
    const-string v0, "com.here.network.NetworkProtocol"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    sget-object v10, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    monitor-enter v10

    .line 405
    :try_start_1
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-nez v0, :cond_3

    .line 406
    invoke-static {p0}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;

    .line 410
    :cond_3
    sget-boolean v0, Lcom/nokia/maps/BaseNativeObject;->H:Z

    if-nez v0, :cond_5

    .line 411
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "MapsEngine#serviceInit some native libs are missing"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 412
    sget-object v0, Lcom/nokia/maps/MapsEngine$e;->e:Lcom/nokia/maps/MapsEngine$e;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Native libraries missing: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/nokia/maps/MapsEngine;->o:Ljava/util/List;

    const-string v3, ", "

    .line 414
    invoke-static {v2, v3}, Lcom/nokia/maps/eu;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Please refer to the user guide for details about proper project setup."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 416
    sget-object v2, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_LIBRARIES:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .line 417
    invoke-static {v2, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 416
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 418
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 419
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 420
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_1

    .line 393
    :catch_0
    move-exception v0

    .line 394
    if-eqz p1, :cond_4

    .line 395
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "MapsEngine#serviceInit NOT FOUND:"

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "com.here.network.NetworkProtocol"

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_LIBRARIES:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    const-string v2, "com.here.network.NetworkProtocol class not found."

    .line 398
    invoke-static {v1, v2, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 396
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 401
    :cond_4
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    goto :goto_1

    .line 423
    :cond_5
    :try_start_2
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->X()V

    .line 428
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/ApplicationContext;->g()Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    sget-object v2, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq v0, v2, :cond_6

    .line 429
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "MapsEngine#serviceInit Init ERROR"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 430
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/ApplicationContext;->g()Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 432
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    monitor-exit v10

    goto/16 :goto_1

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 435
    :cond_6
    :try_start_3
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->b:Lcom/nokia/maps/MapsEngine$e;

    if-ne v0, v2, :cond_9

    .line 436
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "s_initState=%s adding listener(0x%08x) to listener list."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    .line 437
    invoke-virtual {v4}, Lcom/nokia/maps/MapsEngine$e;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    if-nez p1, :cond_8

    :goto_2
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 436
    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 438
    if-eqz p1, :cond_7

    .line 441
    sget-object v0, Lcom/nokia/maps/MapsEngine;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    :cond_7
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    monitor-exit v10

    goto/16 :goto_1

    .line 437
    :cond_8
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v8

    goto :goto_2

    .line 446
    :cond_9
    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->a:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v2, :cond_a

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->e:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v2, :cond_a

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->d:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v2, :cond_a

    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->f:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v2, :cond_a

    .line 452
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Cannot initialize the engine twice"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_a
    sget-object v0, Lcom/nokia/maps/MapsEngine$e;->b:Lcom/nokia/maps/MapsEngine$e;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    .line 459
    invoke-static {p0}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 462
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 463
    sget-object v2, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    invoke-direct {v2, v0}, Lcom/nokia/maps/MapsEngine;->c(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 464
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing permissions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    .line 465
    invoke-static {v0, v2}, Lcom/nokia/maps/eu;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Please refer to the user guide for details about proper project setup."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 467
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_PERMISSION:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .line 468
    invoke-static {v1, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 467
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 469
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 470
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    monitor-exit v10

    goto/16 :goto_1

    .line 474
    :cond_b
    invoke-static {}, Lcom/nokia/maps/MapSettings;->j()Ljava/lang/String;

    move-result-object v0

    .line 475
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 476
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "foo.bar"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 478
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_c

    .line 479
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "ERROR: Failed to delete test file"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/nokia/maps/bp;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    :cond_c
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_d

    .line 482
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "ERROR: Failed to mkdirs() for the diskcache path"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/nokia/maps/bp;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 486
    :cond_d
    :try_start_4
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-nez v0, :cond_e

    .line 487
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "ERROR: Failed to create test file, checking for existence"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 494
    :cond_e
    :try_start_5
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 495
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "ERROR: Problem removing test file from disk cache"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v9

    .line 499
    :goto_3
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_f

    if-nez v0, :cond_11

    .line 500
    :cond_f
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v2, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/nokia/maps/MapsEngine;->a(Landroid/content/Context;)V

    .line 501
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->FILE_RW_ERROR:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    const-string v2, "SDK cache is missing."

    .line 502
    invoke-static {v0, v2}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 501
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 503
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 504
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 505
    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v0, v1

    goto/16 :goto_1

    .line 490
    :catch_1
    move-exception v0

    .line 491
    :try_start_6
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "ERROR: IOException trying to create test file"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 494
    :try_start_7
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 495
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "ERROR: Problem removing test file from disk cache"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    goto :goto_3

    .line 494
    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_10

    .line 495
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "ERROR: Problem removing test file from disk cache"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_10
    throw v0

    .line 509
    :cond_11
    sget-object v0, Lcom/nokia/maps/MapsEngine;->l:Ljava/lang/String;

    if-nez v0, :cond_12

    .line 510
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->a:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_16

    move v0, v9

    :goto_4
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->l:Ljava/lang/String;

    .line 513
    :cond_12
    sget-object v0, Lcom/nokia/maps/MapsEngine;->i:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 514
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->i:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_17

    move v0, v9

    :goto_5
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->i:Ljava/lang/String;

    .line 517
    :cond_13
    sget-object v0, Lcom/nokia/maps/MapsEngine;->j:Ljava/lang/String;

    if-nez v0, :cond_14

    .line 518
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->j:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_18

    move v0, v9

    :goto_6
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->j:Ljava/lang/String;

    .line 521
    :cond_14
    sget-object v0, Lcom/nokia/maps/MapsEngine;->k:Ljava/lang/String;

    if-nez v0, :cond_15

    .line 522
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->k:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_19

    move v0, v9

    :goto_7
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->k:Ljava/lang/String;

    .line 526
    :cond_15
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    invoke-static {}, Lcom/nokia/maps/MapSettings;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nokia/maps/MapsEngine;->d(Ljava/lang/String;)V

    .line 528
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 529
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    invoke-direct {v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 577
    :goto_8
    monitor-exit v10
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 579
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "OUT - s_initState=%s listener=0x%08x"

    new-array v2, v12, [Ljava/lang/Object;

    sget-object v3, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    invoke-virtual {v3}, Lcom/nokia/maps/MapsEngine$e;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    if-nez p1, :cond_1d

    .line 580
    :goto_9
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    .line 579
    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 581
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    goto/16 :goto_1

    :cond_16
    move v0, v8

    .line 510
    goto/16 :goto_4

    :cond_17
    move v0, v8

    .line 514
    goto :goto_5

    :cond_18
    move v0, v8

    .line 518
    goto :goto_6

    :cond_19
    move v0, v8

    .line 522
    goto :goto_7

    .line 532
    :cond_1a
    :try_start_8
    invoke-static {}, Lcom/nokia/maps/MapSettings;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 533
    invoke-static {}, Lcom/nokia/maps/MapServiceClient;->b()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 534
    const-string v0, "Wrong intent name for isolated disk cache."

    .line 535
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    const-string v1, "%s: %s. See \'Embedding the Map Service\' section of User Guide."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-class v4, Lcom/here/android/mpa/service/MapService;

    .line 538
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 536
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 540
    sget-object v2, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->WRONG_SERVICE_INTENT_NAME:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 541
    invoke-static {v2, v1, v3}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 540
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 544
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 545
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;
    :try_end_8
    .catch Lcom/nokia/maps/MapServiceClient$a; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_8

    .line 554
    :catch_2
    move-exception v0

    .line 555
    :try_start_9
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "Map Service was NOT FOUND. Return error."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/here/android/mpa/service/MapService;

    .line 557
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is missing from the AndroidManifest.xml file. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Please refer to the user guide for details about "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "proper project setup."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 561
    sget-object v2, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_SERVICE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .line 562
    invoke-static {v2, v1, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 561
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 563
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 564
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_8

    .line 547
    :cond_1b
    :try_start_a
    sput-object p1, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 548
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v1, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    .line 549
    invoke-static {}, Lcom/nokia/maps/MapSettings;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/nokia/maps/MapsEngine;->l:Ljava/lang/String;

    sget-object v4, Lcom/nokia/maps/MapsEngine;->i:Ljava/lang/String;

    sget-object v5, Lcom/nokia/maps/MapsEngine;->j:Ljava/lang/String;

    .line 550
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->n()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    sget-object v11, Lcom/nokia/maps/MapsEngine$c;->a:Lcom/nokia/maps/MapsEngine$c;

    if-ne v7, v11, :cond_1c

    move v7, v8

    .line 548
    :goto_a
    invoke-virtual/range {v0 .. v7}, Lcom/nokia/maps/MapsEngine;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;S)V
    :try_end_a
    .catch Lcom/nokia/maps/MapServiceClient$a; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_8

    .line 565
    :catch_3
    move-exception v0

    .line 567
    :try_start_b
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "singleton.connectService() failed.  Error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 568
    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 567
    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->UNKNOWN:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    const-string v2, "Unknown error occurred."

    .line 571
    invoke-static {v1, v2, v0}, Lcom/nokia/maps/av;->a(Lcom/here/android/mpa/common/OnEngineInitListener$Error;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v0

    .line 570
    invoke-static {p0, v0, p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 573
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    .line 574
    const/4 v0, 0x0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_8

    .line 550
    :cond_1c
    :try_start_c
    sget-object v7, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    iget-short v7, v7, Lcom/nokia/maps/MapsEngine$c;->g:S
    :try_end_c
    .catch Lcom/nokia/maps/MapServiceClient$a; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_a

    .line 580
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v8

    goto/16 :goto_9

    :cond_1e
    move v0, v8

    goto/16 :goto_3

    :cond_1f
    move v0, v9

    goto/16 :goto_3
.end method

.method static synthetic b(Lcom/nokia/maps/MapsEngine;)Lcom/nokia/maps/MapsEngine;
    .locals 0

    .prologue
    .line 68
    sput-object p0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 68
    sput-object p0, Lcom/nokia/maps/MapsEngine;->L:Ljava/lang/String;

    return-object p0
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1114
    :try_start_0
    const-string v0, "android.telephony.MSimTelephonyManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 1115
    const-class v0, Landroid/content/Context;

    const-string v3, "MSIM_TELEPHONY_SERVICE"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 1116
    sget-object v3, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1117
    sget-object v3, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1118
    const-string v0, "getSimOperator"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v1, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    move v1, v2

    .line 1120
    :goto_0
    const/4 v0, 0x2

    if-ge v1, v0, :cond_1

    .line 1121
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    invoke-virtual {v4, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1122
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 1123
    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_0

    .line 1124
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1125
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_6

    .line 1120
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1128
    :catch_0
    move-exception v0

    .line 1129
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "Couldn\'t find the MSimTelephonyManager class on this device"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1145
    :cond_1
    :goto_1
    return-object p1

    .line 1130
    :catch_1
    move-exception v0

    .line 1131
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Couldn\'t find the MSIM_TELEPHONY_SERVICE field from Context: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 1132
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 1131
    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1133
    :catch_2
    move-exception v0

    .line 1134
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Illegal argument for getSystemService: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1135
    :catch_3
    move-exception v0

    .line 1136
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Illegal access for getSystemService: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1137
    :catch_4
    move-exception v0

    .line 1138
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Couldn\'t find the getSimOperator method: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1139
    :catch_5
    move-exception v0

    .line 1140
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Failed to invoke the getSimOperator method: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1141
    :catch_6
    move-exception v0

    .line 1142
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Failed to invoke the getSimOperator method for unknown reason: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 1143
    invoke-virtual {v0}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 1142
    invoke-static {v1, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener$Error;Lcom/here/android/mpa/common/OnEngineInitListener;)V
    .locals 4

    .prologue
    .line 2373
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2375
    if-nez p2, :cond_0

    sget-object v0, Lcom/nokia/maps/MapsEngine;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396
    :goto_0
    return-void

    .line 2379
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$10;

    invoke-direct {v0, p2, p1}, Lcom/nokia/maps/MapsEngine$10;-><init>(Lcom/here/android/mpa/common/OnEngineInitListener;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V

    .line 2393
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2394
    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private b(Lcom/here/android/mpa/common/OnEngineInitListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 585
    new-instance v0, Lcom/nokia/maps/MapsEngine$j;

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/nokia/maps/MapsEngine$j;-><init>(Lcom/nokia/maps/MapsEngine;Lcom/nokia/maps/MapsEngine;Lcom/here/android/mpa/common/OnEngineInitListener;Z)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapsEngine$j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 586
    return-void
.end method

.method static b(Lcom/nokia/maps/MapsEngine$l;)V
    .locals 2

    .prologue
    .line 877
    if-eqz p0, :cond_0

    .line 878
    sget-object v1, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 879
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->t:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 880
    monitor-exit v1

    .line 882
    :cond_0
    return-void

    .line 880
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/nokia/maps/MapsEngine;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->e(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Z)V
    .locals 5

    .prologue
    .line 1704
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "online: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1705
    sput-boolean p0, Lcom/nokia/maps/MapsEngine;->O:Z

    .line 1707
    :try_start_0
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->d()Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    .line 1708
    if-eqz v0, :cond_0

    sget-object v1, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->c:Lcom/nokia/maps/MapsEngine$e;

    if-ne v1, v2, :cond_0

    .line 1709
    invoke-direct {v0, p0}, Lcom/nokia/maps/MapsEngine;->d(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1713
    :cond_0
    :goto_0
    return-void

    .line 1711
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/nokia/maps/MapsEngine;Z)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->d(Z)Z

    move-result v0

    return v0
.end method

.method public static c()Lcom/nokia/maps/MapsEngine$e;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/nokia/maps/MapsEngine;->w:Lcom/nokia/maps/MapsEngine$e;

    return-object v0
.end method

.method static synthetic c(Lcom/nokia/maps/MapsEngine;)Lcom/nokia/maps/be;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->W:Lcom/nokia/maps/be;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1094
    const/4 v0, 0x0

    .line 1095
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v2, :cond_0

    .line 1096
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1097
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1099
    :cond_0
    return-object v0
.end method

.method static synthetic c(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 68
    invoke-static {p0}, Lcom/nokia/maps/MapsEngine;->g(Landroid/content/Context;)V

    return-void
.end method

.method public static c(Z)V
    .locals 0

    .prologue
    .line 2534
    sput-boolean p0, Lcom/nokia/maps/MapsEngine;->v:Z

    .line 2535
    return-void
.end method

.method static synthetic c(Lcom/nokia/maps/MapsEngine;Z)Z
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/nokia/maps/MapsEngine;->D:Z

    return p1
.end method

.method private c(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2400
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2401
    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2402
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2403
    const-string v1, "android.permission.ACCESS_WIFI_STATE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2404
    const-string v1, "android.permission.INTERNET"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2407
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2408
    sget-object v2, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 2410
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2414
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private native containsChinaMcc([I)Z
.end method

.method private native containsIndiaMcc([I)Z
.end method

.method private synchronized native declared-synchronized continueMapInstallationNative()Z
.end method

.method private native createMapsEngineNative(Z)V
.end method

.method static d()Lcom/nokia/maps/MapsEngine;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 958
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "IN"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 960
    sget-object v1, Lcom/nokia/maps/MapsEngine;->E:Ljava/lang/Object;

    monitor-enter v1

    .line 961
    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "->> synchronized() ..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 963
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-nez v0, :cond_0

    .line 964
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "MapsEngine singleton cannot be used unless it has been initalized"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 969
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 968
    :cond_0
    :try_start_1
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "<<- synchronized() ..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 969
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 971
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v2, "OUT - s_instance %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    if-nez v0, :cond_1

    const-string v0, "NULL"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 972
    sget-object v0, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    return-object v0

    .line 971
    :cond_1
    const-string v0, "OK"

    goto :goto_0
.end method

.method static synthetic d(Lcom/nokia/maps/MapsEngine;)Ljava/util/Locale;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->ab()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1149
    sget-object v0, Lcom/nokia/maps/MapsEngine;->x:Lcom/nokia/maps/MapsEngine$d;

    sget-object v1, Lcom/nokia/maps/MapsEngine$d;->c:Lcom/nokia/maps/MapsEngine$d;

    if-ne v0, v1, :cond_0

    .line 1150
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->Y()V

    .line 1206
    :goto_0
    return-void

    .line 1151
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->x:Lcom/nokia/maps/MapsEngine$d;

    sget-object v1, Lcom/nokia/maps/MapsEngine$d;->b:Lcom/nokia/maps/MapsEngine$d;

    if-ne v0, v1, :cond_1

    .line 1152
    const-string v0, "DEF"

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1154
    :cond_1
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->isSouthKoreaEndpointsEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1155
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->aa()V

    goto :goto_0

    .line 1158
    :cond_2
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->getMapDataCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1160
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1163
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1164
    invoke-direct {p0, v4}, Lcom/nokia/maps/MapsEngine;->a(Ljava/util/List;)V

    .line 1166
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1171
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1194
    :cond_4
    :goto_1
    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1195
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->Y()V

    goto :goto_0

    .line 1172
    :catch_0
    move-exception v0

    .line 1173
    const-string v0, "DEF"

    goto :goto_1

    .line 1176
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [I

    .line 1178
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1179
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    move v1, v3

    .line 1180
    goto :goto_2

    .line 1182
    :cond_6
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->m:Z

    if-eqz v0, :cond_7

    invoke-direct {p0, v5}, Lcom/nokia/maps/MapsEngine;->containsChinaMcc([I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1183
    const-string v0, "CHN"

    goto :goto_1

    .line 1184
    :cond_7
    invoke-direct {p0, v5}, Lcom/nokia/maps/MapsEngine;->containsIndiaMcc([I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1185
    const-string v0, "IND"

    goto :goto_1

    .line 1188
    :cond_8
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->getMccCountryCode(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1196
    :cond_9
    const-string v1, "IND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1197
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->Z()V

    goto/16 :goto_0

    .line 1199
    :cond_a
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1200
    :cond_b
    const-string v0, "DEF"

    .line 1202
    :cond_c
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 316
    .line 317
    invoke-static {p0}, Lcom/nokia/maps/bk;->a(Landroid/content/Context;)Z

    move-result v0

    .line 319
    if-eqz v0, :cond_2

    .line 320
    sget-object v1, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "Loading %d libraries ..."

    new-array v5, v2, [Ljava/lang/Object;

    sget-object v6, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1, v4, v5}, Lcom/nokia/maps/bp;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    sget-object v1, Lcom/nokia/maps/MapsEngine;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$g;

    .line 322
    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/nokia/maps/bk;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 325
    if-nez v4, :cond_4

    .line 326
    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$g;->b()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 327
    sget-object v6, Lcom/nokia/maps/MapsEngine;->o:Ljava/util/List;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 332
    :goto_1
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    move v0, v2

    :goto_2
    move v1, v0

    .line 333
    goto :goto_0

    :cond_0
    move v0, v2

    .line 329
    goto :goto_1

    :cond_1
    move v0, v3

    .line 332
    goto :goto_2

    .line 335
    :cond_2
    sget-object v1, Lcom/nokia/maps/MapsEngine;->o:Ljava/util/List;

    sget-object v2, Lcom/nokia/maps/bk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 337
    :cond_3
    sput-boolean v1, Lcom/nokia/maps/BaseNativeObject;->H:Z

    .line 338
    return v1

    :cond_4
    move v0, v4

    goto :goto_1
.end method

.method private d(Z)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1726
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->T:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$f;

    .line 1727
    invoke-interface {v0, p1}, Lcom/nokia/maps/MapsEngine$f;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1729
    :catch_0
    move-exception v0

    .line 1730
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "onForcedOnlineChanged exception"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1733
    :cond_0
    if-ne p1, v2, :cond_2

    .line 1734
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    if-nez v0, :cond_1

    .line 1736
    :try_start_1
    new-instance v0, Lcom/nokia/maps/MapsEngine$a;

    sget-object v3, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/nokia/maps/MapsEngine$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    .line 1738
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1739
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapsEngine;->a(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1740
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Setting service online Failed!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    move v0, v2

    .line 1764
    :goto_2
    return v0

    .line 1743
    :catch_1
    move-exception v0

    .line 1744
    sget-object v3, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "Exception occured - %s."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1745
    goto :goto_2

    .line 1751
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    if-eqz v0, :cond_3

    .line 1752
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$a;->a()V

    .line 1753
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    .line 1756
    :cond_3
    invoke-direct {p0, v1, v1}, Lcom/nokia/maps/MapsEngine;->setOnlineNative(ZZ)V

    .line 1758
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1759
    invoke-virtual {p0, v1}, Lcom/nokia/maps/MapsEngine;->a(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1760
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Setting service offline Failed!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private native destroyMapsEngineNative()V
.end method

.method public static e()Lcom/nokia/maps/dl;
    .locals 1

    .prologue
    .line 989
    sget-object v0, Lcom/nokia/maps/MapsEngine;->u:Lcom/nokia/maps/dl;

    return-object v0
.end method

.method static synthetic e(Lcom/nokia/maps/MapsEngine;)Ljava/util/List;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->N:Ljava/util/List;

    return-object v0
.end method

.method private e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 885
    new-instance v0, Lcom/nokia/maps/MapsEngine$i;

    invoke-direct {v0, p1}, Lcom/nokia/maps/MapsEngine$i;-><init>(Landroid/content/Context;)V

    .line 886
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1209
    sget-object v0, Lcom/nokia/maps/MapsEngine$c;->b:Lcom/nokia/maps/MapsEngine$c;

    sput-object v0, Lcom/nokia/maps/MapsEngine;->y:Lcom/nokia/maps/MapsEngine$c;

    .line 1210
    sput-object p1, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    .line 1211
    return-void
.end method

.method public static f()Landroid/content/Context;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/Internal;
    .end annotation

    .prologue
    .line 1060
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/nokia/maps/MapsEngine;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->S:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private f(Landroid/content/Context;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1001
    .line 1003
    new-instance v5, Ljava/io/File;

    invoke-static {}, Lcom/nokia/maps/MapSettings;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1004
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1006
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 1010
    :goto_0
    iget-object v4, p0, Lcom/nokia/maps/MapsEngine;->p:[Ljava/lang/String;

    array-length v6, v4

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_0

    aget-object v7, v4, v3

    .line 1011
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v5, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1012
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    move v0, v2

    .line 1019
    :cond_0
    new-instance v6, Ljava/io/File;

    invoke-static {}, Lcom/nokia/maps/MapSettings;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1020
    iget-object v7, p0, Lcom/nokia/maps/MapsEngine;->q:[Ljava/lang/String;

    array-length v8, v7

    move v4, v2

    move v3, v1

    :goto_2
    if-ge v4, v8, :cond_4

    aget-object v9, v7, v4

    .line 1021
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1022
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    move v3, v2

    .line 1020
    :cond_1
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1010
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1024
    :cond_3
    if-nez v0, :cond_1

    .line 1027
    :try_start_0
    invoke-static {v10}, Lcom/nokia/maps/MapsEngineResourceManager;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1028
    :catch_0
    move-exception v0

    .line 1029
    sget-object v3, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v4, "ERROR: deleting dir: %s: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    .line 1030
    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 1029
    invoke-static {v3, v4, v5}, Lcom/nokia/maps/bp;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1055
    :goto_4
    return v2

    .line 1037
    :cond_4
    if-eqz v0, :cond_5

    if-nez v3, :cond_7

    .line 1038
    :cond_5
    invoke-virtual {v5}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1040
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1039
    invoke-static {p1, v0}, Lcom/nokia/maps/MapsEngineResourceManager;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 1041
    if-nez v0, :cond_6

    .line 1042
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Failed to deploy resources into location: %s"

    new-array v1, v1, [Ljava/lang/Object;

    .line 1043
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    .line 1042
    invoke-static {v0, v3, v1}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 1047
    :cond_6
    sget-object v0, Lcom/nokia/maps/MapsEngine;->G:Landroid/content/Context;

    .line 1048
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 1047
    invoke-static {v0, v2}, Lcom/nokia/maps/SupplementaryResourceManager;->a(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_7
    move v2, v1

    .line 1055
    goto :goto_4

    .line 1051
    :cond_8
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "Cannot write to directory: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method static g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1064
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->isUsingMapServerNative()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/nokia/maps/MapsEngine;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private static g(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2025
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2027
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 2028
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/nokia/maps/MapsEngine;->H:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2033
    :goto_1
    const-string v3, "MapsEngine"

    const-string v4, "Debug messages are %s"

    new-array v1, v1, [Ljava/lang/Object;

    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->H:Z

    if-eqz v0, :cond_1

    const-string v0, "ENABLED"

    :goto_2
    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, Lcom/nokia/maps/bp;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2034
    return-void

    :cond_0
    move v0, v2

    .line 2028
    goto :goto_0

    .line 2029
    :catch_0
    move-exception v0

    .line 2030
    sput-boolean v2, Lcom/nokia/maps/MapsEngine;->H:Z

    goto :goto_1

    .line 2033
    :cond_1
    const-string v0, "DISABLED"

    goto :goto_2
.end method

.method private synchronized native declared-synchronized getCompatibleMapVersionsNative()V
.end method

.method public static native getDiskCacheSize()J
.end method

.method private native getMapDataCountryCode(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private synchronized native declared-synchronized getMapVersionNative()V
.end method

.method private native getMccCountryCode(I)Ljava/lang/String;
.end method

.method public static native getPermissionStringTimeExpiry()J
.end method

.method private native getServerUrl(IZ)Ljava/lang/String;
.end method

.method static synthetic h(Lcom/nokia/maps/MapsEngine;)Lcom/nokia/maps/MapsEngine$m;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->V:Lcom/nokia/maps/MapsEngine$m;

    return-object v0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1239
    sget-object v0, Lcom/nokia/maps/MapsEngine;->z:Ljava/lang/String;

    return-object v0
.end method

.method public static i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1248
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->c:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/nokia/maps/MapsEngine;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private native initEngine(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/TelephonyManager;Ljava/lang/String;SLjava/lang/String;IZLjava/lang/String;)I
.end method

.method public static native isEval()Z
.end method

.method private native isSouthKoreaEndpointsEnabled()Z
.end method

.method private static native isUsingMapServerNative()Z
.end method

.method public static j()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1253
    sget-object v0, Lcom/nokia/maps/MapsEngine;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1254
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->d:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapsEngine;->J:Ljava/lang/String;

    .line 1257
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->J:Ljava/lang/String;

    return-object v0

    .line 1254
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/nokia/maps/MapsEngine;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/nokia/maps/MapsEngine;->D:Z

    return v0
.end method

.method public static k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1266
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->e:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic k(Lcom/nokia/maps/MapsEngine;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->ac()V

    return-void
.end method

.method public static l()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1271
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->f:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic l(Lcom/nokia/maps/MapsEngine;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->ad()V

    return-void
.end method

.method public static m()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1276
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->g:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic m(Lcom/nokia/maps/MapsEngine;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->ae()V

    return-void
.end method

.method public static n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1286
    sget-object v0, Lcom/nokia/maps/MapsEngine;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static o()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1290
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->l:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onCompatibleMapVersions([Ljava/lang/String;Z)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1851
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1852
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1853
    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/MapsEngine$k;->a([Ljava/lang/String;Z)V

    goto :goto_0

    .line 1856
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/nokia/maps/MapsEngine$3;-><init>(Lcom/nokia/maps/MapsEngine;[Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1865
    :cond_1
    return-void
.end method

.method private onInstallSelection(Lcom/nokia/maps/MapPackageSelection;)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1982
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1983
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1984
    invoke-interface {v0, p1}, Lcom/nokia/maps/MapsEngine$k;->b(Lcom/nokia/maps/MapPackageSelection;)V

    goto :goto_0

    .line 1987
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$7;

    invoke-direct {v0, p0, p1}, Lcom/nokia/maps/MapsEngine$7;-><init>(Lcom/nokia/maps/MapsEngine;Lcom/nokia/maps/MapPackageSelection;)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1996
    :cond_1
    return-void
.end method

.method private onInstallationSize(JJ)V
    .locals 7
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2000
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "diskSize=%d networkSize=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2001
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 2002
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 2003
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/nokia/maps/MapsEngine$k;->a(JJ)V

    goto :goto_0

    .line 2006
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$8;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/nokia/maps/MapsEngine$8;-><init>(Lcom/nokia/maps/MapsEngine;JJ)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 2017
    :cond_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapsEngine;->I()Z

    .line 2018
    return-void
.end method

.method private onMapVersion(Ljava/lang/String;Z)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1821
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1822
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1823
    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/MapsEngine$k;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1826
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/nokia/maps/MapsEngine$1;-><init>(Lcom/nokia/maps/MapsEngine;Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1835
    :cond_1
    return-void
.end method

.method private onNativeCrash()V
    .locals 8
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 899
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "A crash in native code occurred"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 901
    const-string v0, "NATIVE CRASH"

    const-string v1, "Call stacks ---->"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v0

    .line 904
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 905
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    .line 906
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n\nThread:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 907
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    .line 908
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 909
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\tat "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 908
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 912
    :cond_1
    const-string v0, "NATIVE CRASH"

    const-string v1, "Call stacks <----"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    return-void
.end method

.method private onODMLSelection(Lcom/nokia/maps/MapPackageSelection;Ljava/lang/String;ZZ)V
    .locals 6
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1963
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onODMLSelection:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1964
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1965
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1966
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/nokia/maps/MapsEngine$k;->a(Lcom/nokia/maps/MapPackageSelection;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 1969
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nokia/maps/MapsEngine$6;-><init>(Lcom/nokia/maps/MapsEngine;Lcom/nokia/maps/MapPackageSelection;Ljava/lang/String;ZZ)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1978
    :cond_1
    return-void
.end method

.method private onUpdateToVersionCompleted(Ljava/lang/String;I)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1920
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1921
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1922
    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/MapsEngine$k;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 1925
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/nokia/maps/MapsEngine$5;-><init>(Lcom/nokia/maps/MapsEngine;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1934
    :cond_1
    return-void
.end method

.method private onUpdateToVersionProgress(I)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1902
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1903
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapsEngine$k;

    .line 1904
    invoke-interface {v0, p1}, Lcom/nokia/maps/MapsEngine$k;->a(I)V

    goto :goto_0

    .line 1907
    :cond_0
    new-instance v0, Lcom/nokia/maps/MapsEngine$4;

    invoke-direct {v0, p0, p1}, Lcom/nokia/maps/MapsEngine$4;-><init>(Lcom/nokia/maps/MapsEngine;I)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1916
    :cond_1
    return-void
.end method

.method public static p()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1295
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->m:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1300
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->n:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1305
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->o:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1310
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->p:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native setHttpProxy(Ljava/lang/String;)V
.end method

.method private native setLocaleLanguageNative(Ljava/lang/String;)V
.end method

.method private native setOnlineNative(ZZ)V
.end method

.method public static t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1315
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->q:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1320
    sget-object v1, Lcom/nokia/maps/MapsEngine;->F:Lcom/nokia/maps/MapsEngine;

    sget-object v0, Lcom/nokia/maps/MapsEngine$n;->r:Lcom/nokia/maps/MapsEngine$n;

    invoke-virtual {v0}, Lcom/nokia/maps/MapsEngine$n;->a()I

    move-result v2

    sget-boolean v0, Lcom/nokia/maps/MapServiceClient;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/nokia/maps/MapsEngine;->getServerUrl(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private synchronized native declared-synchronized updateMapToVersionNative(Ljava/lang/String;)V
.end method


# virtual methods
.method public C()Lcom/nokia/maps/ci;
    .locals 1

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    return-object v0
.end method

.method D()Z
    .locals 1

    .prologue
    .line 1721
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->O:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized F()V
    .locals 1

    .prologue
    .line 1804
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapsEngine;->reloadMapModelEngineNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1805
    monitor-exit p0

    return-void

    .line 1804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized G()V
    .locals 1

    .prologue
    .line 1814
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->getMapVersionNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1815
    monitor-exit p0

    return-void

    .line 1814
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized H()V
    .locals 1

    .prologue
    .line 1842
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->getCompatibleMapVersionsNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1843
    monitor-exit p0

    return-void

    .line 1842
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public I()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1945
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v1, "IN"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1946
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->continueMapInstallationNative()Z

    move-result v1

    .line 1948
    sget-object v2, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    const-string v3, "OUT - returns %s"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    if-eqz v1, :cond_0

    const-string v0, "true"

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1949
    return v1

    .line 1948
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public K()V
    .locals 1

    .prologue
    .line 2355
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapsEngine;->a(Ljava/util/Locale;)V

    .line 2356
    return-void
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 2547
    sget-boolean v0, Lcom/nokia/maps/MapsEngine;->Z:Z

    return v0
.end method

.method protected a(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 604
    invoke-super {p0, p1}, Lcom/nokia/maps/MapServiceClient;->a(Landroid/content/ComponentName;)V

    .line 607
    return-void
.end method

.method protected a(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 593
    invoke-super {p0, p1, p2}, Lcom/nokia/maps/MapServiceClient;->a(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 596
    new-instance v0, Lcom/nokia/maps/MapsEngine$j;

    sget-object v1, Lcom/nokia/maps/MapsEngine;->P:Lcom/here/android/mpa/common/OnEngineInitListener;

    const/4 v2, 0x1

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/nokia/maps/MapsEngine$j;-><init>(Lcom/nokia/maps/MapsEngine;Lcom/nokia/maps/MapsEngine;Lcom/here/android/mpa/common/OnEngineInitListener;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapsEngine$j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 597
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;)V
    .locals 2

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    if-eqz v0, :cond_1

    .line 2430
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 2431
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2433
    :cond_0
    if-eqz p1, :cond_1

    .line 2434
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2435
    iget-boolean v0, p0, Lcom/nokia/maps/MapsEngine;->C:Z

    if-nez v0, :cond_1

    .line 2436
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/MapsEngine;->D:Z

    .line 2437
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    iget-object v1, p0, Lcom/nokia/maps/MapsEngine;->Y:Lcom/nokia/maps/ci$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ci;->a(Lcom/nokia/maps/ci$a;)V

    .line 2438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nokia/maps/MapsEngine;->C:Z

    .line 2442
    :cond_1
    return-void
.end method

.method public a(Lcom/nokia/maps/MapsEngine$b;)V
    .locals 1

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->S:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1660
    return-void
.end method

.method public a(Lcom/nokia/maps/MapsEngine$f;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/Internal;
    .end annotation

    .prologue
    .line 1684
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->T:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1685
    return-void
.end method

.method public a(Lcom/nokia/maps/MapsEngine$h;)V
    .locals 2

    .prologue
    .line 1459
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->N:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1460
    return-void
.end method

.method public a(Lcom/nokia/maps/MapsEngine$k;)V
    .locals 1

    .prologue
    .line 2044
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 2045
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1895
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapsEngine;->updateMapToVersionNative(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1896
    monitor-exit p0

    return-void

    .line 1895
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Locale;)V
    .locals 2

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/nokia/maps/MapsEngine;->s:Ljava/util/Locale;

    .line 2360
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2362
    :goto_0
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapsEngine;->setLocaleLanguageNative(Ljava/lang/String;)V

    .line 2363
    return-void

    .line 2360
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2361
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/here/android/mpa/common/MapEngine$OnMapDownloadListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2451
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2452
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2453
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->B:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2454
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->A:Lcom/nokia/maps/ci;

    iget-object v1, p0, Lcom/nokia/maps/MapsEngine;->Y:Lcom/nokia/maps/ci$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ci;->b(Lcom/nokia/maps/ci$a;)V

    .line 2455
    iput-boolean v2, p0, Lcom/nokia/maps/MapsEngine;->C:Z

    .line 2456
    iput-boolean v2, p0, Lcom/nokia/maps/MapsEngine;->D:Z

    .line 2459
    :cond_0
    return-void
.end method

.method public b(Lcom/nokia/maps/MapsEngine$b;)V
    .locals 1

    .prologue
    .line 1663
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->S:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1664
    return-void
.end method

.method public b(Lcom/nokia/maps/MapsEngine$f;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/Internal;
    .end annotation

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->T:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1690
    return-void
.end method

.method public b(Lcom/nokia/maps/MapsEngine$k;)V
    .locals 1

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->U:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2055
    return-void
.end method

.method public synchronized native declared-synchronized beginODMLInstallation()Z
.end method

.method public synchronized native declared-synchronized cancelCompatibleMapVersionQuery()Z
.end method

.method public synchronized native declared-synchronized cancelMapInstallation()Z
.end method

.method public synchronized native declared-synchronized endODMLInstallation()V
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 890
    iget v0, p0, Lcom/nokia/maps/MapsEngine;->nativeptr:I

    if-eqz v0, :cond_0

    .line 892
    invoke-direct {p0}, Lcom/nokia/maps/MapsEngine;->destroyMapsEngineNative()V

    .line 894
    :cond_0
    invoke-super {p0}, Lcom/nokia/maps/MapServiceClient;->finalize()V

    .line 895
    return-void
.end method

.method public native isOnline()Z
.end method

.method public synchronized native declared-synchronized pollMapData()Z
.end method

.method synchronized native declared-synchronized reloadMapModelEngineNative()V
.end method

.method public v()V
    .locals 3

    .prologue
    .line 1392
    sget-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_0

    .line 1401
    :goto_0
    return-void

    .line 1397
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    .line 1398
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->A()V

    .line 1400
    :cond_1
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resume counter value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public w()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1408
    sget-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1409
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->B()V

    .line 1413
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/nokia/maps/MapsEngine;->r:Lcom/nokia/maps/MapsEngine$a;

    if-eqz v0, :cond_1

    .line 1415
    invoke-virtual {p0, v1}, Lcom/nokia/maps/MapsEngine;->a(Z)Z

    .line 1421
    :cond_0
    :goto_0
    sget-object v0, Lcom/nokia/maps/MapsEngine;->I:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resume counter value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1422
    return-void

    .line 1417
    :cond_1
    invoke-virtual {p0, v3}, Lcom/nokia/maps/MapsEngine;->a(Z)Z

    goto :goto_0
.end method

.method public x()V
    .locals 0

    .prologue
    .line 1425
    invoke-virtual {p0}, Lcom/nokia/maps/MapsEngine;->v()V

    .line 1426
    return-void
.end method

.method public y()V
    .locals 0

    .prologue
    .line 1429
    invoke-virtual {p0}, Lcom/nokia/maps/MapsEngine;->w()V

    .line 1430
    return-void
.end method

.method public z()I
    .locals 1

    .prologue
    .line 1433
    sget-object v0, Lcom/nokia/maps/MapsEngine;->Q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method
