.class public Lcom/nokia/maps/ARLayoutControl;
.super Lcom/nokia/maps/BaseNativeObject;
.source "ARLayoutControl.java"

# interfaces
.implements Lcom/nokia/maps/l;


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation


# instance fields
.field final A:Lcom/nokia/maps/aw;

.field B:Ljava/lang/Runnable;

.field C:Ljava/lang/Runnable;

.field D:Lcom/nokia/maps/aw$a;

.field E:Lcom/nokia/maps/aw$a;

.field F:Lcom/nokia/maps/aw$a;

.field G:Lcom/nokia/maps/aw$a;

.field private final J:Lcom/nokia/maps/i;

.field private K:Lcom/here/android/mpa/ar/ARPoseReading;

.field private L:Lcom/nokia/maps/g;

.field private M:Landroid/graphics/Point;

.field private N:Landroid/graphics/Point;

.field private O:Lcom/nokia/maps/y;

.field private P:I

.field private Q:I

.field private R:Landroid/graphics/PointF;

.field private S:Landroid/graphics/PointF;

.field private T:Lcom/nokia/maps/a;

.field private U:Lcom/nokia/maps/ARSensors;

.field private V:Lcom/nokia/maps/ee;

.field private W:Landroid/content/Context;

.field private X:Lcom/nokia/maps/d;

.field private Y:Lcom/here/android/mpa/ar/ARRadarProperties;

.field private Z:Lcom/nokia/maps/MapImpl;

.field final a:Lcom/nokia/maps/aw;

.field private aa:Z

.field private ab:Lcom/nokia/maps/cs;

.field private ac:Z

.field private volatile ad:Z

.field private ae:I

.field private af:Z

.field private volatile ag:Z

.field private final ah:Lcom/nokia/maps/aw$a;

.field private ai:Ljava/lang/Runnable;

.field private aj:Ljava/lang/Runnable;

.field private ak:Ljava/lang/Runnable;

.field private al:Ljava/lang/Runnable;

.field private am:Ljava/lang/Runnable;

.field private final an:Lcom/nokia/maps/aw$a;

.field private final ao:Lcom/nokia/maps/aw$a;

.field private final ap:Lcom/nokia/maps/aw$a;

.field private final aq:Lcom/nokia/maps/aw$a;

.field private final ar:Lcom/nokia/maps/aw$a;

.field private as:Lcom/nokia/maps/aw$a;

.field private at:Lcom/nokia/maps/aw$a;

.field private au:Lcom/nokia/maps/aw$a;

.field private av:Lcom/nokia/maps/aw$a;

.field final b:Lcom/nokia/maps/aw;

.field final c:Lcom/nokia/maps/aw;

.field final d:Lcom/nokia/maps/aw;

.field final e:Lcom/nokia/maps/aw;

.field final f:Lcom/nokia/maps/aw;

.field final g:Lcom/nokia/maps/aw;

.field final h:Lcom/nokia/maps/aw;

.field final i:Lcom/nokia/maps/aw;

.field final j:Lcom/nokia/maps/aw;

.field final k:Lcom/nokia/maps/aw;

.field final l:Lcom/nokia/maps/aw;

.field final m:Lcom/nokia/maps/aw;

.field final n:Lcom/nokia/maps/aw;

.field final o:Lcom/nokia/maps/aw;

.field final p:Lcom/nokia/maps/aw;

.field q:Lcom/nokia/maps/aw;

.field final r:Lcom/nokia/maps/aw;

.field final s:Lcom/nokia/maps/aw;

.field final t:Lcom/nokia/maps/aw;

.field final u:Lcom/nokia/maps/aw;

.field final v:Lcom/nokia/maps/aw;

.field final w:Lcom/nokia/maps/aw;

.field final x:Lcom/nokia/maps/aw;

.field final y:Lcom/nokia/maps/aw;

.field final z:Lcom/nokia/maps/aw;


# direct methods
.method private constructor <init>(I)V
    .locals 4
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 347
    invoke-direct {p0}, Lcom/nokia/maps/BaseNativeObject;-><init>()V

    .line 42
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->a:Lcom/nokia/maps/aw;

    .line 47
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->b:Lcom/nokia/maps/aw;

    .line 52
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->c:Lcom/nokia/maps/aw;

    .line 57
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->d:Lcom/nokia/maps/aw;

    .line 62
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->e:Lcom/nokia/maps/aw;

    .line 67
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->f:Lcom/nokia/maps/aw;

    .line 72
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->g:Lcom/nokia/maps/aw;

    .line 77
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->h:Lcom/nokia/maps/aw;

    .line 82
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->i:Lcom/nokia/maps/aw;

    .line 87
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->j:Lcom/nokia/maps/aw;

    .line 92
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->k:Lcom/nokia/maps/aw;

    .line 97
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->l:Lcom/nokia/maps/aw;

    .line 102
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->m:Lcom/nokia/maps/aw;

    .line 107
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->n:Lcom/nokia/maps/aw;

    .line 112
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->o:Lcom/nokia/maps/aw;

    .line 117
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->p:Lcom/nokia/maps/aw;

    .line 122
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->q:Lcom/nokia/maps/aw;

    .line 128
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->r:Lcom/nokia/maps/aw;

    .line 133
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->s:Lcom/nokia/maps/aw;

    .line 138
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->t:Lcom/nokia/maps/aw;

    .line 143
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->u:Lcom/nokia/maps/aw;

    .line 149
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->v:Lcom/nokia/maps/aw;

    .line 154
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->w:Lcom/nokia/maps/aw;

    .line 159
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->x:Lcom/nokia/maps/aw;

    .line 165
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->y:Lcom/nokia/maps/aw;

    .line 171
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->z:Lcom/nokia/maps/aw;

    .line 176
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->A:Lcom/nokia/maps/aw;

    .line 181
    new-instance v0, Lcom/nokia/maps/i;

    invoke-direct {v0}, Lcom/nokia/maps/i;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    .line 186
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    .line 198
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->M:Landroid/graphics/Point;

    .line 200
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->N:Landroid/graphics/Point;

    .line 252
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    .line 262
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    .line 267
    iput-boolean v3, p0, Lcom/nokia/maps/ARLayoutControl;->aa:Z

    .line 272
    new-instance v0, Lcom/nokia/maps/cs;

    invoke-direct {v0}, Lcom/nokia/maps/cs;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ab:Lcom/nokia/maps/cs;

    .line 277
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    .line 283
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Lcom/nokia/maps/ARLayoutControl;->ae:I

    .line 296
    iput-boolean v3, p0, Lcom/nokia/maps/ARLayoutControl;->af:Z

    .line 302
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    .line 331
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$1;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$1;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ah:Lcom/nokia/maps/aw$a;

    .line 562
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$22;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$22;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->B:Ljava/lang/Runnable;

    .line 812
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$23;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$23;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ai:Ljava/lang/Runnable;

    .line 837
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$24;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$24;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->aj:Ljava/lang/Runnable;

    .line 862
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$25;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$25;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ak:Ljava/lang/Runnable;

    .line 887
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$2;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$2;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->al:Ljava/lang/Runnable;

    .line 912
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$3;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$3;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->am:Ljava/lang/Runnable;

    .line 937
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$4;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$4;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->C:Ljava/lang/Runnable;

    .line 1226
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$5;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$5;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->an:Lcom/nokia/maps/aw$a;

    .line 1246
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$6;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$6;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ao:Lcom/nokia/maps/aw$a;

    .line 1264
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$7;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$7;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ap:Lcom/nokia/maps/aw$a;

    .line 1282
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$8;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$8;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->aq:Lcom/nokia/maps/aw$a;

    .line 1295
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$9;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$9;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ar:Lcom/nokia/maps/aw$a;

    .line 1415
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$11;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$11;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->D:Lcom/nokia/maps/aw$a;

    .line 1447
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$12;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$12;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->E:Lcom/nokia/maps/aw$a;

    .line 1473
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$13;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$13;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->F:Lcom/nokia/maps/aw$a;

    .line 1493
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$14;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$14;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->G:Lcom/nokia/maps/aw$a;

    .line 1529
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$15;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$15;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->as:Lcom/nokia/maps/aw$a;

    .line 1539
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$16;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$16;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->at:Lcom/nokia/maps/aw$a;

    .line 1549
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$17;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$17;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->au:Lcom/nokia/maps/aw$a;

    .line 1559
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$18;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$18;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->av:Lcom/nokia/maps/aw$a;

    .line 348
    iput p1, p0, Lcom/nokia/maps/ARLayoutControl;->nativeptr:I

    .line 349
    return-void
.end method

.method constructor <init>(Lcom/nokia/maps/y;Lcom/nokia/maps/d;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 310
    invoke-direct {p0}, Lcom/nokia/maps/BaseNativeObject;-><init>()V

    .line 42
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->a:Lcom/nokia/maps/aw;

    .line 47
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->b:Lcom/nokia/maps/aw;

    .line 52
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->c:Lcom/nokia/maps/aw;

    .line 57
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->d:Lcom/nokia/maps/aw;

    .line 62
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->e:Lcom/nokia/maps/aw;

    .line 67
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->f:Lcom/nokia/maps/aw;

    .line 72
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->g:Lcom/nokia/maps/aw;

    .line 77
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->h:Lcom/nokia/maps/aw;

    .line 82
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->i:Lcom/nokia/maps/aw;

    .line 87
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->j:Lcom/nokia/maps/aw;

    .line 92
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->k:Lcom/nokia/maps/aw;

    .line 97
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->l:Lcom/nokia/maps/aw;

    .line 102
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->m:Lcom/nokia/maps/aw;

    .line 107
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->n:Lcom/nokia/maps/aw;

    .line 112
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->o:Lcom/nokia/maps/aw;

    .line 117
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->p:Lcom/nokia/maps/aw;

    .line 122
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->q:Lcom/nokia/maps/aw;

    .line 128
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->r:Lcom/nokia/maps/aw;

    .line 133
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->s:Lcom/nokia/maps/aw;

    .line 138
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->t:Lcom/nokia/maps/aw;

    .line 143
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->u:Lcom/nokia/maps/aw;

    .line 149
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->v:Lcom/nokia/maps/aw;

    .line 154
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->w:Lcom/nokia/maps/aw;

    .line 159
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->x:Lcom/nokia/maps/aw;

    .line 165
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->y:Lcom/nokia/maps/aw;

    .line 171
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->z:Lcom/nokia/maps/aw;

    .line 176
    new-instance v0, Lcom/nokia/maps/aw;

    invoke-direct {v0}, Lcom/nokia/maps/aw;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->A:Lcom/nokia/maps/aw;

    .line 181
    new-instance v0, Lcom/nokia/maps/i;

    invoke-direct {v0}, Lcom/nokia/maps/i;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    .line 186
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    .line 198
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->M:Landroid/graphics/Point;

    .line 200
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->N:Landroid/graphics/Point;

    .line 252
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    .line 262
    iput-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    .line 267
    iput-boolean v3, p0, Lcom/nokia/maps/ARLayoutControl;->aa:Z

    .line 272
    new-instance v0, Lcom/nokia/maps/cs;

    invoke-direct {v0}, Lcom/nokia/maps/cs;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ab:Lcom/nokia/maps/cs;

    .line 277
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    .line 283
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Lcom/nokia/maps/ARLayoutControl;->ae:I

    .line 296
    iput-boolean v3, p0, Lcom/nokia/maps/ARLayoutControl;->af:Z

    .line 302
    iput-boolean v2, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    .line 331
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$1;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$1;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ah:Lcom/nokia/maps/aw$a;

    .line 562
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$22;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$22;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->B:Ljava/lang/Runnable;

    .line 812
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$23;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$23;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ai:Ljava/lang/Runnable;

    .line 837
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$24;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$24;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->aj:Ljava/lang/Runnable;

    .line 862
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$25;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$25;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ak:Ljava/lang/Runnable;

    .line 887
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$2;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$2;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->al:Ljava/lang/Runnable;

    .line 912
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$3;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$3;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->am:Ljava/lang/Runnable;

    .line 937
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$4;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$4;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->C:Ljava/lang/Runnable;

    .line 1226
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$5;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$5;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->an:Lcom/nokia/maps/aw$a;

    .line 1246
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$6;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$6;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ao:Lcom/nokia/maps/aw$a;

    .line 1264
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$7;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$7;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ap:Lcom/nokia/maps/aw$a;

    .line 1282
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$8;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$8;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->aq:Lcom/nokia/maps/aw$a;

    .line 1295
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$9;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$9;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ar:Lcom/nokia/maps/aw$a;

    .line 1415
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$11;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$11;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->D:Lcom/nokia/maps/aw$a;

    .line 1447
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$12;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$12;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->E:Lcom/nokia/maps/aw$a;

    .line 1473
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$13;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$13;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->F:Lcom/nokia/maps/aw$a;

    .line 1493
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$14;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$14;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->G:Lcom/nokia/maps/aw$a;

    .line 1529
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$15;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$15;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->as:Lcom/nokia/maps/aw$a;

    .line 1539
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$16;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$16;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->at:Lcom/nokia/maps/aw$a;

    .line 1549
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$17;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$17;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->au:Lcom/nokia/maps/aw$a;

    .line 1559
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$18;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$18;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->av:Lcom/nokia/maps/aw$a;

    .line 311
    iput-object p1, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    .line 312
    iput-object p2, p0, Lcom/nokia/maps/ARLayoutControl;->X:Lcom/nokia/maps/d;

    .line 313
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->W:Landroid/content/Context;

    .line 315
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->createNative()V

    .line 317
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    .line 318
    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/ARLayoutControl;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/nokia/maps/ARLayoutControl;->ae:I

    return p1
.end method

.method static synthetic a(Lcom/nokia/maps/ARLayoutControl;)Lcom/nokia/maps/ARSensors;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    return-object v0
.end method

.method static synthetic a(Lcom/nokia/maps/ARLayoutControl;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/nokia/maps/ARLayoutControl;->af:Z

    return p1
.end method

.method static synthetic b(Lcom/nokia/maps/ARLayoutControl;)Lcom/nokia/maps/d;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->X:Lcom/nokia/maps/d;

    return-object v0
.end method

.method static synthetic b(Lcom/nokia/maps/ARLayoutControl;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    return p1
.end method

.method static synthetic c(Lcom/nokia/maps/ARLayoutControl;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/nokia/maps/ARLayoutControl;->ae:I

    return v0
.end method

.method private native createNative()V
.end method

.method static synthetic d(Lcom/nokia/maps/ARLayoutControl;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->af:Z

    return v0
.end method

.method private native destroyNative()V
.end method

.method static synthetic e(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->k()V

    return-void
.end method

.method static synthetic f(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->l()V

    return-void
.end method

.method private g()V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 646
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    if-eqz v0, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0}, Lcom/nokia/maps/a;->g()Landroid/graphics/PointF;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->S:Landroid/graphics/PointF;

    .line 654
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/nokia/maps/ARLayoutControl;->P:I

    .line 656
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/nokia/maps/ARLayoutControl;->Q:I

    .line 658
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget v1, p0, Lcom/nokia/maps/ARLayoutControl;->P:I

    iget v2, p0, Lcom/nokia/maps/ARLayoutControl;->Q:I

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/a;->a(II)Landroid/graphics/PointF;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->R:Landroid/graphics/PointF;

    .line 660
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    new-instance v2, Lcom/here/android/mpa/common/Size;

    iget v3, p0, Lcom/nokia/maps/ARLayoutControl;->P:I

    iget v4, p0, Lcom/nokia/maps/ARLayoutControl;->Q:I

    invoke-direct {v2, v3, v4}, Lcom/here/android/mpa/common/Size;-><init>(II)V

    iget-object v3, p0, Lcom/nokia/maps/ARLayoutControl;->R:Landroid/graphics/PointF;

    invoke-virtual {v0, v1, v2, v3}, Lcom/nokia/maps/ARSensors;->a(Lcom/nokia/maps/a;Lcom/here/android/mpa/common/Size;Landroid/graphics/PointF;)V

    .line 662
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0}, Lcom/nokia/maps/a;->f()Lcom/here/android/mpa/common/Size;

    move-result-object v0

    .line 664
    if-eqz v0, :cond_2

    iget v1, v0, Lcom/here/android/mpa/common/Size;->width:I

    if-lez v1, :cond_2

    iget v1, v0, Lcom/here/android/mpa/common/Size;->height:I

    if-gtz v1, :cond_3

    .line 667
    :cond_2
    new-instance v0, Lcom/here/android/mpa/common/Size;

    invoke-direct {v0, v9, v9}, Lcom/here/android/mpa/common/Size;-><init>(II)V

    .line 671
    :cond_3
    iget v1, p0, Lcom/nokia/maps/ARLayoutControl;->P:I

    iget v2, p0, Lcom/nokia/maps/ARLayoutControl;->Q:I

    iget-object v3, p0, Lcom/nokia/maps/ARLayoutControl;->R:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/nokia/maps/ARLayoutControl;->R:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget v5, v0, Lcom/here/android/mpa/common/Size;->width:I

    iget v6, v0, Lcom/here/android/mpa/common/Size;->height:I

    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->S:Landroid/graphics/PointF;

    iget v7, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->S:Landroid/graphics/PointF;

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/nokia/maps/ARLayoutControl;->setCameraAndLayout(IIFFIIFF)V

    .line 680
    iput-boolean v9, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->i()V

    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 1310
    new-instance v0, Lcom/nokia/maps/ARLayoutControl$10;

    invoke-direct {v0, p0}, Lcom/nokia/maps/ARLayoutControl$10;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 1325
    return-void
.end method

.method static synthetic h(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->j()V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1332
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    move-result-object v0

    .line 1333
    iget-object v1, v0, Lcom/nokia/maps/g;->d:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->D:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1334
    iget-object v1, v0, Lcom/nokia/maps/g;->a:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->E:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1335
    iget-object v1, v0, Lcom/nokia/maps/g;->b:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->F:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1336
    iget-object v0, v0, Lcom/nokia/maps/g;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->G:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1337
    return-void
.end method

.method static synthetic i(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->m()V

    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1344
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    move-result-object v0

    .line 1345
    iget-object v1, v0, Lcom/nokia/maps/g;->d:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->D:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1346
    iget-object v1, v0, Lcom/nokia/maps/g;->a:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->E:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1347
    iget-object v1, v0, Lcom/nokia/maps/g;->b:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->F:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1348
    iget-object v0, v0, Lcom/nokia/maps/g;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->G:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1349
    return-void
.end method

.method static synthetic j(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->n()V

    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1356
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    move-result-object v0

    .line 1357
    iget-object v1, v0, Lcom/nokia/maps/g;->d:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->av:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1358
    iget-object v1, v0, Lcom/nokia/maps/g;->a:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->as:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1359
    iget-object v1, v0, Lcom/nokia/maps/g;->b:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->at:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1360
    iget-object v0, v0, Lcom/nokia/maps/g;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->au:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 1361
    return-void
.end method

.method static synthetic k(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->g()V

    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 1368
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    move-result-object v0

    .line 1369
    iget-object v1, v0, Lcom/nokia/maps/g;->d:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->av:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1370
    iget-object v1, v0, Lcom/nokia/maps/g;->a:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->as:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1371
    iget-object v1, v0, Lcom/nokia/maps/g;->b:Lcom/nokia/maps/aw;

    iget-object v2, p0, Lcom/nokia/maps/ARLayoutControl;->at:Lcom/nokia/maps/aw$a;

    invoke-virtual {v1, v2}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1372
    iget-object v0, v0, Lcom/nokia/maps/g;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->au:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 1373
    return-void
.end method

.method static synthetic l(Lcom/nokia/maps/ARLayoutControl;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    return v0
.end method

.method private m()V
    .locals 0

    .prologue
    .line 1387
    return-void
.end method

.method static synthetic m(Lcom/nokia/maps/ARLayoutControl;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    return v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 1403
    return-void
.end method

.method static synthetic n(Lcom/nokia/maps/ARLayoutControl;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->h()V

    return-void
.end method

.method static synthetic o(Lcom/nokia/maps/ARLayoutControl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->M:Landroid/graphics/Point;

    return-object v0
.end method

.method private onPose(Lcom/nokia/maps/ARPoseReadingImpl;)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->r:Lcom/nokia/maps/aw;

    invoke-virtual {v0}, Lcom/nokia/maps/aw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1136
    :goto_0
    return-void

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    if-nez v0, :cond_1

    .line 1130
    invoke-static {p1}, Lcom/nokia/maps/ARPoseReadingImpl;->a(Lcom/nokia/maps/ARPoseReadingImpl;)Lcom/here/android/mpa/ar/ARPoseReading;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    .line 1135
    :goto_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->r:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1132
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    invoke-static {v0}, Lcom/nokia/maps/ARPoseReadingImpl;->a(Lcom/here/android/mpa/ar/ARPoseReading;)Lcom/nokia/maps/ARPoseReadingImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ARPoseReadingImpl;->b(Lcom/nokia/maps/ARPoseReadingImpl;)V

    goto :goto_1
.end method

.method private onProjectionCameraUpdated()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->A:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1151
    return-void
.end method

.method static synthetic p(Lcom/nokia/maps/ARLayoutControl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->N:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic q(Lcom/nokia/maps/ARLayoutControl;)Lcom/nokia/maps/y;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    return-object v0
.end method


# virtual methods
.method a()Lcom/here/android/mpa/ar/ARPoseReading;
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->K:Lcom/here/android/mpa/ar/ARPoseReading;

    return-object v0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 1183
    iput p1, p0, Lcom/nokia/maps/ARLayoutControl;->P:I

    .line 1184
    iput p2, p0, Lcom/nokia/maps/ARLayoutControl;->Q:I

    .line 1189
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setLayoutSize(II)V

    .line 1190
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/nokia/maps/ARLayoutControl;->glContextEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195
    :goto_0
    return-void

    .line 1191
    :catch_0
    move-exception v0

    .line 1192
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroySensors()V

    .line 1193
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroyCamera()V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1174
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/nokia/maps/ARLayoutControl;->glContextEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1179
    :goto_0
    return-void

    .line 1175
    :catch_0
    move-exception v0

    .line 1176
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroySensors()V

    .line 1177
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroyCamera()V

    goto :goto_0
.end method

.method a(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    .line 357
    if-eqz p1, :cond_0

    .line 358
    invoke-virtual {p0, p1}, Lcom/nokia/maps/ARLayoutControl;->setMapNative(Lcom/nokia/maps/MapImpl;)V

    .line 360
    :cond_0
    return-void
.end method

.method a(Lcom/nokia/maps/ee;)V
    .locals 2

    .prologue
    .line 324
    iput-object p1, p0, Lcom/nokia/maps/ARLayoutControl;->V:Lcom/nokia/maps/ee;

    .line 325
    iget-object v0, p1, Lcom/nokia/maps/ee;->a:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ah:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 326
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1516
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    if-eqz v0, :cond_0

    .line 1517
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    .line 1518
    iput-boolean v1, p0, Lcom/nokia/maps/ARLayoutControl;->af:Z

    .line 1519
    invoke-virtual {p0, v1}, Lcom/nokia/maps/ARLayoutControl;->startOrientationAnimation(Z)V

    .line 1522
    :cond_0
    iput-boolean p1, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    .line 1523
    invoke-virtual {p0, p1}, Lcom/nokia/maps/ARLayoutControl;->setOrientationAnimation(Z)V

    .line 1524
    return-void
.end method

.method a(ZZ)V
    .locals 0

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/nokia/maps/ARLayoutControl;->aa:Z

    .line 368
    invoke-virtual {p0, p1, p2}, Lcom/nokia/maps/ARLayoutControl;->setMapAutoPitchNative(ZZ)V

    .line 369
    return-void
.end method

.method native addARObject(Lcom/nokia/maps/ARObjectImpl;)V
.end method

.method native addARObject(Lcom/nokia/maps/ARPolylineObjectImpl;)V
.end method

.method native addARViewObject(Lcom/nokia/maps/ARModelObjectImpl;)V
.end method

.method native applicationIsReady()V
.end method

.method b()Lcom/nokia/maps/g;
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->L:Lcom/nokia/maps/g;

    if-nez v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->a()Lcom/nokia/maps/g;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->L:Lcom/nokia/maps/g;

    .line 1160
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->L:Lcom/nokia/maps/g;

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->d()V

    .line 1200
    return-void
.end method

.method native cameraPreviewStarted(Z)V
.end method

.method native cameraStarted(Z)V
.end method

.method native cameraStopped(Z)V
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1205
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/nokia/maps/ARLayoutControl;->glContextEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1210
    :goto_0
    return-void

    .line 1206
    :catch_0
    move-exception v0

    .line 1207
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroySensors()V

    .line 1208
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroyCamera()V

    goto :goto_0
.end method

.method native defocus()V
.end method

.method native depress(J)V
.end method

.method native destroy()V
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1216
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/nokia/maps/ARLayoutControl;->glContextEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221
    :goto_0
    return-void

    .line 1217
    :catch_0
    move-exception v0

    .line 1218
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroySensors()V

    .line 1219
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToDestroyCamera()V

    goto :goto_0
.end method

.method native enableDownIcons(Z)V
.end method

.method native enableRadar(Z)V
.end method

.method native enableStateMachineTraces(Z)V
.end method

.method f()Lcom/nokia/maps/ARSensors;
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->destroyNative()V

    .line 377
    return-void
.end method

.method native focus(J)V
.end method

.method native geoTo3dPosition(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/common/Vector3f;)Z
.end method

.method native getAnimationDelay(I)J
.end method

.method native getAnimationDuration(I)J
.end method

.method native getAnimationInterpolator(I)I
.end method

.method native getBackIconSize()Lcom/here/android/mpa/common/Size;
.end method

.method native getBuildingInfo(Landroid/graphics/PointF;)Lcom/nokia/maps/ARBuildingInfoImpl;
.end method

.method native getCameraMaxZoomScaleUpView()F
.end method

.method native getDownIconOpacity()F
.end method

.method native getDownViewMaxOpacity()F
.end method

.method native getDownViewMinOpacity()F
.end method

.method native getDownViewPitchThreshold()F
.end method

.method native getFilterCoeff(I)F
.end method

.method native getFilterSize(I)I
.end method

.method native getFixedAltitude()F
.end method

.method native getFlyRotateDeg(I)F
.end method

.method native getFrontIconSize()Lcom/here/android/mpa/common/Size;
.end method

.method native getInfoAnimationMinWidthFactor()F
.end method

.method native getIntroAnimationMode()I
.end method

.method native getIntroAnimationTime()J
.end method

.method native getMapAutoGeoCenter()Z
.end method

.method native getMapAutoHeading()Z
.end method

.method native getMapAutoPitch()Z
.end method

.method native getMapAutoTfc()Z
.end method

.method native getMapAutoZoom()Z
.end method

.method native getMaxZoomScale()F
.end method

.method native getMinPitchDownView()F
.end method

.method native getNonSelectedItemsOpacity()F
.end method

.method native getObjects(Landroid/graphics/Point;)[J
.end method

.method native getObjectsRect(Landroid/graphics/Point;Landroid/graphics/Point;)[J
.end method

.method native getOcclusionOpacity()F
.end method

.method native getProjectionType(J)I
.end method

.method native getScreenViewPoint()Landroid/graphics/PointF;
.end method

.method native getSelectedIconSize()Lcom/here/android/mpa/common/Size;
.end method

.method native getSelectedItemMaxViewAngle()F
.end method

.method native getSelectedItemOpacity()F
.end method

.method native getSensorsWaitTimeout()J
.end method

.method native getTiltUpMaxTime()J
.end method

.method native getTiltUpMinTime()J
.end method

.method native getUpViewPitchThreshold()F
.end method

.method native getUseDownIconOpacity()Z
.end method

.method native glContextEvent(I)V
.end method

.method native initProjector()V
.end method

.method native isCameraZoomEnabledUpView()Z
.end method

.method native isEdgeDetectionEnabled()Z
.end method

.method native isMapAsPoseReadingSource()Z
.end method

.method native isOccluded(Lcom/nokia/maps/ARObjectImpl;)Z
.end method

.method native isOcclusionEnabled()Z
.end method

.method native isPitchLockedUpView()Z
.end method

.method native isPoseEngineHeadingUsed()Z
.end method

.method native isShowGridEnabled()Z
.end method

.method native isStateMachineTracesEnabled()Z
.end method

.method native isVisible(Lcom/nokia/maps/ARObjectImpl;)Z
.end method

.method native memoryCheck()V
.end method

.method onCameraLiveSceneStart()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 849
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->b:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 856
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ak:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 857
    return-void
.end method

.method onCameraLiveSceneStop()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 874
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->c:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 881
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->al:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 882
    return-void
.end method

.method onCameraPlaybackSceneStart()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 963
    return-void
.end method

.method onCameraPlaybackSceneStop()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 970
    return-void
.end method

.method onCameraRecSceneStart()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 949
    return-void
.end method

.method onCameraRecSceneStop()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 956
    return-void
.end method

.method onEglSwapBuffers()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 384
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->e()V

    .line 385
    return-void
.end method

.method onFirstLiveSightFrame()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 773
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    check-cast v0, Lcom/nokia/maps/an;

    invoke-virtual {v0}, Lcom/nokia/maps/an;->l()V

    .line 774
    return-void
.end method

.method onGetPitch(F)F
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ab:Lcom/nokia/maps/cs;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/cs;->a(F)V

    .line 1090
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->z:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ab:Lcom/nokia/maps/cs;

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1091
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ab:Lcom/nokia/maps/cs;

    invoke-virtual {v0}, Lcom/nokia/maps/cs;->a()F

    move-result v0

    return v0
.end method

.method onItemRemoved(J)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 994
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->X:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(J)V

    .line 995
    return-void
.end method

.method onLastLiveSightFrame()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 785
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->x:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 786
    return-void
.end method

.method onLivesightStatus(I)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1077
    sget-object v0, Lcom/here/android/mpa/ar/ARController$Error;->NONE:Lcom/here/android/mpa/ar/ARController$Error;

    invoke-virtual {v0}, Lcom/here/android/mpa/ar/ARController$Error;->ordinal()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1078
    invoke-static {}, Lcom/here/android/mpa/ar/ARController$Error;->values()[Lcom/here/android/mpa/ar/ARController$Error;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->w:Lcom/nokia/maps/aw;

    invoke-static {}, Lcom/here/android/mpa/ar/ARController$Error;->values()[Lcom/here/android/mpa/ar/ARController$Error;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1081
    :cond_0
    return-void
.end method

.method onMapSceneStart()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 793
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->d:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 800
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->ai:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 804
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ag:Z

    if-eqz v0, :cond_0

    .line 805
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->h()V

    .line 807
    :cond_0
    return-void
.end method

.method onMapSceneStop()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 824
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->e:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 831
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->aj:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 832
    return-void
.end method

.method onPostPresent()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 986
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->v:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 987
    return-void
.end method

.method onPreDraw()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->s:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1003
    return-void
.end method

.method onPreDrawMap(FFLcom/nokia/maps/GeoCoordinateImpl;)V
    .locals 8
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iput p1, v0, Lcom/nokia/maps/i;->a:F

    .line 1015
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iput p2, v0, Lcom/nokia/maps/i;->b:F

    .line 1016
    if-eqz p3, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iget-object v0, v0, Lcom/nokia/maps/i;->c:Lcom/here/android/mpa/common/GeoCoordinate;

    if-nez v0, :cond_2

    .line 1018
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    .line 1019
    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v4

    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->c()D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    iput-object v1, v0, Lcom/nokia/maps/i;->c:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 1038
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->aa:Z

    if-eqz v0, :cond_1

    .line 1039
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p2}, Lcom/nokia/maps/MapImpl;->b(F)V

    .line 1043
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->D()V

    .line 1044
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Z:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->E()V

    .line 1046
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->t:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1047
    return-void

    .line 1021
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iget-object v0, v0, Lcom/nokia/maps/i;->c:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;->setLatitude(D)V

    .line 1022
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iget-object v0, v0, Lcom/nokia/maps/i;->c:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;->setLongitude(D)V

    .line 1023
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->J:Lcom/nokia/maps/i;

    iget-object v0, v0, Lcom/nokia/maps/i;->c:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p3}, Lcom/nokia/maps/GeoCoordinateImpl;->c()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;->setAltitude(D)V

    goto :goto_0
.end method

.method onPrePresent()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 974
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->b()Lcom/nokia/maps/g;

    move-result-object v0

    .line 975
    if-eqz v0, :cond_0

    .line 976
    invoke-virtual {v0}, Lcom/nokia/maps/g;->c()V

    .line 978
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->u:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 979
    return-void
.end method

.method onRadarUpdate(Lcom/nokia/maps/ARRadar;)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    if-nez v0, :cond_0

    .line 1055
    invoke-static {p1}, Lcom/nokia/maps/ARRadar;->a(Lcom/nokia/maps/ARRadar;)Lcom/here/android/mpa/ar/ARRadarProperties;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    .line 1056
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    invoke-static {v0}, Lcom/nokia/maps/ARRadar;->a(Lcom/here/android/mpa/ar/ARRadarProperties;)Lcom/nokia/maps/ARRadar;

    move-result-object v0

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->X:Lcom/nokia/maps/d;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/ARRadar;->a(Lcom/nokia/maps/d;)V

    .line 1059
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->p:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->Y:Lcom/here/android/mpa/ar/ARRadarProperties;

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1060
    return-void
.end method

.method onRequestToCreateCamera()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 401
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-eqz v0, :cond_0

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    new-instance v0, Lcom/nokia/maps/a;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->W:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/nokia/maps/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    .line 406
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0}, Lcom/nokia/maps/a;->h()V

    goto :goto_0
.end method

.method onRequestToCreateSensors()V
    .locals 3
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 444
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-eqz v0, :cond_0

    .line 557
    :goto_0
    return-void

    .line 453
    :cond_0
    new-instance v0, Lcom/nokia/maps/ARSensors;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->W:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/nokia/maps/ARSensors;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    .line 454
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/ARLayoutControl;->setSensors(Lcom/nokia/maps/ARSensors;)V

    .line 456
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    iget-object v0, v0, Lcom/nokia/maps/ARSensors;->a:Lcom/nokia/maps/aw;

    new-instance v1, Lcom/nokia/maps/ARLayoutControl$19;

    invoke-direct {v1, p0}, Lcom/nokia/maps/ARLayoutControl$19;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 468
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    iget-object v0, v0, Lcom/nokia/maps/ARSensors;->g:Lcom/nokia/maps/aw;

    new-instance v1, Lcom/nokia/maps/ARLayoutControl$20;

    invoke-direct {v1, p0}, Lcom/nokia/maps/ARLayoutControl$20;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 485
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    iget-object v0, v0, Lcom/nokia/maps/ARSensors;->f:Lcom/nokia/maps/aw;

    new-instance v1, Lcom/nokia/maps/ARLayoutControl$21;

    invoke-direct {v1, p0}, Lcom/nokia/maps/ARLayoutControl$21;-><init>(Lcom/nokia/maps/ARLayoutControl;)V

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    goto :goto_0
.end method

.method onRequestToDestroyCamera()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-nez v0, :cond_0

    .line 437
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->a:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->an:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 420
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->b:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ao:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 421
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->d:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->aq:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 422
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0}, Lcom/nokia/maps/a;->i()V

    .line 434
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0}, Lcom/nokia/maps/a;->j()V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    goto :goto_0
.end method

.method onRequestToDestroySensors()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 574
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-nez v0, :cond_0

    .line 589
    :goto_0
    return-void

    .line 580
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/nokia/maps/ARLayoutControl;->onRequestToStopCamera(ZZ)V

    .line 582
    invoke-virtual {p0}, Lcom/nokia/maps/ARLayoutControl;->onRequestToStopSensors()V

    .line 584
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->B:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->b(Ljava/lang/Runnable;)V

    .line 586
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->j()V

    .line 588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    goto :goto_0
.end method

.method onRequestToPauseCamera(ZZ)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 727
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-nez v0, :cond_1

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 731
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ar:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 732
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->b:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ao:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 734
    if-nez p2, :cond_0

    .line 735
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/a;->d(Z)V

    goto :goto_0
.end method

.method onRequestToPauseSensors()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 757
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-nez v0, :cond_0

    .line 763
    :goto_0
    return-void

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->e()V

    goto :goto_0
.end method

.method onRequestToRender()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->f()V

    .line 393
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->O:Lcom/nokia/maps/y;

    invoke-virtual {v0}, Lcom/nokia/maps/y;->requestRender()V

    .line 394
    return-void
.end method

.method onRequestToResumeCamera(Z)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 711
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-nez v0, :cond_0

    .line 720
    :goto_0
    return-void

    .line 715
    :cond_0
    iget-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ad:Z

    if-eqz v0, :cond_1

    .line 716
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->c:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ar:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 718
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->b:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->ao:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 719
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/a;->c(Z)V

    goto :goto_0
.end method

.method onRequestToResumeSensors()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 744
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-nez v0, :cond_0

    .line 750
    :goto_0
    return-void

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->d()V

    goto :goto_0
.end method

.method onRequestToStartCamera(Z)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 596
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-nez v0, :cond_0

    .line 604
    :goto_0
    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->a:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->an:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 601
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->d:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->aq:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->a(Lcom/nokia/maps/aw$c;)V

    .line 603
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/a;->a(Z)V

    goto :goto_0
.end method

.method onRequestToStartSensors()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-nez v0, :cond_0

    .line 639
    :goto_0
    return-void

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->d()V

    .line 636
    invoke-direct {p0}, Lcom/nokia/maps/ARLayoutControl;->g()V

    .line 638
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->p()V

    goto :goto_0
.end method

.method onRequestToStopCamera(ZZ)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 611
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    .line 613
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-nez v0, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->a:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->an:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 618
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v0, v0, Lcom/nokia/maps/a;->d:Lcom/nokia/maps/aw;

    iget-object v1, p0, Lcom/nokia/maps/ARLayoutControl;->aq:Lcom/nokia/maps/aw$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/aw;->b(Lcom/nokia/maps/aw$c;)V

    .line 620
    if-nez p2, :cond_0

    .line 621
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/a;->b(Z)V

    goto :goto_0
.end method

.method onRequestToStopSensors()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 688
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nokia/maps/ARLayoutControl;->ac:Z

    .line 690
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    if-nez v0, :cond_0

    .line 704
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->e()V

    .line 697
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    if-eqz v0, :cond_1

    .line 698
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->T:Lcom/nokia/maps/a;

    iget-object v1, v0, Lcom/nokia/maps/a;->c:Lcom/nokia/maps/aw;

    monitor-enter v1

    .line 699
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->q()V

    .line 700
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 702
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->U:Lcom/nokia/maps/ARSensors;

    invoke-virtual {v0}, Lcom/nokia/maps/ARSensors;->q()V

    goto :goto_0
.end method

.method onSliSceneStart()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 899
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->f:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 906
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->am:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 907
    return-void
.end method

.method onSliSceneStop()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 924
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->g:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 931
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->C:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    .line 932
    return-void
.end method

.method onTerminated()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/nokia/maps/ARLayoutControl;->y:Lcom/nokia/maps/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/nokia/maps/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1069
    return-void
.end method

.method native pan(Landroid/graphics/Point;Landroid/graphics/Point;)V
.end method

.method native panTo(Lcom/nokia/maps/GeoCoordinateImpl;)V
.end method

.method native pause()V
.end method

.method native pixelTo3dPosition(FLandroid/graphics/PointF;Lcom/here/android/mpa/common/Vector3f;)Z
.end method

.method native press(J)V
.end method

.method native removeARObject(Lcom/nokia/maps/ARObjectImpl;)V
.end method

.method native removeARObject(Lcom/nokia/maps/ARPolylineObjectImpl;)V
.end method

.method native removeARViewObject(Lcom/nokia/maps/ARModelObjectImpl;)V
.end method

.method native resume()V
.end method

.method native select(J)V
.end method

.method native selectWithScale(JZF)V
.end method

.method native sensorsAreReady()V
.end method

.method native setAnimationDelay(IJ)V
.end method

.method native setAnimationDuration(IJ)V
.end method

.method native setAnimationInterpolator(II)V
.end method

.method native setBack2FrontIconSizeRatio(F)V
.end method

.method native setBackIconSize(Lcom/here/android/mpa/common/Size;)V
.end method

.method native setCameraAndLayout(IIFFIIFF)V
.end method

.method native setCameraFov(FF)V
.end method

.method native setCameraFrameSize(II)V
.end method

.method native setCameraMaxZoomScaleUpView(F)V
.end method

.method native setCameraZoomEnabledUpView(Z)V
.end method

.method native setDownIconOpacity(F)V
.end method

.method native setDownViewMaxOpacity(F)V
.end method

.method native setDownViewMinOpacity(F)V
.end method

.method native setDownViewPitchThreshold(F)V
.end method

.method native setEdgeDetectionEnabled(Z)V
.end method

.method native setFilterCoeff(IF)V
.end method

.method native setFilterSize(II)V
.end method

.method native setFixedAltitude(FZ)V
.end method

.method native setFlyRotateDeg(IF)V
.end method

.method native setFrontIconSize(Lcom/here/android/mpa/common/Size;)V
.end method

.method native setInfoAnimationInUpViewOnly(Z)V
.end method

.method native setInfoAnimationMinWidthFactor(F)V
.end method

.method native setIntroAnimationMode(I)V
.end method

.method native setIntroAnimationTime(J)V
.end method

.method native setLayoutFov(FF)V
.end method

.method native setLayoutSize(II)V
.end method

.method native setMapAsPoseReadingSource(Z)V
.end method

.method native setMapAutoControlOnEntryExit(Z)V
.end method

.method native setMapAutoGeoPosition(ZZ)V
.end method

.method native setMapAutoHeading(ZZ)V
.end method

.method native setMapAutoPitchNative(ZZ)V
.end method

.method native setMapAutoTfc(ZZ)V
.end method

.method native setMapAutoZoom(ZZ)V
.end method

.method native setMapNative(Lcom/nokia/maps/MapImpl;)V
.end method

.method native setMaxZoomScale(FZZ)V
.end method

.method native setMinPitchDownView(F)V
.end method

.method native setNonSelectedItemsOpacity(F)V
.end method

.method native setOcclusionEnabled(Z)V
.end method

.method native setOcclusionOpacity(F)V
.end method

.method native setOrientationAngle(F)V
.end method

.method native setOrientationAnimation(Z)V
.end method

.method native setPitchLockedUpView(Z)V
.end method

.method native setPitchThresholdForPoseEngineHeading(F)V
.end method

.method native setPlanesParam(FFFF)V
.end method

.method native setProjectionType(JI)V
.end method

.method native setScreenViewPoint(Landroid/graphics/PointF;Z)V
.end method

.method native setSelectedBoundingBox(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
.end method

.method native setSelectedIconSize(Lcom/here/android/mpa/common/Size;)V
.end method

.method native setSelectedItemMaxViewAngle(F)V
.end method

.method native setSelectedItemOpacity(F)V
.end method

.method native setSensors(Lcom/nokia/maps/ARSensors;)V
.end method

.method native setSensorsWaitTimeout(J)V
.end method

.method native setShowGridEnabled(Z)V
.end method

.method native setTiltUpMaxTime(J)V
.end method

.method native setTiltUpMinTime(J)V
.end method

.method native setUpViewPitchThreshold(F)V
.end method

.method native setUpdateDistanceThreshold(F)V
.end method

.method native setUseDownIconOpacity(Z)V
.end method

.method native showFrontItemsOnly(Z)V
.end method

.method native showScene(I)V
.end method

.method native showUpScene(I)V
.end method

.method native startLivesight()Z
.end method

.method native startOrientationAnimation(Z)V
.end method

.method native stopLivesight(Z)V
.end method

.method native touchDown()V
.end method

.method native touchUp()V
.end method

.method native unselect()V
.end method

.method native usePoseEngineHeading(Z)V
.end method
