.class abstract Lcom/nokia/maps/bw$g;
.super Ljava/lang/Object;
.source "MapLoaderImpl.java"

# interfaces
.implements Lcom/nokia/maps/MapsEngine$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nokia/maps/bw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "g"
.end annotation


# instance fields
.field private a:I

.field final synthetic f:Lcom/nokia/maps/bw;


# direct methods
.method private constructor <init>(Lcom/nokia/maps/bw;)V
    .locals 1

    .prologue
    .line 719
    iput-object p1, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 722
    const/4 v0, 0x0

    iput v0, p0, Lcom/nokia/maps/bw$g;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/nokia/maps/bw;Lcom/nokia/maps/bw$1;)V
    .locals 0

    .prologue
    .line 719
    invoke-direct {p0, p1}, Lcom/nokia/maps/bw$g;-><init>(Lcom/nokia/maps/bw;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 809
    iput v1, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 810
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 811
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->k(Lcom/nokia/maps/bw;)V

    .line 812
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->c(Lcom/nokia/maps/bw;Z)V

    .line 813
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 743
    return-void
.end method

.method public a(JJ)V
    .locals 0

    .prologue
    .line 785
    return-void
.end method

.method protected a(Lcom/here/android/mpa/odml/MapPackage;Lcom/here/android/mpa/odml/MapPackage$InstallationState;)V
    .locals 2

    .prologue
    .line 845
    if-nez p1, :cond_1

    .line 855
    :cond_0
    return-void

    .line 848
    :cond_1
    invoke-static {p1}, Lcom/nokia/maps/ca;->a(Lcom/here/android/mpa/odml/MapPackage;)Lcom/nokia/maps/ca;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/nokia/maps/ca;->a(Lcom/here/android/mpa/odml/MapPackage$InstallationState;)V

    .line 849
    invoke-virtual {p1}, Lcom/here/android/mpa/odml/MapPackage;->getChildren()Ljava/util/List;

    move-result-object v0

    .line 850
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 851
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/odml/MapPackage;

    .line 852
    invoke-virtual {p0, v0, p2}, Lcom/nokia/maps/bw$g;->a(Lcom/here/android/mpa/odml/MapPackage;Lcom/here/android/mpa/odml/MapPackage$InstallationState;)V

    goto :goto_0
.end method

.method public a(Lcom/nokia/maps/MapPackageSelection;)V
    .locals 0

    .prologue
    .line 781
    return-void
.end method

.method public a(Lcom/nokia/maps/MapPackageSelection;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 729
    return-void
.end method

.method protected a(Ljava/lang/Runnable;Ljava/lang/Runnable;Z)V
    .locals 4

    .prologue
    .line 789
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->i(Lcom/nokia/maps/bw;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 790
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->j(Lcom/nokia/maps/bw;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 791
    if-eqz p3, :cond_0

    .line 792
    const/4 v0, 0x0

    iput v0, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 793
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 805
    :goto_0
    monitor-exit v1

    .line 806
    return-void

    .line 795
    :cond_0
    iget v0, p0, Lcom/nokia/maps/bw$g;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/nokia/maps/bw$g;->a:I

    const/4 v2, 0x7

    if-gt v0, v2, :cond_1

    .line 796
    const-wide/16 v2, 0x3e8

    invoke-static {p2, v2, v3}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 805
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 798
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/nokia/maps/bw;->f()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Map Loader operation timed out."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 799
    invoke-virtual {p0}, Lcom/nokia/maps/bw$g;->b()V

    goto :goto_0

    .line 803
    :cond_2
    invoke-virtual {p0}, Lcom/nokia/maps/bw$g;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 747
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 735
    return-void
.end method

.method public a([Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 739
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 816
    iput v1, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 817
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 818
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->k(Lcom/nokia/maps/bw;)V

    .line 819
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->c(Lcom/nokia/maps/bw;Z)V

    .line 820
    return-void
.end method

.method public final b(Lcom/nokia/maps/MapPackageSelection;)V
    .locals 2

    .prologue
    .line 767
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->g(Lcom/nokia/maps/bw;)Lcom/nokia/maps/bw$k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/bw$k;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 768
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/nokia/maps/MapPackageSelection;->c(I)Z

    goto :goto_0

    .line 771
    :cond_0
    invoke-virtual {p0, p1}, Lcom/nokia/maps/bw$g;->a(Lcom/nokia/maps/MapPackageSelection;)V

    .line 772
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 823
    iput v1, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 824
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->d(Lcom/nokia/maps/bw;Z)Z

    .line 825
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 826
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->c(Lcom/nokia/maps/bw;Z)V

    .line 827
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 837
    iput v1, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 838
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 839
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->k(Lcom/nokia/maps/bw;)V

    .line 840
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->c(Lcom/nokia/maps/bw;Z)V

    .line 841
    return-void
.end method

.method public abstract e()V
.end method

.method protected f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 830
    iput v1, p0, Lcom/nokia/maps/bw$g;->a:I

    .line 831
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->a(Lcom/nokia/maps/bw;)Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/MapsEngine;->b(Lcom/nokia/maps/MapsEngine$k;)V

    .line 832
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0}, Lcom/nokia/maps/bw;->k(Lcom/nokia/maps/bw;)V

    .line 833
    iget-object v0, p0, Lcom/nokia/maps/bw$g;->f:Lcom/nokia/maps/bw;

    invoke-static {v0, v1}, Lcom/nokia/maps/bw;->c(Lcom/nokia/maps/bw;Z)V

    .line 834
    return-void
.end method
