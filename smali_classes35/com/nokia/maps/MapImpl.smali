.class public final Lcom/nokia/maps/MapImpl;
.super Lcom/nokia/maps/BaseNativeObject;
.source "MapImpl.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nokia/maps/MapImpl$c;,
        Lcom/nokia/maps/MapImpl$d;,
        Lcom/nokia/maps/MapImpl$MapEventDispatcher;,
        Lcom/nokia/maps/MapImpl$a;,
        Lcom/nokia/maps/MapImpl$e;,
        Lcom/nokia/maps/MapImpl$b;,
        Lcom/nokia/maps/MapImpl$g;,
        Lcom/nokia/maps/MapImpl$f;,
        Lcom/nokia/maps/MapImpl$h;
    }
.end annotation


# static fields
.field private static ah:Ljava/lang/Object;

.field private static final e:Ljava/lang/String;

.field private static s:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static u:Lcom/nokia/maps/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/mapping/Map;",
            "Lcom/nokia/maps/MapImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static v:Lcom/nokia/maps/MapImpl$f;

.field private static w:Ljava/lang/String;

.field private static x:Ljava/lang/String;

.field private static y:I

.field private static z:I


# instance fields
.field private A:Lcom/nokia/maps/MapImpl$g;

.field private final B:Lcom/nokia/maps/r;

.field private final C:Lcom/nokia/maps/q;

.field private final D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/here/android/mpa/cluster/ClusterLayer;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/here/android/mpa/mapping/MapOverlay;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Ljava/lang/Runnable;

.field private G:Ljava/lang/Runnable;

.field private final J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/nokia/maps/ev;",
            "Lcom/nokia/maps/ev;",
            ">;>;"
        }
    .end annotation
.end field

.field private K:Ljava/lang/Runnable;

.field private final L:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final M:Ljava/lang/Runnable;

.field private final N:Ljava/lang/Runnable;

.field private final O:Lcom/nokia/maps/ApplicationContext$c;

.field private P:Ljava/lang/Boolean;

.field private final Q:Lcom/nokia/maps/ApplicationContext$c;

.field private final R:Lcom/nokia/maps/ApplicationContext$c;

.field private S:Ljava/lang/String;

.field private final T:Ljava/lang/Runnable;

.field private U:Z

.field private final V:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapImpl$e;",
            ">;"
        }
    .end annotation
.end field

.field private final W:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapImpl$a;",
            ">;"
        }
    .end annotation
.end field

.field private final X:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/mapping/Map$OnTransformListener;",
            ">;"
        }
    .end annotation
.end field

.field private final Y:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final Z:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapImpl$h;",
            ">;"
        }
    .end annotation
.end field

.field final a:Lcom/nokia/maps/MapGestureHandler$MapUserInteractionListener;

.field private final aa:Ljava/lang/Runnable;

.field private ab:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/nokia/maps/MapRouteImpl;",
            ">;"
        }
    .end annotation
.end field

.field private ac:D

.field private ad:Z

.field private ae:I

.field private af:I

.field private final ag:Lcom/nokia/maps/ApplicationContext$c;

.field private final ai:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapRasterTileSource;",
            ">;"
        }
    .end annotation
.end field

.field private final aj:Ljava/lang/Runnable;

.field private volatile ak:Lcom/here/android/mpa/mapping/MapTrafficLayer;

.field private final al:Lcom/nokia/maps/ApplicationContext$c;

.field private am:Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;

.field private final an:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final ao:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final ap:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Lcom/nokia/maps/fm;

.field private final ar:Lcom/nokia/maps/MapImpl$MapEventDispatcher;

.field private final as:Z

.field private b:I

.field private c:I

.field private d:Landroid/graphics/PointF;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapRasterTileSource;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

.field private i:Lcom/here/android/mpa/mapping/PositionIndicator;

.field private final j:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapContainer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:I

.field private o:I

.field private p:Ljava/util/concurrent/atomic/AtomicInteger;

.field private q:Z

.field private final r:Lcom/nokia/maps/cy;

.field private t:Lcom/nokia/maps/MapImpl$c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    const-class v0, Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nokia/maps/MapImpl;->e:Ljava/lang/String;

    .line 104
    sput-object v1, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    .line 111
    sput-object v1, Lcom/nokia/maps/MapImpl;->u:Lcom/nokia/maps/m;

    .line 222
    sget-object v0, Lcom/nokia/maps/MapImpl$f;->d:Lcom/nokia/maps/MapImpl$f;

    sput-object v0, Lcom/nokia/maps/MapImpl;->v:Lcom/nokia/maps/MapImpl$f;

    .line 223
    sput-object v1, Lcom/nokia/maps/MapImpl;->w:Ljava/lang/String;

    .line 224
    sput-object v1, Lcom/nokia/maps/MapImpl;->x:Ljava/lang/String;

    .line 225
    sput v2, Lcom/nokia/maps/MapImpl;->y:I

    .line 226
    sput v2, Lcom/nokia/maps/MapImpl;->z:I

    .line 3026
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapImpl;->ah:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v2, 0x33

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 427
    invoke-direct {p0, v6}, Lcom/nokia/maps/BaseNativeObject;-><init>(Z)V

    .line 82
    iput v5, p0, Lcom/nokia/maps/MapImpl;->b:I

    .line 83
    iput v5, p0, Lcom/nokia/maps/MapImpl;->c:I

    .line 92
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    .line 93
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    .line 95
    iput-boolean v5, p0, Lcom/nokia/maps/MapImpl;->l:Z

    .line 96
    iput-boolean v5, p0, Lcom/nokia/maps/MapImpl;->m:Z

    .line 99
    iput v2, p0, Lcom/nokia/maps/MapImpl;->n:I

    .line 100
    iput v2, p0, Lcom/nokia/maps/MapImpl;->o:I

    .line 101
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 103
    new-instance v0, Lcom/nokia/maps/cy;

    const-class v2, Lcom/nokia/maps/MapImpl;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/nokia/maps/cy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->r:Lcom/nokia/maps/cy;

    .line 229
    invoke-static {}, Lcom/nokia/maps/n;->a()Lcom/nokia/maps/r;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    .line 230
    new-instance v0, Lcom/nokia/maps/q;

    invoke-direct {v0}, Lcom/nokia/maps/q;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->C:Lcom/nokia/maps/q;

    .line 232
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    .line 233
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->E:Ljava/util/Set;

    .line 504
    new-instance v0, Lcom/nokia/maps/MapImpl$1;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$1;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->F:Ljava/lang/Runnable;

    .line 573
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->G:Ljava/lang/Runnable;

    .line 778
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    .line 998
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->K:Ljava/lang/Runnable;

    .line 1137
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1139
    new-instance v0, Lcom/nokia/maps/MapImpl$23;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$23;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->M:Ljava/lang/Runnable;

    .line 1184
    new-instance v0, Lcom/nokia/maps/MapImpl$b;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$b;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->N:Ljava/lang/Runnable;

    .line 1270
    new-instance v0, Lcom/nokia/maps/MapImpl$3;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$3;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->O:Lcom/nokia/maps/ApplicationContext$c;

    .line 1282
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    .line 1336
    new-instance v0, Lcom/nokia/maps/MapImpl$4;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$4;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->Q:Lcom/nokia/maps/ApplicationContext$c;

    .line 1347
    new-instance v0, Lcom/nokia/maps/MapImpl$5;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$5;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->R:Lcom/nokia/maps/ApplicationContext$c;

    .line 1358
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    .line 1359
    new-instance v0, Lcom/nokia/maps/MapImpl$6;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$6;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->T:Ljava/lang/Runnable;

    .line 1711
    iput-boolean v5, p0, Lcom/nokia/maps/MapImpl;->U:Z

    .line 1849
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->V:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1850
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->W:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1852
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->X:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1853
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->Y:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1854
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->Z:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1924
    new-instance v0, Lcom/nokia/maps/MapImpl$7;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$7;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->aa:Ljava/lang/Runnable;

    .line 2370
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2371
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/nokia/maps/MapImpl;->ac:D

    .line 2752
    new-instance v0, Lcom/nokia/maps/MapImpl$13;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$13;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ag:Lcom/nokia/maps/ApplicationContext$c;

    .line 3131
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    .line 3140
    new-instance v0, Lcom/nokia/maps/MapImpl$14;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$14;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->aj:Ljava/lang/Runnable;

    .line 3258
    new-instance v0, Lcom/nokia/maps/MapImpl$15;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$15;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->al:Lcom/nokia/maps/ApplicationContext$c;

    .line 3371
    new-instance v0, Lcom/nokia/maps/MapImpl$16;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$16;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->a:Lcom/nokia/maps/MapGestureHandler$MapUserInteractionListener;

    .line 3382
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->an:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 3423
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 3424
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 3464
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->aq:Lcom/nokia/maps/fm;

    .line 3468
    iput-boolean v6, p0, Lcom/nokia/maps/MapImpl;->as:Z

    .line 430
    invoke-static {p1}, Lcom/nokia/maps/MapsEngine;->b(Landroid/content/Context;)Lcom/nokia/maps/MapsEngine;

    .line 431
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->c()Lcom/nokia/maps/MapsEngine$e;

    move-result-object v0

    sget-object v2, Lcom/nokia/maps/MapsEngine$e;->c:Lcom/nokia/maps/MapsEngine$e;

    if-eq v0, v2, :cond_0

    .line 432
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MapsEngine is not ready. Map Download maybe happening."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->f:Landroid/content/Context;

    .line 439
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->a(Landroid/content/Context;)V

    .line 440
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->b(Landroid/content/Context;)V

    .line 443
    sget-object v0, Lcom/nokia/maps/MapImpl;->w:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 444
    sget-object v2, Lcom/nokia/maps/MapImpl;->w:Ljava/lang/String;

    .line 446
    sget-object v0, Lcom/nokia/maps/MapImpl;->x:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 447
    sget-object v0, Lcom/nokia/maps/MapImpl;->x:Ljava/lang/String;

    .line 451
    :goto_0
    iget-object v3, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v3, v3, Lcom/nokia/maps/MapImpl$g;->c:I

    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v4, v4, Lcom/nokia/maps/MapImpl$g;->d:I

    invoke-direct {p0, v3, v4, v2, v0}, Lcom/nokia/maps/MapImpl;->createMapNative(IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 453
    if-nez v0, :cond_1

    .line 454
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid configuration file. Check MWConfig!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->j()Z

    .line 461
    :try_start_0
    new-instance v0, Lcom/nokia/maps/MapImpl$c;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$c;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->t:Lcom/nokia/maps/MapImpl$c;

    .line 462
    invoke-static {}, Lcom/nokia/maps/MapsEngine;->d()Lcom/nokia/maps/MapsEngine;

    move-result-object v0

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->t:Lcom/nokia/maps/MapImpl$c;

    invoke-virtual {v0, v2}, Lcom/nokia/maps/MapsEngine;->a(Lcom/nokia/maps/MapsEngine$h;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    :goto_1
    sget-object v0, Lcom/nokia/maps/bb;->a:Lcom/nokia/maps/bb;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/bb;)V

    .line 470
    iput-object v1, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;

    .line 471
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    .line 472
    new-instance v0, Lcom/nokia/maps/fm;

    invoke-direct {v0}, Lcom/nokia/maps/fm;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->aq:Lcom/nokia/maps/fm;

    .line 473
    new-instance v0, Lcom/nokia/maps/MapImpl$MapEventDispatcher;

    invoke-direct {v0, p0}, Lcom/nokia/maps/MapImpl$MapEventDispatcher;-><init>(Lcom/nokia/maps/MapImpl;)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ar:Lcom/nokia/maps/MapImpl$MapEventDispatcher;

    .line 474
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ar:Lcom/nokia/maps/MapImpl$MapEventDispatcher;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl$MapEventDispatcher;->start()V

    .line 475
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 478
    invoke-virtual {p0, v6}, Lcom/nokia/maps/MapImpl;->setRetainedLabelsEnabled(Z)V

    .line 479
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 464
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method

.method static synthetic G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/nokia/maps/MapImpl;->e:Ljava/lang/String;

    return-object v0
.end method

.method private H()V
    .locals 6

    .prologue
    .line 792
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    monitor-enter v1

    .line 794
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/nokia/maps/ev;

    iget v2, v0, Lcom/nokia/maps/ev;->x:F

    .line 796
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/nokia/maps/ev;

    iget v3, v0, Lcom/nokia/maps/ev;->y:F

    .line 798
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 800
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/nokia/maps/ev;

    iget v5, v0, Lcom/nokia/maps/ev;->x:F

    .line 801
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/nokia/maps/ev;

    iget v0, v0, Lcom/nokia/maps/ev;->y:F

    .line 803
    invoke-direct {p0, v2, v3, v5, v0}, Lcom/nokia/maps/MapImpl;->a(FFFF)V

    .line 805
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 807
    :cond_0
    monitor-exit v1

    .line 808
    return-void

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private I()V
    .locals 3

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->M:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 1153
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->N()V

    .line 1154
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->J()V

    .line 1156
    :cond_0
    return-void
.end method

.method private J()V
    .locals 1

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->N:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 1193
    :cond_0
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 1215
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->W:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapImpl$a;

    .line 1216
    invoke-interface {v0}, Lcom/nokia/maps/MapImpl$a;->a()V

    goto :goto_0

    .line 1218
    :cond_0
    return-void
.end method

.method private declared-synchronized L()V
    .locals 2

    .prologue
    .line 1287
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1288
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->isLandmarksVisibleNative()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1289
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setLandmarksVisibleNative(Z)Z

    .line 1290
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1291
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0}, Lcom/nokia/maps/r;->h()V

    .line 1294
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1296
    :cond_1
    monitor-exit p0

    return-void

    .line 1287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized M()V
    .locals 2

    .prologue
    .line 1367
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1368
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v0

    .line 1369
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1370
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->setMapSchemeNative(Ljava/lang/String;)Z

    .line 1372
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1373
    :cond_0
    const-string v0, "changescheme"

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->e(Ljava/lang/String;)V

    .line 1378
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1380
    :cond_2
    monitor-exit p0

    return-void

    .line 1375
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private N()V
    .locals 2

    .prologue
    .line 1934
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->setSubPixelLabelPositioningEnabled(Z)V

    .line 1936
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v0

    sget-object v1, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v0, v1, :cond_0

    .line 1937
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->aa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 1941
    :goto_0
    return-void

    .line 1939
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->aa:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private O()V
    .locals 2

    .prologue
    .line 2006
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->V:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapImpl$e;

    .line 2008
    :try_start_0
    invoke-interface {v0}, Lcom/nokia/maps/MapImpl$e;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2009
    :catch_0
    move-exception v0

    goto :goto_0

    .line 2013
    :cond_0
    return-void
.end method

.method private P()V
    .locals 2

    .prologue
    .line 2040
    iget-boolean v0, p0, Lcom/nokia/maps/MapImpl;->l:Z

    if-eqz v0, :cond_0

    .line 2041
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->invalidate()V

    .line 2045
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->V:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapImpl$e;

    .line 2046
    invoke-interface {v0}, Lcom/nokia/maps/MapImpl$e;->a()V

    goto :goto_0

    .line 2049
    :cond_1
    return-void
.end method

.method private Q()V
    .locals 8

    .prologue
    .line 2388
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2433
    :cond_0
    return-void

    .line 2392
    :cond_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getZoomLevel()D

    move-result-wide v0

    .line 2393
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getPanoramaCoverageEnabled()Z

    move-result v4

    .line 2394
    iget-wide v2, p0, Lcom/nokia/maps/MapImpl;->ac:D

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v6, 0x3fb999999999999aL    # 0.1

    cmpl-double v2, v2, v6

    if-gtz v2, :cond_2

    iget-boolean v2, p0, Lcom/nokia/maps/MapImpl;->ad:Z

    if-eq v2, v4, :cond_7

    .line 2396
    :cond_2
    iput-wide v0, p0, Lcom/nokia/maps/MapImpl;->ac:D

    .line 2397
    iput-boolean v4, p0, Lcom/nokia/maps/MapImpl;->ad:Z

    .line 2398
    invoke-virtual {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->b(D)D

    move-result-wide v2

    .line 2399
    if-eqz v4, :cond_4

    .line 2400
    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v4, v4, Lcom/nokia/maps/MapImpl$g;->c:I

    iget-object v5, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v5, v5, Lcom/nokia/maps/MapImpl$g;->d:I

    invoke-static/range {v0 .. v5}, Lcom/nokia/maps/el;->a(DDII)I

    move-result v0

    iput v0, p0, Lcom/nokia/maps/MapImpl;->ae:I

    .line 2402
    iget v0, p0, Lcom/nokia/maps/MapImpl;->ae:I

    iput v0, p0, Lcom/nokia/maps/MapImpl;->af:I

    .line 2403
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapRouteImpl;

    .line 2404
    instance-of v2, v0, Lcom/nokia/maps/cg;

    if-eqz v2, :cond_3

    .line 2405
    check-cast v0, Lcom/nokia/maps/cg;

    iget v2, p0, Lcom/nokia/maps/MapImpl;->ae:I

    invoke-virtual {v0, v2}, Lcom/nokia/maps/cg;->b(I)V

    goto :goto_0

    .line 2409
    :cond_4
    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v4, v4, Lcom/nokia/maps/MapImpl$g;->c:I

    iget-object v5, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v5, v5, Lcom/nokia/maps/MapImpl$g;->d:I

    invoke-static/range {v0 .. v5}, Lcom/nokia/maps/el;->b(DDII)[I

    move-result-object v0

    .line 2411
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/nokia/maps/MapImpl;->ae:I

    .line 2412
    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/nokia/maps/MapImpl;->af:I

    .line 2414
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapRouteImpl;

    .line 2415
    instance-of v1, v0, Lcom/nokia/maps/cg;

    if-eqz v1, :cond_5

    move-object v1, v0

    .line 2416
    check-cast v1, Lcom/nokia/maps/cg;

    .line 2417
    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->b()Lcom/here/android/mpa/mapping/MapRoute$RenderType;

    move-result-object v0

    sget-object v3, Lcom/here/android/mpa/mapping/MapRoute$RenderType;->SECONDARY:Lcom/here/android/mpa/mapping/MapRoute$RenderType;

    if-ne v0, v3, :cond_6

    iget v0, p0, Lcom/nokia/maps/MapImpl;->af:I

    :goto_2
    invoke-virtual {v1, v0}, Lcom/nokia/maps/cg;->b(I)V

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/nokia/maps/MapImpl;->ae:I

    goto :goto_2

    .line 2423
    :cond_7
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapRouteImpl;

    .line 2424
    instance-of v1, v0, Lcom/nokia/maps/cg;

    if-eqz v1, :cond_8

    move-object v1, v0

    .line 2425
    check-cast v1, Lcom/nokia/maps/cg;

    invoke-virtual {v1}, Lcom/nokia/maps/cg;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    move-object v1, v0

    .line 2426
    check-cast v1, Lcom/nokia/maps/cg;

    .line 2427
    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->b()Lcom/here/android/mpa/mapping/MapRoute$RenderType;

    move-result-object v0

    sget-object v3, Lcom/here/android/mpa/mapping/MapRoute$RenderType;->SECONDARY:Lcom/here/android/mpa/mapping/MapRoute$RenderType;

    if-ne v0, v3, :cond_9

    iget v0, p0, Lcom/nokia/maps/MapImpl;->af:I

    :goto_4
    invoke-virtual {v1, v0}, Lcom/nokia/maps/cg;->b(I)V

    goto :goto_3

    :cond_9
    iget v0, p0, Lcom/nokia/maps/MapImpl;->ae:I

    goto :goto_4
.end method

.method static synthetic a(Lcom/here/android/mpa/mapping/Map$Animation;)I
    .locals 1

    .prologue
    .line 80
    invoke-static {p0}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v0

    return v0
.end method

.method private a(Lcom/here/android/mpa/common/ViewObject;)Lcom/nokia/maps/Cluster;
    .locals 4

    .prologue
    .line 2478
    instance-of v0, p1, Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_2

    .line 2479
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/cluster/ClusterLayer;

    .line 2480
    invoke-static {v0}, Lcom/nokia/maps/ag;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)Lcom/nokia/maps/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/ag;->c()Ljava/util/Collection;

    move-result-object v0

    .line 2481
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/Cluster;

    .line 2482
    invoke-static {p1}, Lcom/nokia/maps/ViewObjectImpl;->a(Lcom/here/android/mpa/common/ViewObject;)Lcom/nokia/maps/ViewObjectImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/nokia/maps/ViewObjectImpl;->hashCode()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/nokia/maps/Cluster;->representedBy(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2489
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/nokia/maps/MapImpl;->K:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a([Lcom/here/android/mpa/common/ViewObject;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/here/android/mpa/common/ViewObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2511
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2513
    array-length v3, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, p1, v1

    .line 2515
    sget-object v4, Lcom/nokia/maps/MapImpl$17;->b:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/common/ViewObject;->getBaseType()Lcom/here/android/mpa/common/ViewObject$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/common/ViewObject$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2513
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2517
    :pswitch_0
    check-cast v0, Lcom/here/android/mpa/mapping/MapObject;

    invoke-direct {p0, v2, v0}, Lcom/nokia/maps/MapImpl;->a(Ljava/util/List;Lcom/here/android/mpa/mapping/MapObject;)V

    goto :goto_1

    .line 2520
    :pswitch_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2528
    :cond_0
    return-object v2

    .line 2515
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized a(FFFF)V
    .locals 1

    .prologue
    .line 811
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 812
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->panNative(FFFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 813
    monitor-exit p0

    return-void

    .line 811
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(IIIIFF)V
    .locals 1

    .prologue
    .line 1017
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 1018
    invoke-direct/range {p0 .. p6}, Lcom/nokia/maps/MapImpl;->moveToNative(IIIIFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1019
    monitor-exit p0

    return-void

    .line 1017
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0x1b8

    const/16 v5, 0x190

    const/16 v4, 0xfa

    const/16 v3, 0x258

    .line 319
    new-instance v0, Lcom/nokia/maps/MapImpl$g;

    invoke-direct {v0}, Lcom/nokia/maps/MapImpl$g;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    .line 321
    sget-object v0, Lcom/nokia/maps/MapImpl$17;->a:[I

    sget-object v1, Lcom/nokia/maps/MapImpl;->v:Lcom/nokia/maps/MapImpl$f;

    invoke-virtual {v1}, Lcom/nokia/maps/MapImpl$f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 365
    :goto_0
    return-void

    .line 323
    :pswitch_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v4, v0, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 324
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v4, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 327
    :pswitch_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    const/16 v1, 0x140

    iput v1, v0, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 328
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v5, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 331
    :pswitch_2
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v6, v0, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 332
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v3, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 335
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 336
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xa0

    if-ge v1, v2, :cond_0

    .line 338
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 339
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v4, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 340
    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x14a

    if-ge v1, v2, :cond_1

    .line 342
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 343
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v5, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 344
    :cond_1
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ge v1, v6, :cond_2

    .line 346
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 347
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v3, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 348
    :cond_2
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ge v1, v3, :cond_3

    .line 350
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 351
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v3, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 354
    :cond_3
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v3, v0, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 355
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    iput v3, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 359
    :pswitch_4
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    sget v1, Lcom/nokia/maps/MapImpl;->y:I

    iput v1, v0, Lcom/nokia/maps/MapImpl$g;->c:I

    .line 360
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->A:Lcom/nokia/maps/MapImpl$g;

    sget v1, Lcom/nokia/maps/MapImpl;->z:I

    iput v1, v0, Lcom/nokia/maps/MapImpl$g;->d:I

    goto :goto_0

    .line 321
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lcom/here/android/mpa/mapping/MapOverlay;Z)V
    .locals 3

    .prologue
    .line 1982
    new-instance v0, Lcom/nokia/maps/MapImpl$10;

    invoke-direct {v0, p0, p2, p1}, Lcom/nokia/maps/MapImpl$10;-><init>(Lcom/nokia/maps/MapImpl;ZLcom/here/android/mpa/mapping/MapOverlay;)V

    .line 1998
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v1

    sget-object v2, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v1, v2, :cond_0

    .line 1999
    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 2003
    :goto_0
    return-void

    .line 2001
    :cond_0
    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a(Lcom/here/android/mpa/mapping/MapState;)V
    .locals 3

    .prologue
    .line 1944
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->setSubPixelLabelPositioningEnabled(Z)V

    .line 1946
    new-instance v0, Lcom/nokia/maps/MapImpl$8;

    invoke-direct {v0, p0, p1}, Lcom/nokia/maps/MapImpl$8;-><init>(Lcom/nokia/maps/MapImpl;Lcom/here/android/mpa/mapping/MapState;)V

    .line 1954
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v1

    sget-object v2, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v1, v2, :cond_0

    .line 1955
    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 1959
    :goto_0
    return-void

    .line 1957
    :cond_0
    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;DDIDFF)V
    .locals 1

    .prologue
    .line 80
    invoke-direct/range {p0 .. p9}, Lcom/nokia/maps/MapImpl;->setCenterNative(DDIDFF)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;IIIIFF)V
    .locals 0

    .prologue
    .line 80
    invoke-direct/range {p0 .. p6}, Lcom/nokia/maps/MapImpl;->a(IIIIFF)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/here/android/mpa/mapping/MapState;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapState;)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;IF)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/nokia/maps/MapImpl;->zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IF)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;IFF)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IFF)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;IIIF)V
    .locals 0

    .prologue
    .line 80
    invoke-direct/range {p0 .. p5}, Lcom/nokia/maps/MapImpl;->zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IIIF)V

    return-void
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;IIIIIF)V
    .locals 0

    .prologue
    .line 80
    invoke-direct/range {p0 .. p7}, Lcom/nokia/maps/MapImpl;->zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IIIIIF)V

    return-void
.end method

.method private a(Lcom/nokia/maps/MapMarkerImpl;)V
    .locals 3

    .prologue
    .line 2308
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/cluster/ClusterLayer;

    .line 2314
    invoke-static {v0}, Lcom/nokia/maps/ag;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)Lcom/nokia/maps/ag;

    move-result-object v0

    invoke-static {p1}, Lcom/nokia/maps/MapMarkerImpl;->a(Lcom/nokia/maps/MapMarkerImpl;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/nokia/maps/ag;->c(Lcom/here/android/mpa/mapping/MapMarker;)V

    goto :goto_0

    .line 2317
    :cond_0
    return-void
.end method

.method private a(Lcom/nokia/maps/MapRouteImpl;)V
    .locals 1

    .prologue
    .line 2377
    instance-of v0, p1, Lcom/nokia/maps/cg;

    if-nez v0, :cond_0

    .line 2385
    :goto_0
    return-void

    .line 2381
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_1

    .line 2382
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2384
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Lcom/nokia/maps/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nokia/maps/m",
            "<",
            "Lcom/here/android/mpa/mapping/Map;",
            "Lcom/nokia/maps/MapImpl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    sput-object p0, Lcom/nokia/maps/MapImpl;->u:Lcom/nokia/maps/m;

    .line 124
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 292
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    if-eqz p0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 296
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    if-eqz p1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 298
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 300
    :cond_2
    sput-object p0, Lcom/nokia/maps/MapImpl;->w:Ljava/lang/String;

    .line 301
    sput-object p1, Lcom/nokia/maps/MapImpl;->x:Ljava/lang/String;

    .line 302
    return-void
.end method

.method private a(Ljava/util/List;Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2532
    invoke-static {p2}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v0

    .line 2533
    if-eqz v0, :cond_3

    .line 2536
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapObject;

    .line 2537
    if-nez v0, :cond_2

    .line 2540
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapContainer;

    .line 2541
    invoke-static {v0}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapContainerImpl;

    .line 2542
    invoke-virtual {v0, p2}, Lcom/nokia/maps/MapContainerImpl;->c(Lcom/here/android/mpa/mapping/MapObject;)Lcom/here/android/mpa/mapping/MapObject;

    move-result-object v0

    .line 2543
    if-eqz v0, :cond_0

    .line 2548
    :cond_1
    invoke-direct {p0, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/ViewObject;)Lcom/nokia/maps/Cluster;

    move-result-object v1

    .line 2549
    if-eqz v1, :cond_2

    .line 2550
    new-instance v2, Lcom/nokia/maps/aj;

    invoke-direct {v2, v1}, Lcom/nokia/maps/aj;-><init>(Lcom/nokia/maps/Cluster;)V

    invoke-static {v2}, Lcom/nokia/maps/aj;->a(Lcom/nokia/maps/aj;)Lcom/here/android/mpa/cluster/ClusterViewObject;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2554
    :cond_2
    if-eqz v0, :cond_3

    .line 2557
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2560
    :cond_3
    return-void
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    .line 1253
    if-eqz p1, :cond_0

    .line 1254
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->O:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 1257
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    .line 1259
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/nokia/maps/MapImpl$2;

    invoke-direct {v1, p0}, Lcom/nokia/maps/MapImpl$2;-><init>(Lcom/nokia/maps/MapImpl;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1267
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1268
    return-void
.end method

.method private a(DDD)Z
    .locals 3

    .prologue
    .line 1211
    sub-double v0, p1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p5

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/here/android/mpa/mapping/MapState;Lcom/here/android/mpa/mapping/MapState;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 1200
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1207
    :cond_0
    :goto_0
    return v0

    .line 1204
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapState;->getOrientation()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p2}, Lcom/here/android/mpa/mapping/MapState;->getOrientation()F

    move-result v1

    float-to-double v4, v1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(DDD)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1205
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapState;->getTilt()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p2}, Lcom/here/android/mpa/mapping/MapState;->getTilt()F

    move-result v1

    float-to-double v4, v1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(DDD)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1206
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapState;->getZoomLevel()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/here/android/mpa/mapping/MapState;->getZoomLevel()D

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(DDD)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1207
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapState;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/mapping/MapState;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/common/GeoCoordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/nokia/maps/MapImpl;->q:Z

    return v0
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;Lcom/here/android/mpa/mapping/MapState;Lcom/here/android/mpa/mapping/MapState;)Z
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapState;Lcom/here/android/mpa/mapping/MapState;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/nokia/maps/MapImpl;[B)Z
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->captureScreen([B)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized a(Lcom/nokia/maps/MapObjectImpl;)Z
    .locals 1

    .prologue
    .line 2193
    monitor-enter p0

    .line 2195
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/nokia/maps/MapObjectImpl;->a(Lcom/nokia/maps/MapImpl;)V

    .line 2196
    instance-of v0, p1, Lcom/nokia/maps/cg;

    if-eqz v0, :cond_0

    .line 2197
    check-cast p1, Lcom/nokia/maps/cg;

    .line 2198
    invoke-virtual {p1}, Lcom/nokia/maps/cg;->e()[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->addMapObjectsNative([Ljava/lang/Object;)Z

    move-result v0

    .line 2199
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapRouteImpl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204
    :goto_0
    monitor-exit p0

    return v0

    .line 2201
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->addMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 2193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private synchronized native declared-synchronized addMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z
.end method

.method private native addMapObjectsNative([Ljava/lang/Object;)Z
.end method

.method private synchronized native declared-synchronized addRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z
.end method

.method private static final b(Lcom/here/android/mpa/mapping/Map$Animation;)I
    .locals 2

    .prologue
    .line 3601
    sget-object v0, Lcom/nokia/maps/MapImpl$17;->c:[I

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/Map$Animation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 3609
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Animation value not recognized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3603
    :pswitch_0
    const/4 v0, 0x0

    .line 3607
    :goto_0
    return v0

    .line 3605
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 3607
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 3601
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 368
    const/high16 v0, 0x3f800000    # 1.0f

    .line 369
    sget-object v1, Lcom/nokia/maps/MapImpl$17;->a:[I

    sget-object v2, Lcom/nokia/maps/MapImpl;->v:Lcom/nokia/maps/MapImpl$f;

    invoke-virtual {v2}, Lcom/nokia/maps/MapImpl$f;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 387
    :goto_0
    iget v1, p0, Lcom/nokia/maps/MapImpl;->n:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/nokia/maps/MapImpl;->n:I

    .line 388
    iget v1, p0, Lcom/nokia/maps/MapImpl;->n:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    .line 389
    iget v1, p0, Lcom/nokia/maps/MapImpl;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/nokia/maps/MapImpl;->n:I

    .line 392
    :cond_0
    iget v1, p0, Lcom/nokia/maps/MapImpl;->o:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/nokia/maps/MapImpl;->o:I

    .line 393
    iget v0, p0, Lcom/nokia/maps/MapImpl;->o:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 394
    iget v0, p0, Lcom/nokia/maps/MapImpl;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/nokia/maps/MapImpl;->o:I

    .line 397
    :cond_1
    return-void

    .line 371
    :pswitch_0
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 373
    goto :goto_0

    .line 375
    :pswitch_1
    const/high16 v0, 0x3f400000    # 0.75f

    .line 377
    goto :goto_0

    .line 380
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 381
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    .line 383
    goto :goto_0

    .line 369
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private b(Lcom/here/android/mpa/mapping/Map$Projection;)V
    .locals 1

    .prologue
    .line 2671
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    if-nez v0, :cond_0

    .line 2680
    :goto_0
    return-void

    .line 2675
    :cond_0
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Projection;->GLOBE:Lcom/here/android/mpa/mapping/Map$Projection;

    if-ne p1, v0, :cond_1

    .line 2676
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    invoke-virtual {v0}, Lcom/nokia/maps/PanoramaCoverageRasterTileSource;->b()V

    goto :goto_0

    .line 2678
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    invoke-virtual {v0}, Lcom/nokia/maps/PanoramaCoverageRasterTileSource;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->setPausedNative()V

    return-void
.end method

.method private b(Lcom/nokia/maps/MapRouteImpl;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2436
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_1

    .line 2447
    :cond_0
    :goto_0
    return-void

    .line 2440
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 2441
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2442
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/nokia/maps/MapImpl;->ac:D

    .line 2443
    iput v2, p0, Lcom/nokia/maps/MapImpl;->ae:I

    .line 2444
    iput v2, p0, Lcom/nokia/maps/MapImpl;->af:I

    .line 2445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ab:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto :goto_0
.end method

.method private declared-synchronized b(Lcom/nokia/maps/MapObjectImpl;)Z
    .locals 2

    .prologue
    .line 2282
    monitor-enter p0

    .line 2284
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1}, Lcom/nokia/maps/MapObjectImpl;->a(Lcom/nokia/maps/MapImpl;)V

    .line 2285
    instance-of v1, p1, Lcom/nokia/maps/MapMarkerImpl;

    if-eqz v1, :cond_1

    .line 2286
    move-object v0, p1

    check-cast v0, Lcom/nokia/maps/MapMarkerImpl;

    move-object v1, v0

    .line 2287
    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapMarkerImpl;)V

    .line 2288
    invoke-virtual {v1}, Lcom/nokia/maps/MapMarkerImpl;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2289
    move-object v0, p1

    check-cast v0, Lcom/nokia/maps/MapMarkerImpl;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/nokia/maps/MapMarkerImpl;->h()V

    .line 2291
    :cond_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->removeMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 2304
    :goto_0
    monitor-exit p0

    return v1

    .line 2292
    :cond_1
    :try_start_1
    instance-of v1, p1, Lcom/nokia/maps/MapContainerImpl;

    if-eqz v1, :cond_2

    .line 2295
    move-object v0, p1

    check-cast v0, Lcom/nokia/maps/MapContainerImpl;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/nokia/maps/MapContainerImpl;->c()V

    .line 2296
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->removeMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z

    move-result v1

    goto :goto_0

    .line 2297
    :cond_2
    instance-of v1, p1, Lcom/nokia/maps/cg;

    if-eqz v1, :cond_3

    .line 2298
    check-cast p1, Lcom/nokia/maps/cg;

    .line 2299
    invoke-virtual {p1}, Lcom/nokia/maps/cg;->e()[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->removeMapObjectsNative([Ljava/lang/Object;)Z

    move-result v1

    .line 2300
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/nokia/maps/MapRouteImpl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2282
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2302
    :cond_3
    :try_start_2
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->removeMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    goto :goto_0
.end method

.method private static final c(Lcom/here/android/mpa/mapping/Map$Projection;)I
    .locals 2

    .prologue
    .line 3617
    sget-object v0, Lcom/nokia/maps/MapImpl$17;->d:[I

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/Map$Projection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 3623
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Projection value not recognized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3619
    :pswitch_0
    const/4 v0, 0x0

    .line 3621
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 3617
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final c(I)Lcom/here/android/mpa/mapping/Map$Projection;
    .locals 2

    .prologue
    .line 3628
    packed-switch p0, :pswitch_data_0

    .line 3634
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Projection value not recognized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3630
    :pswitch_0
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Projection;->MERCATOR:Lcom/here/android/mpa/mapping/Map$Projection;

    .line 3632
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Projection;->GLOBE:Lcom/here/android/mpa/mapping/Map$Projection;

    goto :goto_0

    .line 3628
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/nokia/maps/MapImpl;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->F:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1387
    const-string v0, "truck"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1388
    const-string v0, "truck"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1389
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0, p2}, Lcom/nokia/maps/r;->c(Ljava/lang/String;)V

    .line 1392
    const-string v0, "normal.day"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "truck.day"

    .line 1393
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    const-string v0, "hybrid.day"

    .line 1394
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "hybrid.truck.day"

    .line 1395
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    const-string v0, "hybrid.night"

    .line 1396
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "hybrid.truck.night"

    .line 1397
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    const-string v0, "normal.night"

    .line 1398
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "truck.night"

    .line 1399
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    const-string v0, "carnav.day"

    .line 1400
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "trucknav.day"

    .line 1401
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    const-string v0, "carnav.night"

    .line 1402
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "trucknav.night"

    .line 1403
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    const-string v0, "carnav.hybrid.day"

    .line 1404
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "trucknav.hybrid.day"

    .line 1405
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    const-string v0, "carnav.hybrid.night"

    .line 1406
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "trucknav.hybrid.night"

    .line 1407
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 1440
    :cond_7
    :goto_0
    return-void

    .line 1412
    :cond_8
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1413
    :cond_9
    invoke-virtual {p2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 1415
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1418
    :cond_a
    const-string v0, "truck.day"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "normal.day"

    .line 1419
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_b
    const-string v0, "hybrid.truck.day"

    .line 1420
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "hybrid.day"

    .line 1421
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_c
    const-string v0, "hybrid.truck.night"

    .line 1422
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "hybrid.night"

    .line 1423
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_d
    const-string v0, "truck.night"

    .line 1424
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "normal.night"

    .line 1425
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_e
    const-string v0, "carnav.day"

    .line 1426
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "trucknav.day"

    .line 1427
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_f
    const-string v0, "carnav.night"

    .line 1428
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "trucknav.night"

    .line 1429
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_10
    const-string v0, "carnav.hybrid.day"

    .line 1430
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "trucknav.hybrid.day"

    .line 1431
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_11
    const-string v0, "carnav.hybrid.night"

    .line 1432
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "trucknav.hybrid.night"

    .line 1433
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 1438
    :cond_12
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0, p1, p2}, Lcom/nokia/maps/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private c(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    .locals 2

    .prologue
    .line 3158
    invoke-static {p1}, Lcom/nokia/maps/MapRasterTileSourceImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Lcom/nokia/maps/MapRasterTileSourceImpl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->removeRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z

    move-result v0

    .line 3160
    if-eqz v0, :cond_0

    .line 3161
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 3164
    :cond_0
    return v0
.end method

.method private synchronized native declared-synchronized captureScreen([B)Z
.end method

.method private native createCustomizableSchemeNative(Ljava/lang/String;Ljava/lang/String;)Lcom/nokia/maps/CustomizableSchemeImpl;
.end method

.method private native createMapNative(IILjava/lang/String;Ljava/lang/String;)Z
.end method

.method static synthetic d(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    return-void
.end method

.method private native destroyMapNative()V
.end method

.method static synthetic e(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v0, p1}, Lcom/nokia/maps/r;->b(Ljava/lang/String;)V

    .line 1384
    return-void
.end method

.method static synthetic f(Lcom/nokia/maps/MapImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private declared-synchronized f(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1452
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 1453
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    .line 1454
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1455
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1456
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "terrain.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1457
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "satellite.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1458
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "satellite.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1459
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1460
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1461
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "pedestrian.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1462
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "pedestrian.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1463
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.day.transit"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1464
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.night.transit"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1465
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.day.transit"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1466
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.night.transit"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1467
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "pedestrian.day.hybrid"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1468
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "pedestrian.night.hybrid"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1469
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.day.grey"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1470
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.night.grey"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1471
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.grey.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1472
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.grey.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1473
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "reduced.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1474
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "reduced.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1475
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.reduced.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1476
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.reduced.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1477
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "maneuver.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1479
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.traffic.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1480
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "normal.traffic.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1481
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.traffic.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1482
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.traffic.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1483
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1484
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1485
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.hybrid.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1486
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.hybrid.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1487
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.traffic.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1488
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.traffic.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1489
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.traffic.hybrid.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1490
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.traffic.hybrid.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1491
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.day.grey"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1492
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "carnav.night.grey"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1494
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "truck.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1495
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "truck.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1496
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.truck.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1497
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "hybrid.truck.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1498
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "trucknav.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1499
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "trucknav.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1500
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "trucknav.hybrid.day"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1501
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    const-string v1, "trucknav.hybrid.night"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1504
    :cond_0
    sget-object v0, Lcom/nokia/maps/MapImpl;->s:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 1452
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic g(Lcom/nokia/maps/MapImpl;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->M:Ljava/lang/Runnable;

    return-object v0
.end method

.method private g(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1508
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1509
    :cond_0
    const/4 v0, 0x0

    .line 1511
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->isUserSchemeNative(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private native geoToPixel(DDD)Lcom/here/android/mpa/mapping/Map$PixelResult;
.end method

.method private native geoToPixelNative([Lcom/nokia/maps/GeoCoordinateImpl;)[Lcom/here/android/mpa/mapping/Map$PixelResult;
.end method

.method static get(Lcom/here/android/mpa/mapping/Map;)Lcom/nokia/maps/MapImpl;
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    sget-object v1, Lcom/nokia/maps/MapImpl;->u:Lcom/nokia/maps/m;

    if-eqz v1, :cond_0

    .line 117
    sget-object v0, Lcom/nokia/maps/MapImpl;->u:Lcom/nokia/maps/m;

    invoke-interface {v0, p0}, Lcom/nokia/maps/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nokia/maps/MapImpl;

    .line 119
    :cond_0
    return-object v0
.end method

.method private native getAltitudeConversionModeNative()I
.end method

.method private native getBaseSchemeNameNative(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private synchronized native declared-synchronized getBitmapArrayStream(II)[B
.end method

.method private synchronized native declared-synchronized getBoundingBoxNative()Lcom/nokia/maps/GeoBoundingBoxImpl;
.end method

.method private native getCustomizableSchemeNative(Ljava/lang/String;)Lcom/nokia/maps/CustomizableSchemeImpl;
.end method

.method private synchronized native declared-synchronized getDetailLevelNative()I
.end method

.method private native getGlobePanModeNative()I
.end method

.method private native getSelectedObjectsNative(FF)[Lcom/here/android/mpa/common/ViewObject;
.end method

.method private native getSelectedObjectsNative(IIII)[Lcom/here/android/mpa/common/ViewObject;
.end method

.method private synchronized native declared-synchronized getTransformCenterNative()Landroid/graphics/PointF;
.end method

.method private synchronized native declared-synchronized getViewType()I
.end method

.method static synthetic h(Lcom/nokia/maps/MapImpl;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->N:Ljava/lang/Runnable;

    return-object v0
.end method

.method private h(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1962
    new-instance v0, Lcom/nokia/maps/MapImpl$9;

    invoke-direct {v0, p0, p1}, Lcom/nokia/maps/MapImpl$9;-><init>(Lcom/nokia/maps/MapImpl;Ljava/lang/String;)V

    .line 1974
    invoke-static {}, Lcom/nokia/maps/MapSettings;->m()Lcom/nokia/maps/MapSettings$b;

    move-result-object v1

    sget-object v2, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    if-ne v1, v2, :cond_0

    .line 1975
    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/Runnable;)V

    .line 1979
    :goto_0
    return-void

    .line 1977
    :cond_0
    invoke-static {v0}, Lcom/nokia/maps/fh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->L()V

    return-void
.end method

.method private native isLandmarksVisibleNative()Z
.end method

.method private synchronized native declared-synchronized isPoiCategoryVisibleNative(I)Z
.end method

.method private native isUserSchemeNative(Ljava/lang/String;)Z
.end method

.method static synthetic j(Lcom/nokia/maps/MapImpl;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->M()V

    return-void
.end method

.method static synthetic k(Lcom/nokia/maps/MapImpl;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->X:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/nokia/maps/MapImpl;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Y:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic m(Lcom/nokia/maps/MapImpl;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Z:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private mapMoveBegin()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2844
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 2845
    return-void
.end method

.method private native moveTo(FFIDFF)V
.end method

.method private synchronized native declared-synchronized moveToNative(IIIIFF)V
.end method

.method static synthetic n(Lcom/nokia/maps/MapImpl;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    return v0
.end method

.method static synthetic o(Lcom/nokia/maps/MapImpl;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    return v0
.end method

.method private onMapSchemeChanged(Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2828
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->h(Ljava/lang/String;)V

    .line 2829
    return-void
.end method

.method private onOrientationChangeStart()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2859
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 2860
    return-void
.end method

.method private onRenderBufferCreated()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2833
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->O()V

    .line 2834
    return-void
.end method

.method private onScaleChangeStart()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2854
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 2855
    return-void
.end method

.method private onTiltChangeStart()V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2849
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 2850
    return-void
.end method

.method static synthetic p(Lcom/nokia/maps/MapImpl;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private synchronized native declared-synchronized panNative(FFFF)V
.end method

.method private native pixelToGeoNative([Ljava/lang/Object;)[Lcom/nokia/maps/GeoCoordinateImpl;
.end method

.method private native removeCustomizableSchemeNative(Ljava/lang/String;)Z
.end method

.method private native removeMapObjectNative(Lcom/nokia/maps/MapObjectImpl;)Z
.end method

.method private synchronized native declared-synchronized removeMapObjectsNative([Ljava/lang/Object;)Z
.end method

.method private native screenToGeoCoordinates(FF)Lcom/nokia/maps/GeoCoordinateImpl;
.end method

.method private native screenToGeoCoordinates(FFF)Lcom/nokia/maps/GeoCoordinateImpl;
.end method

.method private native setAltitudeConversionMode(I)V
.end method

.method private native setCenterNative(DDIDFF)V
.end method

.method private native setClipRect(IIIIFF)V
.end method

.method private synchronized native declared-synchronized setDetailLevel(I)V
.end method

.method private synchronized native declared-synchronized setFleetFeaturesVisibleNative(I)V
.end method

.method private native setGlobePanModeNative(I)V
.end method

.method private synchronized native declared-synchronized setLandmarksVisibleNative(Z)Z
.end method

.method private native setMapDisplayLanguageNative(Ljava/lang/String;)Z
.end method

.method private synchronized native declared-synchronized setMapSchemeNative(Ljava/lang/String;)Z
.end method

.method private native setMapSecondaryDisplayLanguageNative(Ljava/lang/String;)Z
.end method

.method private native setMaximumTiltFunctionNative(Lcom/nokia/maps/fn;)V
.end method

.method private native setOrientation(FI)V
.end method

.method private native setPausedNative()V
.end method

.method private native setTilt(FI)V
.end method

.method private native setTrafficInfoVisibleNative(Z)I
.end method

.method private native setTransformCenterNative(FF)V
.end method

.method private native setViewRect(IIIIFF)V
.end method

.method private native setViewTypeNative(I)V
.end method

.method private native setVisuals(IIFF)V
.end method

.method private synchronized native declared-synchronized setZoomLevel(DI)V
.end method

.method private synchronized native declared-synchronized showPoiCategoryNative(IZ)Z
.end method

.method private synchronized native declared-synchronized viewGeometryChangedNative(II)V
.end method

.method private native zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IF)V
.end method

.method private native zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IFF)V
.end method

.method private native zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IIIF)V
.end method

.method private native zoomToNative(Lcom/nokia/maps/GeoBoundingBoxImpl;IIIIIF)V
.end method


# virtual methods
.method public A()Lcom/here/android/mpa/mapping/MapTrafficLayer;
    .locals 1

    .prologue
    .line 3200
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ak:Lcom/here/android/mpa/mapping/MapTrafficLayer;

    if-nez v0, :cond_1

    .line 3201
    monitor-enter p0

    .line 3202
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ak:Lcom/here/android/mpa/mapping/MapTrafficLayer;

    if-nez v0, :cond_0

    .line 3203
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapTrafficLayerNative()Lcom/nokia/maps/MapTrafficLayerImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/MapTrafficLayerImpl;->a(Lcom/nokia/maps/MapTrafficLayerImpl;)Lcom/here/android/mpa/mapping/MapTrafficLayer;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->ak:Lcom/here/android/mpa/mapping/MapTrafficLayer;

    .line 3205
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3207
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ak:Lcom/here/android/mpa/mapping/MapTrafficLayer;

    return-object v0

    .line 3205
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method B()Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;
    .locals 1

    .prologue
    .line 3367
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->am:Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;

    return-object v0
.end method

.method C()V
    .locals 2

    .prologue
    .line 3388
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->an:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 3389
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapRasterTileSource;

    .line 3390
    invoke-static {v0}, Lcom/nokia/maps/MapRasterTileSourceImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Lcom/nokia/maps/MapRasterTileSourceImpl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->removeRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z

    goto :goto_0

    .line 3392
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->an:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3394
    :cond_1
    return-void
.end method

.method D()V
    .locals 3

    .prologue
    .line 3400
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->H()V

    .line 3401
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->Q()V

    .line 3403
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3404
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 3405
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 3408
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 3407
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 3408
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3409
    return-void
.end method

.method E()V
    .locals 3

    .prologue
    .line 3415
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 3416
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 3417
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 3420
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 3419
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 3420
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3421
    return-void
.end method

.method public F()V
    .locals 1

    .prologue
    .line 3435
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 3436
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 3437
    return-void
.end method

.method public declared-synchronized a(Landroid/graphics/PointF;F)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 2

    .prologue
    .line 1780
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1, p2}, Lcom/nokia/maps/MapImpl;->screenToGeoCoordinates(FFF)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->create(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/common/GeoCoordinate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;
    .locals 1

    .prologue
    .line 2919
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->get(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    .line 2920
    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/mapping/Map$PixelResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 2919
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/mapping/Map$PixelResult;
    .locals 8

    .prologue
    .line 2927
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2928
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GeoCoordinate supplied is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2931
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v4

    .line 2932
    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->c()D

    move-result-wide v6

    move-object v1, p0

    .line 2931
    invoke-direct/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->geoToPixel(DDD)Lcom/here/android/mpa/mapping/Map$PixelResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2933
    monitor-exit p0

    return-object v0
.end method

.method a()Lcom/nokia/maps/q;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->C:Lcom/nokia/maps/q;

    return-object v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/ViewRect;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2502
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getX()I

    move-result v0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getY()I

    move-result v1

    .line 2503
    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getHeight()I

    move-result v3

    .line 2502
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/nokia/maps/MapImpl;->getSelectedObjectsNative(IIII)[Lcom/here/android/mpa/common/ViewObject;

    move-result-object v0

    .line 2505
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->a([Lcom/here/android/mpa/common/ViewObject;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 2502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1807
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    .line 1808
    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->pixelToGeoNative([Ljava/lang/Object;)[Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1807
    invoke-static {v1}, Lcom/nokia/maps/GeoCoordinateImpl;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(D)V
    .locals 1

    .prologue
    .line 948
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {p0, p1, p2, v0}, Lcom/nokia/maps/MapImpl;->a(DLcom/here/android/mpa/mapping/Map$Animation;)V

    .line 949
    return-void
.end method

.method public a(DLandroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 7

    .prologue
    .line 973
    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    if-nez v0, :cond_1

    .line 974
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This API cannot be called until the map control has been given a size"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 977
    :cond_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMinZoomLevel()D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMaxZoomLevel()D

    move-result-wide v0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_2

    .line 978
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 980
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 981
    iget v0, p3, Landroid/graphics/PointF;->x:F

    float-to-int v4, v0

    iget v0, p3, Landroid/graphics/PointF;->y:F

    float-to-int v5, v0

    invoke-static {p4}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v6

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/nokia/maps/MapImpl;->setZoomLevel(DIII)V

    .line 983
    :cond_2
    return-void
.end method

.method public a(DLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 3

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMinZoomLevel()D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMaxZoomLevel()D

    move-result-wide v0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 963
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 965
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 966
    invoke-static {p3}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/nokia/maps/MapImpl;->setZoomLevel(DI)V

    .line 968
    :cond_0
    return-void
.end method

.method public declared-synchronized a(F)V
    .locals 1

    .prologue
    .line 1040
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->a(FLcom/here/android/mpa/mapping/Map$Animation;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    monitor-exit p0

    return-void

    .line 1040
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFFFFF)V
    .locals 8

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1004
    new-instance v0, Lcom/nokia/maps/MapImpl$22;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/nokia/maps/MapImpl$22;-><init>(Lcom/nokia/maps/MapImpl;FFFFFF)V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->K:Ljava/lang/Runnable;

    .line 1011
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1013
    return-void
.end method

.method public declared-synchronized a(FLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1

    .prologue
    .line 1051
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 1053
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 1054
    invoke-static {p2}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->setOrientation(FI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055
    monitor-exit p0

    return-void

    .line 1051
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 888
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getFleetFeaturesVisible()I

    move-result v0

    .line 890
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->setFleetFeaturesVisibleNative(I)V

    .line 894
    and-int v1, v0, p1

    xor-int/2addr v1, p1

    .line 896
    and-int/lit8 v2, v1, 0x1

    if-lez v2, :cond_0

    .line 897
    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/nokia/maps/r;->d(Ljava/lang/String;)V

    .line 901
    :cond_0
    and-int/lit8 v2, v1, 0x4

    if-gtz v2, :cond_1

    and-int/lit8 v1, v1, 0x2

    if-lez v1, :cond_2

    .line 904
    :cond_1
    and-int/lit8 v0, v0, 0x6

    if-nez v0, :cond_2

    .line 905
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/nokia/maps/r;->e(Ljava/lang/String;)V

    .line 909
    :cond_2
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 910
    return-void
.end method

.method protected declared-synchronized a(II)V
    .locals 2

    .prologue
    .line 1067
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/nokia/maps/MapImpl;->b:I

    .line 1068
    iput p2, p0, Lcom/nokia/maps/MapImpl;->c:I

    .line 1069
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/MapImpl;->viewGeometryChangedNative(II)V

    .line 1071
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->setTransformCenterNative(FF)V

    .line 1073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075
    :cond_0
    monitor-exit p0

    return-void

    .line 1067
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/graphics/PointF;)V
    .locals 2

    .prologue
    .line 605
    monitor-enter p0

    if-nez p1, :cond_0

    .line 615
    :goto_0
    monitor-exit p0

    return-void

    .line 609
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    if-nez v0, :cond_1

    .line 610
    iput-object p1, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 605
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 612
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->d:Landroid/graphics/PointF;

    .line 613
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->setTransformCenterNative(FF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4

    .prologue
    .line 3593
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/nokia/maps/MapImpl;->a(FFFF)V

    .line 3594
    return-void
.end method

.method public declared-synchronized a(Landroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 9

    .prologue
    .line 3576
    monitor-enter p0

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p6, v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMaxTilt()F

    move-result v0

    cmpl-float v0, p6, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMinTilt()F

    move-result v0

    cmpg-float v0, p6, v0

    if-gez v0, :cond_1

    .line 3577
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "New tilt value is out of range."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3579
    :cond_1
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p3, v0

    if-eqz v0, :cond_3

    .line 3580
    :try_start_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMaxZoomLevel()D

    move-result-wide v0

    cmpl-double v0, p3, v0

    if-gtz v0, :cond_2

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMinZoomLevel()D

    move-result-wide v0

    cmpg-double v0, p3, v0

    if-gez v0, :cond_3

    .line 3581
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "New zoom level value is out of range."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3584
    :cond_3
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 3585
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 3586
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-static {p2}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v3

    move-object v0, p0

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/nokia/maps/MapImpl;->moveTo(FFIDFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3587
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/cluster/ClusterLayer;)V
    .locals 2

    .prologue
    .line 2071
    monitor-enter p0

    if-nez p1, :cond_0

    .line 2072
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "layer cannot ne null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2075
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2076
    invoke-static {p1}, Lcom/nokia/maps/ag;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)Lcom/nokia/maps/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nokia/maps/ag;->a(Lcom/nokia/maps/MapImpl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoBoundingBox;IILcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 6

    .prologue
    .line 2915
    invoke-static {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->get(Lcom/here/android/mpa/common/GeoBoundingBox;)Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoBoundingBoxImpl;IILcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 2916
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 1

    .prologue
    .line 2910
    invoke-static {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->get(Lcom/here/android/mpa/common/GeoBoundingBox;)Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 2911
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 1

    .prologue
    .line 2900
    invoke-static {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->get(Lcom/here/android/mpa/common/GeoBoundingBox;)Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 2901
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;FF)V
    .locals 1

    .prologue
    .line 2905
    invoke-static {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->get(Lcom/here/android/mpa/common/GeoBoundingBox;)Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;FF)V

    .line 2906
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1

    .prologue
    .line 2893
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2894
    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->get(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 2896
    :cond_0
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 9

    .prologue
    .line 2887
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2888
    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->get(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 2890
    :cond_0
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/OnScreenCaptureListener;)V
    .locals 2

    .prologue
    .line 3032
    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    if-gtz v0, :cond_1

    .line 3033
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Width and height must be > 0."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3035
    :cond_1
    if-nez p1, :cond_2

    .line 3036
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OnScreenCaptureListener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3040
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/nokia/maps/MapImpl$d;

    invoke-direct {v1, p0, p1}, Lcom/nokia/maps/MapImpl$d;-><init>(Lcom/nokia/maps/MapImpl;Lcom/here/android/mpa/common/OnScreenCaptureListener;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3041
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3042
    return-void
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/common/ViewRect;Landroid/graphics/PointF;)V
    .locals 7

    .prologue
    const/high16 v6, -0x40800000    # -1.0f

    .line 3000
    monitor-enter p0

    .line 3002
    if-eqz p2, :cond_0

    .line 3003
    :try_start_0
    iget v5, p2, Landroid/graphics/PointF;->x:F

    .line 3004
    iget v6, p2, Landroid/graphics/PointF;->y:F

    .line 3007
    :goto_0
    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getX()I

    move-result v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getY()I

    move-result v2

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getHeight()I

    move-result v4

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/nokia/maps/MapImpl;->setClipRect(IIIIFF)V

    .line 3008
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3009
    monitor-exit p0

    return-void

    .line 3000
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v5, v6

    goto :goto_0
.end method

.method public a(Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;)V
    .locals 0

    .prologue
    .line 3363
    iput-object p1, p0, Lcom/nokia/maps/MapImpl;->am:Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;

    .line 3364
    return-void
.end method

.method public a(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V
    .locals 1

    .prologue
    .line 1893
    if-eqz p1, :cond_0

    .line 1894
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1896
    :cond_0
    return-void
.end method

.method public a(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 1

    .prologue
    .line 1881
    if-eqz p1, :cond_0

    .line 1882
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->X:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1884
    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/mapping/Map$Projection;)V
    .locals 2

    .prologue
    .line 2619
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Projection;->GLOBE:Lcom/here/android/mpa/mapping/Map$Projection;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/here/android/mpa/mapping/Map$Projection;->MERCATOR:Lcom/here/android/mpa/mapping/Map$Projection;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Deprecated Projection mode."

    invoke-static {v0, v1}, Lcom/nokia/maps/ef;->a(ZLjava/lang/String;)V

    .line 2621
    invoke-static {p1}, Lcom/nokia/maps/MapImpl;->c(Lcom/here/android/mpa/mapping/Map$Projection;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setViewTypeNative(I)V

    .line 2622
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Projection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2623
    monitor-exit p0

    return-void

    .line 2619
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/here/android/mpa/mapping/customization/CustomizableScheme;)V
    .locals 2

    .prologue
    .line 1325
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/customization/CustomizableScheme;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1326
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1327
    monitor-enter p0

    .line 1328
    :try_start_0
    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    .line 1329
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1330
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1331
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1334
    :cond_0
    return-void

    .line 1329
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoBoundingBoxImpl;IILcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 8

    .prologue
    .line 747
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    if-nez v0, :cond_1

    .line 748
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This API cannot be called until the map control has been given a size"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 747
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 750
    :cond_1
    if-lez p2, :cond_2

    if-gtz p3, :cond_3

    .line 751
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Both width and height must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 752
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->e()Z

    move-result v0

    if-nez v0, :cond_4

    .line 755
    iget-object v7, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/nokia/maps/MapImpl$21;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/nokia/maps/MapImpl$21;-><init>(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;IILcom/here/android/mpa/mapping/Map$Animation;F)V

    invoke-virtual {v7, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 763
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 765
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 7

    .prologue
    .line 707
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    if-nez v0, :cond_1

    .line 708
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This API cannot be called until the map control has been given a size"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 710
    :cond_1
    if-eqz p1, :cond_2

    :try_start_1
    invoke-virtual {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 713
    iget-object v6, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/nokia/maps/MapImpl$20;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nokia/maps/MapImpl$20;-><init>(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 722
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 724
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 2

    .prologue
    .line 643
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/nokia/maps/MapImpl$18;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/nokia/maps/MapImpl$18;-><init>(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 656
    :cond_0
    monitor-exit p0

    return-void

    .line 643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;FF)V
    .locals 7

    .prologue
    .line 674
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/nokia/maps/GeoBoundingBoxImpl;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 677
    iget-object v6, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/nokia/maps/MapImpl$19;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nokia/maps/MapImpl$19;-><init>(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoBoundingBoxImpl;Lcom/here/android/mpa/mapping/Map$Animation;FF)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    :cond_0
    monitor-exit p0

    return-void

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 8

    .prologue
    .line 569
    monitor-enter p0

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v7, -0x40800000    # -1.0f

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    :try_start_0
    invoke-virtual/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    monitor-exit p0

    return-void

    .line 569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 11

    .prologue
    .line 532
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/nokia/maps/MapImpl;->e:Ljava/lang/String;

    const-string v3, "point(%f, %f)"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    if-eqz p1, :cond_2

    .line 533
    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    .line 532
    invoke-static {v2, v3, v4}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 537
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    if-eq p2, v0, :cond_3

    .line 539
    new-instance v0, Lcom/nokia/maps/MapImpl$12;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/nokia/maps/MapImpl$12;-><init>(Lcom/nokia/maps/MapImpl;Lcom/nokia/maps/GeoCoordinateImpl;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 548
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    :goto_2
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558
    :cond_0
    monitor-exit p0

    return-void

    .line 532
    :cond_1
    const-wide/high16 v0, 0x41d0000000000000L    # 1.073741824E9

    goto :goto_0

    .line 533
    :cond_2
    const-wide/high16 v0, 0x41d0000000000000L    # 1.073741824E9

    goto :goto_1

    .line 550
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 551
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 552
    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v4

    invoke-static {p2}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v6

    move-object v1, p0

    move-wide v7, p3

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/nokia/maps/MapImpl;->setCenterNative(DDIDFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/nokia/maps/MapImpl$a;)V
    .locals 1

    .prologue
    .line 1869
    if-eqz p1, :cond_0

    .line 1870
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->W:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1872
    :cond_0
    return-void
.end method

.method public a(Lcom/nokia/maps/MapImpl$e;)V
    .locals 1

    .prologue
    .line 1857
    if-eqz p1, :cond_0

    .line 1858
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->V:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 1860
    :cond_0
    return-void
.end method

.method public a(Lcom/nokia/maps/MapImpl$h;)V
    .locals 2

    .prologue
    .line 1905
    if-eqz p1, :cond_0

    .line 1906
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Z:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1907
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->E:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapOverlay;

    .line 1908
    invoke-interface {p1, v0}, Lcom/nokia/maps/MapImpl$h;->a(Lcom/here/android/mpa/mapping/MapOverlay;)V

    goto :goto_0

    .line 1912
    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lcom/nokia/maps/bb;)V
    .locals 1

    .prologue
    .line 824
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/nokia/maps/bb;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setGlobePanModeNative(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    monitor-exit p0

    return-void

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/nokia/maps/ev;Lcom/nokia/maps/ev;)V
    .locals 3

    .prologue
    .line 784
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    monitor-enter v1

    .line 785
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->J:Ljava/util/List;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 786
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 788
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 789
    return-void

    .line 786
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 3427
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ao:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3428
    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 3458
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->aq:Lcom/nokia/maps/fm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/fm;->a(Ljava/lang/Runnable;J)V

    .line 3459
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1301
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1302
    const-string v0, "satellite"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hybrid"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1303
    :cond_0
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->Q:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 1305
    :cond_1
    const-string v0, "truck"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1306
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->R:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 1308
    :cond_2
    monitor-enter p0

    .line 1309
    :try_start_0
    iput-object p1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    .line 1310
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1311
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1312
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1322
    :cond_3
    :goto_0
    return-void

    .line 1310
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1314
    :cond_4
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1315
    monitor-enter p0

    .line 1316
    :try_start_2
    iput-object p1, p0, Lcom/nokia/maps/MapImpl;->S:Ljava/lang/String;

    .line 1317
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1318
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1319
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    goto :goto_0

    .line 1317
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 515
    iput-boolean p1, p0, Lcom/nokia/maps/MapImpl;->q:Z

    .line 517
    iget-boolean v0, p0, Lcom/nokia/maps/MapImpl;->q:Z

    if-eqz v0, :cond_0

    .line 518
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->setPausedNative()V

    .line 519
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Ljava/lang/Runnable;)V

    .line 521
    :cond_0
    return-void
.end method

.method public a([IZ)V
    .locals 0

    .prologue
    .line 3213
    invoke-virtual {p0, p1, p2}, Lcom/nokia/maps/MapImpl;->setLayerCategory([IZ)V

    .line 3214
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3215
    return-void
.end method

.method public a(Lcom/here/android/mpa/common/IconCategory;)Z
    .locals 1

    .prologue
    .line 871
    invoke-static {p1}, Lcom/nokia/maps/bg;->a(Lcom/here/android/mpa/common/IconCategory;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->isPoiCategoryVisibleNative(I)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/here/android/mpa/common/IconCategory;Z)Z
    .locals 1

    .prologue
    .line 858
    invoke-static {p1}, Lcom/nokia/maps/bg;->a(Lcom/here/android/mpa/common/IconCategory;)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/nokia/maps/MapImpl;->showPoiCategoryNative(IZ)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/mapping/MapObject;)Z
    .locals 4

    .prologue
    .line 2164
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v1

    .line 2165
    const/4 v0, 0x0

    .line 2166
    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2167
    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapObjectImpl;)Z

    move-result v0

    .line 2168
    if-eqz v0, :cond_0

    .line 2169
    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2170
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapObject;->getType()Lcom/here/android/mpa/mapping/MapObject$Type;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/mapping/MapObject$Type;->CONTAINER:Lcom/here/android/mpa/mapping/MapObject$Type;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    .line 2171
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2174
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    check-cast p1, Lcom/here/android/mpa/mapping/MapContainer;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2178
    :cond_0
    monitor-exit p0

    return v0

    .line 2164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/mapping/MapObject;Z)Z
    .locals 1

    .prologue
    .line 2182
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 2183
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapObject;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2185
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapObjectImpl;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 2182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/mapping/MapOverlay;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2108
    monitor-enter p0

    if-nez p1, :cond_0

    .line 2109
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null overlay not allowed."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2111
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->E:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2112
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapOverlay;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3094
    monitor-enter p0

    .line 3098
    :try_start_0
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 3099
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3100
    invoke-static {p1}, Lcom/nokia/maps/MapRasterTileSourceImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Lcom/nokia/maps/MapRasterTileSourceImpl;

    move-result-object v1

    .line 3101
    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->addRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z

    move-result v0

    .line 3102
    if-eqz v0, :cond_1

    .line 3103
    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3104
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3106
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/nokia/maps/MapRasterTileSourceImpl;->getUrl(III)Ljava/lang/String;

    move-result-object v1

    .line 3107
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "here.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3109
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/nokia/maps/r;->g(Ljava/lang/String;)V

    .line 3111
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 3112
    const-class v2, Lcom/here/android/mpa/mapping/HistoricalTrafficRasterTileSource;

    if-ne v1, v2, :cond_1

    .line 3113
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/nokia/maps/r;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3123
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 3118
    :catch_0
    move-exception v1

    .line 3120
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3094
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    .line 1644
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 1646
    :goto_0
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setMapDisplayLanguageNative(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1647
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1648
    const/4 v0, 0x1

    .line 1651
    :goto_1
    return v0

    .line 1644
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1645
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1651
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized b(D)D
    .locals 3

    .prologue
    .line 995
    monitor-enter p0

    double-to-float v0, p1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->getZoomLevelToZoomScale(F)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/nokia/maps/MapImpl;->b:I

    return v0
.end method

.method public declared-synchronized b(Landroid/graphics/PointF;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 2

    .prologue
    .line 1766
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->screenToGeoCoordinates(FF)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->create(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/common/GeoCoordinate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;
    .locals 3

    .prologue
    .line 3223
    const-string v0, "newSchemeName cannot be null"

    invoke-static {p1, v0}, Lcom/nokia/maps/ef;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3224
    const-string v0, "baseSchemeName cannot be null"

    invoke-static {p2, v0}, Lcom/nokia/maps/ef;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3226
    invoke-direct {p0, p2}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3227
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "baseSchemeName is not valid"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3230
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3231
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "newSchemeName is not valid"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3234
    :cond_1
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3235
    :cond_2
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "newSchemeName already exist"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3238
    :cond_3
    const-string v0, "satellite"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "hybrid"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3239
    :cond_4
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->Q:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 3241
    :cond_5
    const-string v0, "truck"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3242
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->R:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 3246
    :cond_6
    invoke-static {}, Lcom/nokia/maps/ApplicationContext;->b()Lcom/nokia/maps/ApplicationContext;

    move-result-object v0

    const/16 v1, 0x2b

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->al:Lcom/nokia/maps/ApplicationContext$c;

    invoke-virtual {v0, v1, v2}, Lcom/nokia/maps/ApplicationContext;->check(ILcom/nokia/maps/ApplicationContext$c;)V

    .line 3248
    invoke-direct {p0, p1, p2}, Lcom/nokia/maps/MapImpl;->createCustomizableSchemeNative(Ljava/lang/String;Ljava/lang/String;)Lcom/nokia/maps/CustomizableSchemeImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/CustomizableSchemeImpl;->a(Lcom/nokia/maps/CustomizableSchemeImpl;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;

    move-result-object v0

    .line 3251
    if-eqz v0, :cond_7

    .line 3252
    const-string v1, "createscheme"

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->e(Ljava/lang/String;)V

    .line 3255
    :cond_7
    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1517
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1518
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->getBaseSchemeNameNative(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1520
    :cond_0
    return-object p1
.end method

.method public declared-synchronized b(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/Map$PixelResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1820
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/nokia/maps/GeoCoordinateImpl;->a(Ljava/util/List;)[Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->geoToPixelNative([Lcom/nokia/maps/GeoCoordinateImpl;)[Lcom/here/android/mpa/mapping/Map$PixelResult;

    move-result-object v0

    .line 1821
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 1820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(F)V
    .locals 1

    .prologue
    .line 1112
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->b(FLcom/here/android/mpa/mapping/Map$Animation;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1113
    monitor-exit p0

    return-void

    .line 1112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1

    .prologue
    .line 1116
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMinTilt()F

    move-result v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMaxTilt()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 1117
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->K()V

    .line 1119
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->I()V

    .line 1120
    invoke-static {p2}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Animation;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->setTilt(FI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1122
    :cond_0
    monitor-exit p0

    return-void

    .line 1116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 917
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->setPedestrianFeaturesVisibleNative(I)V

    .line 918
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 919
    return-void
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/cluster/ClusterLayer;)V
    .locals 1

    .prologue
    .line 2088
    monitor-enter p0

    if-nez p1, :cond_1

    .line 2095
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2092
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093
    invoke-static {p1}, Lcom/nokia/maps/ag;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)Lcom/nokia/maps/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/ag;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/common/ViewRect;)V
    .locals 7

    .prologue
    .line 2957
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getX()I

    move-result v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getY()I

    move-result v2

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/here/android/mpa/common/ViewRect;->getHeight()I

    move-result v4

    .line 2958
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->d()Landroid/graphics/PointF;

    move-result-object v0

    iget v5, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->d()Landroid/graphics/PointF;

    move-result-object v0

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object v0, p0

    .line 2957
    invoke-direct/range {v0 .. v6}, Lcom/nokia/maps/MapImpl;->setViewRect(IIIIFF)V

    .line 2959
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2960
    monitor-exit p0

    return-void

    .line 2957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V
    .locals 1

    .prologue
    .line 1899
    if-eqz p1, :cond_0

    .line 1900
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Y:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1902
    :cond_0
    return-void
.end method

.method public b(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 1

    .prologue
    .line 1887
    if-eqz p1, :cond_0

    .line 1888
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->X:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1890
    :cond_0
    return-void
.end method

.method public b(Lcom/nokia/maps/MapImpl$a;)V
    .locals 1

    .prologue
    .line 1875
    if-eqz p1, :cond_0

    .line 1876
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->W:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1878
    :cond_0
    return-void
.end method

.method public b(Lcom/nokia/maps/MapImpl$e;)V
    .locals 1

    .prologue
    .line 1863
    if-eqz p1, :cond_0

    .line 1864
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->V:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1866
    :cond_0
    return-void
.end method

.method public b(Lcom/nokia/maps/MapImpl$h;)V
    .locals 2

    .prologue
    .line 1915
    if-eqz p1, :cond_0

    .line 1916
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->Z:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->E:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapOverlay;

    .line 1918
    invoke-interface {p1, v0}, Lcom/nokia/maps/MapImpl$h;->b(Lcom/here/android/mpa/mapping/MapOverlay;)V

    goto :goto_0

    .line 1922
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 3431
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3432
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 836
    sget-object v0, Lcom/here/android/mpa/common/IconCategory;->ALL:Lcom/here/android/mpa/common/IconCategory;

    invoke-static {v0}, Lcom/nokia/maps/bg;->a(Lcom/here/android/mpa/common/IconCategory;)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/nokia/maps/MapImpl;->showPoiCategoryNative(IZ)Z

    .line 837
    return-void
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/mapping/MapObject;)Z
    .locals 3

    .prologue
    .line 2267
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v1

    .line 2268
    const/4 v0, 0x0

    .line 2269
    if-eqz v1, :cond_0

    .line 2270
    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->b(Lcom/nokia/maps/MapObjectImpl;)Z

    move-result v0

    .line 2272
    :cond_0
    if-eqz v0, :cond_1

    .line 2273
    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/MapObject;->getType()Lcom/here/android/mpa/mapping/MapObject$Type;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/mapping/MapObject$Type;->CONTAINER:Lcom/here/android/mpa/mapping/MapObject$Type;

    if-ne v1, v2, :cond_1

    .line 2275
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2278
    :cond_1
    monitor-exit p0

    return v0

    .line 2267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/here/android/mpa/mapping/MapOverlay;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2129
    monitor-enter p0

    if-nez p1, :cond_0

    .line 2130
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null overlay not allowed."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2132
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->E:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2133
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapOverlay;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2134
    const/4 v0, 0x1

    .line 2136
    :cond_1
    monitor-exit p0

    return v0
.end method

.method public b(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    .locals 2

    .prologue
    .line 3134
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3135
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->aj:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    .line 3136
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3137
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    .line 1689
    if-nez p1, :cond_0

    .line 1690
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setMapSecondaryDisplayLanguageNative(Ljava/lang/String;)Z

    move-result v0

    .line 1699
    :goto_0
    return v0

    .line 1693
    :cond_0
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 1695
    :goto_1
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setMapSecondaryDisplayLanguageNative(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1696
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 1697
    const/4 v0, 0x1

    goto :goto_0

    .line 1693
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1694
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1699
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/nokia/maps/MapImpl;->c:I

    return v0
.end method

.method public declared-synchronized c(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->e(Landroid/graphics/PointF;)Lcom/here/android/mpa/common/ViewRect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;

    move-result-object v0

    .line 2459
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->d(Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v1

    .line 2460
    invoke-static {v0, v1}, Lcom/nokia/maps/ViewObjectImpl;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2461
    monitor-exit p0

    return-object v0

    .line 2458
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 3446
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/nokia/maps/MapImpl;->a(Ljava/lang/Runnable;J)V

    .line 3447
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 1244
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/nokia/maps/MapImpl;->a(ZZ)V

    .line 1245
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 3274
    const-string v0, "schemeName cannot be null"

    invoke-static {p1, v0}, Lcom/nokia/maps/ef;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3276
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3277
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Scheme is not removable"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3280
    :cond_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3281
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Scheme is not valid"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3284
    :cond_1
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3285
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Current Scheme can not be removed"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3288
    :cond_2
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->removeCustomizableSchemeNative(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized c(Ljava/util/List;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2216
    monitor-enter p0

    .line 2218
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 2254
    :goto_0
    monitor-exit p0

    return v7

    .line 2221
    :cond_0
    :try_start_1
    new-instance v8, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v8, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    .line 2222
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v6

    move v5, v7

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/mapping/MapObject;

    .line 2223
    if-eqz v1, :cond_3

    .line 2224
    invoke-static {v1}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v3

    .line 2226
    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v3}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2227
    invoke-interface {v8, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v1, v2

    move v2, v5

    :goto_2
    move v5, v2

    move v2, v1

    .line 2250
    goto :goto_1

    .line 2230
    :cond_1
    invoke-virtual {v3, p0}, Lcom/nokia/maps/MapObjectImpl;->a(Lcom/nokia/maps/MapImpl;)V

    .line 2231
    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v3}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232
    add-int/lit8 v4, v2, 0x1

    .line 2233
    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/MapObject;->getType()Lcom/here/android/mpa/mapping/MapObject$Type;

    move-result-object v2

    sget-object v10, Lcom/here/android/mpa/mapping/MapObject$Type;->CONTAINER:Lcom/here/android/mpa/mapping/MapObject$Type;

    if-ne v2, v10, :cond_2

    iget-object v2, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    .line 2234
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2237
    iget-object v10, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    move-object v0, v1

    check-cast v0, Lcom/here/android/mpa/mapping/MapContainer;

    move-object v2, v0

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2240
    :cond_2
    instance-of v2, v3, Lcom/nokia/maps/cg;

    if-eqz v2, :cond_7

    .line 2241
    move-object v0, v3

    check-cast v0, Lcom/nokia/maps/cg;

    move-object v2, v0

    .line 2242
    invoke-virtual {v2}, Lcom/nokia/maps/cg;->e()[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/nokia/maps/MapImpl;->addMapObjectsNative([Ljava/lang/Object;)Z

    move-result v3

    .line 2243
    invoke-direct {p0, v2}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapRouteImpl;)V

    .line 2244
    invoke-interface {v8, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v1, v4

    move v2, v3

    goto :goto_2

    :cond_3
    move v1, v2

    move v2, v6

    .line 2248
    goto :goto_2

    .line 2251
    :cond_4
    if-eqz v5, :cond_5

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 2252
    invoke-static {v8}, Lcom/nokia/maps/MapObjectImpl;->a(Ljava/util/List;)[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->addMapObjectsNative([Ljava/lang/Object;)Z

    move-result v5

    .line 2254
    :cond_5
    if-eqz v5, :cond_6

    invoke-interface {p1}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-ne v1, v2, :cond_6

    move v6, v7

    :cond_6
    move v7, v6

    goto/16 :goto_0

    .line 2216
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_7
    move v1, v4

    move v2, v5

    goto :goto_2
.end method

.method public declared-synchronized d()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 625
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->getTransformCenterNative()Landroid/graphics/PointF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 626
    monitor-exit p0

    return-object v0

    .line 625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;
    .locals 2

    .prologue
    .line 3295
    const-string v0, "schemeName cannot be null"

    invoke-static {p1, v0}, Lcom/nokia/maps/ef;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3297
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3298
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "This scheme is not configurable"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3301
    :cond_0
    const/4 v0, 0x0

    .line 3303
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3304
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->getCustomizableSchemeNative(Ljava/lang/String;)Lcom/nokia/maps/CustomizableSchemeImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/CustomizableSchemeImpl;->a(Lcom/nokia/maps/CustomizableSchemeImpl;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;

    move-result-object v0

    .line 3307
    :cond_1
    return-object v0
.end method

.method public declared-synchronized d(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2472
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1}, Lcom/nokia/maps/MapImpl;->getSelectedObjectsNative(FF)[Lcom/here/android/mpa/common/ViewObject;

    move-result-object v0

    .line 2474
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->a([Lcom/here/android/mpa/common/ViewObject;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 2472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2329
    monitor-enter p0

    const/4 v1, 0x1

    .line 2330
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 2363
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 2333
    :cond_1
    :try_start_1
    new-instance v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v5, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    .line 2334
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/mapping/MapObject;

    .line 2335
    if-eqz v1, :cond_5

    .line 2336
    invoke-static {v1}, Lcom/nokia/maps/MapObjectImpl;->d(Lcom/here/android/mpa/mapping/MapObject;)Lcom/nokia/maps/MapObjectImpl;

    move-result-object v3

    .line 2337
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/nokia/maps/MapObjectImpl;->a(Lcom/nokia/maps/MapImpl;)V

    .line 2338
    instance-of v2, v3, Lcom/nokia/maps/MapMarkerImpl;

    if-eqz v2, :cond_3

    .line 2339
    move-object v0, v3

    check-cast v0, Lcom/nokia/maps/MapMarkerImpl;

    move-object v2, v0

    .line 2340
    invoke-direct {p0, v2}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/MapMarkerImpl;)V

    .line 2341
    invoke-virtual {v2}, Lcom/nokia/maps/MapMarkerImpl;->h()V

    move v2, v4

    .line 2352
    :goto_2
    iget-object v4, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v3}, Lcom/nokia/maps/MapObjectImpl;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2353
    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/MapObject;->getType()Lcom/here/android/mpa/mapping/MapObject$Type;

    move-result-object v3

    sget-object v4, Lcom/here/android/mpa/mapping/MapObject$Type;->CONTAINER:Lcom/here/android/mpa/mapping/MapObject$Type;

    if-ne v3, v4, :cond_2

    .line 2354
    iget-object v3, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    move v1, v2

    :goto_3
    move v4, v1

    .line 2359
    goto :goto_1

    .line 2342
    :cond_3
    instance-of v2, v3, Lcom/nokia/maps/MapContainerImpl;

    if-eqz v2, :cond_4

    .line 2345
    move-object v0, v3

    check-cast v0, Lcom/nokia/maps/MapContainerImpl;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/nokia/maps/MapContainerImpl;->c()V

    move v2, v4

    goto :goto_2

    .line 2346
    :cond_4
    instance-of v2, v3, Lcom/nokia/maps/cg;

    if-eqz v2, :cond_8

    .line 2347
    move-object v0, v3

    check-cast v0, Lcom/nokia/maps/cg;

    move-object v2, v0

    .line 2348
    invoke-virtual {v2}, Lcom/nokia/maps/cg;->e()[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/nokia/maps/MapImpl;->removeMapObjectsNative([Ljava/lang/Object;)Z

    move-result v4

    .line 2349
    invoke-direct {p0, v2}, Lcom/nokia/maps/MapImpl;->b(Lcom/nokia/maps/MapRouteImpl;)V

    .line 2350
    invoke-interface {v5, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v2, v4

    goto :goto_2

    .line 2357
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 2360
    :cond_6
    if-eqz v4, :cond_7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 2361
    invoke-static {v5}, Lcom/nokia/maps/MapObjectImpl;->a(Ljava/util/List;)[Lcom/nokia/maps/MapObjectImpl;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/nokia/maps/MapImpl;->removeMapObjectsNative([Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto/16 :goto_0

    .line 2329
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_7
    move v1, v4

    goto/16 :goto_0

    :cond_8
    move v2, v4

    goto :goto_2
.end method

.method public declared-synchronized d(Z)Z
    .locals 3

    .prologue
    .line 1723
    monitor-enter p0

    const/4 v0, 0x0

    .line 1725
    :try_start_0
    invoke-direct {p0, p1}, Lcom/nokia/maps/MapImpl;->setTrafficInfoVisibleNative(Z)I

    move-result v1

    if-nez v1, :cond_0

    .line 1726
    const/4 v0, 0x1

    .line 1727
    iput-boolean p1, p0, Lcom/nokia/maps/MapImpl;->U:Z

    .line 1728
    invoke-static {}, Lcom/nokia/maps/TrafficUpdaterImpl;->a()Lcom/nokia/maps/TrafficUpdaterImpl;

    move-result-object v1

    invoke-virtual {v1, p1, p0}, Lcom/nokia/maps/TrafficUpdaterImpl;->a(ZLcom/nokia/maps/MapImpl;)V

    .line 1730
    if-eqz p1, :cond_0

    .line 1731
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/nokia/maps/r;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1735
    :cond_0
    monitor-exit p0

    return v0

    .line 1723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method native destroyViewObjects()V
.end method

.method protected synchronized native declared-synchronized draw(ZZ)Z
.end method

.method public e(Landroid/graphics/PointF;)Lcom/here/android/mpa/common/ViewRect;
    .locals 5

    .prologue
    .line 2985
    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    .line 2986
    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    .line 2988
    new-instance v2, Lcom/here/android/mpa/common/ViewRect;

    iget v3, p0, Lcom/nokia/maps/MapImpl;->n:I

    add-int/lit8 v3, v3, -0x1

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/nokia/maps/MapImpl;->o:I

    add-int/lit8 v3, v3, -0x1

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v1, v3

    iget v3, p0, Lcom/nokia/maps/MapImpl;->n:I

    iget v4, p0, Lcom/nokia/maps/MapImpl;->o:I

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/here/android/mpa/common/ViewRect;-><init>(IIII)V

    .line 2991
    return-object v2
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 2026
    if-eqz p1, :cond_0

    .line 2027
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->P()V

    .line 2031
    :goto_0
    return-void

    .line 2029
    :cond_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 845
    sget-object v0, Lcom/here/android/mpa/common/IconCategory;->ALL:Lcom/here/android/mpa/common/IconCategory;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/IconCategory;)Z

    move-result v0

    return v0
.end method

.method public f(Z)V
    .locals 3

    .prologue
    .line 2034
    sget-object v0, Lcom/nokia/maps/MapImpl;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting m_limitRedraw to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/nokia/maps/bp;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2035
    iput-boolean p1, p0, Lcom/nokia/maps/MapImpl;->m:Z

    .line 2036
    return-void
.end method

.method public declared-synchronized f()Z
    .locals 1

    .prologue
    .line 1226
    monitor-enter p0

    .line 1227
    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1228
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1232
    :goto_0
    monitor-exit p0

    return v0

    .line 1230
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->isLandmarksVisibleNative()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->D:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/cluster/ClusterLayer;

    .line 484
    invoke-static {v0}, Lcom/nokia/maps/ag;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)Lcom/nokia/maps/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nokia/maps/ag;->b()V

    goto :goto_0

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->aq:Lcom/nokia/maps/fm;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->aq:Lcom/nokia/maps/fm;

    invoke-virtual {v0}, Lcom/nokia/maps/fm;->a()V

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ar:Lcom/nokia/maps/MapImpl$MapEventDispatcher;

    if-eqz v0, :cond_2

    .line 491
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ar:Lcom/nokia/maps/MapImpl$MapEventDispatcher;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl$MapEventDispatcher;->a()V

    .line 493
    :cond_2
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    if-eqz v0, :cond_3

    .line 494
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->j:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 496
    :cond_3
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 497
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 499
    :cond_4
    iget v0, p0, Lcom/nokia/maps/MapImpl;->nativeptr:I

    if-eqz v0, :cond_5

    .line 500
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->destroyMapNative()V

    .line 502
    :cond_5
    return-void
.end method

.method public synchronized native declared-synchronized freeGfxResources()V
.end method

.method public declared-synchronized g()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1529
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapSchemesNative()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1530
    invoke-direct {p0, v4}, Lcom/nokia/maps/MapImpl;->f(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-direct {p0, v4}, Lcom/nokia/maps/MapImpl;->g(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1531
    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1529
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1534
    :cond_2
    monitor-exit p0

    return-object v1

    .line 1527
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 2634
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->h(Z)V

    .line 2635
    return-void
.end method

.method synchronized native declared-synchronized getCenterNative()Lcom/nokia/maps/GeoCoordinateImpl;
.end method

.method public synchronized native declared-synchronized getClipRect()Lcom/here/android/mpa/common/ViewRect;
.end method

.method public final native getCopyright()Ljava/lang/String;
.end method

.method public synchronized native declared-synchronized getExtrudedBuildingsLayer()Lcom/nokia/maps/MapBuildingLayerImpl;
.end method

.method public synchronized native declared-synchronized getFadingAnimations()Z
.end method

.method public synchronized native declared-synchronized getFleetFeaturesVisible()I
.end method

.method public synchronized native declared-synchronized getLayerCategory()[I
.end method

.method public native getMapDisplayLanguage()Ljava/lang/String;
.end method

.method public synchronized native declared-synchronized getMapScheme()Ljava/lang/String;
.end method

.method public synchronized native declared-synchronized getMapSchemesNative()[Ljava/lang/String;
.end method

.method public native getMapSecondaryDisplayLanguage()Ljava/lang/String;
.end method

.method public synchronized native declared-synchronized getMapState()Lcom/here/android/mpa/mapping/MapState;
.end method

.method public native getMapTrafficLayerNative()Lcom/nokia/maps/MapTrafficLayerImpl;
.end method

.method public native getMapTransitLayerNative()Lcom/nokia/maps/MapTransitLayerImpl;
.end method

.method public synchronized native declared-synchronized getMaxTilt()F
.end method

.method public synchronized native declared-synchronized getMaxZoomLevel()D
.end method

.method public synchronized native declared-synchronized getMinTilt()F
.end method

.method public synchronized native declared-synchronized getMinZoomLevel()D
.end method

.method public synchronized native declared-synchronized getOrientation()F
.end method

.method public synchronized native declared-synchronized getPanoramaCoverageEnabled()Z
.end method

.method public native getPedestrianFeaturesVisible()I
.end method

.method public synchronized native declared-synchronized getPoiCategories()[Ljava/lang/String;
.end method

.method public native getSafetySpotsVisible()Z
.end method

.method public native getSupportedMapDisplayLanguagesNative()[Ljava/lang/String;
.end method

.method public synchronized native declared-synchronized getTilt()F
.end method

.method public synchronized native declared-synchronized getViewRect()Lcom/here/android/mpa/common/ViewRect;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public synchronized native declared-synchronized getZoomLevel()D
.end method

.method public native getZoomLevelToZoomScale(F)D
.end method

.method public native getZoomScaleToZoomLevel(D)F
.end method

.method public declared-synchronized h()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 1570
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getCenterNative()Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->create(Lcom/nokia/maps/GeoCoordinateImpl;)Lcom/here/android/mpa/common/GeoCoordinate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h(Z)V
    .locals 1

    .prologue
    .line 2648
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->setPanoramaCoverageEnabledNative(Z)V

    .line 2649
    if-eqz p1, :cond_2

    .line 2650
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    if-nez v0, :cond_0

    .line 2651
    new-instance v0, Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    invoke-direct {v0}, Lcom/nokia/maps/PanoramaCoverageRasterTileSource;-><init>()V

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    .line 2655
    :cond_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->getViewType()I

    move-result v0

    invoke-static {v0}, Lcom/nokia/maps/MapImpl;->c(I)Lcom/here/android/mpa/mapping/Map$Projection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$Projection;)V

    .line 2656
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2662
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2658
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    if-eqz v0, :cond_1

    .line 2659
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->h:Lcom/nokia/maps/PanoramaCoverageRasterTileSource;

    invoke-virtual {p0, v0}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2648
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized i()Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 1

    .prologue
    .line 1591
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->getBoundingBoxNative()Lcom/nokia/maps/GeoBoundingBoxImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/GeoBoundingBoxImpl;->create(Lcom/nokia/maps/GeoBoundingBoxImpl;)Lcom/here/android/mpa/common/GeoBoundingBox;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 2695
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ap:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/nokia/maps/MapImpl$11;

    invoke-direct {v1, p0, p1}, Lcom/nokia/maps/MapImpl$11;-><init>(Lcom/nokia/maps/MapImpl;Z)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 2701
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 2702
    return-void
.end method

.method public synchronized native declared-synchronized invalidate()V
.end method

.method public synchronized native declared-synchronized isExtrudedBuildingsVisible()Z
.end method

.method public native isRetainedLabelsEnabled()Z
.end method

.method public native isSubPixelLabelPositioningEnabled()Z
.end method

.method public j()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1608
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    .line 1609
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 1612
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1613
    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 1614
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1615
    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1616
    if-eqz v3, :cond_0

    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1617
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v3, v2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, v3, v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1628
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->setMapDisplayLanguageNative(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1629
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    move v0, v1

    .line 1632
    :goto_1
    return v0

    .line 1625
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1632
    goto :goto_1
.end method

.method public declared-synchronized j(Z)Z
    .locals 2

    .prologue
    .line 2721
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->isExtrudedBuildingsVisible()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, p1, :cond_1

    .line 2722
    const/4 v0, 0x0

    .line 2729
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 2725
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/nokia/maps/MapImpl;->setExtrudedBuildingsVisibleNative(Z)Z

    move-result v0

    .line 2726
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 2727
    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->B:Lcom/nokia/maps/r;

    invoke-interface {v1}, Lcom/nokia/maps/r;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2721
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized k()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1669
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1671
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getSupportedMapDisplayLanguagesNative()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1672
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1671
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1674
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1675
    monitor-exit p0

    return-object v1

    .line 1669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k(Z)V
    .locals 1

    .prologue
    .line 3355
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    if-eqz v0, :cond_0

    .line 3356
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    invoke-static {v0}, Lcom/nokia/maps/ec;->a(Lcom/here/android/mpa/mapping/PositionIndicator;)Lcom/nokia/maps/ec;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nokia/maps/ec;->d(Z)V

    .line 3358
    :cond_0
    return-void
.end method

.method public declared-synchronized l()Z
    .locals 1

    .prologue
    .line 1753
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/nokia/maps/MapImpl;->U:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lcom/here/android/mpa/mapping/PositionIndicator;
    .locals 2

    .prologue
    .line 2056
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    if-nez v0, :cond_0

    .line 2057
    new-instance v0, Lcom/nokia/maps/ec;

    iget-object v1, p0, Lcom/nokia/maps/MapImpl;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/nokia/maps/ec;-><init>(Landroid/content/Context;Lcom/nokia/maps/MapImpl;)V

    invoke-static {v0}, Lcom/nokia/maps/ec;->a(Lcom/nokia/maps/ec;)Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v0

    iput-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    .line 2060
    :cond_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->i:Lcom/here/android/mpa/mapping/PositionIndicator;

    return-object v0
.end method

.method public synchronized native declared-synchronized need_mapData()Z
.end method

.method public native need_redraw()Z
.end method

.method public declared-synchronized o()Lcom/here/android/mpa/mapping/Map$Projection;
    .locals 1

    .prologue
    .line 2628
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->getViewType()I

    move-result v0

    invoke-static {v0}, Lcom/nokia/maps/MapImpl;->c(I)Lcom/here/android/mpa/mapping/Map$Projection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 2638
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getPanoramaCoverageEnabled()Z

    move-result v0

    return v0
.end method

.method public q()Lcom/here/android/mpa/mapping/MapBuildingLayer;
    .locals 1

    .prologue
    .line 2747
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getExtrudedBuildingsLayer()Lcom/nokia/maps/MapBuildingLayerImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/MapBuildingLayerImpl;->a(Lcom/nokia/maps/MapBuildingLayerImpl;)Lcom/here/android/mpa/mapping/MapBuildingLayer;

    move-result-object v0

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 2875
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public redraw()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 2020
    iget-boolean v0, p0, Lcom/nokia/maps/MapImpl;->m:Z

    if-nez v0, :cond_0

    .line 2021
    invoke-direct {p0}, Lcom/nokia/maps/MapImpl;->P()V

    .line 2023
    :cond_0
    return-void
.end method

.method public synchronized native declared-synchronized removeRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z
.end method

.method public s()V
    .locals 2

    .prologue
    .line 2882
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2883
    return-void
.end method

.method native setAAEnabled(Z)V
.end method

.method public native setDistanceToPoleTuning(D)V
.end method

.method public native setExtrudedBuildingsVisibleNative(Z)Z
.end method

.method public synchronized native declared-synchronized setFadingAnimations(Z)V
.end method

.method public synchronized native declared-synchronized setLayerCategory([IZ)V
.end method

.method public synchronized native declared-synchronized setPanoramaCoverageEnabledNative(Z)V
.end method

.method public native setPedestrianFeaturesVisibleNative(I)V
.end method

.method public native setRenderingStatisticsVisible(Z)V
.end method

.method public native setRetainedLabelsEnabled(Z)V
.end method

.method public native setSafetySpotsVisible(Z)V
.end method

.method public native setSubPixelLabelPositioningEnabled(Z)V
.end method

.method public synchronized native declared-synchronized setZoomLevel(DIII)V
.end method

.method public declared-synchronized x()V
    .locals 2

    .prologue
    .line 3148
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapRasterTileSource;

    .line 3149
    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->c(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3151
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 3152
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3154
    :cond_1
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->ai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3155
    monitor-exit p0

    return-void
.end method

.method y()V
    .locals 2

    .prologue
    .line 3170
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->an:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3171
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/MapRasterTileSource;

    .line 3172
    invoke-static {v0}, Lcom/nokia/maps/MapRasterTileSourceImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Lcom/nokia/maps/MapRasterTileSourceImpl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nokia/maps/MapImpl;->addRasterTileSourceNative(Lcom/nokia/maps/MapRasterTileSourceImpl;)Z

    goto :goto_0

    .line 3174
    :cond_0
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->redraw()V

    .line 3175
    iget-object v0, p0, Lcom/nokia/maps/MapImpl;->an:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3177
    :cond_1
    return-void
.end method

.method public z()Lcom/here/android/mpa/mapping/MapTransitLayer;
    .locals 1

    .prologue
    .line 3187
    invoke-virtual {p0}, Lcom/nokia/maps/MapImpl;->getMapTransitLayerNative()Lcom/nokia/maps/MapTransitLayerImpl;

    move-result-object v0

    invoke-static {v0}, Lcom/nokia/maps/MapTransitLayerImpl;->a(Lcom/nokia/maps/MapTransitLayerImpl;)Lcom/here/android/mpa/mapping/MapTransitLayer;

    move-result-object v0

    return-object v0
.end method
