.class public final Lcom/here/android/mpa/mapping/Map;
.super Ljava/lang/Object;
.source "Map.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/here/android/mpa/mapping/Map$LayerCategory;,
        Lcom/here/android/mpa/mapping/Map$Projection;,
        Lcom/here/android/mpa/mapping/Map$PixelResult;,
        Lcom/here/android/mpa/mapping/Map$PedestrianFeature;,
        Lcom/here/android/mpa/mapping/Map$FleetFeature;,
        Lcom/here/android/mpa/mapping/Map$Scheme;,
        Lcom/here/android/mpa/mapping/Map$Animation;,
        Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;,
        Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;,
        Lcom/here/android/mpa/mapping/Map$OnTransformListener;
    }
.end annotation


# static fields
.field public static final MOVE_PRESERVE_ORIENTATION:F = -1.0f
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public static final MOVE_PRESERVE_TILT:F = -1.0f
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public static final MOVE_PRESERVE_ZOOM_LEVEL:D = -1.0
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field


# instance fields
.field private a:Lcom/nokia/maps/MapImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3173
    new-instance v0, Lcom/here/android/mpa/mapping/Map$1;

    invoke-direct {v0}, Lcom/here/android/mpa/mapping/Map$1;-><init>()V

    invoke-static {v0}, Lcom/nokia/maps/MapImpl;->a(Lcom/nokia/maps/m;)V

    .line 3181
    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 781
    new-instance v0, Lcom/nokia/maps/MapImpl;

    invoke-static {}, Lcom/nokia/maps/MapsEngine;->f()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nokia/maps/MapImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    .line 782
    return-void
.end method

.method static synthetic a(Lcom/here/android/mpa/mapping/Map;)Lcom/nokia/maps/MapImpl;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    return-object v0
.end method

.method public static enableMaximumFpsLimit(Z)V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3134
    invoke-static {p0}, Lcom/nokia/maps/bj;->a(Z)V

    .line 3135
    return-void
.end method

.method public static getMaximumFps()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3167
    invoke-static {}, Lcom/nokia/maps/bj;->b()I

    move-result v0

    return v0
.end method

.method public static isMaximumFpsLimited()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3144
    invoke-static {}, Lcom/nokia/maps/bj;->a()Z

    move-result v0

    return v0
.end method

.method public static setCustomMapConfiguration(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 797
    invoke-static {p0, p1}, Lcom/nokia/maps/MapImpl;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    return-void
.end method

.method public static setMaximumFps(I)V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3156
    invoke-static {p0}, Lcom/nokia/maps/bj;->a(I)V

    .line 3157
    return-void
.end method


# virtual methods
.method a(Lcom/here/android/mpa/mapping/MapObject;Z)Z
    .locals 1

    .prologue
    .line 2300
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapObject;Z)Z

    move-result v0

    return v0
.end method

.method public addClusterLayer(Lcom/here/android/mpa/cluster/ClusterLayer;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/cluster/ClusterLayer;)V

    .line 1576
    return-void
.end method

.method public addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1629
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapObject;)Z

    move-result v0

    return v0
.end method

.method public addMapObjects(Ljava/util/List;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->c(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public addMapOverlay(Lcom/here/android/mpa/mapping/MapOverlay;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1600
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapOverlay;)Z

    move-result v0

    return v0
.end method

.method public addRasterTileSource(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1873
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z

    move-result v0

    return v0
.end method

.method public addSchemeChangedListener(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1795
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V

    .line 1796
    return-void
.end method

.method public addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 1773
    return-void
.end method

.method public areCartoMarkersVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->e()Z

    move-result v0

    return v0
.end method

.method public areCartoMarkersVisible(Lcom/here/android/mpa/common/IconCategory;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2221
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/IconCategory;)Z

    move-result v0

    return v0
.end method

.method public areExtrudedBuildingsVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1859
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->isExtrudedBuildingsVisible()Z

    move-result v0

    return v0
.end method

.method public areLandmarksVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->f()Z

    move-result v0

    return v0
.end method

.method public areSafetySpotsVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2329
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getSafetySpotsVisible()Z

    move-result v0

    return v0
.end method

.method public createCustomizableScheme(Ljava/lang/String;Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2103
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;

    move-result-object v0

    return-object v0
.end method

.method public executeSynchronized(Ljava/lang/Runnable;)V
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2291
    iget-object v1, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    monitor-enter v1

    .line 2292
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 2293
    monitor-exit v1

    .line 2294
    return-void

    .line 2293
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->i()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getCenter()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1414
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->h()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public getCopyright()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1425
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getCopyright()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomizableScheme(Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2139
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->d(Ljava/lang/String;)Lcom/here/android/mpa/mapping/customization/CustomizableScheme;

    move-result-object v0

    return-object v0
.end method

.method public getFleetFeaturesVisible()Ljava/util/EnumSet;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$FleetFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2253
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getFleetFeaturesVisible()I

    move-result v0

    invoke-static {v0}, Lcom/here/android/mpa/mapping/Map$FleetFeature;->a(I)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 817
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->c()I

    move-result v0

    return v0
.end method

.method public getMapBuildingLayer()Lcom/here/android/mpa/mapping/MapBuildingLayer;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->q()Lcom/here/android/mpa/mapping/MapBuildingLayer;

    move-result-object v0

    return-object v0
.end method

.method public getMapDisplayLanguage()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMapDisplayLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMapScheme()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMapScheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMapSchemes()Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1354
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMapSecondaryDisplayLanguage()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMapSecondaryDisplayLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMapState()Lcom/here/android/mpa/mapping/MapState;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMapState()Lcom/here/android/mpa/mapping/MapState;

    move-result-object v0

    return-object v0
.end method

.method public getMapTrafficLayer()Lcom/here/android/mpa/mapping/MapTrafficLayer;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2077
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->A()Lcom/here/android/mpa/mapping/MapTrafficLayer;

    move-result-object v0

    return-object v0
.end method

.method public getMapTransitLayer()Lcom/here/android/mpa/mapping/MapTransitLayer;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->z()Lcom/here/android/mpa/mapping/MapTransitLayer;

    move-result-object v0

    return-object v0
.end method

.method public getMaxTilt()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMaxTilt()F

    move-result v0

    return v0
.end method

.method public getMaxZoomLevel()D
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMaxZoomLevel()D

    move-result-wide v0

    return-wide v0
.end method

.method public getMinTilt()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1291
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMinTilt()F

    move-result v0

    return v0
.end method

.method public getMinZoomLevel()D
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getMinZoomLevel()D

    move-result-wide v0

    return-wide v0
.end method

.method public getOrientation()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getOrientation()F

    move-result v0

    return v0
.end method

.method public getPedestrianFeaturesVisible()Ljava/util/EnumSet;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$PedestrianFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2279
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getPedestrianFeaturesVisible()I

    move-result v0

    invoke-static {v0}, Lcom/here/android/mpa/mapping/Map$PedestrianFeature;->a(I)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1561
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->n()Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v0

    return-object v0
.end method

.method public getProjectionMode()Lcom/here/android/mpa/mapping/Map$Projection;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2189
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->o()Lcom/here/android/mpa/mapping/Map$Projection;

    move-result-object v0

    return-object v0
.end method

.method public getScaleFromZoomLevel(D)D
    .locals 3
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->b(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedObjects(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->d(Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedObjects(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/ViewRect;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedObjectsNearby(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/ViewObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->c(Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedMapDisplayLanguages()Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTilt()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getTilt()F

    move-result v0

    return v0
.end method

.method public getTransformCenter()Landroid/graphics/PointF;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 933
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->d()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleLayers()Ljava/util/EnumSet;
    .locals 5
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$LayerCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3113
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getLayerCategory()[I

    move-result-object v1

    .line 3115
    const-class v0, Lcom/here/android/mpa/mapping/Map$LayerCategory;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 3116
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v1, v0

    .line 3117
    invoke-static {v4}, Lcom/here/android/mpa/mapping/Map$LayerCategory;->a(I)Lcom/here/android/mpa/mapping/Map$LayerCategory;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 3116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3119
    :cond_0
    return-object v2
.end method

.method public getWidth()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 807
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->b()I

    move-result v0

    return v0
.end method

.method public getZoomLevel()D
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->getZoomLevel()D

    move-result-wide v0

    return-wide v0
.end method

.method public isStreetLevelCoverageVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->p()Z

    move-result v0

    return v0
.end method

.method public isTrafficInfoVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1920
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->l()Z

    move-result v0

    return v0
.end method

.method public pan(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 1069
    return-void
.end method

.method public pixelToGeo(Landroid/graphics/PointF;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1445
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Landroid/graphics/PointF;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public pixelToGeo(Landroid/graphics/PointF;F)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Landroid/graphics/PointF;F)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public pixelToGeo(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public projectToPixel(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;

    move-result-object v0

    return-object v0
.end method

.method public projectToPixel(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/Map$PixelResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public removeClusterLayer(Lcom/here/android/mpa/cluster/ClusterLayer;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/cluster/ClusterLayer;)V

    .line 1588
    return-void
.end method

.method public removeCustomizableScheme(Ljava/lang/String;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2122
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/MapObject;)Z

    move-result v0

    return v0
.end method

.method public removeMapObjects(Ljava/util/List;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1674
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->d(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public removeMapOverlay(Lcom/here/android/mpa/mapping/MapOverlay;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1612
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/MapOverlay;)Z

    move-result v0

    return v0
.end method

.method public removeRasterTileSource(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1887
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/MapRasterTileSource;)Z

    move-result v0

    return v0
.end method

.method public removeSchemeChangedListener(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1806
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$OnSchemeChangedListener;)V

    .line 1807
    return-void
.end method

.method public removeTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 1784
    return-void
.end method

.method public setCartoMarkersVisible(Lcom/here/android/mpa/common/IconCategory;Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2206
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/IconCategory;Z)Z

    .line 2207
    return-object p0
.end method

.method public setCartoMarkersVisible(Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Z)V

    .line 1743
    return-object p0
.end method

.method public setCenter(Landroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 9
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 907
    iget-object v1, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(Landroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 908
    return-void
.end method

.method public setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 879
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 880
    return-void
.end method

.method public setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 9
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 864
    iget-object v1, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 865
    return-void
.end method

.method public setExtrudedBuildingsVisible(Z)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1849
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->j(Z)Z

    move-result v0

    return v0
.end method

.method public setFadingAnimations(Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1995
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->i(Z)V

    .line 1996
    return-object p0
.end method

.method public setFleetFeaturesVisible(Ljava/util/EnumSet;)Lcom/here/android/mpa/mapping/Map;
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$FleetFeature;",
            ">;)",
            "Lcom/here/android/mpa/mapping/Map;"
        }
    .end annotation

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-static {p1}, Lcom/here/android/mpa/mapping/Map$FleetFeature;->a(Ljava/util/EnumSet;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapImpl;->a(I)V

    .line 2241
    return-object p0
.end method

.method public setInfoBubbleAdapter(Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1821
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/Map$InfoBubbleAdapter;)V

    .line 1822
    return-void
.end method

.method public setLandmarksVisible(Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->c(Z)V

    .line 1952
    return-object p0
.end method

.method public setMapDisplayLanguage(Ljava/util/Locale;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public setMapScheme(Lcom/here/android/mpa/mapping/customization/CustomizableScheme;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/customization/CustomizableScheme;)V

    .line 1404
    return-object p0
.end method

.method public setMapScheme(Ljava/lang/String;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Ljava/lang/String;)V

    .line 1385
    return-object p0
.end method

.method public setMapSecondaryDisplayLanguage(Ljava/util/Locale;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public setOrientation(F)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(F)V

    .line 1252
    return-object p0
.end method

.method public setOrientation(FLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(FLcom/here/android/mpa/mapping/Map$Animation;)V

    .line 1272
    return-void
.end method

.method public setPedestrianFeaturesVisible(Ljava/util/EnumSet;)Lcom/here/android/mpa/mapping/Map;
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$PedestrianFeature;",
            ">;)",
            "Lcom/here/android/mpa/mapping/Map;"
        }
    .end annotation

    .prologue
    .line 2266
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-static {p1}, Lcom/here/android/mpa/mapping/Map$PedestrianFeature;->a(Ljava/util/EnumSet;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/nokia/maps/MapImpl;->b(I)V

    .line 2267
    return-object p0
.end method

.method public setProjectionMode(Lcom/here/android/mpa/mapping/Map$Projection;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2178
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/mapping/Map$Projection;)V

    .line 2179
    return-object p0
.end method

.method public setSafetySpotsVisible(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2317
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->setSafetySpotsVisible(Z)V

    .line 2318
    return-void
.end method

.method public setStreetLevelCoverageVisible(Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1969
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->g(Z)V

    .line 1970
    return-object p0
.end method

.method public setTilt(F)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->b(F)V

    .line 1318
    return-object p0
.end method

.method public setTilt(FLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1341
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->b(FLcom/here/android/mpa/mapping/Map$Animation;)V

    .line 1342
    return-void
.end method

.method public setTrafficInfoVisible(Z)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->d(Z)Z

    .line 1907
    return-object p0
.end method

.method public setTransformCenter(Landroid/graphics/PointF;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 921
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapImpl;->a(Landroid/graphics/PointF;)V

    .line 922
    return-object p0
.end method

.method public setUseSystemLanguage()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapImpl;->j()Z

    move-result v0

    return v0
.end method

.method public setVisibleLayers(Ljava/util/EnumSet;Z)Lcom/here/android/mpa/mapping/Map;
    .locals 4
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/mapping/Map$LayerCategory;",
            ">;Z)",
            "Lcom/here/android/mpa/mapping/Map;"
        }
    .end annotation

    .prologue
    .line 3092
    invoke-virtual {p1}, Ljava/util/EnumSet;->size()I

    move-result v0

    .line 3093
    new-array v2, v0, [I

    .line 3094
    const/4 v0, 0x0

    .line 3095
    invoke-virtual {p1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/mapping/Map$LayerCategory;

    .line 3096
    iget v0, v0, Lcom/here/android/mpa/mapping/Map$LayerCategory;->a:I

    aput v0, v2, v1

    .line 3097
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 3098
    goto :goto_0

    .line 3100
    :cond_0
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, v2, p2}, Lcom/nokia/maps/MapImpl;->a([IZ)V

    .line 3101
    return-object p0
.end method

.method public setZoomLevel(D)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/MapImpl;->a(D)V

    .line 1131
    return-object p0
.end method

.method public setZoomLevel(DLandroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->a(DLandroid/graphics/PointF;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 1201
    return-void
.end method

.method public setZoomLevel(DLcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/MapImpl;->a(DLcom/here/android/mpa/mapping/Map$Animation;)V

    .line 1163
    return-void
.end method

.method public zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;IILcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 6
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoBoundingBox;IILcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 1056
    return-void
.end method

.method public zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 1018
    return-void
.end method

.method public zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 958
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 959
    return-void
.end method

.method public zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;FF)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 986
    iget-object v0, p0, Lcom/here/android/mpa/mapping/Map;->a:Lcom/nokia/maps/MapImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/nokia/maps/MapImpl;->a(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;FF)V

    .line 987
    return-void
.end method
