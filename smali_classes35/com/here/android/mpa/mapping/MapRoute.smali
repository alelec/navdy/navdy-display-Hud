.class public final Lcom/here/android/mpa/mapping/MapRoute;
.super Lcom/here/android/mpa/mapping/MapObject;
.source "MapRoute.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/here/android/mpa/mapping/MapRoute$RenderType;
    }
.end annotation


# instance fields
.field private a:Lcom/nokia/maps/MapRouteImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 316
    new-instance v0, Lcom/here/android/mpa/mapping/MapRoute$1;

    invoke-direct {v0}, Lcom/here/android/mpa/mapping/MapRoute$1;-><init>()V

    invoke-static {v0}, Lcom/nokia/maps/MapRouteImpl;->b(Lcom/nokia/maps/m;)V

    .line 323
    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lcom/nokia/maps/MapRouteImpl;

    invoke-direct {v0}, Lcom/nokia/maps/MapRouteImpl;-><init>()V

    invoke-direct {p0, v0}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/nokia/maps/MapRouteImpl;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/here/android/mpa/routing/Route;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 92
    invoke-static {p1}, Lcom/nokia/maps/ce;->a(Lcom/here/android/mpa/routing/Route;)Lcom/nokia/maps/MapRouteImpl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/nokia/maps/MapRouteImpl;)V

    .line 93
    invoke-virtual {p0, p1}, Lcom/here/android/mpa/mapping/MapRoute;->setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/mapping/MapRoute;

    .line 94
    return-void
.end method

.method constructor <init>(Lcom/nokia/maps/MapRouteImpl;)V
    .locals 0
    .annotation build Lcom/nokia/maps/annotation/HybridPlusNative;
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/here/android/mpa/mapping/MapObject;-><init>(Lcom/nokia/maps/MapObjectImpl;)V

    .line 100
    iput-object p1, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/here/android/mpa/mapping/MapRoute;)Lcom/nokia/maps/MapRouteImpl;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    return-object v0
.end method


# virtual methods
.method public getColor()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->getColor()I

    move-result v0

    return v0
.end method

.method public getRenderType()Lcom/here/android/mpa/mapping/MapRoute$RenderType;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->b()Lcom/here/android/mpa/mapping/MapRoute$RenderType;

    move-result-object v0

    return-object v0
.end method

.method public getRoute()Lcom/here/android/mpa/routing/Route;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->a()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleMask()Ljava/util/BitSet;
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    return-object v0
.end method

.method public isManeuverNumberVisible()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->isManeuverNumberVisible()Z

    move-result v0

    return v0
.end method

.method public isTrafficEnabled()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0}, Lcom/nokia/maps/MapRouteImpl;->isTrafficEnabled()Z

    move-result v0

    return v0
.end method

.method public resetVisibleMask(Z)Lcom/here/android/mpa/mapping/MapObject;
    .locals 0

    .prologue
    .line 290
    return-object p0
.end method

.method public setColor(I)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapRouteImpl;->a(I)V

    .line 163
    return-object p0
.end method

.method public setManeuverNumberVisible(Z)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapRouteImpl;->setManeuverNumberVisible(Z)V

    .line 125
    return-object p0
.end method

.method public setRenderType(Lcom/here/android/mpa/mapping/MapRoute$RenderType;)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapRouteImpl;->a(Lcom/here/android/mpa/mapping/MapRoute$RenderType;)V

    .line 208
    return-object p0
.end method

.method public setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapRouteImpl;->a(Lcom/here/android/mpa/routing/Route;)V

    .line 186
    return-object p0
.end method

.method public setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/here/android/mpa/mapping/MapRoute;->a:Lcom/nokia/maps/MapRouteImpl;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/MapRouteImpl;->enableTraffic(Z)V

    .line 239
    return-object p0
.end method

.method public setVisibleMask(I)Lcom/here/android/mpa/mapping/MapObject;
    .locals 0

    .prologue
    .line 261
    return-object p0
.end method

.method public setVisibleMask(II)Lcom/here/android/mpa/mapping/MapObject;
    .locals 0

    .prologue
    .line 281
    return-object p0
.end method

.method public unsetVisibleMask(I)Lcom/here/android/mpa/mapping/MapObject;
    .locals 0

    .prologue
    .line 271
    return-object p0
.end method

.method public unsetVisibleMask(II)Lcom/here/android/mpa/mapping/MapObject;
    .locals 0

    .prologue
    .line 300
    return-object p0
.end method
