.class public Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;
.super Ljava/lang/Object;
.source "CustomizableVariables.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/mapping/customization/CustomizableVariables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "River"
.end annotation


# static fields
.field public static final COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS1_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS1_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS2_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS2_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS3_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS3_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS4_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS4_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS5_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS5_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS6_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS6_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS7_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS7_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final DISPLAYCLASS8_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final DISPLAYCLASS8_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

.field public static final WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 434
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 435
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass1.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS1_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 436
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass2.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS2_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 437
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass3.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS3_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 438
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass4.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS4_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 439
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass5.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS5_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 440
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass6.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS6_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 441
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass7.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS7_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 442
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "River.DisplayClass8.Color"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS8_COLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 443
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass1.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS1_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 444
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass2.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS2_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 445
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass3.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS3_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 446
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass4.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS4_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 447
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass5.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS5_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 448
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass6.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS6_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 449
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass7.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS7_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 450
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.DisplayClass8.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->DISPLAYCLASS8_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    .line 451
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    const-string v1, "River.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$River;->WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeFloatProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
