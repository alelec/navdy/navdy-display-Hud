.class public Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;
.super Ljava/lang/Object;
.source "CustomizableVariables.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/mapping/customization/CustomizableVariables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Transit"
.end annotation


# static fields
.field public static final AERIAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final AERIAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final BACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BLENDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BUS_EXPRESS_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BUS_EXPRESS_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final BUS_INTERCITY_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BUS_INTERCITY_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final BUS_PUBLIC_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BUS_PUBLIC_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final BUS_TOURISTIC_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final BUS_TOURISTIC_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final HIGHLIGHTEDBACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final HIGHLIGHTEDFONTOUTLINECOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final INCLINED_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final INCLINED_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final MONORAIL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final MONORAIL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final OUTLINEWIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final RAIL_CITYMETRO_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final RAIL_CITYMETRO_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final RAIL_LIGHT_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final RAIL_LIGHT_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final RAIL_REGIONAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final RAIL_REGIONAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final TRAIN_HIGHSPEED_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final TRAIN_HIGHSPEED_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final TRAIN_INTERCITY_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final TRAIN_INTERCITY_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final TRAIN_REGIONAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final TRAIN_REGIONAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

.field public static final WATER_BACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final WATER_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final WATER_HIGHLIGHTEDBACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final WATER_HIGHLIGHTEDFONTOUTLINECOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

.field public static final WATER_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 693
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Aerial.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->AERIAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 694
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.BackgroundColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 695
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.BlendColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BLENDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 696
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Bus.Express.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_EXPRESS_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 697
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Bus.Intercity.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_INTERCITY_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 698
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Bus.Public.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_PUBLIC_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 699
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Bus.Touristic.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_TOURISTIC_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 700
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.HighlightedBackgroundColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->HIGHLIGHTEDBACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 701
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.HighlightedFontOutlineColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->HIGHLIGHTEDFONTOUTLINECOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 702
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Inclined.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->INCLINED_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 703
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Monorail.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->MONORAIL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 704
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Rail.CityMetro.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_CITYMETRO_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 705
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Rail.Light.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_LIGHT_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 706
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Rail.Regional.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_REGIONAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 707
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Train.HighSpeed.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_HIGHSPEED_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 708
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Train.Intercity.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_INTERCITY_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 709
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Train.Regional.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_REGIONAL_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 710
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Water.BackgroundColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->WATER_BACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 711
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Water.DefaultColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->WATER_DEFAULTCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 712
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Water.HighlightedBackgroundColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->WATER_HIGHLIGHTEDBACKGROUNDCOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 713
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    const-string v1, "Transit.Water.HighlightedFontOutlineColor"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->WATER_HIGHLIGHTEDFONTOUTLINECOLOR:Lcom/here/android/mpa/mapping/customization/SchemeColorProperty;

    .line 714
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Aerial.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->AERIAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 715
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Bus.Express.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_EXPRESS_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 716
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Bus.Intercity.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_INTERCITY_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 717
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Bus.Public.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_PUBLIC_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 718
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Bus.Touristic.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->BUS_TOURISTIC_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 719
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Inclined.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->INCLINED_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 720
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Monorail.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->MONORAIL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 721
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.OutlineWidth"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->OUTLINEWIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 722
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Rail.CityMetro.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_CITYMETRO_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 723
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Rail.Light.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_LIGHT_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 724
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Rail.Regional.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->RAIL_REGIONAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 725
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Train.HighSpeed.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_HIGHSPEED_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 726
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Train.Intercity.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_INTERCITY_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 727
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Train.Regional.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->TRAIN_REGIONAL_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    .line 728
    new-instance v0, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    const-string v1, "Transit.Water.Width"

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/here/android/mpa/mapping/customization/CustomizableVariables$Transit;->WATER_WIDTH:Lcom/here/android/mpa/mapping/customization/SchemeIntegerProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
