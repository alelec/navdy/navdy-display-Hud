.class public final enum Lcom/here/android/mpa/common/CustomConfigurations$Config;
.super Ljava/lang/Enum;
.source "CustomConfigurations.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/common/CustomConfigurations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/here/android/mpa/common/CustomConfigurations$Config;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum MAP:Lcom/here/android/mpa/common/CustomConfigurations$Config;

.field public static final enum MAP_RESOURCE:Lcom/here/android/mpa/common/CustomConfigurations$Config;

.field public static final enum TRAFFIC:Lcom/here/android/mpa/common/CustomConfigurations$Config;

.field private static final synthetic a:[Lcom/here/android/mpa/common/CustomConfigurations$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v2}, Lcom/here/android/mpa/common/CustomConfigurations$Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    .line 37
    new-instance v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;

    const-string v1, "MAP_RESOURCE"

    invoke-direct {v0, v1, v3}, Lcom/here/android/mpa/common/CustomConfigurations$Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP_RESOURCE:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    .line 41
    new-instance v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;

    const-string v1, "TRAFFIC"

    invoke-direct {v0, v1, v4}, Lcom/here/android/mpa/common/CustomConfigurations$Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->TRAFFIC:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/here/android/mpa/common/CustomConfigurations$Config;

    sget-object v1, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    aput-object v1, v0, v2

    sget-object v1, Lcom/here/android/mpa/common/CustomConfigurations$Config;->MAP_RESOURCE:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    aput-object v1, v0, v3

    sget-object v1, Lcom/here/android/mpa/common/CustomConfigurations$Config;->TRAFFIC:Lcom/here/android/mpa/common/CustomConfigurations$Config;

    aput-object v1, v0, v4

    sput-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->a:[Lcom/here/android/mpa/common/CustomConfigurations$Config;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/here/android/mpa/common/CustomConfigurations$Config;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;

    return-object v0
.end method

.method public static values()[Lcom/here/android/mpa/common/CustomConfigurations$Config;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/here/android/mpa/common/CustomConfigurations$Config;->a:[Lcom/here/android/mpa/common/CustomConfigurations$Config;

    invoke-virtual {v0}, [Lcom/here/android/mpa/common/CustomConfigurations$Config;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/here/android/mpa/common/CustomConfigurations$Config;

    return-object v0
.end method
