.class public final enum Lcom/here/android/mpa/routing/Maneuver$Icon;
.super Ljava/lang/Enum;
.source "Maneuver.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/routing/Maneuver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Icon"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/here/android/mpa/routing/Maneuver$Icon;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CHANGE_LINE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum END:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ENTER_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ENTER_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum FERRY:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum GO_STRAIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum HEAD_TO:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum HIGHWAY_KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum HIGHWAY_KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum LEAVE_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum LEAVE_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum PASS_STATION:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_10_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_11_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_12_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_1_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_2_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_3_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_4_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_5_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_6_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_7_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_8_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum ROUNDABOUT_9_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum START:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum UTURN_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field public static final enum UTURN_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

.field private static final synthetic b:[Lcom/here/android/mpa/routing/Maneuver$Icon;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 433
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4, v4}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 435
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "GO_STRAIGHT"

    invoke-direct {v0, v1, v5, v5}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->GO_STRAIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 437
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "UTURN_RIGHT"

    invoke-direct {v0, v1, v6, v6}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 439
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "UTURN_LEFT"

    invoke-direct {v0, v1, v7, v7}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 441
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "KEEP_RIGHT"

    invoke-direct {v0, v1, v8, v8}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 443
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "LIGHT_RIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 445
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "QUITE_RIGHT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 447
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "HEAVY_RIGHT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 449
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "KEEP_MIDDLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 451
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "KEEP_LEFT"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 453
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "LIGHT_LEFT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 455
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "QUITE_LEFT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 457
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "HEAVY_LEFT"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 459
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ENTER_HIGHWAY_RIGHT_LANE"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 461
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ENTER_HIGHWAY_LEFT_LANE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 463
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "LEAVE_HIGHWAY_RIGHT_LANE"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 465
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "LEAVE_HIGHWAY_LEFT_LANE"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 467
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "HIGHWAY_KEEP_RIGHT"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 469
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "HIGHWAY_KEEP_LEFT"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 475
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_1"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 480
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_2"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 485
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_3"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 490
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_4"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 495
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_5"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 500
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_6"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 505
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_7"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 510
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_8"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 515
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_9"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 520
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_10"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 525
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_11"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 530
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_12"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 536
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_1_LH"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 541
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_2_LH"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 546
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_3_LH"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 551
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_4_LH"

    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 556
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_5_LH"

    const/16 v2, 0x23

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 561
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_6_LH"

    const/16 v2, 0x24

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 566
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_7_LH"

    const/16 v2, 0x25

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 571
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_8_LH"

    const/16 v2, 0x26

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 576
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_9_LH"

    const/16 v2, 0x27

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 581
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_10_LH"

    const/16 v2, 0x28

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 586
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_11_LH"

    const/16 v2, 0x29

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 591
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "ROUNDABOUT_12_LH"

    const/16 v2, 0x2a

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 596
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "START"

    const/16 v2, 0x2b

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->START:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 598
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "END"

    const/16 v2, 0x2c

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->END:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 600
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "FERRY"

    const/16 v2, 0x2d

    const/16 v3, 0x2d

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->FERRY:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 602
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "PASS_STATION"

    const/16 v2, 0x2e

    const/16 v3, 0x2e

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->PASS_STATION:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 604
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "HEAD_TO"

    const/16 v2, 0x2f

    const/16 v3, 0x2f

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAD_TO:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 606
    new-instance v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    const-string v1, "CHANGE_LINE"

    const/16 v2, 0x30

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/here/android/mpa/routing/Maneuver$Icon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->CHANGE_LINE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    .line 430
    const/16 v0, 0x31

    new-array v0, v0, [Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v1, v0, v4

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->GO_STRAIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v1, v0, v5

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v1, v0, v6

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v1, v0, v7

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->START:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->END:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->FERRY:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->PASS_STATION:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAD_TO:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->CHANGE_LINE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    aput-object v2, v0, v1

    sput-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->b:[Lcom/here/android/mpa/routing/Maneuver$Icon;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 611
    iput p3, p0, Lcom/here/android/mpa/routing/Maneuver$Icon;->a:I

    .line 612
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/here/android/mpa/routing/Maneuver$Icon;
    .locals 1

    .prologue
    .line 430
    const-class v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/routing/Maneuver$Icon;

    return-object v0
.end method

.method public static values()[Lcom/here/android/mpa/routing/Maneuver$Icon;
    .locals 1

    .prologue
    .line 430
    sget-object v0, Lcom/here/android/mpa/routing/Maneuver$Icon;->b:[Lcom/here/android/mpa/routing/Maneuver$Icon;

    invoke-virtual {v0}, [Lcom/here/android/mpa/routing/Maneuver$Icon;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/here/android/mpa/routing/Maneuver$Icon;

    return-object v0
.end method


# virtual methods
.method public value()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lcom/here/android/mpa/routing/Maneuver$Icon;->a:I

    return v0
.end method
