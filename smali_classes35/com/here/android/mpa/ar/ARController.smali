.class public final Lcom/here/android/mpa/ar/ARController;
.super Ljava/lang/Object;
.source "ARController.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;,
        Lcom/here/android/mpa/ar/ARController$OnPitchFunction;,
        Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;,
        Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;,
        Lcom/here/android/mpa/ar/ARController$OnPoseListener;,
        Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;,
        Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;,
        Lcom/here/android/mpa/ar/ARController$OnTapListener;,
        Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;,
        Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;,
        Lcom/here/android/mpa/ar/ARController$OnPanListener;,
        Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;,
        Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;,
        Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;,
        Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;,
        Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;,
        Lcom/here/android/mpa/ar/ARController$b;,
        Lcom/here/android/mpa/ar/ARController$a;,
        Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;,
        Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;,
        Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;,
        Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;,
        Lcom/here/android/mpa/ar/ARController$ExternalSensors;,
        Lcom/here/android/mpa/ar/ARController$SelectedItemParams;,
        Lcom/here/android/mpa/ar/ARController$InfoParams;,
        Lcom/here/android/mpa/ar/ARController$IconParams;,
        Lcom/here/android/mpa/ar/ARController$UpViewTransitionParams;,
        Lcom/here/android/mpa/ar/ARController$UpViewParams;,
        Lcom/here/android/mpa/ar/ARController$DownViewParams;,
        Lcom/here/android/mpa/ar/ARController$CameraParams;,
        Lcom/here/android/mpa/ar/ARController$FilterParams;,
        Lcom/here/android/mpa/ar/ARController$IntroAnimationParams;,
        Lcom/here/android/mpa/ar/ARController$IntroAnimationMode;,
        Lcom/here/android/mpa/ar/ARController$SensorType;,
        Lcom/here/android/mpa/ar/ARController$ProjectionType;,
        Lcom/here/android/mpa/ar/ARController$c;,
        Lcom/here/android/mpa/ar/ARController$ViewType;,
        Lcom/here/android/mpa/ar/ARController$Error;
    }
.end annotation


# static fields
.field public static final CameraParams:Lcom/here/android/mpa/ar/ARController$CameraParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field


# instance fields
.field public final DownViewParams:Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final ExternalSensors:Lcom/here/android/mpa/ar/ARController$ExternalSensors;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final HeadingFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final IconParams:Lcom/here/android/mpa/ar/ARController$IconParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final InfoParams:Lcom/here/android/mpa/ar/ARController$InfoParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final IntroParams:Lcom/here/android/mpa/ar/ARController$IntroAnimationParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final PitchFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final SelectedItemParams:Lcom/here/android/mpa/ar/ARController$SelectedItemParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final UpViewParams:Lcom/here/android/mpa/ar/ARController$UpViewParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final UpViewTransitionParams:Lcom/here/android/mpa/ar/ARController$UpViewTransitionParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field public final ZoomFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation
.end field

.field protected a:Lcom/nokia/maps/d;
    .annotation build Lcom/nokia/maps/annotation/Internal;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/here/android/mpa/ar/ARController$CameraParams;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/here/android/mpa/ar/ARController$CameraParams;-><init>(Lcom/here/android/mpa/ar/ARController$1;)V

    sput-object v0, Lcom/here/android/mpa/ar/ARController;->CameraParams:Lcom/here/android/mpa/ar/ARController$CameraParams;

    .line 2448
    new-instance v0, Lcom/here/android/mpa/ar/ARController$1;

    invoke-direct {v0}, Lcom/here/android/mpa/ar/ARController$1;-><init>()V

    new-instance v1, Lcom/here/android/mpa/ar/ARController$2;

    invoke-direct {v1}, Lcom/here/android/mpa/ar/ARController$2;-><init>()V

    invoke-static {v0, v1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/m;Lcom/nokia/maps/ar;)V

    .line 2464
    return-void
.end method

.method private constructor <init>(Lcom/nokia/maps/d;)V
    .locals 3
    .annotation build Lcom/nokia/maps/annotation/Internal;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/here/android/mpa/ar/ARController$IntroAnimationParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$IntroAnimationParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->IntroParams:Lcom/here/android/mpa/ar/ARController$IntroAnimationParams;

    .line 63
    new-instance v0, Lcom/here/android/mpa/ar/ARController$FilterParams;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/here/android/mpa/ar/ARController$FilterParams;-><init>(Lcom/here/android/mpa/ar/ARController;ILcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->HeadingFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;

    .line 71
    new-instance v0, Lcom/here/android/mpa/ar/ARController$FilterParams;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/here/android/mpa/ar/ARController$FilterParams;-><init>(Lcom/here/android/mpa/ar/ARController;ILcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->PitchFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;

    .line 79
    new-instance v0, Lcom/here/android/mpa/ar/ARController$FilterParams;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1, v2}, Lcom/here/android/mpa/ar/ARController$FilterParams;-><init>(Lcom/here/android/mpa/ar/ARController;ILcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->ZoomFilterParams:Lcom/here/android/mpa/ar/ARController$FilterParams;

    .line 87
    new-instance v0, Lcom/here/android/mpa/ar/ARController$DownViewParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$DownViewParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->DownViewParams:Lcom/here/android/mpa/ar/ARController$DownViewParams;

    .line 95
    new-instance v0, Lcom/here/android/mpa/ar/ARController$UpViewParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$UpViewParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->UpViewParams:Lcom/here/android/mpa/ar/ARController$UpViewParams;

    .line 103
    new-instance v0, Lcom/here/android/mpa/ar/ARController$UpViewTransitionParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$UpViewTransitionParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->UpViewTransitionParams:Lcom/here/android/mpa/ar/ARController$UpViewTransitionParams;

    .line 111
    new-instance v0, Lcom/here/android/mpa/ar/ARController$IconParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$IconParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->IconParams:Lcom/here/android/mpa/ar/ARController$IconParams;

    .line 119
    new-instance v0, Lcom/here/android/mpa/ar/ARController$InfoParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$InfoParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->InfoParams:Lcom/here/android/mpa/ar/ARController$InfoParams;

    .line 127
    new-instance v0, Lcom/here/android/mpa/ar/ARController$SelectedItemParams;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$SelectedItemParams;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->SelectedItemParams:Lcom/here/android/mpa/ar/ARController$SelectedItemParams;

    .line 135
    new-instance v0, Lcom/here/android/mpa/ar/ARController$ExternalSensors;

    invoke-direct {v0, p0, v2}, Lcom/here/android/mpa/ar/ARController$ExternalSensors;-><init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V

    iput-object v0, p0, Lcom/here/android/mpa/ar/ARController;->ExternalSensors:Lcom/here/android/mpa/ar/ARController$ExternalSensors;

    .line 2906
    iput-object p1, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    .line 2907
    return-void
.end method

.method synthetic constructor <init>(Lcom/nokia/maps/d;Lcom/here/android/mpa/ar/ARController$1;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/here/android/mpa/ar/ARController;-><init>(Lcom/nokia/maps/d;)V

    return-void
.end method


# virtual methods
.method public addARObject(Lcom/here/android/mpa/ar/ARModelObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3019
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARModelObject;)V

    .line 3020
    return-void
.end method

.method public addARObject(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2960
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARObject;)V

    .line 2961
    return-void
.end method

.method public addARObject(Lcom/here/android/mpa/ar/ARPolylineObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2990
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARPolylineObject;)V

    .line 2991
    return-void
.end method

.method public addOnCameraEnteredListener(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3755
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V

    .line 3756
    return-void
.end method

.method public addOnCameraExitedListener(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3784
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V

    .line 3785
    return-void
.end method

.method public addOnCompassCalibrationChangedListener(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4126
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V

    .line 4127
    return-void
.end method

.method public addOnLivesightStatusListener(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4352
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V

    .line 4353
    return-void
.end method

.method public addOnMapEnteredListener(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3813
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V

    .line 3814
    return-void
.end method

.method public addOnMapExitedListener(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3841
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V

    .line 3842
    return-void
.end method

.method public addOnObjectTappedListener(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4157
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V

    .line 4158
    return-void
.end method

.method public addOnPanListener(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3986
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V

    .line 3987
    return-void
.end method

.method public addOnPoseListener(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4212
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V

    .line 4213
    return-void
.end method

.method public addOnPostPresentListener(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4324
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V

    .line 4325
    return-void
.end method

.method public addOnPreDrawListener(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4240
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V

    .line 4241
    return-void
.end method

.method public addOnPreDrawMapListener(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4268
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V

    .line 4269
    return-void
.end method

.method public addOnPrePresentListener(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4296
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V

    .line 4297
    return-void
.end method

.method public addOnProjectionCameraUpdatedListener(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4408
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V

    .line 4409
    return-void
.end method

.method public addOnRadarUpdateListener(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4185
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V

    .line 4186
    return-void
.end method

.method public addOnSensorCalibrationChangedListener(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4096
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V

    .line 4097
    return-void
.end method

.method public addOnTapListener(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4013
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V

    .line 4014
    return-void
.end method

.method public addOnTouchDownListener(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4041
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V

    .line 4042
    return-void
.end method

.method public addOnTouchUpListener(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4068
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V

    .line 4069
    return-void
.end method

.method public defocus()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3078
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->b()V

    .line 3079
    return-void
.end method

.method public depress(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3119
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->g(Lcom/here/android/mpa/ar/ARObject;)V

    .line 3120
    return-void
.end method

.method public focus(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3067
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->e(Lcom/here/android/mpa/ar/ARObject;)V

    .line 3068
    return-void
.end method

.method public geoTo3dPosition(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Vector3f;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4544
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Vector3f;)Z

    move-result v0

    return v0
.end method

.method public getAccelerometerCalibrationStatus()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3426
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->h()I

    move-result v0

    return v0
.end method

.method public getCompassAccuracy()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4508
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->Y()F

    move-result v0

    return v0
.end method

.method public getCompassCalibrationStatus()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3448
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->i()I

    move-result v0

    return v0
.end method

.method public getFixedAltitude()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4483
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->X()F

    move-result v0

    return v0
.end method

.method public getGyroscopeCalibrationStatus()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3470
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->j()I

    move-result v0

    return v0
.end method

.method public getObjectId(Lcom/here/android/mpa/ar/ARObject;)J
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3050
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->i(Lcom/here/android/mpa/ar/ARObject;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getObjects(Landroid/graphics/PointF;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/ar/ARObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3250
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getObjects(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/ViewRect;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/ar/ARObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3265
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/common/ViewRect;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOcclusionOpacity()F
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3725
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->Q()F

    move-result v0

    return v0
.end method

.method public getPose()Lcom/here/android/mpa/ar/ARPoseReading;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3364
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->l()Lcom/here/android/mpa/ar/ARPoseReading;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 8
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3377
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->r()Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    .line 3378
    if-nez v0, :cond_0

    .line 3379
    const/4 v1, 0x0

    .line 3381
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->c()D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    goto :goto_0
.end method

.method public getPosition(Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 8
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3399
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/nokia/maps/GeoCoordinateImpl;

    move-result-object v0

    .line 3400
    if-nez v0, :cond_0

    .line 3401
    const/4 v1, 0x0

    .line 3403
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->a()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->b()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/nokia/maps/GeoCoordinateImpl;->c()D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    goto :goto_0
.end method

.method public getProjectionType()Lcom/here/android/mpa/ar/ARController$ProjectionType;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3632
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->J()Lcom/here/android/mpa/ar/ARController$ProjectionType;

    move-result-object v0

    return-object v0
.end method

.method public getScreenViewPoint()Landroid/graphics/PointF;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3674
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->P()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getSensorsWaitTimeout()J
    .locals 2
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3662
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->O()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUpdateDistanceDelta()I
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3607
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->C()I

    move-result v0

    return v0
.end method

.method public getViewType()Lcom/here/android/mpa/ar/ARController$ViewType;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3352
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->k()Lcom/here/android/mpa/ar/ARController$ViewType;

    move-result-object v0

    return-object v0
.end method

.method public isOccluded(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3222
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->d(Lcom/here/android/mpa/ar/ARObject;)Z

    move-result v0

    return v0
.end method

.method public isOcclusionEnabled()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3700
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->t()Z

    move-result v0

    return v0
.end method

.method public isUsingAlternativeCenter()Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3517
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->y()Z

    move-result v0

    return v0
.end method

.method public isVisible(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3206
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->c(Lcom/here/android/mpa/ar/ARObject;)Z

    move-result v0

    return v0
.end method

.method public pan(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3280
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 3281
    return-void
.end method

.method public panTo(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3297
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 3298
    return-void
.end method

.method public pixelTo3dPosition(FLandroid/graphics/PointF;Lcom/here/android/mpa/common/Vector3f;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4527
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/d;->a(FLandroid/graphics/PointF;Lcom/here/android/mpa/common/Vector3f;)Z

    move-result v0

    return v0
.end method

.method public press(Landroid/graphics/PointF;)Lcom/here/android/mpa/ar/ARObject;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3093
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Landroid/graphics/PointF;)Lcom/here/android/mpa/ar/ARObject;

    move-result-object v0

    return-object v0
.end method

.method public press(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3106
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->f(Lcom/here/android/mpa/ar/ARObject;)V

    .line 3107
    return-void
.end method

.method public removeARObject(Lcom/here/android/mpa/ar/ARModelObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3035
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARModelObject;)Z

    move-result v0

    return v0
.end method

.method public removeARObject(Lcom/here/android/mpa/ar/ARObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2977
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARObject;)Z

    move-result v0

    return v0
.end method

.method public removeARObject(Lcom/here/android/mpa/ar/ARPolylineObject;)Z
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3006
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARPolylineObject;)Z

    move-result v0

    return v0
.end method

.method public removeOnCameraEnteredListener(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3768
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnCameraEnteredListener;)V

    .line 3769
    return-void
.end method

.method public removeOnCameraExitedListener(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3797
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnCameraExitedListener;)V

    .line 3798
    return-void
.end method

.method public removeOnCompassCalibrationChangedListener(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4142
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnCompassCalibrationChangedListener;)V

    .line 4143
    return-void
.end method

.method public removeOnLivesightStatusListener(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4365
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnLivesightStatusListener;)V

    .line 4366
    return-void
.end method

.method public removeOnMapEnteredListener(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3826
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnMapEnteredListener;)V

    .line 3827
    return-void
.end method

.method public removeOnMapExitedListener(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3854
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnMapExitedListener;)V

    .line 3855
    return-void
.end method

.method public removeOnObjectTappedListener(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4170
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnObjectTappedListener;)V

    .line 4171
    return-void
.end method

.method public removeOnPanListener(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3999
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPanListener;)V

    .line 4000
    return-void
.end method

.method public removeOnPoseListener(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4225
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPoseListener;)V

    .line 4226
    return-void
.end method

.method public removeOnPostPresentListener(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4337
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPostPresentListener;)V

    .line 4338
    return-void
.end method

.method public removeOnPreDrawListener(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4253
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPreDrawListener;)V

    .line 4254
    return-void
.end method

.method public removeOnPreDrawMapListener(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4281
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPreDrawMapListener;)V

    .line 4282
    return-void
.end method

.method public removeOnPrePresentListener(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4309
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnPrePresentListener;)V

    .line 4310
    return-void
.end method

.method public removeOnProjectionCameraUpdatedListener(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4422
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnProjectionCameraUpdatedListener;)V

    .line 4423
    return-void
.end method

.method public removeOnRadarUpdateListener(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4198
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnRadarUpdateListener;)V

    .line 4199
    return-void
.end method

.method public removeOnSensorCalibrationChangedListener(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4111
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnSensorCalibrationChangedListener;)V

    .line 4112
    return-void
.end method

.method public removeOnTapListener(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4026
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnTapListener;)V

    .line 4027
    return-void
.end method

.method public removeOnTouchDownListener(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4054
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnTouchDownListener;)V

    .line 4055
    return-void
.end method

.method public removeOnTouchUpListener(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4081
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/ar/ARController$OnTouchUpListener;)V

    .line 4082
    return-void
.end method

.method public removePitchFunction()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4393
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->S()V

    .line 4394
    return-void
.end method

.method public select(Lcom/here/android/mpa/ar/ARObject;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3142
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->h(Lcom/here/android/mpa/ar/ARObject;)V

    .line 3143
    return-void
.end method

.method public select(Lcom/here/android/mpa/ar/ARObject;ZF)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3173
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARObject;ZF)V

    .line 3174
    return-void
.end method

.method public setAlternativeCenter(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3504
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 3505
    return-void
.end method

.method public setCompassAccuracy(F)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4496
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->n(F)V

    .line 4497
    return-void
.end method

.method public setFixedAltitude(FZ)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4471
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(FZ)V

    .line 4472
    return-void
.end method

.method public setInfoAnimationInUpViewOnly(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3739
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->l(Z)V

    .line 3740
    return-void
.end method

.method public setMap(Lcom/here/android/mpa/mapping/Map;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2947
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/mapping/Map;)V

    .line 2948
    return-void
.end method

.method public setOcclusionEnabled(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3688
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->g(Z)V

    .line 3689
    return-void
.end method

.method public setOcclusionOpacity(F)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3713
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->m(F)V

    .line 3714
    return-void
.end method

.method public setOrientationAnimation(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4436
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->m(Z)V

    .line 4437
    return-void
.end method

.method public setPitchFunction(Lcom/here/android/mpa/ar/ARController$OnPitchFunction;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 4380
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$OnPitchFunction;)V

    .line 4381
    return-void
.end method

.method public setPlanesParameters(FFFF)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3558
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/nokia/maps/d;->a(FFFF)V

    .line 3559
    return-void
.end method

.method public setProjectionType(Lcom/here/android/mpa/ar/ARController$ProjectionType;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3620
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$ProjectionType;)V

    .line 3621
    return-void
.end method

.method public setSensorsWaitTimeout(J)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3650
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->f(J)V

    .line 3651
    return-void
.end method

.method public setTapArea(II)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3577
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->d(II)V

    .line 3578
    return-void
.end method

.method public setUpdateDistanceDelta(I)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3593
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(I)V

    .line 3594
    return-void
.end method

.method public setUseDownIconsOnMap(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3531
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->b(Z)V

    .line 3532
    return-void
.end method

.method public showFrontItemsOnly(Z)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3235
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->d(Z)V

    .line 3236
    return-void
.end method

.method public showView(Lcom/here/android/mpa/ar/ARController$ViewType;)V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3339
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Lcom/here/android/mpa/ar/ARController$ViewType;)V

    .line 3340
    return-void
.end method

.method public start()Lcom/here/android/mpa/ar/ARController$Error;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2918
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->a()Lcom/here/android/mpa/ar/ARController$Error;

    move-result-object v0

    return-object v0
.end method

.method public stop(Z)Lcom/here/android/mpa/ar/ARController$Error;
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 2933
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->a(Z)Lcom/here/android/mpa/ar/ARController$Error;

    move-result-object v0

    return-object v0
.end method

.method public unselect()V
    .locals 1
    .annotation build Lcom/nokia/maps/annotation/HybridPlus;
    .end annotation

    .prologue
    .line 3190
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->c()V

    .line 3191
    return-void
.end method
