.class public final Lcom/here/android/mpa/ar/ARController$DownViewParams;
.super Ljava/lang/Object;
.source "ARController.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/ar/ARController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "DownViewParams"
.end annotation


# instance fields
.field final synthetic a:Lcom/here/android/mpa/ar/ARController;


# direct methods
.method private constructor <init>(Lcom/here/android/mpa/ar/ARController;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 685
    return-void
.end method

.method synthetic constructor <init>(Lcom/here/android/mpa/ar/ARController;Lcom/here/android/mpa/ar/ARController$1;)V
    .locals 0

    .prologue
    .line 682
    invoke-direct {p0, p1}, Lcom/here/android/mpa/ar/ARController$DownViewParams;-><init>(Lcom/here/android/mpa/ar/ARController;)V

    return-void
.end method


# virtual methods
.method public getCenterInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->w:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getFadeInAnimationDelay()J
    .locals 2

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFadeInAnimationTime()J
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->b(Lcom/nokia/maps/d$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFadeInInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getFadeOutAnimationDelay()J
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFadeOutAnimationTime()J
    .locals 2

    .prologue
    .line 990
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->b(Lcom/nokia/maps/d$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFadeOutInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getGeoCenterInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->x:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getHeadingInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->t:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAlpha()F
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->I()F

    move-result v0

    return v0
.end method

.method public getMaxZoomOutScale()F
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->M()F

    move-result v0

    return v0
.end method

.method public getMinAlpha()F
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->H()F

    move-result v0

    return v0
.end method

.method public getMinPitch()F
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->N()F

    move-result v0

    return v0
.end method

.method public getPitchInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->u:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getPitchThreshold()F
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->f()F

    move-result v0

    return v0
.end method

.method public getZoomInterpolator()Lcom/here/android/mpa/ar/AnimationInterpolator;
    .locals 2

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->v:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1}, Lcom/nokia/maps/d;->c(Lcom/nokia/maps/d$a;)Lcom/here/android/mpa/ar/AnimationInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public isAutoGeoCenterEnabled()Z
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->q()Z

    move-result v0

    return v0
.end method

.method public isAutoHeadingEnabled()Z
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->o()Z

    move-result v0

    return v0
.end method

.method public isAutoPitchEnabled()Z
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->n()Z

    move-result v0

    return v0
.end method

.method public isAutoTFCEnabled()Z
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->p()Z

    move-result v0

    return v0
.end method

.method public isAutoZoomEnabled()Z
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0}, Lcom/nokia/maps/d;->m()Z

    move-result v0

    return v0
.end method

.method public setAutoControlOnEntryExit(Z)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->e(Z)V

    .line 760
    return-object p0
.end method

.method public setAutoGeoCenterEnabled(ZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->e(ZZ)V

    .line 876
    return-object p0
.end method

.method public setAutoHeadingEnabled(ZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->c(ZZ)V

    .line 826
    return-object p0
.end method

.method public setAutoPitchEnabled(ZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->b(ZZ)V

    .line 801
    return-object p0
.end method

.method public setAutoTFCEnabled(ZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->d(ZZ)V

    .line 851
    return-object p0
.end method

.method public setAutoZoomEnabled(ZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(ZZ)V

    .line 776
    return-object p0
.end method

.method public setCenterInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->w:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1170
    return-object p0
.end method

.method public setFadeInAnimationDelay(J)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 3

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;J)V

    .line 1002
    return-object p0
.end method

.method public setFadeInAnimationTime(J)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 3

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/nokia/maps/d;->b(Lcom/nokia/maps/d$a;J)V

    .line 1023
    return-object p0
.end method

.method public setFadeInInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->m:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1065
    return-object p0
.end method

.method public setFadeOutAnimationDelay(J)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 3

    .prologue
    .line 959
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;J)V

    .line 960
    return-object p0
.end method

.method public setFadeOutAnimationTime(J)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 3

    .prologue
    .line 980
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/nokia/maps/d;->b(Lcom/nokia/maps/d$a;J)V

    .line 981
    return-object p0
.end method

.method public setFadeOutInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->n:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1044
    return-object p0
.end method

.method public setGeoCenterInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->x:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1149
    return-object p0
.end method

.method public setHeadingInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->t:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1086
    return-object p0
.end method

.method public setMaxAlpha(F)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 938
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->i(F)V

    .line 939
    return-object p0
.end method

.method public setMaxZoomOutScale(FZZ)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/nokia/maps/d;->a(FZZ)V

    .line 705
    return-object p0
.end method

.method public setMinAlpha(F)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->h(F)V

    .line 918
    return-object p0
.end method

.method public setMinPitch(F)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->l(F)V

    .line 732
    return-object p0
.end method

.method public setPitchInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->u:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1107
    return-object p0
.end method

.method public setPitchThreshold(F)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1}, Lcom/nokia/maps/d;->d(F)V

    .line 897
    return-object p0
.end method

.method public setTransformCenter(Landroid/graphics/PointF;Z)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 1

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    invoke-virtual {v0, p1, p2}, Lcom/nokia/maps/d;->a(Landroid/graphics/PointF;Z)V

    .line 1198
    return-object p0
.end method

.method public setZoomInterpolator(Lcom/here/android/mpa/ar/AnimationInterpolator;)Lcom/here/android/mpa/ar/ARController$DownViewParams;
    .locals 2

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/here/android/mpa/ar/ARController$DownViewParams;->a:Lcom/here/android/mpa/ar/ARController;

    iget-object v0, v0, Lcom/here/android/mpa/ar/ARController;->a:Lcom/nokia/maps/d;

    sget-object v1, Lcom/nokia/maps/d$a;->v:Lcom/nokia/maps/d$a;

    invoke-virtual {v0, v1, p1}, Lcom/nokia/maps/d;->a(Lcom/nokia/maps/d$a;Lcom/here/android/mpa/ar/AnimationInterpolator;)V

    .line 1128
    return-object p0
.end method
