.class public Lcom/here/android/mpa/service/MapService;
.super Landroid/app/Service;
.source "MapService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/here/android/mpa/service/MapService$a;
    }
.end annotation


# static fields
.field private static i:Landroid/content/Context;

.field private static final j:[Ljava/lang/String;

.field private static k:Z

.field private static l:Z


# instance fields
.field a:Landroid/os/MemoryFile;

.field b:Ljava/util/concurrent/Semaphore;

.field c:J

.field d:Ljava/io/FileDescriptor;

.field protected e:I

.field f:Ljava/util/concurrent/Semaphore;

.field private g:Z

.field private h:Lcom/here/android/mpa/service/MapService$a;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:S

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private final v:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/nokia/maps/be;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Lcom/nokia/maps/bd$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/here/android/mpa/service/MapService;->i:Landroid/content/Context;

    .line 112
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gnustl_shared"

    aput-object v2, v0, v1

    const-string v1, "crypto_here"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "ssl_here"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NuanceVocalizer"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "os_adaptation.context"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "os_adaptation.network"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "MAPSJNI"

    aput-object v2, v0, v1

    sput-object v0, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    .line 118
    sput-boolean v3, Lcom/here/android/mpa/service/MapService;->k:Z

    .line 123
    sput-boolean v3, Lcom/here/android/mpa/service/MapService;->l:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 179
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 103
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->a:Landroid/os/MemoryFile;

    .line 104
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->b:Ljava/util/concurrent/Semaphore;

    .line 105
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/here/android/mpa/service/MapService;->c:J

    .line 106
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->d:Ljava/io/FileDescriptor;

    .line 107
    iput-boolean v3, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 108
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->h:Lcom/here/android/mpa/service/MapService$a;

    .line 240
    iput v3, p0, Lcom/here/android/mpa/service/MapService;->e:I

    .line 241
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    .line 242
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    .line 243
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    .line 244
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    .line 245
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    .line 247
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->s:Ljava/lang/String;

    .line 248
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->t:Ljava/lang/String;

    .line 249
    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->f:Ljava/util/concurrent/Semaphore;

    .line 250
    iput-boolean v3, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    .line 416
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->v:Landroid/os/RemoteCallbackList;

    .line 421
    new-instance v0, Lcom/here/android/mpa/service/MapService$1;

    invoke-direct {v0, p0}, Lcom/here/android/mpa/service/MapService$1;-><init>(Lcom/here/android/mpa/service/MapService;)V

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->w:Lcom/nokia/maps/bd$a;

    .line 181
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->b:Ljava/util/concurrent/Semaphore;

    .line 182
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->f:Ljava/util/concurrent/Semaphore;

    .line 183
    return-void
.end method

.method static synthetic a(Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    .prologue
    .line 101
    sput-object p0, Lcom/here/android/mpa/service/MapService;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/here/android/mpa/service/MapService;)Landroid/os/RemoteCallbackList;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->v:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic a(Lcom/here/android/mpa/service/MapService;Lcom/here/android/mpa/service/MapService$a;)Lcom/here/android/mpa/service/MapService$a;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/here/android/mpa/service/MapService;->h:Lcom/here/android/mpa/service/MapService$a;

    return-object p1
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/here/android/mpa/service/MapService;->k:Z

    return v0
.end method

.method static synthetic a(Lcom/here/android/mpa/service/MapService;Z)Z
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    return p1
.end method

.method static synthetic b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/here/android/mpa/service/MapService;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/here/android/mpa/service/MapService;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    return v0
.end method

.method static synthetic c(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 203
    .line 206
    :try_start_0
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 207
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    .line 210
    if-eqz v1, :cond_0

    .line 211
    const-string v2, "stayResident"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 213
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 2

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 409
    invoke-static {}, Lcom/nokia/maps/MapServiceClient;->stopServer()V

    .line 410
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 411
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    .line 413
    :cond_0
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    monitor-exit p0

    return-void

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/here/android/mpa/service/MapService;)S
    .locals 1

    .prologue
    .line 101
    iget-short v0, p0, Lcom/here/android/mpa/service/MapService;->r:S

    return v0
.end method

.method static synthetic i(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/here/android/mpa/service/MapService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/here/android/mpa/service/MapService;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/here/android/mpa/service/MapService;->d()V

    return-void
.end method

.method static synthetic l(Lcom/here/android/mpa/service/MapService;)Lcom/here/android/mpa/service/MapService$a;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->h:Lcom/here/android/mpa/service/MapService$a;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->w:Lcom/nokia/maps/bd$a;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 192
    invoke-direct {p0}, Lcom/here/android/mpa/service/MapService;->c()Z

    move-result v0

    sput-boolean v0, Lcom/here/android/mpa/service/MapService;->l:Z

    .line 193
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->a:Landroid/os/MemoryFile;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/here/android/mpa/service/MapService;->a:Landroid/os/MemoryFile;

    invoke-virtual {v0}, Landroid/os/MemoryFile;->close()V

    .line 237
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 238
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v0, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 258
    .line 259
    monitor-enter p0

    .line 262
    :try_start_0
    const-string v2, "nukeservice"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 263
    iget-boolean v0, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 264
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 265
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    .line 266
    if-ne v0, v4, :cond_0

    .line 267
    invoke-static {}, Lcom/nokia/maps/MapServiceClient;->stopServer()V

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->stopSelf()V

    .line 271
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 272
    const/4 v0, 0x2

    monitor-exit p0

    .line 386
    :goto_0
    return v0

    .line 275
    :cond_1
    iget-boolean v2, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    if-ne v2, v4, :cond_2

    .line 276
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 277
    const-string v2, "diskcachepath"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 279
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    .line 280
    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    .line 282
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->f:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 283
    invoke-static {}, Lcom/nokia/maps/MapServiceClient;->stopServer()V

    .line 284
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 285
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/here/android/mpa/service/MapService;->u:Z

    .line 290
    :cond_2
    iget-boolean v2, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    if-nez v2, :cond_15

    .line 292
    const-string v0, "mapdataserverurl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    .line 293
    const-string v0, "mapsatelliteserverurl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    .line 294
    const-string v0, "terrainserverurl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    .line 295
    const-string v0, "diskcachepath"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    .line 296
    const-string v0, "sliserverurl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    .line 297
    const-string v0, "mapvariant"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getShortExtra(Ljava/lang/String;S)S

    move-result v0

    iput-short v0, p0, Lcom/here/android/mpa/service/MapService;->r:S

    .line 298
    const-string v0, "USESSL"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/here/android/mpa/service/MapService;->k:Z

    .line 299
    const-string v0, "shutdownmode"

    const/4 v2, 0x3

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 306
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    .line 307
    :cond_3
    const-string v2, "hybrid.api.here.com"

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->m:Ljava/lang/String;

    .line 309
    :cond_4
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    .line 310
    :cond_5
    const-string v2, "http://1.sps.data.here.com"

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->n:Ljava/lang/String;

    .line 312
    :cond_6
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    .line 313
    :cond_7
    const-string v2, "http://hterrain.mfs.data.here.com"

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->o:Ljava/lang/String;

    .line 315
    :cond_8
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_a

    .line 316
    :cond_9
    const-string v2, "sli.data.here.com"

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->q:Ljava/lang/String;

    .line 318
    :cond_a
    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_c

    .line 319
    :cond_b
    invoke-static {}, Lcom/nokia/maps/MapSettings;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    .line 321
    :cond_c
    const-string v2, "isolated_diskcache_enabled"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 322
    const-string v2, "client_app_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->s:Ljava/lang/String;

    .line 323
    const-string v2, "client_app_token"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/here/android/mpa/service/MapService;->t:Ljava/lang/String;

    .line 327
    :cond_d
    sget-object v2, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_11

    aget-object v4, v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :try_start_1
    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    :cond_e
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 330
    :catch_0
    move-exception v5

    .line 331
    :try_start_2
    sget-object v5, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v6, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_e

    sget-object v5, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v6, 0x4

    aget-object v5, v5, v6

    .line 332
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_e

    sget-object v5, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    .line 333
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_e

    goto :goto_2

    .line 341
    :catch_1
    move-exception v5

    .line 342
    sget-object v6, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_f

    .line 346
    const-string v4, "MapService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Library loaded with error:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/nokia/maps/bp;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 348
    :cond_f
    :try_start_3
    sget-object v5, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v6, 0x4

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_10

    sget-object v5, Lcom/here/android/mpa/service/MapService;->j:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    .line 349
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_e

    .line 350
    :cond_10
    const-string v4, "MapService"

    const-string v5, "Library loaded failed; the library may be static"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/nokia/maps/bp;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 359
    :cond_11
    invoke-static {}, Lcom/nokia/maps/BaseNativeObject;->w()V

    .line 361
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/nokia/maps/ApplicationContext;->b(Landroid/content/Context;)V

    .line 362
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-ne v1, v2, :cond_12

    .line 363
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/here/network/NetworkProtocol;->setAppContext(Landroid/content/Context;)V

    .line 367
    :cond_12
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_13

    .line 368
    const-string v1, "MapService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to mkdirs() for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/here/android/mpa/service/MapService;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/nokia/maps/bp;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    :cond_13
    invoke-virtual {p0}, Lcom/here/android/mpa/service/MapService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/nokia/maps/SSLCertManager;->a(Landroid/content/Context;)Z

    .line 374
    iget v1, p0, Lcom/here/android/mpa/service/MapService;->e:I

    if-nez v1, :cond_14

    .line 375
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/lit16 v1, v1, 0x61a8

    iput v1, p0, Lcom/here/android/mpa/service/MapService;->e:I

    .line 380
    :cond_14
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/here/android/mpa/service/MapService;->g:Z

    .line 382
    iget-object v1, p0, Lcom/here/android/mpa/service/MapService;->f:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 384
    :cond_15
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 355
    :catch_2
    move-exception v4

    goto/16 :goto_2
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 402
    sget-boolean v0, Lcom/here/android/mpa/service/MapService;->l:Z

    if-nez v0, :cond_0

    .line 403
    invoke-direct {p0}, Lcom/here/android/mpa/service/MapService;->d()V

    .line 405
    :cond_0
    return-void
.end method

.method public onUnbind()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method
