.class Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;
.super Ljava/lang/Object;
.source "UsageTrackingClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Connection"
.end annotation


# instance fields
.field final mListener:Lcom/here/services/internal/Manager$ConnectionListener;

.field private mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

.field final synthetic this$0:Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;


# direct methods
.method constructor <init>(Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;Lcom/here/services/internal/Manager$ConnectionListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->this$0:Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    .line 60
    return-void
.end method


# virtual methods
.method onDisconnect()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->this$0:Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;

    iget-object v1, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    # invokes: Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->handleServiceDisconnected(Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;)V
    invoke-static {v0, v1}, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->access$200(Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;)V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    .line 98
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onDisconnected()V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 65
    :try_start_0
    invoke-static {p2}, Lcom/here/services/internal/ServiceUtil;->isServiceNotAvailableBinder(Landroid/os/IBinder;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "service is not available"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :catch_0
    move-exception v0

    .line 76
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->this$0:Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;

    # getter for: Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->access$100(Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 77
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnectionFailed()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    :try_start_1
    invoke-static {p2}, Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    move-result-object v0

    iput-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    .line 70
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->this$0:Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;

    iget-object v1, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mService:Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;

    # invokes: Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->handleServiceConnected(Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;)V
    invoke-static {v0, v1}, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;->access$000(Lcom/here/services/positioning/analytics/internal/UsageTrackingClient;Lcom/here/services/positioning/analytics/internal/IUsageTrackingClient;)V

    .line 71
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnected()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/here/services/positioning/analytics/internal/UsageTrackingClient$Connection;->onDisconnect()V

    .line 87
    return-void
.end method
