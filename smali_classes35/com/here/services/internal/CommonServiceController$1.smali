.class Lcom/here/services/internal/CommonServiceController$1;
.super Ljava/lang/Object;
.source "CommonServiceController.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/services/internal/CommonServiceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/here/services/internal/CommonServiceController;


# direct methods
.method constructor <init>(Lcom/here/services/internal/CommonServiceController;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 81
    iget-object v1, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    monitor-enter v1

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    invoke-static {p2}, Lcom/here/services/internal/ILocationServiceController$Stub;->asInterface(Landroid/os/IBinder;)Lcom/here/services/internal/ILocationServiceController;

    move-result-object v2

    # setter for: Lcom/here/services/internal/CommonServiceController;->mServiceController:Lcom/here/services/internal/ILocationServiceController;
    invoke-static {v0, v2}, Lcom/here/services/internal/CommonServiceController;->access$002(Lcom/here/services/internal/CommonServiceController;Lcom/here/services/internal/ILocationServiceController;)Lcom/here/services/internal/ILocationServiceController;

    .line 83
    iget-object v0, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    # invokes: Lcom/here/services/internal/CommonServiceController;->onControllerConnected()V
    invoke-static {v0}, Lcom/here/services/internal/CommonServiceController;->access$200(Lcom/here/services/internal/CommonServiceController;)V

    .line 84
    monitor-exit v1

    .line 85
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 69
    iget-object v1, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    # getter for: Lcom/here/services/internal/CommonServiceController;->mServiceController:Lcom/here/services/internal/ILocationServiceController;
    invoke-static {v0}, Lcom/here/services/internal/CommonServiceController;->access$000(Lcom/here/services/internal/CommonServiceController;)Lcom/here/services/internal/ILocationServiceController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    monitor-exit v1

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    const/4 v2, 0x0

    # setter for: Lcom/here/services/internal/CommonServiceController;->mServiceController:Lcom/here/services/internal/ILocationServiceController;
    invoke-static {v0, v2}, Lcom/here/services/internal/CommonServiceController;->access$002(Lcom/here/services/internal/CommonServiceController;Lcom/here/services/internal/ILocationServiceController;)Lcom/here/services/internal/ILocationServiceController;

    .line 74
    iget-object v0, p0, Lcom/here/services/internal/CommonServiceController$1;->this$0:Lcom/here/services/internal/CommonServiceController;

    # invokes: Lcom/here/services/internal/CommonServiceController;->onControllerDisconnected()V
    invoke-static {v0}, Lcom/here/services/internal/CommonServiceController;->access$100(Lcom/here/services/internal/CommonServiceController;)V

    .line 75
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
