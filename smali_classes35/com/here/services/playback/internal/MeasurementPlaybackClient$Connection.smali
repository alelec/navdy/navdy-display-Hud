.class Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;
.super Ljava/lang/Object;
.source "MeasurementPlaybackClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/services/playback/internal/MeasurementPlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Connection"
.end annotation


# instance fields
.field final mListener:Lcom/here/services/internal/Manager$ConnectionListener;

.field private mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

.field final synthetic this$0:Lcom/here/services/playback/internal/MeasurementPlaybackClient;


# direct methods
.method constructor <init>(Lcom/here/services/playback/internal/MeasurementPlaybackClient;Lcom/here/services/internal/Manager$ConnectionListener;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->this$0:Lcom/here/services/playback/internal/MeasurementPlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p2, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    .line 86
    return-void
.end method


# virtual methods
.method onDisconnect()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->this$0:Lcom/here/services/playback/internal/MeasurementPlaybackClient;

    iget-object v1, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    # invokes: Lcom/here/services/playback/internal/MeasurementPlaybackClient;->handleServiceDisconnected(Lcom/here/services/playback/internal/IMeasurementPlaybackClient;)V
    invoke-static {v0, v1}, Lcom/here/services/playback/internal/MeasurementPlaybackClient;->access$200(Lcom/here/services/playback/internal/MeasurementPlaybackClient;Lcom/here/services/playback/internal/IMeasurementPlaybackClient;)V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    .line 128
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onDisconnected()V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 91
    :try_start_0
    invoke-static {p2}, Lcom/here/services/internal/ServiceUtil;->isServiceNotAvailableBinder(Landroid/os/IBinder;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "service is not available"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :catch_0
    move-exception v0

    .line 106
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->this$0:Lcom/here/services/playback/internal/MeasurementPlaybackClient;

    # getter for: Lcom/here/services/playback/internal/MeasurementPlaybackClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/here/services/playback/internal/MeasurementPlaybackClient;->access$100(Lcom/here/services/playback/internal/MeasurementPlaybackClient;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 107
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnectionFailed()V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    :try_start_1
    invoke-static {p2}, Lcom/here/services/playback/internal/IMeasurementPlaybackClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    move-result-object v0

    iput-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    .line 98
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->this$0:Lcom/here/services/playback/internal/MeasurementPlaybackClient;

    iget-object v1, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mService:Lcom/here/services/playback/internal/IMeasurementPlaybackClient;

    # invokes: Lcom/here/services/playback/internal/MeasurementPlaybackClient;->handleServiceConnected(Lcom/here/services/playback/internal/IMeasurementPlaybackClient;)V
    invoke-static {v0, v1}, Lcom/here/services/playback/internal/MeasurementPlaybackClient;->access$000(Lcom/here/services/playback/internal/MeasurementPlaybackClient;Lcom/here/services/playback/internal/IMeasurementPlaybackClient;)V

    .line 99
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnected()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/here/services/playback/internal/MeasurementPlaybackClient$Connection;->onDisconnect()V

    .line 117
    return-void
.end method
