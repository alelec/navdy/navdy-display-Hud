.class Lcom/here/services/location/internal/PositioningClient$Connection;
.super Ljava/lang/Object;
.source "PositioningClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/services/location/internal/PositioningClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Connection"
.end annotation


# instance fields
.field final mListener:Lcom/here/services/internal/Manager$ConnectionListener;

.field private mService:Lcom/here/services/location/internal/IPositioningClient;

.field final synthetic this$0:Lcom/here/services/location/internal/PositioningClient;


# direct methods
.method constructor <init>(Lcom/here/services/location/internal/PositioningClient;Lcom/here/services/internal/Manager$ConnectionListener;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->this$0:Lcom/here/services/location/internal/PositioningClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p2, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    .line 103
    return-void
.end method


# virtual methods
.method onDisconnect()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mService:Lcom/here/services/location/internal/IPositioningClient;

    if-nez v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->this$0:Lcom/here/services/location/internal/PositioningClient;

    iget-object v1, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mService:Lcom/here/services/location/internal/IPositioningClient;

    # invokes: Lcom/here/services/location/internal/PositioningClient;->handleServiceDisconnected(Lcom/here/services/location/internal/IPositioningClient;)V
    invoke-static {v0, v1}, Lcom/here/services/location/internal/PositioningClient;->access$500(Lcom/here/services/location/internal/PositioningClient;Lcom/here/services/location/internal/IPositioningClient;)V

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mService:Lcom/here/services/location/internal/IPositioningClient;

    .line 145
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onDisconnected()V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 108
    :try_start_0
    invoke-static {p2}, Lcom/here/services/internal/ServiceUtil;->isServiceNotAvailableBinder(Landroid/os/IBinder;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "service is not available"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :catch_0
    move-exception v0

    .line 123
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->this$0:Lcom/here/services/location/internal/PositioningClient;

    # getter for: Lcom/here/services/location/internal/PositioningClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/here/services/location/internal/PositioningClient;->access$400(Lcom/here/services/location/internal/PositioningClient;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 124
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnectionFailed()V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    :try_start_1
    invoke-static {p2}, Lcom/here/services/location/internal/IPositioningClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/here/services/location/internal/IPositioningClient;

    move-result-object v0

    iput-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mService:Lcom/here/services/location/internal/IPositioningClient;

    .line 115
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->this$0:Lcom/here/services/location/internal/PositioningClient;

    iget-object v1, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mService:Lcom/here/services/location/internal/IPositioningClient;

    # invokes: Lcom/here/services/location/internal/PositioningClient;->handleServiceConnected(Lcom/here/services/location/internal/IPositioningClient;)V
    invoke-static {v0, v1}, Lcom/here/services/location/internal/PositioningClient;->access$300(Lcom/here/services/location/internal/PositioningClient;Lcom/here/services/location/internal/IPositioningClient;)V

    .line 116
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/here/services/location/internal/PositioningClient$Connection;->mListener:Lcom/here/services/internal/Manager$ConnectionListener;

    invoke-interface {v0}, Lcom/here/services/internal/Manager$ConnectionListener;->onConnected()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/here/services/location/internal/PositioningClient$Connection;->onDisconnect()V

    .line 134
    return-void
.end method
