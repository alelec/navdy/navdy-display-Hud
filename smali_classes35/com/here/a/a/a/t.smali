.class public final enum Lcom/here/a/a/a/t;
.super Ljava/lang/Enum;
.source "TransportType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/here/a/a/a/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/here/a/a/a/t;

.field public static final enum b:Lcom/here/a/a/a/t;

.field public static final enum c:Lcom/here/a/a/a/t;

.field public static final enum d:Lcom/here/a/a/a/t;

.field public static final enum e:Lcom/here/a/a/a/t;

.field public static final enum f:Lcom/here/a/a/a/t;

.field public static final enum g:Lcom/here/a/a/a/t;

.field public static final enum h:Lcom/here/a/a/a/t;

.field public static final enum i:Lcom/here/a/a/a/t;

.field public static final enum j:Lcom/here/a/a/a/t;

.field public static final enum k:Lcom/here/a/a/a/t;

.field public static final enum l:Lcom/here/a/a/a/t;

.field public static final enum m:Lcom/here/a/a/a/t;

.field public static final enum n:Lcom/here/a/a/a/t;

.field public static final enum o:Lcom/here/a/a/a/t;

.field public static final enum p:Lcom/here/a/a/a/t;

.field public static final enum q:Lcom/here/a/a/a/t;

.field public static final enum r:Lcom/here/a/a/a/t;

.field public static final enum s:Lcom/here/a/a/a/t;

.field public static final enum t:Lcom/here/a/a/a/t;

.field public static final enum u:Lcom/here/a/a/a/t;

.field public static final enum v:Lcom/here/a/a/a/t;

.field public static final enum w:Lcom/here/a/a/a/t;

.field private static final synthetic y:[Lcom/here/a/a/a/t;


# instance fields
.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TRAIN_HIGH_SPEED"

    invoke-direct {v0, v1, v4, v4}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->a:Lcom/here/a/a/a/t;

    .line 8
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TRAIN_INTERCITY"

    invoke-direct {v0, v1, v5, v5}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->b:Lcom/here/a/a/a/t;

    .line 9
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TRAIN_INTERREGIONAL_AND_FAST"

    invoke-direct {v0, v1, v6, v6}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->c:Lcom/here/a/a/a/t;

    .line 10
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TRAIN_REGIONAL_AND_OTHER"

    invoke-direct {v0, v1, v7, v7}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->d:Lcom/here/a/a/a/t;

    .line 11
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TRAIN_CITY"

    invoke-direct {v0, v1, v8, v8}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->e:Lcom/here/a/a/a/t;

    .line 12
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "BUS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->f:Lcom/here/a/a/a/t;

    .line 13
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "WATER_BOAT_OR_FERRYS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->g:Lcom/here/a/a/a/t;

    .line 14
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "RAIL_UNDEGROUND_OR_SUBWAY"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->h:Lcom/here/a/a/a/t;

    .line 15
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "RAIL_TRAM"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->i:Lcom/here/a/a/a/t;

    .line 16
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "ORDERED_SERVICES_OR_TAXI"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->j:Lcom/here/a/a/a/t;

    .line 17
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "RAIL_INCLINED"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->k:Lcom/here/a/a/a/t;

    .line 18
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "AERIAL"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->l:Lcom/here/a/a/a/t;

    .line 19
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "BUS_RAPID"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->m:Lcom/here/a/a/a/t;

    .line 20
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "RAIL_MONORAIL"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->n:Lcom/here/a/a/a/t;

    .line 21
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "FLIGHT"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->o:Lcom/here/a/a/a/t;

    .line 22
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->p:Lcom/here/a/a/a/t;

    .line 23
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "BIKE"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->q:Lcom/here/a/a/a/t;

    .line 24
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "BIKE_SHARE"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->r:Lcom/here/a/a/a/t;

    .line 25
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "PARK_AND_RIDE"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->s:Lcom/here/a/a/a/t;

    .line 26
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "WALK"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->t:Lcom/here/a/a/a/t;

    .line 27
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "CAR"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->u:Lcom/here/a/a/a/t;

    .line 28
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "CAR_SHARE"

    const/16 v2, 0x15

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->v:Lcom/here/a/a/a/t;

    .line 29
    new-instance v0, Lcom/here/a/a/a/t;

    const-string v1, "TAXI"

    const/16 v2, 0x16

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/here/a/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/here/a/a/a/t;->w:Lcom/here/a/a/a/t;

    .line 6
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/here/a/a/a/t;

    sget-object v1, Lcom/here/a/a/a/t;->a:Lcom/here/a/a/a/t;

    aput-object v1, v0, v4

    sget-object v1, Lcom/here/a/a/a/t;->b:Lcom/here/a/a/a/t;

    aput-object v1, v0, v5

    sget-object v1, Lcom/here/a/a/a/t;->c:Lcom/here/a/a/a/t;

    aput-object v1, v0, v6

    sget-object v1, Lcom/here/a/a/a/t;->d:Lcom/here/a/a/a/t;

    aput-object v1, v0, v7

    sget-object v1, Lcom/here/a/a/a/t;->e:Lcom/here/a/a/a/t;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/here/a/a/a/t;->f:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/here/a/a/a/t;->g:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/here/a/a/a/t;->h:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/here/a/a/a/t;->i:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/here/a/a/a/t;->j:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/here/a/a/a/t;->k:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/here/a/a/a/t;->l:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/here/a/a/a/t;->m:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/here/a/a/a/t;->n:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/here/a/a/a/t;->o:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/here/a/a/a/t;->p:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/here/a/a/a/t;->q:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/here/a/a/a/t;->r:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/here/a/a/a/t;->s:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/here/a/a/a/t;->t:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/here/a/a/a/t;->u:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/here/a/a/a/t;->v:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/here/a/a/a/t;->w:Lcom/here/a/a/a/t;

    aput-object v2, v0, v1

    sput-object v0, Lcom/here/a/a/a/t;->y:[Lcom/here/a/a/a/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/here/a/a/a/t;->x:I

    .line 35
    return-void
.end method

.method public static a(I)Lcom/here/a/a/a/t;
    .locals 5

    .prologue
    .line 42
    invoke-static {}, Lcom/here/a/a/a/t;->a()[Lcom/here/a/a/a/t;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 43
    iget v4, v0, Lcom/here/a/a/a/t;->x:I

    if-ne p0, v4, :cond_0

    .line 47
    :goto_1
    return-object v0

    .line 42
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/here/a/a/a/t;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    const/16 v0, 0x10

    new-array v3, v0, [I

    .line 53
    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([II)V

    .line 54
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/a/a/a/t;

    .line 55
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/here/a/a/a/t;->b()I

    move-result v5

    array-length v6, v3

    if-ge v5, v6, :cond_0

    .line 56
    invoke-virtual {v0}, Lcom/here/a/a/a/t;->b()I

    move-result v0

    const/4 v5, 0x1

    aput v5, v3, v0

    goto :goto_0

    .line 58
    :cond_1
    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget v1, v3, v0

    .line 59
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 61
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()[Lcom/here/a/a/a/t;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/here/a/a/a/t;->y:[Lcom/here/a/a/a/t;

    invoke-virtual {v0}, [Lcom/here/a/a/a/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/here/a/a/a/t;

    return-object v0
.end method


# virtual methods
.method public b()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/here/a/a/a/t;->x:I

    return v0
.end method
