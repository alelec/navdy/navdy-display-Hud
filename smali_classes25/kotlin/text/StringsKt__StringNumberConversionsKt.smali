.class Lkotlin/text/StringsKt__StringNumberConversionsKt;
.super Lkotlin/text/StringsKt__StringBuilderKt;
.source "StringNumberConversions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000F\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0005\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0002\u0008\u0007\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0010\n\n\u0002\u0008\u0005\u001a4\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0004\u0008\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u0002H\u00010\u0005H\u0083\u0008\u00a2\u0006\u0004\u0008\u0006\u0010\u0007\u001a\r\u0010\u0008\u001a\u00020\t*\u00020\u0003H\u0087\u0008\u001a\r\u0010\n\u001a\u00020\u000b*\u00020\u0003H\u0087\u0008\u001a\u0015\u0010\n\u001a\u00020\u000b*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000b*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u000f\u001a\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u0010\u001a\r\u0010\u0011\u001a\u00020\u0012*\u00020\u0003H\u0087\u0008\u001a\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0012*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u0014\u001a\r\u0010\u0015\u001a\u00020\u0016*\u00020\u0003H\u0087\u0008\u001a\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0016*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u0018\u001a\r\u0010\u0019\u001a\u00020\r*\u00020\u0003H\u0087\u0008\u001a\u0015\u0010\u0019\u001a\u00020\r*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0013\u0010\u001a\u001a\u0004\u0018\u00010\r*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u001b\u001a\u001b\u0010\u001a\u001a\u0004\u0018\u00010\r*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u001c\u001a\r\u0010\u001d\u001a\u00020\u001e*\u00020\u0003H\u0087\u0008\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0013\u0010\u001f\u001a\u0004\u0018\u00010\u001e*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010 \u001a\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010!\u001a\r\u0010\"\u001a\u00020#*\u00020\u0003H\u0087\u0008\u001a\u0015\u0010\"\u001a\u00020#*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0013\u0010$\u001a\u0004\u0018\u00010#*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010%\u001a\u001b\u0010$\u001a\u0004\u0018\u00010#*\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010&\u001a\u0015\u0010\'\u001a\u00020\u0003*\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0015\u0010\'\u001a\u00020\u0003*\u00020\r2\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0015\u0010\'\u001a\u00020\u0003*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u001a\u0015\u0010\'\u001a\u00020\u0003*\u00020#2\u0006\u0010\u000c\u001a\u00020\rH\u0087\u0008\u00a8\u0006("
    }
    d2 = {
        "screenFloatValue",
        "T",
        "str",
        "",
        "parse",
        "Lkotlin/Function1;",
        "screenFloatValue$StringsKt__StringNumberConversionsKt",
        "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "toBoolean",
        "",
        "toByte",
        "",
        "radix",
        "",
        "toByteOrNull",
        "(Ljava/lang/String;)Ljava/lang/Byte;",
        "(Ljava/lang/String;I)Ljava/lang/Byte;",
        "toDouble",
        "",
        "toDoubleOrNull",
        "(Ljava/lang/String;)Ljava/lang/Double;",
        "toFloat",
        "",
        "toFloatOrNull",
        "(Ljava/lang/String;)Ljava/lang/Float;",
        "toInt",
        "toIntOrNull",
        "(Ljava/lang/String;)Ljava/lang/Integer;",
        "(Ljava/lang/String;I)Ljava/lang/Integer;",
        "toLong",
        "",
        "toLongOrNull",
        "(Ljava/lang/String;)Ljava/lang/Long;",
        "(Ljava/lang/String;I)Ljava/lang/Long;",
        "toShort",
        "",
        "toShortOrNull",
        "(Ljava/lang/String;)Ljava/lang/Short;",
        "(Ljava/lang/String;I)Ljava/lang/Short;",
        "toString",
        "kotlin-stdlib"
    }
    k = 0x5
    mv = {
        0x1,
        0x1,
        0x6
    }
    xi = 0x1
    xs = "kotlin/text/StringsKt"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lkotlin/text/StringsKt__StringBuilderKt;-><init>()V

    return-void
.end method

.method private static final screenFloatValue$StringsKt__StringNumberConversionsKt(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "parse"    # Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1",
            "<-",
            "Ljava/lang/String;",
            "+TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 332
    nop

    .line 333
    :try_start_0
    sget-object v4, Lkotlin/text/ScreenFloatValueRegEx;->value:Lkotlin/text/Regex;

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    move-object v2, v0

    invoke-virtual {v4, v2}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 332
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v3

    .line 336
    goto :goto_0

    .line 337
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/NumberFormatException;
    move-object v2, v3

    .line 338
    goto :goto_0
.end method

.method private static final toBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 44
    invoke-static {p0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static final toByte(Ljava/lang/String;)B
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 52
    invoke-static {p0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method private static final toByte(Ljava/lang/String;I)B
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 61
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;I)B

    move-result v0

    return v0
.end method

.method public static final toByteOrNull(Ljava/lang/String;)Ljava/lang/Byte;
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lkotlin/text/StringsKt;->toByteOrNull(Ljava/lang/String;I)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public static final toByteOrNull(Ljava/lang/String;I)Ljava/lang/Byte;
    .locals 3
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v2, "$receiver"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-static {p0, p1}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 147
    .local v0, "int":I
    const/16 v2, -0x80

    if-lt v0, v2, :cond_0

    const/16 v2, 0x7f

    if-le v0, v2, :cond_1

    .line 148
    .end local v0    # "int":I
    :cond_0
    :goto_0
    return-object v1

    .restart local v0    # "int":I
    :cond_1
    int-to-byte v1, v0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_0
.end method

.method private static final toDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 129
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static final toDoubleOrNull(Ljava/lang/String;)Ljava/lang/Double;
    .locals 8
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-string v4, "$receiver"

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    nop

    .line 349
    nop

    .line 350
    :try_start_0
    sget-object v6, Lkotlin/text/ScreenFloatValueRegEx;->value:Lkotlin/text/Regex;

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    move-object v4, v0

    invoke-virtual {v6, v4}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 351
    move-object v3, p0

    .line 305
    .local v3, "p1":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .end local v3    # "p1":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_0
    move-object v4, v5

    .line 353
    goto :goto_0

    .line 354
    :catch_0
    move-exception v2

    .local v2, "e$iv":Ljava/lang/NumberFormatException;
    move-object v4, v5

    .line 355
    goto :goto_0
.end method

.method private static final toFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 121
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static final toFloatOrNull(Ljava/lang/String;)Ljava/lang/Float;
    .locals 6
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const-string v3, "$receiver"

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    nop

    .line 342
    nop

    .line 343
    :try_start_0
    sget-object v5, Lkotlin/text/ScreenFloatValueRegEx;->value:Lkotlin/text/Regex;

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    move-object v3, v0

    invoke-virtual {v5, v3}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    move-object v2, p0

    .line 297
    .local v2, "p1":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .end local v2    # "p1":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    move-object v3, v4

    .line 346
    goto :goto_0

    .line 347
    :catch_0
    move-exception v1

    .local v1, "e$iv":Ljava/lang/NumberFormatException;
    move-object v3, v4

    .line 348
    goto :goto_0
.end method

.method private static final toInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 87
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static final toInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 96
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static final toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static final toIntOrNull(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 12
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    const-string v10, "$receiver"

    invoke-static {p0, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    .line 184
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 185
    .local v4, "length":I
    if-nez v4, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-object v9

    .line 187
    :cond_1
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 192
    .local v1, "firstChar":C
    const/16 v10, 0x30

    if-ge v1, v10, :cond_3

    .line 193
    const/4 v10, 0x1

    if-eq v4, v10, :cond_0

    .line 195
    const/4 v8, 0x1

    .line 197
    .local v8, "start":I
    const/16 v10, 0x2d

    if-ne v1, v10, :cond_2

    .line 198
    const/4 v3, 0x1

    .line 199
    .local v3, "isNegative":Z
    const/high16 v5, -0x80000000

    .line 209
    .local v5, "limit":I
    :goto_1
    div-int v6, v5, p1

    .line 213
    .local v6, "limitBeforeMul":I
    const/4 v7, 0x0

    .line 214
    .local v7, "result":I
    add-int/lit8 v10, v4, -0x1

    if-gt v8, v10, :cond_4

    .line 215
    .end local v8    # "start":I
    :goto_2
    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11, p1}, Lkotlin/text/CharsKt;->digitOf(CI)I

    move-result v0

    .line 217
    .local v0, "digit":I
    if-ltz v0, :cond_0

    .line 218
    if-lt v7, v6, :cond_0

    .line 220
    mul-int/2addr v7, p1

    .line 222
    add-int v11, v5, v0

    if-lt v7, v11, :cond_0

    .line 224
    sub-int/2addr v7, v0

    .line 214
    if-eq v8, v10, :cond_4

    add-int/lit8 v2, v8, 0x1

    .local v2, "i":I
    move v8, v2

    goto :goto_2

    .line 200
    .end local v0    # "digit":I
    .end local v2    # "i":I
    .end local v3    # "isNegative":Z
    .end local v5    # "limit":I
    .end local v6    # "limitBeforeMul":I
    .end local v7    # "result":I
    .restart local v8    # "start":I
    :cond_2
    const/16 v10, 0x2b

    if-ne v1, v10, :cond_0

    .line 201
    const/4 v3, 0x0

    .line 202
    .restart local v3    # "isNegative":Z
    const v5, -0x7fffffff

    .restart local v5    # "limit":I
    goto :goto_1

    .line 206
    .end local v3    # "isNegative":Z
    .end local v5    # "limit":I
    .end local v8    # "start":I
    :cond_3
    const/4 v8, 0x0

    .line 207
    .restart local v8    # "start":I
    const/4 v3, 0x0

    .line 208
    .restart local v3    # "isNegative":Z
    const v5, -0x7fffffff

    .restart local v5    # "limit":I
    goto :goto_1

    .line 227
    .end local v8    # "start":I
    .restart local v6    # "limitBeforeMul":I
    .restart local v7    # "result":I
    :cond_4
    if-eqz v3, :cond_5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_0

    :cond_5
    neg-int v9, v7

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_0
.end method

.method private static final toLong(Ljava/lang/String;)J
    .locals 2
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 104
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static final toLong(Ljava/lang/String;I)J
    .locals 2
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 113
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lkotlin/text/StringsKt;->toLongOrNull(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static final toLongOrNull(Ljava/lang/String;I)Ljava/lang/Long;
    .locals 18
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v14, "$receiver"

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-static/range {p1 .. p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    .line 245
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 246
    .local v6, "length":I
    if-nez v6, :cond_0

    const/4 v14, 0x0

    .line 288
    :goto_0
    return-object v14

    .line 248
    :cond_0
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 253
    .local v3, "firstChar":C
    const/16 v14, 0x30

    if-ge v3, v14, :cond_4

    .line 254
    const/4 v14, 0x1

    if-ne v6, v14, :cond_1

    const/4 v14, 0x0

    goto :goto_0

    .line 256
    :cond_1
    const/4 v7, 0x1

    .line 258
    .local v7, "start":I
    const/16 v14, 0x2d

    if-ne v3, v14, :cond_2

    .line 259
    const/4 v5, 0x1

    .line 260
    .local v5, "isNegative":Z
    const-wide/high16 v8, -0x8000000000000000L

    .line 270
    .local v8, "limit":J
    :goto_1
    move/from16 v0, p1

    int-to-long v14, v0

    div-long v10, v8, v14

    .line 274
    .local v10, "limitBeforeMul":J
    const-wide/16 v12, 0x0

    .line 275
    .local v12, "result":J
    add-int/lit8 v14, v6, -0x1

    if-gt v7, v14, :cond_8

    .line 276
    .end local v7    # "start":I
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    move/from16 v0, p1

    invoke-static {v15, v0}, Lkotlin/text/CharsKt;->digitOf(CI)I

    move-result v2

    .line 278
    .local v2, "digit":I
    if-gez v2, :cond_5

    const/4 v14, 0x0

    goto :goto_0

    .line 261
    .end local v2    # "digit":I
    .end local v5    # "isNegative":Z
    .end local v8    # "limit":J
    .end local v10    # "limitBeforeMul":J
    .end local v12    # "result":J
    .restart local v7    # "start":I
    :cond_2
    const/16 v14, 0x2b

    if-ne v3, v14, :cond_3

    .line 262
    const/4 v5, 0x0

    .line 263
    .restart local v5    # "isNegative":Z
    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    .restart local v8    # "limit":J
    goto :goto_1

    .line 265
    .end local v5    # "isNegative":Z
    .end local v8    # "limit":J
    :cond_3
    const/4 v14, 0x0

    goto :goto_0

    .line 267
    .end local v7    # "start":I
    :cond_4
    const/4 v7, 0x0

    .line 268
    .restart local v7    # "start":I
    const/4 v5, 0x0

    .line 269
    .restart local v5    # "isNegative":Z
    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    .restart local v8    # "limit":J
    goto :goto_1

    .line 279
    .end local v7    # "start":I
    .restart local v2    # "digit":I
    .restart local v10    # "limitBeforeMul":J
    .restart local v12    # "result":J
    :cond_5
    cmp-long v15, v12, v10

    if-gez v15, :cond_6

    const/4 v14, 0x0

    goto :goto_0

    .line 281
    :cond_6
    move/from16 v0, p1

    int-to-long v0, v0

    move-wide/from16 v16, v0

    mul-long v12, v12, v16

    .line 283
    int-to-long v0, v2

    move-wide/from16 v16, v0

    add-long v16, v16, v8

    cmp-long v15, v12, v16

    if-gez v15, :cond_7

    const/4 v14, 0x0

    goto :goto_0

    .line 285
    :cond_7
    int-to-long v0, v2

    move-wide/from16 v16, v0

    sub-long v12, v12, v16

    .line 275
    if-eq v7, v14, :cond_8

    add-int/lit8 v4, v7, 0x1

    .local v4, "i":I
    move v7, v4

    goto :goto_2

    .line 288
    .end local v2    # "digit":I
    .end local v4    # "i":I
    :cond_8
    if-eqz v5, :cond_9

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    goto :goto_0

    :cond_9
    neg-long v14, v12

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    goto :goto_0
.end method

.method private static final toShort(Ljava/lang/String;)S
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 70
    invoke-static {p0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    return v0
.end method

.method private static final toShort(Ljava/lang/String;I)S
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 79
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v0

    return v0
.end method

.method public static final toShortOrNull(Ljava/lang/String;)Ljava/lang/Short;
    .locals 1
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lkotlin/text/StringsKt;->toShortOrNull(Ljava/lang/String;I)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public static final toShortOrNull(Ljava/lang/String;I)Ljava/lang/Short;
    .locals 3
    .param p0, "$receiver"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v2, "$receiver"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-static {p0, p1}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 165
    .local v0, "int":I
    const/16 v2, -0x8000

    if-lt v0, v2, :cond_0

    const/16 v2, 0x7fff

    if-le v0, v2, :cond_1

    .line 166
    .end local v0    # "int":I
    :cond_0
    :goto_0
    return-object v1

    .restart local v0    # "int":I
    :cond_1
    int-to-short v1, v0

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    goto :goto_0
.end method

.method private static final toString(BI)Ljava/lang/String;
    .locals 2
    .param p0, "$receiver"    # B
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 13
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {v0}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final toString(II)Ljava/lang/String;
    .locals 2
    .param p0, "$receiver"    # I
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 29
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final toString(JI)Ljava/lang/String;
    .locals 2
    .param p0, "$receiver"    # J
    .param p2, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 37
    invoke-static {p2}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, p1, v0}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Long.toString(this, checkRadix(radix))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final toString(SI)Ljava/lang/String;
    .locals 2
    .param p0, "$receiver"    # S
    .param p1, "radix"    # I
    .annotation build Lkotlin/SinceKotlin;
        version = "1.1"
    .end annotation

    .annotation build Lkotlin/internal/InlineOnly;
    .end annotation

    .prologue
    .line 21
    invoke-static {p1}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {v0}, Lkotlin/text/CharsKt;->checkRadix(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
