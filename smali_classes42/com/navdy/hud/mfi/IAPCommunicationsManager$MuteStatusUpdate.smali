.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "MuteStatusUpdate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

.field public static final enum MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    const-string v1, "MuteStatus"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    .line 62
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    return-object v0
.end method
