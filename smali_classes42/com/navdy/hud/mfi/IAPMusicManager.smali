.class public Lcom/navdy/hud/mfi/IAPMusicManager;
.super Lcom/navdy/hud/mfi/IAPControlMessageProcessor;
.source "IAPMusicManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;,
        Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;,
        Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;,
        Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;


# instance fields
.field private aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

.field private mNowPlayingUpdateListener:Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const-class v0, Lcom/navdy/hud/mfi/IAPMusicManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager;->TAG:Ljava/lang/String;

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->NowPlayingUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager;->mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 1
    .param p1, "processor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/hud/mfi/IAPMusicManager;->mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;-><init>([Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;Lcom/navdy/hud/mfi/iAPProcessor;)V

    .line 14
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;

    invoke-direct {v0}, Lcom/navdy/hud/mfi/NowPlayingUpdate;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    .line 18
    return-void
.end method

.method private mergeNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)V
    .locals 6
    .param p1, "incomingUpdate"    # Lcom/navdy/hud/mfi/NowPlayingUpdate;

    .prologue
    const-wide/16 v4, 0x0

    .line 163
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    .line 166
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    .line 169
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    .line 172
    :cond_2
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    .line 177
    :cond_3
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    if-eqz v0, :cond_4

    .line 178
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 180
    :cond_4
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    if-eqz v0, :cond_5

    .line 181
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 183
    :cond_5
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    if-eqz v0, :cond_6

    .line 184
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 188
    :cond_6
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    if-eqz v0, :cond_7

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    .line 191
    :cond_7
    iget-wide v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_8

    .line 192
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-wide v2, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    iput-wide v2, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    .line 195
    :cond_8
    iget v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    if-ltz v0, :cond_9

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    iput v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    .line 199
    :cond_9
    iget v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    if-ltz v0, :cond_a

    .line 200
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    iput v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    .line 203
    :cond_a
    iget-wide v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_b

    .line 204
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-wide v2, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    iput-wide v2, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    .line 208
    :cond_b
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppName:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppName:Ljava/lang/String;

    .line 211
    :cond_c
    iget-object v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppBundleId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 212
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget-object v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppBundleId:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppBundleId:Ljava/lang/String;

    .line 216
    :cond_d
    iget v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    const/16 v1, 0x80

    if-lt v0, v1, :cond_e

    iget v0, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    const/16 v1, 0xff

    if-gt v0, v1, :cond_e

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    iget v1, p1, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    iput v1, v0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    .line 221
    :cond_e
    return-void
.end method

.method private static parseNowPlayingUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/NowPlayingUpdate;
    .locals 9
    .param p0, "params"    # Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    .prologue
    const/4 v8, 0x0

    .line 111
    new-instance v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;

    invoke-direct {v2}, Lcom/navdy/hud/mfi/NowPlayingUpdate;-><init>()V

    .line 112
    .local v2, "nowPlayingUpdate":Lcom/navdy/hud/mfi/NowPlayingUpdate;
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 113
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(Ljava/lang/Enum;)[B

    move-result-object v0

    .line 114
    .local v0, "mediaItemAttributes":[B
    new-instance v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    invoke-direct {v1, v0, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;-><init>([BI)V

    .line 115
    .local v1, "mediaItemAttributesParams":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPersistentIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPersistentIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt64(I)Ljava/math/BigInteger;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPersistentIdentifier:Ljava/math/BigInteger;

    .line 117
    :cond_0
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 118
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    .line 119
    :cond_1
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackCount:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 120
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackCount:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt16(I)I

    move-result v5

    iput v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    .line 121
    :cond_2
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackNumber:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 122
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackNumber:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt16(I)I

    move-result v5

    iput v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    .line 123
    :cond_3
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtist:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 124
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtist:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    .line 125
    :cond_4
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 126
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    .line 127
    :cond_5
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtworkFileTransferIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 128
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtworkFileTransferIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(I)I

    move-result v5

    iput v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    .line 129
    :cond_6
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemGenre:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 130
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemGenre:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    .line 131
    :cond_7
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPlaybackDurationInMilliSeconds:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 132
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPlaybackDurationInMilliSeconds:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt32(I)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    .line 135
    .end local v0    # "mediaItemAttributes":[B
    .end local v1    # "mediaItemAttributesParams":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    :cond_8
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 136
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(Ljava/lang/Enum;)[B

    move-result-object v3

    .line 137
    .local v3, "playBackAttributes":[B
    new-instance v4, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    invoke-direct {v4, v3, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;-><init>([BI)V

    .line 138
    .local v4, "playbackAttributesParams":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackStatus:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 139
    const-class v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v6, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackStatus:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 140
    :cond_9
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackElapsedTimeInMilliseconds:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 141
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackElapsedTimeInMilliseconds:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt32(I)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    .line 142
    :cond_a
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppName:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 143
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppName:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppName:Ljava/lang/String;

    .line 144
    :cond_b
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppBundleId:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 145
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppBundleId:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mAppBundleId:Ljava/lang/String;

    .line 146
    :cond_c
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackShuffleMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 147
    const-class v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    sget-object v6, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackShuffleMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 148
    :cond_d
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackRepeatMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 149
    const-class v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    sget-object v6, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackRepeatMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    iput-object v5, v2, Lcom/navdy/hud/mfi/NowPlayingUpdate;->playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 151
    .end local v3    # "playBackAttributes":[B
    .end local v4    # "playbackAttributesParams":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    :cond_e
    return-object v2
.end method


# virtual methods
.method public bProcessControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V
    .locals 5
    .param p1, "message"    # Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .param p2, "session"    # I
    .param p3, "data"    # [B

    .prologue
    .line 97
    invoke-static {p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v0

    .line 98
    .local v0, "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$1;->$SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage:[I

    invoke-virtual {p1}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 100
    :pswitch_0
    invoke-static {v0}, Lcom/navdy/hud/mfi/IAPMusicManager;->parseNowPlayingUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/NowPlayingUpdate;

    move-result-object v1

    .line 101
    .local v1, "update":Lcom/navdy/hud/mfi/NowPlayingUpdate;
    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parsed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-direct {p0, v1}, Lcom/navdy/hud/mfi/IAPMusicManager;->mergeNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)V

    .line 103
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->mNowPlayingUpdateListener:Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->mNowPlayingUpdateListener:Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;

    iget-object v3, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->aggregatedNowPlayingUpdate:Lcom/navdy/hud/mfi/NowPlayingUpdate;

    invoke-interface {v2, v3}, Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;->onNowPlayingUpdate(Lcom/navdy/hud/mfi/NowPlayingUpdate;)V

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 239
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->onKeyDown(I)V

    .line 240
    return-void
.end method

.method public onKeyDown(Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;)V
    .locals 1
    .param p1, "key"    # Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->onKeyDown(Ljava/lang/Enum;)V

    .line 232
    return-void
.end method

.method public onKeyUp(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->onKeyUp(I)V

    .line 244
    return-void
.end method

.method public onKeyUp(Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;)V
    .locals 1
    .param p1, "key"    # Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->onKeyUp(Ljava/lang/Enum;)V

    .line 236
    return-void
.end method

.method public setNowPlayingUpdateListener(Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;)V
    .locals 0
    .param p1, "nowPlayingUpdateListener"    # Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->mNowPlayingUpdateListener:Lcom/navdy/hud/mfi/IAPNowPlayingUpdateListener;

    .line 71
    return-void
.end method

.method public startNowPlayingUpdates()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 74
    new-instance v2, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v2, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 76
    .local v2, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    new-instance v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    .line 77
    .local v1, "mediaItemProperties":Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    invoke-static {}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->values()[Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    move-result-object v6

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v0, v6, v5

    .line 78
    .local v0, "attribute":Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
    invoke-virtual {v0}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->getParam()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 77
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "attribute":Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
    :cond_0
    sget-object v5, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 82
    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    .line 83
    .local v3, "playbackProperties":Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    invoke-static {}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->values()[Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    move-result-object v5

    array-length v6, v5

    :goto_1
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    .line 84
    .local v0, "attribute":Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;
    invoke-virtual {v0}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->getParam()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 86
    .end local v0    # "attribute":Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;
    :cond_1
    sget-object v4, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 87
    iget-object v4, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 88
    return-void
.end method

.method public stopNowPlayingUpdates()V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 92
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPMusicManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 93
    return-void
.end method
