.class Lcom/navdy/hud/mfi/EASession$OutStream;
.super Ljava/io/OutputStream;
.source "EASession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/EASession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OutStream"
.end annotation


# instance fields
.field private FLUSH_DELAY:I

.field private autoFlush:Ljava/lang/Runnable;

.field private frames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/navdy/hud/mfi/EASession;


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/EASession;)V
    .locals 2
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 151
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->handler:Landroid/os/Handler;

    .line 153
    const/16 v0, 0xfa

    iput v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->FLUSH_DELAY:I

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    .line 157
    new-instance v0, Lcom/navdy/hud/mfi/EASession$OutStream$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/mfi/EASession$OutStream$1;-><init>(Lcom/navdy/hud/mfi/EASession$OutStream;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->autoFlush:Ljava/lang/Runnable;

    return-void
.end method

.method private assembleFrames()[B
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "buffer":[B
    const/4 v4, 0x0

    .line 182
    .local v4, "totalLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 183
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    add-int/2addr v4, v5

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_0
    if-lez v4, :cond_2

    .line 186
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 187
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "buffer":[B
    check-cast v0, [B

    .line 197
    .restart local v0    # "buffer":[B
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 199
    :cond_2
    return-object v0

    .line 189
    :cond_3
    const/4 v2, 0x0

    .line 190
    .local v2, "offset":I
    new-array v0, v4, [B

    .line 191
    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 192
    iget-object v5, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 193
    .local v3, "src":[B
    array-length v5, v3

    invoke-static {v3, v7, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    array-length v5, v3

    add-int/2addr v2, v5

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/navdy/hud/mfi/EASession$OutStream;->flush()V

    .line 169
    return-void
.end method

.method public declared-synchronized flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->autoFlush:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 173
    invoke-direct {p0}, Lcom/navdy/hud/mfi/EASession$OutStream;->assembleFrames()[B

    move-result-object v0

    .line 174
    .local v0, "packet":[B
    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->closed:Z
    invoke-static {v1}, Lcom/navdy/hud/mfi/EASession;->access$100(Lcom/navdy/hud/mfi/EASession;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 175
    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;
    invoke-static {v1}, Lcom/navdy/hud/mfi/EASession;->access$200(Lcom/navdy/hud/mfi/EASession;)Lcom/navdy/hud/mfi/iAPProcessor;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/mfi/EASessionPacket;

    iget-object v3, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/EASession;->getSessionIdentifier()I

    move-result v3

    invoke-direct {v2, v3, v0}, Lcom/navdy/hud/mfi/EASessionPacket;-><init>(I[B)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->queue(Lcom/navdy/hud/mfi/EASessionPacket;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    monitor-exit p0

    return-void

    .line 172
    .end local v0    # "packet":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public write(I)V
    .locals 3
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/EASession$OutStream;->write([B)V

    .line 222
    return-void
.end method

.method public declared-synchronized write([B)V
    .locals 5
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    # getter for: Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/EASession;->access$300()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    # getter for: Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/EASession;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s: OutStream.write: %d bytes: %s, \'%s\'"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    .line 207
    # getter for: Lcom/navdy/hud/mfi/EASession;->mySelf:Lcom/navdy/hud/mfi/EASession;
    invoke-static {v4}, Lcom/navdy/hud/mfi/EASession;->access$400(Lcom/navdy/hud/mfi/EASession;)Lcom/navdy/hud/mfi/EASession;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {p1, v4}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Lcom/navdy/hud/mfi/Utils;->toASCII([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 205
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->frames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->autoFlush:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 212
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->autoFlush:Ljava/lang/Runnable;

    iget v2, p0, Lcom/navdy/hud/mfi/EASession$OutStream;->FLUSH_DELAY:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public write([BII)V
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-static {p1, p2, p3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 217
    .local v0, "bytes":[B
    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/EASession$OutStream;->write([B)V

    .line 218
    return-void
.end method
