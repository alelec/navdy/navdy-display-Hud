.class Lcom/navdy/hud/mfi/EASession$BlockingInputStream;
.super Ljava/io/InputStream;
.source "EASession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/EASession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BlockingInputStream"
.end annotation


# instance fields
.field private source:Ljava/io/InputStream;

.field final synthetic this$0:Lcom/navdy/hud/mfi/EASession;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/EASession;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/EASession;
    .param p2, "source"    # Ljava/io/InputStream;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 112
    iput-object p2, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->source:Ljava/io/InputStream;

    .line 113
    return-void
.end method

.method private waitForAvailable()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->source:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/mfi/EASession;->access$000(Lcom/navdy/hud/mfi/EASession;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/mfi/EASession;->access$000(Lcom/navdy/hud/mfi/EASession;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 118
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->waitForAvailable()V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->source:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    monitor-exit v1

    return v0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/mfi/EASession;->access$000(Lcom/navdy/hud/mfi/EASession;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 126
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->waitForAvailable()V

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->source:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    monitor-exit v1

    return v0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public read([BII)I
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "byteOffset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->this$0:Lcom/navdy/hud/mfi/EASession;

    # getter for: Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/mfi/EASession;->access$000(Lcom/navdy/hud/mfi/EASession;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 134
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->waitForAvailable()V

    .line 135
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;->source:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    monitor-exit v1

    return v0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
