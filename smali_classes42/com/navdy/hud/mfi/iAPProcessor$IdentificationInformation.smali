.class final enum Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "IdentificationInformation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum AppMatchTeamID:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum BluetoothTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum CurrentLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum FirmwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum HardwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum LocationInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum Manufacturer:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum MaximumCurrentDrawnFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum MessagesReceivedFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum MessagesSentByAccessory:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum ModelIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum Name:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum PowerProvidingCapability:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum SerialNumber:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum SerialTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum SupportedExternalAccessoryProtocol:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum SupportedLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum USBDeviceTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum USBHostHIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum USBHostTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum VehicleInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum VehicleStatusComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

.field public static final enum iAP2HIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 294
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "Name"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Name:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 295
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "ModelIdentifier"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->ModelIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 296
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "Manufacturer"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Manufacturer:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 297
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "SerialNumber"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SerialNumber:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 298
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "FirmwareVersion"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->FirmwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 299
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "HardwareVersion"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->HardwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 300
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "MessagesSentByAccessory"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesSentByAccessory:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 301
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "MessagesReceivedFromDevice"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesReceivedFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 302
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "PowerProvidingCapability"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->PowerProvidingCapability:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 303
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "MaximumCurrentDrawnFromDevice"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MaximumCurrentDrawnFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 304
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "SupportedExternalAccessoryProtocol"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedExternalAccessoryProtocol:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 305
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "AppMatchTeamID"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->AppMatchTeamID:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 306
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "CurrentLanguage"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->CurrentLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 307
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "SupportedLanguage"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 308
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "SerialTransportComponent"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SerialTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 309
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "USBDeviceTransportComponent"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBDeviceTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 310
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "USBHostTransportComponent"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBHostTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 311
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "BluetoothTransportComponent"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->BluetoothTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 312
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "iAP2HIDComponent"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->iAP2HIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 313
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "VehicleInformationComponent"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->VehicleInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 314
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "VehicleStatusComponent"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->VehicleStatusComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 315
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "LocationInformationComponent"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->LocationInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 316
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v1, "USBHostHIDComponent"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBHostHIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 293
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Name:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->ModelIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Manufacturer:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SerialNumber:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->FirmwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->HardwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesSentByAccessory:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesReceivedFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->PowerProvidingCapability:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MaximumCurrentDrawnFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedExternalAccessoryProtocol:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->AppMatchTeamID:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->CurrentLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SerialTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBDeviceTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBHostTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->BluetoothTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->iAP2HIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->VehicleInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->VehicleStatusComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->LocationInformationComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->USBHostHIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 293
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 293
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    return-object v0
.end method
