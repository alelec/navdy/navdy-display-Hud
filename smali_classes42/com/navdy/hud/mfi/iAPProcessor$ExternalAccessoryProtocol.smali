.class final enum Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ExternalAccessoryProtocol"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

.field public static final enum ExternalAccessoryProtocolIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

.field public static final enum ExternalAccessoryProtocolMatchAction:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

.field public static final enum ExternalAccessoryProtocolName:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

.field public static final enum NativeTransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 320
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    const-string v1, "ExternalAccessoryProtocolIdentifier"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 321
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    const-string v1, "ExternalAccessoryProtocolName"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolName:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 322
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    const-string v1, "ExternalAccessoryProtocolMatchAction"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolMatchAction:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 323
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    const-string v1, "NativeTransportComponentIdentifier"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->NativeTransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 319
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolName:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolMatchAction:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->NativeTransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 319
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    return-object v0
.end method
