.class Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
.super Ljava/lang/Object;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IAP2Params"
.end annotation


# static fields
.field public static final DATA_OFFSET:I = 0x4

.field public static final ID_OFFSET:I = 0x2


# instance fields
.field data:[B

.field params:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>([BI)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    .line 383
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    .line 384
    move v0, p2

    .line 385
    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 386
    new-instance v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    invoke-direct {v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;-><init>()V

    .line 387
    .local v1, "param":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iput v0, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    .line 388
    invoke-static {p1, v0}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->length:I

    .line 389
    add-int/lit8 v2, v0, 0x2

    invoke-static {p1, v2}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->id:I

    .line 390
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    iget v3, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->id:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 391
    iget v2, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->length:I

    add-int/2addr v0, v2

    .line 392
    goto :goto_0

    .line 393
    .end local v1    # "param":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    :cond_0
    return-void
.end method


# virtual methods
.method getBlob(I)[B
    .locals 5
    .param p1, "paramId"    # I

    .prologue
    .line 436
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    .line 437
    .local v0, "p":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    iget v2, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    add-int/lit8 v2, v2, 0x4

    iget v3, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    iget v4, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->length:I

    add-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    return-object v1
.end method

.method getBlob(Ljava/lang/Enum;)[B
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 441
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method getBoolean(I)Z
    .locals 1
    .param p1, "paramId"    # I

    .prologue
    .line 480
    invoke-virtual {p0, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getBoolean(Ljava/lang/Enum;)Z
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 484
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method getEnum(I)I
    .locals 1
    .param p1, "paramId"    # I

    .prologue
    .line 458
    invoke-virtual {p0, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(I)I

    move-result v0

    return v0
.end method

.method getEnum(Ljava/lang/Enum;)I
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 476
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(I)I

    move-result v0

    return v0
.end method

.method getEnum(Ljava/lang/Class;I)Ljava/lang/Object;
    .locals 3
    .param p2, "param"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;I)TE;"
        }
    .end annotation

    .prologue
    .line 466
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-virtual {p0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(I)I

    move-result v0

    .line 467
    .local v0, "value":I
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    .line 468
    .local v1, "values":[Ljava/lang/Object;, "[TE;"
    if-ltz v0, :cond_0

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 469
    aget-object v2, v1, v0

    .line 471
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method getEnum(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Object;
    .locals 1
    .param p2, "param"    # Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Enum;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 462
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method getUInt16(I)I
    .locals 3
    .param p1, "paramId"    # I

    .prologue
    .line 413
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    .line 414
    .local v0, "p":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    iget v2, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v1

    return v1
.end method

.method getUInt16(Ljava/lang/Enum;)I
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 418
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt16(I)I

    move-result v0

    return v0
.end method

.method getUInt32(I)J
    .locals 4
    .param p1, "paramId"    # I

    .prologue
    .line 426
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    .line 427
    .local v0, "p":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    iget v2, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/navdy/hud/mfi/Utils;->unpackUInt32([BI)J

    move-result-wide v2

    return-wide v2
.end method

.method getUInt32(Ljava/lang/Enum;)J
    .locals 2
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 422
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt32(I)J

    move-result-wide v0

    return-wide v0
.end method

.method getUInt64(I)Ljava/math/BigInteger;
    .locals 3
    .param p1, "paramId"    # I

    .prologue
    .line 431
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    .line 432
    .local v0, "p":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    iget v2, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/navdy/hud/mfi/Utils;->unpackUInt64([BI)Ljava/math/BigInteger;

    move-result-object v1

    return-object v1
.end method

.method getUInt8(I)I
    .locals 3
    .param p1, "paramId"    # I

    .prologue
    .line 408
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;

    .line 409
    .local v0, "p":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->data:[B

    iget v2, v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;->offset:I

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/navdy/hud/mfi/Utils;->unpackUInt8([BI)I

    move-result v1

    return v1
.end method

.method getUInt8(Ljava/lang/Enum;)I
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 404
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(I)I

    move-result v0

    return v0
.end method

.method getUTF8(I)Ljava/lang/String;
    .locals 6
    .param p1, "paramId"    # I

    .prologue
    .line 445
    invoke-virtual {p0, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(I)[B

    move-result-object v0

    .line 447
    .local v0, "blob":[B
    :try_start_0
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    const-string v5, "UTF-8"

    invoke-direct {v2, v0, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :goto_0
    return-object v2

    .line 448
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method getUTF8(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1
    .param p1, "param"    # Ljava/lang/Enum;

    .prologue
    .line 454
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hasParam(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 396
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->params:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method hasParam(Ljava/lang/Enum;)Z
    .locals 1
    .param p1, "e"    # Ljava/lang/Enum;

    .prologue
    .line 400
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v0

    return v0
.end method
