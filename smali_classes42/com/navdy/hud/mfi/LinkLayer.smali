.class public Lcom/navdy/hud/mfi/LinkLayer;
.super Ljava/lang/Object;
.source "LinkLayer.java"

# interfaces
.implements Lcom/navdy/hud/mfi/SessionPacketReceiver;
.implements Lcom/navdy/hud/mfi/LinkPacketReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;
    }
.end annotation


# static fields
.field public static final CONTROL_BYTE_ACK:I = 0x40

.field public static final CONTROL_BYTE_EAK:I = 0x20

.field public static final CONTROL_BYTE_RST:I = 0x10

.field public static final CONTROL_BYTE_SLP:I = 0x8

.field public static final CONTROL_BYTE_SYN:I = 0x80

.field public static final DELAY_MILLIS:I = 0x14

.field public static final MESSAGE_CONNECTION_ENDED:I = 0x4

.field public static final MESSAGE_CONNECTION_STARTED:I = 0x3

.field public static final MESSAGE_LINK_PACKET:I = 0x1

.field public static final MESSAGE_PERIODIC:I = 0x0

.field public static final MESSAGE_SESSION_PACKET:I = 0x2

.field public static final TAG:Ljava/lang/String; = "LinkLayer"

.field private static final sLogLinkPacket:Z = true


# instance fields
.field private controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/navdy/hud/mfi/SessionPacket;",
            ">;"
        }
    .end annotation
.end field

.field private iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

.field private myHandler:Landroid/os/Handler;

.field private nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/navdy/hud/mfi/SessionPacket;",
            ">;"
        }
    .end annotation
.end field

.field private physicalLayer:Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

.field private remoteAddress:Ljava/lang/String;

.field private remoteName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "iap2"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p0}, Lcom/navdy/hud/mfi/LinkLayer;->start()V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/mfi/LinkLayer;)Lcom/navdy/hud/mfi/iAPProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/mfi/LinkLayer;I[BI)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;
    .param p1, "x1"    # I
    .param p2, "x2"    # [B
    .param p3, "x3"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/mfi/LinkLayer;->native_runloop(I[BI)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/mfi/LinkLayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/navdy/hud/mfi/LinkLayer;->native_get_message_session()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/mfi/LinkLayer;I)[B
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;
    .param p1, "x1"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/hud/mfi/LinkLayer;->native_pop_message_data(I)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/mfi/LinkLayer;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;
    .param p1, "x1"    # [B

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/hud/mfi/LinkLayer;->logOutGoingLinkPacket([B)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/mfi/LinkLayer;)Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->physicalLayer:Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/mfi/LinkLayer;Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/LinkLayer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/navdy/hud/mfi/Packet;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/mfi/LinkLayer;->log(Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V

    return-void
.end method

.method private log(Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V
    .locals 5
    .param p1, "desc"    # Ljava/lang/String;
    .param p2, "pkt"    # Lcom/navdy/hud/mfi/Packet;

    .prologue
    .line 179
    const-string v0, "LinkLayer"

    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Lcom/navdy/hud/mfi/Packet;->data:[B

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/mfi/Utils;->logTransfer(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    return-void
.end method

.method private logOutGoingLinkPacket([B)V
    .locals 7
    .param p1, "data"    # [B

    .prologue
    .line 146
    if-eqz p1, :cond_5

    array-length v4, p1

    const/4 v5, 0x7

    if-le v4, v5, :cond_5

    .line 147
    const/4 v4, 0x5

    aget-byte v4, p1, v4

    and-int/lit16 v3, v4, 0xff

    .line 148
    .local v3, "packetSequenceNumber":I
    const/4 v4, 0x6

    aget-byte v4, p1, v4

    and-int/lit16 v0, v4, 0xff

    .line 149
    .local v0, "ackNumber":I
    const/4 v4, 0x4

    aget-byte v4, p1, v4

    and-int/lit16 v1, v4, 0xff

    .line 150
    .local v1, "controlByte":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v2, "controlBytes":Ljava/lang/StringBuilder;
    and-int/lit8 v4, v1, 0x40

    if-eqz v4, :cond_0

    .line 152
    const-string v4, " ACK "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    and-int/lit16 v4, v1, 0x80

    if-eqz v4, :cond_1

    .line 155
    const-string v4, " SYN "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_1
    and-int/lit8 v4, v1, 0x20

    if-eqz v4, :cond_2

    .line 158
    const-string v4, " EAK "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    and-int/lit8 v4, v1, 0x10

    if-eqz v4, :cond_3

    .line 161
    const-string v4, " RST "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_3
    and-int/lit8 v4, v1, 0x8

    if-eqz v4, :cond_4

    .line 164
    const-string v4, " SLP "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    const-string v4, "MFi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Accessory(L)   :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    .end local v0    # "ackNumber":I
    .end local v1    # "controlByte":I
    .end local v2    # "controlBytes":Ljava/lang/StringBuilder;
    .end local v3    # "packetSequenceNumber":I
    :cond_5
    return-void
.end method

.method private native native_get_message_session()I
.end method

.method private native native_pop_message_data(I)[B
.end method

.method private native native_runloop(I[BI)I
.end method

.method private queuePacket(ILjava/lang/Object;)V
    .locals 2
    .param p1, "messageType"    # I
    .param p2, "pkt"    # Ljava/lang/Object;

    .prologue
    .line 174
    iget-object v1, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 175
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 176
    return-void
.end method


# virtual methods
.method public connect(Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;)V
    .locals 0
    .param p1, "physicalLayer"    # Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/navdy/hud/mfi/LinkLayer;->physicalLayer:Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

    .line 41
    return-void
.end method

.method public connect(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 0
    .param p1, "iAPProcessor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/navdy/hud/mfi/LinkLayer;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    .line 45
    return-void
.end method

.method public connectionEnded()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 224
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 225
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 228
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 229
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/mfi/LinkLayer;->queuePacket(ILjava/lang/Object;)V

    .line 230
    return-void
.end method

.method public connectionStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "remoteAddress"    # Ljava/lang/String;
    .param p2, "remoteName"    # Ljava/lang/String;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/navdy/hud/mfi/LinkLayer;->remoteAddress:Ljava/lang/String;

    .line 217
    iput-object p2, p0, Lcom/navdy/hud/mfi/LinkLayer;->remoteName:Ljava/lang/String;

    .line 218
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/mfi/LinkLayer;->queuePacket(ILjava/lang/Object;)V

    .line 219
    return-void
.end method

.method public getLocalAddress()[B
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->physicalLayer:Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

    invoke-interface {v0}, Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;->getLocalAddress()[B

    move-result-object v0

    return-object v0
.end method

.method public getRemoteAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->remoteAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->remoteName:Ljava/lang/String;

    return-object v0
.end method

.method public native native_get_maximum_payload_size()I
.end method

.method public queue(Lcom/navdy/hud/mfi/LinkPacket;)V
    .locals 1
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/LinkPacket;

    .prologue
    .line 185
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/mfi/LinkLayer;->queuePacket(ILjava/lang/Object;)V

    .line 186
    return-void
.end method

.method public queue(Lcom/navdy/hud/mfi/SessionPacket;)V
    .locals 2
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/SessionPacket;

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "iAP->LL["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/mfi/LinkLayer;->log(Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V

    .line 191
    iget v0, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    iget-object v1, p0, Lcom/navdy/hud/mfi/LinkLayer;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->getControlSessionId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 192
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 196
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 197
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 67
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 68
    new-instance v0, Lcom/navdy/hud/mfi/LinkLayer$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/mfi/LinkLayer$1;-><init>(Lcom/navdy/hud/mfi/LinkLayer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/mfi/LinkLayer;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 143
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 171
    return-void
.end method
