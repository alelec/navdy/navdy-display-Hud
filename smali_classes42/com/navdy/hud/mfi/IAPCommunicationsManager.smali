.class public Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.super Lcom/navdy/hud/mfi/IAPControlMessageProcessor;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;,
        Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field public static mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;


# instance fields
.field private callStatus:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field private callUUid:Ljava/lang/String;

.field private mUpdatesListener:Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->TAG:Ljava/lang/String;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CommunicationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 1
    .param p1, "processor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 100
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mMessages:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;-><init>([Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;Lcom/navdy/hud/mfi/iAPProcessor;)V

    .line 101
    return-void
.end method

.method public static parseCallStateUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/CallStateUpdate;
    .locals 3
    .param p0, "updateParams"    # Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    .prologue
    .line 173
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate;

    invoke-direct {v0}, Lcom/navdy/hud/mfi/CallStateUpdate;-><init>()V

    .line 174
    .local v0, "update":Lcom/navdy/hud/mfi/CallStateUpdate;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->remoteID:Ljava/lang/String;

    .line 177
    :cond_0
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 178
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->displayName:Ljava/lang/String;

    .line 180
    :cond_1
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 181
    const-class v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->status:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 183
    :cond_2
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 184
    const-class v1, Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->direction:Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

    .line 186
    :cond_3
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 187
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->callUUID:Ljava/lang/String;

    .line 189
    :cond_4
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 190
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->addressBookID:Ljava/lang/String;

    .line 192
    :cond_5
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 193
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUTF8(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->label:Ljava/lang/String;

    .line 195
    :cond_6
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 196
    const-class v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    .line 198
    :cond_7
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 199
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBoolean(Ljava/lang/Enum;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->isConferenced:Z

    .line 201
    :cond_8
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 202
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(Ljava/lang/Enum;)I

    move-result v1

    iput v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->conferenceGroup:I

    .line 204
    :cond_9
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 205
    const-class v1, Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getEnum(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

    iput-object v1, v0, Lcom/navdy/hud/mfi/CallStateUpdate;->disconnectReason:Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

    .line 206
    :cond_a
    return-object v0
.end method

.method public static parseCommunicationUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/CommunicationUpdate;
    .locals 2
    .param p0, "updateParams"    # Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    .prologue
    .line 210
    new-instance v0, Lcom/navdy/hud/mfi/CommunicationUpdate;

    invoke-direct {v0}, Lcom/navdy/hud/mfi/CommunicationUpdate;-><init>()V

    .line 211
    .local v0, "communicationUpdate":Lcom/navdy/hud/mfi/CommunicationUpdate;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    iget v1, v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->id:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->hasParam(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    iget v1, v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->id:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/navdy/hud/mfi/CommunicationUpdate;->muteStatus:Z

    .line 214
    :cond_0
    return-object v0
.end method


# virtual methods
.method public acceptCall()V
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callUUid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 262
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->AcceptAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->HoldAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 263
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callUUid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 264
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 266
    .end local v0    # "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    :cond_0
    return-void
.end method

.method public acceptCall(Ljava/lang/String;)V
    .locals 3
    .param p1, "callUUid"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 250
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->AcceptAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->HoldAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 251
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 252
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 254
    .end local v0    # "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    :cond_0
    return-void
.end method

.method public bProcessControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V
    .locals 7
    .param p1, "message"    # Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .param p2, "session"    # I
    .param p3, "data"    # [B

    .prologue
    .line 109
    invoke-static {p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v1

    .line 110
    .local v1, "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    iget v4, p1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v5, v5, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    if-ne v4, v5, :cond_1

    .line 111
    invoke-static {v1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->parseCallStateUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/CallStateUpdate;

    move-result-object v3

    .line 112
    .local v3, "update":Lcom/navdy/hud/mfi/CallStateUpdate;
    iget-object v4, v3, Lcom/navdy/hud/mfi/CallStateUpdate;->status:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    iput-object v4, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callStatus:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 113
    iget-object v4, v3, Lcom/navdy/hud/mfi/CallStateUpdate;->callUUID:Ljava/lang/String;

    iput-object v4, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callUUid:Ljava/lang/String;

    .line 115
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mUpdatesListener:Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;

    invoke-interface {v4, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;->onCallStateUpdate(Lcom/navdy/hud/mfi/CallStateUpdate;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v3    # "update":Lcom/navdy/hud/mfi/CallStateUpdate;
    :cond_0
    :goto_0
    return-void

    .line 116
    .restart local v3    # "update":Lcom/navdy/hud/mfi/CallStateUpdate;
    :catch_0
    move-exception v2

    .line 117
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad call state update listener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 119
    .end local v2    # "t":Ljava/lang/Throwable;
    .end local v3    # "update":Lcom/navdy/hud/mfi/CallStateUpdate;
    :cond_1
    iget v4, p1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CommunicationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v5, v5, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    if-ne v4, v5, :cond_0

    .line 120
    invoke-static {v1}, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->parseCommunicationUpdate(Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;)Lcom/navdy/hud/mfi/CommunicationUpdate;

    move-result-object v0

    .line 122
    .local v0, "communicationUpdate":Lcom/navdy/hud/mfi/CommunicationUpdate;
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mUpdatesListener:Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;

    invoke-interface {v4, v0}, Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;->onCommunicationUpdate(Lcom/navdy/hud/mfi/CommunicationUpdate;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 123
    :catch_1
    move-exception v2

    .line 124
    .restart local v2    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad call state update listener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public callVoiceMail()V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 238
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->VoiceMail:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 239
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 240
    return-void
.end method

.method public endCall()V
    .locals 3

    .prologue
    .line 272
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callUUid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 274
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->EndAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndDecline:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 275
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->callUUid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 276
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 278
    .end local v0    # "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    :cond_0
    return-void
.end method

.method public endCall(Ljava/lang/String;)V
    .locals 3
    .param p1, "callUUid"    # Ljava/lang/String;

    .prologue
    .line 286
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 287
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 288
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->EndAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndDecline:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 289
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 290
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 292
    .end local v0    # "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    :cond_0
    return-void
.end method

.method public initiateDestinationCall(Ljava/lang/String;)V
    .locals 3
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 223
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 224
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Destination:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 225
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->DestinationID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 226
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;->Telephony:Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 227
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 228
    return-void
.end method

.method public mute()V
    .locals 3

    .prologue
    .line 299
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 300
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBoolean(Ljava/lang/Enum;Z)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 301
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 302
    return-void
.end method

.method public redial()V
    .locals 3

    .prologue
    .line 231
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 232
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Redial:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 233
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 234
    return-void
.end method

.method public setUpdatesListener(Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->mUpdatesListener:Lcom/navdy/hud/mfi/IAPCommunicationsUpdateListener;

    .line 105
    return-void
.end method

.method public startCommunicationUpdates()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public startUpdates()V
    .locals 3

    .prologue
    .line 131
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 132
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 133
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 134
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 135
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 136
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 137
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 138
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 139
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 140
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 141
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 142
    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 143
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 144
    return-void
.end method

.method public stopCommunicationUpdates()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public stopUpdates()V
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 148
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 149
    return-void
.end method

.method public unMute()V
    .locals 3

    .prologue
    .line 306
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 307
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$MuteStatusUpdate;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBoolean(Ljava/lang/Enum;Z)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 308
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 309
    return-void
.end method
