.class final enum Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
.super Ljava/lang/Enum;
.source "FileTransferSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/FileTransferSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "FileTransferCommand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_FIRST_AND_ONLY_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_FIRST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_LAST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_PAUSE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_SETUP:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_START:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

.field public static final enum COMMAND_SUCCESS:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;


# instance fields
.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_DATA"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 19
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_START"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_START:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 20
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_CANCEL"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 21
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_PAUSE"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_PAUSE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 22
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_SETUP"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SETUP:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 23
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_SUCCESS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SUCCESS:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 24
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_FAILURE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 26
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_LAST_DATA"

    const/4 v2, 0x7

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_LAST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 27
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_FIRST_DATA"

    const/16 v2, 0x8

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 28
    new-instance v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    const-string v1, "COMMAND_FIRST_AND_ONLY_DATA"

    const/16 v2, 0x9

    const/16 v3, 0xc0

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_AND_ONLY_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .line 17
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    sget-object v1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_START:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_PAUSE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SETUP:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SUCCESS:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_LAST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_AND_ONLY_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->$VALUES:[Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    .line 34
    return-void
.end method

.method public static fromInt(I)Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    iget v0, v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    if-lt p0, v0, :cond_0

    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    iget v0, v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    if-gt p0, v0, :cond_0

    .line 38
    invoke-static {}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->values()[Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    move-result-object v0

    aget-object v0, v0, p0

    .line 46
    :goto_0
    return-object v0

    .line 39
    :cond_0
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_LAST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    iget v0, v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    if-ne p0, v0, :cond_1

    .line 40
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_LAST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    goto :goto_0

    .line 41
    :cond_1
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    iget v0, v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    if-ne p0, v0, :cond_2

    .line 42
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    goto :goto_0

    .line 43
    :cond_2
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_AND_ONLY_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    iget v0, v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    if-ne p0, v0, :cond_3

    .line 44
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FIRST_AND_ONLY_DATA:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    goto :goto_0

    .line 46
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->$VALUES:[Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    return-object v0
.end method
