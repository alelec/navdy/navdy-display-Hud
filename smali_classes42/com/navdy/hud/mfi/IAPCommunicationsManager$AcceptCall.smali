.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AcceptCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

.field public static final enum AcceptAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

.field public static final enum CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    const-string v1, "AcceptAction"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->AcceptAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    .line 54
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    const-string v1, "CallUUID"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->AcceptAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 52
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptCall;

    return-object v0
.end method
