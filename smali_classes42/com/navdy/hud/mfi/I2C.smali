.class public Lcom/navdy/hud/mfi/I2C;
.super Ljava/lang/Object;
.source "I2C.java"


# static fields
.field public static final MAX_BLOCK_SIZE:I = 0x20


# instance fields
.field private mBus:I

.field private mFD:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "iap2"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 20
    invoke-static {}, Lcom/navdy/hud/mfi/I2C;->classInitNative()V

    .line 21
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "mBus"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/mfi/I2C;->mFD:I

    .line 37
    iput p1, p0, Lcom/navdy/hud/mfi/I2C;->mBus:I

    .line 38
    return-void
.end method

.method private static native classInitNative()V
.end method

.method private native native_close()V
.end method

.method private native native_open(I)I
.end method

.method private native native_read(II[BII)I
.end method

.method private native native_write(II[BII)I
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/navdy/hud/mfi/I2C;->mFD:I

    if-ltz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/navdy/hud/mfi/I2C;->native_close()V

    .line 51
    :cond_0
    return-void
.end method

.method public open()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/hud/mfi/I2C;->mBus:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/I2C;->native_open(I)I

    .line 42
    iget v0, p0, Lcom/navdy/hud/mfi/I2C;->mFD:I

    if-gez v0, :cond_0

    .line 43
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to open i2c device"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    return-void
.end method

.method public read(II[B)I
    .locals 6
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "bytes"    # [B

    .prologue
    .line 58
    const/4 v4, 0x0

    array-length v5, p3

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/mfi/I2C;->read(II[BII)I

    move-result v0

    return v0
.end method

.method public read(II[BII)I
    .locals 1
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "bytes"    # [B
    .param p4, "offset"    # I
    .param p5, "len"    # I

    .prologue
    .line 54
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/mfi/I2C;->native_read(II[BII)I

    move-result v0

    return v0
.end method

.method public readByte(II)I
    .locals 4
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v2, 0x1

    new-array v0, v2, [B

    .line 93
    .local v0, "bytes":[B
    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/hud/mfi/I2C;->read(II[B)I

    move-result v1

    .line 94
    .local v1, "success":I
    if-eqz v1, :cond_0

    .line 95
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unable to read byte"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :cond_0
    const/4 v2, 0x0

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    return v2
.end method

.method public readShort(II)I
    .locals 4
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v2, 0x2

    new-array v0, v2, [B

    .line 84
    .local v0, "bytes":[B
    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/hud/mfi/I2C;->read(II[B)I

    move-result v1

    .line 85
    .local v1, "success":I
    if-eqz v1, :cond_0

    .line 86
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unable to read short"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 88
    :cond_0
    const/4 v2, 0x0

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    return v2
.end method

.method public write(II[BI)I
    .locals 6
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "bytes"    # [B
    .param p4, "len"    # I

    .prologue
    .line 79
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/mfi/I2C;->native_write(II[BII)I

    move-result v0

    return v0
.end method

.method public write(II[BII)I
    .locals 6
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "bytes"    # [B
    .param p4, "offset"    # I
    .param p5, "len"    # I

    .prologue
    .line 75
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/mfi/I2C;->native_write(II[BII)I

    move-result v0

    return v0
.end method

.method public writeByte(III)I
    .locals 3
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "byteVal"    # I

    .prologue
    .line 69
    const/4 v1, 0x1

    new-array v0, v1, [B

    .line 70
    .local v0, "bytes":[B
    const/4 v1, 0x0

    int-to-byte v2, p3

    aput-byte v2, v0, v1

    .line 71
    array-length v1, v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/navdy/hud/mfi/I2C;->write(II[BI)I

    move-result v1

    return v1
.end method

.method public writeShort(III)I
    .locals 3
    .param p1, "deviceAddress"    # I
    .param p2, "registerAddress"    # I
    .param p3, "shortVal"    # I

    .prologue
    .line 62
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 63
    .local v0, "bytes":[B
    const/4 v1, 0x0

    shr-int/lit8 v2, p3, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 64
    const/4 v1, 0x1

    and-int/lit16 v2, p3, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 65
    array-length v1, v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/navdy/hud/mfi/I2C;->write(II[BI)I

    move-result v1

    return v1
.end method
