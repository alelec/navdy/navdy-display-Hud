.class public final enum Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;
.super Ljava/lang/Enum;
.source "NowPlayingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/NowPlayingUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackRepeat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

.field public static final enum All:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

.field public static final enum Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

.field public static final enum One:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    const-string v1, "Off"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 41
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    const-string v1, "One"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->One:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 42
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    const-string v1, "All"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->All:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->One:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->All:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

    return-object v0
.end method
