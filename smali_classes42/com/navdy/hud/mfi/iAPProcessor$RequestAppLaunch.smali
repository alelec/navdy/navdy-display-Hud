.class final enum Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RequestAppLaunch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

.field public static final enum AppBundleID:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

.field public static final enum LaunchAlert:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 282
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    const-string v1, "AppBundleID"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->AppBundleID:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    .line 283
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    const-string v1, "LaunchAlert"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->LaunchAlert:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    .line 281
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->AppBundleID:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->LaunchAlert:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 281
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 281
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;
    .locals 1

    .prologue
    .line 281
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    return-object v0
.end method
