.class public Lcom/navdy/hud/mfi/FileTransferSession;
.super Ljava/lang/Object;
.source "FileTransferSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
    }
.end annotation


# static fields
.field public static final DATA_OFFSET:I = 0x2

.field private static final INDEX_OF_COMMAND:I = 0x1

.field private static final INDEX_OF_ID:I = 0x0

.field public static final MESSAGE_SIZE:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCanceled:Z

.field private mFileData:[B

.field private mFileReceivedSoFar:I

.field private mFileSize:J

.field public mFileTransferIdentifier:I

.field mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

.field private mFinished:Z

.field private mStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/navdy/hud/mfi/FileTransferSession;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/FileTransferSession;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/navdy/hud/mfi/IIAPFileTransferManager;)V
    .locals 1
    .param p1, "mFileTransferIdentifier"    # I
    .param p2, "fileTransferManager"    # Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mStarted:Z

    .line 59
    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 60
    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mCanceled:Z

    .line 64
    iput-object p2, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    .line 65
    iput p1, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    .line 66
    return-void
.end method

.method private error(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 154
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v1, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-interface {v0, v1, p1}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onError(ILjava/lang/Throwable;)V

    .line 156
    return-void
.end method

.method private sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V
    .locals 4
    .param p1, "command"    # Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    .prologue
    .line 159
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 160
    .local v0, "message":[B
    const/4 v1, 0x0

    iget v2, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 161
    const/4 v1, 0x1

    iget v2, p1, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->value:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 162
    iget-object v1, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v2, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-interface {v1, v2, v0}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->sendMessage(I[B)V

    .line 163
    const-string v1, "MFi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Accessory(F): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Command : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    return-void
.end method


# virtual methods
.method public bProcessFileTransferMessage([B)V
    .locals 8
    .param p1, "data"    # [B

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 69
    aget-byte v3, p1, v5

    and-int/lit16 v1, v3, 0xff

    .line 70
    .local v1, "commandCode":I
    invoke-static {v1}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->fromInt(I)Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    move-result-object v0

    .line 71
    .local v0, "command":Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SETUP:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    if-eq v0, v3, :cond_1

    iget-boolean v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mStarted:Z

    if-nez v3, :cond_1

    .line 72
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File transfer is not initiated, bad state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "File transfer not started"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4, v5}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onError(ILjava/lang/Throwable;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$1;->$SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand:[I

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 78
    :pswitch_0
    iput-boolean v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mStarted:Z

    .line 79
    invoke-static {p1, v6}, Lcom/navdy/hud/mfi/Utils;->unpackInt64([BI)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    .line 80
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", File_Size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-wide v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    invoke-interface {v3}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->getFileTransferLimit()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 82
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession;->TAG:Ljava/lang/String;

    const-string v4, "File too large, not starting the transfer"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    goto :goto_0

    .line 86
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    iget-wide v6, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    invoke-interface {v3, v4, v6, v7}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onFileTransferSetupRequest(IJ)V

    .line 87
    iget-wide v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 88
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 89
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onSuccess(I[B)V

    goto :goto_0

    .line 94
    :pswitch_1
    const/4 v3, 0x2

    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v4, p1

    add-int/lit8 v4, v4, -0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    .line 96
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 97
    :catch_0
    move-exception v2

    .line 98
    .local v2, "t":Ljava/lang/Throwable;
    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/FileTransferSession;->error(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 103
    .end local v2    # "t":Ljava/lang/Throwable;
    :pswitch_2
    const/4 v3, 0x2

    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v4, p1

    add-int/lit8 v4, v4, -0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    .line 105
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SUCCESS:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 107
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 108
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    iget-object v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    invoke-interface {v3, v4, v5}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onSuccess(I[B)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 109
    :catch_1
    move-exception v2

    .line 110
    .restart local v2    # "t":Ljava/lang/Throwable;
    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/FileTransferSession;->error(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 115
    .end local v2    # "t":Ljava/lang/Throwable;
    :pswitch_3
    const/4 v3, 0x2

    :try_start_2
    iget-object v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v4, p1

    add-int/lit8 v4, v4, -0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    .line 117
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 118
    :catch_2
    move-exception v2

    .line 119
    .restart local v2    # "t":Ljava/lang/Throwable;
    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/FileTransferSession;->error(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 124
    .end local v2    # "t":Ljava/lang/Throwable;
    :pswitch_4
    const/4 v3, 0x2

    :try_start_3
    iget-object v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    array-length v4, p1

    add-int/lit8 v4, v4, -0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    .line 126
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileReceivedSoFar:I

    int-to-long v4, v3

    iget-wide v6, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 128
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_FAILURE:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 129
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 130
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    new-instance v5, Ljava/io/IOException;

    const-string v6, "Missed packets"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4, v5}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onError(ILjava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 136
    :catch_3
    move-exception v2

    .line 137
    .restart local v2    # "t":Ljava/lang/Throwable;
    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/FileTransferSession;->error(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 132
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_3
    :try_start_4
    sget-object v3, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_SUCCESS:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 133
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 134
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    iget-object v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    invoke-interface {v3, v4, v5}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onSuccess(I[B)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 141
    :pswitch_5
    iput-boolean v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    .line 142
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onCanceled(I)V

    goto/16 :goto_0

    .line 146
    :pswitch_6
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Apple Device(F): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v3, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget v4, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->onPaused(I)V

    goto/16 :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public declared-synchronized cancel()V
    .locals 1

    .prologue
    .line 171
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mCanceled:Z

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    if-eqz v0, :cond_0

    .line 173
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_CANCEL:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFinished:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized proceed()V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mCanceled:Z

    if-nez v0, :cond_0

    .line 180
    iget-wide v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileSize:J

    long-to-int v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileData:[B

    .line 181
    iget-object v0, p0, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    if-eqz v0, :cond_0

    .line 182
    sget-object v0, Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;->COMMAND_START:Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/FileTransferSession;->sendCommand(Lcom/navdy/hud/mfi/FileTransferSession$FileTransferCommand;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :cond_0
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method
