.class public final enum Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;
.super Ljava/lang/Enum;
.source "NowPlayingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/NowPlayingUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackShuffle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

.field public static final enum Albums:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

.field public static final enum Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

.field public static final enum Songs:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    const-string v1, "Off"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 35
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    const-string v1, "Songs"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Songs:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 36
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    const-string v1, "Albums"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Albums:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Off:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Songs:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->Albums:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;

    return-object v0
.end method
