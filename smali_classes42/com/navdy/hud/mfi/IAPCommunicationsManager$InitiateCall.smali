.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "InitiateCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

.field public static final enum AddressBookId:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

.field public static final enum DestinationID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

.field public static final enum Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

.field public static final enum Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    const-string v1, "Type"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    .line 47
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    const-string v1, "DestinationID"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->DestinationID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    .line 48
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    const-string v1, "Service"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    .line 49
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    const-string v1, "AddressBookId"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->AddressBookId:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    .line 45
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Type:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->DestinationID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->AddressBookId:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCall;

    return-object v0
.end method
