.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "EndCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

.field public static final enum CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

.field public static final enum EndAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    const-string v1, "EndAction"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->EndAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    .line 59
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    const-string v1, "CallUUID"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->EndAction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCall;

    return-object v0
.end method
