.class final enum Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "HIDComponentFunction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum AssistiveSwitchControl:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum AssistiveTouchPointer:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum ExtendedGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum ExtendedGamepadNonFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum Headphone:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum Keyboard:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum MediaPlaybackRemote:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

.field public static final enum StandardGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 340
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "Keyboard"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->Keyboard:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 341
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "MediaPlaybackRemote"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->MediaPlaybackRemote:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 342
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "AssistiveTouchPointer"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->AssistiveTouchPointer:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 343
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "StandardGamepadFormFitting"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->StandardGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 344
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "ExtendedGamepadFormFitting"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->ExtendedGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 345
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "ExtendedGamepadNonFormFitting"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->ExtendedGamepadNonFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 346
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "AssistiveSwitchControl"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->AssistiveSwitchControl:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 347
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    const-string v1, "Headphone"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->Headphone:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 339
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->Keyboard:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->MediaPlaybackRemote:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->AssistiveTouchPointer:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->StandardGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->ExtendedGamepadFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->ExtendedGamepadNonFormFitting:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->AssistiveSwitchControl:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->Headphone:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 339
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    return-object v0
.end method
