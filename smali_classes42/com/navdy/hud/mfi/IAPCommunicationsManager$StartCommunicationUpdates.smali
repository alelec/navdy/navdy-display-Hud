.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "StartCommunicationUpdates"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

.field public static final enum MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;


# instance fields
.field id:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    const-string v1, "MuteStatus"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    .line 89
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->MuteStatus:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput p3, p0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->id:I

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 89
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCommunicationUpdates;

    return-object v0
.end method
