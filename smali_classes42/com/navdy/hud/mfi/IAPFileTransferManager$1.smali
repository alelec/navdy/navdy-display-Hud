.class Lcom/navdy/hud/mfi/IAPFileTransferManager$1;
.super Ljava/lang/Object;
.source "IAPFileTransferManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/mfi/IAPFileTransferManager;->queue([B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

.field final synthetic val$data:[B


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/IAPFileTransferManager;[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/IAPFileTransferManager;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    iput-object p2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->val$data:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 43
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->val$data:[B

    if-eqz v2, :cond_1

    .line 44
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->val$data:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit16 v0, v2, 0xff

    .line 45
    .local v0, "fileTransferIdentifier":I
    const/4 v1, 0x0

    .line 46
    .local v1, "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$000(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    check-cast v1, Lcom/navdy/hud/mfi/FileTransferSession;

    .restart local v1    # "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    if-nez v1, :cond_0

    .line 47
    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Creating a new file transfer session "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v1, Lcom/navdy/hud/mfi/FileTransferSession;

    .end local v1    # "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-direct {v1, v0, v2}, Lcom/navdy/hud/mfi/FileTransferSession;-><init>(ILcom/navdy/hud/mfi/IIAPFileTransferManager;)V

    .line 49
    .restart local v1    # "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$000(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;->val$data:[B

    invoke-virtual {v1, v2}, Lcom/navdy/hud/mfi/FileTransferSession;->bProcessFileTransferMessage([B)V

    .line 53
    .end local v0    # "fileTransferIdentifier":I
    .end local v1    # "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    :cond_1
    return-void
.end method
