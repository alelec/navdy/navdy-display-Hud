.class Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
.super Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IAP2SessionMessage"
.end annotation


# instance fields
.field msgId:I


# direct methods
.method constructor <init>(I)V
    .locals 3
    .param p1, "msgId"    # I

    .prologue
    .line 611
    invoke-direct {p0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    .line 612
    iput p1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->msgId:I

    .line 614
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->dos:Ljava/io/DataOutputStream;

    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->msgStart:[B
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$500()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 616
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->dos:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 617
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 621
    :goto_0
    return-void

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "ie":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception while creating message "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V
    .locals 1
    .param p1, "msgId"    # Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .prologue
    .line 624
    iget v0, p1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(I)V

    .line 625
    return-void
.end method


# virtual methods
.method toBytes()[B
    .locals 3

    .prologue
    .line 628
    invoke-super {p0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v0

    .line 630
    .local v0, "bytes":[B
    const/4 v1, 0x2

    array-length v2, v0

    invoke-static {v2}, Lcom/navdy/hud/mfi/Utils;->packUInt16(I)[B

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->copy([BI[B)V

    .line 631
    return-object v0
.end method
