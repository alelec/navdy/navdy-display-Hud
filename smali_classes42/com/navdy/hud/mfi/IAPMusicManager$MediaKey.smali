.class public final enum Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;
.super Ljava/lang/Enum;
.source "IAPMusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPMusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

.field public static final enum Next:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

.field public static final enum PlayPause:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

.field public static final enum Previous:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

.field public static final enum Siri:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 224
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    const-string v1, "Siri"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Siri:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .line 225
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    const-string v1, "PlayPause"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->PlayPause:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .line 226
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    const-string v1, "Next"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Next:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .line 227
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    const-string v1, "Previous"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Previous:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    .line 223
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Siri:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->PlayPause:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Next:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->Previous:Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 223
    const-class v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaKey;

    return-object v0
.end method
