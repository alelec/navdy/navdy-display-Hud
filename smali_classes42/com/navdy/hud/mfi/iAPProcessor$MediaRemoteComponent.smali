.class public final enum Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaRemoteComponent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

.field public static final enum HIDKeyboard:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

.field public static final enum HIDMediaRemote:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 77
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    const-string v1, "HIDKeyboard"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDKeyboard:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    .line 78
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    const-string v1, "HIDMediaRemote"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDMediaRemote:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    .line 76
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDKeyboard:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDMediaRemote:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    return-object v0
.end method
