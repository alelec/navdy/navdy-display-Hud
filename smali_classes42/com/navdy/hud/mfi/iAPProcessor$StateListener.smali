.class public interface abstract Lcom/navdy/hud/mfi/iAPProcessor$StateListener;
.super Ljava/lang/Object;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StateListener"
.end annotation


# virtual methods
.method public abstract onError()V
.end method

.method public abstract onReady()V
.end method

.method public abstract onSessionStart(Lcom/navdy/hud/mfi/EASession;)V
.end method

.method public abstract onSessionStop(Lcom/navdy/hud/mfi/EASession;)V
.end method
