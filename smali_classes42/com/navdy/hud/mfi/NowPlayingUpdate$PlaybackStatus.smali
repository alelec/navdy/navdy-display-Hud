.class public final enum Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;
.super Ljava/lang/Enum;
.source "NowPlayingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/NowPlayingUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public static final enum Paused:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public static final enum Playing:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public static final enum SeekBackward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public static final enum SeekForward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public static final enum Stopped:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    const-string v1, "Stopped"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Stopped:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 27
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    const-string v1, "Playing"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Playing:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 28
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    const-string v1, "Paused"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Paused:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 29
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    const-string v1, "SeekForward"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekForward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 30
    new-instance v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    const-string v1, "SeekBackward"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekBackward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Stopped:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Playing:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->Paused:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekForward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->SeekBackward:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->$VALUES:[Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    return-object v0
.end method
