.class Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
.super Ljava/lang/Object;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IAP2ParamsCreator"
.end annotation


# static fields
.field private static final META_DATA_LENGTH:I = 0x4

.field public static final SIZE_OF_LENGTH:I = 0x2

.field public static final SIZE_OF_PARAM_ID:I = 0x2


# instance fields
.field dos:Ljava/io/DataOutputStream;

.field params:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->params:Ljava/io/ByteArrayOutputStream;

    .line 497
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->params:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    .line 498
    return-void
.end method


# virtual methods
.method addBlob(I[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 4
    .param p1, "paramId"    # I
    .param p2, "blob"    # [B

    .prologue
    .line 542
    :try_start_0
    array-length v2, p2

    add-int/lit8 v1, v2, 0x4

    .line 543
    .local v1, "length":I
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 544
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 545
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 549
    .end local v1    # "length":I
    :goto_0
    return-object p0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception while creating message "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "blob"    # [B

    .prologue
    .line 595
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(I[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addBoolean(IZ)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # I
    .param p2, "val"    # Z

    .prologue
    .line 505
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method addBoolean(Ljava/lang/Enum;Z)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 2
    .param p1, "param"    # Ljava/lang/Enum;
    .param p2, "val"    # Z

    .prologue
    .line 509
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method addEnum(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # I
    .param p2, "b"    # I

    .prologue
    .line 537
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addEnum(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "b"    # I

    .prologue
    .line 587
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 2
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "b"    # Ljava/lang/Enum;

    .prologue
    .line 591
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addNone(I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 4
    .param p1, "paramId"    # I

    .prologue
    .line 569
    const/4 v1, 0x4

    .line 570
    .local v1, "length":I
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 571
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :goto_0
    return-object p0

    .line 572
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception while creating message "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;

    .prologue
    .line 603
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addString(ILjava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 7
    .param p1, "paramId"    # I
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 554
    :try_start_0
    const-string v3, "UTF-8"

    invoke-virtual {p2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 555
    .local v0, "data":[B
    array-length v3, v0

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v2, v3, 0x1

    .line 556
    .local v2, "length":I
    iget-object v3, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 557
    iget-object v3, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 558
    iget-object v3, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 560
    iget-object v3, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    const/4 v4, 0x1

    new-array v4, v4, [B

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput-byte v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    .end local v0    # "data":[B
    .end local v2    # "length":I
    :goto_0
    return-object p0

    .line 561
    :catch_0
    move-exception v1

    .line 562
    .local v1, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Exception while creating message "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 599
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(ILjava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addUInt16(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 4
    .param p1, "paramId"    # I
    .param p2, "i"    # I

    .prologue
    .line 526
    const/4 v1, 0x6

    .line 527
    .local v1, "length":I
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 528
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 529
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    return-object p0

    .line 530
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception while creating message "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "i"    # I

    .prologue
    .line 583
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method addUInt8(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 4
    .param p1, "paramId"    # I
    .param p2, "b"    # I

    .prologue
    .line 514
    const/4 v1, 0x5

    .line 515
    .local v1, "length":I
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 516
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 517
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    :goto_0
    return-object p0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception while creating message "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method addUInt8(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;
    .locals 1
    .param p1, "paramId"    # Ljava/lang/Enum;
    .param p2, "b"    # I

    .prologue
    .line 579
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(II)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v0

    return-object v0
.end method

.method toBytes()[B
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->params:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
