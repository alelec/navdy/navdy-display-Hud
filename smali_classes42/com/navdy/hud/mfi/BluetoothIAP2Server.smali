.class public Lcom/navdy/hud/mfi/BluetoothIAP2Server;
.super Ljava/lang/Object;
.source "BluetoothIAP2Server.java"

# interfaces
.implements Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;,
        Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;,
        Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;
    }
.end annotation


# static fields
.field public static final ACCESSORY_IAP2:Ljava/util/UUID;

.field public static final DEVICE_IAP2:Ljava/util/UUID;

.field private static final SDP_NAME:Ljava/lang/String; = "Wireless iAP"

.field public static final STATE_CONNECTED:I = 0x3

.field public static final STATE_CONNECTING:I = 0x2

.field public static final STATE_LISTEN:I = 0x1

.field public static final STATE_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BluetoothIAP2Server"


# instance fields
.field private linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

.field private mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

.field private mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

.field private final mHandler:Landroid/os/Handler;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "00000000-deca-fade-deca-deafdecacafe"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->DEVICE_IAP2:Ljava/util/UUID;

    .line 29
    const-string v0, "00000000-deca-fade-deca-deafdecacaff"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->ACCESSORY_IAP2:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I

    .line 53
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    .prologue
    .line 25
    iget v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connectionFailed()V

    return-void
.end method

.method static synthetic access$302(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;)Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;
    .param p1, "x1"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Lcom/navdy/hud/mfi/LinkLayer;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connectionLost()V

    return-void
.end method

.method private connectionFailed()V
    .locals 4

    .prologue
    .line 215
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 216
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 217
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "toast"

    const-string v3, "Unable to connect device"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 219
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 222
    invoke-virtual {p0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->start()V

    .line 224
    return-void
.end method

.method private connectionLost()V
    .locals 6

    .prologue
    .line 230
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/LinkLayer;->connectionEnded()V

    .line 233
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 234
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 235
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "toast"

    const-string v3, "Device connection was lost"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 237
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/hud/mfi/BluetoothIAP2Server$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$1;-><init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 247
    return-void
.end method

.method private declared-synchronized setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    const-string v0, "BluetoothIAP2Server"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iput p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    monitor-exit p0

    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized connect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    const-string v0, "BluetoothIAP2Server"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connect to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->cancel()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->cancel()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    .line 135
    :cond_1
    new-instance v0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;-><init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Landroid/bluetooth/BluetoothDevice;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->start()V

    .line 137
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public connect(Lcom/navdy/hud/mfi/LinkLayer;)V
    .locals 0
    .param p1, "linkLayer"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    .line 65
    return-void
.end method

.method public declared-synchronized connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p1, "socket"    # Landroid/bluetooth/BluetoothSocket;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "connected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->cancel()V

    .line 151
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    if-eqz v2, :cond_1

    .line 156
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->cancel()V

    .line 157
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    .line 161
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    if-eqz v2, :cond_2

    .line 162
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->cancel()V

    .line 163
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    .line 167
    :cond_2
    new-instance v2, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;-><init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Landroid/bluetooth/BluetoothSocket;)V

    iput-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    .line 168
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->start()V

    .line 171
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 172
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "device_name"

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 175
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 177
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    monitor-exit p0

    return-void

    .line 146
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getLocalAddress()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/mfi/Utils;->parseMACAddress(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public queue(Lcom/navdy/hud/mfi/LinkPacket;)V
    .locals 1
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/LinkPacket;

    .prologue
    .line 491
    iget-object v0, p1, Lcom/navdy/hud/mfi/LinkPacket;->data:[B

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->write([B)V

    .line 492
    return-void
.end method

.method public declared-synchronized reconfirmDiscoverableState(Landroid/app/Activity;)V
    .locals 4
    .param p1, "intentContext"    # Landroid/app/Activity;

    .prologue
    .line 475
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 477
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 479
    .local v0, "discoverableIntent":Landroid/content/Intent;
    const-string v2, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    const/16 v3, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 480
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 481
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Now Discoverable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    .end local v0    # "discoverableIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :catch_0
    move-exception v1

    .line 484
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Failed to make discoverable"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 475
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized start()V
    .locals 2

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    const-string v0, "BluetoothIAP2Server"

    const-string v1, "start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->setState(I)V

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    invoke-direct {v0, p0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;-><init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :cond_0
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 2

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    const-string v0, "BluetoothIAP2Server"

    const-string v1, "stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->cancel()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->cancel()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->cancel()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAcceptThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;

    .line 111
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public write([B)V
    .locals 3
    .param p1, "out"    # [B

    .prologue
    .line 202
    monitor-enter p0

    .line 203
    :try_start_0
    iget v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    monitor-exit p0

    .line 208
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectedThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;

    .line 205
    .local v0, "r":Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    invoke-virtual {v0, p1}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->write([B)V

    goto :goto_0

    .line 205
    .end local v0    # "r":Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
