.class public Lcom/navdy/hud/mfi/EASession;
.super Ljava/lang/Object;
.source "EASession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/EASession$OutStream;,
        Lcom/navdy/hud/mfi/EASession$BlockingInputStream;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private buffer:Lokio/Buffer;

.field private closed:Z

.field private final dataAvailable:Ljava/lang/Object;

.field private iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

.field private macAddress:Ljava/lang/String;

.field private mySelf:Lcom/navdy/hud/mfi/EASession;

.field private name:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private sessionIdentifier:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 5
    .param p1, "iAPProcessor"    # Lcom/navdy/hud/mfi/iAPProcessor;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "sessionIdentifier"    # I
    .param p5, "protocol"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    .line 54
    iput-object p0, p0, Lcom/navdy/hud/mfi/EASession;->mySelf:Lcom/navdy/hud/mfi/EASession;

    .line 55
    iput-object p1, p0, Lcom/navdy/hud/mfi/EASession;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    .line 56
    iput-object p2, p0, Lcom/navdy/hud/mfi/EASession;->macAddress:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/navdy/hud/mfi/EASession;->name:Ljava/lang/String;

    .line 58
    iput p4, p0, Lcom/navdy/hud/mfi/EASession;->sessionIdentifier:I

    .line 59
    iput-object p5, p0, Lcom/navdy/hud/mfi/EASession;->protocol:Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    const-string v1, "%s: created"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/mfi/EASession;->mySelf:Lcom/navdy/hud/mfi/EASession;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/EASession;->buffer:Lokio/Buffer;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/mfi/EASession;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/mfi/EASession;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/navdy/hud/mfi/EASession;->closed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/mfi/EASession;)Lcom/navdy/hud/mfi/iAPProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/mfi/EASession;)Lcom/navdy/hud/mfi/EASession;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->mySelf:Lcom/navdy/hud/mfi/EASession;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    const-string v1, "Closing EASession streams"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/hud/mfi/EASession;->closed:Z

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->buffer:Lokio/Buffer;

    invoke-virtual {v0}, Lokio/Buffer;->close()V

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 89
    monitor-exit v1

    .line 90
    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;

    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession;->buffer:Lokio/Buffer;

    invoke-virtual {v1}, Lokio/Buffer;->inputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/mfi/EASession$BlockingInputStream;-><init>(Lcom/navdy/hud/mfi/EASession;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/navdy/hud/mfi/EASession$OutStream;

    invoke-direct {v0, p0}, Lcom/navdy/hud/mfi/EASession$OutStream;-><init>(Lcom/navdy/hud/mfi/EASession;)V

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionIdentifier()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/navdy/hud/mfi/EASession;->sessionIdentifier:I

    return v0
.end method

.method public queue([B)V
    .locals 7
    .param p1, "data"    # [B

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 93
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/navdy/hud/mfi/EASession;->TAG:Ljava/lang/String;

    const-string v1, "%s: buffer.write: %d bytes: %s, \'%s\'"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/mfi/EASession;->mySelf:Lcom/navdy/hud/mfi/EASession;

    aput-object v4, v2, v3

    array-length v3, p1

    .line 96
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {p1, v5}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p1}, Lcom/navdy/hud/mfi/Utils;->toASCII([B)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 94
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->buffer:Lokio/Buffer;

    invoke-virtual {v0, p1}, Lokio/Buffer;->write([B)Lokio/Buffer;

    .line 100
    iget-object v0, p0, Lcom/navdy/hud/mfi/EASession;->dataAvailable:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 101
    monitor-exit v1

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 36
    const-string v0, "%s{%s/%d}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/mfi/EASession;->protocol:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/navdy/hud/mfi/EASession;->sessionIdentifier:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
