.class public abstract Lcom/navdy/hud/mfi/IAPControlMessageProcessor;
.super Ljava/lang/Object;
.source "IAPControlMessageProcessor.java"


# instance fields
.field protected mSerialExecutor:Ljava/util/concurrent/ExecutorService;

.field protected miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;


# direct methods
.method public constructor <init>([Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 4
    .param p1, "messages"    # [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .param p2, "processor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    .line 17
    iput-object p2, p0, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    .line 18
    if-eqz p1, :cond_0

    .line 19
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 20
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    iget-object v3, p0, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;->miAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v3, v0, p0}, Lcom/navdy/hud/mfi/iAPProcessor;->registerControlMessageProcessor(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;Lcom/navdy/hud/mfi/IAPControlMessageProcessor;)V

    .line 19
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 23
    .end local v0    # "message":Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract bProcessControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V
.end method

.method public processControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V
    .locals 2
    .param p1, "message"    # Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .param p2, "session"    # I
    .param p3, "data"    # [B

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/navdy/hud/mfi/IAPControlMessageProcessor$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/mfi/IAPControlMessageProcessor$1;-><init>(Lcom/navdy/hud/mfi/IAPControlMessageProcessor;Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 31
    return-void
.end method
