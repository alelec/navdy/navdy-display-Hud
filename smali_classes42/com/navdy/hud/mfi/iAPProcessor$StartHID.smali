.class final enum Lcom/navdy/hud/mfi/iAPProcessor$StartHID;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "StartHID"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$StartHID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

.field public static final enum HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

.field public static final enum HIDReportDescriptor:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

.field public static final enum LocalizedKeyboardCountryCode:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

.field public static final enum ProductIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

.field public static final enum VendorIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 351
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const-string v1, "HIDComponentIdentifier"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 352
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const-string v1, "VendorIdentifier"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->VendorIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 353
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const-string v1, "ProductIdentifier"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->ProductIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 354
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const-string v1, "LocalizedKeyboardCountryCode"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->LocalizedKeyboardCountryCode:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 355
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const-string v1, "HIDReportDescriptor"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDReportDescriptor:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 350
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->VendorIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->ProductIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->LocalizedKeyboardCountryCode:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDReportDescriptor:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$StartHID;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 350
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$StartHID;
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    return-object v0
.end method
