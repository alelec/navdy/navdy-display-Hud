.class public interface abstract Lcom/navdy/hud/mfi/IIAPFileTransferManager;
.super Ljava/lang/Object;
.source "IIAPFileTransferManager.java"


# static fields
.field public static final FILE_TRANSFER_SESSION_ID:I = 0x2


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract clear()V
.end method

.method public abstract getFileTransferLimit()J
.end method

.method public abstract onCanceled(I)V
.end method

.method public abstract onError(ILjava/lang/Throwable;)V
.end method

.method public abstract onFileTransferSetupRequest(IJ)V
.end method

.method public abstract onFileTransferSetupResponse(IZ)V
.end method

.method public abstract onPaused(I)V
.end method

.method public abstract onSuccess(I[B)V
.end method

.method public abstract queue([B)V
.end method

.method public abstract sendMessage(I[B)V
.end method

.method public abstract setFileTransferLimit(I)V
.end method
