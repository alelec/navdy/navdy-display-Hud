.class Lcom/navdy/hud/mfi/iAPProcessor$1;
.super Landroid/os/Handler;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/mfi/iAPProcessor;


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/iAPProcessor;
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 176
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 178
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/mfi/SessionPacket;

    .line 179
    .local v0, "smsg":Lcom/navdy/hud/mfi/SessionPacket;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    # invokes: Lcom/navdy/hud/mfi/iAPProcessor;->handleSessionPacket(Lcom/navdy/hud/mfi/SessionPacket;)V
    invoke-static {v1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->access$000(Lcom/navdy/hud/mfi/iAPProcessor;Lcom/navdy/hud/mfi/SessionPacket;)V

    goto :goto_0

    .line 182
    .end local v0    # "smsg":Lcom/navdy/hud/mfi/SessionPacket;
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    # invokes: Lcom/navdy/hud/mfi/iAPProcessor;->handleLinkLoss()V
    invoke-static {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->access$100(Lcom/navdy/hud/mfi/iAPProcessor;)V

    goto :goto_0

    .line 185
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    # invokes: Lcom/navdy/hud/mfi/iAPProcessor;->retryDeviceAuthentication()V
    invoke-static {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->access$200(Lcom/navdy/hud/mfi/iAPProcessor;)V

    goto :goto_0

    .line 188
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;
    invoke-static {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->access$300(Lcom/navdy/hud/mfi/iAPProcessor;)Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189
    const-string v1, "MFi"

    const-string v2, "Skipping Device Authentication as we did not get the certificate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$1;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;
    invoke-static {v1}, Lcom/navdy/hud/mfi/iAPProcessor;->access$300(Lcom/navdy/hud/mfi/iAPProcessor;)Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onReady()V

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
