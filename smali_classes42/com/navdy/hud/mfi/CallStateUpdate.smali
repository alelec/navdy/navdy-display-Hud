.class public Lcom/navdy/hud/mfi/CallStateUpdate;
.super Ljava/lang/Object;
.source "CallStateUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;,
        Lcom/navdy/hud/mfi/CallStateUpdate$Direction;,
        Lcom/navdy/hud/mfi/CallStateUpdate$Status;
    }
.end annotation


# instance fields
.field public addressBookID:Ljava/lang/String;

.field public callUUID:Ljava/lang/String;

.field public conferenceGroup:I

.field public direction:Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

.field public disconnectReason:Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

.field public displayName:Ljava/lang/String;

.field public isConferenced:Z

.field public label:Ljava/lang/String;

.field public remoteID:Ljava/lang/String;

.field public service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

.field public status:Lcom/navdy/hud/mfi/CallStateUpdate$Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Disconnected:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    iput-object v0, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->status:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 19
    sget-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Direction;->Unknown:Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

    iput-object v0, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->direction:Lcom/navdy/hud/mfi/CallStateUpdate$Direction;

    .line 34
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;->Unknown:Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    iput-object v0, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$Service;

    .line 37
    sget-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;->Ended:Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

    iput-object v0, p0, Lcom/navdy/hud/mfi/CallStateUpdate;->disconnectReason:Lcom/navdy/hud/mfi/CallStateUpdate$DisconnectReason;

    return-void
.end method
