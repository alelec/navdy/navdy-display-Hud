.class final enum Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "iAP2HIDComponent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

.field public static final enum HIDComponentFunction:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

.field public static final enum HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

.field public static final enum HIDComponentName:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 334
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    const-string v1, "HIDComponentIdentifier"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    .line 335
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    const-string v1, "HIDComponentName"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentName:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    .line 336
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    const-string v1, "HIDComponentFunction"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentFunction:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    .line 333
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentName:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentFunction:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 333
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 333
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    return-object v0
.end method
