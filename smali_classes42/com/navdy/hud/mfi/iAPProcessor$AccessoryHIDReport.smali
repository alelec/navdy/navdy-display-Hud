.class final enum Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AccessoryHIDReport"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

.field public static final enum HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

.field public static final enum HIDReport:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 359
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    const-string v1, "HIDComponentIdentifier"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    .line 360
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    const-string v1, "HIDReport"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDReport:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    .line 358
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDReport:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 358
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;
    .locals 1

    .prologue
    .line 358
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    return-object v0
.end method
