.class public Lcom/navdy/hud/mfi/NowPlayingUpdate;
.super Ljava/lang/Object;
.source "NowPlayingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;,
        Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;,
        Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;
    }
.end annotation


# instance fields
.field public mAppBundleId:Ljava/lang/String;

.field public mAppName:Ljava/lang/String;

.field public mPlaybackElapsedTimeMilliseconds:J

.field public mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

.field public mediaItemAlbumTitle:Ljava/lang/String;

.field public mediaItemAlbumTrackCount:I

.field public mediaItemAlbumTrackNumber:I

.field public mediaItemArtist:Ljava/lang/String;

.field public mediaItemArtworkFileTransferIdentifier:I

.field public mediaItemGenre:Ljava/lang/String;

.field public mediaItemPersistentIdentifier:Ljava/math/BigInteger;

.field public mediaItemPlaybackDurationInMilliseconds:J

.field public mediaItemTitle:Ljava/lang/String;

.field public playbackRepeat:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackRepeat;

.field public playbackShuffle:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackShuffle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x27

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NowPlayingUpdate{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 51
    :cond_0
    iget-wide v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 52
    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemPlaybackDurationInMilliseconds:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 55
    const-string v1, ", albumTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 57
    :cond_2
    iget v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    if-eqz v1, :cond_3

    .line 58
    const-string v1, ", albumTrackNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    :cond_3
    iget v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    if-eqz v1, :cond_4

    .line 61
    const-string v1, ", albumTrackCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemAlbumTrackCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 63
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 64
    const-string v1, ", artist=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 66
    :cond_5
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 67
    const-string v1, ", itemGenre=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemGenre:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    :cond_6
    iget v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    if-eqz v1, :cond_7

    .line 70
    const-string v1, ", fileTransferIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mediaItemArtworkFileTransferIdentifier:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    :cond_7
    iget-object v1, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    if-eqz v1, :cond_8

    .line 73
    const-string v1, ", playbackStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackStatus:Lcom/navdy/hud/mfi/NowPlayingUpdate$PlaybackStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    :cond_8
    const-string v1, ", playbackElapsedTimeMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/mfi/NowPlayingUpdate;->mPlaybackElapsedTimeMilliseconds:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 76
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
