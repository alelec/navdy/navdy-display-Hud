.class Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;
.super Ljava/lang/Thread;
.source "BluetoothIAP2Server.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/BluetoothIAP2Server;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcceptThread"
.end annotation


# instance fields
.field private final mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field final synthetic this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V
    .locals 5

    .prologue
    .line 258
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 259
    const/4 v1, 0x0

    .line 263
    .local v1, "tmp":Landroid/bluetooth/BluetoothServerSocket;
    :try_start_0
    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {p1}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$000(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    const-string v3, "Wireless iAP"

    sget-object v4, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->ACCESSORY_IAP2:Ljava/util/UUID;

    invoke-virtual {v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 267
    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    .line 268
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Socket listen() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 321
    const-string v1, "BluetoothIAP2Server"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Socket cancel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BluetoothIAP2Server"

    const-string v2, "Socket close() of bluetoothServer failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 271
    const-string v2, "BluetoothIAP2Server"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Socket BEGIN mAcceptThread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const-string v2, "AcceptThread"

    invoke-virtual {p0, v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->setName(Ljava/lang/String;)V

    .line 276
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-nez v2, :cond_0

    .line 277
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "No socket (maybe BT not enabled?) - END mAcceptThread"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :goto_0
    return-void

    .line 282
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I
    invoke-static {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$100(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 286
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->mmServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 292
    .local v1, "socket":Landroid/bluetooth/BluetoothSocket;
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Socket accepted"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    if-eqz v1, :cond_0

    .line 296
    iget-object v3, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    monitor-enter v3

    .line 297
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mState:I
    invoke-static {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$100(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 313
    :goto_2
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 287
    .end local v1    # "socket":Landroid/bluetooth/BluetoothSocket;
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Socket accept() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 316
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "END mAcceptThread"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    .restart local v1    # "socket":Landroid/bluetooth/BluetoothSocket;
    :pswitch_0
    :try_start_2
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$AcceptThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 307
    :pswitch_1
    :try_start_3
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 308
    :catch_1
    move-exception v0

    .line 309
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v2, "BluetoothIAP2Server"

    const-string v4, "Could not close unwanted socket"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
