.class public final enum Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;
.super Ljava/lang/Enum;
.source "IAPMusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPMusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NowPlayingUpdatesParameters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

.field public static final enum MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

.field public static final enum PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    const-string v1, "MediaItemAttributes"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    .line 66
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    const-string v1, "PlaybackAttributes"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->MediaItemAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->PlaybackAttributes:Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPMusicManager$NowPlayingUpdatesParameters;

    return-object v0
.end method
