.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "StartCallStateUpdates"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum Skip:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

.field public static final enum Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "RemoteID"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 32
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "DisplayName"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 33
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "Status"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 34
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "Direction"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 35
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "CallUUID"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 36
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "Skip"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Skip:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 37
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "AddressBookID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 38
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "Label"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 39
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "Service"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 40
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "IsConferenced"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 41
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "ConferenceGroup"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 42
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    const-string v1, "DisconnectReason"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    .line 30
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->RemoteID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisplayName:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Status:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Direction:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->CallUUID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Skip:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->AddressBookID:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Label:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->Service:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->IsConferenced:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->ConferenceGroup:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->DisconnectReason:Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$StartCallStateUpdates;

    return-object v0
.end method
