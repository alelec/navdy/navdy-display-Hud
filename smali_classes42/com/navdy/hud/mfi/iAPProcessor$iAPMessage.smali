.class final enum Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "iAPMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AccessoryHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum AuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum CancelIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum CommunicationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum DeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum DeviceAuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum DeviceAuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum DeviceAuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum DeviceHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum HoldStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum IdentificationAccepted:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum IdentificationInformation:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum IdentificationInformationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum IdentificationRejected:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum ListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum NowPlayingUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum RequestAppLaunch:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum RequestAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum RequestAuthenticationRequest:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum RequestDeviceAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartListUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StartNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StatusExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field public static final enum StopNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;


# instance fields
.field id:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 220
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "RequestAuthenticationRequest"

    const v2, 0xaa00

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAuthenticationRequest:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 221
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AuthenticationCertificate"

    const v2, 0xaa01

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 222
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "RequestAuthenticationChallengeResponse"

    const v2, 0xaa02

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 223
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AuthenticationResponse"

    const v2, 0xaa03

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 224
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AuthenticationFailed"

    const v2, 0xaa04

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 225
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AuthenticationSucceeded"

    const/4 v2, 0x5

    const v3, 0xaa05

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 227
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartIdentification"

    const/4 v2, 0x6

    const/16 v3, 0x1d00

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 228
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "IdentificationInformation"

    const/4 v2, 0x7

    const/16 v3, 0x1d01

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationInformation:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 229
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "IdentificationAccepted"

    const/16 v2, 0x8

    const/16 v3, 0x1d02

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationAccepted:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 230
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "IdentificationRejected"

    const/16 v2, 0x9

    const/16 v3, 0x1d03

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationRejected:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 231
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "CancelIdentification"

    const/16 v2, 0xa

    const/16 v3, 0x1d04

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CancelIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 232
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "IdentificationInformationUpdate"

    const/16 v2, 0xb

    const/16 v3, 0x1d05

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationInformationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 234
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartExternalAccessoryProtocolSession"

    const/16 v2, 0xc

    const v3, 0xea00

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 235
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopExternalAccessoryProtocolSession"

    const/16 v2, 0xd

    const v3, 0xea01

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 236
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StatusExternalAccessoryProtocolSession"

    const/16 v2, 0xe

    const v3, 0xea02

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StatusExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 238
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "RequestDeviceAuthenticationCertificate"

    const/16 v2, 0xf

    const v3, 0xaa10

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 239
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "DeviceAuthenticationCertificate"

    const/16 v2, 0x10

    const v3, 0xaa11

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 240
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "RequestDeviceAuthenticationChallengeResponse"

    const/16 v2, 0x11

    const v3, 0xaa12

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 241
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "DeviceAuthenticationResponse"

    const/16 v2, 0x12

    const v3, 0xaa13

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 242
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "DeviceAuthenticationFailed"

    const/16 v2, 0x13

    const v3, 0xaa14

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 243
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "DeviceAuthenticationSucceeded"

    const/16 v2, 0x14

    const v3, 0xaa15

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 245
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartHID"

    const/16 v2, 0x15

    const/16 v3, 0x6800

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 246
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "DeviceHIDReport"

    const/16 v2, 0x16

    const/16 v3, 0x6801

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 247
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AccessoryHIDReport"

    const/16 v2, 0x17

    const/16 v3, 0x6802

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AccessoryHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 248
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopHID"

    const/16 v2, 0x18

    const/16 v3, 0x6803

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 250
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartCallStateUpdates"

    const/16 v2, 0x19

    const/16 v3, 0x4154

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 251
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "CallStateUpdate"

    const/16 v2, 0x1a

    const/16 v3, 0x4155

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 252
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopCallStateUpdates"

    const/16 v2, 0x1b

    const/16 v3, 0x4156

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 253
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "InitiateCall"

    const/16 v2, 0x1c

    const/16 v3, 0x415a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 254
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "AcceptCall"

    const/16 v2, 0x1d

    const/16 v3, 0x415b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 255
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "EndCall"

    const/16 v2, 0x1e

    const/16 v3, 0x415c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 256
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "HoldStatusUpdate"

    const/16 v2, 0x1f

    const/16 v3, 0x415f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->HoldStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 257
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "MuteStatusUpdate"

    const/16 v2, 0x20

    const/16 v3, 0x4160

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 259
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartCommunicationUpdates"

    const/16 v2, 0x21

    const/16 v3, 0x4157

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 260
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "CommunicationUpdate"

    const/16 v2, 0x22

    const/16 v3, 0x4158

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CommunicationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 261
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopCommunicationUpdates"

    const/16 v2, 0x23

    const/16 v3, 0x4159

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 264
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartListUpdates"

    const/16 v2, 0x24

    const/16 v3, 0x4170

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartListUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 265
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "ListUpdate"

    const/16 v2, 0x25

    const/16 v3, 0x4171

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->ListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 266
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopListUpdate"

    const/16 v2, 0x26

    const/16 v3, 0x4172

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 268
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StartNowPlayingUpdates"

    const/16 v2, 0x27

    const/16 v3, 0x5000

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 269
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "NowPlayingUpdate"

    const/16 v2, 0x28

    const/16 v3, 0x5001

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->NowPlayingUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 270
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "StopNowPlayingUpdates"

    const/16 v2, 0x29

    const/16 v3, 0x5002

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 271
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    const-string v1, "RequestAppLaunch"

    const/16 v2, 0x2a

    const v3, 0xea02

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAppLaunch:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 219
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAuthenticationRequest:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationInformation:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationAccepted:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationRejected:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CancelIdentification:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationInformationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StatusExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AccessoryHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->HoldStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CommunicationUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCommunicationUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartListUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->ListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopListUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->NowPlayingUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAppLaunch:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 276
    iput p3, p0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 277
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->iAPMsgById:Landroid/util/SparseArray;

    invoke-virtual {v0, p3, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 278
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 219
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    return-object v0
.end method
