.class final enum Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AppLaunchMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

.field public static final enum LaunchWithUserAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

.field public static final enum LaunchWithoutAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 287
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    const-string v1, "LaunchWithUserAlert"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithUserAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    .line 288
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    const-string v1, "LaunchWithoutAlert"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithoutAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    .line 286
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithUserAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithoutAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 286
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    return-object v0
.end method
