.class Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
.super Ljava/lang/Object;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SessionMessage"
.end annotation


# instance fields
.field builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

.field msgId:I

.field msgLen:I

.field final synthetic this$0:Lcom/navdy/hud/mfi/iAPProcessor;


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;II)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/iAPProcessor;
    .param p2, "msgLen"    # I
    .param p3, "msgId"    # I

    .prologue
    .line 640
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->this$0:Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641
    iput p2, p0, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->msgLen:I

    .line 642
    iput p3, p0, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->msgId:I

    .line 643
    new-instance v0, Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-direct {v0, p2}, Lcom/navdy/hud/mfi/ByteArrayBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 644
    return-void
.end method


# virtual methods
.method append([B)V
    .locals 3
    .param p1, "bytes"    # [B

    .prologue
    .line 648
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addBlob([B)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :goto_0
    return-void

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "ie":Ljava/io/IOException;
    # getter for: Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/iAPProcessor;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error appending messages "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
