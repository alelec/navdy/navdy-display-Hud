.class Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;
.super Ljava/lang/Thread;
.source "BluetoothIAP2Server.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/BluetoothIAP2Server;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectThread"
.end annotation


# instance fields
.field private final mmDevice:Landroid/bluetooth/BluetoothDevice;

.field private final mmSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 342
    iput-object p2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    .line 343
    const/4 v1, 0x0

    .line 348
    .local v1, "tmp":Landroid/bluetooth/BluetoothSocket;
    :try_start_0
    sget-object v2, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->DEVICE_IAP2:Ljava/util/UUID;

    invoke-virtual {p2, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 352
    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    .line 353
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "Socket create() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 389
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :goto_0
    return-void

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BluetoothIAP2Server"

    const-string v2, "close() of connect socket failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 356
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "BEGIN mConnectThread"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const-string v2, "ConnectThread"

    invoke-virtual {p0, v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->setName(Ljava/lang/String;)V

    .line 360
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$000(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 366
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    iget-object v3, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    monitor-enter v3

    .line 380
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->mConnectThread:Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;
    invoke-static {v2, v4}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$302(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;)Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;

    .line 381
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    iget-object v3, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    iget-object v4, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V

    .line 385
    :goto_0
    return-void

    .line 367
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 374
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # invokes: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connectionFailed()V
    invoke-static {v2}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$200(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V

    goto :goto_0

    .line 371
    :catch_1
    move-exception v1

    .line 372
    .local v1, "e2":Ljava/io/IOException;
    const-string v2, "BluetoothIAP2Server"

    const-string v3, "unable to close() socket during connection failure"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 381
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e2":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method
