.class public final enum Lcom/navdy/hud/mfi/CallStateUpdate$Status;
.super Ljava/lang/Enum;
.source "CallStateUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/CallStateUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/CallStateUpdate$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Active:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Connecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Disconnected:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Disconnecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Held:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Ringing:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

.field public static final enum Sending:Lcom/navdy/hud/mfi/CallStateUpdate$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Disconnected"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Disconnected:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 41
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Sending"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Sending:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 42
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Ringing"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Ringing:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 43
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Connecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 44
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Active"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Active:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 45
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Held"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Held:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 46
    new-instance v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    const-string v1, "Disconnecting"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/CallStateUpdate$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Disconnecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    .line 39
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    sget-object v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Disconnected:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Sending:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Ringing:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Connecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Active:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Held:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->Disconnecting:Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->$VALUES:[Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/CallStateUpdate$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/CallStateUpdate$Status;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/mfi/CallStateUpdate$Status;->$VALUES:[Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/CallStateUpdate$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/CallStateUpdate$Status;

    return-object v0
.end method
