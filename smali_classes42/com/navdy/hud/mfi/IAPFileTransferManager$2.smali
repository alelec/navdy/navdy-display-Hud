.class Lcom/navdy/hud/mfi/IAPFileTransferManager$2;
.super Ljava/lang/Object;
.source "IAPFileTransferManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/mfi/IAPFileTransferManager;->cancel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/IAPFileTransferManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/IAPFileTransferManager;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 130
    iget-object v2, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$000(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 131
    .local v0, "fileTransferSessions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/hud/mfi/FileTransferSession;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/FileTransferSession;

    .line 132
    .local v1, "session":Lcom/navdy/hud/mfi/FileTransferSession;
    if-eqz v1, :cond_0

    .line 133
    invoke-virtual {v1}, Lcom/navdy/hud/mfi/FileTransferSession;->isActive()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Canceling, File transfer session , ID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {v1}, Lcom/navdy/hud/mfi/FileTransferSession;->cancel()V

    .line 136
    iget-object v3, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;
    invoke-static {v3}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$200(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Lcom/navdy/hud/mfi/IAPFileTransferListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;->this$0:Lcom/navdy/hud/mfi/IAPFileTransferManager;

    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;
    invoke-static {v3}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$200(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Lcom/navdy/hud/mfi/IAPFileTransferListener;

    move-result-object v3

    iget v4, v1, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/mfi/IAPFileTransferListener;->onFileTransferCancel(I)V

    goto :goto_0

    .line 140
    :cond_1
    # getter for: Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/mfi/IAPFileTransferManager;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File transfer session not canceled , ID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/navdy/hud/mfi/FileTransferSession;->mFileTransferIdentifier:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Not active"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 144
    .end local v1    # "session":Lcom/navdy/hud/mfi/FileTransferSession;
    :cond_2
    return-void
.end method
