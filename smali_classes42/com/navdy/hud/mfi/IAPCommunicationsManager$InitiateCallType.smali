.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "InitiateCallType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

.field public static final enum Destination:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

.field public static final enum Redial:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

.field public static final enum VoiceMail:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    const-string v1, "Destination"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Destination:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    .line 68
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    const-string v1, "VoiceMail"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->VoiceMail:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    .line 69
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    const-string v1, "Redial"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Redial:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Destination:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->VoiceMail:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->Redial:Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$InitiateCallType;

    return-object v0
.end method
