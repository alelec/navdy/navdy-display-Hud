.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AcceptAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

.field public static final enum EndAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

.field public static final enum HoldAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    const-string v1, "HoldAndAccept"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->HoldAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    .line 74
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    const-string v1, "EndAndAccept"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->EndAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    .line 72
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->HoldAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->EndAndAccept:Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$AcceptAction;

    return-object v0
.end method
