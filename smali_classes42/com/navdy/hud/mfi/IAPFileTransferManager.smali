.class public Lcom/navdy/hud/mfi/IAPFileTransferManager;
.super Ljava/lang/Object;
.source "IAPFileTransferManager.java"

# interfaces
.implements Lcom/navdy/hud/mfi/IIAPFileTransferManager;


# static fields
.field public static final DEFAULT_FILE_TRANSFER_LIMIT:I = 0x100000

.field private static final FILE_TRANSFER_IDENTIFIER_INDEX:I = 0x0

.field private static final MAX_SESSIONS:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile expectedFileTransferIdentifier:I

.field private mFileTransferLimit:J

.field private mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

.field private mIapProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

.field private mSerialExecutor:Ljava/util/concurrent/ExecutorService;

.field private mSessionCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/hud/mfi/FileTransferSession;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 2
    .param p1, "processor"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->expectedFileTransferIdentifier:I

    .line 26
    const-wide/32 v0, 0x100000

    iput-wide v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferLimit:J

    .line 30
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mIapProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/IAPFileTransferManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/mfi/IAPFileTransferManager;)Lcom/navdy/hud/mfi/IAPFileTransferListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/IAPFileTransferManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/mfi/IAPFileTransferManager$2;-><init>(Lcom/navdy/hud/mfi/IAPFileTransferManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 146
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 94
    return-void
.end method

.method public getFileTransferLimit()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferLimit:J

    return-wide v0
.end method

.method public onCanceled(I)V
    .locals 3
    .param p1, "fileTransferIdentifier"    # I

    .prologue
    .line 73
    sget-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File transfer was cancelled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    invoke-interface {v0, p1}, Lcom/navdy/hud/mfi/IAPFileTransferListener;->onFileTransferCancel(I)V

    .line 78
    :cond_0
    return-void
.end method

.method public onError(ILjava/lang/Throwable;)V
    .locals 3
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 87
    sget-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error in file transfer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method public onFileTransferSetupRequest(IJ)V
    .locals 2
    .param p1, "identifier"    # I
    .param p2, "size"    # J

    .prologue
    .line 108
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/hud/mfi/IAPFileTransferListener;->onFileTransferSetup(IJ)V

    .line 111
    :cond_0
    return-void
.end method

.method public onFileTransferSetupResponse(IZ)V
    .locals 3
    .param p1, "identifier"    # I
    .param p2, "proceed"    # Z

    .prologue
    .line 115
    iget-object v1, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/FileTransferSession;

    .line 116
    .local v0, "transferSession":Lcom/navdy/hud/mfi/FileTransferSession;
    if-eqz v0, :cond_0

    .line 117
    if-eqz p2, :cond_1

    .line 118
    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession;->proceed()V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/mfi/FileTransferSession;->cancel()V

    goto :goto_0
.end method

.method public onPaused(I)V
    .locals 2
    .param p1, "fileTransferIdentifier"    # I

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    const-string v1, "File transfer paused"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    return-void
.end method

.method public onSuccess(I[B)V
    .locals 3
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "data"    # [B

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File successfully transferred "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/mfi/IAPFileTransferListener;->onFileReceived(I[B)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSessionCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public queue([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/mfi/IAPFileTransferManager$1;-><init>(Lcom/navdy/hud/mfi/IAPFileTransferManager;[B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 55
    return-void
.end method

.method public sendMessage(I[B)V
    .locals 2
    .param p1, "fileTransferIdentifier"    # I
    .param p2, "message"    # [B

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mIapProcessor:Lcom/navdy/hud/mfi/iAPProcessor;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(I[B)V

    .line 83
    return-void
.end method

.method public setFileTransferLimit(I)V
    .locals 2
    .param p1, "limit"    # I

    .prologue
    .line 98
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferLimit:J

    .line 99
    return-void
.end method

.method public setFileTransferListener(Lcom/navdy/hud/mfi/IAPFileTransferListener;)V
    .locals 0
    .param p1, "mFileTransferListener"    # Lcom/navdy/hud/mfi/IAPFileTransferListener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/navdy/hud/mfi/IAPFileTransferManager;->mFileTransferListener:Lcom/navdy/hud/mfi/IAPFileTransferListener;

    .line 36
    return-void
.end method
