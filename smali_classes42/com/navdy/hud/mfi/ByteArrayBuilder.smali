.class public Lcom/navdy/hud/mfi/ByteArrayBuilder;
.super Ljava/lang/Object;
.source "ByteArrayBuilder.java"


# instance fields
.field bos:Ljava/io/ByteArrayOutputStream;

.field dos:Ljava/io/DataOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    .line 19
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    .line 20
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "initialSize"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    .line 28
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    .line 29
    return-void
.end method


# virtual methods
.method public addBlob([B)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->write([B)V

    .line 58
    return-object p0
.end method

.method public addBlob([BII)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "b"    # [B
    .param p2, "startOffset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 63
    return-object p0
.end method

.method public addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "val"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 33
    return-object p0
.end method

.method public addInt32(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "val"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 43
    return-object p0
.end method

.method public addInt64(J)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "val"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 48
    return-object p0
.end method

.method public addInt8(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 1
    .param p1, "val"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38
    return-object p0
.end method

.method public addUTF8(Ljava/lang/String;)Lcom/navdy/hud/mfi/ByteArrayBuilder;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->dos:Ljava/io/DataOutputStream;

    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 53
    return-object p0
.end method

.method public build()[B
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/mfi/ByteArrayBuilder;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    return v0
.end method
