.class public interface abstract Lcom/navdy/hud/mfi/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final DEVICE_NAME:Ljava/lang/String; = "device_name"

.field public static final MESSAGE_DEVICE_NAME:I = 0x4

.field public static final MESSAGE_EA_READ:I = 0x2

.field public static final MESSAGE_EA_WRITE:I = 0x3

.field public static final MESSAGE_STATE_CHANGE:I = 0x1

.field public static final MESSAGE_TOAST:I = 0x5

.field public static final TOAST:Ljava/lang/String; = "toast"
