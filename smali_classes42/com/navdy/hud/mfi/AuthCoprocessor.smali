.class public Lcom/navdy/hud/mfi/AuthCoprocessor;
.super Ljava/lang/Object;
.source "AuthCoprocessor.java"


# static fields
.field private static final ACCESSORY_CERTIFICATE_DATA_REGISTER:I = 0x31

.field private static final ACCESSORY_CERTIFICATE_LENGTH_REGISTER:I = 0x30

.field private static final ACCESSORY_SIGNATURE_GENERATION:I = 0x1

.field private static final AUTH_ERROR_FLAG:I = 0x80

.field private static final BUS:I = 0x0

.field private static final CHALLENGE_DATA_REGISTER:I = 0x21

.field private static final CHALLENGE_LENGTH_REGISTER:I = 0x20

.field private static final DEVICE_ADDRESS:I = 0x11

.field private static final DEVICE_CERTIFICATE_DATA_REGISTER:I = 0x51

.field private static final DEVICE_CERTIFICATE_LENGTH_REGISTER:I = 0x50

.field private static final DEVICE_CERTIFICATE_VALIDATION:I = 0x4

.field private static final DEVICE_CHALLENGE_GENERATION:I = 0x2

.field private static final DEVICE_SIGNATURE_VALIDATION:I = 0x3

.field private static final ERROR_CODE_REGISTER:I = 0x5

.field private static final PAGE_SIZE:I = 0x80

.field private static final SIGNATURE_DATA_REGISTER:I = 0x12

.field private static final SIGNATURE_LENGTH_REGISTER:I = 0x11

.field private static final STATUS_CONTROL_REGISTER:I = 0x10

.field private static final TAG:Ljava/lang/String; = "AuthCoprocessor"


# instance fields
.field private context:Landroid/content/Context;

.field private crashReportSent:Z

.field private i2c:Lcom/navdy/hud/mfi/I2C;

.field private iapListener:Lcom/navdy/hud/mfi/IAPListener;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/mfi/IAPListener;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v1, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->crashReportSent:Z

    .line 45
    new-instance v0, Lcom/navdy/hud/mfi/I2C;

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/I2C;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Lcom/navdy/hud/mfi/AuthCoprocessor$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/mfi/AuthCoprocessor$1;-><init>(Lcom/navdy/hud/mfi/AuthCoprocessor;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    .line 64
    :goto_0
    iput-object p2, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->context:Landroid/content/Context;

    .line 65
    return-void

    .line 62
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    goto :goto_0
.end method

.method private checkStatus(Ljava/lang/String;)V
    .locals 10
    .param p1, "desc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x11

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 98
    iget-object v5, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v6, 0x10

    invoke-virtual {v5, v9, v6}, Lcom/navdy/hud/mfi/I2C;->readByte(II)I

    move-result v3

    .line 99
    .local v3, "status":I
    iget-object v5, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/4 v6, 0x5

    invoke-virtual {v5, v9, v6}, Lcom/navdy/hud/mfi/I2C;->readByte(II)I

    move-result v1

    .line 104
    .local v1, "err":I
    and-int/lit16 v5, v3, 0x80

    if-eqz v5, :cond_0

    .line 105
    new-array v5, v8, [B

    int-to-byte v6, v3

    aput-byte v6, v5, v7

    invoke-static {v5}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([B)Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, "statusHex":Ljava/lang/String;
    new-array v5, v8, [B

    int-to-byte v6, v1

    aput-byte v6, v5, v7

    invoke-static {v5}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([B)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "errorHex":Ljava/lang/String;
    const-string v5, "AuthCoprocessor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "auth CP status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const-string v5, "AuthCoprocessor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "auth CP error code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-direct {p0}, Lcom/navdy/hud/mfi/AuthCoprocessor;->sendCrashReport()V

    .line 111
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    invoke-interface {v5, p1, v4, v2}, Lcom/navdy/hud/mfi/IAPListener;->onCoprocessorStatusCheckFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Throwable;
    const-string v5, "AuthCoprocessor"

    const-string v6, "logging to listener failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v2    # "errorHex":Ljava/lang/String;
    .end local v4    # "statusHex":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendCrashReport()V
    .locals 5

    .prologue
    .line 76
    iget-boolean v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->crashReportSent:Z

    if-eqz v3, :cond_0

    .line 77
    const-string v3, "AuthCoprocessor"

    const-string v4, "skipping duplicate crash report"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->context:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 81
    const-string v3, "AuthCoprocessor"

    const-string v4, "cannot sent crash report, null context"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :cond_1
    :try_start_0
    const-string v3, "com.navdy.hud.app.util.CrashReportService"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 87
    .local v0, "crashReporterClass":Ljava/lang/Class;
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->context:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "DumpCrashReport"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v3, "EXTRA_CRASH_TYPE"

    const-string v4, "MFI_ERROR"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->context:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 91
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->crashReportSent:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    .end local v0    # "crashReporterClass":Ljava/lang/Class;
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AuthCoprocessor"

    const-string v4, "error in lookup of CrashReportService class"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private writePages(II[BLjava/lang/String;)V
    .locals 10
    .param p1, "lengthRegister"    # I
    .param p2, "dataRegister"    # I
    .param p3, "data"    # [B
    .param p4, "desc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x80

    const/16 v1, 0x11

    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    array-length v2, p3

    invoke-virtual {v0, v1, p1, v2}, Lcom/navdy/hud/mfi/I2C;->writeShort(III)I

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "write "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " length"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 123
    array-length v6, p3

    .line 124
    .local v6, "bytesToWrite":I
    const/4 v8, 0x0

    .line 126
    .local v8, "offset":I
    new-array v3, v9, [B

    .line 128
    .local v3, "buf":[B
    :goto_0
    if-lez v6, :cond_0

    .line 129
    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 130
    .local v7, "len":I
    invoke-static {p3, v8, v3, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    array-length v5, v3

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/mfi/I2C;->write(II[BII)I

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "write "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 133
    add-int/lit8 p2, p2, 0x1

    .line 134
    add-int/2addr v8, v7

    .line 135
    sub-int/2addr v6, v7

    .line 136
    goto :goto_0

    .line 137
    .end local v7    # "len":I
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/I2C;->close()V

    .line 73
    return-void
.end method

.method public createAccessorySignature([B)[B
    .locals 7
    .param p1, "challenge"    # [B

    .prologue
    .line 150
    :try_start_0
    array-length v3, p1

    const/16 v4, 0x14

    if-eq v3, v4, :cond_0

    .line 151
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "wrong challenge size"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "AuthCoprocessor"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 168
    const/4 v1, 0x0

    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 154
    :cond_0
    const/16 v3, 0x20

    const/16 v4, 0x21

    :try_start_1
    const-string v5, "accessory signature"

    invoke-direct {p0, v3, v4, p1, v5}, Lcom/navdy/hud/mfi/AuthCoprocessor;->writePages(II[BLjava/lang/String;)V

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x11

    const/16 v6, 0x80

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/hud/mfi/I2C;->writeShort(III)I

    .line 157
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x10

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/hud/mfi/I2C;->writeByte(III)I

    .line 158
    const-string v3, "generate accessory signature"

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 160
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x11

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/I2C;->readShort(II)I

    move-result v2

    .line 161
    .local v2, "signatureSize":I
    new-array v1, v2, [B

    .line 162
    .local v1, "signature":[B
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x12

    invoke-virtual {v3, v4, v5, v1}, Lcom/navdy/hud/mfi/I2C;->read(II[B)I

    .line 163
    const-string v3, "read accessory signature"

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public open()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    invoke-virtual {v0}, Lcom/navdy/hud/mfi/I2C;->open()V

    .line 69
    return-void
.end method

.method public readAccessoryCertificate()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x11

    .line 140
    iget-object v2, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v3, 0x30

    invoke-virtual {v2, v4, v3}, Lcom/navdy/hud/mfi/I2C;->readShort(II)I

    move-result v1

    .line 141
    .local v1, "len":I
    new-array v0, v1, [B

    .line 142
    .local v0, "cert":[B
    iget-object v2, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v3, 0x31

    invoke-virtual {v2, v4, v3, v0}, Lcom/navdy/hud/mfi/I2C;->read(II[B)I

    .line 143
    const-string v2, "read accessory certificate"

    invoke-direct {p0, v2}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 144
    return-object v0
.end method

.method public validateDeviceCertificateAndGenerateDeviceChallenge([B)[B
    .locals 7
    .param p1, "certificate"    # [B

    .prologue
    .line 177
    const/16 v3, 0x50

    const/16 v4, 0x51

    :try_start_0
    const-string v5, "device certificate"

    invoke-direct {p0, v3, v4, p1, v5}, Lcom/navdy/hud/mfi/AuthCoprocessor;->writePages(II[BLjava/lang/String;)V

    .line 179
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x10

    const/4 v6, 0x4

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/hud/mfi/I2C;->writeByte(III)I

    .line 180
    const-string v3, "validate device certificate"

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 182
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x10

    const/4 v6, 0x2

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/hud/mfi/I2C;->writeByte(III)I

    .line 183
    const-string v3, "generate device challenge"

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V

    .line 185
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/I2C;->readShort(II)I

    move-result v2

    .line 186
    .local v2, "len":I
    new-array v0, v2, [B

    .line 187
    .local v0, "challenge":[B
    iget-object v3, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v4, 0x11

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v5, v0}, Lcom/navdy/hud/mfi/I2C;->read(II[B)I

    .line 188
    const-string v3, "read device challenge"

    invoke-direct {p0, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v0    # "challenge":[B
    .end local v2    # "len":I
    :goto_0
    return-object v0

    .line 191
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AuthCoprocessor"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 193
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validateDeviceSignature([B)Z
    .locals 5
    .param p1, "signature"    # [B

    .prologue
    .line 200
    const/16 v1, 0x11

    const/16 v2, 0x12

    :try_start_0
    const-string v3, "device signature"

    invoke-direct {p0, v1, v2, p1, v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->writePages(II[BLjava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/navdy/hud/mfi/AuthCoprocessor;->i2c:Lcom/navdy/hud/mfi/I2C;

    const/16 v2, 0x11

    const/16 v3, 0x10

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/hud/mfi/I2C;->writeByte(III)I

    .line 204
    const-string v1, "validate device signature"

    invoke-direct {p0, v1}, Lcom/navdy/hud/mfi/AuthCoprocessor;->checkStatus(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    const/4 v1, 0x1

    .line 209
    :goto_0
    return v1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AuthCoprocessor"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 209
    const/4 v1, 0x0

    goto :goto_0
.end method
