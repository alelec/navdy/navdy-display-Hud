.class final enum Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;
.super Ljava/lang/Enum;
.source "IAPCommunicationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPCommunicationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "EndCallAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

.field public static final enum EndAll:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

.field public static final enum EndDecline:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    const-string v1, "EndDecline"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndDecline:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    .line 79
    new-instance v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    const-string v1, "EndAll"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndAll:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndDecline:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->EndAll:Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->$VALUES:[Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPCommunicationsManager$EndCallAction;

    return-object v0
.end method
