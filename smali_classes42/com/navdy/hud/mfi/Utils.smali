.class public Lcom/navdy/hud/mfi/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final MFI_TAG:Ljava/lang/String; = "MFi"

.field protected static final TAG:Ljava/lang/String; = "navdy-mfi-Utils"

.field protected static final hexArray:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/Utils;->hexArray:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static bytesToHex([BZ)Ljava/lang/String;
    .locals 7
    .param p0, "bytes"    # [B
    .param p1, "spaces"    # Z

    .prologue
    .line 29
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    .line 30
    .local v0, "charsPerByte":I
    :goto_0
    array-length v4, p0

    mul-int/2addr v4, v0

    new-array v1, v4, [C

    .line 31
    .local v1, "hexChars":[C
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v4, p0

    if-ge v2, v4, :cond_2

    .line 32
    aget-byte v4, p0, v2

    and-int/lit16 v3, v4, 0xff

    .line 33
    .local v3, "v":I
    mul-int v4, v2, v0

    sget-object v5, Lcom/navdy/hud/mfi/Utils;->hexArray:[C

    ushr-int/lit8 v6, v3, 0x4

    aget-char v5, v5, v6

    aput-char v5, v1, v4

    .line 34
    mul-int v4, v2, v0

    add-int/lit8 v4, v4, 0x1

    sget-object v5, Lcom/navdy/hud/mfi/Utils;->hexArray:[C

    and-int/lit8 v6, v3, 0xf

    aget-char v5, v5, v6

    aput-char v5, v1, v4

    .line 35
    if-eqz p1, :cond_0

    .line 36
    mul-int v4, v2, v0

    add-int/lit8 v4, v4, 0x2

    const/16 v5, 0x20

    aput-char v5, v1, v4

    .line 31
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 29
    .end local v0    # "charsPerByte":I
    .end local v1    # "hexChars":[C
    .end local v2    # "j":I
    .end local v3    # "v":I
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 39
    .restart local v0    # "charsPerByte":I
    .restart local v1    # "hexChars":[C
    .restart local v2    # "j":I
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method public static bytesToMacAddress([B)Ljava/lang/String;
    .locals 9
    .param p0, "address"    # [B

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 43
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%02X:%02X:%02X:%02X:%02X:%02X"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    aget-byte v3, p0, v4

    .line 44
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v4

    aget-byte v3, p0, v5

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v5

    aget-byte v3, p0, v6

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v6

    aget-byte v3, p0, v7

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v7

    aget-byte v3, p0, v8

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x5

    const/4 v4, 0x5

    aget-byte v4, p0, v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    .line 43
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getConstantsMap(Ljava/lang/Class;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "c"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/hud/mfi/Utils;->getConstantsMap(Ljava/lang/Class;Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public static getConstantsMap(Ljava/lang/Class;Ljava/lang/String;)Landroid/util/SparseArray;
    .locals 8
    .param p0, "c"    # Ljava/lang/Class;
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 72
    .local v0, "constants":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 73
    .local v3, "fields":[Ljava/lang/reflect/Field;
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v2, v3, v4

    .line 75
    .local v2, "field":Ljava/lang/reflect/Field;
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v6

    invoke-static {v6}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 76
    if-eqz p1, :cond_0

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 77
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v6, "navdy-mfi-Utils"

    const-string v7, ""

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 84
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "field":Ljava/lang/reflect/Field;
    :cond_2
    return-object v0
.end method

.method public static intsToBytes([I)[B
    .locals 3
    .param p0, "ints"    # [I

    .prologue
    .line 103
    array-length v2, p0

    new-array v0, v2, [B

    .line 104
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 105
    aget v2, p0, v1

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    :cond_0
    return-object v0
.end method

.method public static varargs logTransfer(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 8
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "fmt"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    .line 88
    const/4 v3, 0x3

    invoke-static {p0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 89
    const/4 v3, 0x2

    invoke-static {p0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    .line 90
    .local v2, "logPacketData":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p2

    if-ge v1, v3, :cond_2

    .line 91
    aget-object v3, p2, v1

    instance-of v3, v3, [B

    if-eqz v3, :cond_0

    .line 92
    aget-object v3, p2, v1

    check-cast v3, [B

    move-object v0, v3

    check-cast v0, [B

    .line 93
    .local v0, "data":[B
    if-eqz v2, :cond_1

    .line 94
    invoke-static {v0, v7}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v3

    .line 95
    :goto_1
    aput-object v3, p2, v1

    .line 90
    .end local v0    # "data":[B
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .restart local v0    # "data":[B
    :cond_1
    const-string v3, "%d bytes"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    array-length v6, v0

    .line 95
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 98
    .end local v0    # "data":[B
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    .end local v1    # "i":I
    .end local v2    # "logPacketData":Z
    :cond_3
    return-void
.end method

.method public static nibbleToByte(B)I
    .locals 2
    .param p0, "n"    # B

    .prologue
    .line 48
    and-int/lit16 v0, p0, 0xff

    .line 49
    .local v0, "b":I
    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    .line 50
    add-int/lit8 v0, v0, -0x30

    .line 54
    :goto_0
    return v0

    .line 52
    :cond_0
    add-int/lit8 v1, v0, -0x41

    add-int/lit8 v0, v1, 0xa

    goto :goto_0
.end method

.method public static packUInt16(I)[B
    .locals 3
    .param p0, "i"    # I

    .prologue
    .line 137
    const/4 v0, 0x2

    new-array v0, v0, [B

    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method

.method public static packUInt8(I)[B
    .locals 3
    .param p0, "b"    # I

    .prologue
    .line 141
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method

.method public static parseMACAddress(Ljava/lang/String;)[B
    .locals 6
    .param p0, "addr"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x6

    .line 58
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 59
    .local v0, "d":[B
    new-array v2, v5, [B

    .line 60
    .local v2, "mac":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_0

    .line 61
    mul-int/lit8 v3, v1, 0x3

    aget-byte v3, v0, v3

    invoke-static {v3}, Lcom/navdy/hud/mfi/Utils;->nibbleToByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x1

    aget-byte v4, v0, v4

    invoke-static {v4}, Lcom/navdy/hud/mfi/Utils;->nibbleToByte(B)I

    move-result v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return-object v2
.end method

.method public static toASCII([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    .line 111
    const-string v1, ""

    .line 112
    .local v1, "s":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 113
    aget-byte v3, p0, v0

    and-int/lit16 v2, v3, 0xff

    .line 114
    .local v2, "v":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v3, 0x20

    if-lt v2, v3, :cond_0

    const/16 v3, 0x80

    if-ge v2, v3, :cond_0

    int-to-char v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    const/16 v3, 0x2e

    goto :goto_1

    .line 116
    .end local v2    # "v":I
    :cond_1
    return-object v1
.end method

.method public static unpackInt64([BI)J
    .locals 4
    .param p0, "d"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 120
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {p0, p1, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 121
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    return-wide v2
.end method

.method public static unpackUInt16([BI)I
    .locals 2
    .param p0, "d"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 129
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static unpackUInt32([BI)J
    .locals 5
    .param p0, "d"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 125
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public static unpackUInt64([BI)Ljava/math/BigInteger;
    .locals 3
    .param p0, "d"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 145
    const/16 v1, 0x8

    new-array v0, v1, [B

    const/4 v1, 0x0

    aget-byte v2, p0, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    add-int/lit8 v2, p1, 0x4

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    add-int/lit8 v2, p1, 0x5

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x6

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    add-int/lit8 v2, p1, 0x7

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 151
    .local v0, "bytes":[B
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    return-object v1
.end method

.method public static unpackUInt8([BI)I
    .locals 1
    .param p0, "d"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 133
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method
