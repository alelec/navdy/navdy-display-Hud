.class Lcom/navdy/hud/mfi/LinkLayer$1;
.super Landroid/os/Handler;
.source "LinkLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/mfi/LinkLayer;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/mfi/LinkLayer;


# direct methods
.method constructor <init>(Lcom/navdy/hud/mfi/LinkLayer;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/mfi/LinkLayer;
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private doNativeRunLoop(I[BI)V
    .locals 3
    .param p1, "messageType"    # I
    .param p2, "data"    # [B
    .param p3, "session"    # I

    .prologue
    .line 103
    iget-object v1, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # invokes: Lcom/navdy/hud/mfi/LinkLayer;->native_runloop(I[BI)I
    invoke-static {v1, p1, p2, p3}, Lcom/navdy/hud/mfi/LinkLayer;->access$300(Lcom/navdy/hud/mfi/LinkLayer;I[BI)I

    move-result v0

    .line 104
    .local v0, "error":I
    if-gez v0, :cond_0

    .line 106
    const-string v1, "LinkLayer"

    const-string v2, "runloop detected a disconnect"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v1, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/LinkLayer;->connectionEnded()V

    .line 112
    :goto_0
    return-void

    .line 109
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/navdy/hud/mfi/LinkLayer$1;->transferPackets(I)V

    .line 110
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/navdy/hud/mfi/LinkLayer$1;->transferPackets(I)V

    goto :goto_0
.end method

.method private transferPackets(I)V
    .locals 6
    .param p1, "messageType"    # I

    .prologue
    .line 117
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # invokes: Lcom/navdy/hud/mfi/LinkLayer;->native_get_message_session()I
    invoke-static {v3}, Lcom/navdy/hud/mfi/LinkLayer;->access$400(Lcom/navdy/hud/mfi/LinkLayer;)I

    move-result v2

    .line 118
    .local v2, "session":I
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # invokes: Lcom/navdy/hud/mfi/LinkLayer;->native_pop_message_data(I)[B
    invoke-static {v3, p1}, Lcom/navdy/hud/mfi/LinkLayer;->access$500(Lcom/navdy/hud/mfi/LinkLayer;I)[B

    move-result-object v0

    .line 119
    .local v0, "data":[B
    if-nez v0, :cond_0

    .line 139
    return-void

    .line 122
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 136
    const-string v3, "LinkLayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "transferPackets: unsupported message type ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # invokes: Lcom/navdy/hud/mfi/LinkLayer;->logOutGoingLinkPacket([B)V
    invoke-static {v3, v0}, Lcom/navdy/hud/mfi/LinkLayer;->access$600(Lcom/navdy/hud/mfi/LinkLayer;[B)V

    .line 125
    new-instance v1, Lcom/navdy/hud/mfi/LinkPacket;

    invoke-direct {v1, v0}, Lcom/navdy/hud/mfi/LinkPacket;-><init>([B)V

    .line 126
    .local v1, "pkt":Lcom/navdy/hud/mfi/LinkPacket;
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->physicalLayer:Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;
    invoke-static {v3}, Lcom/navdy/hud/mfi/LinkLayer;->access$700(Lcom/navdy/hud/mfi/LinkLayer;)Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/navdy/hud/mfi/LinkLayer$PhysicalLayer;->queue(Lcom/navdy/hud/mfi/LinkPacket;)V

    goto :goto_0

    .line 130
    .end local v1    # "pkt":Lcom/navdy/hud/mfi/LinkPacket;
    :pswitch_1
    new-instance v1, Lcom/navdy/hud/mfi/SessionPacket;

    invoke-direct {v1, v2, v0}, Lcom/navdy/hud/mfi/SessionPacket;-><init>(I[B)V

    .line 131
    .local v1, "pkt":Lcom/navdy/hud/mfi/SessionPacket;
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LL->iAP["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/navdy/hud/mfi/LinkLayer;->log(Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V
    invoke-static {v3, v4, v1}, Lcom/navdy/hud/mfi/LinkLayer;->access$800(Lcom/navdy/hud/mfi/LinkLayer;Ljava/lang/String;Lcom/navdy/hud/mfi/Packet;)V

    .line 132
    iget-object v3, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;
    invoke-static {v3}, Lcom/navdy/hud/mfi/LinkLayer;->access$200(Lcom/navdy/hud/mfi/LinkLayer;)Lcom/navdy/hud/mfi/iAPProcessor;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/navdy/hud/mfi/iAPProcessor;->queue(Lcom/navdy/hud/mfi/SessionPacket;)V

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "data":[B
    const/4 v2, 0x0

    .line 73
    .local v2, "session":I
    iget v1, p1, Landroid/os/Message;->what:I

    .line 74
    .local v1, "messageType":I
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 96
    :cond_0
    :goto_0
    :pswitch_0
    iget v4, p1, Landroid/os/Message;->what:I

    if-nez v4, :cond_1

    .line 97
    const/4 v4, 0x0

    const-wide/16 v6, 0x14

    invoke-virtual {p0, v4, v6, v7}, Lcom/navdy/hud/mfi/LinkLayer$1;->sendEmptyMessageDelayed(IJ)Z

    .line 99
    :cond_1
    invoke-direct {p0, v1, v0, v2}, Lcom/navdy/hud/mfi/LinkLayer$1;->doNativeRunLoop(I[BI)V

    .line 100
    return-void

    .line 76
    :pswitch_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/navdy/hud/mfi/Packet;

    iget-object v0, v4, Lcom/navdy/hud/mfi/Packet;->data:[B

    .line 77
    goto :goto_0

    .line 80
    :pswitch_2
    const/4 v3, 0x0

    .line 81
    .local v3, "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    iget-object v4, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v4}, Lcom/navdy/hud/mfi/LinkLayer;->access$000(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 82
    iget-object v4, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->controlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v4}, Lcom/navdy/hud/mfi/LinkLayer;->access$000(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    check-cast v3, Lcom/navdy/hud/mfi/SessionPacket;

    .line 86
    .restart local v3    # "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    :cond_2
    :goto_1
    if-eqz v3, :cond_0

    .line 87
    const/4 v1, 0x2

    .line 88
    iget-object v0, v3, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    .line 89
    iget v2, v3, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    goto :goto_0

    .line 83
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v4}, Lcom/navdy/hud/mfi/LinkLayer;->access$100(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 84
    iget-object v4, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->nonControlMessages:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v4}, Lcom/navdy/hud/mfi/LinkLayer;->access$100(Lcom/navdy/hud/mfi/LinkLayer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    check-cast v3, Lcom/navdy/hud/mfi/SessionPacket;

    .restart local v3    # "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    goto :goto_1

    .line 93
    .end local v3    # "sessionPacket":Lcom/navdy/hud/mfi/SessionPacket;
    :pswitch_3
    iget-object v4, p0, Lcom/navdy/hud/mfi/LinkLayer$1;->this$0:Lcom/navdy/hud/mfi/LinkLayer;

    # getter for: Lcom/navdy/hud/mfi/LinkLayer;->iAPProcessor:Lcom/navdy/hud/mfi/iAPProcessor;
    invoke-static {v4}, Lcom/navdy/hud/mfi/LinkLayer;->access$200(Lcom/navdy/hud/mfi/LinkLayer;)Lcom/navdy/hud/mfi/iAPProcessor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/mfi/iAPProcessor;->linkLost()V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
