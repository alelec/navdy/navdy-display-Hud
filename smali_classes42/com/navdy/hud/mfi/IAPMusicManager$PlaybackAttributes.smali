.class public final enum Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;
.super Ljava/lang/Enum;
.source "IAPMusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPMusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackAttributes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackAppBundleId:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackAppName:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackElapsedTimeInMilliseconds:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackRepeatMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackShuffleMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

.field public static final enum PlaybackStatus:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;


# instance fields
.field paramIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackStatus"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackStatus:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 48
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackElapsedTimeInMilliseconds"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackElapsedTimeInMilliseconds:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 49
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackShuffleMode"

    invoke-direct {v0, v1, v7, v6}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackShuffleMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 50
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackRepeatMode"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackRepeatMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 51
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackAppName"

    const/4 v2, 0x4

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppName:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 52
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    const-string v1, "PlaybackAppBundleId"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppBundleId:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackStatus:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackElapsedTimeInMilliseconds:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackShuffleMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackRepeatMode:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppName:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->PlaybackAppBundleId:Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->paramIndex:I

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;

    return-object v0
.end method


# virtual methods
.method public getParam()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager$PlaybackAttributes;->paramIndex:I

    return v0
.end method
