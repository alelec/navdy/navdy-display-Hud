.class Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;
.super Ljava/lang/Thread;
.source "BluetoothIAP2Server.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/BluetoothIAP2Server;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectedThread"
.end annotation


# instance fields
.field private final mmInStream:Ljava/io/InputStream;

.field private final mmOutStream:Ljava/io/OutputStream;

.field private final mmSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Landroid/bluetooth/BluetoothSocket;)V
    .locals 5
    .param p2, "socket"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 406
    const-string v3, "BluetoothIAP2Server"

    const-string v4, "create ConnectedThread"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iput-object p2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    .line 408
    const/4 v1, 0x0

    .line 409
    .local v1, "tmpIn":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 413
    .local v2, "tmpOut":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 414
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 419
    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    .line 420
    iput-object v2, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    .line 421
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "BluetoothIAP2Server"

    const-string v4, "temp sockets not created"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 466
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BluetoothIAP2Server"

    const-string v2, "close() of connect socket failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public run()V
    .locals 8

    .prologue
    .line 424
    iget-object v5, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    .line 425
    .local v3, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "BluetoothIAP2Server"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BEGIN mConnectedThread - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " name:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v5, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;
    invoke-static {v5}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$400(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Lcom/navdy/hud/mfi/LinkLayer;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/LinkLayer;->connectionStarted(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const/16 v5, 0x400

    new-array v0, v5, [B

    .line 436
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 437
    .local v1, "bytes":I
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 439
    .local v2, "data":[B
    iget-object v5, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # getter for: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;
    invoke-static {v5}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$400(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Lcom/navdy/hud/mfi/LinkLayer;

    move-result-object v5

    new-instance v6, Lcom/navdy/hud/mfi/LinkPacket;

    invoke-direct {v6, v2}, Lcom/navdy/hud/mfi/LinkPacket;-><init>([B)V

    invoke-virtual {v5, v6}, Lcom/navdy/hud/mfi/LinkLayer;->queue(Lcom/navdy/hud/mfi/LinkPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 440
    .end local v1    # "bytes":I
    .end local v2    # "data":[B
    :catch_0
    move-exception v4

    .line 441
    .local v4, "e":Ljava/io/IOException;
    const-string v5, "BluetoothIAP2Server"

    const-string v6, "disconnected"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 448
    iget-object v5, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;

    # invokes: Lcom/navdy/hud/mfi/BluetoothIAP2Server;->connectionLost()V
    invoke-static {v5}, Lcom/navdy/hud/mfi/BluetoothIAP2Server;->access$500(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V

    .line 449
    return-void
.end method

.method public write([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 458
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 462
    :goto_0
    return-void

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BluetoothIAP2Server"

    const-string v2, "Exception during write"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
