.class final enum Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;
.super Ljava/lang/Enum;
.source "iAPProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/iAPProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BluetoothTransportComponent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

.field public static final enum BluetoothTransportMediaAccessControlAddress:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

.field public static final enum TransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

.field public static final enum TransportComponentName:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

.field public static final enum TransportSupportsiAP2Connection:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 327
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const-string v1, "TransportComponentIdentifier"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    .line 328
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const-string v1, "TransportComponentName"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentName:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    .line 329
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const-string v1, "TransportSupportsiAP2Connection"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportSupportsiAP2Connection:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    .line 330
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const-string v1, "BluetoothTransportMediaAccessControlAddress"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->BluetoothTransportMediaAccessControlAddress:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    .line 326
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentName:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportSupportsiAP2Connection:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->BluetoothTransportMediaAccessControlAddress:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 326
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->$VALUES:[Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    return-object v0
.end method
