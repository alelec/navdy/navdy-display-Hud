.class public Lcom/navdy/hud/mfi/iAPProcessor;
.super Ljava/lang/Object;
.source "iAPProcessor.java"

# interfaces
.implements Lcom/navdy/hud/mfi/SessionPacketReceiver;
.implements Lcom/navdy/hud/mfi/EASessionPacketReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;,
        Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;,
        Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;,
        Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;,
        Lcom/navdy/hud/mfi/iAPProcessor$IAP2Param;,
        Lcom/navdy/hud/mfi/iAPProcessor$StopHID;,
        Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;,
        Lcom/navdy/hud/mfi/iAPProcessor$StartHID;,
        Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;,
        Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;,
        Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;,
        Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;,
        Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;,
        Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;,
        Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;,
        Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;,
        Lcom/navdy/hud/mfi/iAPProcessor$StateListener;,
        Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;
    }
.end annotation


# static fields
.field public static final CHARSET_NAME:Ljava/lang/String; = "UTF-8"

.field public static final DEVICE_AUTHENTICATION_CERTIFICATE_WAIT_TIME:I = 0xbb8

.field public static final DEVICE_AUTHENTICATION_RETRY_INTERVAL:I = 0x1388

.field private static final EASESSION_SESSION:I = 0x7

.field public static final HEADER_BUFFER:I = 0x64

.field public static final HID_COMPONENT_NAME:Ljava/lang/String; = "navdy-media-remote"

.field public static final MAX_DEVICE_AUTHENTICATION_RETRIES:I = 0x5

.field private static final MESSAGE_LINK_LOST:I = 0x2

.field private static final MESSAGE_RETRY_DEVICE_AUTHENTICATION:I = 0x3

.field private static final MESSAGE_SESSION_MESSAGE:I = 0x1

.field private static final MESSAGE_SKIP_DEVICE_AUTHENTICATION:I = 0x4

.field public static final NAVDY_CLIENT_BUNDLE_ID:Ljava/lang/String; = "com.navdy.NavdyClient"

.field public static final PRODUCT_IDENTIFIER:I = 0x1

.field public static final PROTOCOL_V1:Ljava/lang/String; = "com.navdy.hud.api.v1"

.field public static final PROXY_V1:Ljava/lang/String; = "com.navdy.hud.proxy.v1"

.field public static final STARTING_OFFSET:I = 0x6

.field private static final TAG:Ljava/lang/String;

.field public static final VENDOR_IDENTIFIER:I = 0x9886

.field static final iAPMsgById:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;",
            ">;"
        }
    .end annotation
.end field

.field static final isCommunicationUpdateSupported:Z = false

.field static final isCommunicationsSupported:Z = true

.field static final isHIDKeyboardSupported:Z = false

.field static final isHIDMediaRemoteSupported:Z = true

.field private static final isHIDSupported:Z = true

.field static final isMusicSupported:Z = true

.field public static final mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

.field private static final msgStart:[B

.field private static protocols:[Ljava/lang/String;


# instance fields
.field __someiAPMessageToForceEnumLoading__:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

.field accessoryName:Ljava/lang/String;

.field private authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

.field private controlProcessors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;",
            "Lcom/navdy/hud/mfi/IAPControlMessageProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private controlSession:I

.field private deviceAuthenticationRetries:I

.field private eaSessionById:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/hud/mfi/EASession;",
            ">;"
        }
    .end annotation
.end field

.field hidDescriptorKeyboard:[I

.field hidDescriptorMediaRemote:[I

.field private iapListener:Lcom/navdy/hud/mfi/IAPListener;

.field private isDeviceAuthenticationSupported:Z

.field private linkEstablished:Z

.field private linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

.field private mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

.field private final myHandler:Landroid/os/Handler;

.field private reassambleMsgs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;",
            ">;"
        }
    .end annotation
.end field

.field private stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 23
    const-class v0, Lcom/navdy/hud/mfi/iAPProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    .line 48
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.navdy.hud.api.v1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.navdy.hud.proxy.v1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->protocols:[Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDMediaRemote:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    .line 217
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->iAPMsgById:Landroid/util/SparseArray;

    .line 368
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->msgStart:[B

    return-void

    :array_0
    .array-data 1
        0x40t
        0x40t
    .end array-data
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/IAPListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/hud/mfi/IAPListener;

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;-><init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V

    .line 171
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/mfi/IAPListener;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    .line 82
    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->hidDescriptorMediaRemote:[I

    .line 101
    const/16 v0, 0x2d

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->hidDescriptorKeyboard:[I

    .line 139
    const-string v0, "Navdy HUD"

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->accessoryName:Ljava/lang/String;

    .line 173
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$1;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    .line 291
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAuthenticationRequest:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->__someiAPMessageToForceEnumLoading__:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 655
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    .line 164
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    .line 165
    new-instance v0, Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/mfi/AuthCoprocessor;-><init>(Lcom/navdy/hud/mfi/IAPListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlProcessors:Ljava/util/HashMap;

    .line 167
    return-void

    .line 82
    :array_0
    .array-data 4
        0x5
        0xc
        0x9
        0x1
        0xa1
        0x1
        0x15
        0x0
        0x25
        0x1
        0x9
        0xcf
        0x9
        0xcd
        0x9
        0xb5
        0x9
        0xb6
        0x75
        0x1
        0x95
        0x4
        0x81
        0x2
        0x75
        0x4
        0x95
        0x1
        0x81
        0x3
        0xc0
    .end array-data

    .line 101
    :array_1
    .array-data 4
        0x5
        0xc
        0x9
        0x1
        0xa1
        0x1
        0x5
        0x7
        0x15
        0x0
        0x25
        0x1
        0x75
        0x8
        0x95
        0x1
        0x81
        0x3
        0x5
        0xc
        0x15
        0x0
        0x25
        0x1
        0x9
        0x40
        0x9
        0xcd
        0x9
        0xb5
        0x9
        0xb6
        0x75
        0x1
        0x95
        0x4
        0x81
        0x2
        0x75
        0x4
        0x95
        0x1
        0x81
        0x3
        0xc0
    .end array-data
.end method

.method static synthetic access$000(Lcom/navdy/hud/mfi/iAPProcessor;Lcom/navdy/hud/mfi/SessionPacket;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/iAPProcessor;
    .param p1, "x1"    # Lcom/navdy/hud/mfi/SessionPacket;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->handleSessionPacket(Lcom/navdy/hud/mfi/SessionPacket;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/mfi/iAPProcessor;->handleLinkLoss()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/mfi/iAPProcessor;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/mfi/iAPProcessor;->retryDeviceAuthentication()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/mfi/iAPProcessor;)Lcom/navdy/hud/mfi/iAPProcessor$StateListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/mfi/iAPProcessor;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500()[B
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->msgStart:[B

    return-object v0
.end method

.method static copy([BI[B)V
    .locals 2
    .param p0, "dst"    # [B
    .param p1, "dstPos"    # I
    .param p2, "src"    # [B

    .prologue
    .line 214
    const/4 v0, 0x0

    array-length v1, p2

    invoke-static {p2, v0, p0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    return-void
.end method

.method private handleLinkLoss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 668
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 669
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 670
    iput-boolean v4, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkEstablished:Z

    .line 671
    iput v4, p0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    .line 672
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    if-eqz v2, :cond_0

    .line 673
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    invoke-interface {v2}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->clear()V

    .line 675
    :cond_0
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 676
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/mfi/EASession;

    .line 677
    .local v1, "session":Lcom/navdy/hud/mfi/EASession;
    invoke-direct {p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor;->stopEASession(Lcom/navdy/hud/mfi/EASession;)V

    .line 675
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 679
    .end local v1    # "session":Lcom/navdy/hud/mfi/EASession;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 680
    return-void
.end method

.method private handleSessionMessage(II[B)V
    .locals 20
    .param p1, "session"    # I
    .param p2, "msgid"    # I
    .param p3, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 732
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->iAPMsgById:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 733
    .local v11, "msgid_":Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    if-nez v11, :cond_1

    .line 734
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    const-string v4, "received unknown message (%d bytes)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 737
    :cond_1
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytes) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$2;->$SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage:[I

    invoke-virtual {v11}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 904
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->controlProcessors:Ljava/util/HashMap;

    invoke-virtual {v3, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;

    .line 905
    .local v14, "processor":Lcom/navdy/hud/mfi/IAPControlMessageProcessor;
    if-eqz v14, :cond_0

    .line 906
    move/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v14, v11, v0, v1}, Lcom/navdy/hud/mfi/IAPControlMessageProcessor;->processControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;I[B)V

    goto :goto_0

    .line 741
    .end local v14    # "processor":Lcom/navdy/hud/mfi/IAPControlMessageProcessor;
    :pswitch_1
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    .line 742
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    .line 743
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->open()V

    .line 745
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->readAccessoryCertificate()[B

    move-result-object v8

    .line 746
    .local v8, "cert":[B
    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    const/4 v4, 0x0

    .line 749
    invoke-virtual {v3, v4, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(I[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    .line 746
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    goto/16 :goto_0

    .end local v8    # "cert":[B
    :catchall_0
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v4}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    throw v3

    .line 757
    :pswitch_2
    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v13

    .line 758
    .local v13, "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(I)[B

    move-result-object v9

    .line 759
    .local v9, "chg":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->open()V

    .line 761
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3, v9}, Lcom/navdy/hud/mfi/AuthCoprocessor;->createAccessorySignature([B)[B

    move-result-object v10

    .line 762
    .local v10, "chgResponse":[B
    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    const/4 v4, 0x0

    .line 765
    invoke-virtual {v3, v4, v10}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(I[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    .line 762
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 767
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    goto/16 :goto_0

    .end local v10    # "chgResponse":[B
    :catchall_1
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v4}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    throw v3

    .line 778
    .end local v9    # "chg":[B
    .end local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    :pswitch_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    .line 779
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/mfi/iAPProcessor;->sendIdentification(I)V

    goto/16 :goto_0

    .line 786
    :pswitch_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    .line 787
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    if-eqz v3, :cond_2

    .line 788
    const-string v3, "MFi"

    const-string v4, "DeviceAuthentication supported, attempting"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    const-wide/16 v18, 0xbb8

    move-wide/from16 v0, v18

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 794
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v3, :cond_0

    .line 795
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v3}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onReady()V

    goto/16 :goto_0

    .line 802
    :pswitch_5
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    const-string v4, "msg data: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/16 v18, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/navdy/hud/mfi/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    if-eqz v3, :cond_3

    .line 804
    const-string v3, "MFi"

    const-string v4, "Identification rejected, trying without DeviceAuthentication"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    .line 807
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/mfi/iAPProcessor;->sendIdentification(I)V

    goto/16 :goto_0

    .line 809
    :cond_3
    const-string v3, "MFi"

    const-string v4, "Identification rejected, even with DeviceAuthentication turned off"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v3, :cond_0

    .line 812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v3}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onError()V

    goto/16 :goto_0

    .line 820
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 821
    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v13

    .line 822
    .restart local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(I)[B

    move-result-object v8

    .line 823
    .restart local v8    # "cert":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->open()V

    .line 825
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3, v8}, Lcom/navdy/hud/mfi/AuthCoprocessor;->validateDeviceCertificateAndGenerateDeviceChallenge([B)[B

    move-result-object v9

    .line 826
    .restart local v9    # "chg":[B
    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    const/4 v4, 0x0

    .line 829
    invoke-virtual {v3, v4, v9}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(I[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    .line 826
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 831
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    goto/16 :goto_0

    .end local v9    # "chg":[B
    :catchall_2
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v4}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    throw v3

    .line 837
    .end local v8    # "cert":[B
    .end local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    :pswitch_7
    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v13

    .line 838
    .restart local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getBlob(I)[B

    move-result-object v16

    .line 839
    .local v16, "respChg":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->open()V

    .line 841
    const/4 v12, 0x0

    .line 843
    .local v12, "ok":Z
    :try_start_3
    move-object/from16 v0, v16

    array-length v3, v0

    const/16 v4, 0x80

    if-le v3, v4, :cond_8

    .line 844
    const-string v3, "MFi"

    const-string v4, "DeviceAuthenticationResponse response too big - working around iOS 10 bug"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    const/4 v12, 0x1

    .line 849
    :goto_1
    if-nez v12, :cond_4

    .line 850
    const-string v3, "MFi"

    const-string v4, "DeviceAuthenticationResponse validation failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_5

    .line 853
    const-string v3, "MFi"

    const-string v4, "DeviceAuthenticationRetries reached the limit so sending success"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    const/4 v12, 0x1

    .line 856
    :cond_5
    new-instance v4, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    if-eqz v12, :cond_9

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    :goto_2
    invoke-direct {v4, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 861
    if-nez v12, :cond_a

    .line 862
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    const-wide/16 v18, 0x1388

    move-wide/from16 v0, v18

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 868
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v3, :cond_7

    .line 869
    if-eqz v12, :cond_b

    .line 870
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v3}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onReady()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 876
    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    goto/16 :goto_0

    .line 847
    :cond_8
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/navdy/hud/mfi/AuthCoprocessor;->validateDeviceSignature([B)Z

    move-result v12

    goto :goto_1

    .line 856
    :cond_9
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    goto :goto_2

    .line 864
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    if-eqz v3, :cond_6

    .line 865
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->iapListener:Lcom/navdy/hud/mfi/IAPListener;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    invoke-interface {v3, v4}, Lcom/navdy/hud/mfi/IAPListener;->onDeviceAuthenticationSuccess(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_3

    .line 876
    :catchall_3
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/mfi/iAPProcessor;->authCoprocessor:Lcom/navdy/hud/mfi/AuthCoprocessor;

    invoke-virtual {v4}, Lcom/navdy/hud/mfi/AuthCoprocessor;->close()V

    throw v3

    .line 872
    :cond_b
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v3}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onError()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_4

    .line 883
    .end local v12    # "ok":Z
    .end local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    .end local v16    # "respChg":[B
    :pswitch_8
    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v13

    .line 884
    .restart local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt8(I)I

    move-result v15

    .line 885
    .local v15, "protoId":I
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt16(I)I

    move-result v6

    .line 886
    .local v6, "eaSessionId":I
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StartExternalAccessoryProtocolSession: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (protoId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    new-instance v2, Lcom/navdy/hud/mfi/EASession;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    .line 888
    invoke-virtual {v3}, Lcom/navdy/hud/mfi/LinkLayer;->getRemoteAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/LinkLayer;->getRemoteName()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->protocols:[Ljava/lang/String;

    aget-object v7, v3, v15

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/mfi/EASession;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 889
    .local v2, "eaSession":Lcom/navdy/hud/mfi/EASession;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v3, v6, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 890
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->startEASession(Lcom/navdy/hud/mfi/EASession;)V

    goto/16 :goto_0

    .line 895
    .end local v2    # "eaSession":Lcom/navdy/hud/mfi/EASession;
    .end local v6    # "eaSessionId":I
    .end local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    .end local v15    # "protoId":I
    :pswitch_9
    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/mfi/iAPProcessor;->parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    move-result-object v13

    .line 896
    .restart local v13    # "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;->getUInt16(I)I

    move-result v17

    .line 897
    .local v17, "sessionId":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/mfi/EASession;

    .line 898
    .restart local v2    # "eaSession":Lcom/navdy/hud/mfi/EASession;
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->stopEASession(Lcom/navdy/hud/mfi/EASession;)V

    .line 899
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->remove(I)V

    goto/16 :goto_0

    .line 739
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private handleSessionPacket(Lcom/navdy/hud/mfi/SessionPacket;)V
    .locals 9
    .param p1, "smsg"    # Lcom/navdy/hud/mfi/SessionPacket;

    .prologue
    const/4 v7, 0x2

    .line 684
    const/4 v6, 0x1

    :try_start_0
    iput-boolean v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkEstablished:Z

    .line 685
    iget v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    if-ne v6, v7, :cond_1

    .line 686
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    if-eqz v6, :cond_0

    .line 687
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    iget-object v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    invoke-interface {v6, v7}, Lcom/navdy/hud/mfi/IIAPFileTransferManager;->queue([B)V

    .line 729
    :cond_0
    :goto_0
    return-void

    .line 691
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    iget v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;

    .line 692
    .local v5, "rcvMsg":Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
    if-nez v5, :cond_2

    .line 693
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    sget-object v7, Lcom/navdy/hud/mfi/iAPProcessor;->msgStart:[B

    invoke-direct {p0, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor;->startsWith([B[B)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 694
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v3

    .line 695
    .local v3, "msglen":I
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v2

    .line 696
    .local v2, "msgid":I
    new-instance v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;

    .end local v5    # "rcvMsg":Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
    invoke-direct {v5, p0, v3, v2}, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor;II)V

    .line 697
    .restart local v5    # "rcvMsg":Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    iget v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v6, v7, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 711
    .end local v2    # "msgid":I
    .end local v3    # "msglen":I
    :cond_2
    :goto_1
    if-eqz v5, :cond_0

    .line 712
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    invoke-virtual {v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->append([B)V

    .line 713
    iget-object v6, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->size()I

    move-result v6

    iget v7, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->msgLen:I

    if-lt v6, v7, :cond_0

    .line 714
    iget-object v6, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-virtual {v6}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->size()I

    move-result v6

    iget v7, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->msgLen:I

    if-eq v6, v7, :cond_4

    .line 715
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    iget v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 716
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "bad message size"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 722
    .end local v5    # "rcvMsg":Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    const-string v7, ""

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 724
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    iget v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 725
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v6, :cond_0

    .line 726
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v6}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onError()V

    goto :goto_0

    .line 699
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "rcvMsg":Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;
    :cond_3
    :try_start_1
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/navdy/hud/mfi/Utils;->unpackUInt16([BI)I

    move-result v1

    .line 700
    .local v1, "eaSessionId":I
    const-string v6, "MFi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Apple Device(E):"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Size:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 703
    iget-object v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    const/4 v7, 0x2

    iget-object v8, p1, Lcom/navdy/hud/mfi/SessionPacket;->data:[B

    array-length v8, v8

    invoke-static {v6, v7, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    .line 704
    .local v4, "payload":[B
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/mfi/EASession;

    invoke-virtual {v6, v4}, Lcom/navdy/hud/mfi/EASession;->queue([B)V

    goto/16 :goto_1

    .line 718
    .end local v1    # "eaSessionId":I
    .end local v4    # "payload":[B
    :cond_4
    iget v6, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    iget v7, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->msgId:I

    iget-object v8, v5, Lcom/navdy/hud/mfi/iAPProcessor$SessionMessage;->builder:Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-virtual {v8}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->build()[B

    move-result-object v8

    invoke-direct {p0, v6, v7, v8}, Lcom/navdy/hud/mfi/iAPProcessor;->handleSessionMessage(II[B)V

    .line 719
    iget-object v6, p0, Lcom/navdy/hud/mfi/iAPProcessor;->reassambleMsgs:Landroid/util/SparseArray;

    iget v7, p1, Lcom/navdy/hud/mfi/SessionPacket;->session:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static parse([B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    .locals 2
    .param p0, "data"    # [B

    .prologue
    .line 1182
    if-nez p0, :cond_0

    .line 1183
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 1185
    :cond_0
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;

    const/4 v1, 0x6

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;-><init>([BI)V

    .line 1186
    .local v0, "params":Lcom/navdy/hud/mfi/iAPProcessor$IAP2Params;
    return-object v0
.end method

.method private retryDeviceAuthentication()V
    .locals 3

    .prologue
    .line 658
    iget-boolean v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkEstablished:Z

    if-eqz v0, :cond_0

    .line 659
    iget v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->deviceAuthenticationRetries:I

    .line 660
    iget v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    new-instance v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 665
    :cond_0
    return-void
.end method

.method private sendHIDReport(I)V
    .locals 6
    .param p1, "keyMask"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1141
    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v0

    .line 1142
    .local v0, "component":I
    const/4 v1, 0x0

    .line 1143
    .local v1, "report":[I
    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$2;->$SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent:[I

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1151
    :goto_0
    iget v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AccessoryHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    .line 1154
    invoke-virtual {v3, v4, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDReport:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    .line 1156
    invoke-static {v1}, Lcom/navdy/hud/mfi/Utils;->intsToBytes([I)[B

    move-result-object v5

    .line 1155
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    .line 1151
    invoke-virtual {p0, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1157
    return-void

    .line 1145
    :pswitch_0
    const/4 v2, 0x2

    new-array v1, v2, [I

    .end local v1    # "report":[I
    aput v4, v1, v4

    aput p1, v1, v5

    .line 1146
    .restart local v1    # "report":[I
    goto :goto_0

    .line 1148
    :pswitch_1
    new-array v1, v5, [I

    .end local v1    # "report":[I
    aput p1, v1, v4

    .restart local v1    # "report":[I
    goto :goto_0

    .line 1143
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendIdentification(I)V
    .locals 10
    .param p1, "session"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 913
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->IdentificationInformation:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v0, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    .line 914
    .local v0, "message":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Name:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    iget-object v4, p0, Lcom/navdy/hud/mfi/iAPProcessor;->accessoryName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->ModelIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 915
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->Manufacturer:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 916
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SerialNumber:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 917
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->FirmwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 918
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->HardwareVersion:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    sget-object v5, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    .line 919
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 921
    new-instance v2, Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-direct {v2}, Lcom/navdy/hud/mfi/ByteArrayBuilder;-><init>()V

    .line 922
    .local v2, "messageSentByAccessory":Lcom/navdy/hud/mfi/ByteArrayBuilder;
    iget-boolean v3, p0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    if-eqz v3, :cond_0

    .line 923
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 924
    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestDeviceAuthenticationChallengeResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 925
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationSucceeded:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 926
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationFailed:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 927
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->build()[B

    .line 930
    :cond_0
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 931
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AccessoryHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 932
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 936
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopCallStateUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 937
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->InitiateCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->AcceptCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->EndCall:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->MuteStatusUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 945
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopNowPlayingUpdates:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 947
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAppLaunch:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 948
    new-instance v1, Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-direct {v1}, Lcom/navdy/hud/mfi/ByteArrayBuilder;-><init>()V

    .line 949
    .local v1, "messageReceivedByAccessory":Lcom/navdy/hud/mfi/ByteArrayBuilder;
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopExternalAccessoryProtocolSession:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 950
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 952
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceHIDReport:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 956
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->CallStateUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 964
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->NowPlayingUpdate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 966
    iget-boolean v3, p0, Lcom/navdy/hud/mfi/iAPProcessor;->isDeviceAuthenticationSupported:Z

    if-eqz v3, :cond_1

    .line 967
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationCertificate:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v3, v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-virtual {v1, v3}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->DeviceAuthenticationResponse:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v4, v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    .line 968
    invoke-virtual {v3, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    .line 971
    :cond_1
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesSentByAccessory:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->build()[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MessagesReceivedFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 972
    invoke-virtual {v1}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->build()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->PowerProvidingCapability:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 973
    invoke-virtual {v3, v4, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->MaximumCurrentDrawnFromDevice:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    .line 974
    invoke-virtual {v3, v4, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedExternalAccessoryProtocol:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    new-instance v5, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 977
    invoke-virtual {v5, v6, v8}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolName:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    sget-object v7, Lcom/navdy/hud/mfi/iAPProcessor;->protocols:[Ljava/lang/String;

    aget-object v7, v7, v8

    .line 978
    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolMatchAction:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 979
    invoke-virtual {v5, v6, v9}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    .line 980
    invoke-virtual {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v5

    .line 975
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedExternalAccessoryProtocol:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    new-instance v5, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 984
    invoke-virtual {v5, v6, v9}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt8(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolName:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    sget-object v7, Lcom/navdy/hud/mfi/iAPProcessor;->protocols:[Ljava/lang/String;

    aget-object v7, v7, v9

    .line 985
    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;->ExternalAccessoryProtocolMatchAction:Lcom/navdy/hud/mfi/iAPProcessor$ExternalAccessoryProtocol;

    .line 986
    invoke-virtual {v5, v6, v9}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    .line 987
    invoke-virtual {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v5

    .line 982
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->CurrentLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v5, "en"

    .line 989
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->SupportedLanguage:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    const-string v5, "en"

    .line 990
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->BluetoothTransportComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    new-instance v5, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const/16 v7, 0x4d2

    .line 992
    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportComponentName:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    const-string v7, "navdy-transport"

    .line 993
    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->TransportSupportsiAP2Connection:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    .line 994
    invoke-virtual {v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addNone(Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;->BluetoothTransportMediaAccessControlAddress:Lcom/navdy/hud/mfi/iAPProcessor$BluetoothTransportComponent;

    iget-object v7, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    .line 995
    invoke-virtual {v7}, Lcom/navdy/hud/mfi/LinkLayer;->getLocalAddress()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v5

    .line 996
    invoke-virtual {v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v5

    .line 991
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 1000
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;->iAP2HIDComponent:Lcom/navdy/hud/mfi/iAPProcessor$IdentificationInformation;

    new-instance v4, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    invoke-direct {v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;-><init>()V

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->HIDMediaRemote:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    .line 1001
    invoke-virtual {v6}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentName:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    const-string v6, "navdy-media-remote"

    .line 1002
    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;->HIDComponentFunction:Lcom/navdy/hud/mfi/iAPProcessor$iAP2HIDComponent;

    sget-object v6, Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;->MediaPlaybackRemote:Lcom/navdy/hud/mfi/iAPProcessor$HIDComponentFunction;

    .line 1003
    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v4

    .line 1004
    invoke-virtual {v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v4

    .line 1000
    invoke-virtual {v0, v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 1014
    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1016
    return-void
.end method

.method private startEASession(Lcom/navdy/hud/mfi/EASession;)V
    .locals 3
    .param p1, "eaSession"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 1042
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startEASession: session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/mfi/EASession;->getSessionIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onSessionStart(Lcom/navdy/hud/mfi/EASession;)V

    .line 1046
    :cond_0
    return-void
.end method

.method private startsWith([B[B)Z
    .locals 4
    .param p1, "d"    # [B
    .param p2, "prefix"    # [B

    .prologue
    const/4 v1, 0x0

    .line 198
    const/4 v0, 0x0

    .line 200
    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 207
    :cond_0
    :goto_1
    return v1

    .line 203
    :cond_1
    array-length v2, p2

    if-lt v0, v2, :cond_2

    .line 204
    const/4 v1, 0x1

    goto :goto_1

    .line 206
    :cond_2
    aget-byte v2, p1, v0

    aget-byte v3, p2, v0

    if-ne v2, v3, :cond_0

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private stopEASession(Lcom/navdy/hud/mfi/EASession;)V
    .locals 3
    .param p1, "eaSession"    # Lcom/navdy/hud/mfi/EASession;

    .prologue
    .line 1049
    sget-object v0, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopEASession: session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/mfi/EASession;->getSessionIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    if-eqz v0, :cond_0

    .line 1051
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    invoke-interface {v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor$StateListener;->onSessionStop(Lcom/navdy/hud/mfi/EASession;)V

    .line 1053
    :cond_0
    return-void
.end method


# virtual methods
.method public connect(Lcom/navdy/hud/mfi/IIAPFileTransferManager;)V
    .locals 0
    .param p1, "fileTransferManager"    # Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->mFileTransferManager:Lcom/navdy/hud/mfi/IIAPFileTransferManager;

    .line 147
    return-void
.end method

.method public connect(Lcom/navdy/hud/mfi/LinkLayer;)V
    .locals 0
    .param p1, "linkLayer"    # Lcom/navdy/hud/mfi/LinkLayer;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    .line 155
    return-void
.end method

.method public connect(Lcom/navdy/hud/mfi/iAPProcessor$StateListener;)V
    .locals 0
    .param p1, "eaSessionListener"    # Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->stateListener:Lcom/navdy/hud/mfi/iAPProcessor$StateListener;

    .line 151
    return-void
.end method

.method public getControlSessionId()I
    .locals 1

    .prologue
    .line 1160
    iget v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    return v0
.end method

.method public launchApp(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "appBundleId"    # Ljava/lang/String;
    .param p2, "launchAlert"    # Z

    .prologue
    .line 1164
    new-instance v0, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->RequestAppLaunch:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    iget v1, v1, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->id:I

    invoke-direct {v0, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(I)V

    .line 1165
    .local v0, "requestAppLaunchMessage":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->AppBundleID:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addString(Ljava/lang/Enum;Ljava/lang/String;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 1166
    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;->LaunchAlert:Lcom/navdy/hud/mfi/iAPProcessor$RequestAppLaunch;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithUserAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addEnum(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .line 1167
    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1168
    return-void

    .line 1166
    :cond_0
    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;->LaunchWithoutAlert:Lcom/navdy/hud/mfi/iAPProcessor$AppLaunchMethod;

    goto :goto_0
.end method

.method public launchApp(Z)V
    .locals 1
    .param p1, "launchAlert"    # Z

    .prologue
    .line 1177
    const-string v0, "com.navdy.NavdyClient"

    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->launchApp(Ljava/lang/String;Z)V

    .line 1178
    return-void
.end method

.method public linkLost()V
    .locals 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1057
    return-void
.end method

.method public onKeyDown(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 1128
    const/4 v1, 0x1

    shl-int v0, v1, p1

    .line 1129
    .local v0, "keyMask":I
    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendHIDReport(I)V

    .line 1130
    return-void
.end method

.method public onKeyDown(Ljava/lang/Enum;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Enum;

    .prologue
    .line 1124
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->onKeyDown(I)V

    .line 1125
    return-void
.end method

.method public onKeyUp(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 1137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendHIDReport(I)V

    .line 1138
    return-void
.end method

.method public onKeyUp(Ljava/lang/Enum;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Enum;

    .prologue
    .line 1133
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendHIDReport(I)V

    .line 1134
    return-void
.end method

.method public queue(Lcom/navdy/hud/mfi/EASessionPacket;)V
    .locals 13
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/EASessionPacket;

    .prologue
    .line 1067
    iget-object v10, p0, Lcom/navdy/hud/mfi/iAPProcessor;->eaSessionById:Landroid/util/SparseArray;

    iget v11, p1, Lcom/navdy/hud/mfi/EASessionPacket;->session:I

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/mfi/EASession;

    .line 1068
    .local v3, "eaSession":Lcom/navdy/hud/mfi/EASession;
    invoke-virtual {v3}, Lcom/navdy/hud/mfi/EASession;->getSessionIdentifier()I

    move-result v4

    .line 1069
    .local v4, "eaSessionIdentifier":I
    const/4 v9, 0x7

    .line 1070
    .local v9, "session":I
    iget-object v10, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    invoke-virtual {v10}, Lcom/navdy/hud/mfi/LinkLayer;->native_get_maximum_payload_size()I

    move-result v10

    add-int/lit8 v5, v10, -0x64

    .line 1071
    .local v5, "maximumLinkPayloadSize":I
    if-gtz v5, :cond_0

    .line 1090
    :goto_0
    return-void

    .line 1074
    :cond_0
    sget-object v10, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "EA->iAP["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/navdy/hud/mfi/Utils;->logTransfer(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1075
    iget-object v10, p1, Lcom/navdy/hud/mfi/EASessionPacket;->data:[B

    array-length v1, v10

    .line 1076
    .local v1, "dataLength":I
    move v8, v1

    .line 1077
    .local v8, "remainingDataLength":I
    const/4 v6, 0x0

    .line 1079
    .local v6, "offset":I
    :cond_1
    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 1080
    .local v7, "packetLength":I
    sub-int/2addr v8, v7

    .line 1081
    add-int/lit8 v0, v7, 0x2

    .line 1083
    .local v0, "bufferSize":I
    :try_start_0
    const-string v10, "MFi"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Accessory(E)   :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Size:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    new-instance v10, Lcom/navdy/hud/mfi/ByteArrayBuilder;

    invoke-direct {v10, v0}, Lcom/navdy/hud/mfi/ByteArrayBuilder;-><init>(I)V

    invoke-virtual {v10, v4}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addInt16(I)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/navdy/hud/mfi/EASessionPacket;->data:[B

    invoke-virtual {v10, v11, v6, v7}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->addBlob([BII)Lcom/navdy/hud/mfi/ByteArrayBuilder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/hud/mfi/ByteArrayBuilder;->build()[B

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(I[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1085
    add-int/2addr v6, v7

    .line 1089
    :goto_1
    if-gtz v8, :cond_1

    goto :goto_0

    .line 1086
    :catch_0
    move-exception v2

    .line 1087
    .local v2, "e":Ljava/io/IOException;
    sget-object v10, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error while sending to the device"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public queue(Lcom/navdy/hud/mfi/SessionPacket;)V
    .locals 3
    .param p1, "pkt"    # Lcom/navdy/hud/mfi/SessionPacket;

    .prologue
    .line 1061
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1062
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->myHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1063
    return-void
.end method

.method public registerControlMessageProcessor(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;Lcom/navdy/hud/mfi/IAPControlMessageProcessor;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    .param p2, "processor"    # Lcom/navdy/hud/mfi/IAPControlMessageProcessor;

    .prologue
    .line 158
    if-eqz p2, :cond_0

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlProcessors:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :cond_0
    return-void
.end method

.method public sendControlMessage(Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V
    .locals 1
    .param p1, "msg"    # Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .prologue
    .line 1022
    iget v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1023
    return-void
.end method

.method public sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V
    .locals 8
    .param p1, "sessionId"    # I
    .param p2, "msg"    # Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    .prologue
    .line 1026
    invoke-virtual {p2}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->toBytes()[B

    move-result-object v0

    .line 1027
    .local v0, "data":[B
    instance-of v3, p2, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    if-eqz v3, :cond_0

    move-object v1, p2

    .line 1028
    check-cast v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    .line 1029
    .local v1, "imsg":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->iAPMsgById:Landroid/util/SparseArray;

    iget v4, v1, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->msgId:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    .line 1031
    .local v2, "msgId_":Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    const-string v3, "MFi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sending message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1032
    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->TAG:Ljava/lang/String;

    const-string v4, "sending message %s (%d bytes)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    array-length v7, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/navdy/hud/mfi/Utils;->logTransfer(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1034
    .end local v1    # "imsg":Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;
    .end local v2    # "msgId_":Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(I[B)V

    .line 1035
    return-void
.end method

.method public sendToDevice(I[B)V
    .locals 2
    .param p1, "sessionId"    # I
    .param p2, "bytes"    # [B

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/navdy/hud/mfi/iAPProcessor;->linkLayer:Lcom/navdy/hud/mfi/LinkLayer;

    new-instance v1, Lcom/navdy/hud/mfi/SessionPacket;

    invoke-direct {v1, p1, p2}, Lcom/navdy/hud/mfi/SessionPacket;-><init>(I[B)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/mfi/LinkLayer;->queue(Lcom/navdy/hud/mfi/SessionPacket;)V

    .line 1039
    return-void
.end method

.method public setAccessoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "accessoryName"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->accessoryName:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public startHIDSession()V
    .locals 6

    .prologue
    .line 1093
    const/4 v1, 0x0

    .line 1094
    .local v1, "descriptor":[I
    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v2}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v0

    .line 1095
    .local v0, "component":I
    sget-object v2, Lcom/navdy/hud/mfi/iAPProcessor$2;->$SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent:[I

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v3}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1103
    :goto_0
    iget v2, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    new-instance v3, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StartHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v3, v4}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 1106
    invoke-virtual {v3, v4, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->VendorIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const v5, 0x9886

    .line 1107
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->ProductIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    const/4 v5, 0x1

    .line 1108
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/mfi/iAPProcessor$StartHID;->HIDReportDescriptor:Lcom/navdy/hud/mfi/iAPProcessor$StartHID;

    .line 1109
    invoke-static {v1}, Lcom/navdy/hud/mfi/Utils;->intsToBytes([I)[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;->addBlob(Ljava/lang/Enum;[B)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v3

    .line 1103
    invoke-virtual {p0, v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1112
    return-void

    .line 1097
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->hidDescriptorKeyboard:[I

    .line 1098
    goto :goto_0

    .line 1100
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->hidDescriptorMediaRemote:[I

    goto :goto_0

    .line 1095
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public stopHIDSession()V
    .locals 4

    .prologue
    .line 1115
    sget-object v1, Lcom/navdy/hud/mfi/iAPProcessor;->mediaRemoteComponent:Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;

    invoke-virtual {v1}, Lcom/navdy/hud/mfi/iAPProcessor$MediaRemoteComponent;->ordinal()I

    move-result v0

    .line 1116
    .local v0, "component":I
    iget v1, p0, Lcom/navdy/hud/mfi/iAPProcessor;->controlSession:I

    new-instance v2, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;->StopHID:Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;

    invoke-direct {v2, v3}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;-><init>(Lcom/navdy/hud/mfi/iAPProcessor$iAPMessage;)V

    sget-object v3, Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;->HIDComponentIdentifier:Lcom/navdy/hud/mfi/iAPProcessor$AccessoryHIDReport;

    .line 1119
    invoke-virtual {v2, v3, v0}, Lcom/navdy/hud/mfi/iAPProcessor$IAP2SessionMessage;->addUInt16(Ljava/lang/Enum;I)Lcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;

    move-result-object v2

    .line 1116
    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/mfi/iAPProcessor;->sendToDevice(ILcom/navdy/hud/mfi/iAPProcessor$IAP2ParamsCreator;)V

    .line 1121
    return-void
.end method
