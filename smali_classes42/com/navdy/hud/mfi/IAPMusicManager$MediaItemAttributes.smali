.class public final enum Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
.super Ljava/lang/Enum;
.source "IAPMusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/mfi/IAPMusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaItemAttributes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemAlbumTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemAlbumTrackCount:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemAlbumTrackNumber:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemArtist:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemArtworkFileTransferIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemGenre:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemPersistentIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemPlaybackDurationInMilliSeconds:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

.field public static final enum MediaItemTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;


# instance fields
.field paramIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemPersistentIdentifier"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPersistentIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 26
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemTitle"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 27
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemPlaybackDurationInMilliSeconds"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPlaybackDurationInMilliSeconds:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 28
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemAlbumTitle"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 29
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemAlbumTrackNumber"

    invoke-direct {v0, v1, v6, v8}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackNumber:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 30
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemAlbumTrackCount"

    const/4 v2, 0x5

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackCount:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 31
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemArtist"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtist:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 32
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemGenre"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemGenre:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 33
    new-instance v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    const-string v1, "MediaItemArtworkFileTransferIdentifier"

    const/16 v2, 0x8

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtworkFileTransferIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    .line 24
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPersistentIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v1, v0, v5

    const/4 v1, 0x2

    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemPlaybackDurationInMilliSeconds:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTitle:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackNumber:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemAlbumTrackCount:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtist:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemGenre:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v1, v0, v8

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->MediaItemArtworkFileTransferIdentifier:Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->paramIndex:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->$VALUES:[Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    invoke-virtual {v0}, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;

    return-object v0
.end method


# virtual methods
.method public getParam()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/hud/mfi/IAPMusicManager$MediaItemAttributes;->paramIndex:I

    return v0
.end method
